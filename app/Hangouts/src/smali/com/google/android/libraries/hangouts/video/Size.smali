.class public Lcom/google/android/libraries/hangouts/video/Size;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final height:I

.field public final width:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    .line 23
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/Size;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    .line 33
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    .line 34
    return-void
.end method

.method private static getScale(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)F
    .locals 7

    .prologue
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 79
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v1, v1

    div-float v2, v0, v1

    .line 81
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v0, v0

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 83
    float-to-double v3, v2

    cmpg-double v0, v3, v5

    if-gez v0, :cond_0

    float-to-double v3, v1

    cmpl-double v0, v3, v5

    if-gtz v0, :cond_1

    :cond_0
    float-to-double v3, v2

    cmpl-double v0, v3, v5

    if-lez v0, :cond_2

    float-to-double v3, v1

    cmpg-double v0, v3, v5

    if-gez v0, :cond_2

    .line 84
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    div-float v1, v0, v1

    .line 85
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, p1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget v4, p1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    invoke-direct {v0, v3, v4}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    .line 89
    :goto_0
    cmpl-float v1, v2, v1

    if-lez v1, :cond_3

    .line 90
    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 94
    :goto_1
    return v0

    .line 87
    :cond_2
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, p1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget v4, p1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-direct {v0, v3, v4}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    goto :goto_0

    .line 92
    :cond_3
    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_1
.end method

.method public static newWithScale(Lcom/google/android/libraries/hangouts/video/Size;F)Lcom/google/android/libraries/hangouts/video/Size;
    .locals 3

    .prologue
    .line 65
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 66
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    .line 68
    add-int/lit8 v0, v0, 0x2

    and-int/lit8 v0, v0, -0x4

    .line 69
    add-int/lit8 v1, v1, 0x2

    and-int/lit8 v1, v1, -0x4

    .line 70
    new-instance v2, Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v2, v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    return-object v2
.end method

.method public static scaleDownToFitInside(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;
    .locals 5

    .prologue
    .line 116
    invoke-static {p0, p1}, Lcom/google/android/libraries/hangouts/video/Size;->getScale(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)F

    move-result v0

    .line 117
    float-to-double v1, v0

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    .line 118
    invoke-static {p0, v0}, Lcom/google/android/libraries/hangouts/video/Size;->newWithScale(Lcom/google/android/libraries/hangouts/video/Size;F)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object p0

    .line 120
    :cond_0
    return-object p0
.end method

.method public static scaleUpToFitInside(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;
    .locals 5

    .prologue
    .line 103
    invoke-static {p0, p1}, Lcom/google/android/libraries/hangouts/video/Size;->getScale(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)F

    move-result v0

    .line 104
    float-to-double v1, v0

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    .line 105
    invoke-static {p0, v0}, Lcom/google/android/libraries/hangouts/video/Size;->newWithScale(Lcom/google/android/libraries/hangouts/video/Size;F)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object p0

    .line 107
    :cond_0
    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    instance-of v1, p1, Lcom/google/android/libraries/hangouts/video/Size;

    if-nez v1, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    check-cast p1, Lcom/google/android/libraries/hangouts/video/Size;

    .line 49
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getEncodedDimensions()I
    .locals 2

    .prologue
    .line 130
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    or-int/2addr v0, v1

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    mul-int/lit16 v0, v0, 0x7fc9

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
