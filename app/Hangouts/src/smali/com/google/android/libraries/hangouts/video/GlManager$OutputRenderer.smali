.class Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

.field private initialized:Z

.field private final surfaceConfig:[I

.field private final surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

.field private final videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

.field private final videoSourceRenderer:Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/GlManager;Lcom/google/android/libraries/hangouts/video/SurfaceInfo;Lcom/google/android/libraries/hangouts/video/VideoSource;)V
    .locals 1

    .prologue
    .line 285
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 280
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->surfaceConfig:[I

    .line 286
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    .line 287
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    .line 288
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;

    invoke-direct {v0, p3, p2}, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/VideoSource;Lcom/google/android/libraries/hangouts/video/SurfaceInfo;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->videoSourceRenderer:Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;

    .line 289
    return-void

    .line 280
    nop

    :array_0
    .array-data 4
        0x3086
        0x3085
        0x3038
    .end array-data
.end method


# virtual methods
.method initIfNeeded()I
    .locals 5

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->initialized:Z

    if-nez v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    .line 318
    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getSurface()Landroid/view/Surface;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->surfaceConfig:[I

    .line 317
    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->initialized:Z

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v0, v1, :cond_1

    .line 322
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    .line 326
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method release()V
    .locals 5

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_0

    .line 331
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->videoSourceRenderer:Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->release()V

    .line 332
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 336
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 337
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 339
    :cond_0
    return-void
.end method

.method render()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 292
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v1, v2, :cond_1

    .line 293
    const/16 v0, 0x300d

    .line 312
    :cond_0
    :goto_0
    return v0

    .line 295
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/VideoSource;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    .line 299
    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v5}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 298
    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 300
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    goto :goto_0

    .line 304
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->videoSourceRenderer:Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->drawFrame()V

    .line 309
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->glSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 310
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    goto :goto_0
.end method
