.class Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final EGL_CONTEXT_CLIENT_VERSION:I = 0x3098

.field private static final GLES_VERSION:I = 0x2

.field private static final RGB_SIZE_BITS:I = 0x8


# instance fields
.field egl:Ljavax/microedition/khronos/egl/EGL10;

.field eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field eglContext:Ljavax/microedition/khronos/egl/EGLContext;

.field eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private final mConfigSpec:[I

.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/GlManager;


# direct methods
.method private constructor <init>(Lcom/google/android/libraries/hangouts/video/GlManager;)V
    .locals 1

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->mConfigSpec:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x0
        0x3038
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/hangouts/video/GlManager;Lcom/google/android/libraries/hangouts/video/GlManager$1;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;-><init>(Lcom/google/android/libraries/hangouts/video/GlManager;)V

    return-void
.end method

.method private chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 144
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 145
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->mConfigSpec:[I

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_0
    aget v4, v5, v4

    .line 151
    if-gtz v4, :cond_1

    .line 152
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No configs match configSpec"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_1
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 157
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->mConfigSpec:[I

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 159
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig#2 failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_2
    invoke-direct {p0, v3}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->chooseConfig([Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    .line 162
    if-nez v0, :cond_3

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No config chosen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_3
    return-object v0
.end method

.method private chooseConfig([Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v5, 0x0

    .line 169
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_1

    aget-object v3, p1, v6

    .line 170
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/16 v4, 0x3024

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    .line 172
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/16 v4, 0x3023

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    .line 174
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/16 v4, 0x3022

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    .line 176
    if-ne v8, v10, :cond_0

    if-ne v9, v10, :cond_0

    if-ne v0, v10, :cond_0

    .line 180
    :goto_1
    return-object v3

    .line 169
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 180
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 186
    invoke-interface {p1, p2, p3, p4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    const/4 v1, 0x0

    aget p5, v0, v1

    .line 189
    :cond_0
    return p5
.end method


# virtual methods
.method init()V
    .locals 5

    .prologue
    .line 112
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 114
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 115
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglInitialize failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 120
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 121
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 124
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v0, v1, :cond_2

    .line 125
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglCreateContext failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_2
    return-void

    .line 120
    :array_0
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 137
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    .line 138
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 139
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 140
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 141
    return-void
.end method
