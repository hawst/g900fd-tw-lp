.class final Lcom/google/android/libraries/hangouts/video/CallSession;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private final mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    .line 24
    return-void
.end method


# virtual methods
.method public addLogComment(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->addLogComment(Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public bindRenderer(III)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/CallManager;->bindRenderer(III)V

    .line 90
    return-void
.end method

.method public blockMedia(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->blockMedia(Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public getCallTag(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->getCallTag(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public handlePushNotification([B)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->handlePushNotification([B)V

    .line 46
    return-void
.end method

.method public invitePstn(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/CallManager;->invitePstn(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 70
    return-void
.end method

.method public inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 62
    if-eqz p4, :cond_0

    if-ne p4, v0, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/libraries/hangouts/video/CallManager;->inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V

    .line 66
    return-void

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepareCall(Lcom/google/android/libraries/hangouts/video/CallOptions;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->prepareCall(Lcom/google/android/libraries/hangouts/video/CallOptions;)Z

    .line 28
    return-void
.end method

.method public publishVideoMuteState(Z)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->publishVideoMuteState(Z)V

    .line 102
    return-void
.end method

.method public remoteMute(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->remoteMute(Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method public requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V

    .line 86
    return-void
.end method

.method public sendDtmf(CILjava/lang/String;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/CallManager;->sendDtmf(CILjava/lang/String;)V

    .line 74
    return-void
.end method

.method public setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 78
    return-void
.end method

.method public setCallTag(Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallManager;->setCallTag(Ljava/lang/String;Ljava/lang/Object;)V

    .line 117
    return-void
.end method

.method public setMute(Z)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->setMute(Z)V

    .line 82
    return-void
.end method

.method public signIn(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->signIn(Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method public startCall(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/libraries/hangouts/video/CallManager;->startCall(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public terminateCall(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallManager;->terminateCall(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public unbindRenderer(I)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallSession;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->unbindRenderer(I)V

    .line 94
    return-void
.end method
