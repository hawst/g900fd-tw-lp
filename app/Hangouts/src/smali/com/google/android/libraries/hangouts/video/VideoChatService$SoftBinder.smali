.class public Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;
.super Landroid/os/Binder;
.source "PG"


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public addRemoteCallStateListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$200(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->addCallStateListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V

    goto :goto_0
.end method

.method public bindRenderer(III)V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$200(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    :goto_0
    return-void

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/CallManager;->bindRenderer(III)V

    goto :goto_0
.end method

.method public getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$200(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    const/4 v0, 0x0

    .line 362
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    goto :goto_0
.end method

.method public getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$200(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const/4 v0, 0x0

    .line 376
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreviousCall()Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getPreviousCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    return-object v0
.end method

.method public removeRemoteCallStateListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$200(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    :goto_0
    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->removeCallStateListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V

    goto :goto_0
.end method

.method public requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$200(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V

    goto :goto_0
.end method

.method public unbindRenderer(I)V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$200(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    # getter for: Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->unbindRenderer(I)V

    goto :goto_0
.end method
