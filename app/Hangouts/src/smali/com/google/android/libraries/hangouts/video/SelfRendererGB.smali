.class public final Lcom/google/android/libraries/hangouts/video/SelfRendererGB;
.super Lcom/google/android/libraries/hangouts/video/SelfRenderer;
.source "PG"


# instance fields
.field private final mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;

.field private volatile mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

.field private final mCameraStopWatch:Lcxb;

.field private mLastCameraFrameTimeNs:J

.field private mPendingFrame:[B

.field private mRotationTransform:[F

.field private final mSelfFrameReadyLock:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mSelfFrameReadyLock:Ljava/lang/Object;

    .line 18
    new-instance v0, Lcxb;

    const-string v1, "SelfVideo"

    invoke-direct {v0, v1}, Lcxb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraStopWatch:Lcxb;

    .line 22
    new-instance v0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;-><init>(Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;

    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->instantiateRenderer(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    .line 43
    if-eqz p3, :cond_0

    iget v0, p3, Lcom/google/android/libraries/hangouts/video/CameraSpecification;->cameraId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->setDefaultCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->registerRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 47
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mSelfFrameReadyLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;J)J
    .locals 0

    .prologue
    .line 14
    iput-wide p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mLastCameraFrameTimeNs:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;)[B
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mPendingFrame:[B

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;[B)[B
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mPendingFrame:[B

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;)Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    return-object v0
.end method


# virtual methods
.method public drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 194
    const/4 v0, 0x0

    .line 195
    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mHaveInitializedCameraSettings:Z

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mSelfFrameReadyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 198
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mPendingFrame:[B

    .line 199
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mPendingFrame:[B

    .line 200
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    if-eqz v2, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraStopWatch:Lcxb;

    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotationTransform:[F

    iput-object v1, v0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;->transformMatrix:[F

    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;

    iput-object v2, v0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;->frameData:[B

    .line 205
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    iget-wide v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mLastCameraFrameTimeNs:J

    .line 206
    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->translateFrameTime(J)J

    move-result-wide v3

    iput-wide v3, v0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;->lastCameraFrameTimeNs:J

    .line 207
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererGB$RendererFrameInputDataGB;

    invoke-virtual {v0, v1, v3, v5}, Lcom/google/android/libraries/hangouts/video/RendererManager;->renderFrame(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->returnBuffer([B)V

    .line 209
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraStopWatch:Lcxb;

    .line 210
    const/4 v0, 0x1

    .line 213
    :cond_0
    return v0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public encodeFrame()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method protected getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    return-object v0
.end method

.method public getOutputTextureName()I
    .locals 1

    .prologue
    .line 171
    const/4 v0, -0x1

    return v0
.end method

.method public initializeGLContext()V
    .locals 2

    .prologue
    .line 156
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "vclib"

    const-string v1, "initializeGLContext"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mHaveInitializedCameraSettings:Z

    .line 162
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mSelfFrameReadyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 163
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mPendingFrame:[B

    .line 164
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->initializeGLContext(I)Z

    .line 167
    return-void

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onCameraOpened(Lcom/google/android/libraries/hangouts/video/Size;IZ)V
    .locals 4

    .prologue
    .line 53
    invoke-static {}, Lcwz;->c()V

    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    const-string v2, "sub_indims"

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    .line 56
    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentPreviewSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/Size;->getEncodedDimensions()I

    move-result v3

    .line 54
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;-><init>(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->setCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 73
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(Lcom/google/android/libraries/hangouts/video/Size;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 74
    iput-boolean p3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraCaptureFlip:Z

    .line 75
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraOrientation:I

    .line 76
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->recomputeCameraRotation()V

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    invoke-interface {v0, p3}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->onCameraOpened(Z)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->onFrameSizesChanged()V

    .line 79
    return-void
.end method

.method protected onFrameSizesChanged()V
    .locals 5

    .prologue
    .line 84
    invoke-static {}, Lcwz;->c()V

    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-super {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->onFrameSizesChanged()V

    .line 94
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(Lcom/google/android/libraries/hangouts/video/Size;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 96
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraRotation:I

    sparse-switch v0, :sswitch_data_0

    .line 107
    invoke-static {}, Lcxd;->a()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotationTransform:[F

    .line 111
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    const-string v2, "c_rotation"

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraRotation:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    const-string v2, "sub_outdims"

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 115
    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/Size;->getEncodedDimensions()I

    move-result v3

    .line 113
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentPreviewSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    const-string v2, "sub_indims"

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    .line 119
    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentPreviewSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/Size;->getEncodedDimensions()I

    move-result v3

    .line 117
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 122
    :cond_1
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "frameOutputParameters = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->getFrameOutputParameters()Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->size:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMaxFrameOutputParameters = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mMaxFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRotatedCaptureSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 125
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRotatedCameraBufferSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 126
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRotatedScaledCameraBufferSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 128
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRotatedFrameOutputSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 129
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    sub-int/2addr v0, v1

    .line 134
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    sub-int/2addr v1, v2

    .line 136
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraRotation:I

    const/16 v3, 0x5a

    if-eq v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraRotation:I

    const/16 v3, 0x10e

    if-ne v2, v3, :cond_4

    .line 138
    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    const-string v4, "sub_inclip"

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 146
    :goto_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->onFrameGeometryChanged(IIII)V

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mHaveInitializedCameraSettings:Z

    goto/16 :goto_0

    .line 98
    :sswitch_0
    invoke-static {}, Lcxd;->b()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotationTransform:[F

    goto/16 :goto_1

    .line 101
    :sswitch_1
    invoke-static {}, Lcxd;->c()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotationTransform:[F

    goto/16 :goto_1

    .line 104
    :sswitch_2
    invoke-static {}, Lcxd;->d()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRotationTransform:[F

    goto/16 :goto_1

    .line 142
    :cond_4
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    const-string v4, "sub_inclip"

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v1

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    goto :goto_2

    .line 96
    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method

.method public release()V
    .locals 3

    .prologue
    .line 176
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "release enc:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->endCallback()V

    .line 180
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->unregisterRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mEncodeRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->releaseRenderer(I)V

    .line 182
    return-void
.end method
