.class Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver$1;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;)V
    .locals 0

    .prologue
    .line 1584
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver$1;->this$1:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1587
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver$1;->this$1:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mConnected:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->access$2100(Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1589
    const-string v0, "vclib"

    const-string v1, "We still don\'t have a connection after 5 seconds. Terminate the call"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1592
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver$1;->this$1:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->handleNetworkDisconnect()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$2200(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    .line 1594
    :cond_0
    return-void
.end method
