.class public final Lcom/google/android/libraries/hangouts/video/SelfRendererICS;
.super Lcom/google/android/libraries/hangouts/video/SelfRenderer;
.source "PG"


# instance fields
.field private final mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

.field private final mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererICS$RendererFrameInputDataICS;

.field private final mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

.field private final mEncodeStopWatch:Lcxb;

.field private mOutputTextureName:I

.field private mUseMaxSizeForCameraBuffer:Z


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    .line 22
    new-instance v0, Lcxb;

    const-string v1, "SelfVideo.encode"

    invoke-direct {v0, v1}, Lcxb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeStopWatch:Lcxb;

    .line 27
    new-instance v0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS$RendererFrameInputDataICS;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS$RendererFrameInputDataICS;-><init>(Lcom/google/android/libraries/hangouts/video/SelfRendererICS$1;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererICS$RendererFrameInputDataICS;

    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 38
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t use this on Gingerbread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    .line 41
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/CameraManagerICS;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->instantiateRenderer(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    .line 45
    if-eqz p3, :cond_1

    iget v0, p3, Lcom/google/android/libraries/hangouts/video/CameraSpecification;->cameraId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->setDefaultCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->registerRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 49
    return-void
.end method

.method private setOutputTextureName(I)V
    .locals 2

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mOutputTextureName:I

    if-eq p1, v0, :cond_0

    .line 157
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mOutputTextureName:I

    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mOutputTextureName:I

    invoke-interface {v0, v1}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->onOutputTextureNameChanged(I)V

    .line 162
    :cond_0
    return-void
.end method


# virtual methods
.method public drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z
    .locals 2

    .prologue
    .line 193
    const/4 v0, 0x0

    .line 194
    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mHaveInitializedCameraSettings:Z

    if-eqz v1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->render()Z

    move-result v0

    .line 197
    :cond_0
    return v0
.end method

.method public encodeFrame()V
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeStopWatch:Lcxb;

    .line 206
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererICS$RendererFrameInputDataICS;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    .line 207
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->getMostRecentCameraFrameTimeNs()J

    move-result-wide v2

    .line 206
    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->translateFrameTime(J)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS$RendererFrameInputDataICS;->lastCameraFrameTimeNs:J

    .line 208
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/SelfRendererICS$RendererFrameInputDataICS;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->renderFrame(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeStopWatch:Lcxb;

    .line 210
    return-void
.end method

.method protected getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    return-object v0
.end method

.method public getMostRecentCameraFrameTimeNs()J
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->getMostRecentCameraFrameTimeNs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getOutputTextureName()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mOutputTextureName:I

    return v0
.end method

.method public initializeGLContext()V
    .locals 4

    .prologue
    .line 133
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    const-string v0, "vclib"

    const-string v1, "initializeGLContext"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mHaveInitializedCameraSettings:Z

    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->initializeGLContext()V

    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->initializeGLContext(I)Z

    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->getOutputTextureName()I

    move-result v0

    .line 144
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    const-string v3, "sub_intex"

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 147
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->setOutputTextureName(I)V

    .line 148
    return-void
.end method

.method protected onCameraOpened(Lcom/google/android/libraries/hangouts/video/Size;IZ)V
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcwz;->c()V

    .line 55
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 56
    iput-boolean p3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraCaptureFlip:Z

    .line 57
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraOrientation:I

    .line 58
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->recomputeCameraRotation()V

    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    invoke-interface {v0, p3}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->onCameraOpened(Z)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->onFrameSizesChanged()V

    .line 61
    return-void
.end method

.method protected onFrameSizesChanged()V
    .locals 6

    .prologue
    .line 76
    invoke-static {}, Lcwz;->c()V

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    if-nez v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-super {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->onFrameSizesChanged()V

    .line 85
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mUseMaxSizeForCameraBuffer:Z

    if-eqz v0, :cond_2

    .line 86
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(Lcom/google/android/libraries/hangouts/video/Size;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 91
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraRotation:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->setRotation(I)V

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraCaptureFlip:Z

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->setFlipNeeded(Z)V

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    const-string v2, "c_rotation"

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraRotation:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    const-string v2, "sub_outdims"

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 98
    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/Size;->getEncodedDimensions()I

    move-result v3

    .line 96
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    const-string v2, "sub_indims"

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 101
    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/Size;->getEncodedDimensions()I

    move-result v3

    .line 99
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 103
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Self dimensions - frameOutputParameters = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->getFrameOutputParameters()Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->size:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMaxFrameOutputParameters = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mMaxFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRotatedCaptureSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 107
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRotatedCameraBufferSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 108
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRotatedScaledCameraBufferSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 110
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRotatedFrameOutputSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 111
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    const-string v2, "sub_inclip"

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    sub-int/2addr v3, v4

    shl-int/lit8 v3, v3, 0x10

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v5, v5, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    sub-int/2addr v4, v5

    or-int/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->setOutputSize(Lcom/google/android/libraries/hangouts/video/Size;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->onFrameGeometryChanged(IIII)V

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mHaveInitializedCameraSettings:Z

    goto/16 :goto_0

    .line 88
    :cond_2
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(Lcom/google/android/libraries/hangouts/video/Size;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRotatedCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    goto/16 :goto_1
.end method

.method public release()V
    .locals 3

    .prologue
    .line 167
    invoke-static {}, Lcwz;->a()V

    .line 168
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "release enc:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mCameraInput:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->release()V

    .line 173
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->unregisterRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mEncodeRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->releaseRenderer(I)V

    .line 175
    return-void
.end method

.method public setUseMaxSizeForCameraBuffer(Z)V
    .locals 3

    .prologue
    .line 65
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setUseMaxSizeForCameraBuffer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    invoke-static {}, Lcwz;->c()V

    .line 69
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->mUseMaxSizeForCameraBuffer:Z

    .line 70
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->onFrameSizesChanged()V

    .line 71
    return-void
.end method
