.class Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mSecondsSinceCallStart:J

.field private final mStatsObject:Lcom/google/android/libraries/hangouts/video/Stats;

.field private final mTime:J


# direct methods
.method private constructor <init>(JJLcom/google/android/libraries/hangouts/video/Stats;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-wide p1, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mTime:J

    .line 51
    iput-wide p3, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mSecondsSinceCallStart:J

    .line 52
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mStatsObject:Lcom/google/android/libraries/hangouts/video/Stats;

    .line 53
    return-void
.end method

.method synthetic constructor <init>(JJLcom/google/android/libraries/hangouts/video/Stats;Lcom/google/android/libraries/hangouts/video/CallStatistics$1;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct/range {p0 .. p5}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;-><init>(JJLcom/google/android/libraries/hangouts/video/Stats;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)Lcom/google/android/libraries/hangouts/video/Stats;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mStatsObject:Lcom/google/android/libraries/hangouts/video/Stats;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mSecondsSinceCallStart:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mTime:J

    return-wide v0
.end method
