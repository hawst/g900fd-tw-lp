.class Lcom/google/android/gms/people/ownerslisthelper/ShrinkingItem;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 18
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/ownerslisthelper/ShrinkingItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/gms/people/ownerslisthelper/ShrinkingItem;->a:F

    .line 24
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 35
    invoke-virtual {p0}, Lcom/google/android/gms/people/ownerslisthelper/ShrinkingItem;->getMeasuredHeight()I

    move-result v0

    .line 36
    iget v1, p0, Lcom/google/android/gms/people/ownerslisthelper/ShrinkingItem;->a:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/people/ownerslisthelper/ShrinkingItem;->a:F

    int-to-float v0, v0

    mul-float/2addr v0, v1

    .line 37
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 38
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/people/ownerslisthelper/ShrinkingItem;->setMeasuredDimension(II)V

    .line 39
    return-void
.end method
