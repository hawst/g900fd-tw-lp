.class public final Lcom/google/android/gms/internal/ot;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcku;


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Ljava/lang/String;

.field public final d:Z

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcku;

    invoke-direct {v0}, Lcku;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ot;->CREATOR:Lcku;

    return-void
.end method

.method public constructor <init>(IZZLjava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ot;->e:I

    iput-boolean p2, p0, Lcom/google/android/gms/internal/ot;->a:Z

    iput-boolean p3, p0, Lcom/google/android/gms/internal/ot;->b:Z

    iput-object p4, p0, Lcom/google/android/gms/internal/ot;->c:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/ot;->d:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/ot;->e:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lf;->a(Ljava/lang/Object;)Lcip;

    move-result-object v0

    const-string v1, "useOfflineDatabase"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/ot;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcip;->a(Ljava/lang/String;Ljava/lang/Object;)Lcip;

    move-result-object v0

    const-string v1, "useWebData"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/ot;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcip;->a(Ljava/lang/String;Ljava/lang/Object;)Lcip;

    move-result-object v0

    const-string v1, "endpoint"

    iget-object v2, p0, Lcom/google/android/gms/internal/ot;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcip;->a(Ljava/lang/String;Ljava/lang/Object;)Lcip;

    move-result-object v0

    invoke-virtual {v0}, Lcip;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcku;->a(Lcom/google/android/gms/internal/ot;Landroid/os/Parcel;)V

    return-void
.end method
