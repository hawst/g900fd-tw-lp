.class public Lcom/google/common/cache/LocalCache;
.super Ljava/util/AbstractMap;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field public static final b:Lehv;

.field public static final t:Lecw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lecw",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final c:I

.field final d:I

.field public final e:[Lecj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lecj",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final f:Leba;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Leba",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Leba;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Leba",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final h:Lecm;

.field public final i:Lecm;

.field final j:J

.field public final k:La;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final l:J

.field final m:J

.field public final n:J

.field public final o:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ledn",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final p:La;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final q:Lebm;

.field public final r:Lebt;

.field public final s:Lebo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lebo",
            "<-TK;TV;>;"
        }
    .end annotation
.end field

.field v:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field w:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field x:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    const-class v0, Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/common/cache/LocalCache;->a:Ljava/util/logging/Logger;

    .line 157
    invoke-static {}, Lf;->C()Lehv;

    move-result-object v0

    sput-object v0, Lcom/google/common/cache/LocalCache;->b:Lehv;

    .line 690
    new-instance v0, Lebq;

    invoke-direct {v0}, Lebq;-><init>()V

    sput-object v0, Lcom/google/common/cache/LocalCache;->t:Lecw;

    .line 983
    new-instance v0, Lebr;

    invoke-direct {v0}, Lebr;-><init>()V

    sput-object v0, Lcom/google/common/cache/LocalCache;->u:Ljava/util/Queue;

    return-void
.end method

.method public static a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1878
    invoke-interface {p0, p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setNextInAccessQueue(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 1879
    invoke-interface {p1, p0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setPreviousInAccessQueue(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 1880
    return-void
.end method

.method public static b(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1884
    sget-object v0, Leci;->a:Leci;

    .line 1885
    invoke-interface {p0, v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setNextInAccessQueue(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 1886
    invoke-interface {p0, v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setPreviousInAccessQueue(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 1887
    return-void
.end method

.method public static b(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1891
    invoke-interface {p0, p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setNextInWriteQueue(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 1892
    invoke-interface {p1, p0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setPreviousInWriteQueue(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 1893
    return-void
.end method

.method public static c(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1897
    sget-object v0, Leci;->a:Leci;

    .line 1898
    invoke-interface {p0, v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setNextInWriteQueue(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 1899
    invoke-interface {p0, v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setPreviousInWriteQueue(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 1900
    return-void
.end method

.method public static i()Lecw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lecw",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 730
    sget-object v0, Lcom/google/common/cache/LocalCache;->t:Lecw;

    return-object v0
.end method

.method public static j()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 980
    sget-object v0, Leci;->a:Leci;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 1797
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v0, p1}, Leba;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1798
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method a(I)Lecj;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lecj",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1829
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->e:[Lecj;

    iget v1, p0, Lcom/google/common/cache/LocalCache;->d:I

    ushr-int v1, p1, v1

    iget v2, p0, Lcom/google/common/cache/LocalCache;->c:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public a(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;J)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1844
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1855
    :cond_0
    :goto_0
    return-object v0

    .line 1847
    :cond_1
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v1

    invoke-interface {v1}, Lecw;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1848
    if-eqz v1, :cond_0

    .line 1852
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/common/cache/LocalCache;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 1855
    goto :goto_0
.end method

.method public a(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1808
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v0

    .line 1809
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;I)Z

    .line 1810
    return-void
.end method

.method public a(Lecw;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lecw",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1802
    invoke-interface {p1}, Lecw;->b()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 1803
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v1

    .line 1804
    invoke-virtual {p0, v1}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lecj;->a(Ljava/lang/Object;ILecw;)Z

    .line 1805
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 311
    iget-wide v0, p0, Lcom/google/common/cache/LocalCache;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()Z
    .locals 4

    .prologue
    .line 323
    iget-wide v0, p0, Lcom/google/common/cache/LocalCache;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;J)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1864
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1865
    invoke-virtual {p0}, Lcom/google/common/cache/LocalCache;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getAccessTime()J

    move-result-wide v1

    sub-long v1, p2, v1

    iget-wide v3, p0, Lcom/google/common/cache/LocalCache;->l:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1

    .line 1871
    :cond_0
    :goto_0
    return v0

    .line 1868
    :cond_1
    invoke-virtual {p0}, Lcom/google/common/cache/LocalCache;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getWriteTime()J

    move-result-wide v1

    sub-long v1, p2, v1

    iget-wide v3, p0, Lcom/google/common/cache/LocalCache;->m:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    .line 1871
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 4

    .prologue
    .line 327
    iget-wide v0, p0, Lcom/google/common/cache/LocalCache;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 4

    .prologue
    .line 4142
    iget-object v1, p0, Lcom/google/common/cache/LocalCache;->e:[Lecj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 4143
    invoke-virtual {v3}, Lecj;->a()V

    .line 4142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4145
    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 4036
    if-nez p1, :cond_0

    .line 4037
    const/4 v0, 0x0

    .line 4040
    :goto_0
    return v0

    .line 4039
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/common/cache/LocalCache;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4040
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lecj;->b(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 19

    .prologue
    .line 4046
    if-nez p1, :cond_0

    .line 4047
    const/4 v3, 0x0

    .line 4081
    :goto_0
    return v3

    .line 4055
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v3}, Lebm;->a()J

    move-result-wide v11

    .line 4056
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/common/cache/LocalCache;->e:[Lecj;

    .line 4057
    const-wide/16 v6, -0x1

    .line 4058
    const/4 v3, 0x0

    move v8, v3

    move-wide v9, v6

    :goto_1
    const/4 v3, 0x3

    if-ge v8, v3, :cond_5

    .line 4059
    const-wide/16 v4, 0x0

    .line 4060
    array-length v14, v13

    const/4 v3, 0x0

    move-wide v6, v4

    move v5, v3

    :goto_2
    if-ge v5, v14, :cond_4

    aget-object v15, v13, v5

    .line 4063
    iget v3, v15, Lecj;->b:I

    .line 4065
    iget-object v0, v15, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-object/from16 v16, v0

    .line 4066
    const/4 v3, 0x0

    move v4, v3

    :goto_3
    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v3

    if-ge v4, v3, :cond_3

    .line 4067
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    :goto_4
    if-eqz v3, :cond_2

    .line 4068
    invoke-virtual {v15, v3, v11, v12}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Ljava/lang/Object;

    move-result-object v17

    .line 4069
    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->g:Leba;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 4070
    const/4 v3, 0x1

    goto :goto_0

    .line 4067
    :cond_1
    invoke-interface {v3}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v3

    goto :goto_4

    .line 4066
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 4074
    :cond_3
    iget v3, v15, Lecj;->d:I

    int-to-long v3, v3

    add-long/2addr v6, v3

    .line 4060
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 4076
    :cond_4
    cmp-long v3, v6, v9

    if-eqz v3, :cond_5

    .line 4058
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move-wide v9, v6

    goto :goto_1

    .line 4081
    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 4

    .prologue
    .line 331
    iget-wide v0, p0, Lcom/google/common/cache/LocalCache;->n:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/google/common/cache/LocalCache;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/common/cache/LocalCache;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 4178
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->x:Ljava/util/Set;

    .line 4179
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lecd;

    invoke-direct {v0, p0, p0}, Lecd;-><init>(Lcom/google/common/cache/LocalCache;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/common/cache/LocalCache;->x:Ljava/util/Set;

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/google/common/cache/LocalCache;->c()Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->h:Lecm;

    sget-object v1, Lecm;->a:Lecm;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3855
    if-nez p1, :cond_0

    .line 3856
    const/4 v0, 0x0

    .line 3859
    :goto_0
    return-object v0

    .line 3858
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/common/cache/LocalCache;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3859
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lecj;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->i:Lecm;

    sget-object v1, Lecm;->a:Lecm;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3816
    iget-object v6, p0, Lcom/google/common/cache/LocalCache;->e:[Lecj;

    move v0, v1

    move-wide v2, v4

    .line 3817
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 3818
    aget-object v7, v6, v0

    iget v7, v7, Lecj;->b:I

    if-eqz v7, :cond_1

    .line 3835
    :cond_0
    :goto_1
    return v1

    .line 3821
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lecj;->d:I

    int-to-long v7, v7

    add-long/2addr v2, v7

    .line 3817
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3824
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 3825
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 3826
    aget-object v7, v6, v0

    iget v7, v7, Lecj;->b:I

    if-nez v7, :cond_0

    .line 3829
    aget-object v7, v6, v0

    iget v7, v7, Lecj;->d:I

    int-to-long v7, v7

    sub-long/2addr v2, v7

    .line 3825
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3831
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 3835
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public k()V
    .locals 4

    .prologue
    .line 1909
    :goto_0
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledn;

    if-eqz v0, :cond_0

    .line 1911
    :try_start_0
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->p:La;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1912
    :catch_0
    move-exception v0

    .line 1913
    sget-object v1, Lcom/google/common/cache/LocalCache;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1916
    :cond_0
    return-void
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 4159
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->v:Ljava/util/Set;

    .line 4160
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lecg;

    invoke-direct {v0, p0, p0}, Lecg;-><init>(Lcom/google/common/cache/LocalCache;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/common/cache/LocalCache;->v:Ljava/util/Set;

    goto :goto_0
.end method

.method l()J
    .locals 6

    .prologue
    .line 3839
    iget-object v3, p0, Lcom/google/common/cache/LocalCache;->e:[Lecj;

    .line 3840
    const-wide/16 v1, 0x0

    .line 3841
    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 3842
    aget-object v4, v3, v0

    iget v4, v4, Lecj;->b:I

    int-to-long v4, v4

    add-long/2addr v1, v4

    .line 3841
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3844
    :cond_0
    return-wide v1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4086
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4087
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4088
    invoke-virtual {p0, p1}, Lcom/google/common/cache/LocalCache;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4089
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lecj;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4101
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 4102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/common/cache/LocalCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 4104
    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4093
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4094
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4095
    invoke-virtual {p0, p1}, Lcom/google/common/cache/LocalCache;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4096
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lecj;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 4108
    if-nez p1, :cond_0

    .line 4109
    const/4 v0, 0x0

    .line 4112
    :goto_0
    return-object v0

    .line 4111
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/common/cache/LocalCache;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4112
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lecj;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 4116
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 4117
    :cond_0
    const/4 v0, 0x0

    .line 4120
    :goto_0
    return v0

    .line 4119
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/common/cache/LocalCache;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4120
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lecj;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4134
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4135
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4136
    invoke-virtual {p0, p1}, Lcom/google/common/cache/LocalCache;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4137
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lecj;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 4124
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4125
    invoke-static {p3}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4126
    if-nez p2, :cond_0

    .line 4127
    const/4 v0, 0x0

    .line 4130
    :goto_0
    return v0

    .line 4129
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/common/cache/LocalCache;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4130
    invoke-virtual {p0, v0}, Lcom/google/common/cache/LocalCache;->a(I)Lecj;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lecj;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 3849
    invoke-virtual {p0}, Lcom/google/common/cache/LocalCache;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Lf;->c(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 4168
    iget-object v0, p0, Lcom/google/common/cache/LocalCache;->w:Ljava/util/Collection;

    .line 4169
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lecx;

    invoke-direct {v0, p0, p0}, Lecx;-><init>(Lcom/google/common/cache/LocalCache;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/common/cache/LocalCache;->w:Ljava/util/Collection;

    goto :goto_0
.end method
