.class public Lcom/google/common/collect/MapMakerInternalMap;
.super Ljava/util/AbstractMap;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/io/Serializable;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field static final p:Legn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Legn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x5L

.field private static final u:Ljava/util/logging/Logger;


# instance fields
.field final transient a:I

.field final transient b:I

.field public final transient c:[Lega;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lega",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final d:I

.field public final e:Leba;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Leba",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Leba;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Leba",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final g:Legd;

.field public final h:Legd;

.field final i:I

.field public final j:J

.field public final k:J

.field public final l:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lefa",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final m:Lft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lft",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final transient n:Leff;

.field public final o:Lebm;

.field transient r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field transient s:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field transient t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    const-class v0, Lcom/google/common/collect/MapMakerInternalMap;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/common/collect/MapMakerInternalMap;->u:Ljava/util/logging/Logger;

    .line 586
    new-instance v0, Lefb;

    invoke-direct {v0}, Lefb;-><init>()V

    sput-object v0, Lcom/google/common/collect/MapMakerInternalMap;->p:Legn;

    .line 844
    new-instance v0, Lefc;

    invoke-direct {v0}, Lefc;-><init>()V

    sput-object v0, Lcom/google/common/collect/MapMakerInternalMap;->q:Ljava/util/Queue;

    return-void
.end method

.method public constructor <init>(Lees;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 195
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 196
    invoke-virtual {p1}, Lees;->d()I

    move-result v0

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->d:I

    .line 198
    invoke-virtual {p1}, Lees;->e()Legd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->g:Legd;

    .line 199
    invoke-virtual {p1}, Lees;->f()Legd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->h:Legd;

    .line 201
    invoke-virtual {p1}, Lees;->b()Leba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->e:Leba;

    .line 202
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->h:Legd;

    invoke-virtual {v0}, Legd;->a()Leba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->f:Leba;

    .line 204
    iget v0, p1, Lees;->e:I

    iput v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->i:I

    .line 205
    invoke-virtual {p1}, Lees;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->j:J

    .line 206
    invoke-virtual {p1}, Lees;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->k:J

    .line 208
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->g:Legd;

    invoke-virtual {p0}, Lcom/google/common/collect/MapMakerInternalMap;->b()Z

    move-result v1

    invoke-virtual {p0}, Lcom/google/common/collect/MapMakerInternalMap;->a()Z

    move-result v3

    invoke-static {v0, v1, v3}, Leff;->a(Legd;ZZ)Leff;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->n:Leff;

    .line 209
    invoke-virtual {p1}, Lees;->i()Lebm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->o:Lebm;

    .line 211
    invoke-virtual {p1}, Lees;->a()Lft;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->m:Lft;

    .line 212
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->m:Lft;

    sget-object v1, Leea;->a:Leea;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/common/collect/MapMakerInternalMap;->q:Ljava/util/Queue;

    :goto_0
    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->l:Ljava/util/Queue;

    .line 216
    invoke-virtual {p1}, Lees;->c()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 217
    invoke-virtual {p0}, Lcom/google/common/collect/MapMakerInternalMap;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    iget v1, p0, Lcom/google/common/collect/MapMakerInternalMap;->i:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    move v1, v2

    move v3, v4

    .line 226
    :goto_1
    iget v5, p0, Lcom/google/common/collect/MapMakerInternalMap;->d:I

    if-ge v1, v5, :cond_3

    invoke-virtual {p0}, Lcom/google/common/collect/MapMakerInternalMap;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    shl-int/lit8 v5, v1, 0x1

    iget v6, p0, Lcom/google/common/collect/MapMakerInternalMap;->i:I

    if-gt v5, v6, :cond_3

    .line 227
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 228
    shl-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 212
    :cond_2
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_0

    .line 230
    :cond_3
    rsub-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/common/collect/MapMakerInternalMap;->b:I

    .line 231
    add-int/lit8 v3, v1, -0x1

    iput v3, p0, Lcom/google/common/collect/MapMakerInternalMap;->a:I

    .line 233
    new-array v3, v1, [Lega;

    iput-object v3, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    .line 235
    div-int v3, v0, v1

    .line 236
    mul-int v5, v3, v1

    if-ge v5, v0, :cond_8

    .line 237
    add-int/lit8 v0, v3, 0x1

    .line 241
    :goto_2
    if-ge v2, v0, :cond_4

    .line 242
    shl-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 245
    :cond_4
    invoke-virtual {p0}, Lcom/google/common/collect/MapMakerInternalMap;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 247
    iget v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->i:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 248
    iget v3, p0, Lcom/google/common/collect/MapMakerInternalMap;->i:I

    rem-int v1, v3, v1

    .line 249
    :goto_3
    iget-object v3, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    array-length v3, v3

    if-ge v4, v3, :cond_7

    .line 250
    if-ne v4, v1, :cond_5

    .line 251
    add-int/lit8 v0, v0, -0x1

    .line 253
    :cond_5
    iget-object v3, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    invoke-virtual {p0, v2, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(II)Lega;

    move-result-object v5

    aput-object v5, v3, v4

    .line 249
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 256
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    array-length v0, v0

    if-ge v4, v0, :cond_7

    .line 257
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    const/4 v1, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/common/collect/MapMakerInternalMap;->a(II)Lega;

    move-result-object v1

    aput-object v1, v0, v4

    .line 256
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 260
    :cond_7
    return-void

    :cond_8
    move v0, v3

    goto :goto_2
.end method

.method public static a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1837
    invoke-interface {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setNextExpirable(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 1838
    invoke-interface {p1, p0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setPreviousExpirable(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 1839
    return-void
.end method

.method public static b(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1869
    invoke-interface {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setNextEvictable(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 1870
    invoke-interface {p1, p0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setPreviousEvictable(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 1871
    return-void
.end method

.method public static d(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1843
    sget-object v0, Lefz;->a:Lefz;

    .line 1844
    invoke-interface {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setNextExpirable(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 1845
    invoke-interface {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setPreviousExpirable(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 1846
    return-void
.end method

.method public static e(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1875
    sget-object v0, Lefz;->a:Lefz;

    .line 1876
    invoke-interface {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setNextEvictable(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 1877
    invoke-interface {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setPreviousEvictable(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 1878
    return-void
.end method

.method public static g()Legn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Legn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 618
    sget-object v0, Lcom/google/common/collect/MapMakerInternalMap;->p:Legn;

    return-object v0
.end method

.method public static h()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 841
    sget-object v0, Lefz;->a:Lefz;

    return-object v0
.end method

.method public static i()Ljava/util/Queue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 875
    sget-object v0, Lcom/google/common/collect/MapMakerInternalMap;->q:Ljava/util/Queue;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 1759
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->e:Leba;

    invoke-virtual {v0, p1}, Leba;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1760
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method a(I)Lega;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lega",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1791
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    iget v1, p0, Lcom/google/common/collect/MapMakerInternalMap;->b:I

    ushr-int v1, p1, v1

    iget v2, p0, Lcom/google/common/collect/MapMakerInternalMap;->a:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method a(II)Lega;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lega",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1795
    new-instance v0, Lega;

    invoke-direct {v0, p0, p1, p2}, Lega;-><init>(Lcom/google/common/collect/MapMakerInternalMap;II)V

    return-object v0
.end method

.method public a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1770
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getHash()I

    move-result v0

    .line 1771
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lega;->a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;I)Z

    .line 1772
    return-void
.end method

.method public a(Legn;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Legn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1764
    invoke-interface {p1}, Legn;->a()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;

    move-result-object v0

    .line 1765
    invoke-interface {v0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getHash()I

    move-result v1

    .line 1766
    invoke-virtual {p0, v1}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lega;->a(Ljava/lang/Object;ILegn;)Z

    .line 1767
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 263
    iget v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->i:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;J)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;J)Z"
        }
    .end annotation

    .prologue
    .line 1832
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getExpirationTime()J

    move-result-wide v0

    sub-long v0, p2, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1804
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1815
    :cond_0
    :goto_0
    return-object v0

    .line 1807
    :cond_1
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getValueReference()Legn;

    move-result-object v1

    invoke-interface {v1}, Legn;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1808
    if-eqz v1, :cond_0

    .line 1812
    invoke-virtual {p0}, Lcom/google/common/collect/MapMakerInternalMap;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->c(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move-object v0, v1

    .line 1815
    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/google/common/collect/MapMakerInternalMap;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/common/collect/MapMakerInternalMap;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 4

    .prologue
    .line 271
    iget-wide v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 1824
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->o:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;J)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .locals 4

    .prologue
    .line 3453
    iget-object v1, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 3454
    invoke-virtual {v3}, Lega;->a()V

    .line 3453
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3456
    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3349
    if-nez p1, :cond_0

    .line 3350
    const/4 v0, 0x0

    .line 3353
    :goto_0
    return v0

    .line 3352
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3353
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lega;->b(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 14

    .prologue
    .line 3358
    if-nez p1, :cond_0

    .line 3359
    const/4 v0, 0x0

    .line 3392
    :goto_0
    return v0

    .line 3367
    :cond_0
    iget-object v8, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    .line 3368
    const-wide/16 v3, -0x1

    .line 3369
    const/4 v0, 0x0

    move v5, v0

    move-wide v6, v3

    :goto_1
    const/4 v0, 0x3

    if-ge v5, v0, :cond_5

    .line 3370
    const-wide/16 v1, 0x0

    .line 3371
    array-length v9, v8

    const/4 v0, 0x0

    move-wide v3, v1

    move v2, v0

    :goto_2
    if-ge v2, v9, :cond_4

    aget-object v10, v8, v2

    .line 3374
    iget v0, v10, Lega;->b:I

    .line 3376
    iget-object v11, v10, Lega;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3377
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3378
    invoke-virtual {v11, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;

    :goto_4
    if-eqz v0, :cond_2

    .line 3379
    invoke-virtual {v10, v0}, Lega;->a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Ljava/lang/Object;

    move-result-object v12

    .line 3380
    if-eqz v12, :cond_1

    iget-object v13, p0, Lcom/google/common/collect/MapMakerInternalMap;->f:Leba;

    invoke-virtual {v13, p1, v12}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 3381
    const/4 v0, 0x1

    goto :goto_0

    .line 3378
    :cond_1
    invoke-interface {v0}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getNext()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;

    move-result-object v0

    goto :goto_4

    .line 3377
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 3385
    :cond_3
    iget v0, v10, Lega;->c:I

    int-to-long v0, v0

    add-long/2addr v3, v0

    .line 3371
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 3387
    :cond_4
    cmp-long v0, v3, v6

    if-eqz v0, :cond_5

    .line 3369
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move-wide v6, v3

    goto :goto_1

    .line 3392
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 4

    .prologue
    .line 275
    iget-wide v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->g:Legd;

    sget-object v1, Legd;->a:Legd;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3478
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->t:Ljava/util/Set;

    .line 3479
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lefp;

    invoke-direct {v0, p0}, Lefp;-><init>(Lcom/google/common/collect/MapMakerInternalMap;)V

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->t:Ljava/util/Set;

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->h:Legd;

    sget-object v1, Legd;->a:Legd;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3328
    if-nez p1, :cond_0

    .line 3329
    const/4 v0, 0x0

    .line 3332
    :goto_0
    return-object v0

    .line 3331
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3332
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lega;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3294
    iget-object v6, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    move v0, v1

    move-wide v2, v4

    .line 3295
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 3296
    aget-object v7, v6, v0

    iget v7, v7, Lega;->b:I

    if-eqz v7, :cond_1

    .line 3313
    :cond_0
    :goto_1
    return v1

    .line 3299
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lega;->c:I

    int-to-long v7, v7

    add-long/2addr v2, v7

    .line 3295
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3302
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 3303
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 3304
    aget-object v7, v6, v0

    iget v7, v7, Lega;->b:I

    if-nez v7, :cond_0

    .line 3307
    aget-object v7, v6, v0

    iget v7, v7, Lega;->c:I

    int-to-long v7, v7

    sub-long/2addr v2, v7

    .line 3303
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3309
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 3313
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public j()V
    .locals 4

    .prologue
    .line 1857
    :goto_0
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefa;

    if-eqz v0, :cond_0

    .line 1859
    :try_start_0
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->m:Lft;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1860
    :catch_0
    move-exception v0

    .line 1861
    sget-object v1, Lcom/google/common/collect/MapMakerInternalMap;->u:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1864
    :cond_0
    return-void
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 3462
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->r:Ljava/util/Set;

    .line 3463
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lefy;

    invoke-direct {v0, p0}, Lefy;-><init>(Lcom/google/common/collect/MapMakerInternalMap;)V

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->r:Ljava/util/Set;

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3397
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3398
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3399
    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3400
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lega;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3412
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3413
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/common/collect/MapMakerInternalMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3415
    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3404
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3405
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3406
    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3407
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lega;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3419
    if-nez p1, :cond_0

    .line 3420
    const/4 v0, 0x0

    .line 3423
    :goto_0
    return-object v0

    .line 3422
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3423
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lega;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3427
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 3428
    :cond_0
    const/4 v0, 0x0

    .line 3431
    :goto_0
    return v0

    .line 3430
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3431
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lega;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3445
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3446
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3447
    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3448
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lega;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 3435
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3436
    invoke-static {p3}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3437
    if-nez p2, :cond_0

    .line 3438
    const/4 v0, 0x0

    .line 3441
    :goto_0
    return v0

    .line 3440
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3441
    invoke-virtual {p0, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(I)Lega;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lega;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 6

    .prologue
    .line 3318
    iget-object v3, p0, Lcom/google/common/collect/MapMakerInternalMap;->c:[Lega;

    .line 3319
    const-wide/16 v1, 0x0

    .line 3320
    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 3321
    aget-object v4, v3, v0

    iget v4, v4, Lega;->b:I

    int-to-long v4, v4

    add-long/2addr v1, v4

    .line 3320
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3323
    :cond_0
    invoke-static {v1, v2}, Lf;->c(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 3470
    iget-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->s:Ljava/util/Collection;

    .line 3471
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lego;

    invoke-direct {v0, p0}, Lego;-><init>(Lcom/google/common/collect/MapMakerInternalMap;)V

    iput-object v0, p0, Lcom/google/common/collect/MapMakerInternalMap;->s:Ljava/util/Collection;

    goto :goto_0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 3778
    new-instance v0, Legb;

    iget-object v1, p0, Lcom/google/common/collect/MapMakerInternalMap;->g:Legd;

    iget-object v2, p0, Lcom/google/common/collect/MapMakerInternalMap;->h:Legd;

    iget-object v3, p0, Lcom/google/common/collect/MapMakerInternalMap;->e:Leba;

    iget-object v4, p0, Lcom/google/common/collect/MapMakerInternalMap;->f:Leba;

    iget-wide v5, p0, Lcom/google/common/collect/MapMakerInternalMap;->k:J

    iget-wide v7, p0, Lcom/google/common/collect/MapMakerInternalMap;->j:J

    iget v9, p0, Lcom/google/common/collect/MapMakerInternalMap;->i:I

    iget v10, p0, Lcom/google/common/collect/MapMakerInternalMap;->d:I

    iget-object v11, p0, Lcom/google/common/collect/MapMakerInternalMap;->m:Lft;

    move-object v12, p0

    invoke-direct/range {v0 .. v12}, Legb;-><init>(Legd;Legd;Leba;Leba;JJIILft;Ljava/util/concurrent/ConcurrentMap;)V

    return-object v0
.end method
