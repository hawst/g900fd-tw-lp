.class public final Ldni;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldni;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Ldnj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2691
    const/4 v0, 0x0

    new-array v0, v0, [Ldni;

    sput-object v0, Ldni;->a:[Ldni;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2692
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2697
    sget-object v0, Ldnj;->a:[Ldnj;

    iput-object v0, p0, Ldni;->c:[Ldnj;

    .line 2692
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2719
    iget-object v0, p0, Ldni;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2720
    const/4 v0, 0x1

    iget-object v2, p0, Ldni;->b:Ljava/lang/String;

    .line 2721
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2723
    :goto_0
    iget-object v2, p0, Ldni;->c:[Ldnj;

    if-eqz v2, :cond_1

    .line 2724
    iget-object v2, p0, Ldni;->c:[Ldnj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2725
    if-eqz v4, :cond_0

    .line 2726
    const/4 v5, 0x2

    .line 2727
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2724
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2731
    :cond_1
    iget-object v1, p0, Ldni;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2732
    iput v0, p0, Ldni;->cachedSize:I

    .line 2733
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2688
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldni;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldni;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldni;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldni;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldni;->c:[Ldnj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldnj;

    iget-object v3, p0, Ldni;->c:[Ldnj;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldni;->c:[Ldnj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldni;->c:[Ldnj;

    :goto_2
    iget-object v2, p0, Ldni;->c:[Ldnj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldni;->c:[Ldnj;

    new-instance v3, Ldnj;

    invoke-direct {v3}, Ldnj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldni;->c:[Ldnj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldni;->c:[Ldnj;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldni;->c:[Ldnj;

    new-instance v3, Ldnj;

    invoke-direct {v3}, Ldnj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldni;->c:[Ldnj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 2702
    iget-object v0, p0, Ldni;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2703
    const/4 v0, 0x1

    iget-object v1, p0, Ldni;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2705
    :cond_0
    iget-object v0, p0, Ldni;->c:[Ldnj;

    if-eqz v0, :cond_2

    .line 2706
    iget-object v1, p0, Ldni;->c:[Ldnj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2707
    if-eqz v3, :cond_1

    .line 2708
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2706
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2712
    :cond_2
    iget-object v0, p0, Ldni;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2714
    return-void
.end method
