.class final Lnt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# instance fields
.field final synthetic a:Lno;


# direct methods
.method private constructor <init>(Lno;)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lnt;->a:Lno;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lno;B)V
    .locals 0

    .prologue
    .line 531
    invoke-direct {p0, p1}, Lnt;-><init>(Lno;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 567
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->e(Lno;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 568
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-virtual {v0}, Lno;->a()Z

    .line 569
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->a(Lno;)Lns;

    move-result-object v0

    invoke-virtual {v0}, Lns;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 570
    iget-object v1, p0, Lnt;->a:Lno;

    invoke-static {v1}, Lno;->a(Lno;)Lns;

    move-result-object v1

    invoke-virtual {v1}, Lns;->e()Lni;

    move-result-object v1

    invoke-virtual {v1, v0}, Lni;->a(Landroid/content/pm/ResolveInfo;)I

    move-result v0

    .line 571
    iget-object v1, p0, Lnt;->a:Lno;

    invoke-static {v1}, Lno;->a(Lno;)Lns;

    move-result-object v1

    invoke-virtual {v1}, Lns;->e()Lni;

    move-result-object v1

    invoke-virtual {v1, v0}, Lni;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 572
    if-eqz v0, :cond_0

    .line 573
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 574
    iget-object v1, p0, Lnt;->a:Lno;

    invoke-virtual {v1}, Lno;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->f(Lno;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 577
    iget-object v0, p0, Lnt;->a:Lno;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lno;->a(Lno;Z)Z

    .line 578
    iget-object v0, p0, Lnt;->a:Lno;

    iget-object v1, p0, Lnt;->a:Lno;

    invoke-static {v1}, Lno;->g(Lno;)I

    move-result v1

    invoke-static {v0, v1}, Lno;->a(Lno;I)V

    goto :goto_0

    .line 580
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public onDismiss()V
    .locals 2

    .prologue
    .line 600
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->h(Lno;)Landroid/widget/PopupWindow$OnDismissListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->h(Lno;)Landroid/widget/PopupWindow$OnDismissListener;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    .line 601
    :cond_0
    iget-object v0, p0, Lnt;->a:Lno;

    iget-object v0, v0, Lno;->a:Lfr;

    if-eqz v0, :cond_1

    .line 602
    iget-object v0, p0, Lnt;->a:Lno;

    iget-object v0, v0, Lno;->a:Lfr;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfr;->a(Z)V

    .line 604
    :cond_1
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 536
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lns;

    .line 537
    invoke-virtual {v0, p3}, Lns;->getItemViewType(I)I

    move-result v0

    .line 538
    packed-switch v0, :pswitch_data_0

    .line 561
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 540
    :pswitch_0
    iget-object v0, p0, Lnt;->a:Lno;

    const v1, 0x7fffffff

    invoke-static {v0, v1}, Lno;->a(Lno;I)V

    .line 563
    :cond_0
    :goto_0
    return-void

    .line 543
    :pswitch_1
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-virtual {v0}, Lno;->a()Z

    .line 544
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->d(Lno;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    if-lez p3, :cond_0

    .line 547
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->a(Lno;)Lns;

    move-result-object v0

    invoke-virtual {v0}, Lns;->e()Lni;

    move-result-object v0

    invoke-virtual {v0, p3}, Lni;->c(I)V

    goto :goto_0

    .line 552
    :cond_1
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->a(Lno;)Lns;

    move-result-object v0

    invoke-virtual {v0}, Lns;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553
    :goto_1
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->a(Lno;)Lns;

    move-result-object v0

    invoke-virtual {v0}, Lns;->e()Lni;

    move-result-object v0

    invoke-virtual {v0, p3}, Lni;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 554
    if-eqz v0, :cond_0

    .line 555
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 556
    iget-object v1, p0, Lnt;->a:Lno;

    invoke-virtual {v1}, Lno;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 552
    :cond_2
    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    .line 538
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 587
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->e(Lno;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 588
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0}, Lno;->a(Lno;)Lns;

    move-result-object v0

    invoke-virtual {v0}, Lns;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 589
    iget-object v0, p0, Lnt;->a:Lno;

    invoke-static {v0, v2}, Lno;->a(Lno;Z)Z

    .line 590
    iget-object v0, p0, Lnt;->a:Lno;

    iget-object v1, p0, Lnt;->a:Lno;

    invoke-static {v1}, Lno;->g(Lno;)I

    move-result v1

    invoke-static {v0, v1}, Lno;->a(Lno;I)V

    .line 595
    :cond_0
    return v2

    .line 593
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method
