.class public Lbfv;
.super Lbed;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:J


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 1920
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 1921
    iput-wide p2, p0, Lbfv;->a:J

    .line 1922
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    .line 1926
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 1927
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateWatermark build protobuf conversationID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbfv;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " watermark="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbfv;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1931
    :cond_0
    new-instance v0, Ldxm;

    invoke-direct {v0}, Ldxm;-><init>()V

    .line 1933
    iget-object v1, p0, Lbfv;->c:Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v0, Ldxm;->c:Ldqf;

    .line 1934
    iget-wide v1, p0, Lbfv;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldxm;->d:Ljava/lang/Long;

    .line 1935
    invoke-virtual {p0, p1, p2}, Lbfv;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldxm;->b:Ldvm;

    .line 1937
    return-object v0
.end method

.method public a(Lbea;)Z
    .locals 2

    .prologue
    .line 1952
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1953
    check-cast p1, Lbfv;

    .line 1954
    iget-object v0, p1, Lbfv;->c:Ljava/lang/String;

    iget-object v1, p0, Lbfv;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1942
    const-string v0, "conversations/updatewatermark"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1947
    const-string v0, "background_queue"

    return-object v0
.end method
