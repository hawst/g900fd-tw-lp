.class public final Ldsv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsv;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:[B

.field public d:[Ldsu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19385
    const/4 v0, 0x0

    new-array v0, v0, [Ldsv;

    sput-object v0, Ldsv;->a:[Ldsv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19386
    invoke-direct {p0}, Lepn;-><init>()V

    .line 19393
    sget-object v0, Ldsu;->a:[Ldsu;

    iput-object v0, p0, Ldsv;->d:[Ldsu;

    .line 19386
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 19418
    iget-object v0, p0, Ldsv;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 19419
    const/4 v0, 0x1

    iget-object v2, p0, Ldsv;->b:Ljava/lang/Boolean;

    .line 19420
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 19422
    :goto_0
    iget-object v2, p0, Ldsv;->c:[B

    if-eqz v2, :cond_0

    .line 19423
    const/4 v2, 0x2

    iget-object v3, p0, Ldsv;->c:[B

    .line 19424
    invoke-static {v2, v3}, Lepl;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 19426
    :cond_0
    iget-object v2, p0, Ldsv;->d:[Ldsu;

    if-eqz v2, :cond_2

    .line 19427
    iget-object v2, p0, Ldsv;->d:[Ldsu;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 19428
    if-eqz v4, :cond_1

    .line 19429
    const/4 v5, 0x3

    .line 19430
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 19427
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 19434
    :cond_2
    iget-object v1, p0, Ldsv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19435
    iput v0, p0, Ldsv;->cachedSize:I

    .line 19436
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 19382
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldsv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldsv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldsv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsv;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldsv;->c:[B

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsv;->d:[Ldsu;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldsu;

    iget-object v3, p0, Ldsv;->d:[Ldsu;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldsv;->d:[Ldsu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldsv;->d:[Ldsu;

    :goto_2
    iget-object v2, p0, Ldsv;->d:[Ldsu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldsv;->d:[Ldsu;

    new-instance v3, Ldsu;

    invoke-direct {v3}, Ldsu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsv;->d:[Ldsu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldsv;->d:[Ldsu;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldsv;->d:[Ldsu;

    new-instance v3, Ldsu;

    invoke-direct {v3}, Ldsu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsv;->d:[Ldsu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 19398
    iget-object v0, p0, Ldsv;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 19399
    const/4 v0, 0x1

    iget-object v1, p0, Ldsv;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 19401
    :cond_0
    iget-object v0, p0, Ldsv;->c:[B

    if-eqz v0, :cond_1

    .line 19402
    const/4 v0, 0x2

    iget-object v1, p0, Ldsv;->c:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 19404
    :cond_1
    iget-object v0, p0, Ldsv;->d:[Ldsu;

    if-eqz v0, :cond_3

    .line 19405
    iget-object v1, p0, Ldsv;->d:[Ldsu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 19406
    if-eqz v3, :cond_2

    .line 19407
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 19405
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 19411
    :cond_3
    iget-object v0, p0, Ldsv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 19413
    return-void
.end method
