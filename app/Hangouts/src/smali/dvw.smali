.class public final Ldvw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvw;


# instance fields
.field public b:Ldvm;

.field public c:Ldug;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22060
    const/4 v0, 0x0

    new-array v0, v0, [Ldvw;

    sput-object v0, Ldvw;->a:[Ldvw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22061
    invoke-direct {p0}, Lepn;-><init>()V

    .line 22064
    iput-object v0, p0, Ldvw;->b:Ldvm;

    .line 22067
    iput-object v0, p0, Ldvw;->c:Ldug;

    .line 22061
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 22084
    const/4 v0, 0x0

    .line 22085
    iget-object v1, p0, Ldvw;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 22086
    const/4 v0, 0x1

    iget-object v1, p0, Ldvw;->b:Ldvm;

    .line 22087
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22089
    :cond_0
    iget-object v1, p0, Ldvw;->c:Ldug;

    if-eqz v1, :cond_1

    .line 22090
    const/4 v1, 0x2

    iget-object v2, p0, Ldvw;->c:Ldug;

    .line 22091
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22093
    :cond_1
    iget-object v1, p0, Ldvw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22094
    iput v0, p0, Ldvw;->cachedSize:I

    .line 22095
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 22057
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvw;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldvw;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldvw;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldvw;->c:Ldug;

    if-nez v0, :cond_3

    new-instance v0, Ldug;

    invoke-direct {v0}, Ldug;-><init>()V

    iput-object v0, p0, Ldvw;->c:Ldug;

    :cond_3
    iget-object v0, p0, Ldvw;->c:Ldug;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 22072
    iget-object v0, p0, Ldvw;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 22073
    const/4 v0, 0x1

    iget-object v1, p0, Ldvw;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22075
    :cond_0
    iget-object v0, p0, Ldvw;->c:Ldug;

    if-eqz v0, :cond_1

    .line 22076
    const/4 v0, 0x2

    iget-object v1, p0, Ldvw;->c:Ldug;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22078
    :cond_1
    iget-object v0, p0, Ldvw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 22080
    return-void
.end method
