.class public final Leab;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leab;


# instance fields
.field public b:Ldzy;

.field public c:Leaf;

.field public d:Ldzs;

.field public e:Leaa;

.field public f:Lesb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    new-array v0, v0, [Leab;

    sput-object v0, Leab;->a:[Leab;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Lepn;-><init>()V

    .line 73
    iput-object v0, p0, Leab;->b:Ldzy;

    .line 76
    iput-object v0, p0, Leab;->c:Leaf;

    .line 79
    iput-object v0, p0, Leab;->d:Ldzs;

    .line 82
    iput-object v0, p0, Leab;->e:Leaa;

    .line 85
    iput-object v0, p0, Leab;->f:Lesb;

    .line 70
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    iget-object v1, p0, Leab;->b:Ldzy;

    if-eqz v1, :cond_0

    .line 113
    const/4 v0, 0x1

    iget-object v1, p0, Leab;->b:Ldzy;

    .line 114
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 116
    :cond_0
    iget-object v1, p0, Leab;->c:Leaf;

    if-eqz v1, :cond_1

    .line 117
    const/4 v1, 0x2

    iget-object v2, p0, Leab;->c:Leaf;

    .line 118
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_1
    iget-object v1, p0, Leab;->d:Ldzs;

    if-eqz v1, :cond_2

    .line 121
    const/4 v1, 0x3

    iget-object v2, p0, Leab;->d:Ldzs;

    .line 122
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_2
    iget-object v1, p0, Leab;->e:Leaa;

    if-eqz v1, :cond_3

    .line 125
    const/4 v1, 0x4

    iget-object v2, p0, Leab;->e:Leaa;

    .line 126
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_3
    iget-object v1, p0, Leab;->f:Lesb;

    if-eqz v1, :cond_4

    .line 129
    const/4 v1, 0x5

    iget-object v2, p0, Leab;->f:Lesb;

    .line 130
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_4
    iget-object v1, p0, Leab;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    iput v0, p0, Leab;->cachedSize:I

    .line 134
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leab;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leab;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leab;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leab;->b:Ldzy;

    if-nez v0, :cond_2

    new-instance v0, Ldzy;

    invoke-direct {v0}, Ldzy;-><init>()V

    iput-object v0, p0, Leab;->b:Ldzy;

    :cond_2
    iget-object v0, p0, Leab;->b:Ldzy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leab;->c:Leaf;

    if-nez v0, :cond_3

    new-instance v0, Leaf;

    invoke-direct {v0}, Leaf;-><init>()V

    iput-object v0, p0, Leab;->c:Leaf;

    :cond_3
    iget-object v0, p0, Leab;->c:Leaf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leab;->d:Ldzs;

    if-nez v0, :cond_4

    new-instance v0, Ldzs;

    invoke-direct {v0}, Ldzs;-><init>()V

    iput-object v0, p0, Leab;->d:Ldzs;

    :cond_4
    iget-object v0, p0, Leab;->d:Ldzs;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leab;->e:Leaa;

    if-nez v0, :cond_5

    new-instance v0, Leaa;

    invoke-direct {v0}, Leaa;-><init>()V

    iput-object v0, p0, Leab;->e:Leaa;

    :cond_5
    iget-object v0, p0, Leab;->e:Leaa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Leab;->f:Lesb;

    if-nez v0, :cond_6

    new-instance v0, Lesb;

    invoke-direct {v0}, Lesb;-><init>()V

    iput-object v0, p0, Leab;->f:Lesb;

    :cond_6
    iget-object v0, p0, Leab;->f:Lesb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Leab;->b:Ldzy;

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x1

    iget-object v1, p0, Leab;->b:Ldzy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 93
    :cond_0
    iget-object v0, p0, Leab;->c:Leaf;

    if-eqz v0, :cond_1

    .line 94
    const/4 v0, 0x2

    iget-object v1, p0, Leab;->c:Leaf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 96
    :cond_1
    iget-object v0, p0, Leab;->d:Ldzs;

    if-eqz v0, :cond_2

    .line 97
    const/4 v0, 0x3

    iget-object v1, p0, Leab;->d:Ldzs;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 99
    :cond_2
    iget-object v0, p0, Leab;->e:Leaa;

    if-eqz v0, :cond_3

    .line 100
    const/4 v0, 0x4

    iget-object v1, p0, Leab;->e:Leaa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 102
    :cond_3
    iget-object v0, p0, Leab;->f:Lesb;

    if-eqz v0, :cond_4

    .line 103
    const/4 v0, 0x5

    iget-object v1, p0, Leab;->f:Lesb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 105
    :cond_4
    iget-object v0, p0, Leab;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 107
    return-void
.end method
