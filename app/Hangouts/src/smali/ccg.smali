.class Lccg;
.super Landroid/text/style/ImageSpan;
.source "PG"


# instance fields
.field final synthetic a:Lccd;

.field private b:Landroid/graphics/Bitmap;

.field private c:Z

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Lccd;Landroid/graphics/drawable/Drawable;Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 78
    iput-object p1, p0, Lccg;->a:Lccd;

    .line 79
    const/4 v0, 0x1

    invoke-direct {p0, p2, p3, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lccg;->c:Z

    .line 80
    iput p4, p0, Lccg;->d:I

    .line 81
    iput p5, p0, Lccg;->e:I

    .line 82
    return-void
.end method

.method static synthetic a(Lccg;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lccg;->d:I

    return v0
.end method

.method static synthetic b(Lccg;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lccg;->e:I

    return v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 90
    monitor-enter p0

    .line 91
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lccg;->c:Z

    .line 92
    iget-object v0, p0, Lccg;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lccg;->a:Lccd;

    invoke-static {v0}, Lccd;->a(Lccd;)Lbxu;

    move-result-object v0

    iget-object v1, p0, Lccg;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lbxu;->a(Landroid/graphics/Bitmap;)V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lccg;->b:Landroid/graphics/Bitmap;

    .line 96
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 105
    monitor-enter p0

    .line 106
    :try_start_0
    iget-boolean v0, p0, Lccg;->c:Z

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lccg;->a:Lccd;

    invoke-static {v0}, Lccd;->a(Lccd;)Lbxu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbxu;->a(Landroid/graphics/Bitmap;)V

    .line 112
    :goto_0
    monitor-exit p0

    return-void

    .line 110
    :cond_0
    iput-object p1, p0, Lccg;->b:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 120
    iget-object v0, p0, Lccg;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 121
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cZ:I

    .line 122
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 124
    invoke-static {}, Lccd;->b()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lccg;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lccg;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 125
    invoke-static {}, Lccd;->c()Landroid/graphics/Rect;

    move-result-object v1

    float-to-int v2, p5

    add-int/2addr v2, v0

    iget v3, p0, Lccg;->d:I

    int-to-float v3, v3

    add-float/2addr v3, p5

    int-to-float v0, v0

    add-float/2addr v0, v3

    float-to-int v0, v0

    iget v3, p0, Lccg;->e:I

    add-int/2addr v3, p6

    invoke-virtual {v1, v2, p6, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 127
    iget-object v0, p0, Lccg;->b:Landroid/graphics/Bitmap;

    invoke-static {}, Lccd;->b()Landroid/graphics/Rect;

    move-result-object v1

    invoke-static {}, Lccd;->c()Landroid/graphics/Rect;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 129
    :cond_0
    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 5

    .prologue
    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    .line 144
    invoke-super/range {p0 .. p5}, Landroid/text/style/ImageSpan;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    move-result v0

    .line 145
    if-eqz p5, :cond_0

    .line 146
    iget-object v1, p0, Lccg;->a:Lccd;

    invoke-static {v1}, Lccd;->b(Lccd;)Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    .line 150
    iget-object v1, p0, Lccg;->a:Lccd;

    invoke-static {v1}, Lccd;->b(Lccd;)Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    float-to-double v1, v1

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 151
    iget-object v1, p0, Lccg;->a:Lccd;

    invoke-static {v1}, Lccd;->b(Lccd;)Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->descent:F

    float-to-double v1, v1

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 152
    iget-object v1, p0, Lccg;->a:Lccd;

    invoke-static {v1}, Lccd;->b(Lccd;)Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->top:F

    float-to-double v1, v1

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 153
    iget-object v1, p0, Lccg;->a:Lccd;

    invoke-static {v1}, Lccd;->b(Lccd;)Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->bottom:F

    float-to-double v1, v1

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 155
    :cond_0
    return v0
.end method
