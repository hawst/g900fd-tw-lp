.class public final Lewj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewj;


# instance fields
.field public b:[B

.field public c:Ljava/lang/String;

.field public d:[Lewk;

.field public e:Lewk;

.field public f:Levt;

.field public g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lewj;

    sput-object v0, Lewj;->a:[Lewj;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 192
    sget-object v0, Lewk;->a:[Lewk;

    iput-object v0, p0, Lewj;->d:[Lewk;

    .line 195
    iput-object v1, p0, Lewj;->e:Lewk;

    .line 198
    iput-object v1, p0, Lewj;->f:Levt;

    .line 201
    iput-object v1, p0, Lewj;->g:Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 235
    iget-object v0, p0, Lewj;->b:[B

    if-eqz v0, :cond_6

    .line 236
    const/4 v0, 0x1

    iget-object v2, p0, Lewj;->b:[B

    .line 237
    invoke-static {v0, v2}, Lepl;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 239
    :goto_0
    iget-object v2, p0, Lewj;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 240
    const/4 v2, 0x2

    iget-object v3, p0, Lewj;->c:Ljava/lang/String;

    .line 241
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 243
    :cond_0
    iget-object v2, p0, Lewj;->d:[Lewk;

    if-eqz v2, :cond_2

    .line 244
    iget-object v2, p0, Lewj;->d:[Lewk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 245
    if-eqz v4, :cond_1

    .line 246
    const/4 v5, 0x3

    .line 247
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 244
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 251
    :cond_2
    iget-object v1, p0, Lewj;->e:Lewk;

    if-eqz v1, :cond_3

    .line 252
    const/4 v1, 0x4

    iget-object v2, p0, Lewj;->e:Lewk;

    .line 253
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_3
    iget-object v1, p0, Lewj;->f:Levt;

    if-eqz v1, :cond_4

    .line 256
    const/4 v1, 0x5

    iget-object v2, p0, Lewj;->f:Levt;

    .line 257
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_4
    iget-object v1, p0, Lewj;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 260
    const/4 v1, 0x6

    iget-object v2, p0, Lewj;->g:Ljava/lang/Integer;

    .line 261
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_5
    iget-object v1, p0, Lewj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    iput v0, p0, Lewj;->cachedSize:I

    .line 265
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lewj;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lewj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lewj;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Lewj;->b:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewj;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lewj;->d:[Lewk;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lewk;

    iget-object v3, p0, Lewj;->d:[Lewk;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lewj;->d:[Lewk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lewj;->d:[Lewk;

    :goto_2
    iget-object v2, p0, Lewj;->d:[Lewk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lewj;->d:[Lewk;

    new-instance v3, Lewk;

    invoke-direct {v3}, Lewk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewj;->d:[Lewk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lewj;->d:[Lewk;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lewj;->d:[Lewk;

    new-instance v3, Lewk;

    invoke-direct {v3}, Lewk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewj;->d:[Lewk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lewj;->e:Lewk;

    if-nez v0, :cond_5

    new-instance v0, Lewk;

    invoke-direct {v0}, Lewk;-><init>()V

    iput-object v0, p0, Lewj;->e:Lewk;

    :cond_5
    iget-object v0, p0, Lewj;->e:Lewk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lewj;->f:Levt;

    if-nez v0, :cond_6

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lewj;->f:Levt;

    :cond_6
    iget-object v0, p0, Lewj;->f:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewj;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewj;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 206
    iget-object v0, p0, Lewj;->b:[B

    if-eqz v0, :cond_0

    .line 207
    const/4 v0, 0x1

    iget-object v1, p0, Lewj;->b:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 209
    :cond_0
    iget-object v0, p0, Lewj;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 210
    const/4 v0, 0x2

    iget-object v1, p0, Lewj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 212
    :cond_1
    iget-object v0, p0, Lewj;->d:[Lewk;

    if-eqz v0, :cond_3

    .line 213
    iget-object v1, p0, Lewj;->d:[Lewk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 214
    if-eqz v3, :cond_2

    .line 215
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 213
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_3
    iget-object v0, p0, Lewj;->e:Lewk;

    if-eqz v0, :cond_4

    .line 220
    const/4 v0, 0x4

    iget-object v1, p0, Lewj;->e:Lewk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 222
    :cond_4
    iget-object v0, p0, Lewj;->f:Levt;

    if-eqz v0, :cond_5

    .line 223
    const/4 v0, 0x5

    iget-object v1, p0, Lewj;->f:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 225
    :cond_5
    iget-object v0, p0, Lewj;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 226
    const/4 v0, 0x6

    iget-object v1, p0, Lewj;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 228
    :cond_6
    iget-object v0, p0, Lewj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 230
    return-void
.end method
