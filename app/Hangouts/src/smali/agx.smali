.class public final Lagx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahg;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ljava/util/ArrayList;

.field final synthetic c:Z

.field final synthetic d:Ljava/util/ArrayList;

.field final synthetic e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;ZLjava/util/ArrayList;ZLjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 2117
    iput-object p1, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iput-boolean p2, p0, Lagx;->a:Z

    iput-object p3, p0, Lagx;->b:Ljava/util/ArrayList;

    iput-boolean p4, p0, Lagx;->c:Z

    iput-object p5, p0, Lagx;->d:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2133
    iget-object v0, p0, Lagx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lagx;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-nez v0, :cond_1

    .line 2134
    const-string v0, "Babel"

    const-string v1, "can\'t place a call or Hangout in this conversation!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 2166
    :cond_0
    :goto_0
    return-void

    .line 2139
    :cond_1
    iget-object v0, p0, Lagx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lagx;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-ne v0, v2, :cond_4

    .line 2140
    iget-object v0, p0, Lagx;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 2141
    iget-object v1, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, p0, Lagx;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 2143
    :cond_2
    iget-object v0, p0, Lagx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    .line 2144
    instance-of v1, v0, Laed;

    if-eqz v1, :cond_3

    .line 2145
    iget-object v0, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(II)V

    goto :goto_0

    .line 2147
    :cond_3
    iget-object v1, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 2153
    :cond_4
    iget-object v0, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v0

    .line 2154
    if-eqz v0, :cond_0

    .line 2155
    iget-object v1, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getFragmentManager()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v1

    .line 2156
    iget-object v2, v0, Lbdh;->e:Ljava/lang/String;

    iget-object v3, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, v0, Lbdh;->b:Lbdk;

    .line 2159
    invoke-virtual {v3, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Lbdk;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lagx;->b:Ljava/util/ArrayList;

    iget-object v4, p0, Lagx;->d:Ljava/util/ArrayList;

    .line 2157
    invoke-static {v2, v0, v3, v4}, Ladn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ladn;

    move-result-object v0

    .line 2162
    iget-object v2, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0, v2, v5}, Ladn;->setTargetFragment(Lt;I)V

    .line 2163
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ladn;->a(Lao;Ljava/lang/String;)I

    .line 2164
    iget-object v1, p0, Lagx;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ls;)Ls;

    goto :goto_0
.end method

.method public a(Lajk;)Z
    .locals 4

    .prologue
    .line 2120
    iget-object v0, p1, Lajk;->c:Ljava/lang/String;

    invoke-static {v0}, Lsi;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2121
    iget-boolean v0, p0, Lagx;->a:Z

    if-eqz v0, :cond_0

    .line 2122
    iget-object v0, p0, Lagx;->b:Ljava/util/ArrayList;

    new-instance v1, Laeh;

    iget-object v2, p1, Lajk;->c:Ljava/lang/String;

    iget-object v3, p1, Lajk;->f:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Laeh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2124
    :cond_0
    iget-boolean v0, p0, Lagx;->c:Z

    if-eqz v0, :cond_1

    .line 2125
    iget-object v0, p0, Lagx;->d:Ljava/util/ArrayList;

    new-instance v1, Laeh;

    iget-object v2, p1, Lajk;->c:Ljava/lang/String;

    iget-object v3, p1, Lajk;->f:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Laeh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2128
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
