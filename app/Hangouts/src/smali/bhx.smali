.class public Lbhx;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:J

.field private final i:J

.field private final j:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;JJJ)V
    .locals 1

    .prologue
    .line 628
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 629
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbhx;->g:Ljava/lang/String;

    .line 630
    iput-wide p2, p0, Lbhx;->h:J

    .line 631
    iput-wide p4, p0, Lbhx;->i:J

    .line 632
    iput-wide p6, p0, Lbhx;->j:J

    .line 633
    return-void

    .line 629
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 13

    .prologue
    .line 638
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 639
    iget-object v0, p0, Lbhx;->b:Lbea;

    check-cast v0, Lbfc;

    .line 640
    iget-object v1, v0, Lbfc;->r:Ljava/lang/String;

    .line 641
    iget-object v2, v0, Lbfc;->q:Ljava/lang/String;

    .line 642
    iget-object v0, p0, Lbhx;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbhx;->g:Ljava/lang/String;

    .line 646
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_0
    iget-wide v4, p0, Lbhx;->h:J

    iget-wide v6, p0, Lbhx;->i:J

    iget-object v0, p0, Lbhx;->b:Lbea;

    check-cast v0, Lbfc;

    iget-object v0, v0, Lbfc;->m:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v8, 0x1

    :goto_1
    iget-object v0, p0, Lbhx;->b:Lbea;

    check-cast v0, Lbfc;

    iget-wide v9, v0, Lbfc;->o:J

    iget-wide v11, p0, Lbhx;->j:J

    move-object v0, p1

    .line 642
    invoke-static/range {v0 .. v12}, Lyp;->a(Lyt;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJZJJ)V

    .line 652
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "sent_mms_count_since_last_upload"

    invoke-virtual {v0, v1}, Lbsx;->d(Ljava/lang/String;)V

    .line 654
    return-void

    .line 646
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_1
.end method
