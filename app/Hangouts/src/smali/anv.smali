.class public final Lanv;
.super Lbaj;
.source "PG"


# static fields
.field private static final u:Z


# instance fields
.field private final v:Ldh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">.dh;"
        }
    .end annotation
.end field

.field private final w:Lyj;

.field private final x:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lanv;->u:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lyj;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lbaj;-><init>(Landroid/content/Context;Lyj;)V

    .line 37
    new-instance v0, Ldh;

    invoke-direct {v0, p0}, Ldh;-><init>(Ldg;)V

    iput-object v0, p0, Lanv;->v:Ldh;

    .line 38
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->g:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lanv;->a(Landroid/net/Uri;)V

    .line 39
    iput-object p2, p0, Lanv;->w:Lyj;

    .line 40
    iput-object p3, p0, Lanv;->x:[Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public z()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 48
    iget-object v5, p0, Lanv;->w:Lyj;

    iget-object v2, p0, Lanv;->x:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->g:Landroid/net/Uri;

    invoke-static {v0, v5}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s != \'\' AND %s != ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "name"

    aput-object v7, v6, v9

    const-string v7, "chat_id"

    aput-object v7, v6, v8

    invoke-static {v3, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v5}, Lyj;->c()Lbdk;

    move-result-object v5

    iget-object v5, v5, Lbdk;->b:Ljava/lang/String;

    aput-object v5, v4, v9

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s"

    new-array v7, v8, [Ljava/lang/Object;

    const-string v8, "_id"

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 49
    sget-boolean v1, Lanv;->u:Z

    if-eqz v1, :cond_0

    .line 50
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getSuggestedPeople returned cursor with length "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    if-eqz v0, :cond_1

    .line 54
    iget-object v1, p0, Lanv;->v:Ldh;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 56
    :cond_1
    return-object v0
.end method
