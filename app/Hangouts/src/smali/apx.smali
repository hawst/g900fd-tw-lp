.class public final Lapx;
.super Lbor;
.source "PG"

# interfaces
.implements Laoz;


# static fields
.field public static final a:Z


# instance fields
.field private A:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private B:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private C:Z

.field private D:Z

.field private final E:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Laqe;",
            ">;"
        }
    .end annotation
.end field

.field private F:I

.field private G:Z

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Z

.field private L:Z

.field private final M:Ljava/util/Random;

.field private N:I

.field private final O:Lasi;

.field private P:Ljava/lang/String;

.field private Q:Z

.field private final R:Ljava/lang/Runnable;

.field private final S:Ljava/lang/Runnable;

.field private final T:Ljava/lang/Runnable;

.field private final U:Ljava/lang/Runnable;

.field private final V:Ljava/lang/Runnable;

.field private final b:Lapk;

.field private final c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private final g:Landroid/os/Handler;

.field private final h:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

.field private i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

.field private final j:Ljava/lang/String;

.field private final k:I

.field private final l:I

.field private m:Z

.field private final n:Z

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Laop;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Larw;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lbzx;

.field private w:I

.field private x:Z

.field private y:Z

.field private final z:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lapx;->a:Z

    return-void
.end method

.method constructor <init>(Lcom/google/android/libraries/hangouts/video/HangoutRequest;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Lbdh;IIJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/HangoutRequest;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Lbdh;",
            "IIJ)V"
        }
    .end annotation

    .prologue
    .line 290
    invoke-direct {p0}, Lbor;-><init>()V

    .line 163
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v2

    iput-object v2, p0, Lapx;->b:Lapk;

    .line 169
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lapx;->g:Landroid/os/Handler;

    .line 185
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lapx;->s:Ljava/util/List;

    .line 188
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lapx;->t:Ljava/util/ArrayList;

    .line 198
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lapx;->z:Ljava/util/LinkedList;

    .line 201
    const/4 v2, 0x1

    iput-boolean v2, p0, Lapx;->C:Z

    .line 204
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v2, p0, Lapx;->E:Ljava/util/LinkedHashMap;

    .line 209
    const/4 v2, 0x0

    iput v2, p0, Lapx;->H:I

    .line 215
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    iput-object v2, p0, Lapx;->M:Ljava/util/Random;

    .line 224
    const/4 v2, 0x0

    iput v2, p0, Lapx;->N:I

    .line 252
    new-instance v2, Lapy;

    invoke-direct {v2, p0}, Lapy;-><init>(Lapx;)V

    iput-object v2, p0, Lapx;->R:Ljava/lang/Runnable;

    .line 260
    new-instance v2, Lapz;

    invoke-direct {v2, p0}, Lapz;-><init>(Lapx;)V

    iput-object v2, p0, Lapx;->S:Ljava/lang/Runnable;

    .line 268
    new-instance v2, Laqa;

    invoke-direct {v2, p0}, Laqa;-><init>(Lapx;)V

    iput-object v2, p0, Lapx;->T:Ljava/lang/Runnable;

    .line 277
    new-instance v2, Laqb;

    invoke-direct {v2, p0}, Laqb;-><init>(Lapx;)V

    iput-object v2, p0, Lapx;->U:Ljava/lang/Runnable;

    .line 1197
    new-instance v2, Laqc;

    invoke-direct {v2, p0}, Laqc;-><init>(Lapx;)V

    iput-object v2, p0, Lapx;->V:Ljava/lang/Runnable;

    .line 291
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->clone()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v2

    iput-object v2, p0, Lapx;->h:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 292
    iput-object p1, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 293
    iput p7, p0, Lapx;->k:I

    .line 294
    move/from16 v0, p8

    iput v0, p0, Lapx;->l:I

    .line 295
    iput-boolean p2, p0, Lapx;->c:Z

    .line 296
    const/4 v2, 0x1

    iput-boolean v2, p0, Lapx;->n:Z

    .line 298
    iput-object p5, p0, Lapx;->u:Ljava/util/List;

    .line 300
    new-instance v2, Lasi;

    invoke-direct {v2, p0, p6}, Lasi;-><init>(Lapx;Lbdh;)V

    iput-object v2, p0, Lapx;->O:Lasi;

    .line 301
    const-string v2, ""

    iput-object v2, p0, Lapx;->P:Ljava/lang/String;

    .line 302
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    :cond_0
    if-eqz p4, :cond_3

    .line 303
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    :cond_1
    const/4 v2, 0x1

    move v3, v2

    .line 304
    :goto_0
    if-eqz v3, :cond_2

    .line 305
    invoke-static {p5}, Lcwz;->a(Ljava/lang/Object;)V

    .line 308
    new-instance v2, Larw;

    const/4 v4, 0x1

    invoke-direct {v2, p0, v4, p3, p4}, Larw;-><init>(Lapx;ZLjava/util/List;Ljava/util/List;)V

    .line 310
    iget-object v4, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    iget-object v2, p0, Lapx;->O:Lasi;

    invoke-virtual {v2, p3}, Lasi;->a(Ljava/util/List;)V

    .line 315
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lapx;->L:Z

    .line 317
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getCallMediaType()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_4

    const/4 v2, 0x1

    :goto_1
    const/4 v4, 0x0

    .line 316
    invoke-direct {p0, v2, v4}, Lapx;->a(ZZ)V

    .line 319
    if-eqz v3, :cond_5

    sget-object v2, Lbzv;->d:[Ljava/lang/String;

    .line 321
    :goto_2
    new-instance v3, Lcxj;

    invoke-direct {v3}, Lcxj;-><init>()V

    invoke-static {}, Lcxj;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lapx;->j:Ljava/lang/String;

    .line 322
    const/4 v3, 0x0

    iput-boolean v3, p0, Lapx;->Q:Z

    .line 324
    const-string v3, "type_null"

    .line 325
    packed-switch p8, :pswitch_data_0

    .line 339
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HangoutState initialized with a CallType outside the known range: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :goto_3
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "source_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v3, v4, v5

    .line 346
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    .line 345
    invoke-static {v3, v2, v4}, Lbzv;->a(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)Lbzx;

    move-result-object v2

    iput-object v2, p0, Lapx;->v:Lbzx;

    .line 347
    iget-object v2, p0, Lapx;->v:Lbzx;

    const/4 v3, 0x0

    move-wide/from16 v0, p9

    invoke-virtual {v2, v3, v0, v1}, Lbzx;->a(IJ)V

    .line 348
    return-void

    .line 303
    :cond_3
    const/4 v2, 0x0

    move v3, v2

    goto :goto_0

    .line 317
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 319
    :cond_5
    sget-object v2, Lbzv;->c:[Ljava/lang/String;

    goto :goto_2

    .line 327
    :pswitch_0
    const-string v3, "type_video"

    goto :goto_3

    .line 330
    :pswitch_1
    const-string v3, "type_audio"

    goto :goto_3

    .line 333
    :pswitch_2
    const-string v3, "type_pstn"

    goto :goto_3

    .line 336
    :pswitch_3
    const-string v3, "type_unk"

    goto :goto_3

    .line 325
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final A()J
    .locals 3

    .prologue
    .line 927
    const-string v0, "babel_hangout_enter_step_timeout"

    const-wide/16 v1, 0x2ee0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static G()V
    .locals 0

    .prologue
    .line 1172
    return-void
.end method

.method private X()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Larw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 575
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method private Y()Larw;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 616
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    invoke-virtual {v0}, Larw;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 617
    :goto_0
    return-object v0

    .line 616
    :cond_1
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    .line 617
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    goto :goto_0
.end method

.method private Z()V
    .locals 6

    .prologue
    .line 841
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 842
    invoke-static {v0}, Lbkb;->j(Lyj;)Ljava/lang/String;

    move-result-object v0

    .line 843
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v1

    iget-object v2, p0, Lapx;->j:Ljava/lang/String;

    iget-object v3, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 844
    invoke-static {}, Lbld;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    .line 843
    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/google/android/libraries/hangouts/video/VideoChat;->startCall(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1406
    iget v0, p0, Lapx;->H:I

    if-nez v0, :cond_0

    .line 1407
    iput p1, p0, Lapx;->H:I

    .line 1408
    iput-object p2, p0, Lapx;->I:Ljava/lang/String;

    .line 1410
    :cond_0
    return-void
.end method

.method private a(Laqe;)V
    .locals 2

    .prologue
    .line 1213
    invoke-virtual {p0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 1214
    invoke-virtual {v0, p1}, Lapo;->a(Laqe;)V

    goto :goto_0

    .line 1216
    :cond_0
    return-void
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 728
    iget-boolean v0, p0, Lapx;->L:Z

    if-eq v0, p1, :cond_0

    .line 729
    iput-boolean p1, p0, Lapx;->L:Z

    .line 730
    if-eqz p2, :cond_1

    .line 731
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v1, p0, Lapx;->U:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 736
    :cond_0
    :goto_0
    return-void

    .line 733
    :cond_1
    iget-object v0, p0, Lapx;->U:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method static synthetic a(Lapx;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lapx;->L:Z

    return v0
.end method

.method public static a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z
    .locals 1

    .prologue
    .line 917
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aa()V
    .locals 2

    .prologue
    .line 1205
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v1, p0, Lapx;->V:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1206
    iget-object v0, p0, Lapx;->E:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1207
    invoke-virtual {p0}, Lapx;->H()Laqe;

    move-result-object v0

    .line 1208
    invoke-direct {p0, v0}, Lapx;->a(Laqe;)V

    .line 1210
    :cond_0
    return-void
.end method

.method private ab()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1257
    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    .line 1258
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1293
    :cond_0
    :goto_0
    return-void

    .line 1267
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    .line 1268
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    move v3, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 1269
    instance-of v5, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v5, :cond_3

    .line 1270
    or-int/lit8 v3, v3, 0x1

    .line 1274
    :cond_2
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1275
    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    .line 1277
    goto :goto_1

    .line 1271
    :cond_3
    instance-of v5, v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    if-eqz v5, :cond_2

    .line 1272
    or-int/lit8 v3, v3, 0x2

    goto :goto_2

    .line 1280
    :cond_4
    const/4 v0, 0x1

    if-le v1, v0, :cond_5

    .line 1281
    invoke-virtual {p0, v2}, Lapx;->e(Z)V

    .line 1285
    :cond_5
    iget v0, p0, Lapx;->N:I

    if-eq v0, v3, :cond_0

    .line 1286
    iput v3, p0, Lapx;->N:I

    .line 1287
    invoke-virtual {p0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 1288
    invoke-virtual {v0, v3}, Lapo;->a(I)V

    goto :goto_4

    .line 1291
    :cond_6
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    iget-object v1, p0, Lapx;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->updateJoinedNotification(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 1430
    invoke-virtual {p0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 1431
    invoke-virtual {v0}, Lapo;->h()V

    goto :goto_0

    .line 1433
    :cond_0
    return-void
.end method

.method static synthetic b(Lapx;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lapx;->aa()V

    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 457
    if-eqz p0, :cond_0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "STARTED"

    .line 458
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 800
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 801
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->setExternalKey(Ljava/lang/String;)V

    .line 802
    invoke-direct {p0}, Lapx;->Z()V

    .line 803
    return-void
.end method

.method private l(Ljava/lang/String;)Laop;
    .locals 3

    .prologue
    .line 1469
    iget-object v0, p0, Lapx;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laop;

    .line 1470
    invoke-virtual {v0}, Laop;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1474
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final z()J
    .locals 3

    .prologue
    .line 921
    const-string v0, "babel_hangout_outgoing_invite_max_duration_ms"

    const-wide/16 v1, 0x7530

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public B()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 942
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 943
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    .line 944
    invoke-virtual {v0}, Larw;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 946
    :cond_0
    return-object v1
.end method

.method public C()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 950
    iget-object v0, p0, Lapx;->u:Ljava/util/List;

    return-object v0
.end method

.method public D()I
    .locals 1

    .prologue
    .line 954
    iget v0, p0, Lapx;->w:I

    return v0
.end method

.method public E()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 1005
    iget-object v0, p0, Lapx;->A:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method public F()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 1013
    iget-object v0, p0, Lapx;->B:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method public H()Laqe;
    .locals 1

    .prologue
    .line 1219
    iget-object v0, p0, Lapx;->E:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1220
    const/4 v0, 0x0

    .line 1224
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lapx;->E:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqe;

    goto :goto_0
.end method

.method public I()Z
    .locals 1

    .prologue
    .line 1234
    iget-boolean v0, p0, Lapx;->C:Z

    return v0
.end method

.method public J()I
    .locals 1

    .prologue
    .line 1249
    iget v0, p0, Lapx;->N:I

    return v0
.end method

.method public K()Z
    .locals 1

    .prologue
    .line 1296
    iget-boolean v0, p0, Lapx;->L:Z

    return v0
.end method

.method public L()Z
    .locals 1

    .prologue
    .line 1300
    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1301
    :cond_0
    iget-boolean v0, p0, Lapx;->D:Z

    .line 1303
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->isAudioMuted()Z

    move-result v0

    goto :goto_0
.end method

.method public M()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1344
    iget-object v0, p0, Lapx;->P:Ljava/lang/String;

    return-object v0
.end method

.method public N()Z
    .locals 1

    .prologue
    .line 1348
    iget-boolean v0, p0, Lapx;->G:Z

    return v0
.end method

.method O()I
    .locals 2

    .prologue
    .line 1395
    iget v0, p0, Lapx;->H:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1396
    iget v0, p0, Lapx;->H:I

    return v0
.end method

.method P()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1401
    iget v0, p0, Lapx;->H:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1402
    iget-object v0, p0, Lapx;->I:Ljava/lang/String;

    return-object v0
.end method

.method public Q()Z
    .locals 1

    .prologue
    .line 1413
    iget-boolean v0, p0, Lapx;->K:Z

    return v0
.end method

.method public R()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1418
    iget-object v0, p0, Lapx;->J:Ljava/lang/String;

    return-object v0
.end method

.method public S()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Laqd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527
    iget-object v0, p0, Lapx;->O:Lasi;

    invoke-virtual {v0}, Lasi;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method T()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1531
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1501
    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lapx;->c(Ljava/lang/String;)V

    .line 1502
    invoke-virtual {p0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1503
    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 1167
    iput p1, p0, Lapx;->f:I

    .line 1168
    return-void
.end method

.method public a(ILyj;Lbea;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 773
    iget v0, p0, Lapx;->F:I

    if-ne p1, v0, :cond_0

    .line 774
    const-string v0, "Babel"

    const-string v1, "Create hangout id request failed"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    const/4 v0, 0x0

    iput v0, p0, Lapx;->F:I

    .line 776
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lapx;->d(Ljava/lang/String;)V

    .line 778
    :cond_0
    return-void
.end method

.method public a(Laoy;)V
    .locals 2

    .prologue
    .line 1507
    instance-of v0, p1, Laop;

    if-eqz v0, :cond_0

    .line 1508
    invoke-virtual {p0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    invoke-virtual {v0}, Lapo;->g()V

    goto :goto_0

    .line 1510
    :cond_0
    return-void
.end method

.method a(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 2

    .prologue
    .line 848
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v1, p0, Lapx;->R:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 850
    if-eqz p1, :cond_0

    .line 851
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getEndCause()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lapx;->a(ILjava/lang/String;)V

    .line 854
    :cond_0
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    .line 855
    invoke-virtual {v0}, Larw;->h()V

    goto :goto_0

    .line 858
    :cond_1
    iget-boolean v0, p0, Lapx;->x:Z

    if-eqz v0, :cond_2

    .line 859
    invoke-static {}, Lavy;->i()V

    .line 861
    :cond_2
    iget-object v0, p0, Lapx;->O:Lasi;

    invoke-virtual {v0}, Lasi;->b()V

    .line 862
    return-void
.end method

.method public a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 993
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 994
    iget v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->type:I

    if-eq v0, v2, :cond_0

    iget v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 996
    iget v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->type:I

    if-ne v0, v2, :cond_2

    .line 997
    iget-object v0, p0, Lapx;->z:Ljava/util/LinkedList;

    iget v1, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->ssrc:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1002
    :goto_1
    return-void

    .line 994
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1000
    :cond_2
    iget-object v0, p0, Lapx;->z:Ljava/util/LinkedList;

    iget v1, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->ssrc:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Ljava/lang/Integer;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1021
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 1025
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    move v3, v2

    .line 1032
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getVideoSsrcs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1034
    iget-object v5, p0, Lapx;->z:Ljava/util/LinkedList;

    invoke-virtual {v5, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1028
    :cond_0
    iget v0, p0, Lapx;->w:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 1029
    iget v0, p0, Lapx;->w:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lapx;->w:I

    move v3, v1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1028
    goto :goto_2

    .line 1037
    :cond_2
    iget-object v0, p0, Lapx;->A:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-ne p1, v0, :cond_3

    .line 1038
    iput-object v6, p0, Lapx;->A:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 1041
    :cond_3
    iget-object v0, p0, Lapx;->B:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-ne p1, v0, :cond_4

    .line 1042
    iput-object v6, p0, Lapx;->B:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 1045
    :cond_4
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    invoke-virtual {v0, p1}, Larw;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v4, v1

    .line 1046
    :goto_3
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_c

    .line 1049
    iget v0, p0, Lapx;->w:I

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lapx;->o:Z

    if-nez v0, :cond_6

    .line 1051
    const/16 v0, 0x3ed

    invoke-virtual {p0, v0}, Lapx;->c(I)V

    move v3, v2

    .line 1059
    :cond_6
    if-eqz v3, :cond_7

    iget-boolean v0, p0, Lapx;->n:Z

    if-eqz v0, :cond_7

    .line 1060
    sget v0, Lf;->hL:I

    invoke-static {v0}, Lf;->f(I)V

    .line 1062
    :cond_7
    iget-object v0, p0, Lapx;->O:Lasi;

    invoke-virtual {v0, p1}, Lasi;->b(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 1063
    invoke-direct {p0}, Lapx;->ab()V

    .line 1066
    if-eqz p2, :cond_a

    if-eqz v4, :cond_a

    .line 1067
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_a

    .line 1068
    check-cast p1, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    .line 1071
    invoke-static {p2, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    .line 1072
    invoke-virtual {p0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v2

    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 1073
    invoke-virtual {v0, v3}, Lapo;->b(I)Z

    move-result v0

    .line 1074
    if-eqz v0, :cond_8

    .line 1075
    :cond_9
    if-nez v0, :cond_a

    .line 1082
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    packed-switch v3, :pswitch_data_0

    .line 1086
    :cond_a
    :goto_4
    :pswitch_0
    return-void

    :cond_b
    move v4, v2

    .line 1045
    goto :goto_3

    .line 1054
    :cond_c
    invoke-direct {p0}, Lapx;->X()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    .line 1055
    invoke-virtual {v0, p1, p2}, Larw;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Ljava/lang/Integer;)V

    goto :goto_5

    .line 1082
    :pswitch_1
    sget v2, Lh;->dX:I

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4

    :pswitch_2
    sget v2, Lh;->dV:I

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4

    :pswitch_3
    sget v3, Lh;->dW:I

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 959
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isRinging()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Lcwz;->b(Z)V

    .line 961
    iget v0, p0, Lapx;->w:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapx;->w:I

    .line 962
    iput-boolean v1, p0, Lapx;->x:Z

    .line 963
    invoke-direct {p0}, Lapx;->X()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    .line 964
    invoke-virtual {v0, p1}, Larw;->b(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    goto :goto_1

    .line 959
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 966
    :cond_2
    if-nez p2, :cond_3

    iget-boolean v0, p0, Lapx;->n:Z

    if-eqz v0, :cond_3

    .line 967
    sget v0, Lf;->hK:I

    invoke-static {v0}, Lf;->f(I)V

    .line 969
    :cond_3
    iget-object v0, p0, Lapx;->O:Lasi;

    invoke-virtual {v0, p1}, Lasi;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 970
    invoke-direct {p0}, Lapx;->ab()V

    .line 971
    return-void
.end method

.method a(Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;)V
    .locals 3

    .prologue
    .line 674
    iget-object v0, p0, Lapx;->v:Lbzx;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lbzx;->a([I)V

    .line 676
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "muc connected:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " session:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lapx;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    return-void

    .line 674
    :array_0
    .array-data 4
        0x4
        0x5
    .end array-data
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 446
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->cloneWithConversationId(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    iput-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 447
    sget-boolean v0, Lapx;->a:Z

    if-eqz v0, :cond_0

    .line 448
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;C)V
    .locals 3

    .prologue
    .line 1316
    sget-boolean v0, Lapx;->a:Z

    if-eqz v0, :cond_0

    .line 1317
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HangoutState - sending dtmf: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    const-string v1, "dtmf_code_duration_ms"

    const/16 v2, 0x15e

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, p2, v1, p1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendDtmf(CILjava/lang/String;)V

    .line 1320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lapx;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapx;->P:Ljava/lang/String;

    .line 1321
    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1120
    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 1140
    :cond_0
    :goto_1
    return-void

    .line 1120
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1125
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    .line 1127
    const/16 v0, 0x3ec

    invoke-virtual {p0, v0}, Lapx;->c(I)V

    goto :goto_1

    .line 1132
    :cond_3
    new-instance v0, Laqe;

    invoke-direct {v0, p1, p2, p3}, Laqe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1134
    iget-object v2, p0, Lapx;->E:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1137
    iget-object v0, p0, Lapx;->E:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1138
    invoke-direct {p0}, Lapx;->aa()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 741
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v1, p0, Lapx;->S:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 742
    iget-boolean v0, p0, Lapx;->G:Z

    if-nez v0, :cond_1

    .line 743
    iget-boolean v0, p0, Lapx;->G:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "conversation"

    iget-object v2, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKeyType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lyt;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->isNewHangoutRequest()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 746
    :cond_1
    :goto_0
    return-void

    .line 743
    :cond_2
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lapx;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lapx;->b:Lapk;

    invoke-virtual {v0}, Lapk;->j()V

    goto :goto_0
.end method

.method a(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 883
    new-instance v0, Larw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Larw;-><init>(Lapx;ZLjava/util/List;Ljava/util/List;)V

    .line 885
    iget-object v1, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 886
    invoke-virtual {v0}, Larw;->f()V

    .line 887
    iget-object v0, p0, Lapx;->O:Lasi;

    invoke-virtual {v0, p1}, Lasi;->a(Ljava/util/List;)V

    .line 888
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 453
    iput-boolean p1, p0, Lapx;->m:Z

    .line 454
    return-void
.end method

.method public a(Lcom/google/android/apps/hangouts/hangout/HangoutActivity;)Z
    .locals 2

    .prologue
    .line 420
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->o()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    .line 422
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 427
    iget-object v1, p0, Lapx;->h:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 428
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 434
    :goto_0
    return v0

    .line 428
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 434
    :cond_2
    iget-object v1, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method b()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 351
    iget-object v0, p0, Lapx;->v:Lbzx;

    new-array v1, v9, [I

    aput v9, v1, v2

    invoke-virtual {v0, v1}, Lbzx;->a([I)V

    .line 352
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v1, p0, Lapx;->R:Ljava/lang/Runnable;

    const-string v3, "babel_hangout_enter_master_timeout"

    const-wide/32 v4, 0xea60

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 356
    iget-boolean v0, p0, Lapx;->Q:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "babel_hangout_write_logs_2"

    invoke-static {v1, v3, v9}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lapx;->j()Lyj;

    move-result-object v1

    invoke-static {v1}, Lf;->c(Lyj;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lf;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/io/File;

    invoke-static {v0}, Lf;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lf;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_1
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x5

    const/4 v4, 0x7

    invoke-static {v0, v3, v4}, Lf;->a(Ljava/lang/String;II)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lapx;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".log.bz2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapx;->J:Ljava/lang/String;

    :cond_2
    iget v0, p0, Lapx;->l:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    move v2, v9

    :cond_3
    invoke-virtual {p0}, Lapx;->j()Lyj;

    move-result-object v0

    invoke-static {v0}, Lym;->q(Lyj;)Z

    move-result v7

    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallOptions;

    iget-object v1, p0, Lapx;->j:Ljava/lang/String;

    iget-boolean v3, p0, Lapx;->n:Z

    invoke-virtual {p0}, Lapx;->j()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->k()Z

    move-result v5

    iget-boolean v6, p0, Lapx;->c:Z

    iget-object v8, p0, Lapx;->J:Ljava/lang/String;

    move v4, v2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/libraries/hangouts/video/CallOptions;-><init>(Ljava/lang/String;ZZZZZZLjava/lang/String;)V

    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->prepareCall(Lcom/google/android/libraries/hangouts/video/CallOptions;)V

    iput-boolean v9, p0, Lapx;->Q:Z

    .line 360
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKey()Ljava/lang/String;

    move-result-object v0

    .line 362
    invoke-static {}, Lapx;->A()J

    move-result-wide v1

    .line 363
    iget-object v3, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->isNewHangoutRequest()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 364
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v3, p0, Lapx;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 365
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 366
    invoke-virtual {p0}, Lapx;->j()Lyj;

    move-result-object v0

    iget-object v1, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 367
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutType()I

    move-result v1

    .line 366
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;I)I

    move-result v0

    iput v0, p0, Lapx;->F:I

    .line 391
    :goto_0
    iget-object v0, p0, Lapx;->b:Lapk;

    invoke-virtual {v0}, Lapk;->j()V

    .line 392
    return-void

    .line 368
    :cond_4
    const-string v3, "conversation"

    iget-object v4, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKeyType()Ljava/lang/String;

    move-result-object v4

    if-ne v3, v4, :cond_6

    .line 369
    invoke-static {v0}, Lyt;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 371
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 372
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 373
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKey()Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 374
    if-eqz v0, :cond_5

    .line 375
    invoke-direct {p0, v0}, Lapx;->k(Ljava/lang/String;)V

    goto :goto_0

    .line 378
    :cond_5
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v3, p0, Lapx;->S:Ljava/lang/Runnable;

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 381
    :cond_6
    const-string v1, "conversation"

    iget-object v2, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKeyType()Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_7

    .line 387
    invoke-virtual {p0}, Lapx;->j()Lyj;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n(Lyj;Ljava/lang/String;)I

    .line 389
    :cond_7
    invoke-direct {p0}, Lapx;->Z()V

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 1253
    iput p1, p0, Lapx;->N:I

    .line 1254
    return-void
.end method

.method public b(Laoy;)V
    .locals 1

    .prologue
    .line 1521
    instance-of v0, p1, Laop;

    if-eqz v0, :cond_0

    .line 1522
    iget-object v0, p0, Lapx;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1524
    :cond_0
    return-void
.end method

.method b(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 2

    .prologue
    .line 975
    invoke-direct {p0}, Lapx;->X()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    .line 976
    invoke-virtual {v0}, Larw;->i()V

    goto :goto_0

    .line 981
    :cond_0
    iget v0, p0, Lapx;->w:I

    if-nez v0, :cond_1

    .line 982
    invoke-virtual {p0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 983
    invoke-virtual {v0, p1}, Lapo;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    goto :goto_1

    .line 986
    :cond_1
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 462
    iput-boolean p1, p0, Lapx;->o:Z

    .line 464
    iget-boolean v0, p0, Lapx;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lapx;->q:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 465
    return-void

    .line 464
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    return-object v0
.end method

.method public c(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1352
    iget-boolean v0, p0, Lapx;->G:Z

    if-eqz v0, :cond_1

    .line 1388
    :cond_0
    :goto_0
    return-void

    .line 1355
    :cond_1
    iput-boolean v1, p0, Lapx;->G:Z

    .line 1357
    iget-boolean v0, p0, Lapx;->Q:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 1358
    iput-boolean v2, p0, Lapx;->Q:Z

    .line 1359
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lapx;->a(ILjava/lang/String;)V

    .line 1361
    iget-object v0, p0, Lapx;->v:Lbzx;

    invoke-virtual {v0}, Lbzx;->c()V

    .line 1362
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v3, p0, Lapx;->R:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1363
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v3, p0, Lapx;->S:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1364
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v3, p0, Lapx;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1366
    iput v2, p0, Lapx;->F:I

    .line 1367
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 1369
    iget-object v0, p0, Lapx;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laop;

    .line 1370
    invoke-virtual {v0}, Laop;->f()V

    goto :goto_1

    .line 1372
    :cond_2
    iget-object v0, p0, Lapx;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1373
    iget-object v0, p0, Lapx;->j:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 1374
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    iget-object v3, p0, Lapx;->j:Ljava/lang/String;

    invoke-virtual {v0, v3, p1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->terminateCall(Ljava/lang/String;I)V

    .line 1375
    invoke-direct {p0}, Lapx;->X()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larw;

    .line 1376
    invoke-virtual {v0}, Larw;->g()V

    goto :goto_2

    .line 1378
    :cond_3
    invoke-virtual {p0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 1379
    invoke-virtual {v0}, Lapo;->d()V

    goto :goto_3

    .line 1384
    :cond_4
    iget v0, p0, Lapx;->w:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    if-eqz v0, :cond_0

    .line 1385
    invoke-virtual {p0}, Lapx;->j()Lyj;

    move-result-object v0

    iget-object v1, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 1386
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v1

    .line 1385
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Lyj;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 1384
    goto :goto_4
.end method

.method public c(Laoy;)V
    .locals 1

    .prologue
    .line 1514
    instance-of v0, p1, Laop;

    if-eqz v0, :cond_0

    .line 1515
    invoke-direct {p0}, Lapx;->ac()V

    .line 1517
    :cond_0
    return-void
.end method

.method public c(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 0

    .prologue
    .line 1009
    iput-object p1, p0, Lapx;->A:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 1010
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lapx;->p:Ljava/lang/String;

    .line 473
    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 439
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->cloneWithHangoutId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    iput-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 440
    sget-boolean v0, Lapx;->a:Z

    if-eqz v0, :cond_0

    .line 441
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 522
    iput-boolean p1, p0, Lapx;->q:Z

    .line 524
    iget-boolean v0, p0, Lapx;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lapx;->q:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 525
    return-void

    .line 524
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()I
    .locals 1

    .prologue
    .line 400
    iget v0, p0, Lapx;->k:I

    return v0
.end method

.method public d(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 0

    .prologue
    .line 1017
    iput-object p1, p0, Lapx;->B:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 1018
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 750
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->isNewHangoutRequest()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 751
    iget v0, p0, Lapx;->F:I

    if-nez v0, :cond_0

    .line 768
    :goto_0
    return-void

    .line 756
    :cond_0
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v1, p0, Lapx;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 757
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 758
    const/4 v0, 0x0

    iput v0, p0, Lapx;->F:I

    .line 760
    if-nez p1, :cond_1

    .line 761
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lapx;->c(I)V

    goto :goto_0

    .line 763
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lapx;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    iget-object v0, p0, Lapx;->b:Lapk;

    invoke-virtual {v0}, Lapk;->j()V

    .line 766
    invoke-direct {p0}, Lapx;->Z()V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1176
    iput-object p2, p0, Lapx;->d:Ljava/lang/String;

    .line 1178
    invoke-static {p1}, Lapx;->b(Ljava/lang/String;)Z

    move-result v0

    .line 1179
    invoke-static {p2}, Lapx;->b(Ljava/lang/String;)Z

    move-result v1

    .line 1181
    if-eq v0, v1, :cond_0

    .line 1183
    invoke-virtual {p0}, Lapx;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1184
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    if-eqz v1, :cond_1

    sget v0, Lh;->fi:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1192
    :goto_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1193
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1195
    :cond_0
    return-void

    .line 1184
    :cond_1
    sget v0, Lh;->eS:I

    goto :goto_0

    .line 1187
    :cond_2
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    if-eqz v1, :cond_3

    sget v0, Lh;->fd:I

    :goto_2
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget v0, Lh;->fe:I

    goto :goto_2
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 536
    iput-boolean p1, p0, Lapx;->r:Z

    .line 537
    return-void
.end method

.method e()I
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lapx;->l:I

    return v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1143
    invoke-virtual {p0}, Lapx;->H()Laqe;

    move-result-object v1

    .line 1145
    iget-object v0, p0, Lapx;->E:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqe;

    .line 1148
    if-ne v1, v0, :cond_0

    .line 1149
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lapx;->a(Laqe;)V

    .line 1150
    iget-object v0, p0, Lapx;->E:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1157
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v1, p0, Lapx;->V:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1160
    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 3

    .prologue
    .line 713
    iget-boolean v0, p0, Lapx;->L:Z

    if-eqz v0, :cond_1

    .line 716
    iget-boolean v0, p0, Lapx;->C:Z

    if-nez v0, :cond_0

    .line 717
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v0

    .line 718
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->EARPIECE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 719
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAvailableAudioDevices()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 720
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 723
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lapx;->a(ZZ)V

    .line 725
    :cond_1
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1163
    iput-object p1, p0, Lapx;->e:Ljava/lang/String;

    .line 1164
    return-void
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 1238
    iput-boolean p1, p0, Lapx;->C:Z

    .line 1239
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 528
    iget-boolean v0, p0, Lapx;->q:Z

    return v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1422
    iget-object v0, p0, Lapx;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1424
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->addLogComment(Ljava/lang/String;)V

    .line 1425
    const/4 v0, 0x1

    iput-boolean v0, p0, Lapx;->K:Z

    .line 1427
    :cond_0
    return-void
.end method

.method public g(Z)V
    .locals 1

    .prologue
    .line 1308
    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1309
    :cond_0
    iput-boolean p1, p0, Lapx;->D:Z

    .line 1313
    :goto_0
    return-void

    .line 1311
    :cond_1
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->setMute(Z)V

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 532
    iget-boolean v0, p0, Lapx;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lapx;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1447
    invoke-direct {p0, p1}, Lapx;->l(Ljava/lang/String;)Laop;

    move-result-object v0

    .line 1448
    if-eqz v0, :cond_0

    .line 1449
    invoke-virtual {v0}, Laop;->c()V

    .line 1454
    :goto_0
    return-void

    .line 1453
    :cond_0
    invoke-direct {p0}, Lapx;->ac()V

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 540
    iget-boolean v1, p0, Lapx;->r:Z

    if-eqz v1, :cond_0

    const-string v1, "babel_enable_abuse_reporting"

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1462
    invoke-direct {p0, p1}, Lapx;->l(Ljava/lang/String;)Laop;

    move-result-object v0

    .line 1463
    if-eqz v0, :cond_0

    .line 1464
    invoke-virtual {v0}, Laop;->d()V

    .line 1466
    :cond_0
    return-void
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 546
    iget-boolean v0, p0, Lapx;->n:Z

    return v0
.end method

.method public j()Lyj;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lapx;->i:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    return-object v0
.end method

.method public j(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1483
    new-instance v0, Laop;

    .line 1484
    invoke-virtual {p0}, Lapx;->j()Lyj;

    move-result-object v1

    .line 1485
    invoke-virtual {p0}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutId()Ljava/lang/String;

    move-result-object v2

    .line 1486
    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getJidNickname()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p1}, Laop;-><init>(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    iget-object v1, p0, Lapx;->s:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1489
    invoke-virtual {v0, p0}, Laop;->a(Laoz;)V

    .line 1490
    invoke-virtual {v0}, Laop;->b()V

    .line 1491
    return-void
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lapx;->j:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lapx;->d:Ljava/lang/String;

    return-object v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 566
    iget v0, p0, Lapx;->f:I

    return v0
.end method

.method n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lapo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 570
    iget-object v0, p0, Lapx;->b:Lapk;

    invoke-virtual {v0}, Lapk;->i()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 3

    .prologue
    .line 580
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v1

    .line 581
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lapx;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 582
    return-object v1

    .line 581
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 586
    iget-object v0, p0, Lapx;->v:Lbzx;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lbzx;->a([I)V

    .line 588
    iput-boolean v4, p0, Lapx;->K:Z

    .line 591
    iget-object v0, p0, Lapx;->J:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 592
    iget-object v0, p0, Lapx;->M:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    rem-int/lit16 v0, v0, 0x2710

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 593
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "babel_hangout_upload_rate_2"

    const/16 v3, 0xa

    .line 592
    invoke-static {v1, v2, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 596
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    const-string v1, "Triggering sampled debug log"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->addLogComment(Ljava/lang/String;)V

    .line 597
    iput-boolean v5, p0, Lapx;->K:Z

    .line 600
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_hangout_upload_logs_2"

    .line 599
    invoke-static {v0, v1, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 603
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    const-string v1, "Triggering log upload for auto_plugin_log_upload experiment"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->addLogComment(Ljava/lang/String;)V

    .line 605
    iput-boolean v5, p0, Lapx;->K:Z

    .line 608
    :cond_1
    return-void

    .line 586
    :array_0
    .array-data 4
        0x2
        0x3
    .end array-data
.end method

.method q()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Larw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 621
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    return-object v0
.end method

.method r()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 625
    invoke-direct {p0}, Lapx;->Y()Larw;

    move-result-object v0

    .line 626
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Larw;->b()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method s()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 630
    invoke-direct {p0}, Lapx;->Y()Larw;

    move-result-object v0

    .line 631
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Larw;->c()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public t()Z
    .locals 4

    .prologue
    .line 639
    invoke-direct {p0}, Lapx;->Y()Larw;

    move-result-object v0

    .line 640
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Larw;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method u()V
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lapx;->g:Landroid/os/Handler;

    iget-object v1, p0, Lapx;->R:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 683
    iget-object v0, p0, Lapx;->v:Lbzx;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lbzx;->a([I)V

    .line 686
    iget-boolean v0, p0, Lapx;->C:Z

    if-eqz v0, :cond_0

    .line 692
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->publishVideoMuteState(Z)V

    .line 694
    :cond_0
    iget-boolean v0, p0, Lapx;->D:Z

    if-eqz v0, :cond_1

    .line 695
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    iget-boolean v1, p0, Lapx;->D:Z

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->setMute(Z)V

    .line 697
    :cond_1
    return-void

    .line 683
    :array_0
    .array-data 4
        0x6
        0x7
    .end array-data
.end method

.method public v()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 701
    iget-boolean v0, p0, Lapx;->y:Z

    if-nez v0, :cond_0

    .line 702
    iget-object v0, p0, Lapx;->v:Lbzx;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Lbzx;->a([I)V

    .line 703
    iput-boolean v2, p0, Lapx;->y:Z

    .line 705
    :cond_0
    invoke-virtual {p0, v2}, Lapx;->e(Z)V

    .line 706
    return-void

    .line 702
    :array_0
    .array-data 4
        0x8
        0x9
    .end array-data
.end method

.method w()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 869
    iget-boolean v1, p0, Lapx;->m:Z

    if-eqz v1, :cond_1

    .line 874
    :cond_0
    :goto_0
    return v0

    .line 873
    :cond_1
    invoke-virtual {p0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v1

    .line 874
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->isInProgress()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method x()V
    .locals 1

    .prologue
    .line 878
    invoke-direct {p0}, Lapx;->Y()Larw;

    move-result-object v0

    invoke-virtual {v0}, Larw;->f()V

    .line 879
    return-void
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapx;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
