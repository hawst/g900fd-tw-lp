.class public final Leow;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leow;


# instance fields
.field public b:[Leot;

.field public c:[Leoy;

.field public d:[Leov;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 684
    const/4 v0, 0x0

    new-array v0, v0, [Leow;

    sput-object v0, Leow;->a:[Leow;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 685
    invoke-direct {p0}, Lepn;-><init>()V

    .line 688
    sget-object v0, Leot;->a:[Leot;

    iput-object v0, p0, Leow;->b:[Leot;

    .line 691
    sget-object v0, Leoy;->a:[Leoy;

    iput-object v0, p0, Leow;->c:[Leoy;

    .line 694
    sget-object v0, Leov;->a:[Leov;

    iput-object v0, p0, Leow;->d:[Leov;

    .line 685
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 727
    iget-object v0, p0, Leow;->b:[Leot;

    if-eqz v0, :cond_1

    .line 728
    iget-object v3, p0, Leow;->b:[Leot;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 729
    if-eqz v5, :cond_0

    .line 730
    const/4 v6, 0x1

    .line 731
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 728
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 735
    :cond_2
    iget-object v2, p0, Leow;->c:[Leoy;

    if-eqz v2, :cond_4

    .line 736
    iget-object v3, p0, Leow;->c:[Leoy;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 737
    if-eqz v5, :cond_3

    .line 738
    const/4 v6, 0x2

    .line 739
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 736
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 743
    :cond_4
    iget-object v2, p0, Leow;->d:[Leov;

    if-eqz v2, :cond_6

    .line 744
    iget-object v2, p0, Leow;->d:[Leov;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 745
    if-eqz v4, :cond_5

    .line 746
    const/4 v5, 0x3

    .line 747
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 744
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 751
    :cond_6
    iget-object v1, p0, Leow;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 752
    iput v0, p0, Leow;->cachedSize:I

    .line 753
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 681
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leow;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leow;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leow;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leow;->b:[Leot;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leot;

    iget-object v3, p0, Leow;->b:[Leot;

    if-eqz v3, :cond_2

    iget-object v3, p0, Leow;->b:[Leot;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Leow;->b:[Leot;

    :goto_2
    iget-object v2, p0, Leow;->b:[Leot;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leow;->b:[Leot;

    new-instance v3, Leot;

    invoke-direct {v3}, Leot;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leow;->b:[Leot;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leow;->b:[Leot;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Leow;->b:[Leot;

    new-instance v3, Leot;

    invoke-direct {v3}, Leot;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leow;->b:[Leot;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leow;->c:[Leoy;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leoy;

    iget-object v3, p0, Leow;->c:[Leoy;

    if-eqz v3, :cond_5

    iget-object v3, p0, Leow;->c:[Leoy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Leow;->c:[Leoy;

    :goto_4
    iget-object v2, p0, Leow;->c:[Leoy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Leow;->c:[Leoy;

    new-instance v3, Leoy;

    invoke-direct {v3}, Leoy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leow;->c:[Leoy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Leow;->c:[Leoy;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Leow;->c:[Leoy;

    new-instance v3, Leoy;

    invoke-direct {v3}, Leoy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leow;->c:[Leoy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leow;->d:[Leov;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Leov;

    iget-object v3, p0, Leow;->d:[Leov;

    if-eqz v3, :cond_8

    iget-object v3, p0, Leow;->d:[Leov;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Leow;->d:[Leov;

    :goto_6
    iget-object v2, p0, Leow;->d:[Leov;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Leow;->d:[Leov;

    new-instance v3, Leov;

    invoke-direct {v3}, Leov;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leow;->d:[Leov;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Leow;->d:[Leov;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Leow;->d:[Leov;

    new-instance v3, Leov;

    invoke-direct {v3}, Leov;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leow;->d:[Leov;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 699
    iget-object v1, p0, Leow;->b:[Leot;

    if-eqz v1, :cond_1

    .line 700
    iget-object v2, p0, Leow;->b:[Leot;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 701
    if-eqz v4, :cond_0

    .line 702
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 700
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 706
    :cond_1
    iget-object v1, p0, Leow;->c:[Leoy;

    if-eqz v1, :cond_3

    .line 707
    iget-object v2, p0, Leow;->c:[Leoy;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 708
    if-eqz v4, :cond_2

    .line 709
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 707
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 713
    :cond_3
    iget-object v1, p0, Leow;->d:[Leov;

    if-eqz v1, :cond_5

    .line 714
    iget-object v1, p0, Leow;->d:[Leov;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 715
    if-eqz v3, :cond_4

    .line 716
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 714
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 720
    :cond_5
    iget-object v0, p0, Leow;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 722
    return-void
.end method
