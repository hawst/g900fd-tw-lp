.class public final Ldgw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgw;


# instance fields
.field public A:Ljava/lang/Integer;

.field public B:Ljava/lang/Boolean;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:[Ljava/lang/String;

.field public F:Ljava/lang/Integer;

.field public G:Ljava/lang/Integer;

.field public H:[Lera;

.field public I:[Ldhw;

.field public J:Ljava/lang/Long;

.field public K:Ljava/lang/Long;

.field public L:[Ljava/lang/String;

.field public M:Ljava/lang/Boolean;

.field public N:[Ldgx;

.field public O:[Ldht;

.field public P:Ljava/lang/Boolean;

.field public Q:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ldhe;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:[Ldmn;

.field public o:[Ldhe;

.field public p:[Ljava/lang/String;

.field public q:Ldhl;

.field public r:Ljava/lang/Integer;

.field public s:Ljava/lang/Boolean;

.field public t:Ljava/lang/Integer;

.field public u:Ljava/lang/String;

.field public v:[Ldlt;

.field public w:Ljava/lang/Integer;

.field public x:Ljava/lang/Integer;

.field public y:Ljava/lang/Boolean;

.field public z:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2165
    const/4 v0, 0x0

    new-array v0, v0, [Ldgw;

    sput-object v0, Ldgw;->a:[Ldgw;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2166
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2187
    iput-object v1, p0, Ldgw;->b:Ljava/lang/Integer;

    .line 2190
    iput-object v1, p0, Ldgw;->c:Ldhe;

    .line 2205
    iput-object v1, p0, Ldgw;->j:Ljava/lang/Integer;

    .line 2214
    sget-object v0, Ldmn;->a:[Ldmn;

    iput-object v0, p0, Ldgw;->n:[Ldmn;

    .line 2217
    sget-object v0, Ldhe;->a:[Ldhe;

    iput-object v0, p0, Ldgw;->o:[Ldhe;

    .line 2220
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldgw;->p:[Ljava/lang/String;

    .line 2223
    iput-object v1, p0, Ldgw;->q:Ldhl;

    .line 2226
    iput-object v1, p0, Ldgw;->r:Ljava/lang/Integer;

    .line 2231
    iput-object v1, p0, Ldgw;->t:Ljava/lang/Integer;

    .line 2236
    sget-object v0, Ldlt;->a:[Ldlt;

    iput-object v0, p0, Ldgw;->v:[Ldlt;

    .line 2255
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldgw;->E:[Ljava/lang/String;

    .line 2262
    sget-object v0, Lera;->a:[Lera;

    iput-object v0, p0, Ldgw;->H:[Lera;

    .line 2265
    sget-object v0, Ldhw;->a:[Ldhw;

    iput-object v0, p0, Ldgw;->I:[Ldhw;

    .line 2272
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldgw;->L:[Ljava/lang/String;

    .line 2277
    sget-object v0, Ldgx;->a:[Ldgx;

    iput-object v0, p0, Ldgw;->N:[Ldgx;

    .line 2280
    sget-object v0, Ldht;->a:[Ldht;

    iput-object v0, p0, Ldgw;->O:[Ldht;

    .line 2285
    iput-object v1, p0, Ldgw;->Q:Ljava/lang/Integer;

    .line 2166
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2453
    iget-object v0, p0, Ldgw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_31

    .line 2454
    const/4 v0, 0x1

    iget-object v2, p0, Ldgw;->b:Ljava/lang/Integer;

    .line 2455
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2457
    :goto_0
    iget-object v2, p0, Ldgw;->c:Ldhe;

    if-eqz v2, :cond_0

    .line 2458
    const/4 v2, 0x2

    iget-object v3, p0, Ldgw;->c:Ldhe;

    .line 2459
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2461
    :cond_0
    const/4 v2, 0x3

    iget-object v3, p0, Ldgw;->f:Ljava/lang/String;

    .line 2462
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2463
    iget-object v2, p0, Ldgw;->g:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 2464
    const/4 v2, 0x4

    iget-object v3, p0, Ldgw;->g:Ljava/lang/String;

    .line 2465
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2467
    :cond_1
    iget-object v2, p0, Ldgw;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 2468
    const/4 v2, 0x5

    iget-object v3, p0, Ldgw;->h:Ljava/lang/Integer;

    .line 2469
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2471
    :cond_2
    const/4 v2, 0x6

    iget-object v3, p0, Ldgw;->i:Ljava/lang/String;

    .line 2472
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2473
    iget-object v2, p0, Ldgw;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 2474
    const/4 v2, 0x7

    iget-object v3, p0, Ldgw;->j:Ljava/lang/Integer;

    .line 2475
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2477
    :cond_3
    iget-object v2, p0, Ldgw;->k:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 2478
    const/16 v2, 0x8

    iget-object v3, p0, Ldgw;->k:Ljava/lang/String;

    .line 2479
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2481
    :cond_4
    iget-object v2, p0, Ldgw;->l:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 2482
    const/16 v2, 0x9

    iget-object v3, p0, Ldgw;->l:Ljava/lang/String;

    .line 2483
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2485
    :cond_5
    iget-object v2, p0, Ldgw;->m:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 2486
    const/16 v2, 0xa

    iget-object v3, p0, Ldgw;->m:Ljava/lang/String;

    .line 2487
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2489
    :cond_6
    iget-object v2, p0, Ldgw;->o:[Ldhe;

    if-eqz v2, :cond_8

    .line 2490
    iget-object v3, p0, Ldgw;->o:[Ldhe;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_8

    aget-object v5, v3, v2

    .line 2491
    if-eqz v5, :cond_7

    .line 2492
    const/16 v6, 0xb

    .line 2493
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2490
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2497
    :cond_8
    iget-object v2, p0, Ldgw;->n:[Ldmn;

    if-eqz v2, :cond_a

    .line 2498
    iget-object v3, p0, Ldgw;->n:[Ldmn;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    .line 2499
    if-eqz v5, :cond_9

    .line 2500
    const/16 v6, 0xc

    .line 2501
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2498
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2505
    :cond_a
    iget-object v2, p0, Ldgw;->p:[Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Ldgw;->p:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 2507
    iget-object v4, p0, Ldgw;->p:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_3
    if-ge v2, v5, :cond_b

    aget-object v6, v4, v2

    .line 2509
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 2507
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2511
    :cond_b
    add-int/2addr v0, v3

    .line 2512
    iget-object v2, p0, Ldgw;->p:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2514
    :cond_c
    iget-object v2, p0, Ldgw;->q:Ldhl;

    if-eqz v2, :cond_d

    .line 2515
    const/16 v2, 0xe

    iget-object v3, p0, Ldgw;->q:Ldhl;

    .line 2516
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2518
    :cond_d
    iget-object v2, p0, Ldgw;->r:Ljava/lang/Integer;

    if-eqz v2, :cond_e

    .line 2519
    const/16 v2, 0xf

    iget-object v3, p0, Ldgw;->r:Ljava/lang/Integer;

    .line 2520
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2522
    :cond_e
    iget-object v2, p0, Ldgw;->E:[Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, Ldgw;->E:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_10

    .line 2524
    iget-object v4, p0, Ldgw;->E:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_4
    if-ge v2, v5, :cond_f

    aget-object v6, v4, v2

    .line 2526
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 2524
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2528
    :cond_f
    add-int/2addr v0, v3

    .line 2529
    iget-object v2, p0, Ldgw;->E:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 2531
    :cond_10
    iget-object v2, p0, Ldgw;->t:Ljava/lang/Integer;

    if-eqz v2, :cond_11

    .line 2532
    const/16 v2, 0x11

    iget-object v3, p0, Ldgw;->t:Ljava/lang/Integer;

    .line 2533
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2535
    :cond_11
    iget-object v2, p0, Ldgw;->u:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 2536
    const/16 v2, 0x12

    iget-object v3, p0, Ldgw;->u:Ljava/lang/String;

    .line 2537
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2539
    :cond_12
    iget-object v2, p0, Ldgw;->s:Ljava/lang/Boolean;

    if-eqz v2, :cond_13

    .line 2540
    const/16 v2, 0x13

    iget-object v3, p0, Ldgw;->s:Ljava/lang/Boolean;

    .line 2541
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2543
    :cond_13
    iget-object v2, p0, Ldgw;->v:[Ldlt;

    if-eqz v2, :cond_15

    .line 2544
    iget-object v3, p0, Ldgw;->v:[Ldlt;

    array-length v4, v3

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_15

    aget-object v5, v3, v2

    .line 2545
    if-eqz v5, :cond_14

    .line 2546
    const/16 v6, 0x14

    .line 2547
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2544
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 2551
    :cond_15
    iget-object v2, p0, Ldgw;->w:Ljava/lang/Integer;

    if-eqz v2, :cond_16

    .line 2552
    const/16 v2, 0x15

    iget-object v3, p0, Ldgw;->w:Ljava/lang/Integer;

    .line 2553
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2555
    :cond_16
    iget-object v2, p0, Ldgw;->x:Ljava/lang/Integer;

    if-eqz v2, :cond_17

    .line 2556
    const/16 v2, 0x16

    iget-object v3, p0, Ldgw;->x:Ljava/lang/Integer;

    .line 2557
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2559
    :cond_17
    iget-object v2, p0, Ldgw;->y:Ljava/lang/Boolean;

    if-eqz v2, :cond_18

    .line 2560
    const/16 v2, 0x17

    iget-object v3, p0, Ldgw;->y:Ljava/lang/Boolean;

    .line 2561
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2563
    :cond_18
    iget-object v2, p0, Ldgw;->z:Ljava/lang/Integer;

    if-eqz v2, :cond_19

    .line 2564
    const/16 v2, 0x18

    iget-object v3, p0, Ldgw;->z:Ljava/lang/Integer;

    .line 2565
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2567
    :cond_19
    iget-object v2, p0, Ldgw;->A:Ljava/lang/Integer;

    if-eqz v2, :cond_1a

    .line 2568
    const/16 v2, 0x19

    iget-object v3, p0, Ldgw;->A:Ljava/lang/Integer;

    .line 2569
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2571
    :cond_1a
    iget-object v2, p0, Ldgw;->B:Ljava/lang/Boolean;

    if-eqz v2, :cond_1b

    .line 2572
    const/16 v2, 0x1a

    iget-object v3, p0, Ldgw;->B:Ljava/lang/Boolean;

    .line 2573
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2575
    :cond_1b
    iget-object v2, p0, Ldgw;->D:Ljava/lang/String;

    if-eqz v2, :cond_1c

    .line 2576
    const/16 v2, 0x1b

    iget-object v3, p0, Ldgw;->D:Ljava/lang/String;

    .line 2577
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2579
    :cond_1c
    iget-object v2, p0, Ldgw;->F:Ljava/lang/Integer;

    if-eqz v2, :cond_1d

    .line 2580
    const/16 v2, 0x1c

    iget-object v3, p0, Ldgw;->F:Ljava/lang/Integer;

    .line 2581
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2583
    :cond_1d
    iget-object v2, p0, Ldgw;->G:Ljava/lang/Integer;

    if-eqz v2, :cond_1e

    .line 2584
    const/16 v2, 0x1d

    iget-object v3, p0, Ldgw;->G:Ljava/lang/Integer;

    .line 2585
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2587
    :cond_1e
    iget-object v2, p0, Ldgw;->H:[Lera;

    if-eqz v2, :cond_20

    .line 2588
    iget-object v3, p0, Ldgw;->H:[Lera;

    array-length v4, v3

    move v2, v1

    :goto_6
    if-ge v2, v4, :cond_20

    aget-object v5, v3, v2

    .line 2589
    if-eqz v5, :cond_1f

    .line 2590
    const/16 v6, 0x1e

    .line 2591
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2588
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 2595
    :cond_20
    iget-object v2, p0, Ldgw;->C:Ljava/lang/String;

    if-eqz v2, :cond_21

    .line 2596
    const/16 v2, 0x1f

    iget-object v3, p0, Ldgw;->C:Ljava/lang/String;

    .line 2597
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2599
    :cond_21
    iget-object v2, p0, Ldgw;->J:Ljava/lang/Long;

    if-eqz v2, :cond_22

    .line 2600
    const/16 v2, 0x20

    iget-object v3, p0, Ldgw;->J:Ljava/lang/Long;

    .line 2601
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2603
    :cond_22
    iget-object v2, p0, Ldgw;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_23

    .line 2604
    const/16 v2, 0x21

    iget-object v3, p0, Ldgw;->d:Ljava/lang/Boolean;

    .line 2605
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2607
    :cond_23
    iget-object v2, p0, Ldgw;->K:Ljava/lang/Long;

    if-eqz v2, :cond_24

    .line 2608
    const/16 v2, 0x22

    iget-object v3, p0, Ldgw;->K:Ljava/lang/Long;

    .line 2609
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2611
    :cond_24
    iget-object v2, p0, Ldgw;->L:[Ljava/lang/String;

    if-eqz v2, :cond_26

    iget-object v2, p0, Ldgw;->L:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_26

    .line 2613
    iget-object v4, p0, Ldgw;->L:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_7
    if-ge v2, v5, :cond_25

    aget-object v6, v4, v2

    .line 2615
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 2613
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 2617
    :cond_25
    add-int/2addr v0, v3

    .line 2618
    iget-object v2, p0, Ldgw;->L:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 2620
    :cond_26
    iget-object v2, p0, Ldgw;->I:[Ldhw;

    if-eqz v2, :cond_28

    .line 2621
    iget-object v3, p0, Ldgw;->I:[Ldhw;

    array-length v4, v3

    move v2, v1

    :goto_8
    if-ge v2, v4, :cond_28

    aget-object v5, v3, v2

    .line 2622
    if-eqz v5, :cond_27

    .line 2623
    const/16 v6, 0x24

    .line 2624
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2621
    :cond_27
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 2628
    :cond_28
    iget-object v2, p0, Ldgw;->M:Ljava/lang/Boolean;

    if-eqz v2, :cond_29

    .line 2629
    const/16 v2, 0x25

    iget-object v3, p0, Ldgw;->M:Ljava/lang/Boolean;

    .line 2630
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2632
    :cond_29
    iget-object v2, p0, Ldgw;->N:[Ldgx;

    if-eqz v2, :cond_2b

    .line 2633
    iget-object v3, p0, Ldgw;->N:[Ldgx;

    array-length v4, v3

    move v2, v1

    :goto_9
    if-ge v2, v4, :cond_2b

    aget-object v5, v3, v2

    .line 2634
    if-eqz v5, :cond_2a

    .line 2635
    const/16 v6, 0x26

    .line 2636
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2633
    :cond_2a
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 2640
    :cond_2b
    iget-object v2, p0, Ldgw;->O:[Ldht;

    if-eqz v2, :cond_2d

    .line 2641
    iget-object v2, p0, Ldgw;->O:[Ldht;

    array-length v3, v2

    :goto_a
    if-ge v1, v3, :cond_2d

    aget-object v4, v2, v1

    .line 2642
    if-eqz v4, :cond_2c

    .line 2643
    const/16 v5, 0x27

    .line 2644
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2641
    :cond_2c
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 2648
    :cond_2d
    iget-object v1, p0, Ldgw;->e:Ljava/lang/String;

    if-eqz v1, :cond_2e

    .line 2649
    const/16 v1, 0x28

    iget-object v2, p0, Ldgw;->e:Ljava/lang/String;

    .line 2650
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2652
    :cond_2e
    iget-object v1, p0, Ldgw;->P:Ljava/lang/Boolean;

    if-eqz v1, :cond_2f

    .line 2653
    const/16 v1, 0x29

    iget-object v2, p0, Ldgw;->P:Ljava/lang/Boolean;

    .line 2654
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2656
    :cond_2f
    iget-object v1, p0, Ldgw;->Q:Ljava/lang/Integer;

    if-eqz v1, :cond_30

    .line 2657
    const/16 v1, 0x2a

    iget-object v2, p0, Ldgw;->Q:Ljava/lang/Integer;

    .line 2658
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2660
    :cond_30
    iget-object v1, p0, Ldgw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2661
    iput v0, p0, Ldgw;->cachedSize:I

    .line 2662
    return v0

    :cond_31
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2162
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldgw;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldgw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldgw;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-ne v0, v6, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldgw;->c:Ldhe;

    if-nez v0, :cond_4

    new-instance v0, Ldhe;

    invoke-direct {v0}, Ldhe;-><init>()V

    iput-object v0, p0, Ldgw;->c:Ldhe;

    :cond_4
    iget-object v0, p0, Ldgw;->c:Ldhe;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_5

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_5

    if-eq v0, v6, :cond_5

    if-eq v0, v7, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    const/4 v2, 0x6

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8

    if-eq v0, v2, :cond_5

    const/4 v2, 0x7

    if-ne v0, v2, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->o:[Ldhe;

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldhe;

    iget-object v3, p0, Ldgw;->o:[Ldhe;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldgw;->o:[Ldhe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldgw;->o:[Ldhe;

    :goto_2
    iget-object v2, p0, Ldgw;->o:[Ldhe;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldgw;->o:[Ldhe;

    new-instance v3, Ldhe;

    invoke-direct {v3}, Ldhe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->o:[Ldhe;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Ldgw;->o:[Ldhe;

    array-length v0, v0

    goto :goto_1

    :cond_9
    iget-object v2, p0, Ldgw;->o:[Ldhe;

    new-instance v3, Ldhe;

    invoke-direct {v3}, Ldhe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->o:[Ldhe;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->n:[Ldmn;

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldmn;

    iget-object v3, p0, Ldgw;->n:[Ldmn;

    if-eqz v3, :cond_a

    iget-object v3, p0, Ldgw;->n:[Ldmn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Ldgw;->n:[Ldmn;

    :goto_4
    iget-object v2, p0, Ldgw;->n:[Ldmn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Ldgw;->n:[Ldmn;

    new-instance v3, Ldmn;

    invoke-direct {v3}, Ldmn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->n:[Ldmn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    iget-object v0, p0, Ldgw;->n:[Ldmn;

    array-length v0, v0

    goto :goto_3

    :cond_c
    iget-object v2, p0, Ldgw;->n:[Ldmn;

    new-instance v3, Ldmn;

    invoke-direct {v3}, Ldmn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->n:[Ldmn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->p:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldgw;->p:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldgw;->p:[Ljava/lang/String;

    :goto_5
    iget-object v2, p0, Ldgw;->p:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Ldgw;->p:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_d
    iget-object v2, p0, Ldgw;->p:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Ldgw;->q:Ldhl;

    if-nez v0, :cond_e

    new-instance v0, Ldhl;

    invoke-direct {v0}, Ldhl;-><init>()V

    iput-object v0, p0, Ldgw;->q:Ldhl;

    :cond_e
    iget-object v0, p0, Ldgw;->q:Ldhl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_f

    if-eq v0, v4, :cond_f

    if-eq v0, v5, :cond_f

    if-eq v0, v6, :cond_f

    if-eq v0, v7, :cond_f

    const/4 v2, 0x5

    if-ne v0, v2, :cond_10

    :cond_f
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->r:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_10
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->r:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_10
    const/16 v0, 0x82

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->E:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldgw;->E:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldgw;->E:[Ljava/lang/String;

    :goto_6
    iget-object v2, p0, Ldgw;->E:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    iget-object v2, p0, Ldgw;->E:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_11
    iget-object v2, p0, Ldgw;->E:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_12

    if-eq v0, v5, :cond_12

    const/4 v2, 0x6

    if-eq v0, v2, :cond_12

    if-eq v0, v6, :cond_12

    if-eq v0, v7, :cond_12

    const/4 v2, 0x5

    if-ne v0, v2, :cond_13

    :cond_12
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->t:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_13
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->t:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->u:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgw;->s:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_14
    const/16 v0, 0xa2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->v:[Ldlt;

    if-nez v0, :cond_15

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ldlt;

    iget-object v3, p0, Ldgw;->v:[Ldlt;

    if-eqz v3, :cond_14

    iget-object v3, p0, Ldgw;->v:[Ldlt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_14
    iput-object v2, p0, Ldgw;->v:[Ldlt;

    :goto_8
    iget-object v2, p0, Ldgw;->v:[Ldlt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_16

    iget-object v2, p0, Ldgw;->v:[Ldlt;

    new-instance v3, Ldlt;

    invoke-direct {v3}, Ldlt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->v:[Ldlt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_15
    iget-object v0, p0, Ldgw;->v:[Ldlt;

    array-length v0, v0

    goto :goto_7

    :cond_16
    iget-object v2, p0, Ldgw;->v:[Ldlt;

    new-instance v3, Ldlt;

    invoke-direct {v3}, Ldlt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->v:[Ldlt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->w:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->x:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgw;->y:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->z:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->A:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgw;->B:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->D:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->F:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->G:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1e
    const/16 v0, 0xf2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->H:[Lera;

    if-nez v0, :cond_18

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lera;

    iget-object v3, p0, Ldgw;->H:[Lera;

    if-eqz v3, :cond_17

    iget-object v3, p0, Ldgw;->H:[Lera;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_17
    iput-object v2, p0, Ldgw;->H:[Lera;

    :goto_a
    iget-object v2, p0, Ldgw;->H:[Lera;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_19

    iget-object v2, p0, Ldgw;->H:[Lera;

    new-instance v3, Lera;

    invoke-direct {v3}, Lera;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->H:[Lera;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_18
    iget-object v0, p0, Ldgw;->H:[Lera;

    array-length v0, v0

    goto :goto_9

    :cond_19
    iget-object v2, p0, Ldgw;->H:[Lera;

    new-instance v3, Lera;

    invoke-direct {v3}, Lera;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->H:[Lera;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->C:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_20
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgw;->J:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_21
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgw;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_22
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgw;->K:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_23
    const/16 v0, 0x11a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->L:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldgw;->L:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldgw;->L:[Ljava/lang/String;

    :goto_b
    iget-object v2, p0, Ldgw;->L:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1a

    iget-object v2, p0, Ldgw;->L:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_1a
    iget-object v2, p0, Ldgw;->L:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_24
    const/16 v0, 0x122

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->I:[Ldhw;

    if-nez v0, :cond_1c

    move v0, v1

    :goto_c
    add-int/2addr v2, v0

    new-array v2, v2, [Ldhw;

    iget-object v3, p0, Ldgw;->I:[Ldhw;

    if-eqz v3, :cond_1b

    iget-object v3, p0, Ldgw;->I:[Ldhw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1b
    iput-object v2, p0, Ldgw;->I:[Ldhw;

    :goto_d
    iget-object v2, p0, Ldgw;->I:[Ldhw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1d

    iget-object v2, p0, Ldgw;->I:[Ldhw;

    new-instance v3, Ldhw;

    invoke-direct {v3}, Ldhw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->I:[Ldhw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_1c
    iget-object v0, p0, Ldgw;->I:[Ldhw;

    array-length v0, v0

    goto :goto_c

    :cond_1d
    iget-object v2, p0, Ldgw;->I:[Ldhw;

    new-instance v3, Ldhw;

    invoke-direct {v3}, Ldhw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->I:[Ldhw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_25
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgw;->M:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_26
    const/16 v0, 0x132

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->N:[Ldgx;

    if-nez v0, :cond_1f

    move v0, v1

    :goto_e
    add-int/2addr v2, v0

    new-array v2, v2, [Ldgx;

    iget-object v3, p0, Ldgw;->N:[Ldgx;

    if-eqz v3, :cond_1e

    iget-object v3, p0, Ldgw;->N:[Ldgx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1e
    iput-object v2, p0, Ldgw;->N:[Ldgx;

    :goto_f
    iget-object v2, p0, Ldgw;->N:[Ldgx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_20

    iget-object v2, p0, Ldgw;->N:[Ldgx;

    new-instance v3, Ldgx;

    invoke-direct {v3}, Ldgx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->N:[Ldgx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_1f
    iget-object v0, p0, Ldgw;->N:[Ldgx;

    array-length v0, v0

    goto :goto_e

    :cond_20
    iget-object v2, p0, Ldgw;->N:[Ldgx;

    new-instance v3, Ldgx;

    invoke-direct {v3}, Ldgx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->N:[Ldgx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_27
    const/16 v0, 0x13a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgw;->O:[Ldht;

    if-nez v0, :cond_22

    move v0, v1

    :goto_10
    add-int/2addr v2, v0

    new-array v2, v2, [Ldht;

    iget-object v3, p0, Ldgw;->O:[Ldht;

    if-eqz v3, :cond_21

    iget-object v3, p0, Ldgw;->O:[Ldht;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_21
    iput-object v2, p0, Ldgw;->O:[Ldht;

    :goto_11
    iget-object v2, p0, Ldgw;->O:[Ldht;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_23

    iget-object v2, p0, Ldgw;->O:[Ldht;

    new-instance v3, Ldht;

    invoke-direct {v3}, Ldht;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->O:[Ldht;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_22
    iget-object v0, p0, Ldgw;->O:[Ldht;

    array-length v0, v0

    goto :goto_10

    :cond_23
    iget-object v2, p0, Ldgw;->O:[Ldht;

    new-instance v3, Ldht;

    invoke-direct {v3}, Ldht;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgw;->O:[Ldht;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_28
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgw;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_29
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgw;->P:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_2a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_24

    if-eq v0, v4, :cond_24

    if-eq v0, v5, :cond_24

    if-ne v0, v6, :cond_25

    :cond_24
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->Q:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgw;->Q:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe0 -> :sswitch_1c
        0xe8 -> :sswitch_1d
        0xf2 -> :sswitch_1e
        0xfa -> :sswitch_1f
        0x100 -> :sswitch_20
        0x108 -> :sswitch_21
        0x110 -> :sswitch_22
        0x11a -> :sswitch_23
        0x122 -> :sswitch_24
        0x128 -> :sswitch_25
        0x132 -> :sswitch_26
        0x13a -> :sswitch_27
        0x142 -> :sswitch_28
        0x148 -> :sswitch_29
        0x150 -> :sswitch_2a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2290
    iget-object v1, p0, Ldgw;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2291
    const/4 v1, 0x1

    iget-object v2, p0, Ldgw;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2293
    :cond_0
    iget-object v1, p0, Ldgw;->c:Ldhe;

    if-eqz v1, :cond_1

    .line 2294
    const/4 v1, 0x2

    iget-object v2, p0, Ldgw;->c:Ldhe;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2296
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Ldgw;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2297
    iget-object v1, p0, Ldgw;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2298
    const/4 v1, 0x4

    iget-object v2, p0, Ldgw;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2300
    :cond_2
    iget-object v1, p0, Ldgw;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2301
    const/4 v1, 0x5

    iget-object v2, p0, Ldgw;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2303
    :cond_3
    const/4 v1, 0x6

    iget-object v2, p0, Ldgw;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2304
    iget-object v1, p0, Ldgw;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2305
    const/4 v1, 0x7

    iget-object v2, p0, Ldgw;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2307
    :cond_4
    iget-object v1, p0, Ldgw;->k:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 2308
    const/16 v1, 0x8

    iget-object v2, p0, Ldgw;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2310
    :cond_5
    iget-object v1, p0, Ldgw;->l:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 2311
    const/16 v1, 0x9

    iget-object v2, p0, Ldgw;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2313
    :cond_6
    iget-object v1, p0, Ldgw;->m:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 2314
    const/16 v1, 0xa

    iget-object v2, p0, Ldgw;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2316
    :cond_7
    iget-object v1, p0, Ldgw;->o:[Ldhe;

    if-eqz v1, :cond_9

    .line 2317
    iget-object v2, p0, Ldgw;->o:[Ldhe;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 2318
    if-eqz v4, :cond_8

    .line 2319
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2317
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2323
    :cond_9
    iget-object v1, p0, Ldgw;->n:[Ldmn;

    if-eqz v1, :cond_b

    .line 2324
    iget-object v2, p0, Ldgw;->n:[Ldmn;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 2325
    if-eqz v4, :cond_a

    .line 2326
    const/16 v5, 0xc

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2324
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2330
    :cond_b
    iget-object v1, p0, Ldgw;->p:[Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 2331
    iget-object v2, p0, Ldgw;->p:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 2332
    const/16 v5, 0xd

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 2331
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2335
    :cond_c
    iget-object v1, p0, Ldgw;->q:Ldhl;

    if-eqz v1, :cond_d

    .line 2336
    const/16 v1, 0xe

    iget-object v2, p0, Ldgw;->q:Ldhl;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2338
    :cond_d
    iget-object v1, p0, Ldgw;->r:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 2339
    const/16 v1, 0xf

    iget-object v2, p0, Ldgw;->r:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2341
    :cond_e
    iget-object v1, p0, Ldgw;->E:[Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 2342
    iget-object v2, p0, Ldgw;->E:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_f

    aget-object v4, v2, v1

    .line 2343
    const/16 v5, 0x10

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 2342
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2346
    :cond_f
    iget-object v1, p0, Ldgw;->t:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 2347
    const/16 v1, 0x11

    iget-object v2, p0, Ldgw;->t:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2349
    :cond_10
    iget-object v1, p0, Ldgw;->u:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 2350
    const/16 v1, 0x12

    iget-object v2, p0, Ldgw;->u:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2352
    :cond_11
    iget-object v1, p0, Ldgw;->s:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    .line 2353
    const/16 v1, 0x13

    iget-object v2, p0, Ldgw;->s:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 2355
    :cond_12
    iget-object v1, p0, Ldgw;->v:[Ldlt;

    if-eqz v1, :cond_14

    .line 2356
    iget-object v2, p0, Ldgw;->v:[Ldlt;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_14

    aget-object v4, v2, v1

    .line 2357
    if-eqz v4, :cond_13

    .line 2358
    const/16 v5, 0x14

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2356
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2362
    :cond_14
    iget-object v1, p0, Ldgw;->w:Ljava/lang/Integer;

    if-eqz v1, :cond_15

    .line 2363
    const/16 v1, 0x15

    iget-object v2, p0, Ldgw;->w:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2365
    :cond_15
    iget-object v1, p0, Ldgw;->x:Ljava/lang/Integer;

    if-eqz v1, :cond_16

    .line 2366
    const/16 v1, 0x16

    iget-object v2, p0, Ldgw;->x:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2368
    :cond_16
    iget-object v1, p0, Ldgw;->y:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    .line 2369
    const/16 v1, 0x17

    iget-object v2, p0, Ldgw;->y:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 2371
    :cond_17
    iget-object v1, p0, Ldgw;->z:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    .line 2372
    const/16 v1, 0x18

    iget-object v2, p0, Ldgw;->z:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2374
    :cond_18
    iget-object v1, p0, Ldgw;->A:Ljava/lang/Integer;

    if-eqz v1, :cond_19

    .line 2375
    const/16 v1, 0x19

    iget-object v2, p0, Ldgw;->A:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2377
    :cond_19
    iget-object v1, p0, Ldgw;->B:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    .line 2378
    const/16 v1, 0x1a

    iget-object v2, p0, Ldgw;->B:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 2380
    :cond_1a
    iget-object v1, p0, Ldgw;->D:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 2381
    const/16 v1, 0x1b

    iget-object v2, p0, Ldgw;->D:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2383
    :cond_1b
    iget-object v1, p0, Ldgw;->F:Ljava/lang/Integer;

    if-eqz v1, :cond_1c

    .line 2384
    const/16 v1, 0x1c

    iget-object v2, p0, Ldgw;->F:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2386
    :cond_1c
    iget-object v1, p0, Ldgw;->G:Ljava/lang/Integer;

    if-eqz v1, :cond_1d

    .line 2387
    const/16 v1, 0x1d

    iget-object v2, p0, Ldgw;->G:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2389
    :cond_1d
    iget-object v1, p0, Ldgw;->H:[Lera;

    if-eqz v1, :cond_1f

    .line 2390
    iget-object v2, p0, Ldgw;->H:[Lera;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_1f

    aget-object v4, v2, v1

    .line 2391
    if-eqz v4, :cond_1e

    .line 2392
    const/16 v5, 0x1e

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2390
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 2396
    :cond_1f
    iget-object v1, p0, Ldgw;->C:Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 2397
    const/16 v1, 0x1f

    iget-object v2, p0, Ldgw;->C:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2399
    :cond_20
    iget-object v1, p0, Ldgw;->J:Ljava/lang/Long;

    if-eqz v1, :cond_21

    .line 2400
    const/16 v1, 0x20

    iget-object v2, p0, Ldgw;->J:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 2402
    :cond_21
    iget-object v1, p0, Ldgw;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    .line 2403
    const/16 v1, 0x21

    iget-object v2, p0, Ldgw;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 2405
    :cond_22
    iget-object v1, p0, Ldgw;->K:Ljava/lang/Long;

    if-eqz v1, :cond_23

    .line 2406
    const/16 v1, 0x22

    iget-object v2, p0, Ldgw;->K:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 2408
    :cond_23
    iget-object v1, p0, Ldgw;->L:[Ljava/lang/String;

    if-eqz v1, :cond_24

    .line 2409
    iget-object v2, p0, Ldgw;->L:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_24

    aget-object v4, v2, v1

    .line 2410
    const/16 v5, 0x23

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 2409
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2413
    :cond_24
    iget-object v1, p0, Ldgw;->I:[Ldhw;

    if-eqz v1, :cond_26

    .line 2414
    iget-object v2, p0, Ldgw;->I:[Ldhw;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_26

    aget-object v4, v2, v1

    .line 2415
    if-eqz v4, :cond_25

    .line 2416
    const/16 v5, 0x24

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2414
    :cond_25
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 2420
    :cond_26
    iget-object v1, p0, Ldgw;->M:Ljava/lang/Boolean;

    if-eqz v1, :cond_27

    .line 2421
    const/16 v1, 0x25

    iget-object v2, p0, Ldgw;->M:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 2423
    :cond_27
    iget-object v1, p0, Ldgw;->N:[Ldgx;

    if-eqz v1, :cond_29

    .line 2424
    iget-object v2, p0, Ldgw;->N:[Ldgx;

    array-length v3, v2

    move v1, v0

    :goto_8
    if-ge v1, v3, :cond_29

    aget-object v4, v2, v1

    .line 2425
    if-eqz v4, :cond_28

    .line 2426
    const/16 v5, 0x26

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2424
    :cond_28
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 2430
    :cond_29
    iget-object v1, p0, Ldgw;->O:[Ldht;

    if-eqz v1, :cond_2b

    .line 2431
    iget-object v1, p0, Ldgw;->O:[Ldht;

    array-length v2, v1

    :goto_9
    if-ge v0, v2, :cond_2b

    aget-object v3, v1, v0

    .line 2432
    if-eqz v3, :cond_2a

    .line 2433
    const/16 v4, 0x27

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2431
    :cond_2a
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 2437
    :cond_2b
    iget-object v0, p0, Ldgw;->e:Ljava/lang/String;

    if-eqz v0, :cond_2c

    .line 2438
    const/16 v0, 0x28

    iget-object v1, p0, Ldgw;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2440
    :cond_2c
    iget-object v0, p0, Ldgw;->P:Ljava/lang/Boolean;

    if-eqz v0, :cond_2d

    .line 2441
    const/16 v0, 0x29

    iget-object v1, p0, Ldgw;->P:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 2443
    :cond_2d
    iget-object v0, p0, Ldgw;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_2e

    .line 2444
    const/16 v0, 0x2a

    iget-object v1, p0, Ldgw;->Q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2446
    :cond_2e
    iget-object v0, p0, Ldgw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2448
    return-void
.end method
