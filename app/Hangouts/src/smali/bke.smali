.class public final Lbke;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected a:I

.field b:Lbke;

.field private final c:Lyj;

.field private d:Lbki;

.field private e:Ljava/lang/Exception;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lyj;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778
    const/16 v0, 0x64

    iput v0, p0, Lbke;->a:I

    .line 1779
    iput-object v2, p0, Lbke;->e:Ljava/lang/Exception;

    .line 1780
    iput-boolean v1, p0, Lbke;->f:Z

    .line 1781
    iput-boolean v1, p0, Lbke;->g:Z

    .line 1782
    iput-boolean v1, p0, Lbke;->h:Z

    .line 1783
    iput-boolean v1, p0, Lbke;->i:Z

    .line 1784
    iput-boolean v1, p0, Lbke;->j:Z

    .line 1785
    iput-boolean v1, p0, Lbke;->k:Z

    .line 1786
    iput-object v2, p0, Lbke;->b:Lbke;

    .line 1789
    iput-object p1, p0, Lbke;->c:Lyj;

    .line 1790
    new-instance v0, Lbki;

    iget-object v1, p0, Lbke;->c:Lyj;

    invoke-direct {v0, v1}, Lbki;-><init>(Lyj;)V

    iput-object v0, p0, Lbke;->d:Lbki;

    .line 1791
    return-void
.end method

.method private a(ILjava/lang/Exception;Z)Z
    .locals 5

    .prologue
    const/16 v4, 0x66

    const/4 v0, 0x0

    .line 1921
    monitor-enter p0

    .line 1922
    if-gt p1, v4, :cond_0

    .line 1923
    :try_start_0
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "accountState should be an error state:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 1924
    monitor-exit p0

    .line 1956
    :goto_0
    return v0

    .line 1927
    :cond_0
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Account setup failed with error state:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " account:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1928
    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1927
    invoke-static {v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1930
    const/16 v1, 0x68

    if-ne p1, v1, :cond_1

    .line 1931
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbke;->f:Z

    .line 1934
    invoke-virtual {p0}, Lbke;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1935
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1957
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1937
    :cond_1
    if-eqz p3, :cond_3

    .line 1938
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lbke;->g:Z

    .line 1939
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbke;->i:Z

    .line 1946
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lbke;->h()I

    move-result v1

    if-lt v1, v4, :cond_4

    .line 1949
    const/16 v1, 0x6a

    if-eq p1, v1, :cond_4

    .line 1950
    monitor-exit p0

    goto :goto_0

    .line 1941
    :cond_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbke;->h:Z

    .line 1942
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbke;->j:Z

    goto :goto_1

    .line 1954
    :cond_4
    iput p1, p0, Lbke;->a:I

    .line 1955
    iput-object p2, p0, Lbke;->e:Ljava/lang/Exception;

    .line 1956
    const/4 v0, 0x1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method static synthetic b(Lbke;)V
    .locals 8

    .prologue
    .line 1775
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not valid for babel. Skip device registration renew."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lbke;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lbke;->i:Z

    if-eqz v0, :cond_2

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lym;->n(Lyj;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v4, v0, v2

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    if-gtz v4, :cond_3

    sub-long v0, v2, v0

    invoke-static {}, Lbkb;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    :cond_3
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Renewing account registration:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lbke;->c:Lyj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbke;->i:Z

    :cond_5
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1799
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbdk;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Map;Lyk;Z[Ljava/lang/String;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbdk;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbdm;",
            ">;",
            "Lyk;",
            "Z[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2371
    monitor-enter p0

    .line 2372
    :try_start_0
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Retrieved account setting:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2374
    iget-object v1, p0, Lbke;->c:Lyj;

    invoke-virtual {v1}, Lyj;->g()Z

    move-result v11

    .line 2377
    iget-object v1, p0, Lbke;->c:Lyj;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v1 .. v10}, Lym;->a(Lyj;Lbdk;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Map;Lyk;Z[Ljava/lang/String;)V

    .line 2381
    if-eqz p1, :cond_0

    .line 2382
    iget-object v1, p0, Lbke;->c:Lyj;

    .line 2383
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2382
    invoke-static {v1, v2, v3}, Lym;->b(Lyj;J)V

    .line 2386
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbke;->h:Z

    .line 2387
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbke;->j:Z

    .line 2389
    if-nez v11, :cond_1

    iget-object v1, p0, Lbke;->c:Lyj;

    invoke-virtual {v1}, Lyj;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2391
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lbke;->a(Z)V

    .line 2394
    :cond_1
    if-eqz p9, :cond_2

    move-object/from16 v0, p9

    array-length v1, v0

    if-lez v1, :cond_2

    .line 2397
    move-object/from16 v0, p9

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, p9, v1

    .line 2398
    invoke-static {v3}, Lbkb;->d(Ljava/lang/String;)Lbke;

    move-result-object v3

    .line 2399
    invoke-virtual {v3, p0}, Lbke;->a(Lbke;)V

    .line 2400
    invoke-virtual {v3}, Lbke;->g()Lyj;

    move-result-object v4

    invoke-static {v4}, Lym;->m(Lyj;)V

    .line 2401
    invoke-virtual {v3}, Lbke;->s()V

    .line 2397
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2404
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public a(Lbke;)V
    .locals 1

    .prologue
    .line 1815
    monitor-enter p0

    .line 1816
    :try_start_0
    iput-object p1, p0, Lbke;->b:Lbke;

    .line 1817
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2034
    monitor-enter p0

    .line 2035
    :try_start_0
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account registration complete:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2037
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0, p1}, Lym;->b(Lyj;Ljava/lang/String;)V

    .line 2038
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2039
    iget-object v0, p0, Lbke;->c:Lyj;

    .line 2040
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 2039
    invoke-static {v0, v1, v2}, Lym;->a(Lyj;J)V

    .line 2042
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->g:Z

    .line 2043
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->i:Z

    .line 2044
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 2055
    monitor-enter p0

    .line 2056
    :try_start_0
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2057
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "register:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2059
    :cond_0
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2060
    const-string v0, "Babel"

    const-string v1, "Account not valid for babel. Skip device registration."

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2061
    monitor-exit p0

    .line 2096
    :goto_0
    return-void

    .line 2064
    :cond_1
    invoke-virtual {p0}, Lbke;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2065
    const-string v0, "Babel"

    const-string v1, "Account already registered. Skip device registration."

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2066
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2096
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2069
    :cond_2
    :try_start_1
    invoke-static {}, Lbkb;->E()Lbkd;

    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2078
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "register - retrying gcm registration:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2079
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbke;->f:Z

    .line 2080
    invoke-static {}, Lbkb;->E()Lbkd;

    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->i()V

    .line 2081
    monitor-exit p0

    goto :goto_0

    .line 2084
    :cond_3
    invoke-static {}, Lbkb;->E()Lbkd;

    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->d()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2085
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbke;->f:Z

    .line 2086
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account registration pending Gcm:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096
    :cond_4
    :goto_1
    monitor-exit p0

    goto :goto_0

    .line 2087
    :cond_5
    iget-boolean v0, p0, Lbke;->g:Z

    if-eqz v0, :cond_6

    if-eqz p1, :cond_4

    :cond_6
    iget-object v0, p0, Lbke;->c:Lyj;

    .line 2088
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2089
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting account registration:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091
    iget-object v0, p0, Lbke;->c:Lyj;

    iget-boolean v1, p0, Lbke;->k:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Z)V

    .line 2093
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbke;->g:Z

    .line 2094
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public a(ZZ)V
    .locals 3

    .prologue
    .line 2222
    monitor-enter p0

    .line 2223
    :try_start_0
    invoke-static {}, Lbkb;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2224
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setLoggedOff logoff: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2226
    :cond_0
    if-eqz p1, :cond_2

    .line 2227
    iget-object v0, p0, Lbke;->c:Lyj;

    const/4 v1, 0x1

    invoke-static {v0, v1, p2}, Lym;->a(Lyj;ZZ)V

    .line 2229
    invoke-virtual {p0}, Lbke;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2230
    invoke-virtual {p0}, Lbke;->p()V

    .line 2236
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lbke;->j()V

    .line 2237
    monitor-exit p0

    return-void

    .line 2233
    :cond_2
    iget-object v0, p0, Lbke;->c:Lyj;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lym;->a(Lyj;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Exception;Z)Z
    .locals 4

    .prologue
    const/16 v1, 0x6a

    const/16 v2, 0x69

    .line 1874
    if-eqz p1, :cond_2

    .line 1875
    instance-of v0, p1, Lbph;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1876
    check-cast v0, Lbph;

    .line 1877
    invoke-virtual {v0}, Lbph;->e()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move v0, v2

    :goto_0
    move v1, v0

    .line 1906
    :cond_0
    :goto_1
    invoke-direct {p0, v1, p1, p2}, Lbke;->a(ILjava/lang/Exception;Z)Z

    move-result v0

    return v0

    .line 1882
    :pswitch_1
    invoke-virtual {v0}, Lbph;->d()Lbxl;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1883
    invoke-virtual {v0}, Lbph;->d()Lbxl;

    move-result-object p1

    goto :goto_1

    .line 1887
    :pswitch_2
    const/16 v1, 0x6c

    .line 1888
    goto :goto_1

    .line 1890
    :pswitch_3
    const/16 v1, 0x67

    .line 1891
    goto :goto_1

    .line 1896
    :pswitch_4
    const/16 v1, 0x6e

    .line 1897
    goto :goto_1

    .line 1902
    :cond_1
    instance-of v0, p1, Lbxl;

    if-nez v0, :cond_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 1877
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 2275
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbke;->b(ZZ)V

    .line 2276
    return-void
.end method

.method public b(ZZ)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2285
    monitor-enter p0

    .line 2286
    :try_start_0
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting up account:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " clean: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " fromBackground: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2288
    if-eqz p1, :cond_1

    .line 2291
    iget-object v1, p0, Lbke;->c:Lyj;

    invoke-virtual {v1}, Lyj;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2292
    iget-object v1, p0, Lbke;->c:Lyj;

    invoke-static {v1}, Lym;->i(Lyj;)V

    .line 2295
    :cond_0
    invoke-virtual {p0}, Lbke;->j()V

    .line 2298
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lbke;->h()I

    move-result v1

    const/16 v2, 0x65

    if-ne v1, v2, :cond_2

    .line 2299
    const-string v0, "Babel"

    const-string v1, "Skip setting up account in background since there is already an account setup pending"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2301
    monitor-exit p0

    .line 2320
    :goto_0
    return-void

    .line 2304
    :cond_2
    invoke-static {}, Lbkb;->F()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2305
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting up account "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " fromBackground="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2309
    :cond_3
    if-nez p2, :cond_7

    iget-boolean v1, p0, Lbke;->k:Z

    if-eqz v1, :cond_7

    .line 2310
    :goto_1
    if-eqz v0, :cond_4

    .line 2311
    const-string v1, "Babel"

    const-string v2, "Setting up account from backgound in progress. Will resend requests"

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2315
    :cond_4
    iput-boolean p2, p0, Lbke;->k:Z

    .line 2318
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "retrieveAccountSetting resendRequests: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mPendingAccountSetting: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lbke;->h:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lbke;->c:Lyj;

    invoke-virtual {v1}, Lyj;->g()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lbke;->c:Lyj;

    invoke-virtual {v1}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_6
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2319
    :goto_2
    :try_start_2
    invoke-virtual {p0, v0}, Lbke;->a(Z)V

    .line 2320
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2309
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 2318
    :cond_8
    :try_start_3
    iget-boolean v1, p0, Lbke;->h:Z

    if-eqz v1, :cond_9

    if-eqz v0, :cond_a

    :cond_9
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Start retrieving account setting:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbke;->c:Lyj;

    iget-boolean v2, p0, Lbke;->k:Z

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Z)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lbke;->h:Z

    :cond_a
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1803
    monitor-enter p0

    .line 1804
    :try_start_0
    iget-object v0, p0, Lbke;->b:Lbke;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1805
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1809
    monitor-enter p0

    .line 1810
    :try_start_0
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->R()[Ljava/lang/String;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1811
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Lbki;
    .locals 1

    .prologue
    .line 1821
    monitor-enter p0

    .line 1822
    :try_start_0
    iget-object v0, p0, Lbke;->d:Lbki;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1823
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1827
    invoke-virtual {p0}, Lbke;->g()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->Y()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1831
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Lyj;
    .locals 1

    .prologue
    .line 1835
    monitor-enter p0

    .line 1836
    :try_start_0
    iget-object v0, p0, Lbke;->c:Lyj;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1837
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()I
    .locals 3

    .prologue
    const/16 v0, 0x66

    const/16 v2, 0x65

    .line 1841
    monitor-enter p0

    .line 1842
    :try_start_0
    iget-object v1, p0, Lbke;->c:Lyj;

    invoke-virtual {v1}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1843
    monitor-exit p0

    .line 1860
    :goto_0
    return v0

    .line 1845
    :cond_0
    iget v0, p0, Lbke;->a:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_1

    iget v0, p0, Lbke;->a:I

    if-ne v0, v2, :cond_2

    .line 1847
    :cond_1
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1848
    const/16 v0, 0x6d

    iput v0, p0, Lbke;->a:I

    .line 1856
    :cond_2
    :goto_1
    invoke-static {}, Lbkb;->F()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1857
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1858
    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbke;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1857
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1860
    :cond_3
    iget v0, p0, Lbke;->a:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1861
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1849
    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lbke;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1850
    const/16 v0, 0x66

    iput v0, p0, Lbke;->a:I

    goto :goto_1

    .line 1851
    :cond_5
    iget-boolean v0, p0, Lbke;->f:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lbke;->g:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lbke;->h:Z

    if-eqz v0, :cond_2

    .line 1852
    :cond_6
    const/16 v0, 0x65

    iput v0, p0, Lbke;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public i()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 1865
    monitor-enter p0

    .line 1866
    :try_start_0
    iget-object v0, p0, Lbke;->e:Ljava/lang/Exception;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1867
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()V
    .locals 3

    .prologue
    .line 1961
    invoke-static {}, Lbkb;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Clearing account state for"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1965
    :cond_0
    monitor-enter p0

    .line 1966
    const/16 v0, 0x64

    :try_start_0
    iput v0, p0, Lbke;->a:I

    .line 1967
    const/4 v0, 0x0

    iput-object v0, p0, Lbke;->e:Ljava/lang/Exception;

    .line 1968
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->f:Z

    .line 1969
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->g:Z

    .line 1970
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->h:Z

    .line 1971
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->i:Z

    .line 1972
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->j:Z

    .line 1973
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->k:Z

    .line 1974
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1983
    monitor-enter p0

    .line 1984
    :try_start_0
    invoke-virtual {p0}, Lbke;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1985
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 1994
    invoke-virtual {p0}, Lbke;->h()I

    move-result v0

    const/16 v1, 0x66

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 2003
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->s()Z

    move-result v0

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 2012
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->w()Z

    move-result v0

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2021
    monitor-enter p0

    .line 2022
    :try_start_0
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lym;->j(Lyj;)Ljava/lang/String;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 2023
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p()V
    .locals 3

    .prologue
    .line 2103
    monitor-enter p0

    .line 2104
    :try_start_0
    invoke-virtual {p0}, Lbke;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2105
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account should have been registered:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2113
    :cond_0
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2114
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;)V

    .line 2117
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbke;->a(Ljava/lang/String;)V

    .line 2118
    invoke-virtual {p0}, Lbke;->j()V

    .line 2119
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public q()V
    .locals 3

    .prologue
    .line 2126
    iget-object v0, p0, Lbke;->c:Lyj;

    const-wide/16 v1, -0x1

    invoke-static {v0, v1, v2}, Lym;->a(Lyj;J)V

    .line 2127
    return-void
.end method

.method public r()V
    .locals 3

    .prologue
    .line 2130
    iget-object v0, p0, Lbke;->c:Lyj;

    const-wide/16 v1, -0x1

    invoke-static {v0, v1, v2}, Lym;->b(Lyj;J)V

    .line 2131
    return-void
.end method

.method s()V
    .locals 8

    .prologue
    .line 2173
    monitor-enter p0

    .line 2177
    :try_start_0
    invoke-virtual {p0}, Lbke;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2178
    monitor-exit p0

    .line 2203
    :goto_0
    return-void

    .line 2180
    :cond_0
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2181
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2184
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lbke;->j:Z

    if-eqz v0, :cond_2

    .line 2185
    monitor-exit p0

    goto :goto_0

    .line 2188
    :cond_2
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lym;->s(Lyj;)J

    move-result-wide v0

    .line 2189
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2194
    sub-long v4, v0, v2

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    if-gtz v4, :cond_3

    sub-long v0, v2, v0

    .line 2195
    invoke-static {}, Lbkb;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 2196
    :cond_3
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2197
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Renewing account setting:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    :cond_4
    iget-object v0, p0, Lbke;->c:Lyj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Z)I

    .line 2201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbke;->j:Z

    .line 2203
    :cond_5
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 2207
    monitor-enter p0

    .line 2208
    :try_start_0
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lym;->k(Lyj;)Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 2209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 2216
    monitor-enter p0

    .line 2217
    :try_start_0
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lym;->l(Lyj;)Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 2218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public v()V
    .locals 4

    .prologue
    const/16 v3, 0x66

    .line 2417
    monitor-enter p0

    :try_start_0
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account sign in complete with state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbke;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lbke;->k:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lbke;->a:I

    if-le v0, v3, :cond_1

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Notify sign in failed for account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbkb;->G()Lbkf;

    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lbne;->a(Lyj;)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbke;->k:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2419
    invoke-static {}, Lbkb;->H()Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 2420
    :try_start_1
    invoke-static {}, Lbkb;->H()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkc;

    .line 2421
    iget-object v3, p0, Lbke;->c:Lyj;

    invoke-interface {v0}, Lbkc;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2423
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 2417
    :cond_1
    :try_start_2
    iget v0, p0, Lbke;->a:I

    if-ne v0, v3, :cond_0

    invoke-static {}, Lbkb;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Clear sign in failed for account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Lbkb;->G()Lbkf;

    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lbne;->b(Lyj;)V

    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->aa()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbke;->c:Lyj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lyj;->e(Z)V

    invoke-static {}, Lbkb;->F()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Performed delayed cleanse after sign in: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lbwf;->a(Lyj;)V

    :cond_4
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-virtual {v0}, Lyj;->Z()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbke;->c:Lyj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lyj;->d(Z)V

    invoke-static {}, Lbkb;->F()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Performed delayed sync after sign in: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lbke;->c:Lyj;

    invoke-static {v0}, Lbwf;->c(Lyj;)V

    :cond_6
    invoke-static {}, Lbkb;->E()Lbkd;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2423
    :cond_7
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method
