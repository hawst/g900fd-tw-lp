.class public Lbfs;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:J

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbeq;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Z

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/util/List;Ljava/util/List;ZZLjava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lbeq;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;ZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3606
    invoke-direct {p0}, Lbep;-><init>()V

    .line 3607
    iput-wide p1, p0, Lbfs;->a:J

    .line 3608
    iput-object p3, p0, Lbfs;->b:Ljava/util/List;

    .line 3609
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    new-instance v4, Ldxl;

    invoke-direct {v4}, Ldxl;-><init>()V

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v4, Ldxl;->b:Ldqf;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v4, Ldxl;->c:Ljava/lang/Long;

    invoke-static {v4}, Ldxl;->toByteArray(Lepr;)[B

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v2, p0, Lbfs;->h:Ljava/util/List;

    .line 3610
    iput-boolean p5, p0, Lbfs;->i:Z

    .line 3611
    iput-boolean p6, p0, Lbfs;->c:Z

    .line 3612
    iput-object p7, p0, Lbfs;->k:Ljava/lang/String;

    .line 3613
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3617
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 3618
    const-string v0, "Babel_RequestWriter"

    const-string v1, "SyncAllNewEvents.buildProtobuf"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3620
    :cond_0
    new-instance v4, Ldxf;

    invoke-direct {v4}, Ldxf;-><init>()V

    .line 3621
    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3622
    new-instance v0, Lbyc;

    invoke-direct {v0}, Lbyc;-><init>()V

    const-string v1, "sane_build_proto"

    invoke-virtual {v0, v1}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "id="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3623
    invoke-virtual {v0, v1}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "retry="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3624
    invoke-virtual {v0, v1}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v0

    .line 3625
    invoke-virtual {v0}, Lbyc;->b()V

    .line 3628
    :cond_1
    iget-object v0, p0, Lbfs;->b:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 3629
    iget-object v0, p0, Lbfs;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ldtp;

    iput-object v0, v4, Ldxf;->d:[Ldtp;

    .line 3631
    iget-object v0, p0, Lbfs;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbeq;

    .line 3632
    iget-object v6, v4, Ldxf;->d:[Ldtp;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, Lbeq;->a()Ldtp;

    move-result-object v0

    aput-object v0, v6, v1

    move v1, v3

    .line 3633
    goto :goto_0

    .line 3635
    :cond_2
    const-string v0, "Babel_RequestWriter"

    const-string v1, "SyncAllNewEventsRequest.buildProtobuf: null localState"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3637
    :cond_3
    iget-object v0, p0, Lbfs;->h:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 3638
    iget-object v0, p0, Lbfs;->h:Ljava/util/List;

    .line 3639
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ldxl;

    iput-object v0, v4, Ldxf;->e:[Ldxl;

    .line 3640
    :goto_1
    iget-object v0, p0, Lbfs;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 3641
    iget-object v0, p0, Lbfs;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 3643
    :try_start_0
    iget-object v1, v4, Ldxf;->e:[Ldxl;

    new-instance v3, Ldxl;

    invoke-direct {v3}, Ldxl;-><init>()V

    invoke-static {v3, v0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxl;

    aput-object v0, v1, v2
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    .line 3640
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3649
    :cond_4
    const-string v0, "Babel_RequestWriter"

    const-string v1, "SyncAllNewEventsRequest.buildProtobuf: null rawUnreadConversationStates"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3653
    :cond_5
    const-string v0, "babel_smaxbytesperws"

    const v1, 0x186a0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Ldxf;->f:Ljava/lang/Integer;

    .line 3656
    invoke-virtual {p0, p1, p2}, Lbfs;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v4, Ldxf;->b:Ldvm;

    .line 3657
    iget-wide v0, p0, Lbfs;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Ldxf;->c:Ljava/lang/Long;

    .line 3658
    iget-boolean v0, p0, Lbfs;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Ldxf;->g:Ljava/lang/Boolean;

    .line 3659
    return-object v4

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public a(Lyj;Lbph;)V
    .locals 5

    .prologue
    .line 3699
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    .line 3700
    invoke-virtual {p2}, Lbph;->e()I

    move-result v0

    const/16 v2, 0x78

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    .line 3702
    :goto_0
    sget-boolean v2, Lbea;->d:Z

    if-eqz v2, :cond_0

    .line 3703
    const-string v2, "Babel_RequestWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SyncAllNewEvents.onFailed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3705
    :cond_0
    invoke-static {}, Lbya;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3706
    new-instance v2, Lbyc;

    invoke-direct {v2}, Lbyc;-><init>()V

    const-string v3, "sane_expired"

    invoke-virtual {v2, v3}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 3707
    invoke-virtual {v2, v1}, Lbyc;->d(Ljava/lang/String;)Lbyc;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isReplaced="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lbfs;->j:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3708
    invoke-virtual {v1, v2}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "expired="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3709
    invoke-virtual {v1, v0}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v0

    .line 3710
    invoke-virtual {v0}, Lbyc;->b()V

    .line 3713
    :cond_1
    invoke-static {p1}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v0

    .line 3714
    iget-object v1, p0, Lbfs;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbpf;->d(Ljava/lang/String;)Z

    .line 3715
    return-void

    .line 3700
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lbea;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3719
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3721
    check-cast p1, Lbfs;

    .line 3722
    iget-boolean v0, p1, Lbfs;->i:Z

    if-nez v0, :cond_0

    .line 3723
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbfs;->i:Z

    .line 3725
    :cond_0
    iput-boolean v2, p1, Lbfs;->j:Z

    .line 3726
    return v2
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3680
    const-string v0, "conversations/syncallnewevents"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 3685
    iget-boolean v0, p0, Lbfs;->i:Z

    if-eqz v0, :cond_0

    .line 3687
    const-string v0, "babel_sane_refresh_timeout"

    const-wide/32 v1, 0x15f90

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 3692
    :goto_0
    return-wide v0

    :cond_0
    const-string v0, "babel_sane_timeout"

    sget-wide v1, Lbkj;->d:J

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 3730
    iget-boolean v0, p0, Lbfs;->i:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3734
    iget-object v0, p0, Lbfs;->k:Ljava/lang/String;

    return-object v0
.end method
