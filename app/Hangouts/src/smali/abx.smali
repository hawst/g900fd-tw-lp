.class public final Labx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcav;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Labx;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcau;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 73
    iget-object v0, p0, Labx;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V

    .line 74
    invoke-virtual {p1}, Lcau;->b()Ljava/lang/String;

    move-result-object v3

    .line 75
    if-nez v3, :cond_0

    .line 76
    iget-object v0, p0, Labx;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getActivity()Ly;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->ku:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, ""

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 93
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-virtual {p1}, Lcau;->a()Lbdk;

    move-result-object v6

    .line 84
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 85
    iget-object v1, p0, Labx;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lyj;

    move-result-object v1

    invoke-virtual {v1}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    .line 86
    :cond_1
    iget-object v0, p0, Labx;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lyj;

    move-result-object v0

    .line 88
    :cond_2
    iget-object v1, v6, Lbdk;->a:Ljava/lang/String;

    iget-object v2, v6, Lbdk;->b:Ljava/lang/String;

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 91
    iget-object v1, p0, Labx;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lfe;

    move-result-object v1

    new-instance v2, Lacb;

    invoke-direct {v2, v6, v3}, Lacb;-><init>(Lbdk;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Lfe;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, Labx;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Labz;

    move-result-object v0

    invoke-virtual {v0}, Labz;->notifyDataSetChanged()V

    goto :goto_0
.end method
