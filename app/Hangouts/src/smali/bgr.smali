.class public Lbgr;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x4L


# instance fields
.field private final g:[B

.field private final h:[Lbdh;


# direct methods
.method private constructor <init>(Ldsl;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3135
    iget-object v0, p1, Ldsl;->b:Ldvn;

    const-wide/16 v2, -0x1

    invoke-direct {p0, v0, v2, v3}, Lbfz;-><init>(Ldvn;J)V

    .line 3137
    iget-object v0, p1, Ldsl;->d:Ldql;

    if-eqz v0, :cond_2

    iget-object v0, p1, Ldsl;->d:Ldql;

    .line 3138
    invoke-static {v0}, Ldql;->toByteArray(Lepr;)[B

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbgr;->g:[B

    .line 3141
    iget-object v0, p1, Ldsl;->d:Ldql;

    if-eqz v0, :cond_0

    .line 3142
    const/4 v0, 0x1

    new-array v1, v0, [Ldql;

    .line 3143
    const/4 v0, 0x0

    iget-object v2, p1, Ldsl;->d:Ldql;

    aput-object v2, v1, v0

    .line 3145
    :cond_0
    iget-object v0, p1, Ldsl;->g:[Ldro;

    invoke-static {v0, v1}, Lbfz;->a([Ldro;[Ldql;)[Lbdh;

    move-result-object v0

    iput-object v0, p0, Lbgr;->h:[Lbdh;

    .line 3147
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 3148
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetConversationResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3150
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 3138
    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3154
    new-instance v0, Ldsl;

    invoke-direct {v0}, Ldsl;-><init>()V

    .line 3155
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldsl;

    .line 3156
    iget-object v1, v0, Ldsl;->b:Ldvn;

    invoke-static {v1}, Lbgr;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3157
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldsl;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3159
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgr;

    invoke-direct {v1, v0}, Lbgr;-><init>(Ldsl;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 25

    .prologue
    .line 3194
    invoke-super/range {p0 .. p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 3196
    const-string v2, ""

    .line 3197
    const/4 v1, 0x0

    .line 3198
    move-object/from16 v0, p0

    iget-object v3, v0, Lbgr;->g:[B

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lbgr;->c:Lbht;

    iget-wide v5, v5, Lbht;->d:J

    invoke-static {v3, v4, v5, v6}, Lbck;->a([BZJ)Lbck;

    move-result-object v20

    .line 3200
    if-eqz v20, :cond_25

    .line 3201
    move-object/from16 v0, v20

    iget-object v2, v0, Lbck;->a:Ljava/lang/String;

    .line 3202
    move-object/from16 v0, v20

    iget-object v1, v0, Lbck;->c:Ljava/util/List;

    move-object v11, v2

    move-object v2, v1

    .line 3205
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lbgr;->b:Lbea;

    move-object v10, v1

    check-cast v10, Lbek;

    .line 3206
    iget-boolean v0, v10, Lbek;->b:Z

    move/from16 v21, v0

    .line 3207
    iget-object v1, v10, Lbek;->j:[B

    if-nez v1, :cond_6

    iget-wide v3, v10, Lbek;->k:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_6

    const/4 v1, 0x1

    move v12, v1

    .line 3209
    :goto_1
    iget-object v1, v10, Lbek;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    move/from16 v19, v1

    .line 3211
    :goto_2
    if-nez v21, :cond_0

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3212
    const-string v1, "Events were not requested, eventList should be empty"

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    .line 3216
    :cond_0
    iget-object v1, v10, Lbek;->o:Lbjg;

    if-eqz v1, :cond_24

    .line 3217
    sget-boolean v1, Lbfz;->a:Z

    if-eqz v1, :cond_1

    .line 3218
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Adding saved pushEvent to GetConversationResponse "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " eventId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v10, Lbek;->o:Lbjg;

    iget-object v4, v4, Lbjg;->m:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " timestamp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v10, Lbek;->o:Lbjg;

    iget-wide v4, v4, Lbjg;->e:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3225
    :cond_1
    const/4 v1, 0x0

    .line 3226
    if-eqz v2, :cond_2

    .line 3227
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 3229
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    add-int/lit8 v4, v1, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 3230
    if-eqz v1, :cond_3

    .line 3231
    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 3233
    :cond_3
    iget-object v1, v10, Lbek;->o:Lbjg;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v13, v3

    .line 3237
    :goto_3
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3238
    if-eqz v20, :cond_a

    .line 3240
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_8

    .line 3241
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, " earliest: "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjg;

    iget-wide v3, v1, Lbjg;->e:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " latest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3242
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjg;

    iget-wide v3, v1, Lbjg;->e:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3246
    :goto_4
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processGetConversationResponse conversationId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " requestedEvents: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " eventCount: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3249
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " continuationToken: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v20

    iget-object v3, v0, Lbck;->e:[B

    .line 3250
    invoke-static {v3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " continuationEventTimestamp: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v20

    iget-wide v3, v0, Lbck;->f:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " num entities: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lbgr;->h:[Lbdh;

    if-nez v1, :cond_9

    const/4 v1, 0x0

    :goto_5
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3246
    invoke-static {v2, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3262
    :cond_4
    :goto_6
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 3263
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_b

    const/4 v1, 0x0

    .line 3264
    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjg;

    iget-wide v1, v1, Lbjg;->e:J

    move-wide v14, v1

    .line 3266
    :goto_7
    const/16 v16, 0x0

    .line 3267
    const/4 v1, 0x0

    .line 3268
    const/4 v9, 0x0

    .line 3270
    invoke-virtual/range {p1 .. p1}, Lyt;->a()V

    .line 3271
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v22, v2, v4

    .line 3273
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbgr;->h:[Lbdh;

    if-eqz v2, :cond_c

    .line 3276
    move-object/from16 v0, p0

    iget-object v3, v0, Lbgr;->h:[Lbdh;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_8
    if-ge v2, v4, :cond_c

    aget-object v5, v3, v2

    .line 3278
    if-eqz v5, :cond_5

    .line 3279
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Lyt;->a(Lbdh;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3276
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 3207
    :cond_6
    const/4 v1, 0x0

    move v12, v1

    goto/16 :goto_1

    .line 3209
    :cond_7
    const/4 v1, 0x0

    move/from16 v19, v1

    goto/16 :goto_2

    .line 3244
    :cond_8
    const-string v1, ""

    goto/16 :goto_4

    .line 3250
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lbgr;->h:[Lbdh;

    array-length v1, v1

    goto :goto_5

    .line 3255
    :cond_a
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processGetConversationResponse requestedEvents: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 3264
    :cond_b
    const-wide/16 v1, 0x0

    move-wide v14, v1

    goto :goto_7

    .line 3286
    :cond_c
    :try_start_1
    move-object/from16 v0, v20

    iget-object v2, v0, Lbck;->b:Lbjc;

    if-eqz v2, :cond_23

    .line 3287
    move-object/from16 v0, v20

    iget-object v2, v0, Lbck;->b:Lbjc;

    move-object/from16 v0, v20

    iget-wide v3, v0, Lbck;->h:J

    const/4 v5, 0x0

    const/4 v7, 0x0

    iget-object v1, v10, Lbek;->q:Lbee;

    if-eqz v1, :cond_d

    const/4 v8, 0x1

    :goto_9
    move-object/from16 v1, p1

    move-object/from16 v6, p2

    .line 3288
    invoke-static/range {v1 .. v8}, Lyp;->a(Lyt;Lbjc;JLjava/lang/String;Lbnl;Lys;Z)Z

    move-result v1

    move/from16 v18, v1

    .line 3294
    :goto_a
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    move v1, v9

    move/from16 v3, v16

    :goto_b
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbjg;

    .line 3295
    instance-of v1, v2, Lbji;

    if-eqz v1, :cond_e

    .line 3296
    const/4 v1, 0x1

    move/from16 v16, v1

    .line 3313
    :goto_c
    const/16 v17, 0x1

    .line 3315
    invoke-virtual/range {p0 .. p0}, Lbgr;->d()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lbgr;->e()J

    move-result-wide v6

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move-wide/from16 v8, v22

    .line 3314
    invoke-static/range {v1 .. v9}, Lyp;->a(Lyt;Lbjg;Lbnl;JJJ)V

    move/from16 v1, v17

    move/from16 v3, v16

    .line 3316
    goto :goto_b

    .line 3287
    :cond_d
    const/4 v8, 0x0

    goto :goto_9

    .line 3297
    :cond_e
    instance-of v1, v2, Lbiv;

    if-eqz v1, :cond_11

    .line 3298
    if-eqz v21, :cond_11

    .line 3299
    iget-object v1, v10, Lbek;->j:[B

    if-eqz v1, :cond_f

    .line 3300
    move-object v0, v2

    check-cast v0, Lbiv;

    move-object v1, v0

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lbiv;->a(I)V

    move/from16 v16, v3

    goto :goto_c

    .line 3302
    :cond_f
    if-eqz v19, :cond_10

    .line 3303
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v1

    iget-object v4, v10, Lbek;->l:Ljava/lang/String;

    invoke-static {v1, v4}, Lbgr;->a(Lyj;Ljava/lang/String;)V

    .line 3304
    move-object v0, v2

    check-cast v0, Lbiv;

    move-object v1, v0

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lbiv;->a(I)V

    move/from16 v16, v3

    goto :goto_c

    .line 3307
    :cond_10
    move-object v0, v2

    check-cast v0, Lbiv;

    move-object v1, v0

    const/4 v4, 0x5

    invoke-virtual {v1, v4}, Lbiv;->a(I)V

    :cond_11
    move/from16 v16, v3

    goto :goto_c

    .line 3320
    :cond_12
    if-eqz v21, :cond_15

    if-eqz v12, :cond_15

    .line 3321
    const-string v2, "last_successful_sync_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v4

    .line 3324
    sget-boolean v2, Lyp;->a:Z

    if-nez v2, :cond_13

    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_14

    .line 3325
    :cond_13
    const-string v2, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "lastSuccessfulSyncTime "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " oldestEventInListTime "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3329
    :cond_14
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_15

    cmp-long v2, v14, v4

    if-lez v2, :cond_15

    .line 3333
    const-string v2, "Babel"

    const-string v4, "getting rid of older messages"

    invoke-static {v2, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3335
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 3334
    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3339
    :cond_15
    if-eqz v19, :cond_19

    .line 3340
    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_16

    .line 3341
    const-string v2, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "conversation is now synced:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3345
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-static {v2}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v2

    .line 3347
    iget-object v4, v10, Lbek;->l:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lbpf;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 3349
    sget-boolean v2, Lyp;->a:Z

    if-nez v2, :cond_17

    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_18

    .line 3350
    :cond_17
    const-string v2, "Babel"

    const-string v4, "all conversations now synced"

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3352
    :cond_18
    const-string v2, "in_progress_sync_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v4

    .line 3354
    const-string v2, "last_successful_sync_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Lyt;->h(Ljava/lang/String;J)V

    .line 3356
    const-string v2, "in_progress_sync_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Lyt;->h(Ljava/lang/String;J)V

    .line 3362
    :cond_19
    if-eqz v21, :cond_1a

    .line 3363
    move-object/from16 v0, v20

    iget-object v2, v0, Lbck;->e:[B

    move-object/from16 v0, v20

    iget-wide v4, v0, Lbck;->f:J

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v4, v5}, Lyp;->a(Lyt;Ljava/lang/String;[BJ)V

    .line 3367
    :cond_1a
    invoke-virtual/range {p1 .. p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3369
    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    .line 3371
    if-eqz v18, :cond_1b

    .line 3372
    invoke-static/range {p1 .. p1}, Lyp;->d(Lyt;)V

    .line 3374
    :cond_1b
    if-eqz v1, :cond_1c

    .line 3375
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 3377
    :cond_1c
    if-eqz v3, :cond_1d

    .line 3378
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lyp;->c(Lyt;Ljava/lang/String;)V

    .line 3384
    :cond_1d
    iget-object v1, v10, Lbek;->q:Lbee;

    .line 3386
    if-eqz v1, :cond_1f

    .line 3387
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 3388
    iget-object v1, v1, Lbee;->h:Ljava/lang/String;

    .line 3389
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_1e

    .line 3390
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ProcessGetConversationResponse found conversation; client id == "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; server id == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3395
    :cond_1e
    move-object/from16 v0, p1

    invoke-static {v0, v1, v11}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyt;Ljava/lang/String;Ljava/lang/String;)V

    .line 3407
    :cond_1f
    :goto_d
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_20

    if-eqz v20, :cond_20

    move-object/from16 v0, v20

    iget-object v1, v0, Lbck;->b:Lbjc;

    if-eqz v1, :cond_20

    iget-wide v1, v10, Lbek;->n:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_20

    .line 3410
    iget-wide v1, v10, Lbek;->n:J

    move-object/from16 v0, v20

    iget-object v3, v0, Lbck;->b:Lbjc;

    iget-wide v3, v3, Lbjc;->t:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_20

    .line 3411
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SortTimestamp mismatched ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, v10, Lbek;->n:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " != "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    iget-object v3, v0, Lbck;->b:Lbjc;

    iget-wide v3, v3, Lbjc;->t:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") forcing immediate SANE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3415
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ZZIZ)V

    .line 3422
    :cond_20
    return-void

    .line 3369
    :catchall_0
    move-exception v1

    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    throw v1

    .line 3400
    :cond_21
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_22

    .line 3401
    const-string v2, "Babel"

    const-string v3, "ProcessGetConversationResponse didn\'t find conversation"

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3403
    :cond_22
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lbnl;->a(Lbea;)V

    goto :goto_d

    :cond_23
    move/from16 v18, v1

    goto/16 :goto_a

    :cond_24
    move-object v13, v2

    goto/16 :goto_3

    :cond_25
    move-object v11, v2

    move-object v2, v1

    goto/16 :goto_0
.end method
