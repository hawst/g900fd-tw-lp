.class public Lbel;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbcn;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final h:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbcn;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 3302
    invoke-direct {p0}, Lbep;-><init>()V

    .line 3303
    iput-object p1, p0, Lbel;->a:Ljava/util/List;

    .line 3304
    iput-boolean p3, p0, Lbel;->h:Z

    .line 3305
    iput-object p2, p0, Lbel;->b:Ljava/lang/String;

    .line 3306
    iput-boolean p4, p0, Lbel;->c:Z

    .line 3307
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 3388
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3389
    iget-object v0, p0, Lbel;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcn;

    .line 3390
    if-eqz v0, :cond_0

    .line 3391
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3394
    :cond_1
    iput-object v1, p0, Lbel;->a:Ljava/util/List;

    .line 3395
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3349
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 3350
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "GetEntityByIdRequest: lookupSpecs="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbel;->a:Ljava/util/List;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3353
    :cond_0
    new-instance v5, Ldsm;

    invoke-direct {v5}, Ldsm;-><init>()V

    .line 3354
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v3, p1, p2}, Lbel;->a(Ldpt;ZLjava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v5, Ldsm;->b:Ldvm;

    .line 3358
    iget-object v0, p0, Lbel;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcn;

    .line 3359
    if-eqz v0, :cond_7

    .line 3360
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 3362
    goto :goto_0

    .line 3364
    :cond_1
    if-lez v1, :cond_2

    .line 3365
    new-array v0, v1, [Ldrp;

    iput-object v0, v5, Ldsm;->c:[Ldrp;

    .line 3367
    :cond_2
    iget-object v0, p0, Lbel;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    move v0, v3

    .line 3370
    :goto_2
    iget-object v1, p0, Lbel;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v0

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcn;

    .line 3371
    if-nez v0, :cond_4

    .line 3372
    const-string v0, "Babel_RequestWriter"

    const-string v1, "GetEntityByIdRequest: null spec!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v3

    .line 3373
    goto :goto_3

    :cond_3
    move v0, v2

    .line 3367
    goto :goto_2

    .line 3375
    :cond_4
    iget-object v7, v5, Ldsm;->c:[Ldrp;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0}, Lbcn;->d()Ldrp;

    move-result-object v0

    aput-object v0, v7, v2

    move v2, v1

    .line 3377
    goto :goto_3

    .line 3381
    :cond_5
    if-eqz v4, :cond_6

    .line 3382
    invoke-direct {p0}, Lbel;->h()V

    .line 3384
    :cond_6
    return-object v5

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public a(Lyj;Lbph;)V
    .locals 4

    .prologue
    .line 3330
    iget-boolean v0, p0, Lbel;->c:Z

    if-eqz v0, :cond_0

    .line 3332
    invoke-static {p1}, Lbow;->a(Lyj;)Lbow;

    move-result-object v0

    .line 3333
    invoke-virtual {v0}, Lbow;->d()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 3334
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RefreshParticipantsOperation failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3335
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3334
    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 3336
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbow;->a(I)V

    .line 3340
    :cond_0
    iget-boolean v0, p0, Lbel;->h:Z

    if-nez v0, :cond_1

    .line 3341
    iget-object v0, p0, Lbel;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcn;

    .line 3342
    invoke-static {p1}, Lbrf;->a(Lyj;)Lbrf;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbrf;->a(Lbcn;)V

    goto :goto_0

    .line 3345
    :cond_1
    return-void
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 3311
    iget-boolean v0, p0, Lbel;->h:Z

    if-eqz v0, :cond_0

    .line 3313
    const/4 v0, 0x0

    .line 3315
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lbep;->a(JI)Z

    move-result v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3399
    const-string v0, "contacts/getentitybyid"

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 3321
    iget-boolean v0, p0, Lbel;->h:Z

    if-eqz v0, :cond_0

    .line 3322
    const-wide/16 v0, 0x0

    .line 3325
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0}, Lbep;->d()J

    move-result-wide v0

    goto :goto_0
.end method
