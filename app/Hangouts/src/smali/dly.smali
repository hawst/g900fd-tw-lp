.class public final Ldly;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldly;


# instance fields
.field public b:[Ldml;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5078
    const/4 v0, 0x0

    new-array v0, v0, [Ldly;

    sput-object v0, Ldly;->a:[Ldly;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5079
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5082
    sget-object v0, Ldml;->a:[Ldml;

    iput-object v0, p0, Ldly;->b:[Ldml;

    .line 5079
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 5101
    iget-object v1, p0, Ldly;->b:[Ldml;

    if-eqz v1, :cond_1

    .line 5102
    iget-object v2, p0, Ldly;->b:[Ldml;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 5103
    if-eqz v4, :cond_0

    .line 5104
    const/4 v5, 0x1

    .line 5105
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5102
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5109
    :cond_1
    iget-object v1, p0, Ldly;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5110
    iput v0, p0, Ldly;->cachedSize:I

    .line 5111
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5075
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldly;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldly;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldly;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldly;->b:[Ldml;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldml;

    iget-object v3, p0, Ldly;->b:[Ldml;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldly;->b:[Ldml;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldly;->b:[Ldml;

    :goto_2
    iget-object v2, p0, Ldly;->b:[Ldml;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldly;->b:[Ldml;

    new-instance v3, Ldml;

    invoke-direct {v3}, Ldml;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldly;->b:[Ldml;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldly;->b:[Ldml;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldly;->b:[Ldml;

    new-instance v3, Ldml;

    invoke-direct {v3}, Ldml;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldly;->b:[Ldml;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 5087
    iget-object v0, p0, Ldly;->b:[Ldml;

    if-eqz v0, :cond_1

    .line 5088
    iget-object v1, p0, Ldly;->b:[Ldml;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 5089
    if-eqz v3, :cond_0

    .line 5090
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 5088
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5094
    :cond_1
    iget-object v0, p0, Ldly;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5096
    return-void
.end method
