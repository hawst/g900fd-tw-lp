.class public final Lela;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lela;


# instance fields
.field public b:Lemc;

.field public c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 834
    const/4 v0, 0x0

    new-array v0, v0, [Lela;

    sput-object v0, Lela;->a:[Lela;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 835
    invoke-direct {p0}, Lepn;-><init>()V

    .line 847
    const/4 v0, 0x0

    iput-object v0, p0, Lela;->b:Lemc;

    .line 850
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Lela;->c:[I

    .line 835
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 870
    iget-object v0, p0, Lela;->b:Lemc;

    if-eqz v0, :cond_2

    .line 871
    const/4 v0, 0x1

    iget-object v2, p0, Lela;->b:Lemc;

    .line 872
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 874
    :goto_0
    iget-object v2, p0, Lela;->c:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lela;->c:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 876
    iget-object v3, p0, Lela;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 878
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 876
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 880
    :cond_0
    add-int/2addr v0, v2

    .line 881
    iget-object v1, p0, Lela;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 883
    :cond_1
    iget-object v1, p0, Lela;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 884
    iput v0, p0, Lela;->cachedSize:I

    .line 885
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 831
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lela;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lela;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lela;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lela;->b:Lemc;

    if-nez v0, :cond_2

    new-instance v0, Lemc;

    invoke-direct {v0}, Lemc;-><init>()V

    iput-object v0, p0, Lela;->b:Lemc;

    :cond_2
    iget-object v0, p0, Lela;->b:Lemc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Lela;->c:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Lela;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lela;->c:[I

    :goto_1
    iget-object v1, p0, Lela;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lela;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lela;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 855
    iget-object v0, p0, Lela;->b:Lemc;

    if-eqz v0, :cond_0

    .line 856
    const/4 v0, 0x1

    iget-object v1, p0, Lela;->b:Lemc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 858
    :cond_0
    iget-object v0, p0, Lela;->c:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lela;->c:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 859
    iget-object v1, p0, Lela;->c:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 860
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 859
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 863
    :cond_1
    iget-object v0, p0, Lela;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 865
    return-void
.end method
