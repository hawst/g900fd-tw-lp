.class public final Lafy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahn;


# instance fields
.field final synthetic a:Lbdh;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lbdh;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 5229
    iput-object p1, p0, Lafy;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iput-object p2, p0, Lafy;->a:Lbdh;

    iput-object p3, p0, Lafy;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lahf;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 5232
    iget-object v0, p0, Lafy;->a:Lbdh;

    iget v0, v0, Lbdh;->a:I

    if-ne v0, v6, :cond_0

    iget-object v0, p0, Lafy;->a:Lbdh;

    iget-object v0, v0, Lbdh;->c:Ljava/lang/String;

    .line 5233
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5234
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PARTICIPANT_TYPE_PERSON with phone found in conversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lafy;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mergeKey "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lahf;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ParticipantEntity phone number like: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lafy;->a:Lbdh;

    .line 5237
    invoke-virtual {v2}, Lbdh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5234
    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 5240
    :cond_0
    iget-object v0, p0, Lafy;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5250
    :cond_1
    return-void

    .line 5243
    :cond_2
    iget-object v0, p2, Lahf;->f:Ljava/lang/String;

    invoke-static {v0}, Lyt;->aj(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5246
    iget-object v0, p2, Lahf;->f:Ljava/lang/String;

    invoke-static {v0}, Lyt;->ak(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5249
    iget-object v0, p2, Lahf;->f:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "g:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lafy;->a:Lbdh;

    iget-object v2, v2, Lbdh;->b:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5252
    const-string v0, "Babel"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "in conversation %s gaia mismatch %s vs %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lafy;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    iget-object v4, p2, Lahf;->f:Ljava/lang/String;

    aput-object v4, v3, v6

    const/4 v4, 0x2

    iget-object v5, p0, Lafy;->a:Lbdh;

    iget-object v5, v5, Lbdh;->b:Lbdk;

    iget-object v5, v5, Lbdk;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Gaia mismatch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
