.class public final Ldhn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldhn;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:[Ldho;

.field public e:Ljava/lang/Long;

.field public f:Ldhj;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Boolean;

.field public i:Ldho;

.field public j:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    new-array v0, v0, [Ldhn;

    sput-object v0, Ldhn;->a:[Ldhn;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 223
    invoke-direct {p0}, Lepn;-><init>()V

    .line 244
    iput-object v1, p0, Ldhn;->c:Ljava/lang/Integer;

    .line 247
    sget-object v0, Ldho;->a:[Ldho;

    iput-object v0, p0, Ldhn;->d:[Ldho;

    .line 252
    iput-object v1, p0, Ldhn;->f:Ldhj;

    .line 259
    iput-object v1, p0, Ldhn;->i:Ldho;

    .line 262
    iput-object v1, p0, Ldhn;->j:Ljava/lang/Integer;

    .line 223
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 305
    iget-object v0, p0, Ldhn;->b:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 306
    const/4 v0, 0x1

    iget-object v2, p0, Ldhn;->b:Ljava/lang/String;

    .line 307
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 309
    :goto_0
    iget-object v2, p0, Ldhn;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 310
    const/4 v2, 0x2

    iget-object v3, p0, Ldhn;->c:Ljava/lang/Integer;

    .line 311
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 313
    :cond_0
    iget-object v2, p0, Ldhn;->d:[Ldho;

    if-eqz v2, :cond_2

    .line 314
    iget-object v2, p0, Ldhn;->d:[Ldho;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 315
    if-eqz v4, :cond_1

    .line 316
    const/4 v5, 0x3

    .line 317
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 314
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 321
    :cond_2
    iget-object v1, p0, Ldhn;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 322
    const/4 v1, 0x4

    iget-object v2, p0, Ldhn;->e:Ljava/lang/Long;

    .line 323
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 325
    :cond_3
    iget-object v1, p0, Ldhn;->f:Ldhj;

    if-eqz v1, :cond_4

    .line 326
    const/4 v1, 0x5

    iget-object v2, p0, Ldhn;->f:Ldhj;

    .line 327
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 329
    :cond_4
    iget-object v1, p0, Ldhn;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 330
    const/4 v1, 0x6

    iget-object v2, p0, Ldhn;->g:Ljava/lang/String;

    .line 331
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 333
    :cond_5
    iget-object v1, p0, Ldhn;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 334
    const/4 v1, 0x7

    iget-object v2, p0, Ldhn;->h:Ljava/lang/Boolean;

    .line 335
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 337
    :cond_6
    iget-object v1, p0, Ldhn;->i:Ldho;

    if-eqz v1, :cond_7

    .line 338
    const/16 v1, 0x8

    iget-object v2, p0, Ldhn;->i:Ldho;

    .line 339
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 341
    :cond_7
    iget-object v1, p0, Ldhn;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 342
    const/16 v1, 0x9

    iget-object v2, p0, Ldhn;->j:Ljava/lang/Integer;

    .line 343
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 345
    :cond_8
    iget-object v1, p0, Ldhn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 346
    iput v0, p0, Ldhn;->cachedSize:I

    .line 347
    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 219
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldhn;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldhn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldhn;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhn;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    if-eqz v0, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-ne v0, v7, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhn;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhn;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldhn;->d:[Ldho;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldho;

    iget-object v3, p0, Ldhn;->d:[Ldho;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldhn;->d:[Ldho;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldhn;->d:[Ldho;

    :goto_2
    iget-object v2, p0, Ldhn;->d:[Ldho;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldhn;->d:[Ldho;

    new-instance v3, Ldho;

    invoke-direct {v3}, Ldho;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhn;->d:[Ldho;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldhn;->d:[Ldho;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldhn;->d:[Ldho;

    new-instance v3, Ldho;

    invoke-direct {v3}, Ldho;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhn;->d:[Ldho;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldhn;->e:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldhn;->f:Ldhj;

    if-nez v0, :cond_7

    new-instance v0, Ldhj;

    invoke-direct {v0}, Ldhj;-><init>()V

    iput-object v0, p0, Ldhn;->f:Ldhj;

    :cond_7
    iget-object v0, p0, Ldhn;->f:Ldhj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhn;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldhn;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldhn;->i:Ldho;

    if-nez v0, :cond_8

    new-instance v0, Ldho;

    invoke-direct {v0}, Ldho;-><init>()V

    iput-object v0, p0, Ldhn;->i:Ldho;

    :cond_8
    iget-object v0, p0, Ldhn;->i:Ldho;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_9

    if-eq v0, v5, :cond_9

    if-eq v0, v6, :cond_9

    if-eq v0, v7, :cond_9

    if-ne v0, v4, :cond_a

    :cond_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhn;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhn;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 267
    iget-object v0, p0, Ldhn;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 268
    const/4 v0, 0x1

    iget-object v1, p0, Ldhn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 270
    :cond_0
    iget-object v0, p0, Ldhn;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 271
    const/4 v0, 0x2

    iget-object v1, p0, Ldhn;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 273
    :cond_1
    iget-object v0, p0, Ldhn;->d:[Ldho;

    if-eqz v0, :cond_3

    .line 274
    iget-object v1, p0, Ldhn;->d:[Ldho;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 275
    if-eqz v3, :cond_2

    .line 276
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 274
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 280
    :cond_3
    iget-object v0, p0, Ldhn;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 281
    const/4 v0, 0x4

    iget-object v1, p0, Ldhn;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 283
    :cond_4
    iget-object v0, p0, Ldhn;->f:Ldhj;

    if-eqz v0, :cond_5

    .line 284
    const/4 v0, 0x5

    iget-object v1, p0, Ldhn;->f:Ldhj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 286
    :cond_5
    iget-object v0, p0, Ldhn;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 287
    const/4 v0, 0x6

    iget-object v1, p0, Ldhn;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 289
    :cond_6
    iget-object v0, p0, Ldhn;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 290
    const/4 v0, 0x7

    iget-object v1, p0, Ldhn;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 292
    :cond_7
    iget-object v0, p0, Ldhn;->i:Ldho;

    if-eqz v0, :cond_8

    .line 293
    const/16 v0, 0x8

    iget-object v1, p0, Ldhn;->i:Ldho;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 295
    :cond_8
    iget-object v0, p0, Ldhn;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 296
    const/16 v0, 0x9

    iget-object v1, p0, Ldhn;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 298
    :cond_9
    iget-object v0, p0, Ldhn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 300
    return-void
.end method
