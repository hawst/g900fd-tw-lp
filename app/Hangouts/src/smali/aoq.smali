.class public final Laoq;
.super Ls;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ls;-><init>()V

    return-void
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 27
    invoke-virtual {p0}, Laoq;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 28
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 29
    sget v1, Lf;->fm:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 30
    sget v1, Lg;->cB:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 31
    const-string v4, "abuse_android"

    sget v5, Lh;->dD:I

    invoke-static {v1, v0, v2, v4, v5}, Lf;->a(Landroid/widget/TextView;Landroid/app/Activity;Landroid/content/res/Resources;Ljava/lang/String;I)V

    .line 33
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v0, Lh;->dE:I

    .line 34
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 35
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lh;->hZ:I

    .line 36
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lh;->ab:I

    .line 37
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Laoq;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 55
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->r()V

    .line 56
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Laoq;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 44
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 45
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->k()Lyj;

    move-result-object v1

    invoke-static {v1}, Lym;->r(Lyj;)V

    .line 46
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->q()Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e()V

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->r()V

    goto :goto_0
.end method
