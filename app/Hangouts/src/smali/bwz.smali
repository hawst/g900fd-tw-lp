.class public final Lbwz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lft;


# static fields
.field private static final a:Z

.field private static final b:Landroid/net/Uri;

.field private static final c:Ljava/util/regex/Pattern;

.field private static d:Ljava/lang/String;


# instance fields
.field private e:Lorg/apache/http/client/HttpClient;

.field private f:J

.field private g:Lbcp;

.field private h:Lbwy;

.field private i:Ljava/lang/String;

.field private final j:Lbpk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lbys;->q:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbwz;->a:Z

    .line 68
    const-string v0, "https://picasaweb.google.com/data/upload/resumable/media/create-session/feed/api/user/default/albumid/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbwz;->b:Landroid/net/Uri;

    .line 90
    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbwz;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lbpk;)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lbwz;->j:Lbpk;

    .line 109
    invoke-static {}, Lbwz;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxb;->a(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lbwz;->e:Lorg/apache/http/client/HttpClient;

    .line 110
    return-void
.end method

.method private static a(Ljava/io/InputStream;[BI)I
    .locals 3

    .prologue
    .line 571
    const/4 v0, 0x0

    .line 572
    :goto_0
    if-ge v0, p2, :cond_0

    .line 573
    sub-int v1, p2, v0

    invoke-virtual {p0, p1, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 574
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 579
    :cond_0
    return v0

    .line 577
    :cond_1
    add-int/2addr v0, v1

    .line 578
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 586
    if-eqz p0, :cond_0

    .line 587
    sget-object v0, Lbwz;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 588
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 589
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 592
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static a(Lorg/apache/http/HttpEntity;)Lbxa;
    .locals 3

    .prologue
    .line 476
    if-nez p0, :cond_0

    .line 477
    new-instance v0, Lbxj;

    const-string v1, "null HttpEntity in response"

    invoke-direct {v0, v1}, Lbxj;-><init>(Ljava/lang/String;)V

    throw v0

    .line 479
    :cond_0
    new-instance v0, Lbxa;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lbxa;-><init>(B)V

    .line 480
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 482
    :try_start_0
    sget-object v2, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-static {v1, v2, v0}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 486
    invoke-virtual {v0}, Lbxa;->a()V

    .line 487
    return-object v0

    .line 484
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private static a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;
    .locals 5

    .prologue
    .line 596
    new-instance v0, Lorg/apache/http/entity/BufferedHttpEntity;

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/entity/BufferedHttpEntity;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 597
    invoke-virtual {v0}, Lorg/apache/http/entity/BufferedHttpEntity;->getContentLength()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 598
    invoke-static {v0}, Lbwz;->b(Lorg/apache/http/HttpEntity;)V

    .line 599
    const/4 v0, 0x0

    .line 601
    :cond_0
    return-object v0
.end method

.method private a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 5

    .prologue
    .line 409
    iget-object v0, p0, Lbwz;->e:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 410
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 412
    sget-boolean v2, Lbwz;->a:Z

    if-eqz v2, :cond_0

    .line 413
    const-string v2, "GDataUploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GDataUploader: response code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :cond_0
    const/16 v2, 0x191

    if-eq v1, v2, :cond_1

    const/16 v2, 0x193

    if-ne v1, v2, :cond_6

    .line 443
    :cond_1
    :try_start_0
    iget-object v0, p0, Lbwz;->h:Lbwy;

    iget-object v1, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v1}, Lbcp;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbwz;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbwy;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbwz;->i:Ljava/lang/String;

    .line 444
    iget-object v0, p0, Lbwz;->i:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 445
    new-instance v0, Lbxi;

    const-string v1, "null auth token"

    invoke-direct {v0, v1}, Lbxi;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    .line 447
    :catch_0
    move-exception v0

    .line 449
    sget-boolean v1, Lbwz;->a:Z

    if-eqz v1, :cond_2

    .line 450
    const-string v1, "GDataUploader"

    const-string v2, "authentication canceled"

    invoke-static {v1, v2, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 452
    :cond_2
    new-instance v1, Lbxi;

    invoke-direct {v1, v0}, Lbxi;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 453
    :catch_1
    move-exception v0

    .line 456
    const-string v1, "GDataUploader"

    const-string v2, "authentication failed"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 457
    throw v0

    .line 458
    :catch_2
    move-exception v0

    .line 459
    const-string v1, "GDataUploader"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 460
    const-string v1, "GDataUploader"

    const-string v2, "AuthenticatorException"

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 462
    :cond_3
    new-instance v1, Lbxi;

    invoke-direct {v1, v0}, Lbxi;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 465
    :cond_4
    const-string v0, "Authorization"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GoogleLogin auth="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbwz;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    sget-boolean v0, Lbwz;->a:Z

    if-eqz v0, :cond_5

    .line 467
    const-string v0, "GDataUploader"

    const-string v1, "executeWithAuthRetry: attempt #2"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_5
    iget-object v0, p0, Lbwz;->e:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 471
    :cond_6
    return-object v0
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 492
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 496
    const-string v0, "\r\n\r\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 500
    if-lez v0, :cond_0

    .line 501
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 502
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object p1, v1

    .line 508
    :goto_0
    invoke-static {p1}, Lbwz;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 509
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 510
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 511
    invoke-virtual {v4, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    move-object v2, v3

    .line 504
    goto :goto_0

    .line 515
    :cond_1
    if-eqz v2, :cond_2

    .line 516
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v0, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 517
    invoke-virtual {v0, v3}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 518
    invoke-virtual {v4, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 520
    :cond_2
    sget-boolean v0, Lbwz;->a:Z

    if-eqz v0, :cond_3

    .line 521
    const-string v0, "GDataUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getInitialRequest "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    :cond_3
    return-object v4
.end method

.method private static a(Lbxa;)V
    .locals 2

    .prologue
    .line 605
    if-eqz p0, :cond_0

    const-string v0, "LimitQuota"

    iget-object v1, p0, Lbxa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    new-instance v0, Lbxg;

    iget-object v1, p0, Lbxa;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lbxg;-><init>(Ljava/lang/String;)V

    throw v0

    .line 608
    :cond_0
    return-void
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 13

    .prologue
    .line 305
    const/high16 v0, 0x40000

    new-array v3, v0, [B

    .line 306
    :goto_0
    iget-object v0, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0}, Lbcp;->n()J

    move-result-wide v0

    iget-object v2, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v2}, Lbcp;->m()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_c

    .line 308
    iget-object v0, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0}, Lbcp;->n()J

    move-result-wide v4

    .line 309
    iget-object v0, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0}, Lbcp;->m()J

    move-result-wide v0

    sub-long/2addr v0, v4

    long-to-int v0, v0

    .line 311
    sget-boolean v1, Lbwz;->a:Z

    if-eqz v1, :cond_0

    .line 312
    const-string v1, "GDataUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "GDataUploader: uploadChunks offset: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " length: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_0
    const/high16 v1, 0x40000

    if-gt v0, v1, :cond_4

    const/4 v1, 0x1

    .line 316
    :goto_1
    if-nez v1, :cond_1

    .line 317
    const/high16 v0, 0x40000

    .line 321
    :cond_1
    const/high16 v1, 0x40000

    invoke-virtual {p1, v1}, Ljava/io/InputStream;->mark(I)V

    .line 324
    :try_start_0
    invoke-static {p1, v3, v0}, Lbwz;->a(Ljava/io/InputStream;[BI)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    sget-boolean v1, Lbwz;->a:Z

    if-eqz v1, :cond_2

    .line 330
    const-string v1, "GDataUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "--- UPLOAD task: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " uploadUrl "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p0, Lbwz;->g:Lbcp;

    .line 331
    invoke-virtual {v6}, Lbcp;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 330
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_2
    iget-object v1, p0, Lbwz;->g:Lbcp;

    .line 336
    invoke-virtual {v1}, Lbcp;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbwz;->g:Lbcp;

    .line 337
    invoke-virtual {v2}, Lbcp;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lbwz;->g:Lbcp;

    .line 340
    invoke-virtual {v6}, Lbcp;->m()J

    move-result-wide v6

    .line 335
    new-instance v8, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v8, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    int-to-long v9, v0

    add-long/2addr v9, v4

    const-wide/16 v11, 0x1

    sub-long/2addr v9, v11

    const-string v1, "Content-Range"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "bytes "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v1, v6}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Content-Type"

    invoke-virtual {v8, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    new-instance v2, Lorg/apache/http/entity/InputStreamEntity;

    int-to-long v6, v0

    invoke-direct {v2, v1, v6, v7}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 342
    invoke-direct {p0, v8}, Lbwz;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 346
    :try_start_1
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 347
    invoke-static {v1}, Lbwz;->a(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 348
    invoke-static {v6}, Lbwz;->a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Lbwz;->a(Lorg/apache/http/HttpEntity;)Lbxa;

    move-result-object v0

    .line 349
    invoke-static {v0}, Lbwz;->a(Lbxa;)V

    .line 352
    sget-boolean v0, Lbwz;->a:Z

    if-eqz v0, :cond_3

    .line 353
    const-string v0, "GDataUploader"

    const-string v1, "UPLOAD_SUCCESS"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_3
    iget-object v0, p0, Lbwz;->g:Lbcp;

    iget-object v1, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v1}, Lbcp;->m()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lbcp;->a(J)V

    .line 356
    iget-object v0, p0, Lbwz;->j:Lbpk;

    iget-wide v1, p0, Lbwz;->f:J

    iget-object v4, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0, v1, v2, v4}, Lbpk;->a(JLbea;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395
    :goto_2
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Lbwz;->b(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_0

    .line 315
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 325
    :catch_0
    move-exception v0

    .line 326
    new-instance v1, Lbxe;

    invoke-direct {v1, v0}, Lbxe;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 357
    :cond_5
    :try_start_2
    invoke-static {v1}, Lbwz;->b(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 359
    const-string v1, "range"

    invoke-interface {v6, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 360
    if-eqz v1, :cond_6

    .line 361
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbwz;->a(Ljava/lang/String;)J

    move-result-wide v1

    .line 363
    :goto_3
    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-gez v7, :cond_7

    .line 364
    new-instance v0, Lbxj;

    const-string v1, "malformed or missing range header for subsequent upload"

    invoke-direct {v0, v1}, Lbxj;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 395
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-static {v1}, Lbwz;->b(Lorg/apache/http/HttpEntity;)V

    throw v0

    .line 361
    :cond_6
    const-wide/16 v1, -0x1

    goto :goto_3

    .line 369
    :cond_7
    int-to-long v7, v0

    add-long/2addr v4, v7

    cmp-long v0, v1, v4

    if-gez v0, :cond_8

    .line 371
    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    .line 372
    invoke-virtual {p1, v1, v2}, Ljava/io/InputStream;->skip(J)J

    .line 376
    :cond_8
    iget-object v0, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0, v1, v2}, Lbcp;->a(J)V

    .line 377
    iget-object v0, p0, Lbwz;->j:Lbpk;

    iget-wide v1, p0, Lbwz;->f:J

    iget-object v4, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0, v1, v2, v4}, Lbpk;->a(JLbea;)V

    goto :goto_2

    .line 378
    :cond_9
    const/16 v0, 0x190

    if-ne v1, v0, :cond_a

    .line 381
    new-instance v0, Lbxj;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "upload failed (bad payload, file too large) "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 383
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbxj;-><init>(Ljava/lang/String;)V

    throw v0

    .line 385
    :cond_a
    const/16 v0, 0x1f4

    if-lt v1, v0, :cond_b

    const/16 v0, 0x258

    if-ge v1, v0, :cond_b

    .line 388
    new-instance v0, Lbxh;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "upload transient error"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 389
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbxh;-><init>(Ljava/lang/String;)V

    throw v0

    .line 391
    :cond_b
    new-instance v0, Lbxj;

    .line 392
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbxj;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 397
    :cond_c
    new-instance v0, Lbxj;

    const-string v1, "upload is done but no server confirmation"

    invoke-direct {v0, v1}, Lbxj;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 559
    const/16 v0, 0xc8

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 620
    new-instance v2, Les;

    invoke-direct {v2}, Les;-><init>()V

    .line 621
    const-string v0, "\r\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 622
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 623
    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 624
    array-length v6, v5

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 625
    aget-object v6, v5, v1

    const/4 v7, 0x1

    aget-object v5, v5, v7

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 628
    :cond_1
    return-object v2
.end method

.method private b()V
    .locals 4

    .prologue
    .line 402
    iget-object v0, p0, Lbwz;->g:Lbcp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbcp;->a(Ljava/lang/String;)V

    .line 403
    iget-object v0, p0, Lbwz;->g:Lbcp;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lbcp;->a(J)V

    .line 404
    iget-object v0, p0, Lbwz;->j:Lbpk;

    iget-wide v1, p0, Lbwz;->f:J

    iget-object v3, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0, v1, v2, v3}, Lbpk;->a(JLbea;)V

    .line 405
    return-void
.end method

.method private static b(Lorg/apache/http/HttpEntity;)V
    .locals 1

    .prologue
    .line 611
    if-eqz p0, :cond_0

    .line 613
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 617
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 563
    const/16 v0, 0x134

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 749
    sget-object v0, Lbwz;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 752
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 753
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 752
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 757
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s/%s; %s/%s/%s/%s; %s/%s/%s/%d"

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v0, v3, v6

    const/4 v0, 0x2

    sget-object v4, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x3

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x4

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x5

    sget-object v4, Landroid/os/Build;->ID:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x6

    sget-object v4, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x7

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v4, v3, v0

    const/16 v0, 0x8

    sget-object v4, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    aput-object v4, v3, v0

    const/16 v0, 0x9

    .line 767
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    .line 757
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbwz;->d:Ljava/lang/String;

    .line 769
    :cond_0
    sget-object v0, Lbwz;->d:Ljava/lang/String;

    return-object v0

    .line 755
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getPackageInfo failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    iput-object v0, p0, Lbwz;->e:Lorg/apache/http/client/HttpClient;

    .line 199
    return-void
.end method

.method public a(JLbcp;)V
    .locals 10

    .prologue
    .line 115
    sget-boolean v0, Lbwz;->a:Z

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "GDataUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UploadService: GDataUploader: upload "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    invoke-virtual {p3}, Lbcp;->m()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 120
    new-instance v0, Lbxf;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Zero length file can\'t be uploaded"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lbxf;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 124
    :cond_1
    iput-wide p1, p0, Lbwz;->f:J

    .line 125
    iput-object p3, p0, Lbwz;->g:Lbcp;

    .line 128
    new-instance v0, Lbwy;

    const-string v1, "lh2"

    invoke-direct {v0, v1}, Lbwy;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbwz;->h:Lbwy;

    .line 130
    :try_start_0
    iget-object v0, p0, Lbwz;->h:Lbwy;

    iget-object v1, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v1}, Lbcp;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbwy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbwz;->i:Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    .line 154
    invoke-static {}, Lbwz;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbwz;->i:Ljava/lang/String;

    .line 155
    invoke-virtual {p3}, Lbcp;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lbcp;->k()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    .line 156
    invoke-virtual {p3}, Lbcp;->l()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p3}, Lbcp;->h()Ljava/lang/String;

    move-result-object v5

    .line 157
    invoke-virtual {p3}, Lbcp;->m()J

    move-result-wide v6

    .line 154
    const-string v8, "/"

    invoke-virtual {v0, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    const-string v9, "?"

    invoke-virtual {v0, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    if-lez v8, :cond_c

    if-lez v9, :cond_b

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Authorization: GoogleLogin auth="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "\r\nUser-Agent: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\nGData-Version: 2.0\r\nSlug: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\nX-Upload-Content-Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\nX-Upload-Content-Length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\nHost: upload.photos.google.com\r\nContent-Type: application/atom+xml; charset=UTF-8\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\r\n<entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:gphoto=\'http://schemas.google.com/photos/2007\'><category scheme=\'http://schemas.google.com/g/2005#kind\' term=\'http://schemas.google.com/photos/2007#photo\'/>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "<title>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lbxk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</title>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "<summary>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-static {v0}, Lbxk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</summary>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v0, "<gphoto:timestamp>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "</gphoto:timestamp>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "<gphoto:streamId>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</gphoto:streamId>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const-string v0, "<gphoto:streamId>mobile_uploaded</gphoto:streamId>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</entry>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-virtual {p3}, Lbcp;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lbcp;->h()Ljava/lang/String;

    move-result-object v2

    .line 159
    invoke-virtual {p3}, Lbcp;->h()Ljava/lang/String;

    .line 158
    sget-object v3, Lbwz;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v4, "caid"

    invoke-virtual {v1, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "xmlerrors"

    const-string v4, "1"

    invoke-virtual {v1, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "fres"

    const-string v2, "true"

    invoke-virtual {v3, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    sget-boolean v1, Lbwz;->a:Z

    if-eqz v1, :cond_5

    .line 162
    const-string v1, "GDataUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "GDataUploader: about to upload to url "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_5
    const/4 v2, 0x0

    .line 167
    :try_start_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 168
    invoke-virtual {p3}, Lbcp;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    .line 169
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 170
    :try_start_2
    iget-object v2, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v2}, Lbcp;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 171
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-boolean v3, Lbwz;->a:Z

    if-eqz v3, :cond_6

    const-string v3, "GDataUploader"

    const-string v4, "GDataUploader: start"

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-static {v2, v0}, Lbwz;->a(Landroid/net/Uri;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lbwz;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-static {v0}, Lbwz;->a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v3

    :try_start_3
    invoke-static {v3}, Lbwz;->a(I)Z

    move-result v4

    if-eqz v4, :cond_d

    if-eqz v2, :cond_7

    invoke-static {v2}, Lbwz;->a(Lorg/apache/http/HttpEntity;)Lbxa;

    move-result-object v3

    invoke-static {v3}, Lbwz;->a(Lbxa;)V

    :cond_7
    const-string v3, "Location"

    invoke-interface {v0, v3}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    iget-object v3, p0, Lbwz;->g:Lbcp;

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbcp;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lbwz;->g:Lbcp;

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v3, v4}, Lbcp;->a(J)V

    iget-object v0, p0, Lbwz;->j:Lbpk;

    iget-wide v3, p0, Lbwz;->f:J

    iget-object v5, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0, v3, v4, v5}, Lbpk;->a(JLbea;)V

    invoke-direct {p0, v1}, Lbwz;->a(Ljava/io/InputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-static {v2}, Lbwz;->b(Lorg/apache/http/HttpEntity;)V
    :try_end_4
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 187
    :goto_1
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_6

    .line 193
    :goto_2
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 133
    sget-boolean v1, Lbwz;->a:Z

    if-eqz v1, :cond_8

    .line 134
    const-string v1, "GDataUploader"

    const-string v2, "authentication canceled"

    invoke-static {v1, v2, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 136
    :cond_8
    new-instance v1, Lbxi;

    invoke-direct {v1, v0}, Lbxi;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 137
    :catch_1
    move-exception v0

    .line 140
    sget-boolean v1, Lbwz;->a:Z

    if-eqz v1, :cond_9

    .line 141
    const-string v1, "GDataUploader"

    const-string v2, "authentication failed"

    invoke-static {v1, v2, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    :cond_9
    throw v0

    .line 144
    :catch_2
    move-exception v0

    .line 145
    const-string v1, "GDataUploader"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 146
    const-string v1, "GDataUploader"

    const-string v2, "AuthenticationException"

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 148
    :cond_a
    new-instance v1, Lbxi;

    invoke-direct {v1, v0}, Lbxi;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 154
    :cond_b
    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    if-lez v9, :cond_2

    const/4 v8, 0x0

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 171
    :cond_d
    const/16 v4, 0x190

    if-ne v3, v4, :cond_f

    :try_start_6
    new-instance v3, Lbxj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "upload failed (bad payload or file too large) "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lbxj;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_7
    invoke-static {v2}, Lbwz;->b(Lorg/apache/http/HttpEntity;)V

    throw v0
    :try_end_7
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 175
    :catch_3
    move-exception v0

    .line 176
    :goto_3
    :try_start_8
    new-instance v2, Ljava/io/IOException;

    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 186
    :catchall_1
    move-exception v0

    .line 187
    :goto_4
    if-eqz v1, :cond_e

    .line 188
    :try_start_9
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_7

    .line 192
    :cond_e
    :goto_5
    throw v0

    .line 171
    :cond_f
    const/16 v4, 0x191

    if-ne v3, v4, :cond_10

    :try_start_a
    new-instance v3, Lbxi;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lbxi;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_10
    const/16 v4, 0x1f4

    if-lt v3, v4, :cond_11

    const/16 v4, 0x258

    if-ge v3, v4, :cond_11

    new-instance v3, Lbxh;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "upload transient error:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lbxh;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_11
    new-instance v3, Lbxj;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lbxj;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 173
    :cond_12
    :try_start_b
    iget-object v0, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0}, Lbcp;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    const-string v0, "Content-Range"

    const-string v3, "bytes */*"

    invoke-virtual {v2, v0, v3}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lbwz;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-static {v0}, Lbwz;->a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v3

    if-nez v3, :cond_13

    sget-boolean v4, Lbwz;->a:Z

    if-eqz v4, :cond_13

    const-string v4, "GDataUploader"

    const-string v5, "  Entity: content length was 0."

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_13
    :try_start_c
    invoke-static {v2}, Lbwz;->b(I)Z

    move-result v4

    if-eqz v4, :cond_16

    const-string v4, "range"

    invoke-interface {v0, v4}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    const-string v2, "range"

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbwz;->a(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-gez v2, :cond_14

    invoke-direct {p0}, Lbwz;->b()V

    new-instance v2, Lbxh;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "negative range offset: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lbxh;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :catchall_2
    move-exception v0

    :try_start_d
    invoke-static {v3}, Lbwz;->b(Lorg/apache/http/HttpEntity;)V

    throw v0
    :try_end_d
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 177
    :catch_4
    move-exception v0

    .line 178
    :goto_6
    :try_start_e
    new-instance v2, Lbxe;

    invoke-direct {v2, v0}, Lbxe;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 173
    :cond_14
    :try_start_f
    invoke-virtual {v1, v4, v5}, Ljava/io/InputStream;->skip(J)J

    const/high16 v0, 0x40000

    invoke-virtual {v1, v0}, Ljava/io/InputStream;->mark(I)V

    iget-object v0, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0, v4, v5}, Lbcp;->a(J)V

    iget-object v0, p0, Lbwz;->j:Lbpk;

    iget-wide v4, p0, Lbwz;->f:J

    iget-object v2, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0, v4, v5, v2}, Lbpk;->a(JLbea;)V

    invoke-direct {p0, v1}, Lbwz;->a(Ljava/io/InputStream;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    :goto_7
    :try_start_10
    invoke-static {v3}, Lbwz;->b(Lorg/apache/http/HttpEntity;)V
    :try_end_10
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_1

    .line 179
    :catch_5
    move-exception v0

    .line 181
    :goto_8
    :try_start_11
    const-string v2, "GDataUploader"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 182
    const-string v2, "GDataUploader"

    const-string v3, "error in parsing response"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    :cond_15
    new-instance v2, Lbxj;

    const-string v3, "error in parsing response"

    invoke-direct {v2, v3, v0}, Lbxj;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 173
    :cond_16
    :try_start_12
    invoke-static {v2}, Lbwz;->a(I)Z

    move-result v4

    if-eqz v4, :cond_18

    invoke-static {v3}, Lbwz;->a(Lorg/apache/http/HttpEntity;)Lbxa;

    move-result-object v0

    invoke-static {v0}, Lbwz;->a(Lbxa;)V

    const-string v0, "GDataUploader"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "GDataUploader"

    const-string v2, "nothing to resume, upload already completed"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_17
    iget-object v0, p0, Lbwz;->g:Lbcp;

    iget-object v2, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v2}, Lbcp;->m()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lbcp;->a(J)V

    iget-object v0, p0, Lbwz;->j:Lbpk;

    iget-wide v4, p0, Lbwz;->f:J

    iget-object v2, p0, Lbwz;->g:Lbcp;

    invoke-virtual {v0, v4, v5, v2}, Lbpk;->a(JLbea;)V

    goto :goto_7

    :cond_18
    const/16 v4, 0x191

    if-ne v2, v4, :cond_19

    new-instance v2, Lbxi;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lbxi;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_19
    invoke-direct {p0}, Lbwz;->b()V

    new-instance v2, Lbxh;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "unexpected resume response: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lbxh;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    .line 190
    :catch_6
    move-exception v0

    .line 191
    const-string v1, "GDataUploader"

    const-string v2, "close fail "

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 190
    :catch_7
    move-exception v1

    .line 191
    const-string v2, "GDataUploader"

    const-string v3, "close fail "

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    .line 186
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto/16 :goto_4

    .line 179
    :catch_8
    move-exception v0

    move-object v1, v2

    goto/16 :goto_8

    .line 177
    :catch_9
    move-exception v0

    move-object v1, v2

    goto/16 :goto_6

    .line 175
    :catch_a
    move-exception v0

    move-object v1, v2

    goto/16 :goto_3
.end method
