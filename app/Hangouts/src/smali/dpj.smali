.class public final Ldpj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpj;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/Long;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/Long;

.field public o:Ljava/lang/Integer;

.field public p:Ljava/lang/Integer;

.field public q:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7865
    const/4 v0, 0x0

    new-array v0, v0, [Ldpj;

    sput-object v0, Ldpj;->a:[Ldpj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7866
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7906
    iput-object v0, p0, Ldpj;->b:Ljava/lang/Integer;

    .line 7923
    iput-object v0, p0, Ldpj;->j:Ljava/lang/Integer;

    .line 7926
    iput-object v0, p0, Ldpj;->k:Ljava/lang/Integer;

    .line 7935
    iput-object v0, p0, Ldpj;->o:Ljava/lang/Integer;

    .line 7940
    iput-object v0, p0, Ldpj;->q:Ljava/lang/Integer;

    .line 7866
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 7999
    const/4 v0, 0x0

    .line 8000
    iget-object v1, p0, Ldpj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 8001
    const/4 v0, 0x1

    iget-object v1, p0, Ldpj;->b:Ljava/lang/Integer;

    .line 8002
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8004
    :cond_0
    iget-object v1, p0, Ldpj;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 8005
    const/4 v1, 0x2

    iget-object v2, p0, Ldpj;->c:Ljava/lang/Long;

    .line 8006
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8008
    :cond_1
    iget-object v1, p0, Ldpj;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 8009
    const/4 v1, 0x3

    iget-object v2, p0, Ldpj;->d:Ljava/lang/String;

    .line 8010
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8012
    :cond_2
    iget-object v1, p0, Ldpj;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 8013
    const/4 v1, 0x4

    iget-object v2, p0, Ldpj;->e:Ljava/lang/Boolean;

    .line 8014
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8016
    :cond_3
    iget-object v1, p0, Ldpj;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 8017
    const/4 v1, 0x5

    iget-object v2, p0, Ldpj;->f:Ljava/lang/Boolean;

    .line 8018
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8020
    :cond_4
    iget-object v1, p0, Ldpj;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 8021
    const/4 v1, 0x6

    iget-object v2, p0, Ldpj;->g:Ljava/lang/Boolean;

    .line 8022
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8024
    :cond_5
    iget-object v1, p0, Ldpj;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 8025
    const/4 v1, 0x7

    iget-object v2, p0, Ldpj;->h:Ljava/lang/Boolean;

    .line 8026
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8028
    :cond_6
    iget-object v1, p0, Ldpj;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 8029
    const/16 v1, 0x8

    iget-object v2, p0, Ldpj;->i:Ljava/lang/Boolean;

    .line 8030
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8032
    :cond_7
    iget-object v1, p0, Ldpj;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 8033
    const/16 v1, 0x9

    iget-object v2, p0, Ldpj;->j:Ljava/lang/Integer;

    .line 8034
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8036
    :cond_8
    iget-object v1, p0, Ldpj;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 8037
    const/16 v1, 0xa

    iget-object v2, p0, Ldpj;->k:Ljava/lang/Integer;

    .line 8038
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8040
    :cond_9
    iget-object v1, p0, Ldpj;->l:Ljava/lang/Long;

    if-eqz v1, :cond_a

    .line 8041
    const/16 v1, 0xb

    iget-object v2, p0, Ldpj;->l:Ljava/lang/Long;

    .line 8042
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8044
    :cond_a
    iget-object v1, p0, Ldpj;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 8045
    const/16 v1, 0xc

    iget-object v2, p0, Ldpj;->m:Ljava/lang/String;

    .line 8046
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8048
    :cond_b
    iget-object v1, p0, Ldpj;->n:Ljava/lang/Long;

    if-eqz v1, :cond_c

    .line 8049
    const/16 v1, 0xd

    iget-object v2, p0, Ldpj;->n:Ljava/lang/Long;

    .line 8050
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8052
    :cond_c
    iget-object v1, p0, Ldpj;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 8053
    const/16 v1, 0xe

    iget-object v2, p0, Ldpj;->o:Ljava/lang/Integer;

    .line 8054
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8056
    :cond_d
    iget-object v1, p0, Ldpj;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 8057
    const/16 v1, 0xf

    iget-object v2, p0, Ldpj;->p:Ljava/lang/Integer;

    .line 8058
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8060
    :cond_e
    iget-object v1, p0, Ldpj;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 8061
    const/16 v1, 0x10

    iget-object v2, p0, Ldpj;->q:Ljava/lang/Integer;

    .line 8062
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8064
    :cond_f
    iget-object v1, p0, Ldpj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8065
    iput v0, p0, Ldpj;->cachedSize:I

    .line 8066
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/16 v5, 0xa

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 7862
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldpj;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldpj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldpj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    if-eq v0, v5, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpj;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpj;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpj;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpj;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpj;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpj;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpj;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v2, :cond_4

    if-ne v0, v4, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v5, :cond_6

    const/16 v1, 0x14

    if-eq v0, v1, :cond_6

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpj;->l:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpj;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpj;->n:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_8

    const/16 v1, 0x64

    if-eq v0, v1, :cond_8

    const/16 v1, 0x65

    if-eq v0, v1, :cond_8

    const/16 v1, 0x2bd

    if-eq v0, v1, :cond_8

    const/16 v1, 0x2be

    if-eq v0, v1, :cond_8

    const/16 v1, 0x2bf

    if-eq v0, v1, :cond_8

    const/16 v1, 0x2c0

    if-eq v0, v1, :cond_8

    const/16 v1, 0x2c1

    if-eq v0, v1, :cond_8

    const/16 v1, 0x2c2

    if-ne v0, v1, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_a

    if-eq v0, v2, :cond_a

    if-eq v0, v4, :cond_a

    if-eq v0, v6, :cond_a

    const/4 v1, 0x4

    if-eq v0, v1, :cond_a

    const/4 v1, 0x5

    if-ne v0, v1, :cond_b

    :cond_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_b
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpj;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 7945
    iget-object v0, p0, Ldpj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 7946
    const/4 v0, 0x1

    iget-object v1, p0, Ldpj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7948
    :cond_0
    iget-object v0, p0, Ldpj;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 7949
    const/4 v0, 0x2

    iget-object v1, p0, Ldpj;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 7951
    :cond_1
    iget-object v0, p0, Ldpj;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 7952
    const/4 v0, 0x3

    iget-object v1, p0, Ldpj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7954
    :cond_2
    iget-object v0, p0, Ldpj;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 7955
    const/4 v0, 0x4

    iget-object v1, p0, Ldpj;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7957
    :cond_3
    iget-object v0, p0, Ldpj;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 7958
    const/4 v0, 0x5

    iget-object v1, p0, Ldpj;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7960
    :cond_4
    iget-object v0, p0, Ldpj;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 7961
    const/4 v0, 0x6

    iget-object v1, p0, Ldpj;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7963
    :cond_5
    iget-object v0, p0, Ldpj;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 7964
    const/4 v0, 0x7

    iget-object v1, p0, Ldpj;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7966
    :cond_6
    iget-object v0, p0, Ldpj;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 7967
    const/16 v0, 0x8

    iget-object v1, p0, Ldpj;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7969
    :cond_7
    iget-object v0, p0, Ldpj;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 7970
    const/16 v0, 0x9

    iget-object v1, p0, Ldpj;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7972
    :cond_8
    iget-object v0, p0, Ldpj;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 7973
    const/16 v0, 0xa

    iget-object v1, p0, Ldpj;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7975
    :cond_9
    iget-object v0, p0, Ldpj;->l:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 7976
    const/16 v0, 0xb

    iget-object v1, p0, Ldpj;->l:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 7978
    :cond_a
    iget-object v0, p0, Ldpj;->m:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 7979
    const/16 v0, 0xc

    iget-object v1, p0, Ldpj;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7981
    :cond_b
    iget-object v0, p0, Ldpj;->n:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 7982
    const/16 v0, 0xd

    iget-object v1, p0, Ldpj;->n:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 7984
    :cond_c
    iget-object v0, p0, Ldpj;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 7985
    const/16 v0, 0xe

    iget-object v1, p0, Ldpj;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7987
    :cond_d
    iget-object v0, p0, Ldpj;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 7988
    const/16 v0, 0xf

    iget-object v1, p0, Ldpj;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7990
    :cond_e
    iget-object v0, p0, Ldpj;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 7991
    const/16 v0, 0x10

    iget-object v1, p0, Ldpj;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7993
    :cond_f
    iget-object v0, p0, Ldpj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7995
    return-void
.end method
