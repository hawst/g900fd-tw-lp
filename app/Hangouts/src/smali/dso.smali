.class public final Ldso;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldso;


# instance fields
.field public b:Ldrp;

.field public c:[Ldro;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20983
    const/4 v0, 0x0

    new-array v0, v0, [Ldso;

    sput-object v0, Ldso;->a:[Ldso;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20984
    invoke-direct {p0}, Lepn;-><init>()V

    .line 20987
    const/4 v0, 0x0

    iput-object v0, p0, Ldso;->b:Ldrp;

    .line 20990
    sget-object v0, Ldro;->a:[Ldro;

    iput-object v0, p0, Ldso;->c:[Ldro;

    .line 20984
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 21012
    iget-object v0, p0, Ldso;->b:Ldrp;

    if-eqz v0, :cond_2

    .line 21013
    const/4 v0, 0x1

    iget-object v2, p0, Ldso;->b:Ldrp;

    .line 21014
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21016
    :goto_0
    iget-object v2, p0, Ldso;->c:[Ldro;

    if-eqz v2, :cond_1

    .line 21017
    iget-object v2, p0, Ldso;->c:[Ldro;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 21018
    if-eqz v4, :cond_0

    .line 21019
    const/4 v5, 0x2

    .line 21020
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 21017
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 21024
    :cond_1
    iget-object v1, p0, Ldso;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21025
    iput v0, p0, Ldso;->cachedSize:I

    .line 21026
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20980
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldso;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldso;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldso;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldso;->b:Ldrp;

    if-nez v0, :cond_2

    new-instance v0, Ldrp;

    invoke-direct {v0}, Ldrp;-><init>()V

    iput-object v0, p0, Ldso;->b:Ldrp;

    :cond_2
    iget-object v0, p0, Ldso;->b:Ldrp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldso;->c:[Ldro;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldro;

    iget-object v3, p0, Ldso;->c:[Ldro;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldso;->c:[Ldro;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldso;->c:[Ldro;

    :goto_2
    iget-object v2, p0, Ldso;->c:[Ldro;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldso;->c:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldso;->c:[Ldro;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldso;->c:[Ldro;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldso;->c:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldso;->c:[Ldro;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 20995
    iget-object v0, p0, Ldso;->b:Ldrp;

    if-eqz v0, :cond_0

    .line 20996
    const/4 v0, 0x1

    iget-object v1, p0, Ldso;->b:Ldrp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 20998
    :cond_0
    iget-object v0, p0, Ldso;->c:[Ldro;

    if-eqz v0, :cond_2

    .line 20999
    iget-object v1, p0, Ldso;->c:[Ldro;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 21000
    if-eqz v3, :cond_1

    .line 21001
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 20999
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21005
    :cond_2
    iget-object v0, p0, Ldso;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 21007
    return-void
.end method
