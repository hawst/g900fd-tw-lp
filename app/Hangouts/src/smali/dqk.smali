.class public final Ldqk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqk;


# instance fields
.field public b:Ldqf;

.field public c:[Ldth;

.field public d:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10433
    const/4 v0, 0x0

    new-array v0, v0, [Ldqk;

    sput-object v0, Ldqk;->a:[Ldqk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10434
    invoke-direct {p0}, Lepn;-><init>()V

    .line 10437
    const/4 v0, 0x0

    iput-object v0, p0, Ldqk;->b:Ldqf;

    .line 10440
    sget-object v0, Ldth;->a:[Ldth;

    iput-object v0, p0, Ldqk;->c:[Ldth;

    .line 10443
    sget-object v0, Lept;->k:[[B

    iput-object v0, p0, Ldqk;->d:[[B

    .line 10434
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 10470
    iget-object v0, p0, Ldqk;->b:Ldqf;

    if-eqz v0, :cond_4

    .line 10471
    const/4 v0, 0x1

    iget-object v2, p0, Ldqk;->b:Ldqf;

    .line 10472
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10474
    :goto_0
    iget-object v2, p0, Ldqk;->d:[[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldqk;->d:[[B

    array-length v2, v2

    if-lez v2, :cond_1

    .line 10476
    iget-object v4, p0, Ldqk;->d:[[B

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 10478
    invoke-static {v6}, Lepl;->b([B)I

    move-result v6

    add-int/2addr v3, v6

    .line 10476
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 10480
    :cond_0
    add-int/2addr v0, v3

    .line 10481
    iget-object v2, p0, Ldqk;->d:[[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 10483
    :cond_1
    iget-object v2, p0, Ldqk;->c:[Ldth;

    if-eqz v2, :cond_3

    .line 10484
    iget-object v2, p0, Ldqk;->c:[Ldth;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 10485
    if-eqz v4, :cond_2

    .line 10486
    const/4 v5, 0x3

    .line 10487
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 10484
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 10491
    :cond_3
    iget-object v1, p0, Ldqk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10492
    iput v0, p0, Ldqk;->cachedSize:I

    .line 10493
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10430
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldqk;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldqk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldqk;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqk;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldqk;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldqk;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqk;->d:[[B

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [[B

    iget-object v3, p0, Ldqk;->d:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldqk;->d:[[B

    :goto_1
    iget-object v2, p0, Ldqk;->d:[[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ldqk;->d:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Ldqk;->d:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqk;->c:[Ldth;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldth;

    iget-object v3, p0, Ldqk;->c:[Ldth;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldqk;->c:[Ldth;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldqk;->c:[Ldth;

    :goto_3
    iget-object v2, p0, Ldqk;->c:[Ldth;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldqk;->c:[Ldth;

    new-instance v3, Ldth;

    invoke-direct {v3}, Ldth;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqk;->c:[Ldth;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v0, p0, Ldqk;->c:[Ldth;

    array-length v0, v0

    goto :goto_2

    :cond_6
    iget-object v2, p0, Ldqk;->c:[Ldth;

    new-instance v3, Ldth;

    invoke-direct {v3}, Ldth;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqk;->c:[Ldth;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 10448
    iget-object v1, p0, Ldqk;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 10449
    const/4 v1, 0x1

    iget-object v2, p0, Ldqk;->b:Ldqf;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 10451
    :cond_0
    iget-object v1, p0, Ldqk;->d:[[B

    if-eqz v1, :cond_1

    .line 10452
    iget-object v2, p0, Ldqk;->d:[[B

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 10453
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->a(I[B)V

    .line 10452
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 10456
    :cond_1
    iget-object v1, p0, Ldqk;->c:[Ldth;

    if-eqz v1, :cond_3

    .line 10457
    iget-object v1, p0, Ldqk;->c:[Ldth;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 10458
    if-eqz v3, :cond_2

    .line 10459
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 10457
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 10463
    :cond_3
    iget-object v0, p0, Ldqk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 10465
    return-void
.end method
