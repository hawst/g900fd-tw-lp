.class public final Lddf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lddf;


# instance fields
.field public b:Ldcv;

.field public c:Ldny;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 564
    const/4 v0, 0x0

    new-array v0, v0, [Lddf;

    sput-object v0, Lddf;->a:[Lddf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 565
    invoke-direct {p0}, Lepn;-><init>()V

    .line 568
    iput-object v0, p0, Lddf;->b:Ldcv;

    .line 571
    iput-object v0, p0, Lddf;->c:Ldny;

    .line 565
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 588
    const/4 v0, 0x0

    .line 589
    iget-object v1, p0, Lddf;->b:Ldcv;

    if-eqz v1, :cond_0

    .line 590
    const/4 v0, 0x1

    iget-object v1, p0, Lddf;->b:Ldcv;

    .line 591
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 593
    :cond_0
    iget-object v1, p0, Lddf;->c:Ldny;

    if-eqz v1, :cond_1

    .line 594
    const/4 v1, 0x2

    iget-object v2, p0, Lddf;->c:Ldny;

    .line 595
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 597
    :cond_1
    iget-object v1, p0, Lddf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 598
    iput v0, p0, Lddf;->cachedSize:I

    .line 599
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 561
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lddf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lddf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lddf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lddf;->b:Ldcv;

    if-nez v0, :cond_2

    new-instance v0, Ldcv;

    invoke-direct {v0}, Ldcv;-><init>()V

    iput-object v0, p0, Lddf;->b:Ldcv;

    :cond_2
    iget-object v0, p0, Lddf;->b:Ldcv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lddf;->c:Ldny;

    if-nez v0, :cond_3

    new-instance v0, Ldny;

    invoke-direct {v0}, Ldny;-><init>()V

    iput-object v0, p0, Lddf;->c:Ldny;

    :cond_3
    iget-object v0, p0, Lddf;->c:Ldny;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Lddf;->b:Ldcv;

    if-eqz v0, :cond_0

    .line 577
    const/4 v0, 0x1

    iget-object v1, p0, Lddf;->b:Ldcv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 579
    :cond_0
    iget-object v0, p0, Lddf;->c:Ldny;

    if-eqz v0, :cond_1

    .line 580
    const/4 v0, 0x2

    iget-object v1, p0, Lddf;->c:Ldny;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 582
    :cond_1
    iget-object v0, p0, Lddf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 584
    return-void
.end method
