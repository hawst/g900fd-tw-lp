.class final Larb;
.super Landroid/view/animation/Animation;
.source "PG"


# instance fields
.field final synthetic a:Landroid/widget/RelativeLayout$LayoutParams;

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:Laqv;


# direct methods
.method constructor <init>(Laqv;Landroid/widget/RelativeLayout$LayoutParams;II)V
    .locals 0

    .prologue
    .line 664
    iput-object p1, p0, Larb;->d:Laqv;

    iput-object p2, p0, Larb;->a:Landroid/widget/RelativeLayout$LayoutParams;

    iput p3, p0, Larb;->b:I

    iput p4, p0, Larb;->c:I

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 667
    iget-object v0, p0, Larb;->a:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Larb;->b:I

    iget v2, p0, Larb;->c:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 668
    iget-object v0, p0, Larb;->d:Laqv;

    iget-object v0, v0, Laqv;->i:Landroid/view/ViewGroup;

    iget-object v1, p0, Larb;->a:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 669
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 670
    iget-object v0, p0, Larb;->d:Laqv;

    invoke-virtual {v0, p1}, Laqv;->a(F)V

    .line 672
    :cond_0
    iget-object v0, p0, Larb;->d:Laqv;

    iget-object v0, v0, Laqv;->j:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->e()V

    .line 673
    return-void
.end method
