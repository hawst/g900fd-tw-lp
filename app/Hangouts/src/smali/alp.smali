.class public final Lalp;
.super Le;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
    .locals 6

    .prologue
    .line 302
    iput-object p1, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Le;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 338
    invoke-super {p0, p1}, Le;->a(I)V

    .line 339
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->h(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Ljava/lang/Runnable;

    .line 346
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 316
    invoke-super {p0, p1}, Le;->a(Landroid/view/View;)V

    .line 317
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->u_()V

    .line 318
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->f(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V

    .line 319
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->g(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lbme;

    move-result-object v0

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x616

    .line 320
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 319
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 321
    return-void
.end method

.method public final a(Landroid/view/View;F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 305
    invoke-super {p0, p1, p2}, Le;->a(Landroid/view/View;F)V

    .line 306
    cmpl-float v0, p2, v1

    if-nez v0, :cond_1

    .line 307
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->a(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;Z)Z

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->e(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    cmpl-float v0, p2, v1

    if-lez v0, :cond_0

    .line 309
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->a(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;Z)Z

    .line 310
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lf;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 325
    invoke-super {p0, p1}, Le;->b(Landroid/view/View;)V

    .line 326
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->b(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lalt;

    move-result-object v0

    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v1

    invoke-static {v0, v1}, Lalt;->a(Lalt;Lyj;)V

    .line 327
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->u_()V

    .line 328
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->g(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lbme;

    move-result-object v0

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x617

    .line 329
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 328
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 330
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->h(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->h(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 332
    iget-object v0, p0, Lalp;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Ljava/lang/Runnable;

    .line 334
    :cond_0
    return-void
.end method
