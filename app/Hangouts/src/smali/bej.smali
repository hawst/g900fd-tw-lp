.class public Lbej;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4430
    invoke-direct {p0}, Lbep;-><init>()V

    .line 4431
    iput-object p1, p0, Lbej;->a:Ljava/lang/String;

    .line 4432
    iput-object p2, p0, Lbej;->b:Ljava/lang/String;

    .line 4433
    iput-object p3, p0, Lbej;->c:Ljava/util/ArrayList;

    .line 4434
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4438
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 4439
    const-string v0, "Babel_RequestWriter"

    const-string v1, "FinishPhoneVerificationRequest build protobuf "

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4442
    :cond_0
    new-instance v7, Ldsh;

    invoke-direct {v7}, Ldsh;-><init>()V

    .line 4444
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v7, Ldsh;->e:Ljava/lang/Boolean;

    .line 4447
    iget-object v0, p0, Lbej;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4448
    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    .line 4449
    iget-object v1, p0, Lbej;->a:Ljava/lang/String;

    iput-object v1, v0, Leir;->b:Ljava/lang/String;

    .line 4451
    iput-object v0, v7, Ldsh;->c:Leir;

    .line 4452
    iget-object v0, p0, Lbej;->b:Ljava/lang/String;

    iput-object v0, v7, Ldsh;->d:Ljava/lang/String;

    move v1, v2

    .line 4464
    :goto_0
    iget-object v0, p0, Lbej;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_c

    move v4, v2

    move v5, v2

    move v6, v2

    .line 4466
    :goto_1
    iget-object v0, p0, Lbej;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 4467
    iget-object v0, p0, Lbej;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4468
    iget-object v8, p0, Lbej;->a:Ljava/lang/String;

    invoke-static {v0, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4469
    if-eqz v1, :cond_2

    move v5, v3

    .line 4474
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 4466
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_3
    move v1, v3

    .line 4456
    goto :goto_0

    :cond_4
    move v3, v6

    .line 4478
    :goto_2
    const/4 v0, 0x0

    .line 4480
    if-eqz v1, :cond_b

    if-nez v5, :cond_b

    .line 4481
    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    .line 4482
    iget-object v4, p0, Lbej;->a:Ljava/lang/String;

    iput-object v4, v0, Leir;->b:Ljava/lang/String;

    .line 4483
    add-int/lit8 v3, v3, 0x1

    move-object v4, v0

    move v5, v3

    .line 4486
    :goto_3
    if-lez v5, :cond_7

    .line 4487
    new-array v0, v5, [Leir;

    iput-object v0, v7, Ldsh;->f:[Leir;

    .line 4490
    iget-object v0, p0, Lbej;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_a

    move v3, v2

    .line 4492
    :goto_4
    iget-object v0, p0, Lbej;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 4493
    iget-object v0, p0, Lbej;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4494
    iget-object v6, p0, Lbej;->a:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 4495
    if-eqz v1, :cond_9

    .line 4496
    :cond_5
    iget-object v6, v7, Ldsh;->f:[Leir;

    new-instance v8, Leir;

    invoke-direct {v8}, Leir;-><init>()V

    aput-object v8, v6, v3

    .line 4501
    iget-object v6, v7, Ldsh;->f:[Leir;

    aget-object v6, v6, v3

    iput-object v0, v6, Leir;->b:Ljava/lang/String;

    .line 4502
    add-int/lit8 v0, v3, 0x1

    .line 4492
    :goto_5
    add-int/lit8 v2, v2, 0x1

    move v3, v0

    goto :goto_4

    :cond_6
    move v1, v3

    .line 4505
    :goto_6
    if-eqz v4, :cond_8

    .line 4506
    iget-object v2, v7, Ldsh;->f:[Leir;

    add-int/lit8 v0, v1, 0x1

    aput-object v4, v2, v1

    .line 4508
    :goto_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 4511
    :cond_7
    return-object v7

    :cond_8
    move v0, v1

    goto :goto_7

    :cond_9
    move v0, v3

    goto :goto_5

    :cond_a
    move v1, v2

    goto :goto_6

    :cond_b
    move-object v4, v0

    move v5, v3

    goto :goto_3

    :cond_c
    move v5, v2

    move v3, v2

    goto :goto_2
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 4523
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    const/16 v1, 0x69

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbmz;->a(IZ)V

    .line 4524
    return-void
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 4518
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4528
    const-string v0, "devices/finishphonenumberverification"

    return-object v0
.end method
