.class public final Ldge;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldge;


# instance fields
.field public b:Ldfi;

.field public c:Lden;

.field public d:Ljava/lang/String;

.field public e:Ldez;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6500
    const/4 v0, 0x0

    new-array v0, v0, [Ldge;

    sput-object v0, Ldge;->a:[Ldge;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6501
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6504
    iput-object v0, p0, Ldge;->b:Ldfi;

    .line 6507
    iput-object v0, p0, Ldge;->c:Lden;

    .line 6512
    iput-object v0, p0, Ldge;->e:Ldez;

    .line 6501
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 6535
    const/4 v0, 0x0

    .line 6536
    iget-object v1, p0, Ldge;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 6537
    const/4 v0, 0x1

    iget-object v1, p0, Ldge;->b:Ldfi;

    .line 6538
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6540
    :cond_0
    iget-object v1, p0, Ldge;->c:Lden;

    if-eqz v1, :cond_1

    .line 6541
    const/4 v1, 0x2

    iget-object v2, p0, Ldge;->c:Lden;

    .line 6542
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6544
    :cond_1
    iget-object v1, p0, Ldge;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6545
    const/4 v1, 0x3

    iget-object v2, p0, Ldge;->d:Ljava/lang/String;

    .line 6546
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6548
    :cond_2
    iget-object v1, p0, Ldge;->e:Ldez;

    if-eqz v1, :cond_3

    .line 6549
    const/4 v1, 0x4

    iget-object v2, p0, Ldge;->e:Ldez;

    .line 6550
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6552
    :cond_3
    iget-object v1, p0, Ldge;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6553
    iput v0, p0, Ldge;->cachedSize:I

    .line 6554
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6497
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldge;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldge;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldge;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldge;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldge;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldge;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldge;->c:Lden;

    if-nez v0, :cond_3

    new-instance v0, Lden;

    invoke-direct {v0}, Lden;-><init>()V

    iput-object v0, p0, Ldge;->c:Lden;

    :cond_3
    iget-object v0, p0, Ldge;->c:Lden;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldge;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldge;->e:Ldez;

    if-nez v0, :cond_4

    new-instance v0, Ldez;

    invoke-direct {v0}, Ldez;-><init>()V

    iput-object v0, p0, Ldge;->e:Ldez;

    :cond_4
    iget-object v0, p0, Ldge;->e:Ldez;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 6517
    iget-object v0, p0, Ldge;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 6518
    const/4 v0, 0x1

    iget-object v1, p0, Ldge;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6520
    :cond_0
    iget-object v0, p0, Ldge;->c:Lden;

    if-eqz v0, :cond_1

    .line 6521
    const/4 v0, 0x2

    iget-object v1, p0, Ldge;->c:Lden;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6523
    :cond_1
    iget-object v0, p0, Ldge;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6524
    const/4 v0, 0x3

    iget-object v1, p0, Ldge;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6526
    :cond_2
    iget-object v0, p0, Ldge;->e:Ldez;

    if-eqz v0, :cond_3

    .line 6527
    const/4 v0, 0x4

    iget-object v1, p0, Ldge;->e:Ldez;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6529
    :cond_3
    iget-object v0, p0, Ldge;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6531
    return-void
.end method
