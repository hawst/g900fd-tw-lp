.class public final Lemp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lemp;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lemv;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lemb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    new-array v0, v0, [Lemp;

    sput-object v0, Lemp;->a:[Lemp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 169
    invoke-direct {p0}, Lepn;-><init>()V

    .line 174
    iput-object v0, p0, Lemp;->c:Lemv;

    .line 183
    iput-object v0, p0, Lemp;->g:Lemb;

    .line 169
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 213
    iget-object v1, p0, Lemp;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 214
    const/4 v0, 0x1

    iget-object v1, p0, Lemp;->b:Ljava/lang/String;

    .line 215
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 217
    :cond_0
    iget-object v1, p0, Lemp;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 218
    const/4 v1, 0x2

    iget-object v2, p0, Lemp;->d:Ljava/lang/String;

    .line 219
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_1
    iget-object v1, p0, Lemp;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 222
    const/4 v1, 0x3

    iget-object v2, p0, Lemp;->e:Ljava/lang/String;

    .line 223
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    :cond_2
    iget-object v1, p0, Lemp;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 226
    const/4 v1, 0x4

    iget-object v2, p0, Lemp;->f:Ljava/lang/String;

    .line 227
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_3
    iget-object v1, p0, Lemp;->g:Lemb;

    if-eqz v1, :cond_4

    .line 230
    const/4 v1, 0x5

    iget-object v2, p0, Lemp;->g:Lemb;

    .line 231
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_4
    iget-object v1, p0, Lemp;->c:Lemv;

    if-eqz v1, :cond_5

    .line 234
    const/4 v1, 0x6

    iget-object v2, p0, Lemp;->c:Lemv;

    .line 235
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_5
    iget-object v1, p0, Lemp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    iput v0, p0, Lemp;->cachedSize:I

    .line 239
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 165
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lemp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lemp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lemp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lemp;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lemp;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lemp;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lemp;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lemp;->g:Lemb;

    if-nez v0, :cond_2

    new-instance v0, Lemb;

    invoke-direct {v0}, Lemb;-><init>()V

    iput-object v0, p0, Lemp;->g:Lemb;

    :cond_2
    iget-object v0, p0, Lemp;->g:Lemb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lemp;->c:Lemv;

    if-nez v0, :cond_3

    new-instance v0, Lemv;

    invoke-direct {v0}, Lemv;-><init>()V

    iput-object v0, p0, Lemp;->c:Lemv;

    :cond_3
    iget-object v0, p0, Lemp;->c:Lemv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lemp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 189
    const/4 v0, 0x1

    iget-object v1, p0, Lemp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 191
    :cond_0
    iget-object v0, p0, Lemp;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 192
    const/4 v0, 0x2

    iget-object v1, p0, Lemp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 194
    :cond_1
    iget-object v0, p0, Lemp;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 195
    const/4 v0, 0x3

    iget-object v1, p0, Lemp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 197
    :cond_2
    iget-object v0, p0, Lemp;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 198
    const/4 v0, 0x4

    iget-object v1, p0, Lemp;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 200
    :cond_3
    iget-object v0, p0, Lemp;->g:Lemb;

    if-eqz v0, :cond_4

    .line 201
    const/4 v0, 0x5

    iget-object v1, p0, Lemp;->g:Lemb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 203
    :cond_4
    iget-object v0, p0, Lemp;->c:Lemv;

    if-eqz v0, :cond_5

    .line 204
    const/4 v0, 0x6

    iget-object v1, p0, Lemp;->c:Lemv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 206
    :cond_5
    iget-object v0, p0, Lemp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 208
    return-void
.end method
