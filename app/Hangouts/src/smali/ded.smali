.class public final Lded;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lded;


# instance fields
.field public b:Ldfi;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4708
    const/4 v0, 0x0

    new-array v0, v0, [Lded;

    sput-object v0, Lded;->a:[Lded;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4709
    invoke-direct {p0}, Lepn;-><init>()V

    .line 4712
    const/4 v0, 0x0

    iput-object v0, p0, Lded;->b:Ldfi;

    .line 4709
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 4751
    const/4 v0, 0x0

    .line 4752
    iget-object v1, p0, Lded;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 4753
    const/4 v0, 0x1

    iget-object v1, p0, Lded;->b:Ldfi;

    .line 4754
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4756
    :cond_0
    iget-object v1, p0, Lded;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4757
    const/4 v1, 0x2

    iget-object v2, p0, Lded;->c:Ljava/lang/String;

    .line 4758
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4760
    :cond_1
    iget-object v1, p0, Lded;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 4761
    const/4 v1, 0x3

    iget-object v2, p0, Lded;->d:Ljava/lang/Boolean;

    .line 4762
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4764
    :cond_2
    iget-object v1, p0, Lded;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 4765
    const/4 v1, 0x4

    iget-object v2, p0, Lded;->e:Ljava/lang/Integer;

    .line 4766
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4768
    :cond_3
    iget-object v1, p0, Lded;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 4769
    const/4 v1, 0x5

    iget-object v2, p0, Lded;->f:Ljava/lang/Integer;

    .line 4770
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4772
    :cond_4
    iget-object v1, p0, Lded;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 4773
    const/4 v1, 0x6

    iget-object v2, p0, Lded;->g:Ljava/lang/Integer;

    .line 4774
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4776
    :cond_5
    iget-object v1, p0, Lded;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4777
    iput v0, p0, Lded;->cachedSize:I

    .line 4778
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4705
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lded;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lded;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lded;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lded;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Lded;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Lded;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lded;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lded;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lded;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lded;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lded;->g:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 4727
    iget-object v0, p0, Lded;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 4728
    const/4 v0, 0x1

    iget-object v1, p0, Lded;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4730
    :cond_0
    iget-object v0, p0, Lded;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4731
    const/4 v0, 0x2

    iget-object v1, p0, Lded;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4733
    :cond_1
    iget-object v0, p0, Lded;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 4734
    const/4 v0, 0x3

    iget-object v1, p0, Lded;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 4736
    :cond_2
    iget-object v0, p0, Lded;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 4737
    const/4 v0, 0x4

    iget-object v1, p0, Lded;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4739
    :cond_3
    iget-object v0, p0, Lded;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 4740
    const/4 v0, 0x5

    iget-object v1, p0, Lded;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4742
    :cond_4
    iget-object v0, p0, Lded;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 4743
    const/4 v0, 0x6

    iget-object v1, p0, Lded;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4745
    :cond_5
    iget-object v0, p0, Lded;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4747
    return-void
.end method
