.class public final Ldkf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldkf;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ldke;

.field public d:Ldkc;

.field public e:Ldkg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 432
    const/4 v0, 0x0

    new-array v0, v0, [Ldkf;

    sput-object v0, Ldkf;->a:[Ldkf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 433
    invoke-direct {p0}, Lepn;-><init>()V

    .line 438
    iput-object v0, p0, Ldkf;->c:Ldke;

    .line 441
    iput-object v0, p0, Ldkf;->d:Ldkc;

    .line 444
    iput-object v0, p0, Ldkf;->e:Ldkg;

    .line 433
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 467
    const/4 v0, 0x0

    .line 468
    iget-object v1, p0, Ldkf;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 469
    const/4 v0, 0x1

    iget-object v1, p0, Ldkf;->b:Ljava/lang/String;

    .line 470
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 472
    :cond_0
    iget-object v1, p0, Ldkf;->d:Ldkc;

    if-eqz v1, :cond_1

    .line 473
    const/4 v1, 0x2

    iget-object v2, p0, Ldkf;->d:Ldkc;

    .line 474
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 476
    :cond_1
    iget-object v1, p0, Ldkf;->e:Ldkg;

    if-eqz v1, :cond_2

    .line 477
    const/4 v1, 0x3

    iget-object v2, p0, Ldkf;->e:Ldkg;

    .line 478
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 480
    :cond_2
    iget-object v1, p0, Ldkf;->c:Ldke;

    if-eqz v1, :cond_3

    .line 481
    const/4 v1, 0x4

    iget-object v2, p0, Ldkf;->c:Ldke;

    .line 482
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 484
    :cond_3
    iget-object v1, p0, Ldkf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 485
    iput v0, p0, Ldkf;->cachedSize:I

    .line 486
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 429
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldkf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldkf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldkf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkf;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldkf;->d:Ldkc;

    if-nez v0, :cond_2

    new-instance v0, Ldkc;

    invoke-direct {v0}, Ldkc;-><init>()V

    iput-object v0, p0, Ldkf;->d:Ldkc;

    :cond_2
    iget-object v0, p0, Ldkf;->d:Ldkc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldkf;->e:Ldkg;

    if-nez v0, :cond_3

    new-instance v0, Ldkg;

    invoke-direct {v0}, Ldkg;-><init>()V

    iput-object v0, p0, Ldkf;->e:Ldkg;

    :cond_3
    iget-object v0, p0, Ldkf;->e:Ldkg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldkf;->c:Ldke;

    if-nez v0, :cond_4

    new-instance v0, Ldke;

    invoke-direct {v0}, Ldke;-><init>()V

    iput-object v0, p0, Ldkf;->c:Ldke;

    :cond_4
    iget-object v0, p0, Ldkf;->c:Ldke;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Ldkf;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 450
    const/4 v0, 0x1

    iget-object v1, p0, Ldkf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 452
    :cond_0
    iget-object v0, p0, Ldkf;->d:Ldkc;

    if-eqz v0, :cond_1

    .line 453
    const/4 v0, 0x2

    iget-object v1, p0, Ldkf;->d:Ldkc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 455
    :cond_1
    iget-object v0, p0, Ldkf;->e:Ldkg;

    if-eqz v0, :cond_2

    .line 456
    const/4 v0, 0x3

    iget-object v1, p0, Ldkf;->e:Ldkg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 458
    :cond_2
    iget-object v0, p0, Ldkf;->c:Ldke;

    if-eqz v0, :cond_3

    .line 459
    const/4 v0, 0x4

    iget-object v1, p0, Ldkf;->c:Ldke;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 461
    :cond_3
    iget-object v0, p0, Ldkf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 463
    return-void
.end method
