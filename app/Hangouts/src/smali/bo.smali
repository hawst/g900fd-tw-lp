.class final Lbo;
.super Lbt;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 509
    invoke-direct {p0}, Lbt;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbk;)Landroid/app/Notification;
    .locals 23

    .prologue
    .line 512
    new-instance v1, Lbw;

    move-object/from16 v0, p1

    iget-object v2, v0, Lbk;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lbk;->w:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, Lbk;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Lbk;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Lbk;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, Lbk;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v8, v0, Lbk;->i:I

    move-object/from16 v0, p1

    iget-object v9, v0, Lbk;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v10, v0, Lbk;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, Lbk;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v12, v0, Lbk;->n:I

    move-object/from16 v0, p1

    iget v13, v0, Lbk;->o:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lbk;->p:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, Lbk;->k:Z

    move-object/from16 v0, p1

    iget v0, v0, Lbk;->j:I

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbk;->m:Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lbk;->u:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbk;->v:Landroid/os/Bundle;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbk;->q:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lbk;->r:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbk;->s:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-direct/range {v1 .. v22}, Lbw;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZILjava/lang/CharSequence;ZLandroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 518
    move-object/from16 v0, p1

    iget-object v2, v0, Lbk;->t:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lbf;->a(Lbd;Ljava/util/ArrayList;)V

    .line 519
    move-object/from16 v0, p1

    iget-object v2, v0, Lbk;->l:Lbu;

    invoke-static {v1, v2}, Lbf;->a(Lbe;Lbu;)V

    .line 520
    invoke-virtual {v1}, Lbw;->b()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method

.method public a([Lbg;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lbg;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 539
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    invoke-static {v3}, Lf;->a(Lbx;)Landroid/app/Notification$Action;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
