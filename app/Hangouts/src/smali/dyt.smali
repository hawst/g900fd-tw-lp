.class public final Ldyt;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyt;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:[Ldyu;

.field public j:[Ljava/lang/String;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/Integer;

.field public p:Ljava/lang/Integer;

.field public q:Ldyv;

.field public r:Ldzd;

.field public s:Ljava/lang/Integer;

.field public t:Ljava/lang/Boolean;

.field public u:[I

.field public v:Ljava/lang/Boolean;

.field public w:Lein;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 692
    const/4 v0, 0x0

    new-array v0, v0, [Ldyt;

    sput-object v0, Ldyt;->a:[Ldyt;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 693
    invoke-direct {p0}, Lepn;-><init>()V

    .line 925
    sget-object v0, Ldyu;->a:[Ldyu;

    iput-object v0, p0, Ldyt;->i:[Ldyu;

    .line 928
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldyt;->j:[Ljava/lang/String;

    .line 937
    iput-object v1, p0, Ldyt;->n:Ljava/lang/Integer;

    .line 940
    iput-object v1, p0, Ldyt;->o:Ljava/lang/Integer;

    .line 943
    iput-object v1, p0, Ldyt;->p:Ljava/lang/Integer;

    .line 946
    iput-object v1, p0, Ldyt;->q:Ldyv;

    .line 949
    iput-object v1, p0, Ldyt;->r:Ldzd;

    .line 952
    iput-object v1, p0, Ldyt;->s:Ljava/lang/Integer;

    .line 957
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldyt;->u:[I

    .line 962
    iput-object v1, p0, Ldyt;->w:Lein;

    .line 693
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1048
    iget-object v0, p0, Ldyt;->b:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 1049
    const/4 v0, 0x1

    iget-object v2, p0, Ldyt;->b:Ljava/lang/String;

    .line 1050
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1052
    :goto_0
    iget-object v2, p0, Ldyt;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1053
    const/4 v2, 0x2

    iget-object v3, p0, Ldyt;->c:Ljava/lang/String;

    .line 1054
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1056
    :cond_0
    iget-object v2, p0, Ldyt;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1057
    const/4 v2, 0x3

    iget-object v3, p0, Ldyt;->d:Ljava/lang/String;

    .line 1058
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1060
    :cond_1
    iget-object v2, p0, Ldyt;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1061
    const/4 v2, 0x4

    iget-object v3, p0, Ldyt;->e:Ljava/lang/String;

    .line 1062
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1064
    :cond_2
    iget-object v2, p0, Ldyt;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1065
    const/4 v2, 0x5

    iget-object v3, p0, Ldyt;->f:Ljava/lang/String;

    .line 1066
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1068
    :cond_3
    iget-object v2, p0, Ldyt;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 1069
    const/4 v2, 0x6

    iget-object v3, p0, Ldyt;->g:Ljava/lang/Boolean;

    .line 1070
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1072
    :cond_4
    iget-object v2, p0, Ldyt;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 1073
    const/4 v2, 0x7

    iget-object v3, p0, Ldyt;->h:Ljava/lang/Boolean;

    .line 1074
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1076
    :cond_5
    iget-object v2, p0, Ldyt;->j:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ldyt;->j:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 1078
    iget-object v4, p0, Ldyt;->j:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_6

    aget-object v6, v4, v2

    .line 1080
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1078
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1082
    :cond_6
    add-int/2addr v0, v3

    .line 1083
    iget-object v2, p0, Ldyt;->j:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1085
    :cond_7
    iget-object v2, p0, Ldyt;->k:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    .line 1086
    const/16 v2, 0x9

    iget-object v3, p0, Ldyt;->k:Ljava/lang/Boolean;

    .line 1087
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1089
    :cond_8
    iget-object v2, p0, Ldyt;->l:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 1090
    const/16 v2, 0xa

    iget-object v3, p0, Ldyt;->l:Ljava/lang/String;

    .line 1091
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1093
    :cond_9
    iget-object v2, p0, Ldyt;->m:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 1094
    const/16 v2, 0xb

    iget-object v3, p0, Ldyt;->m:Ljava/lang/String;

    .line 1095
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1097
    :cond_a
    iget-object v2, p0, Ldyt;->n:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    .line 1098
    const/16 v2, 0xc

    iget-object v3, p0, Ldyt;->n:Ljava/lang/Integer;

    .line 1099
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1101
    :cond_b
    iget-object v2, p0, Ldyt;->o:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    .line 1102
    const/16 v2, 0xd

    iget-object v3, p0, Ldyt;->o:Ljava/lang/Integer;

    .line 1103
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1105
    :cond_c
    iget-object v2, p0, Ldyt;->p:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    .line 1106
    const/16 v2, 0xe

    iget-object v3, p0, Ldyt;->p:Ljava/lang/Integer;

    .line 1107
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1109
    :cond_d
    iget-object v2, p0, Ldyt;->q:Ldyv;

    if-eqz v2, :cond_e

    .line 1110
    const/16 v2, 0xf

    iget-object v3, p0, Ldyt;->q:Ldyv;

    .line 1111
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1113
    :cond_e
    iget-object v2, p0, Ldyt;->r:Ldzd;

    if-eqz v2, :cond_f

    .line 1114
    const/16 v2, 0x10

    iget-object v3, p0, Ldyt;->r:Ldzd;

    .line 1115
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1117
    :cond_f
    iget-object v2, p0, Ldyt;->s:Ljava/lang/Integer;

    if-eqz v2, :cond_10

    .line 1118
    const/16 v2, 0x11

    iget-object v3, p0, Ldyt;->s:Ljava/lang/Integer;

    .line 1119
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1121
    :cond_10
    iget-object v2, p0, Ldyt;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    .line 1122
    const/16 v2, 0x12

    iget-object v3, p0, Ldyt;->t:Ljava/lang/Boolean;

    .line 1123
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1125
    :cond_11
    iget-object v2, p0, Ldyt;->u:[I

    if-eqz v2, :cond_13

    iget-object v2, p0, Ldyt;->u:[I

    array-length v2, v2

    if-lez v2, :cond_13

    .line 1127
    iget-object v4, p0, Ldyt;->u:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_12

    aget v6, v4, v2

    .line 1129
    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 1127
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1131
    :cond_12
    add-int/2addr v0, v3

    .line 1132
    iget-object v2, p0, Ldyt;->u:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 1134
    :cond_13
    iget-object v2, p0, Ldyt;->i:[Ldyu;

    if-eqz v2, :cond_15

    .line 1135
    iget-object v2, p0, Ldyt;->i:[Ldyu;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_15

    aget-object v4, v2, v1

    .line 1136
    if-eqz v4, :cond_14

    .line 1137
    const/16 v5, 0x14

    .line 1138
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1135
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1142
    :cond_15
    iget-object v1, p0, Ldyt;->v:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    .line 1143
    const/16 v1, 0x15

    iget-object v2, p0, Ldyt;->v:Ljava/lang/Boolean;

    .line 1144
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1146
    :cond_16
    iget-object v1, p0, Ldyt;->w:Lein;

    if-eqz v1, :cond_17

    .line 1147
    const/16 v1, 0x16

    iget-object v2, p0, Ldyt;->w:Lein;

    .line 1148
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1150
    :cond_17
    iget-object v1, p0, Ldyt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1151
    iput v0, p0, Ldyt;->cachedSize:I

    .line 1152
    return v0

    :cond_18
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 689
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldyt;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldyt;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldyt;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyt;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyt;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyt;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyt;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyt;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyt;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyt;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldyt;->j:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldyt;->j:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldyt;->j:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Ldyt;->j:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ldyt;->j:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Ldyt;->j:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyt;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyt;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyt;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    if-eq v0, v6, :cond_3

    if-ne v0, v7, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldyt;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldyt;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_5

    if-eq v0, v6, :cond_5

    if-eq v0, v7, :cond_5

    const/4 v2, 0x5

    if-ne v0, v2, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldyt;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldyt;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_7

    if-eq v0, v5, :cond_7

    if-ne v0, v6, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldyt;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldyt;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Ldyt;->q:Ldyv;

    if-nez v0, :cond_9

    new-instance v0, Ldyv;

    invoke-direct {v0}, Ldyv;-><init>()V

    iput-object v0, p0, Ldyt;->q:Ldyv;

    :cond_9
    iget-object v0, p0, Ldyt;->q:Ldyv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Ldyt;->r:Ldzd;

    if-nez v0, :cond_a

    new-instance v0, Ldzd;

    invoke-direct {v0}, Ldzd;-><init>()V

    iput-object v0, p0, Ldyt;->r:Ldzd;

    :cond_a
    iget-object v0, p0, Ldyt;->r:Ldzd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_b

    if-eq v0, v4, :cond_b

    const/16 v2, 0xa

    if-eq v0, v2, :cond_b

    const/16 v2, 0xb

    if-eq v0, v2, :cond_b

    const/16 v2, 0xc

    if-eq v0, v2, :cond_b

    const/16 v2, 0x14

    if-eq v0, v2, :cond_b

    const/16 v2, 0x15

    if-eq v0, v2, :cond_b

    const/16 v2, 0x16

    if-ne v0, v2, :cond_c

    :cond_b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldyt;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldyt;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyt;->t:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_13
    const/16 v0, 0x98

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldyt;->u:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Ldyt;->u:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldyt;->u:[I

    :goto_2
    iget-object v2, p0, Ldyt;->u:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Ldyt;->u:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_d
    iget-object v2, p0, Ldyt;->u:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    :sswitch_14
    const/16 v0, 0xa2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldyt;->i:[Ldyu;

    if-nez v0, :cond_f

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldyu;

    iget-object v3, p0, Ldyt;->i:[Ldyu;

    if-eqz v3, :cond_e

    iget-object v3, p0, Ldyt;->i:[Ldyu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    iput-object v2, p0, Ldyt;->i:[Ldyu;

    :goto_4
    iget-object v2, p0, Ldyt;->i:[Ldyu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    iget-object v2, p0, Ldyt;->i:[Ldyu;

    new-instance v3, Ldyu;

    invoke-direct {v3}, Ldyu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldyt;->i:[Ldyu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_f
    iget-object v0, p0, Ldyt;->i:[Ldyu;

    array-length v0, v0

    goto :goto_3

    :cond_10
    iget-object v2, p0, Ldyt;->i:[Ldyu;

    new-instance v3, Ldyu;

    invoke-direct {v3}, Ldyu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldyt;->i:[Ldyu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyt;->v:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Ldyt;->w:Lein;

    if-nez v0, :cond_11

    new-instance v0, Lein;

    invoke-direct {v0}, Lein;-><init>()V

    iput-object v0, p0, Ldyt;->w:Lein;

    :cond_11
    iget-object v0, p0, Ldyt;->w:Lein;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb2 -> :sswitch_16
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 967
    iget-object v1, p0, Ldyt;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 968
    const/4 v1, 0x1

    iget-object v2, p0, Ldyt;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 970
    :cond_0
    iget-object v1, p0, Ldyt;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 971
    const/4 v1, 0x2

    iget-object v2, p0, Ldyt;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 973
    :cond_1
    iget-object v1, p0, Ldyt;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 974
    const/4 v1, 0x3

    iget-object v2, p0, Ldyt;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 976
    :cond_2
    iget-object v1, p0, Ldyt;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 977
    const/4 v1, 0x4

    iget-object v2, p0, Ldyt;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 979
    :cond_3
    iget-object v1, p0, Ldyt;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 980
    const/4 v1, 0x5

    iget-object v2, p0, Ldyt;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 982
    :cond_4
    iget-object v1, p0, Ldyt;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 983
    const/4 v1, 0x6

    iget-object v2, p0, Ldyt;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 985
    :cond_5
    iget-object v1, p0, Ldyt;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 986
    const/4 v1, 0x7

    iget-object v2, p0, Ldyt;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 988
    :cond_6
    iget-object v1, p0, Ldyt;->j:[Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 989
    iget-object v2, p0, Ldyt;->j:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 990
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 989
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 993
    :cond_7
    iget-object v1, p0, Ldyt;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 994
    const/16 v1, 0x9

    iget-object v2, p0, Ldyt;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 996
    :cond_8
    iget-object v1, p0, Ldyt;->l:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 997
    const/16 v1, 0xa

    iget-object v2, p0, Ldyt;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 999
    :cond_9
    iget-object v1, p0, Ldyt;->m:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 1000
    const/16 v1, 0xb

    iget-object v2, p0, Ldyt;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 1002
    :cond_a
    iget-object v1, p0, Ldyt;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 1003
    const/16 v1, 0xc

    iget-object v2, p0, Ldyt;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 1005
    :cond_b
    iget-object v1, p0, Ldyt;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 1006
    const/16 v1, 0xd

    iget-object v2, p0, Ldyt;->o:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 1008
    :cond_c
    iget-object v1, p0, Ldyt;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 1009
    const/16 v1, 0xe

    iget-object v2, p0, Ldyt;->p:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 1011
    :cond_d
    iget-object v1, p0, Ldyt;->q:Ldyv;

    if-eqz v1, :cond_e

    .line 1012
    const/16 v1, 0xf

    iget-object v2, p0, Ldyt;->q:Ldyv;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 1014
    :cond_e
    iget-object v1, p0, Ldyt;->r:Ldzd;

    if-eqz v1, :cond_f

    .line 1015
    const/16 v1, 0x10

    iget-object v2, p0, Ldyt;->r:Ldzd;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 1017
    :cond_f
    iget-object v1, p0, Ldyt;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 1018
    const/16 v1, 0x11

    iget-object v2, p0, Ldyt;->s:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 1020
    :cond_10
    iget-object v1, p0, Ldyt;->t:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 1021
    const/16 v1, 0x12

    iget-object v2, p0, Ldyt;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 1023
    :cond_11
    iget-object v1, p0, Ldyt;->u:[I

    if-eqz v1, :cond_12

    iget-object v1, p0, Ldyt;->u:[I

    array-length v1, v1

    if-lez v1, :cond_12

    .line 1024
    iget-object v2, p0, Ldyt;->u:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_12

    aget v4, v2, v1

    .line 1025
    const/16 v5, 0x13

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 1024
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1028
    :cond_12
    iget-object v1, p0, Ldyt;->i:[Ldyu;

    if-eqz v1, :cond_14

    .line 1029
    iget-object v1, p0, Ldyt;->i:[Ldyu;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_14

    aget-object v3, v1, v0

    .line 1030
    if-eqz v3, :cond_13

    .line 1031
    const/16 v4, 0x14

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1029
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1035
    :cond_14
    iget-object v0, p0, Ldyt;->v:Ljava/lang/Boolean;

    if-eqz v0, :cond_15

    .line 1036
    const/16 v0, 0x15

    iget-object v1, p0, Ldyt;->v:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1038
    :cond_15
    iget-object v0, p0, Ldyt;->w:Lein;

    if-eqz v0, :cond_16

    .line 1039
    const/16 v0, 0x16

    iget-object v1, p0, Ldyt;->w:Lein;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1041
    :cond_16
    iget-object v0, p0, Ldyt;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1043
    return-void
.end method
