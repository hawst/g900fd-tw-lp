.class public final Lemw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lemw;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lemx;

.field public d:Lemy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    new-array v0, v0, [Lemw;

    sput-object v0, Lemw;->a:[Lemw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Lepn;-><init>()V

    .line 272
    iput-object v0, p0, Lemw;->c:Lemx;

    .line 275
    iput-object v0, p0, Lemw;->d:Lemy;

    .line 83
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 295
    const/4 v0, 0x0

    .line 296
    iget-object v1, p0, Lemw;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 297
    const/4 v0, 0x1

    iget-object v1, p0, Lemw;->b:Ljava/lang/String;

    .line 298
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 300
    :cond_0
    iget-object v1, p0, Lemw;->c:Lemx;

    if-eqz v1, :cond_1

    .line 301
    const/4 v1, 0x2

    iget-object v2, p0, Lemw;->c:Lemx;

    .line 302
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_1
    iget-object v1, p0, Lemw;->d:Lemy;

    if-eqz v1, :cond_2

    .line 305
    const/4 v1, 0x3

    iget-object v2, p0, Lemw;->d:Lemy;

    .line 306
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_2
    iget-object v1, p0, Lemw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    iput v0, p0, Lemw;->cachedSize:I

    .line 310
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lemw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lemw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lemw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lemw;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lemw;->c:Lemx;

    if-nez v0, :cond_2

    new-instance v0, Lemx;

    invoke-direct {v0}, Lemx;-><init>()V

    iput-object v0, p0, Lemw;->c:Lemx;

    :cond_2
    iget-object v0, p0, Lemw;->c:Lemx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lemw;->d:Lemy;

    if-nez v0, :cond_3

    new-instance v0, Lemy;

    invoke-direct {v0}, Lemy;-><init>()V

    iput-object v0, p0, Lemw;->d:Lemy;

    :cond_3
    iget-object v0, p0, Lemw;->d:Lemy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lemw;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 281
    const/4 v0, 0x1

    iget-object v1, p0, Lemw;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 283
    :cond_0
    iget-object v0, p0, Lemw;->c:Lemx;

    if-eqz v0, :cond_1

    .line 284
    const/4 v0, 0x2

    iget-object v1, p0, Lemw;->c:Lemx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 286
    :cond_1
    iget-object v0, p0, Lemw;->d:Lemy;

    if-eqz v0, :cond_2

    .line 287
    const/4 v0, 0x3

    iget-object v1, p0, Lemw;->d:Lemy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 289
    :cond_2
    iget-object v0, p0, Lemw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 291
    return-void
.end method
