.class public Lbfk;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1998
    invoke-direct {p0}, Lbep;-><init>()V

    .line 1999
    iput-object p1, p0, Lbfk;->a:Ljava/lang/String;

    .line 2000
    iput p2, p0, Lbfk;->b:I

    .line 2001
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 2

    .prologue
    .line 2005
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2006
    const-string v0, "Babel_RequestWriter"

    const-string v1, "setHangoutNotificationStatus build protobuf"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2008
    :cond_0
    new-instance v0, Ldwi;

    invoke-direct {v0}, Ldwi;-><init>()V

    .line 2010
    iget-object v1, p0, Lbfk;->a:Ljava/lang/String;

    iput-object v1, v0, Ldwi;->c:Ljava/lang/String;

    .line 2011
    iget v1, p0, Lbfk;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldwi;->d:Ljava/lang/Integer;

    .line 2012
    invoke-virtual {p0, p1, p2}, Lbfk;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldwi;->b:Ldvm;

    .line 2013
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2018
    const-string v0, "hangouts/sethangoutnotificationstatus"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2023
    const-string v0, "background_queue"

    return-object v0
.end method
