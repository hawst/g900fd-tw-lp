.class public Lmn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Lmp;


# static fields
.field static final b:I


# instance fields
.field private a:Landroid/content/Context;

.field c:Z

.field private d:Landroid/view/LayoutInflater;

.field private e:Loc;

.field private f:Lma;

.field private g:I

.field private h:Landroid/view/View;

.field private i:Z

.field private j:Landroid/view/ViewTreeObserver;

.field private k:Lmo;

.field private l:Lmq;

.field private m:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    sget v0, Lf;->an:I

    sput v0, Lmn;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lma;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmn;-><init>(Landroid/content/Context;Lma;Landroid/view/View;Z)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lma;Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lmn;->a:Landroid/content/Context;

    .line 80
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lmn;->d:Landroid/view/LayoutInflater;

    .line 81
    iput-object p2, p0, Lmn;->f:Lma;

    .line 82
    iput-boolean p4, p0, Lmn;->i:Z

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Lf;->x:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lmn;->g:I

    .line 88
    iput-object p3, p0, Lmn;->h:Landroid/view/View;

    .line 90
    invoke-virtual {p2, p0}, Lma;->a(Lmp;)V

    .line 91
    return-void
.end method

.method static synthetic a(Lmn;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lmn;->i:Z

    return v0
.end method

.method static synthetic b(Lmn;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lmn;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic c(Lmn;)Lma;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lmn;->f:Lma;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Lmn;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MenuPopupHelper cannot be used without an anchor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Lma;)V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public a(Lma;Z)V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lmn;->f:Lma;

    if-eq p1, v0, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    invoke-virtual {p0}, Lmn;->c()V

    .line 267
    iget-object v0, p0, Lmn;->l:Lmq;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lmn;->l:Lmq;

    invoke-interface {v0, p1, p2}, Lmq;->a(Lma;Z)V

    goto :goto_0
.end method

.method public a(Lmq;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lmn;->l:Lmq;

    .line 230
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Lmn;->c:Z

    .line 99
    return-void
.end method

.method public a(Lmu;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    invoke-virtual {p1}, Lmu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 235
    new-instance v3, Lmn;

    iget-object v0, p0, Lmn;->a:Landroid/content/Context;

    iget-object v4, p0, Lmn;->h:Landroid/view/View;

    invoke-direct {v3, v0, p1, v4, v2}, Lmn;-><init>(Landroid/content/Context;Lma;Landroid/view/View;Z)V

    .line 236
    iget-object v0, p0, Lmn;->l:Lmq;

    invoke-virtual {v3, v0}, Lmn;->a(Lmq;)V

    .line 239
    invoke-virtual {p1}, Lmu;->size()I

    move-result v4

    move v0, v2

    .line 240
    :goto_0
    if-ge v0, v4, :cond_3

    .line 241
    invoke-virtual {p1, v0}, Lmu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 242
    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_1

    move v0, v1

    .line 247
    :goto_1
    invoke-virtual {v3, v0}, Lmn;->a(Z)V

    .line 249
    invoke-virtual {v3}, Lmn;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250
    iget-object v0, p0, Lmn;->l:Lmq;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lmn;->l:Lmq;

    invoke-interface {v0, p1}, Lmq;->b(Lma;)Z

    .line 256
    :cond_0
    :goto_2
    return v1

    .line 240
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 256
    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public b()Z
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 108
    new-instance v0, Loc;

    iget-object v4, p0, Lmn;->a:Landroid/content/Context;

    sget v5, Lf;->p:I

    invoke-direct {v0, v4, v3, v5}, Loc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lmn;->e:Loc;

    .line 109
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0, p0}, Loc;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 110
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0, p0}, Loc;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 112
    new-instance v0, Lmo;

    iget-object v4, p0, Lmn;->f:Lma;

    invoke-direct {v0, p0, v4}, Lmo;-><init>(Lmn;Lma;)V

    iput-object v0, p0, Lmn;->k:Lmo;

    .line 113
    iget-object v0, p0, Lmn;->e:Loc;

    iget-object v4, p0, Lmn;->k:Lmo;

    invoke-virtual {v0, v4}, Loc;->a(Landroid/widget/ListAdapter;)V

    .line 114
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0}, Loc;->b()V

    .line 116
    iget-object v4, p0, Lmn;->h:Landroid/view/View;

    .line 117
    if-eqz v4, :cond_3

    .line 118
    iget-object v0, p0, Lmn;->j:Landroid/view/ViewTreeObserver;

    if-nez v0, :cond_2

    move v0, v1

    .line 119
    :goto_0
    invoke-virtual {v4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    iput-object v5, p0, Lmn;->j:Landroid/view/ViewTreeObserver;

    .line 120
    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lmn;->j:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 123
    :cond_0
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0, v4}, Loc;->a(Landroid/view/View;)V

    .line 128
    iget-object v7, p0, Lmn;->e:Loc;

    iget-object v8, p0, Lmn;->k:Lmo;

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I

    move-result v11

    move v5, v2

    move-object v4, v3

    move v6, v2

    :goto_1
    if-ge v5, v11, :cond_4

    invoke-interface {v8, v5}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    if-eq v0, v2, :cond_5

    move-object v2, v3

    :goto_2
    iget-object v4, p0, Lmn;->m:Landroid/view/ViewGroup;

    if-nez v4, :cond_1

    new-instance v4, Landroid/widget/FrameLayout;

    iget-object v12, p0, Lmn;->a:Landroid/content/Context;

    invoke-direct {v4, v12}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lmn;->m:Landroid/view/ViewGroup;

    :cond_1
    iget-object v4, p0, Lmn;->m:Landroid/view/ViewGroup;

    invoke-interface {v8, v5, v2, v4}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9, v10}, Landroid/view/View;->measure(II)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 118
    goto :goto_0

    :cond_3
    move v1, v2

    .line 132
    :goto_3
    return v1

    .line 128
    :cond_4
    iget v0, p0, Lmn;->g:I

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v7, v0}, Loc;->d(I)V

    .line 129
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0}, Loc;->g()V

    .line 130
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0}, Loc;->e()V

    .line 131
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0}, Loc;->k()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_3

    :cond_5
    move v0, v2

    move-object v2, v4

    goto :goto_2
.end method

.method public b(Lme;)Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lmn;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0}, Loc;->f()V

    .line 139
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lmn;->k:Lmo;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lmn;->k:Lmo;

    invoke-virtual {v0}, Lmo;->notifyDataSetChanged()V

    .line 225
    :cond_0
    return-void
.end method

.method public c(Lme;)Z
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lmn;->e:Loc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0}, Loc;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method

.method public onDismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    iput-object v1, p0, Lmn;->e:Loc;

    .line 143
    iget-object v0, p0, Lmn;->f:Lma;

    invoke-virtual {v0}, Lma;->close()V

    .line 144
    iget-object v0, p0, Lmn;->j:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lmn;->j:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lmn;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lmn;->j:Landroid/view/ViewTreeObserver;

    .line 148
    :cond_0
    iget-object v0, p0, Lmn;->j:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 149
    iput-object v1, p0, Lmn;->j:Landroid/view/ViewTreeObserver;

    .line 151
    :cond_1
    return-void
.end method

.method public onGlobalLayout()V
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lmn;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lmn;->h:Landroid/view/View;

    .line 201
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_2

    .line 202
    :cond_0
    invoke-virtual {p0}, Lmn;->c()V

    .line 208
    :cond_1
    :goto_0
    return-void

    .line 203
    :cond_2
    invoke-virtual {p0}, Lmn;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lmn;->e:Loc;

    invoke-virtual {v0}, Loc;->e()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lmn;->k:Lmo;

    .line 160
    invoke-static {v0}, Lmo;->a(Lmo;)Lma;

    move-result-object v1

    invoke-virtual {v0, p3}, Lmo;->a(I)Lme;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lma;->a(Landroid/view/MenuItem;I)Z

    .line 161
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 164
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_0

    .line 165
    invoke-virtual {p0}, Lmn;->c()V

    .line 168
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
