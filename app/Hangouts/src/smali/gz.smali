.class public final Lgz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lhi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 981
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 982
    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 983
    new-instance v0, Lhh;

    invoke-direct {v0}, Lhh;-><init>()V

    sput-object v0, Lgz;->a:Lhi;

    .line 997
    :goto_0
    return-void

    .line 984
    :cond_0
    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 985
    new-instance v0, Lhg;

    invoke-direct {v0}, Lhg;-><init>()V

    sput-object v0, Lgz;->a:Lhi;

    goto :goto_0

    .line 986
    :cond_1
    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 987
    new-instance v0, Lhf;

    invoke-direct {v0}, Lhf;-><init>()V

    sput-object v0, Lgz;->a:Lhi;

    goto :goto_0

    .line 988
    :cond_2
    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 989
    new-instance v0, Lhe;

    invoke-direct {v0}, Lhe;-><init>()V

    sput-object v0, Lgz;->a:Lhi;

    goto :goto_0

    .line 990
    :cond_3
    const/16 v1, 0xb

    if-lt v0, v1, :cond_4

    .line 991
    new-instance v0, Lhd;

    invoke-direct {v0}, Lhd;-><init>()V

    sput-object v0, Lgz;->a:Lhi;

    goto :goto_0

    .line 992
    :cond_4
    const/16 v1, 0x9

    if-lt v0, v1, :cond_5

    .line 993
    new-instance v0, Lhc;

    invoke-direct {v0}, Lhc;-><init>()V

    sput-object v0, Lgz;->a:Lhi;

    goto :goto_0

    .line 995
    :cond_5
    new-instance v0, Lha;

    invoke-direct {v0}, Lha;-><init>()V

    sput-object v0, Lgz;->a:Lhi;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1032
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0}, Lhi;->a(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;Lfh;)V
    .locals 1

    .prologue
    .line 1169
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0, p1}, Lhi;->a(Landroid/view/View;Lfh;)V

    .line 1170
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1237
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0, p1}, Lhi;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1238
    return-void
.end method

.method public static a(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 1007
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0, p1}, Lhi;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1205
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0}, Lhi;->b(Landroid/view/View;)V

    .line 1206
    return-void
.end method

.method public static b(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1296
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0, p1}, Lhi;->b(Landroid/view/View;I)V

    .line 1297
    return-void
.end method

.method public static c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1272
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0}, Lhi;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1395
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0, p1}, Lhi;->c(Landroid/view/View;I)V

    .line 1396
    return-void
.end method

.method public static d(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1487
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0}, Lhi;->d(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/view/View;)Landroid/view/ViewParent;
    .locals 1

    .prologue
    .line 1519
    sget-object v0, Lgz;->a:Lhi;

    invoke-interface {v0, p0}, Lhi;->e(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method
