.class public final Ldih;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldih;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ldhd;

.field public g:Ljava/lang/String;

.field public h:Ldhl;

.field public i:Ldig;

.field public j:Ljava/lang/Boolean;

.field public k:[Ldih;

.field public l:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldih;

    sput-object v0, Ldih;->a:[Ldih;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 17
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldih;->d:[Ljava/lang/String;

    .line 22
    iput-object v1, p0, Ldih;->f:Ldhd;

    .line 27
    iput-object v1, p0, Ldih;->h:Ldhl;

    .line 30
    iput-object v1, p0, Ldih;->i:Ldig;

    .line 35
    sget-object v0, Ldih;->a:[Ldih;

    iput-object v0, p0, Ldih;->k:[Ldih;

    .line 38
    iput-object v1, p0, Ldih;->l:Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 89
    iget-object v0, p0, Ldih;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 90
    const/4 v0, 0x1

    iget-object v2, p0, Ldih;->b:Ljava/lang/String;

    .line 91
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 93
    :goto_0
    iget-object v2, p0, Ldih;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 94
    const/4 v2, 0x2

    iget-object v3, p0, Ldih;->c:Ljava/lang/String;

    .line 95
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 97
    :cond_0
    iget-object v2, p0, Ldih;->d:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldih;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 99
    iget-object v4, p0, Ldih;->d:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 101
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 103
    :cond_1
    add-int/2addr v0, v3

    .line 104
    iget-object v2, p0, Ldih;->d:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 106
    :cond_2
    iget-object v2, p0, Ldih;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 107
    const/4 v2, 0x4

    iget-object v3, p0, Ldih;->e:Ljava/lang/String;

    .line 108
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 110
    :cond_3
    iget-object v2, p0, Ldih;->f:Ldhd;

    if-eqz v2, :cond_4

    .line 111
    const/4 v2, 0x5

    iget-object v3, p0, Ldih;->f:Ldhd;

    .line 112
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 114
    :cond_4
    iget-object v2, p0, Ldih;->g:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 115
    const/4 v2, 0x6

    iget-object v3, p0, Ldih;->g:Ljava/lang/String;

    .line 116
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 118
    :cond_5
    iget-object v2, p0, Ldih;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 119
    const/4 v2, 0x7

    iget-object v3, p0, Ldih;->j:Ljava/lang/Boolean;

    .line 120
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 122
    :cond_6
    iget-object v2, p0, Ldih;->k:[Ldih;

    if-eqz v2, :cond_8

    .line 123
    iget-object v2, p0, Ldih;->k:[Ldih;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 124
    if-eqz v4, :cond_7

    .line 125
    const/16 v5, 0x8

    .line 126
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 123
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 130
    :cond_8
    iget-object v1, p0, Ldih;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 131
    const/16 v1, 0x9

    iget-object v2, p0, Ldih;->l:Ljava/lang/Integer;

    .line 132
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_9
    iget-object v1, p0, Ldih;->h:Ldhl;

    if-eqz v1, :cond_a

    .line 135
    const/16 v1, 0xa

    iget-object v2, p0, Ldih;->h:Ldhl;

    .line 136
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_a
    iget-object v1, p0, Ldih;->i:Ldig;

    if-eqz v1, :cond_b

    .line 139
    const/16 v1, 0xb

    iget-object v2, p0, Ldih;->i:Ldig;

    .line 140
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_b
    iget-object v1, p0, Ldih;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    iput v0, p0, Ldih;->cachedSize:I

    .line 144
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldih;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldih;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldih;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldih;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldih;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldih;->d:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldih;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldih;->d:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Ldih;->d:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ldih;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Ldih;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldih;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldih;->f:Ldhd;

    if-nez v0, :cond_3

    new-instance v0, Ldhd;

    invoke-direct {v0}, Ldhd;-><init>()V

    iput-object v0, p0, Ldih;->f:Ldhd;

    :cond_3
    iget-object v0, p0, Ldih;->f:Ldhd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldih;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldih;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldih;->k:[Ldih;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldih;

    iget-object v3, p0, Ldih;->k:[Ldih;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldih;->k:[Ldih;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldih;->k:[Ldih;

    :goto_3
    iget-object v2, p0, Ldih;->k:[Ldih;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldih;->k:[Ldih;

    new-instance v3, Ldih;

    invoke-direct {v3}, Ldih;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldih;->k:[Ldih;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v0, p0, Ldih;->k:[Ldih;

    array-length v0, v0

    goto :goto_2

    :cond_6
    iget-object v2, p0, Ldih;->k:[Ldih;

    new-instance v3, Ldih;

    invoke-direct {v3}, Ldih;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldih;->k:[Ldih;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    if-eq v0, v2, :cond_7

    const/4 v2, 0x5

    if-eq v0, v2, :cond_7

    const/4 v2, 0x6

    if-eq v0, v2, :cond_7

    const/16 v2, 0x64

    if-eq v0, v2, :cond_7

    const/16 v2, 0x65

    if-eq v0, v2, :cond_7

    const/16 v2, 0x6e

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldih;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldih;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ldih;->h:Ldhl;

    if-nez v0, :cond_9

    new-instance v0, Ldhl;

    invoke-direct {v0}, Ldhl;-><init>()V

    iput-object v0, p0, Ldih;->h:Ldhl;

    :cond_9
    iget-object v0, p0, Ldih;->h:Ldhl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldih;->i:Ldig;

    if-nez v0, :cond_a

    new-instance v0, Ldig;

    invoke-direct {v0}, Ldig;-><init>()V

    iput-object v0, p0, Ldih;->i:Ldig;

    :cond_a
    iget-object v0, p0, Ldih;->i:Ldig;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Ldih;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 44
    const/4 v1, 0x1

    iget-object v2, p0, Ldih;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 46
    :cond_0
    iget-object v1, p0, Ldih;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 47
    const/4 v1, 0x2

    iget-object v2, p0, Ldih;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 49
    :cond_1
    iget-object v1, p0, Ldih;->d:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 50
    iget-object v2, p0, Ldih;->d:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 51
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    :cond_2
    iget-object v1, p0, Ldih;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 55
    const/4 v1, 0x4

    iget-object v2, p0, Ldih;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 57
    :cond_3
    iget-object v1, p0, Ldih;->f:Ldhd;

    if-eqz v1, :cond_4

    .line 58
    const/4 v1, 0x5

    iget-object v2, p0, Ldih;->f:Ldhd;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 60
    :cond_4
    iget-object v1, p0, Ldih;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 61
    const/4 v1, 0x6

    iget-object v2, p0, Ldih;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 63
    :cond_5
    iget-object v1, p0, Ldih;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 64
    const/4 v1, 0x7

    iget-object v2, p0, Ldih;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 66
    :cond_6
    iget-object v1, p0, Ldih;->k:[Ldih;

    if-eqz v1, :cond_8

    .line 67
    iget-object v1, p0, Ldih;->k:[Ldih;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 68
    if-eqz v3, :cond_7

    .line 69
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 67
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 73
    :cond_8
    iget-object v0, p0, Ldih;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 74
    const/16 v0, 0x9

    iget-object v1, p0, Ldih;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 76
    :cond_9
    iget-object v0, p0, Ldih;->h:Ldhl;

    if-eqz v0, :cond_a

    .line 77
    const/16 v0, 0xa

    iget-object v1, p0, Ldih;->h:Ldhl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 79
    :cond_a
    iget-object v0, p0, Ldih;->i:Ldig;

    if-eqz v0, :cond_b

    .line 80
    const/16 v0, 0xb

    iget-object v1, p0, Ldih;->i:Ldig;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 82
    :cond_b
    iget-object v0, p0, Ldih;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 84
    return-void
.end method
