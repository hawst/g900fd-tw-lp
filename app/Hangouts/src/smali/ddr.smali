.class public final Lddr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lddr;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:[Lddr;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lddr;

    sput-object v0, Lddr;->a:[Lddr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lddr;->b:Ljava/lang/Integer;

    .line 39
    sget-object v0, Lddr;->a:[Lddr;

    iput-object v0, p0, Lddr;->g:[Lddr;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lddr;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 89
    const/4 v0, 0x1

    iget-object v2, p0, Lddr;->b:Ljava/lang/Integer;

    .line 90
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 92
    :goto_0
    iget-object v2, p0, Lddr;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 93
    const/4 v2, 0x2

    iget-object v3, p0, Lddr;->c:Ljava/lang/String;

    .line 94
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_0
    iget-object v2, p0, Lddr;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 97
    const/4 v2, 0x3

    iget-object v3, p0, Lddr;->d:Ljava/lang/String;

    .line 98
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 100
    :cond_1
    iget-object v2, p0, Lddr;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 101
    const/4 v2, 0x4

    iget-object v3, p0, Lddr;->e:Ljava/lang/String;

    .line 102
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_2
    iget-object v2, p0, Lddr;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 105
    const/4 v2, 0x5

    iget-object v3, p0, Lddr;->f:Ljava/lang/Boolean;

    .line 106
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 108
    :cond_3
    iget-object v2, p0, Lddr;->g:[Lddr;

    if-eqz v2, :cond_5

    .line 109
    iget-object v2, p0, Lddr;->g:[Lddr;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 110
    if-eqz v4, :cond_4

    .line 111
    const/4 v5, 0x6

    .line 112
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 109
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 116
    :cond_5
    iget-object v1, p0, Lddr;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 117
    const/4 v1, 0x7

    iget-object v2, p0, Lddr;->h:Ljava/lang/String;

    .line 118
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_6
    iget-object v1, p0, Lddr;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 121
    const/16 v1, 0x8

    iget-object v2, p0, Lddr;->i:Ljava/lang/String;

    .line 122
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_7
    iget-object v1, p0, Lddr;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 125
    const/16 v1, 0x9

    iget-object v2, p0, Lddr;->j:Ljava/lang/Integer;

    .line 126
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_8
    iget-object v1, p0, Lddr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    iput v0, p0, Lddr;->cachedSize:I

    .line 130
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lddr;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lddr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lddr;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddr;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddr;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lddr;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lddr;->g:[Lddr;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lddr;

    iget-object v3, p0, Lddr;->g:[Lddr;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lddr;->g:[Lddr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lddr;->g:[Lddr;

    :goto_2
    iget-object v2, p0, Lddr;->g:[Lddr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lddr;->g:[Lddr;

    new-instance v3, Lddr;

    invoke-direct {v3}, Lddr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddr;->g:[Lddr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lddr;->g:[Lddr;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lddr;->g:[Lddr;

    new-instance v3, Lddr;

    invoke-direct {v3}, Lddr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddr;->g:[Lddr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddr;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 50
    iget-object v0, p0, Lddr;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 51
    const/4 v0, 0x1

    iget-object v1, p0, Lddr;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 53
    :cond_0
    iget-object v0, p0, Lddr;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 54
    const/4 v0, 0x2

    iget-object v1, p0, Lddr;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 56
    :cond_1
    iget-object v0, p0, Lddr;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 57
    const/4 v0, 0x3

    iget-object v1, p0, Lddr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 59
    :cond_2
    iget-object v0, p0, Lddr;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 60
    const/4 v0, 0x4

    iget-object v1, p0, Lddr;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 62
    :cond_3
    iget-object v0, p0, Lddr;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 63
    const/4 v0, 0x5

    iget-object v1, p0, Lddr;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 65
    :cond_4
    iget-object v0, p0, Lddr;->g:[Lddr;

    if-eqz v0, :cond_6

    .line 66
    iget-object v1, p0, Lddr;->g:[Lddr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 67
    if-eqz v3, :cond_5

    .line 68
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 66
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_6
    iget-object v0, p0, Lddr;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 73
    const/4 v0, 0x7

    iget-object v1, p0, Lddr;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 75
    :cond_7
    iget-object v0, p0, Lddr;->i:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 76
    const/16 v0, 0x8

    iget-object v1, p0, Lddr;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 78
    :cond_8
    iget-object v0, p0, Lddr;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 79
    const/16 v0, 0x9

    iget-object v1, p0, Lddr;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 81
    :cond_9
    iget-object v0, p0, Lddr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 83
    return-void
.end method
