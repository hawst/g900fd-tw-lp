.class final Lxz;
.super Lbyq;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Lyj;Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lyj;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 51
    invoke-virtual {p0, p4}, Lxz;->a(I)Lbyq;

    .line 52
    invoke-virtual {p0, v1}, Lxz;->a(Z)Lbyq;

    .line 53
    invoke-virtual {p0, v1}, Lxz;->c(Z)Lbyq;

    .line 54
    invoke-virtual {p0, v1}, Lxz;->b(I)Lbyq;

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lxz;->d(Z)Lbyq;

    .line 56
    iput-object p1, p0, Lxz;->a:Ljava/util/List;

    .line 57
    iput-object p3, p0, Lxz;->e:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 62
    invoke-virtual {p0}, Lxz;->c()I

    move-result v0

    .line 63
    invoke-virtual {p0}, Lxz;->d()I

    move-result v1

    invoke-virtual {p0}, Lxz;->e()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 62
    invoke-static {v0, v1, v2, v3, v4}, Lxz;->a(IIZZZ)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lxz;->b:Ljava/lang/String;

    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    iget-object v0, p0, Lxz;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lxz;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "|"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    :cond_0
    iget-object v0, p0, Lxz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 72
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const-string v0, "|"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lxz;->c:Ljava/lang/String;

    .line 77
    return-void
.end method
