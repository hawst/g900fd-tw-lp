.class public final Lyp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Z

.field protected static final b:Ljava/util/Random;

.field public static c:J

.field public static d:J

.field public static e:J

.field public static f:J

.field public static g:J

.field public static h:J

.field private static i:Ljava/lang/CharSequence;

.field private static final j:Ljava/lang/Object;

.field private static k:I

.field private static final l:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 150
    sget-object v0, Lbys;->c:Lcyp;

    sput-boolean v2, Lyp;->a:Z

    .line 164
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lyp;->b:Ljava/util/Random;

    .line 2460
    const-wide/16 v0, 0x1

    sput-wide v0, Lyp;->c:J

    .line 2461
    const-wide/16 v0, 0x2

    sput-wide v0, Lyp;->d:J

    .line 2462
    const-wide/16 v0, 0x4

    sput-wide v0, Lyp;->e:J

    .line 2463
    const-wide/16 v0, 0x8

    sput-wide v0, Lyp;->f:J

    .line 2464
    const-wide/16 v0, 0x10

    sput-wide v0, Lyp;->g:J

    .line 2465
    const-wide/32 v0, -0x80000000

    sput-wide v0, Lyp;->h:J

    .line 3094
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lyp;->j:Ljava/lang/Object;

    .line 3095
    sput v2, Lyp;->k:I

    .line 3096
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lyp;->l:Ljava/util/HashSet;

    return-void
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 4207
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 4208
    new-instance v0, Lyt;

    invoke-direct {v0, p0}, Lyt;-><init>(Lyj;)V

    .line 4209
    invoke-virtual {v0, p2}, Lyt;->ac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4210
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4211
    const-string v0, "hangouts/gv_voicemail"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4212
    invoke-static {p0, p2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r(Lyj;Ljava/lang/String;)I

    .line 4216
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 4218
    :cond_1
    return-object v0

    .line 4213
    :cond_2
    invoke-static {p3}, Lf;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4214
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h(Lyj;Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Lyt;ZLjava/util/List;Ljava/util/List;Ljava/util/List;ZIIZLbnl;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbcn;",
            ">;ZIIZ",
            "Lbnl;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1529
    invoke-virtual {p0}, Lyt;->a()V

    .line 1531
    :try_start_0
    invoke-static/range {p0 .. p9}, Lyp;->b(Lyt;ZLjava/util/List;Ljava/util/List;Ljava/util/List;ZIIZLbnl;)Ljava/lang/String;

    move-result-object v0

    .line 1534
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1537
    invoke-virtual {p0}, Lyt;->c()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public static a(Lyj;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1766
    new-instance v0, Lyt;

    invoke-direct {v0, p0}, Lyt;-><init>(Lyj;)V

    .line 1767
    invoke-virtual {p0}, Lyj;->c()Lbdk;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lyt;->a(Ljava/lang/String;Lbdk;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 3104
    sget-object v1, Lyp;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 3105
    :try_start_0
    sget v0, Lyp;->k:I

    if-gez v0, :cond_0

    .line 3106
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sPendingChangeNotificationsCount is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lyp;->k:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3110
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 3109
    :cond_0
    :try_start_1
    sget v0, Lyp;->k:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lyp;->k:I

    .line 3110
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static a(J)V
    .locals 6

    .prologue
    .line 4177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 4178
    sub-long/2addr v0, p0

    .line 4181
    invoke-static {v0, v1}, Lbvx;->c(J)I

    move-result v2

    .line 4182
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Message purging: deleted "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " old sms/mms messages in telephony"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4184
    if-lez v2, :cond_0

    .line 4185
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v2

    .line 4186
    new-instance v3, Lyt;

    invoke-direct {v3, v2}, Lyt;-><init>(Lyj;)V

    .line 4188
    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    invoke-virtual {v3, v0, v1}, Lyt;->l(J)I

    move-result v0

    .line 4189
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Message purging: deleted "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " old sms/mms messages in Hangouts"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4191
    invoke-static {v3}, Lyp;->c(Lyt;)V

    .line 4193
    :cond_0
    return-void
.end method

.method private static a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 3137
    sget-object v1, Lyp;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 3138
    :try_start_0
    sget v0, Lyp;->k:I

    if-nez v0, :cond_1

    .line 3139
    invoke-static {p0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3140
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "account"

    .line 3141
    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3140
    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 3149
    :goto_0
    monitor-exit v1

    return-void

    .line 3143
    :cond_0
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3144
    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3149
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 3147
    :cond_1
    :try_start_1
    sget-object v0, Lyp;->l:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static a(Lbnl;Ljava/lang/String;J)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 2125
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requesting conversation metadata for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2128
    :cond_0
    new-instance v0, Lbek;

    const/4 v2, 0x1

    const-wide/16 v6, 0x0

    move-object v1, p1

    move v4, v3

    move-object v8, v5

    move-wide v9, p2

    move-object v11, v5

    invoke-direct/range {v0 .. v11}, Lbek;-><init>(Ljava/lang/String;ZZZ[BJLjava/lang/String;JLbjg;)V

    invoke-virtual {p0, v0}, Lbnl;->a(Lbea;)V

    .line 2130
    return-void
.end method

.method public static a(Lbnl;Lyt;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1877
    invoke-virtual {p1, p2}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v1

    .line 1878
    const/4 v2, 0x0

    .line 1880
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v0, v3

    invoke-virtual {p1, p2, v0}, Lyt;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v6

    .line 1885
    :try_start_1
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_0

    .line 1886
    const-string v2, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "sendUnsentMessages "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " cursor "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v6, :cond_5

    const/4 v0, -0x1

    .line 1887
    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " message rows"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1886
    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1889
    :cond_0
    invoke-virtual {p1}, Lyt;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1891
    if-eqz v6, :cond_3

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1893
    :cond_1
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_2

    .line 1894
    const-string v0, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendUnsentMessages "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sending msgID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    .line 1895
    invoke-interface {v6, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1894
    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1897
    :cond_2
    const/4 v0, 0x0

    .line 1900
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x0

    move-object v0, p1

    move-object v5, p0

    .line 1897
    invoke-static/range {v0 .. v5}, Lyp;->a(Lyt;Lyv;JZLbnl;)V

    .line 1904
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1912
    :cond_3
    new-instance v0, Lbeu;

    .line 1914
    invoke-virtual {p1, p2}, Lyt;->Y(Ljava/lang/String;)J

    move-result-wide v1

    invoke-direct {v0, p2, v1, v2}, Lbeu;-><init>(Ljava/lang/String;J)V

    .line 1912
    invoke-virtual {p0, v0}, Lbnl;->a(Lbea;)V

    .line 1916
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1918
    :try_start_3
    invoke-virtual {p1}, Lyt;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1921
    if-eqz v6, :cond_4

    .line 1922
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1925
    :cond_4
    return-void

    .line 1887
    :cond_5
    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 1918
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1921
    :catchall_1
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_6

    .line 1922
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 1921
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private static a(Lbnl;Lzg;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 2763
    new-instance v3, Lber;

    iget-object v4, p1, Lzg;->a:Ljava/lang/String;

    iget-object v5, p1, Lzg;->b:Ljava/lang/String;

    if-eqz p2, :cond_0

    move v2, v1

    :goto_0
    if-eqz p2, :cond_1

    :goto_1
    invoke-direct {v3, v4, v5, v2, v0}, Lber;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {p0, v3}, Lbnl;->a(Lbea;)V

    .line 2770
    return-void

    :cond_0
    move v2, v0

    .line 2763
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;ILbnl;)V
    .locals 3

    .prologue
    .line 2221
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2222
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendTypingRequestLocally  conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " typingType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2226
    :cond_0
    new-instance v0, Lbfo;

    invoke-direct {v0, p0, p1}, Lbfo;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2, v0}, Lbnl;->a(Lbea;)V

    .line 2227
    return-void
.end method

.method public static a(Ljava/lang/String;Lrk;)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 3844
    invoke-static {p0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 3845
    if-nez v1, :cond_0

    .line 3846
    const-string v0, "Babel_db"

    const-string v1, "EsConversationsData.processMmsNotification: no account"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3892
    :goto_0
    return-void

    .line 3849
    :cond_0
    new-instance v0, Lyt;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 3852
    invoke-virtual {p1}, Lrk;->e()[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Lbvx;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3853
    invoke-virtual {p1}, Lrk;->h()[B

    move-result-object v2

    .line 3854
    const-string v3, "UTF-8"

    .line 3855
    invoke-static {v2, v3}, Lbvx;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 3856
    invoke-virtual {p1}, Lrk;->d()Lrh;

    move-result-object v3

    .line 3862
    if-eqz v3, :cond_2

    .line 3863
    invoke-virtual {v3}, Lrh;->c()Ljava/lang/String;

    move-result-object v3

    .line 3862
    :goto_1
    if-eqz v3, :cond_1

    const-string v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "<"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 3864
    :cond_1
    :goto_2
    invoke-virtual {p1}, Lrk;->g()J

    move-result-wide v5

    .line 3865
    invoke-virtual {p1}, Lrk;->f()J

    move-result-wide v7

    .line 3866
    invoke-static {v3}, Lbdk;->d(Ljava/lang/String;)Lbdk;

    move-result-object v4

    .line 3868
    invoke-static {v4}, Lyp;->a(Lbdk;)Z

    move-result v4

    .line 3870
    invoke-static {}, Lbvx;->c()Z

    move-result v10

    if-eqz v10, :cond_5

    if-nez v4, :cond_5

    .line 3873
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 3874
    const-string v9, "content_location"

    invoke-virtual {v4, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3875
    const-string v9, "transaction_id"

    invoke-virtual {v4, v9, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3876
    const-string v9, "from_address"

    invoke-virtual {v4, v9, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3877
    const-string v3, "message_size"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3878
    const-string v3, "expiry"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3879
    invoke-virtual {v0, v4}, Lyt;->a(Landroid/content/ContentValues;)J

    move-result-wide v3

    .line 3882
    const/4 v5, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Ljava/lang/String;[BJZ)V

    goto :goto_0

    .line 3863
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 3862
    :cond_3
    invoke-static {v3}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v5

    if-eqz v5, :cond_1

    array-length v4, v5

    if-lez v4, :cond_1

    array-length v6, v5

    move v4, v9

    :goto_3
    if-ge v4, v6, :cond_1

    aget-object v7, v5, v4

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-virtual {v7}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 3888
    :cond_5
    sget v10, Lh;->hf:I

    move-object v2, v11

    invoke-static/range {v0 .. v10}, Lyp;->a(Lyt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJJII)V

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;ZILbnl;)V
    .locals 3

    .prologue
    .line 2236
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2237
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendPresenceRequestLocally: conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isFocused: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2240
    :cond_0
    new-instance v0, Lbfj;

    invoke-direct {v0, p0, p1, p2}, Lbfj;-><init>(Ljava/lang/String;ZI)V

    invoke-virtual {p3, v0}, Lbnl;->a(Lbea;)V

    .line 2242
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbjv;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1372
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    .line 1374
    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_0

    .line 1375
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "process userReadState  chatId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lbjv;->a:Lbdk;

    iget-object v4, v4, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  conversationId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  timestamp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v0, Lbjv;->b:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1380
    :cond_0
    new-instance v2, Lbjr;

    iget-object v3, v0, Lbjv;->a:Lbdk;

    iget-wide v4, v0, Lbjv;->b:J

    invoke-direct {v2, p1, v3, v4, v5}, Lbjr;-><init>(Ljava/lang/String;Lbdk;J)V

    .line 1383
    invoke-static {v2}, Laas;->a(Lbjr;)V

    .line 1384
    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbjr;)V

    goto :goto_0

    .line 1386
    :cond_1
    return-void
.end method

.method public static a(Lyj;)V
    .locals 2

    .prologue
    .line 3228
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_0

    .line 3229
    const-string v0, "Babel_db"

    const-string v1, "NOTIFY CONVERSATIONS"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3233
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->d:Landroid/net/Uri;

    .line 3234
    invoke-static {v0, p0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    .line 3233
    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3238
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->e:Landroid/net/Uri;

    .line 3239
    invoke-static {v0, p0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    .line 3238
    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3242
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->f:Landroid/net/Uri;

    .line 3243
    invoke-static {v0, p0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    .line 3242
    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3246
    invoke-static {p0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Lyj;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3247
    return-void
.end method

.method public static a(Lyj;JZI)V
    .locals 13

    .prologue
    .line 4051
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 4052
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "displayMmsDownloadFailure: invalid notification row id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 4100
    :cond_0
    :goto_0
    return-void

    .line 4056
    :cond_1
    new-instance v11, Lyt;

    invoke-direct {v11, p0}, Lyt;-><init>(Lyj;)V

    .line 4057
    if-eqz p3, :cond_3

    .line 4060
    invoke-virtual {v11}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v1, "mms_notification_inds"

    sget-object v2, Lyr;->a:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 4068
    if-eqz v12, :cond_0

    .line 4070
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4071
    const/4 v0, 0x0

    .line 4073
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    .line 4074
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x2

    .line 4075
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v0, 0x3

    .line 4077
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const/4 v0, 0x4

    .line 4078
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    sget v10, Lh;->hf:I

    move-object v0, v11

    move/from16 v9, p4

    .line 4071
    invoke-static/range {v0 .. v10}, Lyp;->a(Lyt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJJII)V

    .line 4082
    invoke-virtual {v11, p1, p2}, Lyt;->m(J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4085
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v0

    .line 4090
    :cond_3
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 4091
    const-string v2, "status"

    if-nez p4, :cond_4

    const/4 v0, 0x4

    .line 4092
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 4091
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4095
    const-string v0, "sending_error"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4096
    invoke-virtual {v11}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v2, "messages"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4098
    invoke-static {v11, p1, p2}, Lyp;->b(Lyt;J)V

    goto/16 :goto_0

    .line 4091
    :cond_4
    const/4 v0, 0x3

    goto :goto_1
.end method

.method public static a(Lyj;Landroid/content/Intent;)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3534
    invoke-static {p1}, Landroid/provider/Telephony$Sms$Intents;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;

    move-result-object v2

    .line 3535
    const-string v0, "format"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3536
    if-eqz v2, :cond_0

    array-length v0, v2

    if-gtz v0, :cond_1

    .line 3537
    :cond_0
    const-string v0, "Babel_db"

    const-string v1, "processReceivedSms: null or zero message"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3564
    :goto_0
    return-void

    .line 3543
    :cond_1
    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v2, v0

    .line 3545
    :try_start_0
    invoke-virtual {v5}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3543
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3547
    :catch_0
    move-exception v0

    const-string v0, "Babel_db"

    const-string v1, "processReceivedSms: NPE inside SmsMessage"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3551
    :cond_2
    invoke-static {}, Lbvx;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3552
    aget-object v0, v2, v1

    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide v4

    const-string v0, "pdus"

    .line 3553
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 3552
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "smsdump-"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    :cond_3
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v5, Ljava/io/BufferedOutputStream;

    invoke-direct {v5, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v4, Ljava/io/ObjectOutputStream;

    invoke-direct {v4, v5}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-virtual {v4, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v4}, Ljava/io/ObjectOutputStream;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v4}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 3555
    :cond_4
    :goto_2
    aget-object v0, v2, v1

    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getMessageClass()Landroid/telephony/SmsMessage$MessageClass;

    move-result-object v0

    sget-object v4, Landroid/telephony/SmsMessage$MessageClass;->CLASS_0:Landroid/telephony/SmsMessage$MessageClass;

    if-ne v0, v4, :cond_5

    .line 3556
    aget-object v0, v2, v1

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/hangouts/phone/ClassZeroActivity;

    invoke-direct {v2, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "pdu"

    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getPdu()[B

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "format"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "account_name"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v2, 0x18000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3552
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v4}, Ljava/io/ObjectOutputStream;->close()V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "dumpSms: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 3560
    :cond_5
    const-string v0, "errorCode"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v2, p0, v0, v1}, Lyp;->a([Landroid/telephony/SmsMessage;Lyj;ILjava/lang/Boolean;)V

    .line 3562
    invoke-static {p0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "received_sms_count_since_last_upload"

    invoke-virtual {v0, v1}, Lbsx;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static a(Lyj;Landroid/content/Intent;Z)V
    .locals 7

    .prologue
    .line 3333
    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 3337
    const-string v0, "notification_row_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 3340
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v5, p2

    invoke-static/range {v0 .. v6}, Lyp;->a(Lyj;Landroid/net/Uri;JLjava/lang/Boolean;ZZ)V

    .line 3342
    return-void
.end method

.method private static a(Lyj;Landroid/net/Uri;JLjava/lang/Boolean;ZZ)V
    .locals 37

    .prologue
    .line 3357
    if-nez p1, :cond_1

    .line 3525
    :cond_0
    :goto_0
    return-void

    .line 3361
    :cond_1
    new-instance v31, Lyt;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 3362
    const/4 v3, 0x0

    .line 3363
    const-wide/16 v4, 0x0

    cmp-long v4, p2, v4

    if-lez v4, :cond_15

    .line 3364
    if-eqz p5, :cond_2

    .line 3366
    move-object/from16 v0, v31

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lyt;->m(J)I

    move-object/from16 v29, v3

    .line 3374
    :goto_1
    invoke-static/range {p1 .. p1}, Lbvx;->e(Landroid/net/Uri;)Lbvj;

    move-result-object v32

    .line 3375
    if-nez v32, :cond_3

    .line 3377
    const-string v3, "Babel_db"

    const-string v4, "EsConversationsData.processReceivedMms: failed to load mms"

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3368
    :cond_2
    move-object/from16 v0, v31

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lyt;->n(J)Ljava/lang/String;

    move-result-object v3

    .line 3370
    move-object/from16 v0, v31

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lyt;->f(J)V

    move-object/from16 v29, v3

    goto :goto_1

    .line 3380
    :cond_3
    move-object/from16 v0, v32

    iget-object v3, v0, Lbvj;->w:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-gtz v3, :cond_4

    .line 3382
    const-string v3, "Babel_db"

    const-string v4, "EsConversationsData.processReceivedMms: MMS message has no part"

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3385
    :cond_4
    move-object/from16 v0, v32

    iget-wide v3, v0, Lbvj;->r:J

    invoke-static {v3, v4}, Lbvx;->a(J)Ljava/util/List;

    move-result-object v33

    .line 3386
    if-eqz v33, :cond_5

    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v3

    if-gtz v3, :cond_6

    .line 3388
    :cond_5
    const-string v3, "Babel_db"

    const-string v4, "EsConversationsData.processReceivedMms: MMS message has no recipient"

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3391
    :cond_6
    const/4 v3, 0x0

    .line 3392
    if-eqz p6, :cond_7

    .line 3396
    invoke-virtual/range {v31 .. v31}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v5

    move-object/from16 v30, v3

    .line 3401
    :goto_2
    if-nez v5, :cond_9

    .line 3403
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EsConversationsData.processReceivedMms: MMS has no From: id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    iget-wide v5, v0, Lbvj;->m:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v32

    iget v5, v0, Lbvj;->n:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " thread_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v32

    iget-wide v5, v0, Lbvj;->r:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3398
    :cond_7
    move-object/from16 v0, v32

    iget-wide v3, v0, Lbvj;->m:J

    move-object/from16 v0, v33

    invoke-static {v0, v3, v4}, Lbvx;->a(Ljava/util/List;J)Ljava/lang/String;

    move-result-object v3

    .line 3399
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-static {v3}, Lbdk;->d(Ljava/lang/String;)Lbdk;

    move-result-object v5

    move-object/from16 v30, v3

    goto :goto_2

    :cond_8
    const/4 v5, 0x0

    move-object/from16 v30, v3

    goto :goto_2

    .line 3410
    :cond_9
    move-object/from16 v0, v32

    iget-wide v6, v0, Lbvj;->q:J

    .line 3412
    invoke-static {v5}, Lyp;->a(Lbdk;)Z

    move-result v34

    .line 3414
    if-eqz v34, :cond_a

    move-object v3, v5

    :goto_3
    move-object/from16 v0, v31

    move-object/from16 v1, v33

    invoke-static {v0, v3, v1}, Lbvx;->a(Lyt;Lbdk;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    .line 3416
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 3417
    const-string v3, "Babel_db"

    const-string v4, "EsConversationsData.processNewMms: empty conversation id"

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3414
    :cond_a
    const/4 v3, 0x0

    goto :goto_3

    .line 3420
    :cond_b
    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;)Z

    move-result v35

    .line 3422
    if-nez p4, :cond_c

    .line 3424
    invoke-static/range {v35 .. v35}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    .line 3427
    :cond_c
    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lbvx;->a(Landroid/net/Uri;Z)V

    .line 3429
    invoke-virtual/range {v32 .. v32}, Lbvj;->k()I

    move-result v3

    .line 3430
    invoke-virtual/range {v32 .. v32}, Lbvj;->i()Z

    move-result v25

    .line 3431
    const/4 v8, 0x1

    if-gt v3, v8, :cond_d

    if-lez v3, :cond_11

    if-nez v25, :cond_11

    :cond_d
    const/4 v3, 0x1

    move/from16 v24, v3

    .line 3433
    :goto_4
    new-instance v36, Lbnl;

    invoke-direct/range {v36 .. v36}, Lbnl;-><init>()V

    .line 3434
    new-instance v3, Lbqs;

    .line 3438
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 3441
    invoke-virtual/range {v32 .. v32}, Lbvj;->e()Ljava/lang/String;

    move-result-object v10

    .line 3442
    invoke-virtual/range {v32 .. v32}, Lbvj;->l()Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    const/4 v15, 0x3

    const/16 v16, 0x0

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    .line 3448
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-wide/16 v18, 0x0

    move-object/from16 v0, v32

    iget v0, v0, Lbvj;->s:I

    move/from16 v20, v0

    .line 3451
    invoke-virtual/range {v32 .. v32}, Lbvj;->h()J

    move-result-wide v21

    move-object/from16 v0, v32

    iget-object v0, v0, Lbvj;->o:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v24, :cond_12

    const/16 v24, 0x9

    :goto_5
    const/16 v25, 0x0

    const/16 v26, 0x0

    const-wide/16 v27, 0x0

    invoke-direct/range {v3 .. v28}, Lbqs;-><init>(Ljava/lang/String;Lbdk;JLjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZIIILjava/lang/String;Ljava/lang/String;JIJLjava/lang/String;ILjava/lang/String;IJ)V

    const/4 v5, 0x1

    .line 3461
    invoke-virtual {v3, v5}, Lbqs;->a(I)Lbqs;

    move-result-object v3

    .line 3462
    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Lbqs;->a(Ljava/lang/String;)Lbqs;

    move-result-object v3

    .line 3463
    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lbvx;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lbqs;->b(Ljava/lang/String;)Lbqs;

    move-result-object v3

    .line 3464
    move-object/from16 v0, v31

    move-object/from16 v1, v36

    invoke-virtual {v3, v0, v1}, Lbqs;->a(Lyt;Lbnl;)V

    .line 3466
    invoke-virtual/range {v31 .. v31}, Lyt;->a()V

    .line 3469
    :try_start_0
    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6, v7}, Lyt;->j(Ljava/lang/String;J)I

    .line 3471
    move-object/from16 v0, v32

    iget-wide v8, v0, Lbvj;->r:J

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v8, v9}, Lyt;->p(Ljava/lang/String;J)V

    .line 3472
    if-nez v34, :cond_e

    .line 3474
    const/4 v3, 0x0

    const/4 v5, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-static {v0, v3, v1, v5}, Lbne;->a(Lyj;ZZI)V

    .line 3479
    :cond_e
    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 3482
    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6, v7}, Lyt;->c(Ljava/lang/String;J)V

    .line 3488
    :cond_f
    :goto_6
    invoke-virtual/range {v31 .. v31}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3490
    invoke-virtual/range {v31 .. v31}, Lyt;->c()V

    .line 3492
    move-object/from16 v0, v31

    invoke-static {v0, v4}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 3493
    move-object/from16 v0, v29

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_10

    const-wide/16 v3, 0x0

    cmp-long v3, p2, v3

    if-lez v3, :cond_10

    if-nez p5, :cond_10

    .line 3501
    new-instance v3, Lyq;

    move-object/from16 v0, v30

    invoke-direct {v3, v0}, Lyq;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Lyj;Ljava/lang/Runnable;)V

    .line 3518
    :cond_10
    invoke-static/range {p1 .. p1}, Lyp;->b(Landroid/net/Uri;)V

    .line 3520
    if-eqz v30, :cond_0

    .line 3522
    invoke-static/range {p0 .. p0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v3

    const-string v4, "received_mms_count_since_last_upload"

    invoke-virtual {v3, v4}, Lbsx;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3431
    :cond_11
    const/4 v3, 0x0

    move/from16 v24, v3

    goto/16 :goto_4

    .line 3451
    :cond_12
    if-eqz v25, :cond_13

    const/16 v24, 0x3

    goto/16 :goto_5

    :cond_13
    const/16 v24, 0x2

    goto/16 :goto_5

    .line 3483
    :cond_14
    if-eqz v34, :cond_f

    .line 3486
    :try_start_1
    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lyt;->u(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    .line 3490
    :catchall_0
    move-exception v3

    invoke-virtual/range {v31 .. v31}, Lyt;->c()V

    throw v3

    :cond_15
    move-object/from16 v29, v3

    goto/16 :goto_1
.end method

.method public static a(Lyj;Lbnl;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4244
    new-instance v0, Lyt;

    invoke-direct {v0, p0}, Lyt;-><init>(Lyj;)V

    .line 4245
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lyt;->e(Ljava/lang/String;I)V

    .line 4247
    invoke-static {p1, v0, p2}, Lyp;->a(Lbnl;Lyt;Ljava/lang/String;)V

    .line 4248
    return-void
.end method

.method public static a(Lyj;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 3299
    if-ltz p2, :cond_0

    .line 3300
    new-instance v0, Lyt;

    invoke-direct {v0, p0}, Lyt;-><init>(Lyj;)V

    .line 3301
    invoke-virtual {v0}, Lyt;->a()V

    .line 3303
    const/4 v2, -0x1

    .line 3304
    :try_start_0
    invoke-virtual {v0, p1}, Lyt;->Q(Ljava/lang/String;)J

    move-result-wide v3

    move v1, p2

    move-object v5, p1

    .line 3303
    invoke-virtual/range {v0 .. v5}, Lyt;->a(IIJLjava/lang/String;)V

    .line 3305
    invoke-virtual {v0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3307
    invoke-virtual {v0}, Lyt;->c()V

    .line 3312
    :goto_0
    return-void

    .line 3307
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lyt;->c()V

    throw v1

    .line 3309
    :cond_0
    const-string v0, "Babel_db"

    const-string v1, "revertConversationOtrStatus: invalid status"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3280
    if-eqz p2, :cond_0

    .line 3281
    new-instance v1, Lyt;

    invoke-direct {v1, p0}, Lyt;-><init>(Lyj;)V

    .line 3282
    invoke-virtual {v1}, Lyt;->a()V

    .line 3284
    :try_start_0
    invoke-virtual {v1, p1, p2}, Lyt;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3285
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3287
    invoke-virtual {v1}, Lyt;->c()V

    .line 3292
    :goto_0
    return-void

    .line 3287
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0

    .line 3289
    :cond_0
    const-string v0, "Babel_db"

    const-string v1, "revertConversationName: empty name"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lyt;)V
    .locals 1

    .prologue
    .line 2145
    invoke-virtual {p0}, Lyt;->v()V

    .line 2146
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 2147
    return-void
.end method

.method public static a(Lyt;J)V
    .locals 8

    .prologue
    .line 2168
    invoke-virtual {p0}, Lyt;->a()V

    .line 2172
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lyt;->c(J)Landroid/util/Pair;

    move-result-object v0

    .line 2173
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 2175
    if-eqz v0, :cond_0

    .line 2177
    invoke-virtual {p0, p1, p2}, Lyt;->f(J)V

    .line 2179
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2180
    const-string v2, "snippet_type"

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2181
    const-string v2, "snippet_author_chat_id"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2182
    const-string v2, "snippet_text"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2183
    const-string v2, "snippet_image_url"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2184
    const-string v2, "snippet_message_row_id"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2185
    const-string v2, "snippet_status"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2186
    const-string v2, "previous_latest_timestamp"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2187
    const-string v2, "snippet_new_conversation_name"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2188
    const-string v2, "snippet_participant_keys"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2189
    const-string v2, "snippet_sms_type"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2195
    invoke-virtual {p0}, Lyt;->e()Lzr;

    move-result-object v2

    const-string v3, "conversations"

    const-string v4, "conversation_id=? AND snippet_message_row_id=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    .line 2200
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 2195
    invoke-virtual {v2, v3, v1, v4, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2203
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206
    :cond_0
    invoke-virtual {p0}, Lyt;->c()V

    .line 2208
    if-eqz v0, :cond_1

    .line 2209
    invoke-static {p0, v0}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 2213
    :goto_0
    return-void

    .line 2206
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0

    .line 2211
    :cond_1
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Lyj;)V

    goto :goto_0
.end method

.method private static a(Lyt;JLbnl;Lzg;)V
    .locals 25

    .prologue
    .line 2820
    move-object/from16 v0, p4

    iget-object v1, v0, Lzg;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lyp;->f(Lyt;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 2821
    move-object/from16 v0, p4

    iget-object v1, v0, Lzg;->b:Ljava/lang/String;

    const/16 v2, 0x3e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lyt;->b(Ljava/lang/String;I)J

    move-result-wide v10

    .line 2823
    move-object/from16 v0, p4

    iget v1, v0, Lzg;->y:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 2824
    move-object/from16 v0, p4

    iget-object v0, v0, Lzg;->o:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 2825
    const-string v1, "hangouts/location"

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2827
    const-string v19, "image/jpeg"

    .line 2830
    :cond_0
    move-object/from16 v0, p4

    iget-object v1, v0, Lzg;->F:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2832
    new-instance v1, Lbfc;

    move-object/from16 v0, p4

    iget-object v2, v0, Lzg;->a:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v3, v0, Lzg;->b:Ljava/lang/String;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    .line 2835
    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v5, v0, Lzg;->B:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v6, v0, Lzg;->F:Ljava/lang/String;

    move-object/from16 v0, p4

    iget v7, v0, Lzg;->G:I

    move-wide/from16 v8, p1

    invoke-direct/range {v1 .. v11}, Lbfc;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V

    .line 2855
    :goto_0
    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lbnl;->a(Lbea;)V

    .line 2881
    :cond_1
    const/4 v9, 0x2

    move-object/from16 v6, p0

    move-wide/from16 v7, p1

    invoke-virtual/range {v6 .. v11}, Lyt;->a(JIJ)V

    .line 2885
    :goto_1
    return-void

    .line 2842
    :cond_2
    new-instance v12, Lbfc;

    move-object/from16 v0, p4

    iget-object v13, v0, Lzg;->a:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v14, v0, Lzg;->b:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    .line 2845
    invoke-interface {v4, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v0, v0, Lzg;->B:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p4

    iget-object v0, v0, Lzg;->e:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p4

    iget-object v0, v0, Lzg;->j:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p4

    iget v0, v0, Lzg;->z:I

    move/from16 v20, v0

    move-object/from16 v0, p4

    iget v0, v0, Lzg;->A:I

    move/from16 v21, v0

    move-object/from16 v0, p4

    iget v0, v0, Lzg;->C:I

    move/from16 v22, v0

    move-wide/from16 v23, v10

    invoke-direct/range {v12 .. v24}, Lbfc;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJ)V

    move-object v1, v12

    goto :goto_0

    .line 2857
    :cond_3
    move-object/from16 v0, p4

    iget v1, v0, Lzg;->y:I

    if-nez v1, :cond_4

    .line 2861
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4}, Lbvx;->a(Landroid/content/Context;Ljava/util/List;)J

    move-result-wide v17

    .line 2862
    move-object/from16 v0, p4

    iget-object v1, v0, Lzg;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lyt;->Z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2866
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 2867
    new-instance v12, Lbff;

    move-object/from16 v0, p4

    iget-object v13, v0, Lzg;->a:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v14, v0, Lzg;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v0, v0, Lzg;->e:Ljava/lang/String;

    move-object/from16 v16, v0

    move-wide/from16 v20, p1

    move-wide/from16 v22, v10

    invoke-direct/range {v12 .. v23}, Lbff;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJ)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Lbnl;->a(Lbea;)V

    goto :goto_2

    .line 2878
    :cond_4
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Resending unknown type of SMS message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    iget v3, v0, Lzg;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static a(Lyt;Lbfz;Lbnl;)V
    .locals 3

    .prologue
    .line 344
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processServerResponse of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 345
    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p1, Lbfz;->f:Z

    .line 349
    invoke-virtual {p1, p0, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 350
    iget-boolean v0, p1, Lbfz;->f:Z

    if-nez v0, :cond_1

    .line 351
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "did not call through to super -- "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_1
    return-void
.end method

.method public static a(Lyt;Lbiq;Lbnl;JJJ)V
    .locals 21

    .prologue
    .line 172
    move-object/from16 v0, p1

    instance-of v4, v0, Lbjg;

    if-eqz v4, :cond_10

    move-object/from16 v9, p1

    .line 173
    check-cast v9, Lbjg;

    iget-boolean v4, v9, Lbjg;->q:Z

    if-eqz v4, :cond_1

    iget-object v5, v9, Lbjg;->c:Ljava/lang/String;

    iget-wide v6, v9, Lbjg;->e:J

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    invoke-static/range {v4 .. v9}, Lyp;->a(Lyt;Ljava/lang/String;JLbnl;Lbjg;)Z

    move-result v4

    if-nez v4, :cond_8

    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_0

    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processEvent saving event "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v9, Lbjg;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " eventId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v9, Lbjg;->m:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " timestamp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v9, Lbjg;->e:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v4

    iget-object v5, v9, Lbjg;->d:Lbdk;

    invoke-virtual {v5}, Lbdk;->a()Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v4, "Babel_db"

    const-string v5, "Sender id for unpersisted event should not be empty"

    invoke-static {v4, v5}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_1
    const/4 v4, 0x0

    :goto_2
    if-nez v4, :cond_8

    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_0

    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unable to process unpersisted event "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v9, Lbjg;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " eventId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v9, Lbjg;->m:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " timestamp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v9, Lbjg;->e:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v5, v9, Lbjg;->d:Lbdk;

    invoke-virtual {v5, v4}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, v9, Lbjg;->d:Lbdk;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7}, Lyt;->a(Lbdk;ZI)Lzi;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v6, v5, Lzi;->a:Ljava/lang/String;

    invoke-static {v6}, Lyt;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_4

    const-string v4, "Babel_db"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "unpersisted conversation "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v9, Lbjg;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " exists locally as"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v5, Lzi;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v4, v5, Lzi;->a:Ljava/lang/String;

    iput-object v4, v9, Lbjg;->c:Ljava/lang/String;

    :goto_3
    const/4 v4, 0x1

    goto :goto_2

    :cond_5
    const-string v5, "Babel_db"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "Babel_db"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "conversation "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v9, Lbjg;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " does not exist locally"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v5, v9, Lbjg;->c:Ljava/lang/String;

    iget-wide v6, v9, Lbjg;->e:J

    iget-object v8, v9, Lbjg;->d:Lbdk;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7, v8}, Lyt;->a(Ljava/lang/String;JLbdk;)V

    iget-object v5, v9, Lbjg;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lyt;->F(Ljava/lang/String;)V

    iget-object v5, v9, Lbjg;->c:Ljava/lang/String;

    iget-object v10, v9, Lbjg;->d:Lbdk;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static/range {v10 .. v15}, Lbdh;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lyt;->a(Ljava/lang/String;Lbdh;)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, v9, Lbjg;->d:Lbdk;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, v9, Lbjg;->c:Ljava/lang/String;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v4, v5}, Lbnl;->a(Lyt;Ljava/lang/String;Ljava/util/List;)Z

    iget-object v4, v9, Lbjg;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lyp;->b(Lyt;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    const-string v4, "Babel_db"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Only self user Id is present, cannot determine conversation for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v9, Lbjg;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    iget-object v4, v9, Lbjg;->c:Ljava/lang/String;

    invoke-virtual {v9}, Lbjg;->b()Ljava/util/List;

    move-result-object v5

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v4, v5}, Lbnl;->a(Lyt;Ljava/lang/String;Ljava/util/List;)Z

    instance-of v4, v9, Lbiv;

    if-eqz v4, :cond_b

    move-object v11, v9

    check-cast v11, Lbiv;

    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_9

    invoke-virtual {v11}, Lbiv;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    const-string v5, "Babel_Stress"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Received stress message push for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v7

    invoke-virtual {v7}, Lyj;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    new-instance v10, Lbqs;

    const/4 v12, 0x1

    const-wide/16 v19, 0x0

    move-wide/from16 v13, p3

    move-wide/from16 v15, p5

    move-wide/from16 v17, p7

    invoke-direct/range {v10 .. v20}, Lbqs;-><init>(Lbiv;ZJJJJ)V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v10, v0, v1}, Lbqs;->a(Lyt;Lbnl;)V

    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lyp;->a(Lyt;Lbiv;)V

    :cond_a
    :goto_4
    iget-boolean v4, v9, Lbjg;->o:Z

    if-eqz v4, :cond_0

    iget-object v4, v9, Lbjg;->c:Ljava/lang/String;

    iget-wide v5, v9, Lbjg;->e:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v6}, Lyt;->i(Ljava/lang/String;J)V

    goto/16 :goto_0

    :cond_b
    instance-of v4, v9, Lbji;

    if-eqz v4, :cond_d

    move-object v4, v9

    check-cast v4, Lbji;

    sget-boolean v5, Lyp;->a:Z

    if-eqz v5, :cond_c

    const-string v5, "Babel_db"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "processMembershipChange conversationId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v4, Lbji;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    iget-object v5, v4, Lbji;->c:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    const/4 v6, 0x1

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v4, v1, v6}, Lyp;->a(Lyt;Lbji;Lbnl;Z)V

    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    const-string v6, "Babel_db"

    const-string v7, "updating conversation (name/avatars) because of a memebership change."

    invoke-static {v6, v7}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v4, Lbji;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lyp;->b(Lyt;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-static {v4}, Lyp;->a(Lyj;)V

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lyp;->d(Lyt;Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lyp;->c(Lyt;Ljava/lang/String;)V

    goto :goto_4

    :catchall_0
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v4

    :cond_d
    instance-of v4, v9, Lbjd;

    if-eqz v4, :cond_e

    new-instance v5, Lbqt;

    move-object v4, v9

    check-cast v4, Lbjd;

    invoke-direct {v5, v4}, Lbqt;-><init>(Lbjd;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Lbqt;->a(Lyt;Lbnl;)V

    goto :goto_4

    :cond_e
    instance-of v4, v9, Lbjh;

    if-eqz v4, :cond_f

    move-object v4, v9

    check-cast v4, Lbjh;

    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-wide/from16 v2, p3

    invoke-static {v0, v4, v1, v2, v3}, Lyp;->a(Lyt;Lbjh;Lbnl;J)Z

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    if-eqz v5, :cond_a

    iget-object v4, v4, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lyp;->d(Lyt;Ljava/lang/String;)V

    goto/16 :goto_4

    :catchall_1
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v4

    :cond_f
    instance-of v4, v9, Lbjk;

    if-eqz v4, :cond_a

    new-instance v5, Lbqv;

    move-object v4, v9

    check-cast v4, Lbjk;

    invoke-direct {v5, v4}, Lbqv;-><init>(Lbjk;)V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lbqv;->b(Lyt;)V

    goto/16 :goto_4

    .line 174
    :cond_10
    move-object/from16 v0, p1

    instance-of v4, v0, Lbjr;

    if-eqz v4, :cond_15

    .line 175
    check-cast p1, Lbjr;

    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_11

    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processWatermarkNotification ConversationId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjr;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Timestamp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-wide v6, v0, Lbjr;->e:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    move-object/from16 v0, p1

    iget-object v5, v0, Lbjr;->c:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    :try_start_2
    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_12

    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjr;->d:Lbdk;

    invoke-virtual {v4, v6}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    const-string v4, "self"

    :goto_5
    const-string v6, "Babel_db"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "processWatermarkNotificationInTransaction  conversationId: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v8, v0, Lbjr;->c:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " chatId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " timestamp: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-wide v7, v0, Lbjr;->e:J

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "  ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v7, Ljava/util/Date;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lbjr;->e:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjr;->d:Lbdk;

    invoke-virtual {v4, v6}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    move-object/from16 v0, p1

    iget-object v4, v0, Lbjr;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v6, v0, Lbjr;->e:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6, v7}, Lyt;->c(Ljava/lang/String;J)V

    invoke-virtual/range {p2 .. p2}, Lbnl;->d()V

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lyp;->d(Lyt;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    :try_start_3
    move-object/from16 v0, p1

    iget-object v4, v0, Lbjr;->d:Lbdk;

    iget-object v4, v4, Lbdk;->b:Ljava/lang/String;

    goto/16 :goto_5

    :cond_14
    invoke-static/range {p1 .. p1}, Laas;->a(Lbjr;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbjr;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_6

    :catchall_2
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v4

    .line 176
    :cond_15
    move-object/from16 v0, p1

    instance-of v4, v0, Lbjc;

    if-eqz v4, :cond_16

    move-object/from16 v5, p1

    .line 177
    check-cast v5, Lbjc;

    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    move-object/from16 v9, p2

    :try_start_4
    invoke-static/range {v4 .. v11}, Lyp;->a(Lyt;Lbjc;JLjava/lang/String;Lbnl;Lys;Z)Z

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    if-eqz v4, :cond_0

    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-static {v4}, Lyp;->a(Lyj;)V

    goto/16 :goto_0

    :catchall_3
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v4

    .line 178
    :cond_16
    move-object/from16 v0, p1

    instance-of v4, v0, Lbjo;

    if-eqz v4, :cond_1c

    .line 179
    check-cast p1, Lbjo;

    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_17

    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processFocus conversationId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjo;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " senderId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjo;->d:Lbdk;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget v6, v0, Lbjo;->b:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and timestamp "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-wide v6, v0, Lbjo;->e:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    move-object/from16 v0, p1

    iget-wide v7, v0, Lbjo;->e:J

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_17
    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    :try_start_5
    move-object/from16 v0, p1

    iget-object v4, v0, Lbjo;->c:Ljava/lang/String;

    if-nez v4, :cond_19

    const-string v4, "Babel_db"

    const-string v5, "Received a Presence message without conversation id"

    invoke-static {v4, v5}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    goto/16 :goto_0

    :cond_19
    :try_start_6
    move-object/from16 v0, p1

    iget-object v5, v0, Lbjo;->c:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjo;->d:Lbdk;

    invoke-virtual {v4, v6}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_18

    if-nez v5, :cond_1a

    const-string v4, "Babel_db"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "attempt to process presence for a nonexistent conversation id ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_7

    :catchall_4
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v4

    :cond_1a
    :try_start_7
    move-object/from16 v0, p1

    iget-object v6, v0, Lbjo;->d:Lbdk;

    const/4 v4, 0x1

    move-object/from16 v0, p1

    iget v7, v0, Lbjo;->b:I

    if-ne v4, v7, :cond_1b

    const/4 v4, 0x1

    :goto_8
    invoke-static {v5, v6, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Lbdk;Z)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_7

    :cond_1b
    const/4 v4, 0x0

    goto :goto_8

    .line 180
    :cond_1c
    move-object/from16 v0, p1

    instance-of v4, v0, Lbjp;

    if-eqz v4, :cond_21

    .line 181
    check-cast p1, Lbjp;

    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_1d

    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processTyping senderId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjp;->d:Lbdk;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " conversationId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjp;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget v6, v0, Lbjp;->b:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " timestamp "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-wide v6, v0, Lbjp;->e:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    move-object/from16 v0, p1

    iget-wide v7, v0, Lbjp;->e:J

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lbjp;->d:Lbdk;

    invoke-virtual {v4, v5}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1e

    move-object/from16 v0, p1

    iget-object v5, v0, Lbjp;->c:Ljava/lang/String;

    if-nez v5, :cond_1f

    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "attempt to process typing for a nonexistent conversation id ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjp;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1e
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    goto/16 :goto_0

    :cond_1f
    :try_start_9
    move-object/from16 v0, p1

    iget-object v4, v0, Lbjp;->d:Lbdk;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lyt;->a(Lbdk;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v7, v0, Lbjp;->d:Lbdk;

    move-object/from16 v0, p1

    iget v4, v0, Lbjp;->b:I

    const/4 v8, 0x1

    if-ne v4, v8, :cond_20

    const/4 v4, 0x1

    :goto_a
    invoke-static {v5, v7, v6, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Lbdk;Ljava/lang/String;Z)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    goto :goto_9

    :catchall_5
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v4

    :cond_20
    const/4 v4, 0x0

    goto :goto_a

    .line 183
    :cond_21
    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unexpected update type from babel server "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static a(Lyt;Lbiv;)V
    .locals 5

    .prologue
    .line 572
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v0

    iget-object v1, p1, Lbiv;->d:Lbdk;

    invoke-virtual {v0, v1}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 573
    new-instance v0, Lbjr;

    iget-object v1, p1, Lbiv;->c:Ljava/lang/String;

    iget-object v2, p1, Lbiv;->d:Lbdk;

    iget-wide v3, p1, Lbiv;->e:J

    invoke-direct {v0, v1, v2, v3, v4}, Lbjr;-><init>(Ljava/lang/String;Lbdk;J)V

    .line 575
    invoke-static {v0}, Laas;->a(Lbjr;)V

    .line 576
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbjr;)V

    .line 578
    :cond_0
    return-void
.end method

.method public static a(Lyt;Lbjg;Lbnl;JJJ)V
    .locals 11

    .prologue
    .line 190
    iget-object v1, p1, Lbjg;->c:Ljava/lang/String;

    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lyp;->a(Lyt;Ljava/lang/String;JLbnl;Lbjg;)Z

    .line 192
    iget-object v0, p1, Lbjg;->c:Ljava/lang/String;

    .line 193
    invoke-virtual {p1}, Lbjg;->b()Ljava/util/List;

    move-result-object v1

    .line 192
    invoke-virtual {p2, p0, v0, v1}, Lbnl;->a(Lyt;Ljava/lang/String;Ljava/util/List;)Z

    .line 198
    instance-of v0, p1, Lbiv;

    if-eqz v0, :cond_1

    move-object v1, p1

    .line 199
    check-cast v1, Lbiv;

    new-instance v0, Lbqs;

    const/4 v2, 0x0

    const-wide/16 v5, 0x0

    move-wide v3, p3

    move-wide/from16 v7, p5

    move-wide/from16 v9, p7

    invoke-direct/range {v0 .. v10}, Lbqs;-><init>(Lbiv;ZJJJJ)V

    invoke-virtual {v0, p0, p2}, Lbqs;->b(Lyt;Lbnl;)V

    invoke-static {p0, v1}, Lyp;->a(Lyt;Lbiv;)V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    instance-of v0, p1, Lbji;

    if-eqz v0, :cond_2

    .line 202
    check-cast p1, Lbji;

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lyp;->a(Lyt;Lbji;Lbnl;Z)V

    goto :goto_0

    .line 204
    :cond_2
    instance-of v0, p1, Lbjd;

    if-eqz v0, :cond_3

    .line 205
    new-instance v0, Lbqt;

    check-cast p1, Lbjd;

    invoke-direct {v0, p1}, Lbqt;-><init>(Lbjd;)V

    .line 206
    invoke-virtual {v0, p0, p2}, Lbqt;->b(Lyt;Lbnl;)V

    goto :goto_0

    .line 207
    :cond_3
    instance-of v0, p1, Lbjh;

    if-eqz v0, :cond_4

    .line 208
    check-cast p1, Lbjh;

    const-wide/16 v0, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lyp;->a(Lyt;Lbjh;Lbnl;J)Z

    goto :goto_0

    .line 209
    :cond_4
    instance-of v0, p1, Lbjk;

    if-eqz v0, :cond_0

    .line 210
    new-instance v0, Lbqv;

    check-cast p1, Lbjk;

    invoke-direct {v0, p1}, Lbqv;-><init>(Lbjk;)V

    .line 211
    invoke-virtual {v0, p0}, Lbqv;->c(Lyt;)V

    goto :goto_0
.end method

.method private static a(Lyt;Lbji;Lbnl;Z)V
    .locals 15

    .prologue
    .line 1394
    move-object/from16 v0, p1

    iget-wide v7, v0, Lbji;->e:J

    .line 1395
    sget-boolean v1, Lyp;->a:Z

    if-eqz v1, :cond_0

    .line 1396
    const-string v2, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "processMembershipChange conversationId: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v3, v0, Lbji;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    iget v3, v0, Lbji;->b:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " sender: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v3, v0, Lbji;->d:Lbdk;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " participant count: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->u:Ljava/util/List;

    if-nez v1, :cond_3

    const-string v1, "(null)"

    .line 1402
    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " timestamp: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1396
    invoke-static {v2, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    :cond_0
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1406
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "empty participants in membership change "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v3, v0, Lbji;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    :cond_1
    move-object/from16 v0, p1

    iget v1, v0, Lbji;->b:I

    packed-switch v1, :pswitch_data_0

    .line 1504
    const-string v1, "Babel_db"

    const-string v2, "Ignoring unsupported membership change event"

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1506
    :cond_2
    :goto_1
    return-void

    .line 1396
    :cond_3
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->u:Ljava/util/List;

    .line 1402
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 1412
    :pswitch_0
    const/4 v1, 0x1

    .line 1413
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lbji;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, p1

    iget-object v2, v0, Lbji;->d:Lbdk;

    .line 1416
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v3

    .line 1415
    invoke-virtual {v2, v3}, Lbdk;->a(Lbdk;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1417
    :cond_4
    const/4 v1, 0x0

    move v14, v1

    .line 1421
    :goto_2
    if-eqz p3, :cond_5

    .line 1422
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbdk;

    .line 1423
    move-object/from16 v0, p1

    iget-object v3, v0, Lbji;->c:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v1, v4}, Lyt;->a(Ljava/lang/String;Lbdk;Z)V

    goto :goto_3

    .line 1428
    :cond_5
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lbji;->u:Ljava/util/List;

    invoke-virtual {p0, v1, v2}, Lyt;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v13

    .line 1430
    move-object/from16 v0, p1

    iget-object v2, v0, Lbji;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lbji;->m:Ljava/lang/String;

    const/4 v4, 0x4

    move-object/from16 v0, p1

    iget-object v5, v0, Lbji;->d:Lbdk;

    const/16 v6, 0xc

    move-object/from16 v0, p1

    iget-wide v9, v0, Lbji;->n:J

    move-object/from16 v0, p1

    iget v11, v0, Lbji;->g:I

    const/4 v12, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v13}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;)J

    move-result-wide v9

    .line 1438
    move-object/from16 v0, p1

    iget-object v2, v0, Lbji;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v3, v0, Lbji;->e:J

    move-object/from16 v0, p1

    iget-wide v5, v0, Lbji;->n:J

    const/4 v7, 0x4

    move-object/from16 v0, p1

    iget-object v8, v0, Lbji;->d:Lbdk;

    const/4 v11, 0x4

    const/4 v12, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v13}, Lyt;->a(Ljava/lang/String;JJILbdk;JILjava/lang/String;Ljava/lang/String;)Z

    .line 1445
    if-eqz v14, :cond_2

    .line 1446
    move-object/from16 v0, p1

    iget-wide v1, v0, Lbji;->e:J

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Lbnl;->a(J)V

    goto/16 :goto_1

    .line 1454
    :pswitch_1
    const/4 v2, 0x0

    .line 1455
    const/4 v13, 0x0

    .line 1456
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_a

    .line 1458
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->u:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbdk;

    .line 1460
    if-eqz v1, :cond_a

    .line 1461
    if-eqz p3, :cond_7

    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v3

    .line 1462
    invoke-virtual {v3, v1}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1464
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->c:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1466
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->c:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lyt;->i(Ljava/lang/String;)V

    .line 1467
    const/4 v1, 0x1

    .line 1488
    :goto_4
    if-nez v1, :cond_2

    .line 1489
    move-object/from16 v0, p1

    iget-object v2, v0, Lbji;->c:Ljava/lang/String;

    .line 1490
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    move-object/from16 v0, p1

    iget-object v5, v0, Lbji;->d:Lbdk;

    move-object/from16 v0, p1

    iget v1, v0, Lbji;->f:I

    const/4 v6, 0x1

    if-ne v1, v6, :cond_9

    const/16 v6, 0xe

    :goto_5
    move-object/from16 v0, p1

    iget-wide v9, v0, Lbji;->n:J

    move-object/from16 v0, p1

    iget v11, v0, Lbji;->g:I

    const/4 v12, 0x0

    move-object v1, p0

    .line 1489
    invoke-virtual/range {v1 .. v13}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;)J

    goto/16 :goto_1

    .line 1469
    :cond_6
    const-string v1, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "attempt to process presence for a nonexistant conversation id ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lbji;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_4

    .line 1474
    :cond_7
    if-eqz p3, :cond_8

    .line 1476
    move-object/from16 v0, p1

    iget-object v3, v0, Lbji;->c:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v1, v4}, Lyt;->a(Ljava/lang/String;Lbdk;Z)V

    .line 1479
    :cond_8
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1481
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1482
    move-object/from16 v0, p1

    iget-object v1, v0, Lbji;->c:Ljava/lang/String;

    invoke-virtual {p0, v1, v3}, Lyt;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v13

    move v1, v2

    goto :goto_4

    .line 1490
    :cond_9
    const/16 v6, 0xb

    goto :goto_5

    :cond_a
    move v1, v2

    goto :goto_4

    :cond_b
    move v14, v1

    goto/16 :goto_2

    .line 1410
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lyt;Lbnl;I)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 2051
    const/4 v0, 0x0

    .line 2052
    invoke-static {}, Lbya;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2053
    new-instance v0, Lbyc;

    invoke-direct {v0}, Lbyc;-><init>()V

    const-string v1, "request_more_conversations"

    invoke-virtual {v0, v1}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "filterMode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2054
    invoke-virtual {v0, v1}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v0

    .line 2055
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v0

    .line 2057
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 2060
    :try_start_0
    invoke-virtual {p0, p2}, Lyt;->c(I)J

    move-result-wide v1

    .line 2061
    const-wide/16 v3, -0x2

    cmp-long v3, v1, v3

    if-nez v3, :cond_3

    .line 2063
    if-eqz v0, :cond_1

    .line 2064
    const-string v1, "continuationEndTimeStamp=exhausted"

    invoke-virtual {v0, v1}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v1

    const-string v2, "notifyExhausted"

    .line 2065
    invoke-virtual {v1, v2}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v1

    .line 2066
    invoke-virtual {v1}, Lbyc;->b()V

    .line 2113
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2115
    invoke-virtual {p0}, Lyt;->c()V

    .line 2117
    if-eqz v0, :cond_2

    .line 2118
    invoke-virtual {v0}, Lbyc;->b()V

    .line 2120
    :cond_2
    return-void

    .line 2069
    :cond_3
    :try_start_1
    const-string v3, "last_successful_sync_time"

    invoke-virtual {p0, v3}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v3

    .line 2072
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_4

    .line 2075
    invoke-virtual {p0, p2}, Lyt;->b(I)J

    move-result-wide v1

    .line 2078
    :cond_4
    sget-boolean v5, Lyp;->a:Z

    if-eqz v5, :cond_5

    .line 2079
    const-string v5, "Babel_db"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "requesting conversations before "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2082
    :cond_5
    const-string v5, "Babel_db"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2083
    const-string v5, "Babel_db"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "create SRC from scroll back, filter="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", oldestConversationTimestamp="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", suppressNotification=true"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087
    :cond_6
    if-eqz v0, :cond_7

    .line 2088
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "continuationEndTimeStamp="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    .line 2090
    :cond_7
    cmp-long v5, v1, v8

    if-gtz v5, :cond_8

    cmp-long v3, v3, v8

    if-nez v3, :cond_8

    const/4 v3, 0x1

    if-eq p2, v3, :cond_a

    .line 2092
    :cond_8
    if-eqz v0, :cond_9

    .line 2093
    const-string v3, "calledSRC"

    invoke-virtual {v0, v3}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    .line 2095
    :cond_9
    new-instance v3, Lbft;

    invoke-direct {v3, v1, v2, p2}, Lbft;-><init>(JI)V

    invoke-virtual {p1, v3}, Lbnl;->a(Lbea;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 2115
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0

    .line 2104
    :cond_a
    if-eqz v0, :cond_b

    .line 2105
    :try_start_2
    const-string v1, "empty"

    invoke-virtual {v0, v1}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    .line 2107
    :cond_b
    const-string v1, "Babel_db"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2108
    const-string v1, "Babel_db"

    const-string v2, "requestMoreConversations - db empty, warm sync executing doing nothing"

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public static a(Lyt;Lbnl;ZZLjava/lang/String;)V
    .locals 11

    .prologue
    .line 390
    const-string v0, "Babel_db"

    const-string v1, "requestWarmSync"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-virtual {p0}, Lyt;->a()V

    .line 397
    const/4 v0, 0x1

    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lyt;->a(II)Ljava/util/List;

    move-result-object v0

    .line 398
    const-string v1, "last_successful_sync_time"

    invoke-virtual {p0, v1}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v1

    .line 401
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    .line 402
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 404
    :cond_0
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_1

    .line 405
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Warm sync fall back to cold sync for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 406
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", create SRC request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 405
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_1
    new-instance v0, Lbft;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbft;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v0}, Lbnl;->a(Lbea;)V

    .line 412
    new-instance v0, Lbft;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbft;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v0}, Lbnl;->a(Lbea;)V

    .line 473
    :goto_0
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    invoke-virtual {p0}, Lyt;->c()V

    .line 477
    const-string v0, "babel_gsi_atwarmsync"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 480
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    .line 482
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_2

    .line 485
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Z)I

    .line 488
    :cond_2
    return-void

    .line 417
    :cond_3
    :try_start_1
    invoke-virtual {p0, v1, v2}, Lyt;->d(J)Ljava/util/List;

    move-result-object v5

    .line 419
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 421
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 423
    const/4 v0, 0x0

    .line 424
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v5, v0

    move-object v6, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 425
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    .line 426
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 427
    sget-boolean v8, Lyp;->a:Z

    if-eqz v8, :cond_4

    .line 428
    const-string v8, "Babel_db"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Warm sync new event since last sync: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_4
    if-eqz v5, :cond_5

    .line 432
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 433
    :cond_5
    if-eqz v5, :cond_6

    .line 434
    new-instance v8, Lbeq;

    invoke-direct {v8, v5, v6}, Lbeq;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 436
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 438
    :cond_6
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 441
    :goto_2
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v6, v5

    move-object v5, v4

    .line 442
    goto :goto_1

    .line 444
    :cond_7
    if-eqz v5, :cond_8

    .line 445
    new-instance v0, Lbeq;

    invoke-direct {v0, v5, v6}, Lbeq;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 447
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 450
    :cond_8
    invoke-virtual {p0}, Lyt;->m()Ljava/util/List;

    move-result-object v4

    .line 463
    new-instance v0, Lbfs;

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lbfs;-><init>(JLjava/util/List;Ljava/util/List;ZZLjava/lang/String;)V

    .line 466
    const-string v3, "Babel_db"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 467
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SyncAllNewEventsRequest "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " noMissedEventsExpected="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " suppressNotifications="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    :cond_9
    invoke-virtual {p1, v0}, Lbnl;->a(Lbea;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 475
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0

    :cond_a
    move-object v4, v5

    move-object v5, v6

    goto :goto_2
.end method

.method public static a(Lyt;Lbxz;)V
    .locals 4

    .prologue
    .line 2376
    const/4 v1, 0x0

    .line 2377
    invoke-virtual {p0}, Lyt;->a()V

    .line 2379
    if-nez p1, :cond_1

    .line 2380
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lyt;->u(Ljava/lang/String;)I

    move-result v0

    .line 2381
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lyt;->w(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2388
    :goto_0
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2390
    invoke-virtual {p0}, Lyt;->c()V

    .line 2392
    if-lez v0, :cond_0

    .line 2393
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    .line 2394
    const/4 v1, 0x1

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lbne;->a(Lyj;ZI)V

    .line 2398
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2400
    :cond_0
    return-void

    .line 2383
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lbxz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2384
    invoke-virtual {p0, v0}, Lyt;->u(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 2385
    invoke-virtual {p0, v0}, Lyt;->w(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    add-int/2addr v1, v0

    .line 2386
    goto :goto_1

    .line 2390
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lyt;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2137
    invoke-virtual {p0, p1}, Lyt;->af(Ljava/lang/String;)V

    .line 2138
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 2139
    return-void
.end method

.method public static a(Lyt;Ljava/lang/String;J)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2482
    invoke-virtual {p0, p1}, Lyt;->e(Ljava/lang/String;)Lyv;

    .line 2485
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lyt;->m(Ljava/lang/String;J)V

    .line 2487
    sget-wide v0, Lyp;->f:J

    .line 2488
    sget-wide v0, Lyp;->g:J

    .line 2491
    sget-wide v0, Lyp;->e:J

    and-long/2addr v0, p2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2496
    new-instance v0, Lbka;

    invoke-direct {v0, p1}, Lbka;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lbka;->a(Lyt;)V

    .line 2500
    :cond_0
    sget-wide v0, Lyp;->c:J

    .line 2501
    sget-wide v0, Lyp;->d:J

    and-long/2addr v0, p2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2506
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyt;Ljava/lang/String;[Ljava/lang/String;)V

    .line 2509
    :cond_1
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 2510
    return-void
.end method

.method public static a(Lyt;Ljava/lang/String;Lbnl;)V
    .locals 7

    .prologue
    .line 1934
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateConversationWatermark conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1937
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 1940
    :try_start_0
    invoke-virtual {p0, p1}, Lyt;->R(Ljava/lang/String;)Lyy;

    move-result-object v0

    .line 1941
    if-eqz v0, :cond_1

    .line 1942
    iget-wide v1, v0, Lyy;->a:J

    .line 1943
    invoke-virtual {p0, p1}, Lyt;->l(Ljava/lang/String;)J

    move-result-wide v3

    .line 1942
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    .line 1944
    iget-wide v3, v0, Lyy;->b:J

    cmp-long v0, v1, v3

    if-lez v0, :cond_1

    .line 1945
    invoke-virtual {p0, p1, v1, v2}, Lyt;->c(Ljava/lang/String;J)V

    .line 1947
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    .line 1948
    const/4 v3, 0x1

    const/4 v4, 0x7

    invoke-static {v0, v3, v4}, Lbne;->a(Lyj;ZI)V

    .line 1952
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    .line 1953
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 1952
    invoke-static {v3, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1954
    if-eqz p2, :cond_2

    .line 1955
    invoke-static {p1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1957
    new-instance v0, Lbfv;

    invoke-direct {v0, p1, v1, v2}, Lbfv;-><init>(Ljava/lang/String;J)V

    invoke-virtual {p2, v0}, Lbnl;->a(Lbea;)V

    .line 1961
    invoke-virtual {p2}, Lbnl;->d()V

    .line 1973
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1975
    invoke-virtual {p0}, Lyt;->c()V

    .line 1978
    invoke-static {p0, p1}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 1979
    return-void

    .line 1962
    :cond_2
    :try_start_1
    invoke-static {p1}, Lyt;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1965
    invoke-virtual {p0, p1}, Lyt;->aa(Ljava/lang/String;)J

    move-result-wide v3

    .line 1966
    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-eqz v0, :cond_1

    .line 1967
    const-wide/16 v5, 0x3e8

    div-long v0, v1, v5

    invoke-static {v3, v4, v0, v1}, Lbvx;->a(JJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1975
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public static a(Lyt;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2156
    const/4 v0, 0x3

    invoke-virtual {p0, p1, p2, v0, p3}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2157
    return-void
.end method

.method public static a(Lyt;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJZJJ)V
    .locals 10

    .prologue
    .line 3777
    :try_start_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 3778
    invoke-virtual {p0, p1, p2}, Lyt;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    .line 3779
    if-nez v9, :cond_0

    .line 3781
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processSmsMmsSendResponse: received response for nonexistant  clientGeneratedId "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3806
    :goto_0
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3808
    invoke-virtual {p0}, Lyt;->c()V

    .line 3811
    invoke-static {p0, p1}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 3813
    invoke-static {p3}, Lyp;->b(Landroid/net/Uri;)V

    .line 3814
    return-void

    .line 3784
    :cond_0
    if-eqz p8, :cond_1

    .line 3790
    :try_start_1
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    const/4 v3, 0x1

    .line 3791
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v3, p3

    move-wide/from16 v4, p9

    .line 3790
    invoke-static/range {v2 .. v8}, Lyp;->a(Lyj;Landroid/net/Uri;JLjava/lang/Boolean;ZZ)V

    .line 3797
    :cond_1
    invoke-static {v9}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    const-wide/16 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-wide/from16 v6, p6

    .line 3796
    invoke-virtual/range {v2 .. v9}, Lyt;->a(Ljava/lang/String;JJJ)V

    .line 3799
    invoke-virtual/range {p0 .. p5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;J)V

    .line 3802
    move-wide/from16 v0, p6

    invoke-virtual {p0, p1, v0, v1}, Lyt;->j(Ljava/lang/String;J)I

    .line 3804
    move-wide/from16 v0, p11

    invoke-virtual {p0, p1, v0, v1}, Lyt;->p(Ljava/lang/String;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3808
    :catchall_0
    move-exception v2

    invoke-virtual {p0}, Lyt;->c()V

    throw v2
.end method

.method private static a(Lyt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJJII)V
    .locals 29

    .prologue
    .line 3932
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v28

    .line 3933
    invoke-static {}, Lyt;->g()Landroid/content/Context;

    move-result-object v7

    .line 3934
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 3935
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3938
    new-instance v3, Lbdk;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Lbdk;-><init>(Ljava/lang/String;)V

    .line 3939
    if-eqz p4, :cond_0

    move-object v2, v3

    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v2, v1}, Lbvx;->a(Lyt;Lbdk;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v3, v2

    .line 3949
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3950
    const-string v2, "Babel_db"

    const-string v3, "EsConversationsData.storeMmsNotificationMessage: failed to get conv."

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 4014
    :goto_2
    return-void

    .line 3939
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 3944
    :cond_1
    invoke-static {}, Lbvx;->f()Lbdh;

    move-result-object v2

    .line 3945
    iget-object v4, v2, Lbdh;->b:Lbdk;

    .line 3946
    const/4 v3, 0x1

    new-array v3, v3, [Lbdh;

    const/4 v5, 0x0

    aput-object v2, v3, v5

    .line 3947
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 3946
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-static {v0, v1, v2}, Lbvx;->a(Lyt;ZLjava/util/List;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 3953
    :cond_2
    const/16 v2, 0x3e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lyt;->b(Ljava/lang/String;I)J

    move-result-wide v5

    .line 3955
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 3956
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {p5 .. p6}, Lbvx;->b(J)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lh;->fQ:I

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v2, v9

    const/4 v9, 0x1

    const-wide/16 v10, 0x3e8

    mul-long v10, v10, p7

    const v12, 0x20019

    .line 3957
    invoke-static {v7, v10, v11, v12}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v9

    .line 3955
    move/from16 v0, p10

    invoke-virtual {v8, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 3966
    new-instance v2, Lbqs;

    .line 3970
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x2

    .line 3972
    invoke-static {v8, v9}, Lf;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    if-nez p9, :cond_3

    const/4 v12, 0x4

    :goto_3
    const/4 v14, 0x3

    const/4 v15, 0x0

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/16 v16, 0x0

    aput-object p1, v13, v16

    const/16 v16, 0x1

    aput-object p2, v13, v16

    .line 3982
    invoke-static {v13}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-wide/16 v17, 0x0

    const/16 v19, 0x0

    const-wide/16 v20, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x2

    const/16 v24, 0x0

    const/16 v25, 0x0

    const-wide/16 v26, 0x0

    move/from16 v13, p9

    invoke-direct/range {v2 .. v27}, Lbqs;-><init>(Ljava/lang/String;Lbdk;JLjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZIIILjava/lang/String;Ljava/lang/String;JIJLjava/lang/String;ILjava/lang/String;IJ)V

    const/4 v4, 0x2

    .line 3993
    invoke-virtual {v2, v4}, Lbqs;->a(I)Lbqs;

    move-result-object v2

    new-instance v4, Lbnl;

    invoke-direct {v4}, Lbnl;-><init>()V

    .line 3994
    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v4}, Lbqs;->a(Lyt;Lbnl;)V

    .line 3996
    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    .line 3998
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5, v6}, Lyt;->i(Ljava/lang/String;J)V

    .line 3999
    if-eqz p4, :cond_4

    .line 4002
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lyt;->u(Ljava/lang/String;)I

    .line 4011
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4013
    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    goto/16 :goto_2

    .line 3972
    :cond_3
    const/4 v12, 0x3

    goto :goto_3

    .line 4005
    :cond_4
    const/4 v2, 0x0

    .line 4006
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x7

    .line 4005
    move-object/from16 v0, v28

    invoke-static {v0, v2, v3, v4}, Lbne;->a(Lyj;ZZI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 4013
    :catchall_0
    move-exception v2

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v2
.end method

.method public static a(Lyt;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1780
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1781
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "forking a new conversation for an existing conversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with additional participants: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785
    :cond_0
    invoke-virtual {p0, p1}, Lyt;->K(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1787
    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1788
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v1

    .line 1789
    invoke-static {v0}, Lf;->a(Ljava/util/Collection;)Lxm;

    move-result-object v0

    .line 1788
    invoke-static {v1, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lxm;Ljava/lang/String;)I

    .line 1791
    return-void
.end method

.method public static a(Lyt;Ljava/lang/String;Ljava/util/List;Lbnl;)V
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbcx;",
            ">;",
            "Lbnl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2253
    const-string v3, "Babel_db"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2254
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "inviteParticipantsLocally: conversationId="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2256
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    .line 2261
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lyt;->M(Ljava/lang/String;)Lbza;

    move-result-object v6

    .line 2264
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 2265
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbcx;

    .line 2269
    iget-object v4, v3, Lbcx;->c:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 2270
    iget-object v4, v3, Lbcx;->c:Ljava/lang/String;

    iget-object v3, v3, Lbcx;->e:Ljava/lang/String;

    invoke-static {v4, v3}, Lbcx;->b(Ljava/lang/String;Ljava/lang/String;)Lbcx;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2332
    :catchall_0
    move-exception v3

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v3

    .line 2272
    :cond_2
    :try_start_1
    invoke-virtual {v3}, Lbcx;->a()Lbdk;

    move-result-object v8

    .line 2273
    const/4 v5, 0x0

    .line 2274
    invoke-virtual {v6}, Lbza;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbdk;

    .line 2275
    invoke-virtual {v4, v8}, Lbdk;->a(Lbdk;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2276
    const/4 v4, 0x1

    .line 2280
    :goto_1
    if-nez v4, :cond_1

    .line 2281
    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2286
    :cond_4
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_5

    .line 2287
    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2332
    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    .line 2333
    :goto_2
    return-void

    .line 2294
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lyt;->b(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v15

    .line 2300
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v7

    .line 2302
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v5

    .line 2303
    invoke-virtual/range {p0 .. p1}, Lyt;->Q(Ljava/lang/String;)J

    move-result-wide v9

    .line 2304
    const/4 v6, 0x1

    const/16 v8, 0xc

    const-wide/16 v11, -0x1

    const/16 v13, 0xa

    const/4 v14, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v15}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;)J

    move-result-wide v24

    .line 2316
    const-wide/16 v20, -0x1

    const/16 v22, 0x4

    .line 2320
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v23

    const/16 v26, 0x1

    const/16 v27, 0x0

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move-wide/from16 v18, v9

    move-object/from16 v28, v15

    .line 2316
    invoke-virtual/range {v16 .. v28}, Lyt;->a(Ljava/lang/String;JJILbdk;JILjava/lang/String;Ljava/lang/String;)Z

    .line 2323
    invoke-static/range {p1 .. p1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2324
    new-instance v11, Lbeb;

    move-object v12, v5

    move-object/from16 v13, p1

    move-object/from16 v14, v29

    move-wide v15, v9

    invoke-direct/range {v11 .. v16}, Lbeb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;J)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lbnl;->a(Lbea;)V

    .line 2330
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2332
    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    goto :goto_2

    :cond_7
    move v4, v5

    goto :goto_1
.end method

.method private static final a(Lyt;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2907
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_0

    .line 2908
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "insertTemporaryConversationParticipants "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2912
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 2913
    invoke-virtual {p0, p1, v0}, Lyt;->a(Ljava/lang/String;Lbdh;)V

    goto :goto_0

    .line 2916
    :cond_1
    if-eqz p3, :cond_2

    .line 2917
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxo;

    .line 2918
    invoke-virtual {p0, p1, v0}, Lyt;->a(Ljava/lang/String;Lxo;)V

    goto :goto_1

    .line 2921
    :cond_2
    invoke-virtual {p0, p1}, Lyt;->J(Ljava/lang/String;)V

    .line 2924
    :cond_3
    invoke-static {p0, p1}, Lyp;->b(Lyt;Ljava/lang/String;)V

    .line 2925
    return-void
.end method

.method public static a(Lyt;Ljava/lang/String;Lyv;JLbnl;JJ)V
    .locals 19

    .prologue
    .line 1807
    const-string v3, "Babel_db"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1808
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "resendMessageLocally messageRowId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1810
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lyt;->a()V

    .line 1811
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v15, v3, v5

    .line 1814
    :try_start_0
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lyt;->b(J)Lzg;

    move-result-object v9

    .line 1815
    const/4 v7, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-wide/from16 v5, p3

    move-object/from16 v8, p5

    invoke-static/range {v3 .. v8}, Lyp;->a(Lyt;Lyv;JZLbnl;)V

    .line 1817
    invoke-virtual/range {p0 .. p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1819
    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    .line 1823
    if-eqz v9, :cond_1

    iget-object v3, v9, Lzg;->a:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1824
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v17, v3, v5

    .line 1825
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v3

    .line 1826
    iget-object v4, v9, Lzg;->a:Ljava/lang/String;

    .line 1827
    invoke-static {v4}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v11

    .line 1829
    const/16 v6, 0xa

    const/16 v7, 0x12e

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/4 v14, 0x0

    move-wide/from16 v4, p6

    move-object/from16 v13, p1

    invoke-static/range {v3 .. v14}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 1839
    const/16 v6, 0xa

    const/16 v7, 0xcd

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/4 v14, 0x0

    move-wide/from16 v4, p8

    move-object/from16 v13, p1

    invoke-static/range {v3 .. v14}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 1849
    const/16 v6, 0xa

    const/16 v7, 0x69

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/4 v14, 0x0

    move-wide v4, v15

    move-object/from16 v13, p1

    invoke-static/range {v3 .. v14}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 1859
    const/16 v6, 0xa

    const/16 v7, 0x6a

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/4 v14, 0x0

    move-wide/from16 v4, v17

    move-object/from16 v13, p1

    invoke-static/range {v3 .. v14}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 1871
    :cond_1
    invoke-static/range {p0 .. p1}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 1872
    return-void

    .line 1819
    :catchall_0
    move-exception v3

    invoke-virtual/range {p0 .. p0}, Lyt;->c()V

    throw v3
.end method

.method private static a(Lyt;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2608
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_0

    .line 2609
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processPendingMute "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612
    :cond_0
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v1

    if-eqz p2, :cond_1

    const/16 v0, 0xa

    .line 2611
    :goto_0
    invoke-static {v1, p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;I)I

    .line 2615
    return-void

    .line 2612
    :cond_1
    const/16 v0, 0x1e

    goto :goto_0
.end method

.method public static a(Lyt;Ljava/lang/String;[BJ)V
    .locals 4

    .prologue
    .line 498
    invoke-virtual {p0, p1}, Lyt;->g(Ljava/lang/String;)J

    move-result-wide v0

    .line 502
    cmp-long v2, p3, v0

    if-lez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 504
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lyt;->a(Ljava/lang/String;[BJ)V

    .line 506
    :cond_1
    return-void
.end method

.method private static final a(Lyt;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2935
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2936
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    .line 2937
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 2938
    invoke-virtual {v0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v4

    .line 2939
    if-eqz v4, :cond_0

    .line 2940
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 2943
    iget-object v0, v0, Lbdh;->b:Lbdk;

    .line 2944
    const-string v5, "phone_id"

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2947
    const/4 v4, 0x0

    iget-object v0, v0, Lbdk;->b:Ljava/lang/String;

    aput-object v0, v2, v4

    .line 2948
    invoke-virtual {p0}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v4, "participants"

    const-string v5, "chat_id=?"

    invoke-virtual {v0, v4, v1, v5, v2}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 2952
    :cond_1
    return-void
.end method

.method public static a(Lyt;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/util/List",
            "<",
            "Lbcw;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ)V"
        }
    .end annotation

    .prologue
    .line 1031
    const-string v2, "Babel_db"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1032
    const-string v2, "Babel_db"

    const-string v3, "processContactErrorInTransaction"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1037
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {p0, v0, v1}, Lyt;->c(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v14

    .line 1038
    const/4 v4, 0x0

    .line 1039
    const/4 v3, 0x0

    .line 1040
    const/4 v2, 0x0

    .line 1041
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v5, v4

    move v4, v3

    move v3, v2

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbcw;

    .line 1042
    iget v8, v2, Lbcw;->b:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_3

    .line 1043
    const/4 v4, 0x1

    .line 1050
    :cond_2
    :goto_1
    iget-object v2, v2, Lbcw;->a:Lbcx;

    .line 1051
    iget-object v8, v2, Lbcx;->a:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1054
    iget-object v2, v2, Lbcx;->a:Ljava/lang/String;

    invoke-static {v2}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v2

    .line 1062
    :goto_2
    invoke-virtual {p0, v2}, Lyt;->b(Lbdk;)Ljava/lang/String;

    move-result-object v8

    .line 1063
    if-nez v8, :cond_1

    .line 1064
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1045
    :cond_3
    const/4 v3, 0x1

    .line 1046
    iget v8, v2, Lbcw;->b:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_2

    .line 1047
    const/4 v5, 0x1

    goto :goto_1

    .line 1055
    :cond_4
    iget-object v8, v2, Lbcx;->b:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 1056
    iget-object v2, v2, Lbcx;->b:Ljava/lang/String;

    invoke-static {v2}, Lbdk;->c(Ljava/lang/String;)Lbdk;

    move-result-object v2

    goto :goto_2

    .line 1058
    :cond_5
    const-string v2, "Babel_db"

    const-string v8, "processContactErrorInTransaction: server sends empty ids!"

    invoke-static {v2, v8}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1068
    :cond_6
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 1070
    new-instance v2, Lbnl;

    invoke-direct {v2}, Lbnl;-><init>()V

    .line 1071
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v7

    invoke-virtual {v2, v7}, Lbnl;->a(Lyj;)V

    .line 1072
    move-object/from16 v0, p2

    invoke-virtual {v2, p0, v0, v6}, Lbnl;->a(Lyt;Ljava/lang/String;Ljava/util/List;)Z

    .line 1073
    invoke-virtual {v2}, Lbnl;->c()V

    .line 1075
    :cond_7
    const/4 v7, 0x5

    .line 1076
    if-eqz v5, :cond_9

    .line 1079
    const/16 v7, 0xd

    .line 1084
    :cond_8
    :goto_3
    const/4 v5, 0x4

    .line 1088
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->c()Lbdk;

    move-result-object v6

    const/16 v12, 0x1e

    const/4 v13, 0x0

    move-object v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-wide/from16 v8, p4

    move-wide/from16 v10, p6

    .line 1084
    invoke-virtual/range {v2 .. v14}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;)J

    .line 1096
    const/4 v2, 0x3

    const/4 v3, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {p0, v0, v1, v2, v3}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 1098
    return-void

    .line 1080
    :cond_9
    if-eqz v4, :cond_8

    if-nez v3, :cond_8

    .line 1082
    const/16 v7, 0xf

    goto :goto_3
.end method

.method private static a(Lyt;Lyv;JZLbnl;)V
    .locals 19

    .prologue
    .line 2691
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lyt;->b(J)Lzg;

    move-result-object v15

    .line 2692
    if-eqz v15, :cond_0

    iget-object v4, v15, Lzg;->b:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 2695
    :cond_0
    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No such message "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2743
    :goto_0
    return-void

    .line 2699
    :cond_1
    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_2

    .line 2700
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sending queued message type: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v15, Lzg;->g:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " conversationId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v15, Lzg;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " timestamp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v15, Lzg;->h:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2705
    :cond_2
    iget v4, v15, Lzg;->g:I

    packed-switch v4, :pswitch_data_0

    .line 2739
    :pswitch_0
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unknown message type to send: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v15, Lzg;->g:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2707
    :pswitch_1
    iget v4, v15, Lzg;->w:I

    invoke-static {v4}, Lf;->b(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2710
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move-object/from16 v3, p5

    invoke-static {v0, v1, v2, v3, v15}, Lyp;->a(Lyt;JLbnl;Lzg;)V

    goto :goto_0

    .line 2712
    :cond_3
    iget v4, v15, Lzg;->f:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_4

    const-string v4, "Babel_db"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Skip sending message since it is already on server:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v15, Lzg;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p1

    invoke-static {v0, v1, v15, v2}, Lbps;->a(Lyt;Lbnl;Lzg;Lyv;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v4

    if-eqz p4, :cond_6

    iget-object v14, v15, Lzg;->j:Ljava/lang/String;

    if-nez v14, :cond_5

    iget-object v14, v15, Lzg;->k:Ljava/lang/String;

    :cond_5
    iget-object v5, v15, Lzg;->b:Ljava/lang/String;

    iget-wide v8, v15, Lzg;->i:J

    const/4 v10, 0x6

    iget-object v11, v15, Lzg;->o:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v12

    iget-object v13, v15, Lzg;->e:Ljava/lang/String;

    iget v0, v15, Lzg;->y:I

    move/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v4, p0

    move-wide/from16 v15, p2

    invoke-virtual/range {v4 .. v18}, Lyt;->a(Ljava/lang/String;JJILjava/lang/String;Lbdk;Ljava/lang/String;Ljava/lang/String;JII)Z

    :cond_6
    const/4 v10, 0x1

    if-eqz p4, :cond_7

    move-wide v8, v6

    :goto_1
    move-object/from16 v4, p0

    move-wide/from16 v5, p2

    move v7, v10

    invoke-virtual/range {v4 .. v9}, Lyt;->a(JIJ)V

    goto/16 :goto_0

    :cond_7
    const-wide/16 v8, -0x1

    goto :goto_1

    .line 2718
    :pswitch_2
    const/4 v4, 0x1

    move-object/from16 v0, p5

    invoke-static {v0, v15, v4}, Lyp;->a(Lbnl;Lzg;Z)V

    goto/16 :goto_0

    .line 2723
    :pswitch_3
    const/4 v4, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v15, v4}, Lyp;->a(Lbnl;Lzg;Z)V

    goto/16 :goto_0

    .line 2729
    :pswitch_4
    iget-object v4, v15, Lzg;->E:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lyt;->L(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    new-instance v4, Lbeb;

    iget-object v5, v15, Lzg;->a:Ljava/lang/String;

    iget-object v6, v15, Lzg;->b:Ljava/lang/String;

    iget-wide v8, v15, Lzg;->h:J

    invoke-direct/range {v4 .. v9}, Lbeb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;J)V

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lbnl;->a(Lbea;)V

    goto/16 :goto_0

    .line 2734
    :pswitch_5
    new-instance v4, Lbex;

    iget-object v5, v15, Lzg;->a:Ljava/lang/String;

    iget-object v6, v15, Lzg;->b:Ljava/lang/String;

    iget-object v7, v15, Lzg;->D:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct {v4, v5, v6, v7, v8}, Lbex;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lbnl;->a(Lbea;)V

    goto/16 :goto_0

    .line 2705
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public static a([Landroid/telephony/SmsMessage;Lyj;ILjava/lang/Boolean;)V
    .locals 33
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 3630
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v5

    .line 3632
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v5, v0, v1}, Lbvx;->a(Landroid/content/Context;[Landroid/telephony/SmsMessage;I)Landroid/content/ContentValues;

    move-result-object v28

    .line 3633
    const-string v2, "address"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 3634
    const-string v2, "body"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 3635
    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3636
    :cond_0
    const-string v2, "Babel_db"

    const-string v3, "EsConversationsData.storeSmsMessage: empty sender or body"

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3749
    :goto_0
    return-void

    .line 3640
    :cond_1
    new-instance v30, Lyt;

    move-object/from16 v0, v30

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 3643
    new-instance v4, Lbdk;

    move-object/from16 v0, v29

    invoke-direct {v4, v0}, Lbdk;-><init>(Ljava/lang/String;)V

    .line 3645
    invoke-static {v4}, Lyp;->a(Lbdk;)Z

    move-result v31

    .line 3647
    if-eqz v31, :cond_2

    move-object v2, v4

    :goto_1
    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-static {v0, v2, v1}, Lbvx;->a(Lyt;Lbdk;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3649
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3650
    const-string v2, "Babel_db"

    const-string v3, "EsConversationsData.storeSmsMessage: empty conversation id"

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3647
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 3653
    :cond_3
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;)Z

    move-result v32

    .line 3655
    if-nez p3, :cond_4

    .line 3657
    invoke-static/range {v32 .. v32}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    .line 3659
    :cond_4
    const/4 v2, 0x0

    aget-object v2, p0, v2

    .line 3660
    const/16 v6, 0x3e8

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v6}, Lyt;->b(Ljava/lang/String;I)J

    move-result-wide v6

    const-wide/16 v10, 0x3e8

    div-long/2addr v6, v10

    invoke-static {v2, v6, v7}, Lbvx;->a(Landroid/telephony/SmsMessage;J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v6

    .line 3664
    const-string v2, "date"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3665
    const-string v8, "read"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    .line 3666
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3665
    :goto_2
    move-object/from16 v0, v28

    invoke-virtual {v0, v8, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3668
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Landroid/provider/Telephony$Sms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v28

    invoke-static {v2, v5, v0}, Lf;->b(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 3670
    const/16 v16, 0x0

    .line 3671
    if-eqz v2, :cond_8

    .line 3672
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-static {v5}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 3677
    :goto_3
    const-wide/16 v10, 0x3e8

    mul-long v5, v6, v10

    .line 3678
    const-string v2, "date_sent"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 3679
    if-nez v2, :cond_9

    const-wide/16 v17, 0x0

    .line 3682
    :goto_4
    new-instance v2, Lbqs;

    .line 3686
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    const/4 v14, 0x3

    const/4 v15, 0x0

    const/16 v19, 0x0

    const-wide/16 v20, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x2

    const/16 v24, 0x0

    const/16 v25, 0x0

    const-wide/16 v26, 0x0

    invoke-direct/range {v2 .. v27}, Lbqs;-><init>(Ljava/lang/String;Lbdk;JLjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZIIILjava/lang/String;Ljava/lang/String;JIJLjava/lang/String;ILjava/lang/String;IJ)V

    const/4 v4, 0x0

    .line 3706
    invoke-virtual {v2, v4}, Lbqs;->a(I)Lbqs;

    move-result-object v2

    .line 3707
    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Lbqs;->a(Ljava/lang/String;)Lbqs;

    move-result-object v2

    new-instance v4, Lbnl;

    invoke-direct {v4}, Lbnl;-><init>()V

    .line 3708
    move-object/from16 v0, v30

    invoke-virtual {v2, v0, v4}, Lbqs;->a(Lyt;Lbnl;)V

    .line 3710
    invoke-virtual/range {v30 .. v30}, Lyt;->a()V

    .line 3713
    :try_start_0
    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v5, v6}, Lyt;->j(Ljava/lang/String;J)I

    .line 3715
    const-string v2, "reply_path_present"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 3716
    const-string v4, "service_center"

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3717
    if-eqz v2, :cond_a

    const/4 v7, 0x0

    invoke-static {v2, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_a

    .line 3718
    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Lyt;->k(Ljava/lang/String;Ljava/lang/String;)V

    .line 3725
    :goto_5
    const-string v2, "thread_id"

    .line 3726
    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v7

    .line 3725
    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v7, v8}, Lyt;->p(Ljava/lang/String;J)V

    .line 3727
    if-nez v31, :cond_5

    .line 3729
    const/4 v2, 0x0

    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-static {v0, v2, v1, v4}, Lbne;->a(Lyj;ZZI)V

    .line 3734
    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 3737
    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v5, v6}, Lyt;->c(Ljava/lang/String;J)V

    .line 3744
    :cond_6
    :goto_6
    invoke-virtual/range {v30 .. v30}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3746
    invoke-virtual/range {v30 .. v30}, Lyt;->c()V

    .line 3748
    move-object/from16 v0, v30

    invoke-static {v0, v3}, Lyp;->d(Lyt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3666
    :cond_7
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_2

    .line 3674
    :cond_8
    const-string v2, "Babel_db"

    const-string v5, "storeSmsMessage: failed to insert SMS to telephony"

    invoke-static {v2, v5}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3680
    :cond_9
    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v7

    const-wide/16 v10, 0x3e8

    mul-long v17, v7, v10

    goto/16 :goto_4

    .line 3720
    :cond_a
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v2}, Lyt;->k(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 3746
    :catchall_0
    move-exception v2

    invoke-virtual/range {v30 .. v30}, Lyt;->c()V

    throw v2

    .line 3739
    :cond_b
    if-eqz v31, :cond_6

    .line 3742
    :try_start_2
    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Lyt;->u(Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6
.end method

.method public static a(Lbdk;)Z
    .locals 2

    .prologue
    .line 4334
    const/4 v0, 0x0

    .line 4335
    if-eqz p0, :cond_0

    iget-object v1, p0, Lbdk;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4336
    new-instance v0, Lyt;

    .line 4337
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v1

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 4338
    iget-object v1, p0, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lyt;->G(Ljava/lang/String;)Z

    move-result v0

    .line 4340
    :cond_0
    return v0
.end method

.method public static a(Lyt;Lbjc;JLjava/lang/String;Lbnl;Lys;Z)Z
    .locals 6

    .prologue
    .line 1124
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1125
    const-string v1, "Babel_db"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "processConversationInTransaction conversationId: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", clientGeneratedId: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lbjc;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", requestClientGeneratedId: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", participantCount: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lbjc;->g:Ljava/util/List;

    .line 1129
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", notificationLevel: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lbjc;->k:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", status: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lbjc;->l:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", view: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lbjc;->m:[I

    .line 1132
    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", hasActiveHangout: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p1, Lbjc;->p:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", selfReadState.latestReadTimestamp: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p1, Lbjc;->o:Lbjv;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1135
    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", otrStatus: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lbjc;->q:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", sortTimestamp: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, Lbjc;->t:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", otrToggle: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lbjc;->r:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isTemporary: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p1, Lbjc;->x:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1125
    invoke-static {v1, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1151
    iget-object v0, p1, Lbjc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 1152
    iget-object v0, v0, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1153
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1132
    :cond_1
    iget-object v0, p1, Lbjc;->o:Lbjv;

    iget-wide v3, v0, Lbjv;->b:J

    .line 1135
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 1163
    :cond_2
    if-nez p7, :cond_3

    iget v0, p1, Lbjc;->l:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    .line 1165
    invoke-virtual {p0, v0}, Lyt;->s(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1167
    iget-object v0, p1, Lbjc;->m:[I

    if-eqz v0, :cond_3

    iget-object v0, p1, Lbjc;->m:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1168
    iget-object v0, p1, Lbjc;->m:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1169
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1170
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lyt;->C(Ljava/lang/String;)V

    .line 1171
    const/4 v0, 0x1

    .line 1367
    :goto_2
    return v0

    .line 1180
    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_6

    .line 1181
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_4

    .line 1182
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got upperBoundTimestamp "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " calling deleteConversation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    :cond_4
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, p2, p3}, Lyt;->e(Ljava/lang/String;J)Z

    .line 1189
    iget-wide v0, p1, Lbjc;->t:J

    cmp-long v0, v0, p2

    if-gtz v0, :cond_6

    .line 1190
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_5

    .line 1191
    const-string v0, "Babel"

    const-string v1, "upperBoundTimestamp >= timestamp, returning"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1193
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 1197
    :cond_6
    const/4 v1, 0x0

    .line 1198
    iget v0, p1, Lbjc;->b:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1b

    .line 1199
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v2

    .line 1200
    iget-object v0, p1, Lbjc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 1201
    invoke-virtual {v0, v2}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1202
    const/4 v2, 0x1

    const/4 v3, 0x1

    .line 1203
    invoke-virtual {p0, v0, v2, v3}, Lyt;->a(Lbdk;ZI)Lzi;

    move-result-object v0

    .line 1205
    if-eqz v0, :cond_1b

    .line 1206
    iget-object v0, v0, Lzi;->a:Ljava/lang/String;

    .line 1212
    :goto_3
    iget-object v1, p1, Lbjc;->c:Ljava/lang/String;

    iget-object v2, p1, Lbjc;->j:Ljava/lang/String;

    invoke-virtual {p0, v1, p4, v2, v0}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lzd;

    move-result-object v2

    .line 1215
    iget-object v3, v2, Lzd;->a:Lza;

    .line 1216
    const/4 v0, 0x0

    .line 1217
    iget-boolean v1, v2, Lzd;->b:Z

    if-eqz v1, :cond_8

    .line 1218
    const/4 v0, 0x1

    .line 1220
    :cond_8
    sget-boolean v1, Lyp;->a:Z

    if-eqz v1, :cond_9

    .line 1221
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "getExistingMergedConversationId returned "

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v3, :cond_d

    const-string v1, "(null)"

    :goto_4
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " conversationIdsMerged: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, v2, Lzd;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[EsConversationsData] "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    :cond_9
    if-nez v3, :cond_e

    .line 1228
    const/4 v1, 0x0

    move-object v2, v1

    .line 1235
    :goto_5
    if-eqz v2, :cond_11

    .line 1237
    iget-object v1, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1240
    const/4 v0, 0x1

    .line 1244
    :cond_a
    if-nez v0, :cond_f

    iget-object v1, v3, Lza;->b:[B

    if-eqz v1, :cond_f

    iget-object v1, v3, Lza;->b:[B

    iget-object v4, p1, Lbjc;->u:[B

    .line 1245
    invoke-static {v1, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1247
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_b

    .line 1248
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Skipping update "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v3, Lza;->b:[B

    .line 1249
    invoke-static {v2}, Lf;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1248
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    :cond_b
    iget-object v0, p1, Lbjc;->h:Ljava/util/List;

    iget-object v1, p1, Lbjc;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lyp;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 1253
    if-eqz p6, :cond_c

    .line 1254
    iget-object v0, v3, Lza;->a:Ljava/lang/String;

    iput-object v0, p6, Lys;->a:Ljava/lang/String;

    .line 1256
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1221
    :cond_d
    iget-object v1, v3, Lza;->a:Ljava/lang/String;

    goto :goto_4

    .line 1230
    :cond_e
    iget-object v1, v3, Lza;->a:Ljava/lang/String;

    .line 1233
    invoke-virtual {p0, v1}, Lyt;->o(Ljava/lang/String;)Z

    move-object v2, v1

    goto :goto_5

    .line 1258
    :cond_f
    iget v1, v3, Lza;->c:I

    invoke-virtual {p0, v2, v1, p1}, Lyt;->a(Ljava/lang/String;ILbjc;)V

    .line 1260
    iget-object v1, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lyt;->J(Ljava/lang/String;)V

    move v1, v0

    .line 1269
    :goto_6
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    iget-object v3, p1, Lbjc;->g:Ljava/util/List;

    invoke-virtual {p5, p0, v0, v3}, Lbnl;->a(Lyt;Ljava/lang/String;Ljava/util/List;)Z

    .line 1274
    iget-object v0, p1, Lbjc;->g:Ljava/util/List;

    iget-object v3, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v3}, Lyt;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 1277
    iget-object v0, p1, Lbjc;->i:Ljava/util/List;

    if-eqz v0, :cond_13

    .line 1278
    iget-object v0, p1, Lbjc;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_10
    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcj;

    .line 1279
    iget-object v4, v0, Lbcj;->a:Lbdk;

    if-eqz v4, :cond_10

    iget-object v4, v0, Lbcj;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 1280
    invoke-virtual {p0, v0}, Lyt;->a(Lbcj;)V

    goto :goto_7

    .line 1262
    :cond_11
    invoke-virtual {p0, p1}, Lyt;->a(Lbjc;)V

    .line 1264
    iget v1, p1, Lbjc;->l:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_12

    .line 1265
    iget-wide v3, p1, Lbjc;->e:J

    invoke-virtual {p5, v3, v4}, Lbnl;->a(J)V

    :cond_12
    move v1, v0

    goto :goto_6

    .line 1285
    :cond_13
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lyp;->b(Lyt;Ljava/lang/String;)V

    .line 1288
    if-eqz v1, :cond_14

    .line 1289
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    iget-object v1, p1, Lbjc;->c:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    invoke-static {p5, p0, v0}, Lyp;->a(Lbnl;Lyt;Ljava/lang/String;)V

    .line 1301
    :cond_14
    if-eqz v2, :cond_16

    .line 1303
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    sget-wide v1, Lyp;->c:J

    sget-wide v3, Lyp;->d:J

    add-long/2addr v1, v3

    invoke-virtual {p0, v0, v1, v2}, Lyt;->o(Ljava/lang/String;J)Z

    move-result v0

    .line 1305
    if-eqz v0, :cond_16

    .line 1308
    if-eqz p6, :cond_15

    .line 1309
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    iput-object v0, p6, Lys;->a:Ljava/lang/String;

    .line 1311
    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1315
    :cond_16
    iget-object v0, p1, Lbjc;->o:Lbjv;

    if-eqz v0, :cond_17

    .line 1316
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    iget-object v1, p1, Lbjc;->o:Lbjv;

    iget-wide v1, v1, Lbjv;->b:J

    invoke-virtual {p0, v0, v1, v2}, Lyt;->c(Ljava/lang/String;J)V

    .line 1319
    :cond_17
    iget-object v0, p1, Lbjc;->h:Ljava/util/List;

    iget-object v1, p1, Lbjc;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lyp;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 1324
    iget v1, p1, Lbjc;->q:I

    iget v2, p1, Lbjc;->r:I

    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    .line 1326
    invoke-virtual {p0, v0}, Lyt;->Q(Ljava/lang/String;)J

    move-result-wide v3

    iget-object v5, p1, Lbjc;->c:Ljava/lang/String;

    move-object v0, p0

    .line 1324
    invoke-virtual/range {v0 .. v5}, Lyt;->a(IIJLjava/lang/String;)V

    .line 1328
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    iget-wide v1, p1, Lbjc;->t:J

    invoke-virtual {p0, v0, v1, v2}, Lyt;->j(Ljava/lang/String;J)I

    .line 1329
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    iget-object v1, p1, Lbjc;->w:Ljava/lang/Boolean;

    invoke-virtual {p0, v0, v1}, Lyt;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1331
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    iget-boolean v1, p1, Lbjc;->x:Z

    invoke-virtual {p0, v0, v1}, Lyt;->a(Ljava/lang/String;Z)V

    .line 1332
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    iget-object v1, p1, Lbjc;->z:Lbcm;

    iget-object v2, p1, Lbjc;->y:Ljava/util/List;

    invoke-virtual {p0, v0, v1, v2}, Lyt;->a(Ljava/lang/String;Lbcm;Ljava/util/List;)V

    .line 1339
    if-eqz p7, :cond_19

    .line 1340
    iget-object v0, p1, Lbjc;->m:[I

    if-eqz v0, :cond_18

    iget-object v0, p1, Lbjc;->m:[I

    array-length v0, v0

    if-lez v0, :cond_18

    .line 1341
    iget-object v0, p1, Lbjc;->m:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1342
    const/4 v1, 0x2

    if-ne v0, v1, :cond_18

    .line 1344
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    iget-object v1, p1, Lbjc;->c:Ljava/lang/String;

    iget-wide v2, p1, Lbjc;->e:J

    const/4 v4, 0x0

    .line 1343
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;JZ)I

    .line 1351
    :cond_18
    iget v0, p1, Lbjc;->l:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_19

    .line 1352
    new-instance v0, Lbey;

    iget-object v1, p1, Lbjc;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lbey;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5, v0}, Lbnl;->a(Lbea;)V

    .line 1359
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lyt;->ae(Ljava/lang/String;)V

    .line 1363
    :cond_19
    if-eqz p6, :cond_1a

    .line 1364
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    iput-object v0, p6, Lys;->a:Ljava/lang/String;

    .line 1366
    :cond_1a
    iget-object v0, p1, Lbjc;->c:Ljava/lang/String;

    invoke-static {p0, v0}, Lyt;->a(Lyt;Ljava/lang/String;)I

    .line 1367
    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_1b
    move-object v0, v1

    goto/16 :goto_3
.end method

.method private static a(Lyt;Lbjh;Lbnl;J)Z
    .locals 35

    .prologue
    .line 636
    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_0

    .line 637
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processHangoutEventInTransaction conversationId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lbjh;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mediaType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Lbjh;->f:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " event type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Lbjh;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " duration "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-wide v4, v0, Lbjh;->v:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " transfer "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lbjh;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " participants: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lbjh;->u:Ljava/util/List;

    .line 643
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 637
    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    :cond_0
    move-object/from16 v0, p1

    iget v2, v0, Lbjh;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    const/4 v15, 0x1

    .line 648
    :goto_0
    const/4 v2, 0x1

    if-ne v15, v2, :cond_2

    const/4 v2, 0x1

    move/from16 v19, v2

    .line 649
    :goto_1
    move-object/from16 v0, p1

    iget v12, v0, Lbjh;->g:I

    .line 650
    const/4 v2, 0x0

    .line 651
    const/4 v7, -0x1

    .line 652
    move-object/from16 v0, p1

    iget v3, v0, Lbjh;->b:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 653
    const/4 v2, 0x1

    .line 654
    const/16 v7, 0x8

    .line 659
    const/16 v12, 0xa

    move/from16 v34, v2

    .line 669
    :goto_2
    const-wide/16 v21, -0x1

    .line 670
    if-eqz v34, :cond_a

    .line 671
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v4

    .line 675
    const/4 v14, 0x0

    .line 686
    iget v2, v4, Lyv;->b:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 687
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->c()Lbdk;

    move-result-object v5

    .line 689
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjh;->d:Lbdk;

    invoke-virtual {v5, v2}, Lbdk;->a(Lbdk;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 690
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjh;->u:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 691
    if-lez v2, :cond_4

    .line 692
    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->u:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbdk;

    .line 693
    invoke-virtual {v5, v2}, Lbdk;->a(Lbdk;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 694
    add-int/lit8 v2, v3, -0x1

    :goto_4
    move v3, v2

    .line 696
    goto :goto_3

    .line 645
    :cond_1
    const/4 v15, 0x2

    goto :goto_0

    .line 648
    :cond_2
    const/4 v2, 0x0

    move/from16 v19, v2

    goto :goto_1

    .line 660
    :cond_3
    move-object/from16 v0, p1

    iget v3, v0, Lbjh;->b:I

    if-nez v3, :cond_16

    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->w:Ljava/lang/String;

    if-eqz v3, :cond_16

    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->w:Ljava/lang/String;

    .line 662
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lyt;->s(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_16

    .line 666
    const/16 v12, 0xa

    move/from16 v34, v2

    goto :goto_2

    :cond_4
    move v3, v2

    .line 699
    :cond_5
    if-nez v3, :cond_8

    .line 700
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 702
    iget-object v2, v4, Lyv;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbdh;

    .line 703
    iget-object v2, v2, Lbdh;->b:Lbdk;

    .line 704
    invoke-virtual {v5, v2}, Lbdk;->a(Lbdk;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 705
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 710
    :cond_7
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_8

    .line 711
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lyt;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v14

    .line 717
    :cond_8
    if-nez v14, :cond_9

    .line 719
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->u:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lyt;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v14

    .line 723
    :cond_9
    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lbjh;->m:Ljava/lang/String;

    const/4 v5, 0x4

    move-object/from16 v0, p1

    iget-object v6, v0, Lbjh;->d:Lbdk;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lbjh;->e:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lbjh;->n:J

    const/4 v13, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v15}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;I)J

    move-result-wide v21

    .line 730
    const/16 v2, 0xa

    if-ne v12, v2, :cond_c

    .line 732
    invoke-virtual/range {p2 .. p2}, Lbnl;->d()V

    .line 738
    :cond_a
    :goto_6
    const/16 v29, 0x0

    .line 739
    move-object/from16 v0, p1

    iget v2, v0, Lbjh;->b:I

    packed-switch v2, :pswitch_data_0

    .line 786
    :goto_7
    move-object/from16 v0, p1

    iget-wide v2, v0, Lbjh;->e:J

    move-object/from16 v0, p1

    iget-object v4, v0, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v2, v3, v4}, Lyt;->a(IJLjava/lang/String;)V

    .line 789
    move-object/from16 v0, p1

    iget-wide v2, v0, Lbjh;->i:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    const-string v2, "babel_transport_events"

    const/4 v3, 0x0

    .line 790
    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 793
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjh;->c:Ljava/lang/String;

    .line 794
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lyt;->m(Ljava/lang/String;)Lzc;

    move-result-object v2

    .line 795
    iget-wide v2, v2, Lzc;->a:J

    move-object/from16 v0, p1

    iget-wide v4, v0, Lbjh;->e:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_11

    const/4 v2, 0x1

    .line 797
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lbjh;->c:Ljava/lang/String;

    .line 796
    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;)Z

    move-result v4

    .line 798
    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->c:Ljava/lang/String;

    .line 799
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lyt;->t(Ljava/lang/String;)J

    move-result-wide v5

    .line 800
    move-object/from16 v0, p1

    iget-wide v7, v0, Lbjh;->e:J

    cmp-long v3, v7, v5

    if-lez v3, :cond_12

    const/4 v3, 0x1

    .line 803
    :goto_9
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v5

    .line 804
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Lyj;)J

    move-result-wide v5

    .line 805
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long v30, v7, v9

    .line 806
    move-object/from16 v0, p1

    iget-object v7, v0, Lbjh;->l:Ljava/lang/String;

    .line 807
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_13

    const-wide/16 v25, 0x0

    .line 813
    :goto_a
    new-instance v32, Lyu;

    invoke-direct/range {v32 .. v32}, Lyu;-><init>()V

    .line 817
    invoke-virtual/range {p2 .. p2}, Lbnl;->e()I

    move-result v7

    move-object/from16 v0, v32

    iput v7, v0, Lyu;->a:I

    .line 818
    move-object/from16 v0, v32

    iput-boolean v2, v0, Lyu;->b:Z

    .line 819
    move-object/from16 v0, v32

    iput-boolean v3, v0, Lyu;->c:Z

    .line 820
    cmp-long v2, v30, v5

    if-gtz v2, :cond_14

    const/4 v2, 0x1

    :goto_b
    move-object/from16 v0, v32

    iput-boolean v2, v0, Lyu;->d:Z

    .line 821
    move-object/from16 v0, v32

    iput-boolean v4, v0, Lyu;->e:Z

    .line 824
    move-object/from16 v0, p1

    iget v2, v0, Lbjh;->h:I

    move-object/from16 v0, v32

    iput v2, v0, Lyu;->f:I

    .line 825
    move-object/from16 v0, v32

    iput v12, v0, Lyu;->g:I

    .line 827
    move-object/from16 v0, p1

    iget-wide v0, v0, Lbjh;->i:J

    move-wide/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbjh;->m:Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x4

    const/16 v33, 0x0

    move-object/from16 v20, p0

    invoke-virtual/range {v20 .. v33}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    .line 832
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_b

    .line 833
    move-object/from16 v0, p1

    iget-wide v0, v0, Lbjh;->i:J

    move-wide/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbjh;->m:Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-object/from16 v20, p0

    move-wide/from16 v30, p3

    invoke-virtual/range {v20 .. v33}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    .line 840
    :cond_b
    return v34

    .line 734
    :cond_c
    move-object/from16 v0, p1

    iget-wide v2, v0, Lbjh;->e:J

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Lbnl;->a(J)V

    goto/16 :goto_6

    .line 744
    :pswitch_0
    const/16 v29, 0x2be

    .line 745
    goto/16 :goto_7

    .line 747
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->c()Lbdk;

    move-result-object v2

    .line 748
    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->u:Ljava/util/List;

    .line 749
    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 750
    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->c:Ljava/lang/String;

    .line 751
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lyt;->l(Ljava/lang/String;)J

    move-result-wide v3

    .line 752
    move-object/from16 v0, p1

    iget-wide v5, v0, Lbjh;->e:J

    cmp-long v3, v5, v3

    if-lez v3, :cond_d

    .line 753
    if-eqz v2, :cond_f

    .line 754
    move-object/from16 v0, p1

    iget-object v14, v0, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v15, v0, Lbjh;->e:J

    move-object/from16 v0, p1

    iget-wide v0, v0, Lbjh;->n:J

    move-wide/from16 v17, v0

    if-eqz v19, :cond_e

    const/16 v19, 0xb

    :goto_c
    const/16 v20, 0x0

    const/16 v23, 0x4

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v13, p0

    invoke-virtual/range {v13 .. v25}, Lyt;->a(Ljava/lang/String;JJILbdk;JILjava/lang/String;Ljava/lang/String;)Z

    .line 769
    :cond_d
    :goto_d
    const/4 v15, 0x0

    .line 770
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjh;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 772
    const/16 v29, 0x2c1

    .line 773
    goto/16 :goto_7

    .line 754
    :cond_e
    const/16 v19, 0x8

    goto :goto_c

    .line 761
    :cond_f
    move-object/from16 v0, p1

    iget-object v14, v0, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v15, v0, Lbjh;->e:J

    move-object/from16 v0, p1

    iget-wide v0, v0, Lbjh;->n:J

    move-wide/from16 v17, v0

    if-eqz v19, :cond_10

    const/16 v19, 0xa

    :goto_e
    const/16 v20, 0x0

    const/16 v23, 0x4

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v13, p0

    invoke-virtual/range {v13 .. v25}, Lyt;->a(Ljava/lang/String;JJILbdk;JILjava/lang/String;Ljava/lang/String;)Z

    goto :goto_d

    :cond_10
    const/16 v19, 0x1

    goto :goto_e

    .line 775
    :pswitch_2
    const/16 v29, 0x2bf

    .line 776
    goto/16 :goto_7

    .line 778
    :pswitch_3
    const/16 v29, 0x2c0

    .line 779
    goto/16 :goto_7

    .line 781
    :pswitch_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjh;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lbjh;->u:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 783
    const/16 v29, 0x2c2

    goto/16 :goto_7

    .line 795
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_8

    .line 800
    :cond_12
    const/4 v3, 0x0

    goto/16 :goto_9

    .line 807
    :cond_13
    move-object/from16 v0, p1

    iget-object v7, v0, Lbjh;->l:Ljava/lang/String;

    .line 809
    invoke-static {v7}, Lyt;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    .line 808
    invoke-static {v7}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v25

    goto/16 :goto_a

    .line 820
    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_15
    move v2, v3

    goto/16 :goto_4

    :cond_16
    move/from16 v34, v2

    goto/16 :goto_2

    .line 739
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Lyt;Ljava/lang/String;JLbnl;Lbjg;)Z
    .locals 13

    .prologue
    .line 900
    invoke-virtual {p0, p1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v3

    .line 901
    if-eqz v3, :cond_2

    .line 902
    sget-boolean v1, Lyp;->a:Z

    if-eqz v1, :cond_0

    .line 903
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "conversation "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " exists locally,  metadataPresent "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, v3, Lyv;->e:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    iget-boolean v1, v3, Lyv;->e:Z

    if-nez v1, :cond_5

    .line 927
    :cond_1
    new-instance v1, Lbek;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    move-object v2, p1

    move-object/from16 v12, p5

    invoke-direct/range {v1 .. v12}, Lbek;-><init>(Ljava/lang/String;ZZZ[BJLjava/lang/String;JLbjg;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Lbnl;->a(Lbea;)V

    .line 936
    const/4 v1, 0x0

    .line 938
    :goto_1
    return v1

    .line 908
    :cond_2
    const-string v1, "Babel_db"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 909
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "conversation "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " does not exist locally"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    :cond_3
    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-nez v1, :cond_4

    .line 918
    const-wide/16 v1, 0x0

    .line 922
    :goto_2
    invoke-virtual {p0, p1, v1, v2}, Lyt;->a(Ljava/lang/String;J)V

    goto :goto_0

    .line 920
    :cond_4
    const-wide/16 v1, 0x1

    sub-long v1, p2, v1

    goto :goto_2

    .line 938
    :cond_5
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static b(Lyt;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 3002
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 3003
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v7

    .line 3005
    invoke-virtual {p0, p1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v0

    .line 3013
    if-eqz v0, :cond_0

    iget v1, v0, Lyv;->b:I

    if-eq v1, v3, :cond_0

    iget v1, v0, Lyv;->b:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_9

    iget v0, v0, Lyv;->c:I

    .line 3016
    invoke-static {v0}, Lf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_0
    move v1, v3

    .line 3025
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v4, v2

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 3027
    iget-object v5, v0, Lbdh;->d:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 3028
    iget-object v0, v0, Lbdh;->e:Ljava/lang/String;

    .line 3053
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 3054
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 3055
    const-string v5, ", "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3057
    :cond_2
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 3029
    :cond_3
    iget-object v5, v0, Lbdh;->b:Lbdk;

    if-eqz v5, :cond_1

    .line 3031
    if-nez v4, :cond_4

    iget-object v5, v0, Lbdh;->b:Lbdk;

    .line 3032
    invoke-virtual {v5, v7}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v3, :cond_4

    move v4, v3

    .line 3034
    goto :goto_1

    .line 3039
    :cond_4
    sget-object v5, Lyp;->i:Ljava/lang/CharSequence;

    if-nez v5, :cond_5

    .line 3040
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v5

    .line 3041
    sput-object v5, Lyp;->i:Ljava/lang/CharSequence;

    if-nez v5, :cond_5

    .line 3042
    const-string v5, ""

    sput-object v5, Lyp;->i:Ljava/lang/CharSequence;

    .line 3045
    :cond_5
    if-nez v4, :cond_6

    invoke-virtual {v0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v5

    sget-object v9, Lyp;->i:Ljava/lang/CharSequence;

    invoke-static {v5, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    move v4, v3

    .line 3047
    goto :goto_1

    .line 3049
    :cond_6
    if-nez v1, :cond_7

    move v5, v3

    :goto_3
    invoke-virtual {v0, v5}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    move v5, v2

    goto :goto_3

    .line 3060
    :cond_8
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_9
    move v1, v2

    goto :goto_0
.end method

.method public static b(Lyt;ZLjava/util/List;Ljava/util/List;Ljava/util/List;ZIIZLbnl;)Ljava/lang/String;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbcn;",
            ">;ZIIZ",
            "Lbnl;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1547
    const-string v3, "Babel_db"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1548
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "createConversationLocally:  num invitees "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1550
    :cond_0
    const/4 v8, 0x0

    .line 1551
    const/4 v12, 0x0

    .line 1552
    const-wide/16 v10, 0x0

    .line 1553
    const/4 v9, 0x0

    .line 1559
    const/4 v7, 0x0

    .line 1561
    const/4 v6, 0x0

    .line 1562
    const/4 v5, 0x0

    .line 1564
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v14

    .line 1566
    invoke-virtual {v14}, Lyj;->c()Lbdk;

    move-result-object v15

    .line 1567
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v13, v3

    :goto_0
    if-ltz v13, :cond_1

    .line 1568
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbdh;

    .line 1569
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbdh;

    iget-object v4, v4, Lbdh;->b:Lbdk;

    .line 1570
    iget-object v0, v4, Lbdk;->b:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v0, v4, Lbdk;->a:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v15 .. v17}, Lbdk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_1f

    .line 1572
    invoke-virtual {v3}, Lbdh;->c()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    or-int v7, v7, v16

    .line 1575
    iget-object v4, v4, Lbdk;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 1576
    invoke-virtual {v3}, Lbdh;->e()Z

    move-result v4

    or-int/2addr v4, v6

    .line 1577
    invoke-virtual {v3}, Lbdh;->f()Z

    move-result v3

    or-int/2addr v3, v5

    move v5, v7

    .line 1567
    :goto_1
    add-int/lit8 v6, v13, -0x1

    move v13, v6

    move v7, v5

    move v6, v4

    move v5, v3

    goto :goto_0

    .line 1582
    :cond_1
    if-nez v7, :cond_6

    if-eqz p3, :cond_2

    .line 1583
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_6

    :cond_2
    const/4 v3, 0x1

    move v13, v3

    .line 1585
    :goto_2
    if-nez p7, :cond_1e

    .line 1586
    if-eqz v13, :cond_8

    .line 1589
    invoke-virtual {v14}, Lyj;->x()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    move/from16 v0, p6

    if-ne v0, v3, :cond_3

    if-nez v6, :cond_3

    if-eqz v5, :cond_7

    .line 1593
    :cond_3
    const/4 v5, 0x3

    .line 1610
    :goto_3
    const/4 v3, 0x2

    move/from16 v0, p6

    if-ne v0, v3, :cond_9

    if-eqz v13, :cond_9

    .line 1611
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lyt;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    .line 1614
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1d

    .line 1615
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v6

    .line 1616
    iget v3, v6, Lyv;->r:I

    .line 1617
    iget-wide v6, v6, Lyv;->q:J

    move/from16 v18, v3

    move-object v3, v4

    move/from16 v4, v18

    .line 1639
    :goto_4
    if-nez v3, :cond_18

    .line 1641
    if-eqz v13, :cond_d

    .line 1642
    invoke-static {}, Lyt;->i()Ljava/lang/String;

    move-result-object v8

    .line 1644
    :goto_5
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Creating new conversation no match found. generated id: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    invoke-static {v5}, Lf;->b(I)Z

    move-result v3

    if-nez v3, :cond_e

    const/4 v3, 0x1

    move v10, v3

    .line 1653
    :goto_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v3

    if-eqz p1, :cond_f

    const/4 v9, 0x2

    :goto_7
    move-object/from16 v3, p0

    move/from16 v4, p6

    .line 1650
    invoke-virtual/range {v3 .. v9}, Lyt;->a(IIJLjava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 1657
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lyt;->F(Ljava/lang/String;)V

    .line 1664
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lyp;->a(Lyt;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 1668
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbdh;

    .line 1669
    invoke-virtual {v3}, Lbdh;->d()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1671
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lyt;->b(Lyt;Ljava/lang/String;)I

    .line 1682
    :cond_5
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lyt;->a(Lyt;Ljava/lang/String;)I

    .line 1684
    if-eqz v10, :cond_14

    if-eqz p9, :cond_14

    .line 1688
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1689
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbdh;

    .line 1691
    if-eqz p4, :cond_10

    .line 1694
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbcn;

    .line 1695
    invoke-virtual {v3, v4}, Lbdh;->a(Lbcn;)V

    goto :goto_9

    .line 1583
    :cond_6
    const/4 v3, 0x0

    move v13, v3

    goto/16 :goto_2

    .line 1602
    :cond_7
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Lyj;->a(I)I

    move-result v5

    goto/16 :goto_3

    .line 1606
    :cond_8
    const/4 v5, 0x1

    goto/16 :goto_3

    .line 1619
    :cond_9
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1c

    if-eqz p3, :cond_a

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1c

    :cond_a
    const/4 v3, 0x1

    move/from16 v0, p6

    if-ne v0, v3, :cond_1c

    .line 1623
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbdh;

    .line 1624
    iget-object v3, v3, Lbdh;->b:Lbdk;

    .line 1625
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Lbdk;->a()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1626
    :cond_b
    const-string v3, "Babel_db"

    const-string v4, "Server returned a conversation with empty participantId"

    invoke-static {v3, v4}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v9

    move-wide v6, v10

    move-object v3, v12

    goto/16 :goto_4

    .line 1628
    :cond_c
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Lyt;->a(Lbdk;ZI)Lzi;

    move-result-object v3

    .line 1631
    if-eqz v3, :cond_1c

    .line 1632
    iget-object v4, v3, Lzi;->a:Ljava/lang/String;

    .line 1633
    iget-wide v6, v3, Lzi;->c:J

    .line 1634
    iget v3, v3, Lzi;->d:I

    move/from16 v18, v3

    move-object v3, v4

    move/from16 v4, v18

    goto/16 :goto_4

    .line 1643
    :cond_d
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_5

    .line 1648
    :cond_e
    const/4 v3, 0x0

    move v10, v3

    goto/16 :goto_6

    .line 1653
    :cond_f
    const/4 v9, 0x1

    goto/16 :goto_7

    .line 1698
    :cond_10
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 1701
    :cond_11
    if-eqz p3, :cond_12

    .line 1702
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lxo;

    .line 1703
    invoke-virtual {v3}, Lxo;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lxo;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 1715
    :cond_12
    new-instance v3, Lbee;

    const/4 v4, 0x0

    move/from16 v0, p6

    invoke-direct {v3, v8, v0, v4, v6}, Lbee;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 1717
    if-nez p5, :cond_16

    .line 1718
    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_13

    .line 1719
    const-string v4, "Babel_db"

    const-string v6, "CreateConversationLocally -- calling GetConversationRequest"

    invoke-static {v4, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1722
    :cond_13
    new-instance v4, Lbek;

    invoke-direct {v4, v3}, Lbek;-><init>(Lbee;)V

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, Lbnl;->a(Lbea;)V

    .line 1731
    :cond_14
    :goto_b
    const/4 v3, 0x1

    move v4, v3

    move-object v3, v5

    .line 1753
    :goto_c
    if-eqz v4, :cond_15

    .line 1754
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 1757
    :cond_15
    return-object v3

    .line 1724
    :cond_16
    sget-boolean v4, Lyp;->a:Z

    if-eqz v4, :cond_17

    .line 1725
    const-string v4, "Babel_db"

    const-string v6, "CreateConversationLocally -- calling CreateConversationRequest"

    invoke-static {v4, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1728
    :cond_17
    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Lbnl;->a(Lbea;)V

    goto :goto_b

    .line 1733
    :cond_18
    const-string v5, "Babel_db"

    const/4 v9, 0x3

    invoke-static {v5, v9}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 1734
    const-string v5, "Babel_db"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Using existing conversation "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    :cond_19
    sget-wide v9, Lyp;->c:J

    sget-wide v11, Lyp;->h:J

    or-long/2addr v9, v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9, v10}, Lyt;->m(Ljava/lang/String;J)V

    .line 1740
    if-nez p1, :cond_1a

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1a

    .line 1743
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v4

    const/4 v5, 0x0

    .line 1742
    invoke-static {v4, v3, v6, v7, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;JZ)I

    .line 1748
    :cond_1a
    if-eqz v13, :cond_1b

    if-eqz p8, :cond_1b

    .line 1749
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lyp;->a(Lyt;Ljava/util/List;)V

    :cond_1b
    move v4, v8

    goto :goto_c

    :cond_1c
    move v4, v9

    move-wide v6, v10

    move-object v3, v12

    goto/16 :goto_4

    :cond_1d
    move-wide v6, v10

    move-object v3, v4

    move v4, v9

    goto/16 :goto_4

    :cond_1e
    move/from16 v5, p7

    goto/16 :goto_3

    :cond_1f
    move v3, v5

    move v4, v6

    move v5, v7

    goto/16 :goto_1
.end method

.method public static b()V
    .locals 6

    .prologue
    .line 3114
    sget-object v1, Lyp;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 3115
    :try_start_0
    sget v0, Lyp;->k:I

    if-gtz v0, :cond_0

    .line 3116
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sPendingChangeNotificationsCount is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lyp;->k:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3133
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 3119
    :cond_0
    :try_start_1
    sget v0, Lyp;->k:I

    add-int/lit8 v0, v0, -0x1

    .line 3121
    sput v0, Lyp;->k:I

    if-nez v0, :cond_3

    .line 3122
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 3123
    sget-object v0, Lyp;->l:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 3124
    invoke-static {v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3125
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v4

    const-string v5, "account"

    .line 3126
    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3125
    invoke-static {v4, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 3128
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 3131
    :cond_2
    sget-object v0, Lyp;->l:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 3133
    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static b(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 3817
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 3831
    :cond_0
    :goto_0
    return-void

    .line 3820
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 3821
    const/4 v0, -0x1

    .line 3822
    const-string v2, "sms"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3823
    const/4 v0, 0x0

    .line 3827
    :cond_2
    :goto_1
    if-ltz v0, :cond_0

    .line 3829
    invoke-static {p0}, Lbvx;->c(Landroid/net/Uri;)J

    move-result-wide v1

    .line 3828
    invoke-static {v0, v1, v2}, Lbwf;->b(IJ)V

    goto :goto_0

    .line 3824
    :cond_3
    const-string v2, "mms"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3825
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static b(Lyj;)V
    .locals 2

    .prologue
    .line 3262
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_0

    .line 3263
    const-string v0, "Babel_db"

    const-string v1, "NOTIFY CONVERSATION"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3268
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->e:Landroid/net/Uri;

    .line 3269
    invoke-static {v0, p0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    .line 3268
    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3272
    invoke-static {p0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Lyj;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3273
    return-void
.end method

.method public static b(Lyj;Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 4114
    invoke-static {p1}, Lbvx;->a(Landroid/content/Intent;)Landroid/telephony/SmsMessage;

    move-result-object v0

    .line 4115
    if-nez v0, :cond_1

    .line 4116
    const-string v0, "Babel_db"

    const-string v1, "EsConversationsData.processSmsDeliveryReport: empty report message"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 4144
    :cond_0
    :goto_0
    return-void

    .line 4119
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v1

    .line 4120
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-gez v3, :cond_2

    .line 4121
    const-string v0, "Babel_db"

    const-string v1, "EsConversationsData.processSmsDeliveryReport: can\'t find message"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4124
    :cond_2
    :try_start_0
    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getStatus()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 4133
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 4135
    new-instance v0, Lyt;

    invoke-direct {v0, p0}, Lyt;-><init>(Lyj;)V

    .line 4136
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v6

    invoke-virtual/range {v0 .. v5}, Lyt;->b(JIJ)V

    .line 4137
    invoke-static {v0, v1, v2}, Lyp;->b(Lyt;J)V

    .line 4140
    invoke-virtual {v0, v1, v2}, Lyt;->g(J)Ljava/lang/String;

    move-result-object v0

    .line 4139
    invoke-static {v0}, Lf;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4141
    if-eqz v0, :cond_0

    .line 4142
    invoke-static {v0, v3, v6, v7}, Lbvx;->a(Ljava/lang/String;IJ)V

    goto :goto_0

    .line 4130
    :catch_0
    move-exception v0

    const-string v0, "Babel_db"

    const-string v1, "processSmsDeliveryReport: NPE inside SmsMessage"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Lyj;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 4258
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4259
    const-string v0, "Babel_db"

    const-string v1, "receiveSmsFromDumpFile: empty dump file name"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 4271
    :goto_0
    return-void

    .line 4262
    :cond_0
    const-string v2, "smsdump-"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4264
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    if-eqz v0, :cond_1

    array-length v2, v0

    if-gtz v2, :cond_2

    :cond_1
    const-string v2, "Babel_db"

    const-string v3, "receiveSmsFromDumpFile: empty data"

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    array-length v2, v0

    new-array v3, v2, [Landroid/telephony/SmsMessage;

    move v2, v1

    :goto_1
    array-length v1, v0

    if-ge v2, v1, :cond_3

    aget-object v1, v0, v2

    check-cast v1, [B

    invoke-static {v1}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v1

    aput-object v1, v3, v2

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v0

    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "receiveSmsFromDumpFile: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_3
    invoke-static {v3, p0, v0, v1}, Lyp;->a([Landroid/telephony/SmsMessage;Lyj;ILjava/lang/Boolean;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "receiveSmsFromDumpFile: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 4265
    :cond_4
    const-string v1, "mmsdump-"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 4267
    :try_start_4
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lf;->a(Ljava/io/File;)Ljava/nio/ByteBuffer;

    move-result-object v2

    if-nez v2, :cond_7

    const/4 v1, 0x0

    new-array v1, v1, [B

    :goto_2
    if-eqz v1, :cond_5

    new-instance v0, Lrt;

    invoke-direct {v0, v1}, Lrt;-><init>([B)V

    invoke-virtual {v0}, Lrt;->a()Lri;

    move-result-object v0

    :cond_5
    if-eqz v0, :cond_6

    instance-of v1, v0, Lsa;

    if-nez v1, :cond_9

    :cond_6
    const-string v0, "Babel_db"

    const-string v1, "receiveMmsFromDumpFile: failed to parse PDU"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "receiveMmsFromDumpFile: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_7
    :try_start_5
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    goto :goto_2

    :cond_8
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_2

    :cond_9
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    check-cast v0, Lsa;

    invoke-static {v1, v0}, Lbvx;->a(Landroid/content/Context;Lsa;)Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_a

    const-string v0, "Babel_db"

    const-string v1, "receiveMmsFromDumpFile: failed to persist PDU"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lyp;->a(Lyj;Landroid/net/Uri;JLjava/lang/Boolean;ZZ)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 4269
    :cond_b
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "receiveSmsFromDumpFile: invalid dump file name "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static b(Lyj;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 3319
    new-instance v0, Lyt;

    invoke-direct {v0, p0}, Lyt;-><init>(Lyj;)V

    .line 3320
    invoke-virtual {v0, p1, p2}, Lyt;->e(Ljava/lang/String;I)V

    .line 3321
    return-void
.end method

.method public static b(Lyt;)V
    .locals 2

    .prologue
    .line 3067
    const-string v0, "Babel_db"

    const-string v1, "NOTIFY ALL PARTICIPANTS"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3068
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lyc;->a(Lyj;Ljava/lang/String;)V

    .line 3069
    return-void
.end method

.method public static b(Lyt;J)V
    .locals 1

    .prologue
    .line 4104
    invoke-virtual {p0, p1, p2}, Lyt;->b(J)Lzg;

    move-result-object v0

    .line 4105
    if-eqz v0, :cond_0

    .line 4106
    iget-object v0, v0, Lzg;->b:Ljava/lang/String;

    invoke-static {p0, v0}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 4108
    :cond_0
    return-void
.end method

.method public static b(Lyt;Lbxz;)V
    .locals 3

    .prologue
    .line 2407
    const/4 v1, 0x0

    .line 2408
    invoke-virtual {p0}, Lyt;->a()V

    .line 2410
    if-nez p1, :cond_1

    .line 2411
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lyt;->v(Ljava/lang/String;)I

    move-result v0

    .line 2417
    :goto_0
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2419
    invoke-virtual {p0}, Lyt;->c()V

    .line 2421
    if-lez v0, :cond_0

    .line 2423
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    .line 2422
    invoke-static {v0, p1}, Lbne;->a(Lyj;Lbxz;)V

    .line 2425
    :cond_0
    return-void

    .line 2413
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lbxz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2414
    invoke-virtual {p0, v0}, Lyt;->v(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    add-int/2addr v1, v0

    .line 2415
    goto :goto_1

    .line 2419
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static b(Lyt;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2959
    invoke-virtual {p0, p1}, Lyt;->K(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 2961
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2962
    const/4 v2, 0x0

    .line 2963
    const/4 v0, 0x0

    .line 2965
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v6

    .line 2967
    :try_start_0
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 2968
    invoke-virtual {p0, v6, v0}, Lyt;->a(Lbsc;Lbdh;)V

    .line 2969
    iget-object v1, v0, Lbdh;->h:Ljava/lang/String;

    .line 2970
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 2971
    iget-object v0, v0, Lbdh;->b:Lbdk;

    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v8

    invoke-virtual {v8}, Lyj;->c()Lbdk;

    move-result-object v8

    invoke-virtual {v0, v8}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2972
    const/4 v0, 0x5

    if-ge v3, v0, :cond_1

    .line 2973
    if-lez v3, :cond_0

    .line 2974
    const/16 v0, 0x7c

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2976
    :cond_0
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2978
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    :goto_1
    move-object v2, v0

    .line 2983
    goto :goto_0

    .line 2985
    :cond_3
    invoke-virtual {v6}, Lbsc;->b()V

    .line 2988
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2989
    invoke-static {p0, p1, v4}, Lyp;->b(Lyt;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 2991
    const-string v3, "Babel_db"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2992
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "generated name "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2993
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "packed participant urls "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2996
    :cond_4
    invoke-virtual {p0, p1, v1, v0, v2}, Lyt;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2998
    return-void

    .line 2985
    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lbsc;->b()V

    throw v0

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method public static b(Lyt;Ljava/lang/String;J)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const-wide/16 v5, 0x0

    .line 2566
    invoke-virtual {p0, p1}, Lyt;->e(Ljava/lang/String;)Lyv;

    .line 2573
    sget-wide v0, Lyp;->h:J

    invoke-virtual {p0, p1, v0, v1}, Lyt;->m(Ljava/lang/String;J)V

    .line 2576
    sget-wide v0, Lyp;->f:J

    and-long/2addr v0, p2

    cmp-long v0, v0, v5

    if-eqz v0, :cond_0

    .line 2577
    invoke-static {p0, p1, v7}, Lyp;->a(Lyt;Ljava/lang/String;Z)V

    .line 2579
    :cond_0
    sget-wide v0, Lyp;->g:J

    and-long/2addr v0, p2

    cmp-long v0, v0, v5

    if-eqz v0, :cond_1

    .line 2580
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lyp;->a(Lyt;Ljava/lang/String;Z)V

    .line 2584
    :cond_1
    sget-wide v0, Lyp;->e:J

    and-long/2addr v0, p2

    cmp-long v0, v0, v5

    if-eqz v0, :cond_3

    .line 2585
    invoke-virtual {p0, p1}, Lyt;->l(Ljava/lang/String;)J

    move-result-wide v0

    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_2

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processPendingArchive "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-static {v2, p1, v0, v1, v7}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;JZ)I

    .line 2589
    :cond_3
    sget-wide v0, Lyp;->c:J

    and-long/2addr v0, p2

    cmp-long v0, v0, v5

    if-eqz v0, :cond_5

    .line 2590
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_4

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processPendingLeave "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g(Lyj;Ljava/lang/String;)I

    .line 2594
    :cond_5
    sget-wide v0, Lyp;->d:J

    and-long/2addr v0, p2

    cmp-long v0, v0, v5

    if-eqz v0, :cond_7

    .line 2595
    invoke-virtual {p0, p1}, Lyt;->l(Ljava/lang/String;)J

    move-result-wide v0

    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_6

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processPendingDelete "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-static {v2, p1, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;J)I

    .line 2597
    :cond_7
    return-void
.end method

.method public static b(Lyt;Ljava/lang/String;Lbnl;)V
    .locals 12

    .prologue
    .line 1989
    invoke-virtual {p0}, Lyt;->a()V

    .line 1991
    :try_start_0
    invoke-virtual {p0, p1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v1

    .line 1994
    if-nez v1, :cond_0

    .line 1995
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested older events for an unknown conversation Account:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1996
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conversationId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1995
    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000
    new-instance v0, Lbek;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lbek;-><init>(Ljava/lang/String;ZZZ[BJLjava/lang/String;JLbjg;)V

    .line 2028
    :goto_0
    invoke-virtual {p2, v0}, Lbnl;->a(Lbea;)V

    .line 2030
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2032
    invoke-virtual {p0}, Lyt;->c()V

    .line 2033
    :goto_1
    return-void

    .line 2003
    :cond_0
    :try_start_1
    iget-boolean v0, v1, Lyv;->p:Z

    if-eqz v0, :cond_2

    .line 2004
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2005
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Skip requesting old events for conversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " since it already has oldest event"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2008
    :cond_1
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2032
    invoke-virtual {p0}, Lyt;->c()V

    goto :goto_1

    .line 2012
    :cond_2
    :try_start_2
    const-string v0, "Babel_db"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2013
    const-string v0, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requesting events before token "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v1, Lyv;->f:[B

    .line 2014
    invoke-static {v3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " eventId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v1, Lyv;->g:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " conversation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2013
    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017
    :cond_3
    new-instance v0, Lbek;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, v1, Lyv;->f:[B

    iget-wide v6, v1, Lyv;->g:J

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lbek;-><init>(Ljava/lang/String;ZZZ[BJLjava/lang/String;JLbjg;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 2032
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public static c()V
    .locals 5

    .prologue
    .line 4156
    invoke-static {}, Lbvx;->d()I

    move-result v0

    .line 4157
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Message purging: deleted "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " media messages in telephony"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4160
    if-lez v0, :cond_0

    .line 4161
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 4162
    new-instance v1, Lyt;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 4163
    invoke-virtual {v1}, Lyt;->u()I

    move-result v0

    .line 4164
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Message purging: deleted "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " media messages in Hangouts"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4166
    invoke-static {v1}, Lyp;->c(Lyt;)V

    .line 4168
    :cond_0
    return-void
.end method

.method public static c(Lyt;)V
    .locals 2

    .prologue
    .line 3090
    const-string v0, "Babel_db"

    const-string v1, "NOTIFY ALL MESSAGES"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3091
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 3092
    return-void
.end method

.method public static c(Lyt;Lbxz;)V
    .locals 2

    .prologue
    .line 2432
    invoke-virtual {p0}, Lyt;->a()V

    .line 2434
    if-nez p1, :cond_1

    .line 2435
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lyt;->U(Ljava/lang/String;)V

    .line 2441
    :cond_0
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2443
    invoke-virtual {p0}, Lyt;->c()V

    .line 2445
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbne;->d(Lyj;)V

    .line 2446
    return-void

    .line 2437
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lbxz;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2438
    invoke-virtual {p0, v0}, Lyt;->U(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2443
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public static c(Lyt;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3078
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NOTIFY PARTICIPANTS, conversation_id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3079
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->d:Landroid/net/Uri;

    .line 3080
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    .line 3079
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3082
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0, p1}, Lyc;->a(Lyj;Ljava/lang/String;)V

    .line 3084
    return-void
.end method

.method public static c(Lyt;Ljava/lang/String;Lbnl;)V
    .locals 3

    .prologue
    .line 2345
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2346
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "leaveConversationLocally: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2348
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 2350
    :try_start_0
    invoke-virtual {p0, p1}, Lyt;->q(Ljava/lang/String;)V

    .line 2352
    invoke-static {p1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2353
    sget-wide v0, Lyp;->c:J

    invoke-virtual {p0, p1, v0, v1}, Lyt;->n(Ljava/lang/String;J)V

    .line 2364
    :goto_0
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2366
    invoke-virtual {p0}, Lyt;->c()V

    .line 2368
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 2369
    return-void

    .line 2356
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lyt;->D(Ljava/lang/String;)V

    .line 2358
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v0

    .line 2360
    invoke-static {v0, p1}, Lbew;->a(Ljava/lang/String;Ljava/lang/String;)Lbew;

    move-result-object v0

    .line 2359
    invoke-virtual {p2, v0}, Lbnl;->a(Lbea;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2366
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public static d(Lyt;)V
    .locals 1

    .prologue
    .line 3221
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 3222
    return-void
.end method

.method public static d(Lyt;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 3154
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3155
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NOTIFY MESSAGES, conversation_id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3158
    :cond_0
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v1

    .line 3159
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->d:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3163
    const/4 v0, 0x0

    .line 3164
    if-eqz p1, :cond_1

    .line 3165
    invoke-virtual {p0, p1}, Lyt;->ag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 3167
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 3168
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3170
    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lyp;->a(Landroid/net/Uri;)V

    .line 3171
    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 3173
    :cond_2
    if-eqz p1, :cond_3

    .line 3174
    invoke-static {v1, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3176
    invoke-static {v1, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3175
    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3182
    :cond_3
    invoke-static {v1, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->d(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3181
    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3185
    invoke-static {v1}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Lyj;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3186
    return-void
.end method

.method public static e(Lyt;)V
    .locals 1

    .prologue
    .line 3254
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lyp;->b(Lyj;)V

    .line 3255
    return-void
.end method

.method public static e(Lyt;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3196
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3197
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NOTIFY EVENT SUGGESTIONS, conversation_id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3201
    :cond_0
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    .line 3200
    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->e(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Landroid/net/Uri;)V

    .line 3202
    return-void
.end method

.method public static f(Lyt;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4228
    invoke-virtual {p0, p1}, Lyt;->K(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 4229
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4230
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 4231
    invoke-virtual {v0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v0

    .line 4232
    if-eqz v0, :cond_0

    .line 4233
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4236
    :cond_1
    return-object v1
.end method
