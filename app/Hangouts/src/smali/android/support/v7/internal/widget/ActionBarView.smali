.class public Landroid/support/v7/internal/widget/ActionBarView;
.super Lmw;
.source "PG"


# instance fields
.field private A:Lom;

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Lma;

.field private M:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private N:Lll;

.field private O:Landroid/widget/SpinnerAdapter;

.field private P:La;

.field private Q:Ljava/lang/Runnable;

.field private R:Lnf;

.field private final S:Lny;

.field private final T:Landroid/view/View$OnClickListener;

.field private final U:Landroid/view/View$OnClickListener;

.field public g:Landroid/view/View;

.field public h:Landroid/view/Window$Callback;

.field private i:I

.field private j:I

.field private k:Ljava/lang/CharSequence;

.field private l:Ljava/lang/CharSequence;

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Landroid/content/Context;

.field private p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

.field private q:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/view/View;

.field private v:Lot;

.field private w:Landroid/widget/LinearLayout;

.field private x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

.field private y:Landroid/view/View;

.field private z:Lom;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 166
    invoke-direct {p0, p1, p2}, Lmw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    const/4 v1, -0x1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    .line 135
    new-instance v1, Lnc;

    invoke-direct {v1, p0}, Lnc;-><init>(Landroid/support/v7/internal/widget/ActionBarView;)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->S:Lny;

    .line 149
    new-instance v1, Lnd;

    invoke-direct {v1, p0}, Lnd;-><init>(Landroid/support/v7/internal/widget/ActionBarView;)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->T:Landroid/view/View$OnClickListener;

    .line 159
    new-instance v1, Lne;

    invoke-direct {v1, p0}, Lne;-><init>(Landroid/support/v7/internal/widget/ActionBarView;)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->U:Landroid/view/View$OnClickListener;

    .line 167
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    .line 170
    invoke-virtual {p0, v7}, Landroid/support/v7/internal/widget/ActionBarView;->setBackgroundResource(I)V

    .line 172
    sget-object v1, Lle;->a:[I

    sget v2, Lf;->f:I

    invoke-virtual {p1, p2, v1, v2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 175
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 176
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 177
    const/4 v1, 0x2

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    .line 179
    invoke-virtual {v2, v7}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->k:Ljava/lang/CharSequence;

    .line 180
    const/4 v1, 0x4

    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->l:Ljava/lang/CharSequence;

    .line 181
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->n:Landroid/graphics/drawable/Drawable;

    .line 182
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->n:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 183
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x9

    if-lt v1, v5, :cond_1

    .line 184
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 186
    :try_start_0
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/pm/PackageManager;->getActivityLogo(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->n:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :cond_0
    :goto_0
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->n:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 192
    invoke-virtual {v3, v4}, Landroid/content/pm/ApplicationInfo;->loadLogo(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->n:Landroid/graphics/drawable/Drawable;

    .line 198
    :cond_1
    const/4 v1, 0x7

    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->m:Landroid/graphics/drawable/Drawable;

    .line 199
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->m:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    .line 200
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_2

    .line 202
    :try_start_1
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->m:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 207
    :cond_2
    :goto_1
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->m:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    .line 208
    invoke-virtual {v3, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->m:Landroid/graphics/drawable/Drawable;

    .line 212
    :cond_3
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 214
    const/16 v1, 0xe

    sget v4, Lf;->Z:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 218
    invoke-virtual {v3, v4, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    .line 220
    invoke-virtual {v3, v4, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->q:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    .line 221
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->q:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v1, v8}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->a(Z)V

    .line 222
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->q:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->q:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lf;->aq:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 226
    const/4 v1, 0x5

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->D:I

    .line 227
    const/4 v1, 0x6

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->E:I

    .line 228
    const/16 v1, 0xf

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->F:I

    .line 229
    const/16 v1, 0x10

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->G:I

    .line 232
    const/16 v1, 0x11

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->B:I

    .line 234
    const/16 v1, 0x12

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    .line 236
    const/4 v1, 0x3

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->b(I)V

    .line 238
    const/16 v1, 0xd

    invoke-virtual {v2, v1, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 239
    if-eqz v1, :cond_4

    .line 240
    invoke-virtual {v3, v1, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    .line 241
    iput v7, p0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    .line 242
    iget v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->b(I)V

    .line 245
    :cond_4
    invoke-virtual {v2, v8, v7}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->f:I

    .line 246
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 247
    new-instance v1, Lll;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->k:Ljava/lang/CharSequence;

    invoke-direct {v1, p1, v2}, Lll;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->N:Lll;

    .line 248
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->U:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v1, v8}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setClickable(Z)V

    .line 250
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v1, v8}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setFocusable(Z)V

    .line 252
    return-void

    .line 187
    :catch_0
    move-exception v1

    .line 188
    const-string v5, "ActionBarView"

    const-string v6, "Activity component name not found!"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 203
    :catch_1
    move-exception v1

    .line 204
    const-string v5, "ActionBarView"

    const-string v6, "Activity component name not found!"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1
.end method

.method public static synthetic a(Landroid/support/v7/internal/widget/ActionBarView;)La;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->P:La;

    return-object v0
.end method

.method private a(Lma;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 462
    if-eqz p1, :cond_0

    .line 463
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {p1, v0}, Lma;->a(Lmp;)V

    .line 464
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    invoke-virtual {p1, v0}, Lma;->a(Lmp;)V

    .line 471
    :goto_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0, v2}, Lln;->c(Z)V

    .line 472
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    invoke-virtual {v0, v2}, Lnf;->c(Z)V

    .line 473
    return-void

    .line 466
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    invoke-virtual {v0, v1, v3}, Lln;->a(Landroid/content/Context;Lma;)V

    .line 467
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    invoke-virtual {v0, v1, v3}, Lnf;->a(Landroid/content/Context;Lma;)V

    goto :goto_0
.end method

.method public static synthetic b(Landroid/support/v7/internal/widget/ActionBarView;)Lnf;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    return-object v0
.end method

.method public static synthetic c(Landroid/support/v7/internal/widget/ActionBarView;)Lll;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->N:Lll;

    return-object v0
.end method

.method public static synthetic d(Landroid/support/v7/internal/widget/ActionBarView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->m:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private d(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 527
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->k:Ljava/lang/CharSequence;

    .line 528
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->s:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 529
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 530
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-nez v0, :cond_3

    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->k:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->l:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 533
    :goto_0
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 535
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->N:Lll;

    if-eqz v0, :cond_2

    .line 536
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->N:Lll;

    invoke-virtual {v0, p1}, Lll;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 538
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 530
    goto :goto_0

    .line 533
    :cond_4
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public static synthetic e(Landroid/support/v7/internal/widget/ActionBarView;)Landroid/support/v7/internal/widget/ActionBarView$HomeView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->q:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    return-object v0
.end method

.method public static synthetic f(Landroid/support/v7/internal/widget/ActionBarView;)Landroid/support/v7/internal/widget/ActionBarView$HomeView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    return-object v0
.end method

.method public static synthetic g(Landroid/support/v7/internal/widget/ActionBarView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public static synthetic h(Landroid/support/v7/internal/widget/ActionBarView;)Landroid/support/v7/internal/widget/ScrollingTabContainerView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    return-object v0
.end method

.method public static synthetic i(Landroid/support/v7/internal/widget/ActionBarView;)Lot;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    return-object v0
.end method

.method public static synthetic j(Landroid/support/v7/internal/widget/ActionBarView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic k(Landroid/support/v7/internal/widget/ActionBarView;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    return v0
.end method

.method public static synthetic l(Landroid/support/v7/internal/widget/ActionBarView;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarView;->q()V

    return-void
.end method

.method public static synthetic m(Landroid/support/v7/internal/widget/ActionBarView;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    return v0
.end method

.method private q()V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 765
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-nez v0, :cond_4

    .line 766
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 767
    sget v3, Lf;->ac:I

    invoke-virtual {v0, v3, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    .line 769
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    sget v3, Lf;->F:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->s:Landroid/widget/TextView;

    .line 770
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    sget v3, Lf;->E:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->t:Landroid/widget/TextView;

    .line 771
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    sget v3, Lf;->V:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->u:Landroid/view/View;

    .line 773
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->U:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 775
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->D:I

    if-eqz v0, :cond_0

    .line 776
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->s:Landroid/widget/TextView;

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    iget v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->D:I

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 778
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->k:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 779
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->s:Landroid/widget/TextView;

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 782
    :cond_1
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->E:I

    if-eqz v0, :cond_2

    .line 783
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->t:Landroid/widget/TextView;

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    iget v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->E:I

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 785
    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->l:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    .line 786
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->t:Landroid/widget/TextView;

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->l:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 787
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 790
    :cond_3
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    move v0, v1

    .line 791
    :goto_0
    iget v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_8

    move v3, v1

    .line 792
    :goto_1
    iget-object v6, p0, Landroid/support/v7/internal/widget/ActionBarView;->u:Landroid/view/View;

    if-nez v3, :cond_a

    if-eqz v0, :cond_9

    move v4, v2

    :goto_2
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    .line 793
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_b

    if-nez v3, :cond_b

    :goto_3
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 796
    :cond_4
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 797
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-nez v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->k:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->l:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 800
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 802
    :cond_6
    return-void

    :cond_7
    move v0, v2

    .line 790
    goto :goto_0

    :cond_8
    move v3, v2

    .line 791
    goto :goto_1

    .line 792
    :cond_9
    const/4 v4, 0x4

    goto :goto_2

    :cond_a
    move v4, v5

    goto :goto_2

    :cond_b
    move v1, v2

    .line 793
    goto :goto_3
.end method


# virtual methods
.method public bridge synthetic a()I
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lmw;->a()I

    move-result v0

    return v0
.end method

.method public bridge synthetic a(I)V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0, p1}, Lmw;->a(I)V

    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 655
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->n:Landroid/graphics/drawable/Drawable;

    .line 656
    if-eqz p1, :cond_0

    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 659
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Landroid/support/v7/internal/widget/ActionBarContainer;)V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0, p1}, Lmw;->a(Landroid/support/v7/internal/widget/ActionBarContainer;)V

    return-void
.end method

.method public a(Landroid/support/v7/internal/widget/ActionBarContextView;)V
    .locals 0

    .prologue
    .line 805
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->M:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 806
    return-void
.end method

.method public a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 377
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    .line 380
    :cond_0
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    .line 381
    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->I:Z

    .line 382
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->I:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 383
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 384
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 385
    const/4 v2, -0x2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 386
    const/4 v2, -0x1

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 387
    invoke-virtual {p1, v1}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->a(Z)V

    .line 389
    :cond_1
    return-void

    .line 381
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 489
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 490
    :goto_0
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 491
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    .line 493
    :cond_0
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    .line 494
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 497
    :cond_1
    return-void

    .line 489
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/Window$Callback;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->h:Landroid/view/Window$Callback;

    .line 295
    return-void
.end method

.method public a(Ldw;Lmq;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 396
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->L:Lma;

    if-ne p1, v0, :cond_0

    .line 459
    :goto_0
    return-void

    .line 400
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->L:Lma;

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->L:Lma;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0, v1}, Lma;->b(Lmp;)V

    .line 402
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->L:Lma;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    invoke-virtual {v0, v1}, Lma;->b(Lmp;)V

    .line 405
    :cond_1
    check-cast p1, Lma;

    .line 406
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->L:Lma;

    .line 407
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    if-eqz v0, :cond_2

    .line 408
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 409
    if-eqz v0, :cond_2

    .line 410
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 413
    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    if-nez v0, :cond_3

    .line 414
    new-instance v0, Lln;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    invoke-direct {v0, v1}, Lln;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    .line 415
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0, p2}, Lln;->a(Lmq;)V

    .line 416
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    sget v1, Lf;->H:I

    invoke-virtual {v0, v1}, Lln;->b(I)V

    .line 417
    new-instance v0, Lnf;

    invoke-direct {v0, p0, v4}, Lnf;-><init>(Landroid/support/v7/internal/widget/ActionBarView;B)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    .line 421
    :cond_3
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {v2, v0, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 423
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->d:Z

    if-nez v0, :cond_5

    .line 424
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lf;->r:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lln;->b(Z)V

    .line 427
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Lma;)V

    .line 428
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0, p0}, Lln;->a(Landroid/view/ViewGroup;)Lmr;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ActionMenuView;

    .line 429
    invoke-virtual {v0, p1}, Landroid/support/v7/internal/view/menu/ActionMenuView;->a(Lma;)V

    .line 430
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 431
    if-eqz v1, :cond_4

    if-eq v1, p0, :cond_4

    .line 432
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 434
    :cond_4
    invoke-virtual {p0, v0, v2}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 458
    :goto_1
    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    goto/16 :goto_0

    .line 436
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0, v4}, Lln;->b(Z)V

    .line 438
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0, v1}, Lln;->a(I)V

    .line 441
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0}, Lln;->b()V

    .line 443
    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 444
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Lma;)V

    .line 445
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0, p0}, Lln;->a(Landroid/view/ViewGroup;)Lmr;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ActionMenuView;

    .line 446
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->c:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v1, :cond_7

    .line 447
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 448
    if-eqz v1, :cond_6

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->c:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eq v1, v3, :cond_6

    .line 449
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 451
    :cond_6
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ActionMenuView;->setVisibility(I)V

    .line 452
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->c:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 455
    :cond_7
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/view/menu/ActionMenuView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->H:Z

    .line 511
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->d(Ljava/lang/CharSequence;)V

    .line 512
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 329
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->d:Z

    if-eq v0, p1, :cond_5

    .line 330
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    if-eqz v0, :cond_2

    .line 331
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 332
    if-eqz v0, :cond_0

    .line 333
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 335
    :cond_0
    if-eqz p1, :cond_6

    .line 336
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->c:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->c:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->addView(Landroid/view/View;)V

    .line 339
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v2, -0x1

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 344
    :goto_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->requestLayout()V

    .line 346
    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->c:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_3

    .line 347
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->c:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz p1, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 350
    :cond_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    if-eqz v0, :cond_4

    .line 351
    if-nez p1, :cond_8

    .line 352
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->r:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lln;->b(Z)V

    .line 364
    :cond_4
    :goto_2
    invoke-super {p0, p1}, Lmw;->a(Z)V

    .line 366
    :cond_5
    return-void

    .line 341
    :cond_6
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 342
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v2, -0x2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0

    .line 347
    :cond_7
    const/16 v0, 0x8

    goto :goto_1

    .line 356
    :cond_8
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0, v1}, Lln;->b(Z)V

    .line 358
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0, v1}, Lln;->a(I)V

    .line 361
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0}, Lln;->b()V

    goto :goto_2
.end method

.method public b(I)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/4 v0, -0x1

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 572
    iget v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    if-ne v1, v0, :cond_6

    .line 573
    :goto_0
    iput p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    .line 575
    and-int/lit8 v1, v0, 0x1f

    if-eqz v1, :cond_11

    .line 576
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_7

    move v5, v4

    .line 577
    :goto_1
    if-eqz v5, :cond_8

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-nez v1, :cond_8

    move v1, v2

    .line 578
    :goto_2
    iget-object v6, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v6, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setVisibility(I)V

    .line 580
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 581
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_9

    move v1, v4

    .line 582
    :goto_3
    iget-object v6, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v6, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->a(Z)V

    .line 588
    if-eqz v1, :cond_0

    .line 589
    invoke-virtual {p0, v4}, Landroid/support/v7/internal/widget/ActionBarView;->c(Z)V

    .line 593
    :cond_0
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_1

    .line 594
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_a

    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_a

    move v1, v4

    .line 596
    :goto_4
    iget-object v6, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    if-eqz v1, :cond_b

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->n:Landroid/graphics/drawable/Drawable;

    :goto_5
    invoke-virtual {v6, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 599
    :cond_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    .line 600
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_c

    .line 601
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarView;->q()V

    .line 607
    :cond_2
    :goto_6
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_4

    and-int/lit8 v1, v0, 0x6

    if-eqz v1, :cond_4

    .line 609
    iget v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_d

    move v1, v4

    .line 610
    :goto_7
    iget-object v6, p0, Landroid/support/v7/internal/widget/ActionBarView;->u:Landroid/view/View;

    if-nez v5, :cond_3

    if-eqz v1, :cond_e

    move v3, v2

    :cond_3
    :goto_8
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 611
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-nez v5, :cond_f

    if-eqz v1, :cond_f

    :goto_9
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 614
    :cond_4
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 615
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_10

    .line 616
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 622
    :cond_5
    :goto_a
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->requestLayout()V

    .line 628
    :goto_b
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_12

    .line 629
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 637
    :goto_c
    return-void

    .line 572
    :cond_6
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    xor-int/2addr v0, p1

    goto/16 :goto_0

    :cond_7
    move v5, v2

    .line 576
    goto/16 :goto_1

    :cond_8
    move v1, v3

    .line 577
    goto/16 :goto_2

    :cond_9
    move v1, v2

    .line 581
    goto :goto_3

    :cond_a
    move v1, v2

    .line 594
    goto :goto_4

    .line 596
    :cond_b
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->m:Landroid/graphics/drawable/Drawable;

    goto :goto_5

    .line 603
    :cond_c
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    goto :goto_6

    :cond_d
    move v1, v2

    .line 609
    goto :goto_7

    .line 610
    :cond_e
    const/4 v3, 0x4

    goto :goto_8

    :cond_f
    move v4, v2

    .line 611
    goto :goto_9

    .line 618
    :cond_10
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    goto :goto_a

    .line 624
    :cond_11
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->invalidate()V

    goto :goto_b

    .line 630
    :cond_12
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_13

    .line 631
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->aq:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 634
    :cond_13
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->ap:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_c
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1205
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1206
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 521
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->H:Z

    if-nez v0, :cond_0

    .line 522
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->d(Ljava/lang/CharSequence;)V

    .line 524
    :cond_0
    return-void
.end method

.method public bridge synthetic b(Z)V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0, p1}, Lmw;->b(Z)V

    return-void
.end method

.method public bridge synthetic b()Z
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lmw;->b()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic c()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Lmw;->c()V

    return-void
.end method

.method public c(I)V
    .locals 3

    .prologue
    .line 666
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    .line 667
    if-eq p1, v0, :cond_2

    .line 668
    packed-switch v0, :pswitch_data_0

    .line 680
    :cond_0
    :goto_0
    packed-switch p1, :pswitch_data_1

    .line 704
    :cond_1
    :goto_1
    iput p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    .line 705
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->requestLayout()V

    .line 707
    :cond_2
    return-void

    .line 670
    :pswitch_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 675
    :pswitch_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->I:Z

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 682
    :pswitch_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    if-nez v0, :cond_3

    .line 683
    new-instance v0, Lot;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    sget v2, Lf;->i:I

    invoke-direct {v0, v1, v2}, Lot;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    .line 685
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->ad:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    .line 687
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 689
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 690
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 692
    :cond_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    invoke-virtual {v0}, Lot;->e()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->O:Landroid/widget/SpinnerAdapter;

    if-eq v0, v1, :cond_4

    .line 693
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->O:Landroid/widget/SpinnerAdapter;

    invoke-virtual {v0, v1}, Lot;->a(Landroid/widget/SpinnerAdapter;)V

    .line 695
    :cond_4
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->S:Lny;

    invoke-virtual {v0, v1}, Lot;->a(Lny;)V

    .line 696
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 699
    :pswitch_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->I:Z

    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 668
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 680
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 545
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->l:Ljava/lang/CharSequence;

    .line 546
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->t:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 547
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->t:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 549
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-nez v0, :cond_3

    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->k:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->l:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 552
    :goto_1
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 554
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 548
    goto :goto_0

    :cond_3
    move v0, v1

    .line 549
    goto :goto_1

    :cond_4
    move v1, v2

    .line 552
    goto :goto_2
.end method

.method public c(Z)V
    .locals 3

    .prologue
    .line 557
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setEnabled(Z)V

    .line 558
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setFocusable(Z)V

    .line 560
    if-nez p1, :cond_0

    .line 561
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 569
    :goto_0
    return-void

    .line 562
    :cond_0
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 563
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->aq:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 566
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->ap:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    invoke-virtual {v0, p1}, Lot;->a(I)V

    .line 722
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 809
    iput-boolean p1, p0, Landroid/support/v7/internal/widget/ActionBarView;->J:Z

    .line 810
    return-void
.end method

.method public bridge synthetic d()Z
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lmw;->d()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic e()Z
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lmw;->e()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic f()Z
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lmw;->f()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic g()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Lmw;->g()V

    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 744
    new-instance v0, Lkf;

    invoke-direct {v0}, Lkf;-><init>()V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 1159
    new-instance v0, Lkf;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lkf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .prologue
    .line 1164
    if-nez p1, :cond_0

    .line 1165
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    .line 1167
    :cond_0
    return-object p1
.end method

.method public h()V
    .locals 3

    .prologue
    .line 312
    new-instance v0, Lom;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    iget v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->F:I

    invoke-direct {v0, v1, v2}, Lom;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    .line 313
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    sget v1, Lf;->Q:I

    invoke-virtual {v0, v1}, Lom;->setId(I)V

    .line 314
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lom;->a(I)V

    .line 315
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lom;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 317
    return-void
.end method

.method public i()V
    .locals 3

    .prologue
    .line 320
    new-instance v0, Lom;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->o:Landroid/content/Context;

    iget v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->G:I

    invoke-direct {v0, v1, v2}, Lom;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    .line 322
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    sget v1, Lf;->P:I

    invoke-virtual {v0, v1}, Lom;->setId(I)V

    .line 323
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lom;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 325
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 369
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->d:Z

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    iget-object v0, v0, Lnf;->b:Lme;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 483
    :goto_0
    if-eqz v0, :cond_0

    .line 484
    invoke-virtual {v0}, Lme;->collapseActionView()Z

    .line 486
    :cond_0
    return-void

    .line 481
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    iget-object v0, v0, Lnf;->b:Lme;

    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->v:Lot;

    invoke-virtual {v0}, Lot;->g()I

    move-result v0

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 733
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    return v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 737
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256
    invoke-super {p0, p1}, Lmw;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 258
    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->s:Landroid/widget/TextView;

    .line 259
    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->t:Landroid/widget/TextView;

    .line 260
    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->u:Landroid/view/View;

    .line 261
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 262
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    .line 264
    :cond_0
    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    .line 265
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    .line 266
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarView;->q()V

    .line 269
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->I:Z

    if-eqz v0, :cond_3

    .line 270
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_2

    .line 272
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 273
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 275
    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->a(Z)V

    .line 278
    :cond_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    if-eqz v0, :cond_4

    .line 279
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    .line 280
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->h()V

    .line 282
    :cond_4
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    if-eqz v0, :cond_5

    .line 283
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    .line 284
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->i()V

    .line 286
    :cond_5
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 299
    invoke-super {p0}, Lmw;->onDetachedFromWindow()V

    .line 300
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->Q:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 301
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0}, Lln;->d()Z

    .line 303
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->b:Lln;

    invoke-virtual {v0}, Lln;->f()Z

    .line 305
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 749
    invoke-super {p0}, Lmw;->onFinishInflate()V

    .line 751
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 753
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 754
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 755
    if-eq v0, p0, :cond_1

    .line 756
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 757
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 759
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    .line 762
    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 1015
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingLeft()I

    move-result v1

    .line 1016
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingTop()I

    move-result v2

    .line 1017
    sub-int v0, p5, p3

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingBottom()I

    move-result v3

    sub-int v3, v0, v3

    .line 1019
    if-gtz v3, :cond_1

    .line 1155
    :cond_0
    :goto_0
    return-void

    .line 1024
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->q:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    .line 1025
    :goto_1
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_15

    .line 1026
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->a()I

    move-result v4

    .line 1027
    add-int v5, v1, v4

    invoke-virtual {p0, v0, v5, v2, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;III)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    .line 1030
    :goto_2
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-nez v1, :cond_3

    .line 1031
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_8

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    const/16 v4, 0x8

    if-eq v1, v4, :cond_8

    iget v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    .line 1034
    :goto_3
    if-eqz v1, :cond_2

    .line 1035
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v4, v0, v2, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;III)I

    move-result v4

    add-int/2addr v0, v4

    .line 1038
    :cond_2
    iget v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    packed-switch v4, :pswitch_data_0

    :cond_3
    move v1, v0

    .line 1060
    :goto_4
    sub-int v0, p4, p2

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v0, v4

    .line 1061
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    if-eqz v4, :cond_4

    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v4}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-ne v4, p0, :cond_4

    .line 1062
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {p0, v4, v0, v2, v3}, Landroid/support/v7/internal/widget/ActionBarView;->b(Landroid/view/View;III)I

    .line 1063
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v4}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v0, v4

    .line 1066
    :cond_4
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    if-eqz v4, :cond_14

    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    invoke-virtual {v4}, Lom;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_14

    .line 1068
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    invoke-virtual {p0, v4, v0, v2, v3}, Landroid/support/v7/internal/widget/ActionBarView;->b(Landroid/view/View;III)I

    .line 1069
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    invoke-virtual {v2}, Lom;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    move v2, v0

    .line 1072
    :goto_5
    const/4 v0, 0x0

    .line 1073
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-eqz v3, :cond_b

    .line 1074
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    move-object v7, v0

    .line 1079
    :goto_6
    if-eqz v7, :cond_6

    .line 1080
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1081
    instance-of v3, v0, Lkf;

    if-eqz v3, :cond_c

    check-cast v0, Lkf;

    move-object v5, v0

    .line 1084
    :goto_7
    if-eqz v5, :cond_d

    iget v0, v5, Lkf;->a:I

    .line 1085
    :goto_8
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 1087
    const/4 v4, 0x0

    .line 1088
    const/4 v3, 0x0

    .line 1089
    if-eqz v5, :cond_12

    .line 1090
    iget v3, v5, Lkf;->leftMargin:I

    add-int v4, v1, v3

    .line 1091
    iget v1, v5, Lkf;->rightMargin:I

    sub-int v3, v2, v1

    .line 1092
    iget v2, v5, Lkf;->topMargin:I

    .line 1093
    iget v1, v5, Lkf;->bottomMargin:I

    move v5, v2

    move v6, v3

    move v2, v4

    move v4, v1

    .line 1096
    :goto_9
    and-int/lit8 v1, v0, 0x7

    .line 1098
    const/4 v3, 0x1

    if-ne v1, v3, :cond_10

    .line 1099
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getWidth()I

    move-result v3

    sub-int/2addr v3, v8

    div-int/lit8 v3, v3, 0x2

    .line 1100
    if-ge v3, v2, :cond_e

    .line 1101
    const/4 v1, 0x3

    move v3, v1

    .line 1109
    :goto_a
    const/4 v1, 0x0

    .line 1110
    packed-switch v3, :pswitch_data_1

    :pswitch_0
    move v2, v1

    .line 1122
    :goto_b
    :pswitch_1
    and-int/lit8 v1, v0, 0x70

    .line 1124
    const/4 v3, -0x1

    if-ne v0, v3, :cond_5

    .line 1125
    const/16 v0, 0x10

    move v1, v0

    .line 1128
    :cond_5
    const/4 v0, 0x0

    .line 1129
    sparse-switch v1, :sswitch_data_0

    .line 1143
    :goto_c
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 1144
    add-int/2addr v1, v2

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v7, v2, v0, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 1146
    :cond_6
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    if-eqz v0, :cond_0

    .line 1150
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    invoke-virtual {v0}, Lom;->bringToFront()V

    .line 1151
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    invoke-virtual {v0}, Lom;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 1152
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    iget v2, p0, Landroid/support/v7/internal/widget/ActionBarView;->B:I

    neg-int v3, v0

    iget v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->B:I

    iget-object v5, p0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    invoke-virtual {v5}, Lom;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Lom;->layout(IIII)V

    goto/16 :goto_0

    .line 1024
    :cond_7
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    goto/16 :goto_1

    .line 1031
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_3

    :pswitch_2
    move v1, v0

    .line 1040
    goto/16 :goto_4

    .line 1042
    :pswitch_3
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_3

    .line 1043
    if-eqz v1, :cond_9

    .line 1044
    iget v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    add-int/2addr v0, v1

    .line 1046
    :cond_9
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;III)I

    move-result v1

    iget v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    move v1, v0

    goto/16 :goto_4

    .line 1050
    :pswitch_4
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v4, :cond_3

    .line 1051
    if-eqz v1, :cond_a

    .line 1052
    iget v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    add-int/2addr v0, v1

    .line 1054
    :cond_a
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;III)I

    move-result v1

    iget v4, p0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    move v1, v0

    goto/16 :goto_4

    .line 1075
    :cond_b
    iget v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_13

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    if-eqz v3, :cond_13

    .line 1077
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    move-object v7, v0

    goto/16 :goto_6

    .line 1081
    :cond_c
    const/4 v0, 0x0

    move-object v5, v0

    goto/16 :goto_7

    .line 1084
    :cond_d
    const/16 v0, 0x13

    goto/16 :goto_8

    .line 1102
    :cond_e
    add-int/2addr v3, v8

    if-le v3, v6, :cond_f

    .line 1103
    const/4 v1, 0x5

    :cond_f
    move v3, v1

    .line 1105
    goto/16 :goto_a

    :cond_10
    const/4 v3, -0x1

    if-ne v0, v3, :cond_11

    .line 1106
    const/4 v1, 0x3

    move v3, v1

    goto/16 :goto_a

    .line 1112
    :pswitch_5
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getWidth()I

    move-result v1

    sub-int/2addr v1, v8

    div-int/lit8 v1, v1, 0x2

    move v2, v1

    .line 1113
    goto/16 :goto_b

    .line 1118
    :pswitch_6
    sub-int v1, v6, v8

    move v2, v1

    goto/16 :goto_b

    .line 1131
    :sswitch_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingTop()I

    move-result v0

    .line 1132
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v1, v3

    .line 1133
    sub-int v0, v1, v0

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    goto/16 :goto_c

    .line 1136
    :sswitch_1
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, v5

    .line 1137
    goto/16 :goto_c

    .line 1139
    :sswitch_2
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    sub-int/2addr v0, v4

    goto/16 :goto_c

    :cond_11
    move v3, v1

    goto/16 :goto_a

    :cond_12
    move v5, v4

    move v6, v2

    move v4, v3

    move v2, v1

    goto/16 :goto_9

    :cond_13
    move-object v7, v0

    goto/16 :goto_6

    :cond_14
    move v2, v0

    goto/16 :goto_5

    :cond_15
    move v0, v1

    goto/16 :goto_2

    .line 1038
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 1110
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_6
    .end packed-switch

    .line 1129
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x30 -> :sswitch_1
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 19

    .prologue
    .line 818
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/internal/widget/ActionBarView;->getChildCount()I

    move-result v13

    .line 819
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->J:Z

    if-eqz v1, :cond_4

    .line 820
    const/4 v2, 0x0

    .line 821
    const/4 v1, 0x0

    move/from16 v18, v1

    move v1, v2

    move/from16 v2, v18

    :goto_0
    if-ge v2, v13, :cond_2

    .line 822
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 823
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    if-ne v3, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v3}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getChildCount()I

    move-result v3

    if-eqz v3, :cond_1

    .line 825
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 821
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 829
    :cond_2
    if-nez v1, :cond_4

    .line 831
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/widget/ActionBarView;->setMeasuredDimension(II)V

    .line 832
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->K:Z

    .line 1011
    :cond_3
    :goto_1
    return-void

    .line 836
    :cond_4
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->K:Z

    .line 838
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 839
    const/high16 v2, 0x40000000    # 2.0f

    if-eq v1, v2, :cond_5

    .line 840
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " can only be used with android:layout_width=\"MATCH_PARENT\" (or fill_parent)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 844
    :cond_5
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 845
    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_6

    .line 846
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " can only be used with android:layout_height=\"wrap_content\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 850
    :cond_6
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    .line 852
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->f:I

    if-lez v1, :cond_e

    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->f:I

    move v3, v1

    .line 855
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingTop()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingBottom()I

    move-result v2

    add-int v15, v1, v2

    .line 856
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingLeft()I

    move-result v1

    .line 857
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/internal/widget/ActionBarView;->getPaddingRight()I

    move-result v2

    .line 858
    sub-int v10, v3, v15

    .line 859
    const/high16 v4, -0x80000000

    invoke-static {v10, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 861
    sub-int v1, v14, v1

    sub-int v5, v1, v2

    .line 862
    div-int/lit8 v4, v5, 0x2

    .line 865
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-eqz v1, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->q:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    .line 867
    :goto_3
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->getVisibility()I

    move-result v2

    const/16 v7, 0x8

    if-eq v2, v7, :cond_20

    .line 868
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 870
    iget v7, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gez v7, :cond_10

    .line 871
    const/high16 v2, -0x80000000

    invoke-static {v5, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 875
    :goto_4
    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v10, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v1, v2, v7}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->measure(II)V

    .line 877
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView$HomeView;->a()I

    move-result v1

    add-int/2addr v1, v2

    .line 878
    const/4 v2, 0x0

    sub-int/2addr v5, v1

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 879
    const/4 v5, 0x0

    sub-int v1, v2, v1

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 882
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    if-eqz v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v5}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    move-object/from16 v0, p0

    if-ne v5, v0, :cond_7

    .line 883
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v2, v6}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;II)I

    move-result v2

    .line 885
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/internal/widget/ActionBarView;->a:Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v7}, Landroid/support/v7/internal/view/menu/ActionMenuView;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v4, v7

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 888
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    if-eqz v5, :cond_8

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    invoke-virtual {v5}, Lom;->getVisibility()I

    move-result v5

    const/16 v7, 0x8

    if-eq v5, v7, :cond_8

    .line 890
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v2, v6}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;II)I

    move-result v2

    .line 892
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->A:Lom;

    invoke-virtual {v6}, Lom;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v4, v6

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 896
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_11

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_11

    const/4 v5, 0x1

    .line 899
    :goto_6
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-nez v6, :cond_9

    .line 900
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->i:I

    packed-switch v6, :pswitch_data_0

    :cond_9
    move v6, v1

    move v7, v2

    .line 930
    :goto_7
    const/4 v1, 0x0

    .line 931
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-eqz v2, :cond_14

    .line 932
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    move-object v12, v1

    .line 938
    :goto_8
    if-eqz v12, :cond_c

    .line 939
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 940
    instance-of v1, v2, Lkf;

    if-eqz v1, :cond_15

    move-object v1, v2

    check-cast v1, Lkf;

    move-object v11, v1

    .line 943
    :goto_9
    const/4 v8, 0x0

    .line 944
    const/4 v1, 0x0

    .line 945
    if-eqz v11, :cond_a

    .line 946
    iget v1, v11, Lkf;->leftMargin:I

    iget v8, v11, Lkf;->rightMargin:I

    add-int/2addr v8, v1

    .line 947
    iget v1, v11, Lkf;->topMargin:I

    iget v9, v11, Lkf;->bottomMargin:I

    add-int/2addr v1, v9

    .line 953
    :cond_a
    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/v7/internal/widget/ActionBarView;->f:I

    if-lez v9, :cond_16

    .line 954
    iget v9, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/16 v16, -0x2

    move/from16 v0, v16

    if-eq v9, v0, :cond_16

    const/high16 v9, 0x40000000    # 2.0f

    .line 959
    :goto_a
    const/16 v16, 0x0

    iget v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v17, v0

    if-ltz v17, :cond_b

    iget v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    :cond_b
    sub-int v1, v10, v1

    move/from16 v0, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 962
    iget v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v10, -0x2

    if-eq v1, v10, :cond_17

    const/high16 v1, 0x40000000    # 2.0f

    .line 964
    :goto_b
    const/16 v17, 0x0

    iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ltz v10, :cond_18

    iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v10, v7}, Ljava/lang/Math;->min(II)I

    move-result v10

    :goto_c
    sub-int/2addr v10, v8

    move/from16 v0, v17

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 967
    if-eqz v11, :cond_19

    iget v11, v11, Lkf;->a:I

    :goto_d
    and-int/lit8 v11, v11, 0x7

    .line 972
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v11, v0, :cond_1e

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v11, -0x1

    if-ne v2, v11, :cond_1e

    .line 973
    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    .line 976
    :goto_e
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    move/from16 v0, v16

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v12, v1, v2}, Landroid/view/View;->measure(II)V

    .line 979
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v8

    sub-int/2addr v7, v1

    .line 982
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->g:Landroid/view/View;

    if-nez v1, :cond_d

    if-eqz v5, :cond_d

    .line 983
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/internal/widget/ActionBarView;->f:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v7, v2}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;II)I

    .line 985
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/internal/widget/ActionBarView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v6, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    .line 988
    :cond_d
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->f:I

    if-gtz v1, :cond_1c

    .line 989
    const/4 v2, 0x0

    .line 990
    const/4 v1, 0x0

    move v3, v1

    :goto_f
    if-ge v3, v13, :cond_1a

    .line 991
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 992
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v15

    .line 993
    if-le v1, v2, :cond_1d

    .line 990
    :goto_10
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_f

    .line 852
    :cond_e
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    move v3, v1

    goto/16 :goto_2

    .line 865
    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->p:Landroid/support/v7/internal/widget/ActionBarView$HomeView;

    goto/16 :goto_3

    .line 873
    :cond_10
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_4

    .line 896
    :cond_11
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 902
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    if-eqz v6, :cond_9

    .line 903
    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    shl-int/lit8 v6, v6, 0x1

    .line 904
    :goto_11
    const/4 v7, 0x0

    sub-int/2addr v2, v6

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 905
    const/4 v7, 0x0

    sub-int/2addr v1, v6

    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 906
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    const/high16 v7, -0x80000000

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v10, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/LinearLayout;->measure(II)V

    .line 909
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v6

    .line 910
    const/4 v7, 0x0

    sub-int/2addr v2, v6

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 911
    const/4 v7, 0x0

    sub-int/2addr v1, v6

    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v6, v1

    move v7, v2

    .line 912
    goto/16 :goto_7

    .line 903
    :cond_12
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    goto :goto_11

    .line 915
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v6, :cond_9

    .line 916
    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    shl-int/lit8 v6, v6, 0x1

    .line 917
    :goto_12
    const/4 v7, 0x0

    sub-int/2addr v2, v6

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 918
    const/4 v7, 0x0

    sub-int/2addr v1, v6

    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 919
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    const/high16 v7, -0x80000000

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v10, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->measure(II)V

    .line 922
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->x:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v6}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->getMeasuredWidth()I

    move-result v6

    .line 923
    const/4 v7, 0x0

    sub-int/2addr v2, v6

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 924
    const/4 v7, 0x0

    sub-int/2addr v1, v6

    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v6, v1

    move v7, v2

    goto/16 :goto_7

    .line 916
    :cond_13
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/internal/widget/ActionBarView;->C:I

    goto :goto_12

    .line 933
    :cond_14
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/internal/widget/ActionBarView;->j:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    if-eqz v2, :cond_1f

    .line 935
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->y:Landroid/view/View;

    move-object v12, v1

    goto/16 :goto_8

    .line 940
    :cond_15
    const/4 v1, 0x0

    move-object v11, v1

    goto/16 :goto_9

    .line 954
    :cond_16
    const/high16 v9, -0x80000000

    goto/16 :goto_a

    .line 962
    :cond_17
    const/high16 v1, -0x80000000

    goto/16 :goto_b

    :cond_18
    move v10, v7

    .line 964
    goto/16 :goto_c

    .line 967
    :cond_19
    const/16 v11, 0x13

    goto/16 :goto_d

    .line 997
    :cond_1a
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, Landroid/support/v7/internal/widget/ActionBarView;->setMeasuredDimension(II)V

    .line 1002
    :goto_13
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->M:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v1, :cond_1b

    .line 1003
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->M:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/internal/widget/ActionBarView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(I)V

    .line 1006
    :cond_1b
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    invoke-virtual {v1}, Lom;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_3

    .line 1007
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarView;->z:Lom;

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/internal/widget/ActionBarView;->B:I

    shl-int/lit8 v2, v2, 0x1

    sub-int v2, v14, v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/internal/widget/ActionBarView;->getMeasuredHeight()I

    move-result v3

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lom;->measure(II)V

    goto/16 :goto_1

    .line 999
    :cond_1c
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3}, Landroid/support/v7/internal/widget/ActionBarView;->setMeasuredDimension(II)V

    goto :goto_13

    :cond_1d
    move v1, v2

    goto/16 :goto_10

    :cond_1e
    move v2, v10

    goto/16 :goto_e

    :cond_1f
    move-object v12, v1

    goto/16 :goto_8

    :cond_20
    move v1, v4

    move v2, v5

    goto/16 :goto_5

    .line 900
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1186
    check-cast p1, Lng;

    .line 1188
    invoke-virtual {p1}, Lng;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lmw;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1190
    iget v0, p1, Lng;->a:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->L:Lma;

    if-eqz v0, :cond_0

    .line 1192
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->L:Lma;

    iget v1, p1, Lng;->a:I

    invoke-virtual {v0, v1}, Lma;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, Ldx;

    .line 1194
    if-eqz v0, :cond_0

    .line 1195
    invoke-interface {v0}, Ldx;->expandActionView()Z

    .line 1199
    :cond_0
    iget-boolean v0, p1, Lng;->b:Z

    if-eqz v0, :cond_1

    .line 1200
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->c()V

    .line 1202
    :cond_1
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1172
    invoke-super {p0}, Lmw;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1173
    new-instance v1, Lng;

    invoke-direct {v1, v0}, Lng;-><init>(Landroid/os/Parcelable;)V

    .line 1175
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    iget-object v0, v0, Lnf;->b:Lme;

    if-eqz v0, :cond_0

    .line 1176
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->R:Lnf;

    iget-object v0, v0, Lnf;->b:Lme;

    invoke-virtual {v0}, Lme;->getItemId()I

    move-result v0

    iput v0, v1, Lng;->a:I

    .line 1179
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarView;->e()Z

    move-result v0

    iput-boolean v0, v1, Lng;->b:Z

    .line 1181
    return-object v1
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 813
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarView;->K:Z

    return v0
.end method

.method public bridge synthetic setVisibility(I)V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0, p1}, Lmw;->setVisibility(I)V

    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    return v0
.end method
