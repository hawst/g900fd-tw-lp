.class public final Lavg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field final synthetic a:Lavb;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:[Ljava/io/InputStream;


# direct methods
.method private constructor <init>(Lavb;Ljava/lang/String;J[Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 707
    iput-object p1, p0, Lavg;->a:Lavb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 708
    iput-object p2, p0, Lavg;->b:Ljava/lang/String;

    .line 709
    iput-wide p3, p0, Lavg;->c:J

    .line 710
    iput-object p5, p0, Lavg;->d:[Ljava/io/InputStream;

    .line 711
    return-void
.end method

.method synthetic constructor <init>(Lavb;Ljava/lang/String;J[Ljava/io/InputStream;B)V
    .locals 0

    .prologue
    .line 702
    invoke-direct/range {p0 .. p5}, Lavg;-><init>(Lavb;Ljava/lang/String;J[Ljava/io/InputStream;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 725
    iget-object v0, p0, Lavg;->d:[Ljava/io/InputStream;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public close()V
    .locals 4

    .prologue
    .line 737
    iget-object v1, p0, Lavg;->d:[Ljava/io/InputStream;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 738
    invoke-static {v3}, Lavb;->a(Ljava/io/Closeable;)V

    .line 737
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 740
    :cond_0
    return-void
.end method
