.class public final Lbsx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:[Ljava/lang/String;

.field private static final c:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbsx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Lzr;

.field private volatile f:Z

.field private final g:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    sget-object v0, Lbys;->l:Lcyp;

    sput-boolean v2, Lbsx;->a:Z

    .line 34
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "key"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "value"

    aput-object v2, v0, v1

    sput-object v0, Lbsx;->b:[Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbsx;->c:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lyj;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbsx;->g:Ljava/util/concurrent/ConcurrentHashMap;

    .line 101
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsx;->d:Ljava/lang/String;

    .line 103
    invoke-static {p1}, Lzo;->a(Lyj;)Lzo;

    move-result-object v0

    invoke-virtual {v0}, Lzo;->d()Lzr;

    move-result-object v0

    iput-object v0, p0, Lbsx;->e:Lzr;

    .line 104
    return-void
.end method

.method public static a(Lyj;)Lbsx;
    .locals 3

    .prologue
    .line 64
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    .line 65
    sget-object v0, Lbsx;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsx;

    .line 66
    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lbsx;

    invoke-direct {v0, p0}, Lbsx;-><init>(Lyj;)V

    .line 73
    sget-object v2, Lbsx;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lbsx;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsx;

    .line 76
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lbsx;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-void
.end method

.method public static b(Lyj;)Z
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 85
    sget-object v1, Lbsx;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsx;

    .line 86
    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, v0, Lbsx;->f:Z

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 181
    invoke-virtual {p0, p1}, Lbsx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 183
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    :goto_0
    return-wide p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p2

    goto :goto_0
.end method

.method public a()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 107
    iget-boolean v0, p0, Lbsx;->f:Z

    if-eqz v0, :cond_0

    .line 156
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->a()V

    .line 124
    :try_start_0
    iget-object v8, p0, Lbsx;->g:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 125
    :try_start_1
    iget-boolean v0, p0, Lbsx;->f:Z

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->b()V

    .line 127
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 155
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    goto :goto_0

    .line 130
    :cond_1
    :try_start_2
    sget-boolean v0, Lbsx;->a:Z

    if-eqz v0, :cond_2

    .line 131
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loading metadata "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbsx;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 135
    :cond_2
    :try_start_3
    iget-object v0, p0, Lbsx;->e:Lzr;

    const-string v1, "realtimechat_metadata"

    sget-object v2, Lbsx;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 139
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 141
    :cond_3
    iget-object v0, p0, Lbsx;->g:Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    .line 142
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 141
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result v0

    if-nez v0, :cond_3

    .line 146
    :cond_4
    if-eqz v1, :cond_5

    .line 147
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 151
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsx;->f:Z

    .line 152
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->b()V

    .line 153
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 155
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_1
    if-eqz v1, :cond_6

    .line 147
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 153
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit v8

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 155
    :catchall_2
    move-exception v0

    iget-object v1, p0, Lbsx;->e:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0

    .line 146
    :catchall_3
    move-exception v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 193
    sget-boolean v0, Lbsx;->a:Z

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "saveMetadata: key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_0
    invoke-virtual {p0}, Lbsx;->a()V

    .line 199
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 200
    const-string v1, "key"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v1, "value"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v1, p0, Lbsx;->e:Lzr;

    invoke-virtual {v1}, Lzr;->a()V

    .line 205
    :try_start_0
    iget-object v1, p0, Lbsx;->e:Lzr;

    const-string v2, "realtimechat_metadata"

    invoke-virtual {v1, v2, v0}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 207
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->b()V

    .line 208
    iget-object v0, p0, Lbsx;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    .line 211
    return-void

    .line 210
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbsx;->e:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0
.end method

.method public a(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-static {p2, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbsx;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 160
    invoke-virtual {p0}, Lbsx;->a()V

    .line 162
    iget-object v0, p0, Lbsx;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 163
    sget-boolean v1, Lbsx;->a:Z

    if-eqz v1, :cond_0

    .line 164
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "queryMetadata: key="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_0
    return-object v0
.end method

.method public b(Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 235
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbsx;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 218
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 219
    new-instance v0, Lbsy;

    invoke-direct {v0, p0, p1, p2}, Lbsy;-><init>(Lbsx;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->executeOnThreadPool(Ljava/lang/Runnable;)V

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_0
    invoke-virtual {p0, p1, p2}, Lbsx;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lbsx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 177
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 240
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-virtual {p0, p1, v0, v1}, Lbsx;->b(Ljava/lang/String;J)V

    .line 241
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 244
    sget-boolean v0, Lbsx;->a:Z

    if-eqz v0, :cond_0

    .line 245
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clearMetadata: key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_0
    invoke-virtual {p0}, Lbsx;->a()V

    .line 250
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->a()V

    .line 252
    :try_start_0
    iget-object v0, p0, Lbsx;->e:Lzr;

    const-string v1, "realtimechat_metadata"

    const-string v2, "key=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->b()V

    .line 257
    iget-object v0, p0, Lbsx;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    iget-object v0, p0, Lbsx;->e:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    .line 260
    return-void

    .line 259
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbsx;->e:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0
.end method
