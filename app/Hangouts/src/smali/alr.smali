.class public final Lalr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcwp;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Lalr;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;B)V
    .locals 0

    .prologue
    .line 447
    invoke-direct {p0, p1}, Lalr;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V

    return-void
.end method


# virtual methods
.method public final a(Lcwq;Lcwc;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 456
    iget-object v0, p0, Lalr;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    :goto_0
    return-void

    .line 459
    :cond_0
    invoke-interface {p2}, Lcwc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    invoke-interface {p2}, Lcwc;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 462
    :goto_1
    check-cast p1, Lals;

    .line 463
    invoke-virtual {v0}, Lyj;->x()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 464
    iget-object v1, p1, Lals;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 468
    :goto_2
    invoke-interface {p2}, Lcwc;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 469
    iget-object v1, p1, Lals;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 470
    iget-object v1, p1, Lals;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 477
    :goto_3
    invoke-static {v0}, Lbkb;->l(Lyj;)Z

    move-result v0

    .line 478
    if-eqz v0, :cond_4

    .line 479
    iget-object v0, p1, Lals;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 480
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->ct:I

    .line 481
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 482
    iget-object v1, p1, Lals;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 483
    iget-object v1, p1, Lals;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 484
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->hm:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lals;->e:Landroid/widget/TextView;

    .line 485
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v5

    .line 484
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 486
    iget-object v1, p1, Lals;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 461
    :cond_1
    invoke-interface {p2}, Lcwc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    goto :goto_1

    .line 466
    :cond_2
    iget-object v1, p1, Lals;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 472
    :cond_3
    iget-object v1, p1, Lals;->c:Landroid/widget/TextView;

    .line 473
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lf;->cu:I

    .line 474
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 472
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 488
    :cond_4
    iget-object v0, p1, Lals;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 489
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cG:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 490
    iget-object v1, p1, Lals;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0
.end method
