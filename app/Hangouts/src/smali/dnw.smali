.class public final Ldnw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnw;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 652
    const/4 v0, 0x0

    new-array v0, v0, [Ldnw;

    sput-object v0, Ldnw;->a:[Ldnw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 653
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 674
    const/4 v0, 0x0

    .line 675
    iget-object v1, p0, Ldnw;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 676
    const/4 v0, 0x1

    iget-object v1, p0, Ldnw;->b:Ljava/lang/Integer;

    .line 677
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 679
    :cond_0
    iget-object v1, p0, Ldnw;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 680
    const/4 v1, 0x2

    iget-object v2, p0, Ldnw;->c:Ljava/lang/Integer;

    .line 681
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 683
    :cond_1
    iget-object v1, p0, Ldnw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 684
    iput v0, p0, Ldnw;->cachedSize:I

    .line 685
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 649
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnw;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnw;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 662
    iget-object v0, p0, Ldnw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 663
    const/4 v0, 0x1

    iget-object v1, p0, Ldnw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 665
    :cond_0
    iget-object v0, p0, Ldnw;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 666
    const/4 v0, 0x2

    iget-object v1, p0, Ldnw;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 668
    :cond_1
    iget-object v0, p0, Ldnw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 670
    return-void
.end method
