.class public final Ldzd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzd;


# instance fields
.field public b:Leir;

.field public c:Leir;

.field public d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598
    const/4 v0, 0x0

    new-array v0, v0, [Ldzd;

    sput-object v0, Ldzd;->a:[Ldzd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 599
    invoke-direct {p0}, Lepn;-><init>()V

    .line 602
    iput-object v0, p0, Ldzd;->b:Leir;

    .line 605
    iput-object v0, p0, Ldzd;->c:Leir;

    .line 599
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 627
    const/4 v0, 0x0

    .line 628
    iget-object v1, p0, Ldzd;->b:Leir;

    if-eqz v1, :cond_0

    .line 629
    const/4 v0, 0x1

    iget-object v1, p0, Ldzd;->b:Leir;

    .line 630
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 632
    :cond_0
    iget-object v1, p0, Ldzd;->c:Leir;

    if-eqz v1, :cond_1

    .line 633
    const/4 v1, 0x2

    iget-object v2, p0, Ldzd;->c:Leir;

    .line 634
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 636
    :cond_1
    iget-object v1, p0, Ldzd;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 637
    const/4 v1, 0x3

    iget-object v2, p0, Ldzd;->d:Ljava/lang/Boolean;

    .line 638
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 640
    :cond_2
    iget-object v1, p0, Ldzd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 641
    iput v0, p0, Ldzd;->cachedSize:I

    .line 642
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 595
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzd;->b:Leir;

    if-nez v0, :cond_2

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Ldzd;->b:Leir;

    :cond_2
    iget-object v0, p0, Ldzd;->b:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldzd;->c:Leir;

    if-nez v0, :cond_3

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Ldzd;->c:Leir;

    :cond_3
    iget-object v0, p0, Ldzd;->c:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldzd;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 612
    iget-object v0, p0, Ldzd;->b:Leir;

    if-eqz v0, :cond_0

    .line 613
    const/4 v0, 0x1

    iget-object v1, p0, Ldzd;->b:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 615
    :cond_0
    iget-object v0, p0, Ldzd;->c:Leir;

    if-eqz v0, :cond_1

    .line 616
    const/4 v0, 0x2

    iget-object v1, p0, Ldzd;->c:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 618
    :cond_1
    iget-object v0, p0, Ldzd;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 619
    const/4 v0, 0x3

    iget-object v1, p0, Ldzd;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 621
    :cond_2
    iget-object v0, p0, Ldzd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 623
    return-void
.end method
