.class public final Lemr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lemr;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Lemq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1604
    const/4 v0, 0x0

    new-array v0, v0, [Lemr;

    sput-object v0, Lemr;->a:[Lemr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1605
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1629
    iput-object v0, p0, Lemr;->d:Ljava/lang/Integer;

    .line 1632
    iput-object v0, p0, Lemr;->e:Lemq;

    .line 1605
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1655
    const/4 v0, 0x0

    .line 1656
    iget-object v1, p0, Lemr;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1657
    const/4 v0, 0x1

    iget-object v1, p0, Lemr;->b:Ljava/lang/Integer;

    .line 1658
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1660
    :cond_0
    iget-object v1, p0, Lemr;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1661
    const/4 v1, 0x2

    iget-object v2, p0, Lemr;->c:Ljava/lang/Integer;

    .line 1662
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1664
    :cond_1
    iget-object v1, p0, Lemr;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1665
    const/4 v1, 0x3

    iget-object v2, p0, Lemr;->d:Ljava/lang/Integer;

    .line 1666
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1668
    :cond_2
    iget-object v1, p0, Lemr;->e:Lemq;

    if-eqz v1, :cond_3

    .line 1669
    const/4 v1, 0x4

    iget-object v2, p0, Lemr;->e:Lemq;

    .line 1670
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1672
    :cond_3
    iget-object v1, p0, Lemr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1673
    iput v0, p0, Lemr;->cachedSize:I

    .line 1674
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1601
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lemr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lemr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lemr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lemr;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lemr;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lemr;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lemr;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lemr;->e:Lemq;

    if-nez v0, :cond_4

    new-instance v0, Lemq;

    invoke-direct {v0}, Lemq;-><init>()V

    iput-object v0, p0, Lemr;->e:Lemq;

    :cond_4
    iget-object v0, p0, Lemr;->e:Lemq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1637
    iget-object v0, p0, Lemr;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1638
    const/4 v0, 0x1

    iget-object v1, p0, Lemr;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1640
    :cond_0
    iget-object v0, p0, Lemr;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1641
    const/4 v0, 0x2

    iget-object v1, p0, Lemr;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1643
    :cond_1
    iget-object v0, p0, Lemr;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1644
    const/4 v0, 0x3

    iget-object v1, p0, Lemr;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1646
    :cond_2
    iget-object v0, p0, Lemr;->e:Lemq;

    if-eqz v0, :cond_3

    .line 1647
    const/4 v0, 0x4

    iget-object v1, p0, Lemr;->e:Lemq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1649
    :cond_3
    iget-object v0, p0, Lemr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1651
    return-void
.end method
