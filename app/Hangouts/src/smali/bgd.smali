.class public Lbgd;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:[B

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbcw;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Z


# direct methods
.method private constructor <init>(Ldqo;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 440
    iget-object v1, p1, Ldqo;->b:Ldvn;

    const-wide/16 v2, -0x1

    invoke-direct {p0, v1, v2, v3}, Lbfz;-><init>(Ldvn;J)V

    .line 441
    iget-object v1, p1, Ldqo;->d:Ldqa;

    if-eqz v1, :cond_2

    .line 442
    iget-object v1, p1, Ldqo;->d:Ldqa;

    invoke-static {v1}, Lepr;->toByteArray(Lepr;)[B

    move-result-object v1

    iput-object v1, p0, Lbgd;->g:[B

    .line 446
    :goto_0
    iget-object v1, p1, Ldqo;->e:[Ldtg;

    invoke-static {v1}, Lbcw;->a([Ldtg;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lbgd;->h:Ljava/util/List;

    .line 451
    iget-object v1, p1, Ldqo;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v1, p1, Ldqo;->f:Ljava/lang/Boolean;

    .line 452
    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lbgd;->i:Z

    .line 453
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 454
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CreateConversationResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    :cond_1
    return-void

    .line 444
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lbgd;->g:[B

    goto :goto_0
.end method

.method private f()Lbjc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 472
    :try_start_0
    iget-object v0, p0, Lbgd;->g:[B

    if-nez v0, :cond_0

    move-object v0, v1

    .line 480
    :goto_0
    return-object v0

    .line 475
    :cond_0
    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iget-object v2, p0, Lbgd;->g:[B

    invoke-static {v0, v2}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldqa;

    .line 477
    new-instance v2, Lbjc;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lbjc;-><init>(Ldqa;B)V
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    goto :goto_0

    .line 480
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 460
    new-instance v0, Ldqo;

    invoke-direct {v0}, Ldqo;-><init>()V

    .line 461
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldqo;

    .line 462
    iget-object v1, v0, Ldqo;->b:Ldvn;

    invoke-static {v1}, Lbgd;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 463
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldqo;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 465
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgd;

    invoke-direct {v1, v0}, Lbgd;-><init>(Ldqo;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 13

    .prologue
    const-wide/16 v9, 0x0

    .line 491
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 492
    invoke-direct {p0}, Lbgd;->f()Lbjc;

    move-result-object v1

    .line 493
    iget-object v0, p0, Lbgd;->b:Lbea;

    check-cast v0, Lbee;

    iget-object v4, v0, Lbee;->h:Ljava/lang/String;

    .line 495
    const-string v0, "Babel"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processCreateConversationResponse requestClientGeneratedId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " clientGeneratedId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lbjc;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " conversationId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 500
    invoke-virtual {v1}, Lbjc;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 496
    invoke-static {v0, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :cond_0
    invoke-virtual {p1}, Lyt;->a()V

    .line 505
    :try_start_0
    iget-object v0, p0, Lbgd;->c:Lbht;

    iget v0, v0, Lbht;->b:I

    .line 514
    new-instance v6, Lys;

    invoke-direct {v6}, Lys;-><init>()V

    .line 521
    const/4 v2, 0x6

    if-ne v0, v2, :cond_1

    .line 522
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v7, 0x3e8

    mul-long/2addr v2, v7

    .line 523
    iget-wide v7, v1, Lbjc;->t:J

    cmp-long v0, v7, v9

    if-nez v0, :cond_5

    :goto_0
    invoke-virtual {v1, v2, v3}, Lbjc;->a(J)V

    .line 527
    :cond_1
    const-wide/16 v2, 0x0

    const/4 v7, 0x1

    move-object v0, p1

    move-object v5, p2

    invoke-static/range {v0 .. v7}, Lyp;->a(Lyt;Lbjc;JLjava/lang/String;Lbnl;Lys;Z)Z

    .line 530
    iget-object v2, v6, Lys;->a:Ljava/lang/String;

    .line 532
    iget-object v0, p0, Lbgd;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 534
    iget-wide v0, v1, Lbjc;->t:J

    const-wide/16 v3, 0x1

    add-long v4, v0, v3

    .line 536
    iget-object v1, p0, Lbgd;->h:Ljava/util/List;

    .line 537
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v6, -0x1

    move-object v0, p1

    .line 536
    invoke-static/range {v0 .. v7}, Lyp;->a(Lyt;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 541
    :cond_2
    iget-boolean v0, p0, Lbgd;->i:Z

    if-eqz v0, :cond_3

    .line 543
    new-instance v1, Lbek;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    invoke-direct/range {v1 .. v12}, Lbek;-><init>(Ljava/lang/String;ZZZ[BJLjava/lang/String;JLbjg;)V

    invoke-virtual {p2, v1}, Lbnl;->a(Lbea;)V

    .line 555
    :cond_3
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    invoke-virtual {p1}, Lyt;->c()V

    .line 559
    if-eqz v2, :cond_4

    .line 560
    invoke-static {p1, v2}, Lyp;->c(Lyt;Ljava/lang/String;)V

    .line 561
    invoke-static {p1, v2}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 563
    :cond_4
    return-void

    :cond_5
    move-wide v2, v7

    .line 523
    goto :goto_0

    .line 557
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
.end method
