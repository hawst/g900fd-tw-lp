.class public final Ldek;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldek;


# instance fields
.field public b:Ldfj;

.field public c:Ldfl;

.field public d:Ldeo;

.field public e:Ldgc;

.field public f:Ldgc;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Boolean;

.field public i:Ldfc;

.field public j:Ldfv;

.field public k:Ldft;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6937
    const/4 v0, 0x0

    new-array v0, v0, [Ldek;

    sput-object v0, Ldek;->a:[Ldek;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6938
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6941
    iput-object v0, p0, Ldek;->b:Ldfj;

    .line 6944
    iput-object v0, p0, Ldek;->c:Ldfl;

    .line 6947
    iput-object v0, p0, Ldek;->d:Ldeo;

    .line 6950
    iput-object v0, p0, Ldek;->e:Ldgc;

    .line 6953
    iput-object v0, p0, Ldek;->f:Ldgc;

    .line 6960
    iput-object v0, p0, Ldek;->i:Ldfc;

    .line 6963
    iput-object v0, p0, Ldek;->j:Ldfv;

    .line 6966
    iput-object v0, p0, Ldek;->k:Ldft;

    .line 6938
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 7007
    const/4 v0, 0x0

    .line 7008
    iget-object v1, p0, Ldek;->c:Ldfl;

    if-eqz v1, :cond_0

    .line 7009
    const/4 v0, 0x1

    iget-object v1, p0, Ldek;->c:Ldfl;

    .line 7010
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7012
    :cond_0
    iget-object v1, p0, Ldek;->d:Ldeo;

    if-eqz v1, :cond_1

    .line 7013
    const/4 v1, 0x2

    iget-object v2, p0, Ldek;->d:Ldeo;

    .line 7014
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7016
    :cond_1
    iget-object v1, p0, Ldek;->e:Ldgc;

    if-eqz v1, :cond_2

    .line 7017
    const/4 v1, 0x3

    iget-object v2, p0, Ldek;->e:Ldgc;

    .line 7018
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7020
    :cond_2
    iget-object v1, p0, Ldek;->f:Ldgc;

    if-eqz v1, :cond_3

    .line 7021
    const/4 v1, 0x4

    iget-object v2, p0, Ldek;->f:Ldgc;

    .line 7022
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7024
    :cond_3
    iget-object v1, p0, Ldek;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 7025
    const/4 v1, 0x5

    iget-object v2, p0, Ldek;->g:Ljava/lang/String;

    .line 7026
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7028
    :cond_4
    iget-object v1, p0, Ldek;->i:Ldfc;

    if-eqz v1, :cond_5

    .line 7029
    const/4 v1, 0x6

    iget-object v2, p0, Ldek;->i:Ldfc;

    .line 7030
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7032
    :cond_5
    iget-object v1, p0, Ldek;->j:Ldfv;

    if-eqz v1, :cond_6

    .line 7033
    const/4 v1, 0x7

    iget-object v2, p0, Ldek;->j:Ldfv;

    .line 7034
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7036
    :cond_6
    iget-object v1, p0, Ldek;->k:Ldft;

    if-eqz v1, :cond_7

    .line 7037
    const/16 v1, 0x8

    iget-object v2, p0, Ldek;->k:Ldft;

    .line 7038
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7040
    :cond_7
    iget-object v1, p0, Ldek;->b:Ldfj;

    if-eqz v1, :cond_8

    .line 7041
    const/16 v1, 0x9

    iget-object v2, p0, Ldek;->b:Ldfj;

    .line 7042
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7044
    :cond_8
    iget-object v1, p0, Ldek;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 7045
    const/16 v1, 0xa

    iget-object v2, p0, Ldek;->h:Ljava/lang/Boolean;

    .line 7046
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7048
    :cond_9
    iget-object v1, p0, Ldek;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7049
    iput v0, p0, Ldek;->cachedSize:I

    .line 7050
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6934
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldek;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldek;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldek;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldek;->c:Ldfl;

    if-nez v0, :cond_2

    new-instance v0, Ldfl;

    invoke-direct {v0}, Ldfl;-><init>()V

    iput-object v0, p0, Ldek;->c:Ldfl;

    :cond_2
    iget-object v0, p0, Ldek;->c:Ldfl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldek;->d:Ldeo;

    if-nez v0, :cond_3

    new-instance v0, Ldeo;

    invoke-direct {v0}, Ldeo;-><init>()V

    iput-object v0, p0, Ldek;->d:Ldeo;

    :cond_3
    iget-object v0, p0, Ldek;->d:Ldeo;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldek;->e:Ldgc;

    if-nez v0, :cond_4

    new-instance v0, Ldgc;

    invoke-direct {v0}, Ldgc;-><init>()V

    iput-object v0, p0, Ldek;->e:Ldgc;

    :cond_4
    iget-object v0, p0, Ldek;->e:Ldgc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldek;->f:Ldgc;

    if-nez v0, :cond_5

    new-instance v0, Ldgc;

    invoke-direct {v0}, Ldgc;-><init>()V

    iput-object v0, p0, Ldek;->f:Ldgc;

    :cond_5
    iget-object v0, p0, Ldek;->f:Ldgc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldek;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldek;->i:Ldfc;

    if-nez v0, :cond_6

    new-instance v0, Ldfc;

    invoke-direct {v0}, Ldfc;-><init>()V

    iput-object v0, p0, Ldek;->i:Ldfc;

    :cond_6
    iget-object v0, p0, Ldek;->i:Ldfc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldek;->j:Ldfv;

    if-nez v0, :cond_7

    new-instance v0, Ldfv;

    invoke-direct {v0}, Ldfv;-><init>()V

    iput-object v0, p0, Ldek;->j:Ldfv;

    :cond_7
    iget-object v0, p0, Ldek;->j:Ldfv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldek;->k:Ldft;

    if-nez v0, :cond_8

    new-instance v0, Ldft;

    invoke-direct {v0}, Ldft;-><init>()V

    iput-object v0, p0, Ldek;->k:Ldft;

    :cond_8
    iget-object v0, p0, Ldek;->k:Ldft;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldek;->b:Ldfj;

    if-nez v0, :cond_9

    new-instance v0, Ldfj;

    invoke-direct {v0}, Ldfj;-><init>()V

    iput-object v0, p0, Ldek;->b:Ldfj;

    :cond_9
    iget-object v0, p0, Ldek;->b:Ldfj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldek;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 6971
    iget-object v0, p0, Ldek;->c:Ldfl;

    if-eqz v0, :cond_0

    .line 6972
    const/4 v0, 0x1

    iget-object v1, p0, Ldek;->c:Ldfl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6974
    :cond_0
    iget-object v0, p0, Ldek;->d:Ldeo;

    if-eqz v0, :cond_1

    .line 6975
    const/4 v0, 0x2

    iget-object v1, p0, Ldek;->d:Ldeo;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6977
    :cond_1
    iget-object v0, p0, Ldek;->e:Ldgc;

    if-eqz v0, :cond_2

    .line 6978
    const/4 v0, 0x3

    iget-object v1, p0, Ldek;->e:Ldgc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6980
    :cond_2
    iget-object v0, p0, Ldek;->f:Ldgc;

    if-eqz v0, :cond_3

    .line 6981
    const/4 v0, 0x4

    iget-object v1, p0, Ldek;->f:Ldgc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6983
    :cond_3
    iget-object v0, p0, Ldek;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 6984
    const/4 v0, 0x5

    iget-object v1, p0, Ldek;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6986
    :cond_4
    iget-object v0, p0, Ldek;->i:Ldfc;

    if-eqz v0, :cond_5

    .line 6987
    const/4 v0, 0x6

    iget-object v1, p0, Ldek;->i:Ldfc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6989
    :cond_5
    iget-object v0, p0, Ldek;->j:Ldfv;

    if-eqz v0, :cond_6

    .line 6990
    const/4 v0, 0x7

    iget-object v1, p0, Ldek;->j:Ldfv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6992
    :cond_6
    iget-object v0, p0, Ldek;->k:Ldft;

    if-eqz v0, :cond_7

    .line 6993
    const/16 v0, 0x8

    iget-object v1, p0, Ldek;->k:Ldft;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6995
    :cond_7
    iget-object v0, p0, Ldek;->b:Ldfj;

    if-eqz v0, :cond_8

    .line 6996
    const/16 v0, 0x9

    iget-object v1, p0, Ldek;->b:Ldfj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6998
    :cond_8
    iget-object v0, p0, Ldek;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 6999
    const/16 v0, 0xa

    iget-object v1, p0, Ldek;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7001
    :cond_9
    iget-object v0, p0, Ldek;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7003
    return-void
.end method
