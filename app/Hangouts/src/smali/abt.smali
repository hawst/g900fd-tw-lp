.class public final Labt;
.super Lack;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lack",
        "<",
        "Lbde;",
        "Lbhi;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Ly;

.field private final e:Lyj;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Z


# direct methods
.method public constructor <init>(Ly;Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lack;-><init>()V

    .line 40
    iput-object p1, p0, Labt;->d:Ly;

    .line 41
    iput-object p2, p0, Labt;->e:Lyj;

    .line 42
    iput-object p3, p0, Labt;->f:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Labt;->g:Ljava/lang/String;

    .line 44
    iput-object p5, p0, Labt;->h:Ljava/lang/String;

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Labt;->i:Z

    .line 46
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    iget-object v0, p0, Labt;->d:Ly;

    invoke-virtual {v0}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->J:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Labt;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 55
    iget-object v0, p0, Labt;->e:Lyj;

    iget-object v1, p0, Labt;->g:Ljava/lang/String;

    iget-object v2, p0, Labt;->h:Ljava/lang/String;

    iget-object v3, p0, Labt;->f:Ljava/lang/String;

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 6

    .prologue
    .line 61
    invoke-static {}, Lack;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lh;->H:I

    .line 63
    :goto_0
    iget-object v1, p0, Labt;->d:Ly;

    iget-object v2, p0, Labt;->d:Ly;

    invoke-virtual {v2}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Labt;->f:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 64
    return-void

    .line 61
    :cond_0
    sget v0, Lh;->I:I

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 68
    iget-boolean v0, p0, Labt;->i:Z

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Labt;->d:Ly;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ly;->setResult(I)V

    .line 70
    iget-object v0, p0, Labt;->d:Ly;

    invoke-virtual {v0}, Ly;->finish()V

    .line 72
    :cond_0
    return-void
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbde;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    const-class v0, Lbde;

    return-object v0
.end method

.method public f()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbhi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    const-class v0, Lbhi;

    return-object v0
.end method
