.class public Lbda;
.super Lbcz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 356
    invoke-direct {p0}, Lbcz;-><init>()V

    .line 357
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 358
    iput-object p1, p0, Lbda;->a:Ljava/lang/String;

    .line 359
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 363
    new-instance v0, Leod;

    invoke-direct {v0}, Leod;-><init>()V

    .line 364
    new-instance v1, Lemz;

    invoke-direct {v1}, Lemz;-><init>()V

    .line 365
    iget-object v2, p0, Lbda;->a:Ljava/lang/String;

    iput-object v2, v1, Lemz;->b:Ljava/lang/String;

    .line 366
    new-instance v2, Leoc;

    invoke-direct {v2}, Leoc;-><init>()V

    .line 367
    new-array v3, v5, [Lemz;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    iput-object v3, v2, Leoc;->b:[Lemz;

    .line 368
    iput-object v2, v0, Leod;->b:Leoc;

    .line 370
    new-instance v1, Lekm;

    invoke-direct {v1}, Lekm;-><init>()V

    .line 371
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lekm;->b:Ljava/lang/Boolean;

    .line 372
    new-instance v2, Lekl;

    invoke-direct {v2}, Lekl;-><init>()V

    .line 373
    iput-object v1, v2, Lekl;->c:Lekm;

    .line 374
    new-instance v1, Lekk;

    invoke-direct {v1}, Lekk;-><init>()V

    .line 375
    iput-object v2, v1, Lekk;->c:Lekl;

    .line 376
    new-instance v2, Lenh;

    invoke-direct {v2}, Lenh;-><init>()V

    .line 377
    iput-object v1, v2, Lenh;->e:Lekk;

    .line 378
    new-instance v1, Leni;

    invoke-direct {v1}, Leni;-><init>()V

    .line 379
    iput-object v2, v1, Leni;->d:Lenh;

    .line 380
    iput-object v1, v0, Leod;->c:Leni;

    .line 383
    new-instance v1, Lddh;

    invoke-direct {v1}, Lddh;-><init>()V

    .line 385
    iput-object v0, v1, Lddh;->c:Leod;

    .line 386
    return-object v1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    const-string v0, "readitemsbyid"

    return-object v0
.end method
