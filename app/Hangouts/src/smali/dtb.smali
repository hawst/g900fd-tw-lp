.class public final Ldtb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtb;


# instance fields
.field public b:Ldps;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16100
    const/4 v0, 0x0

    new-array v0, v0, [Ldtb;

    sput-object v0, Ldtb;->a:[Ldtb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16101
    invoke-direct {p0}, Lepn;-><init>()V

    .line 16104
    iput-object v0, p0, Ldtb;->b:Ldps;

    .line 16107
    iput-object v0, p0, Ldtb;->c:Ljava/lang/Integer;

    .line 16101
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 16129
    const/4 v0, 0x0

    .line 16130
    iget-object v1, p0, Ldtb;->b:Ldps;

    if-eqz v1, :cond_0

    .line 16131
    const/4 v0, 0x1

    iget-object v1, p0, Ldtb;->b:Ldps;

    .line 16132
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16134
    :cond_0
    iget-object v1, p0, Ldtb;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 16135
    const/4 v1, 0x2

    iget-object v2, p0, Ldtb;->c:Ljava/lang/Integer;

    .line 16136
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16138
    :cond_1
    iget-object v1, p0, Ldtb;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 16139
    const/4 v1, 0x3

    iget-object v2, p0, Ldtb;->d:Ljava/lang/Long;

    .line 16140
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16142
    :cond_2
    iget-object v1, p0, Ldtb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16143
    iput v0, p0, Ldtb;->cachedSize:I

    .line 16144
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 16097
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldtb;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldtb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldtb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldtb;->b:Ldps;

    if-nez v0, :cond_2

    new-instance v0, Ldps;

    invoke-direct {v0}, Ldps;-><init>()V

    iput-object v0, p0, Ldtb;->b:Ldps;

    :cond_2
    iget-object v0, p0, Ldtb;->b:Ldps;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/16 v1, 0x64

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtb;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtb;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldtb;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 16114
    iget-object v0, p0, Ldtb;->b:Ldps;

    if-eqz v0, :cond_0

    .line 16115
    const/4 v0, 0x1

    iget-object v1, p0, Ldtb;->b:Ldps;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16117
    :cond_0
    iget-object v0, p0, Ldtb;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 16118
    const/4 v0, 0x2

    iget-object v1, p0, Ldtb;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 16120
    :cond_1
    iget-object v0, p0, Ldtb;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 16121
    const/4 v0, 0x3

    iget-object v1, p0, Ldtb;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 16123
    :cond_2
    iget-object v0, p0, Ldtb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 16125
    return-void
.end method
