.class public abstract Latu;
.super Late;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SelfRendererType:",
        "Lcom/google/android/libraries/hangouts/video/SelfRenderer;",
        ">",
        "Late;"
    }
.end annotation


# instance fields
.field protected final E:Landroid/view/WindowManager;

.field protected volatile F:I

.field protected final G:Latz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Latu",
            "<TSelfRendererType;>.atz;"
        }
    .end annotation
.end field

.field protected H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSelfRendererType;"
        }
    .end annotation
.end field

.field protected I:I

.field protected J:Z

.field protected K:I

.field protected L:I

.field protected M:I

.field private final N:Latw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Latu",
            "<TSelfRendererType;>.atw;"
        }
    .end annotation
.end field

.field private volatile O:Z

.field private final P:Lauj;

.field private final Q:Lapa;

.field private final R:Latx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Latu",
            "<TSelfRendererType;>.atx;"
        }
    .end annotation
.end field

.field private S:Z

.field private volatile T:Z

.field private volatile U:Z


# direct methods
.method public constructor <init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 300
    invoke-direct/range {p0 .. p8}, Late;-><init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V

    .line 265
    new-instance v0, Latw;

    invoke-direct {v0, p0, v1}, Latw;-><init>(Latu;B)V

    iput-object v0, p0, Latu;->N:Latw;

    .line 272
    iput v1, p0, Latu;->I:I

    .line 278
    iput v1, p0, Latu;->K:I

    .line 285
    new-instance v0, Latx;

    invoke-direct {v0, p0, v1}, Latx;-><init>(Latu;B)V

    iput-object v0, p0, Latu;->R:Latx;

    .line 289
    iput-boolean v1, p0, Latu;->T:Z

    .line 302
    invoke-virtual {p1}, Latd;->g()Lauj;

    move-result-object v0

    iput-object v0, p0, Latu;->P:Lauj;

    .line 303
    invoke-virtual {p0}, Latu;->z()Latz;

    move-result-object v0

    iput-object v0, p0, Latu;->G:Latz;

    .line 304
    invoke-virtual {p1}, Latd;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Latu;->E:Landroid/view/WindowManager;

    .line 306
    new-instance v0, Lapa;

    invoke-direct {v0}, Lapa;-><init>()V

    iput-object v0, p0, Latu;->Q:Lapa;

    .line 308
    iget-object v0, p0, Latu;->B:Latg;

    iget-object v1, p0, Latu;->e:Latd;

    .line 309
    invoke-virtual {v1}, Latd;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->fp:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 308
    invoke-virtual {v0, v1}, Latg;->a(Ljava/lang/String;)V

    .line 310
    return-void
.end method

.method private C()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 537
    invoke-virtual {p0, v4}, Latu;->a(I)V

    .line 540
    iget-object v0, p0, Latu;->w:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 557
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    iget-object v0, p0, Latu;->e:Latd;

    invoke-virtual {v0}, Latd;->c()Lapv;

    move-result-object v0

    invoke-virtual {v0}, Lapv;->a()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->F()Ljava/lang/String;

    move-result-object v0

    .line 545
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 546
    iget-object v1, p0, Latu;->e:Latd;

    invoke-virtual {v1}, Latd;->c()Lapv;

    move-result-object v1

    invoke-virtual {v1}, Lapv;->a()Lyj;

    move-result-object v1

    .line 547
    new-instance v2, Lzx;

    new-instance v3, Lbyq;

    invoke-direct {v3, v0, v1}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 549
    invoke-static {}, Lyn;->f()I

    move-result v0

    invoke-virtual {v3, v0}, Lbyq;->a(I)Lbyq;

    move-result-object v0

    .line 550
    invoke-virtual {v0, v4}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    new-instance v1, Latv;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Latv;-><init>(Latu;B)V

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v4, v3}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    .line 555
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbsn;->a(Lbrv;)Z

    goto :goto_0
.end method

.method private D()V
    .locals 4

    .prologue
    .line 593
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Latu;->a(I)V

    .line 594
    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getCurrentCameraId()I

    move-result v0

    .line 595
    iget-object v1, p0, Latu;->Q:Lapa;

    invoke-static {v0}, Lapa;->b(I)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v1

    .line 596
    iget-object v2, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    new-instance v3, Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    invoke-direct {v3, v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraSpecification;-><init>(ILcom/google/android/libraries/hangouts/video/Size;)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->useCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    .line 597
    const/4 v0, 0x0

    invoke-static {v0}, Latu;->d(Z)V

    .line 598
    return-void
.end method

.method private E()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 609
    invoke-virtual {p0}, Latu;->A()Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    move-result-object v0

    .line 610
    if-eqz v0, :cond_1

    .line 611
    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->suspendCamera()V

    .line 614
    iget-object v0, p0, Latu;->d:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 615
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lapx;->N()Z

    move-result v0

    if-nez v0, :cond_1

    .line 616
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Latu;->d(Z)V

    .line 617
    invoke-direct {p0}, Latu;->C()V

    .line 621
    :cond_1
    iput-boolean v1, p0, Latu;->J:Z

    .line 622
    iput-boolean v1, p0, Latu;->U:Z

    .line 623
    return-void
.end method

.method private F()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 636
    iget-object v0, p0, Latu;->e:Latd;

    invoke-virtual {v0}, Latd;->c()Lapv;

    move-result-object v5

    .line 637
    invoke-virtual {v5}, Lapv;->h()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    .line 639
    :goto_0
    invoke-virtual {v5}, Lapv;->h()I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    move v3, v1

    .line 642
    :goto_1
    iget-object v4, p0, Latu;->d:Lapk;

    invoke-virtual {v4}, Lapk;->d()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 643
    iget-object v4, p0, Latu;->d:Lapk;

    invoke-virtual {v4}, Lapk;->d()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v4

    .line 645
    :goto_2
    iget-object v6, p0, Latu;->j:Ljava/lang/Object;

    monitor-enter v6

    .line 646
    if-nez v0, :cond_0

    .line 647
    :try_start_0
    invoke-virtual {v5}, Lapv;->c()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 649
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Latu;->T:Z

    .line 657
    :goto_3
    monitor-exit v6

    return-void

    :cond_1
    move v0, v2

    .line 637
    goto :goto_0

    :cond_2
    move v3, v2

    .line 639
    goto :goto_1

    .line 650
    :cond_3
    if-eqz v3, :cond_5

    .line 652
    if-le v4, v1, :cond_4

    :goto_4
    iput-boolean v1, p0, Latu;->T:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 657
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_4
    move v1, v2

    .line 652
    goto :goto_4

    .line 655
    :cond_5
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Latu;->T:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :cond_6
    move v4, v2

    goto :goto_2
.end method

.method static synthetic a(Latu;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Latu;->F()V

    return-void
.end method

.method static synthetic a(Latu;Z)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Latu;->e:Latd;

    invoke-virtual {v0}, Latd;->h()Laqv;

    move-result-object v0

    invoke-virtual {v0, p1}, Laqv;->c(Z)V

    return-void
.end method

.method static synthetic b(Latu;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Latu;->S:Z

    return v0
.end method

.method static synthetic b(Latu;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Latu;->S:Z

    return p1
.end method

.method static synthetic c(Latu;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 55
    iget v0, p0, Latu;->p:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Latu;->d:Lapk;

    invoke-virtual {v3}, Lapk;->c()Lapx;

    move-result-object v4

    iget-boolean v3, p0, Latu;->U:Z

    if-eqz v3, :cond_2

    move v1, v0

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lapx;->J()I

    move-result v3

    if-ne v3, v1, :cond_3

    if-nez v0, :cond_0

    :cond_3
    iput-boolean v1, p0, Latu;->U:Z

    if-eqz v0, :cond_5

    invoke-direct {p0}, Latu;->D()V

    :goto_2
    if-eqz v4, :cond_4

    if-nez v0, :cond_6

    move v3, v1

    :goto_3
    invoke-virtual {v4, v3}, Lapx;->f(Z)V

    :cond_4
    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_1

    :cond_5
    invoke-direct {p0}, Latu;->E()V

    goto :goto_2

    :cond_6
    move v3, v2

    goto :goto_3
.end method

.method static synthetic c(Latu;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Latu;->U:Z

    return p1
.end method

.method static synthetic d(Latu;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Latu;->E()V

    return-void
.end method

.method private static d(Z)V
    .locals 1

    .prologue
    .line 628
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 629
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->isMediaConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->publishVideoMuteState(Z)V

    .line 632
    :cond_0
    return-void
.end method

.method static synthetic e(Latu;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Latu;->U:Z

    return v0
.end method

.method static synthetic f(Latu;)V
    .locals 4

    .prologue
    .line 55
    iget-object v0, p0, Latu;->Q:Lapa;

    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getCurrentCameraId()I

    move-result v0

    invoke-static {v0}, Lapa;->a(I)I

    move-result v0

    iget-object v1, p0, Latu;->Q:Lapa;

    invoke-static {v0}, Lapa;->b(I)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v1

    iget-object v2, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    new-instance v3, Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    invoke-direct {v3, v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraSpecification;-><init>(ILcom/google/android/libraries/hangouts/video/Size;)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->useCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    return-void
.end method


# virtual methods
.method public A()Lcom/google/android/libraries/hangouts/video/SelfRenderer;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    return-object v0
.end method

.method protected B()Lcom/google/android/libraries/hangouts/video/CameraSpecification;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 324
    iget-object v1, p0, Latu;->Q:Lapa;

    if-nez v1, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-object v0

    .line 327
    :cond_1
    iget-object v1, p0, Latu;->Q:Lapa;

    sget-object v1, Lapa;->a:[I

    array-length v1, v1

    if-nez v1, :cond_2

    move v1, v2

    .line 328
    :goto_1
    if-eq v1, v2, :cond_0

    .line 331
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    iget-object v2, p0, Latu;->Q:Lapa;

    .line 332
    invoke-static {v1}, Lapa;->b(I)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CameraSpecification;-><init>(ILcom/google/android/libraries/hangouts/video/Size;)V

    goto :goto_0

    .line 327
    :cond_2
    sget-object v1, Lapa;->a:[I

    const/4 v3, 0x0

    aget v1, v1, v3

    goto :goto_1
.end method

.method protected a(Landroid/content/Context;)Latg;
    .locals 1

    .prologue
    .line 337
    new-instance v0, Laty;

    invoke-direct {v0, p0, p1}, Laty;-><init>(Latu;Landroid/content/Context;)V

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 346
    invoke-super {p0}, Late;->a()V

    .line 348
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_1

    .line 350
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->EARPIECE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Latu;->S:Z

    .line 352
    iget-object v0, p0, Latu;->e:Latd;

    invoke-virtual {v0}, Latd;->c()Lapv;

    move-result-object v0

    invoke-virtual {v0}, Lapv;->c()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 353
    iget-object v0, p0, Latu;->d:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 354
    invoke-virtual {v0}, Lapx;->L()Z

    move-result v0

    .line 355
    iget-object v1, p0, Latu;->e:Latd;

    invoke-virtual {v1}, Latd;->h()Laqv;

    move-result-object v1

    invoke-virtual {v1, v0}, Laqv;->c(Z)V

    .line 359
    :cond_0
    invoke-direct {p0}, Latu;->C()V

    .line 361
    iget-object v0, p0, Latu;->d:Lapk;

    iget-object v1, p0, Latu;->N:Latw;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 363
    iget-object v0, p0, Latu;->E:Landroid/view/WindowManager;

    .line 364
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 363
    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->b(I)I

    move-result v0

    iput v0, p0, Latu;->F:I

    .line 365
    iget-object v0, p0, Latu;->e:Latd;

    invoke-virtual {v0}, Latd;->h()Laqv;

    move-result-object v0

    iget-object v1, p0, Latu;->R:Latx;

    invoke-virtual {v0, v1}, Laqv;->a(Laro;)V

    .line 366
    return-void

    .line 350
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(I)V
    .locals 7

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 521
    invoke-static {}, Lcwz;->a()V

    .line 522
    invoke-static {p1, v0, v1}, Lcwz;->a(III)V

    .line 523
    iget-object v1, p0, Latu;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 524
    :try_start_0
    const-string v0, "Babel"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    const-string v0, "Babel"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "SelfGLArea#setMode: this=%s old=%d new=%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Latu;->p:I

    .line 526
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 525
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    :cond_0
    iget v0, p0, Latu;->p:I

    if-eq v0, p1, :cond_1

    .line 529
    iput p1, p0, Latu;->p:I

    .line 530
    iget-object v0, p0, Latu;->g:Lasx;

    invoke-virtual {v0}, Lasx;->e()V

    .line 532
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 416
    invoke-super {p0, p1}, Late;->a(Landroid/content/res/Configuration;)V

    .line 417
    iget-object v0, p0, Latu;->E:Landroid/view/WindowManager;

    .line 418
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 417
    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->b(I)I

    move-result v0

    .line 419
    iget v1, p0, Latu;->F:I

    if-eq v0, v1, :cond_0

    .line 420
    iget-object v1, p0, Latu;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 421
    :try_start_0
    iput v0, p0, Latu;->F:I

    .line 423
    const/4 v0, 0x1

    iput-boolean v0, p0, Latu;->O:Z

    .line 424
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    :cond_0
    return-void

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 0

    .prologue
    .line 511
    iput-object p1, p0, Latu;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 512
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 402
    invoke-super {p0}, Late;->b()V

    .line 403
    iget-object v0, p0, Latu;->d:Lapk;

    iget-object v1, p0, Latu;->N:Latw;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 404
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 371
    invoke-super {p0}, Late;->h()V

    .line 373
    invoke-direct {p0}, Latu;->F()V

    .line 375
    iget-object v2, p0, Latu;->d:Lapk;

    invoke-virtual {v2}, Lapk;->c()Lapx;

    move-result-object v2

    .line 378
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lapx;->I()Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v0

    .line 379
    :goto_0
    if-eqz v2, :cond_1

    .line 380
    invoke-virtual {v2}, Lapx;->J()I

    move-result v2

    if-ne v2, v0, :cond_1

    move v2, v0

    .line 381
    :goto_1
    if-nez v3, :cond_2

    iget-boolean v3, p0, Latu;->S:Z

    if-nez v3, :cond_2

    if-nez v2, :cond_2

    move v2, v0

    .line 382
    :goto_2
    if-eqz v2, :cond_3

    .line 383
    invoke-direct {p0}, Latu;->D()V

    .line 385
    invoke-static {v1}, Latu;->d(Z)V

    .line 389
    :goto_3
    iget-object v3, p0, Latu;->e:Latd;

    invoke-virtual {v3}, Latd;->h()Laqv;

    move-result-object v3

    if-nez v2, :cond_4

    :goto_4
    invoke-virtual {v3, v0}, Laqv;->d(Z)V

    .line 390
    return-void

    :cond_0
    move v3, v1

    .line 378
    goto :goto_0

    :cond_1
    move v2, v1

    .line 380
    goto :goto_1

    :cond_2
    move v2, v1

    .line 381
    goto :goto_2

    .line 387
    :cond_3
    invoke-direct {p0}, Latu;->C()V

    goto :goto_3

    :cond_4
    move v0, v1

    .line 389
    goto :goto_4
.end method

.method public i()V
    .locals 0

    .prologue
    .line 395
    invoke-super {p0}, Late;->i()V

    .line 396
    invoke-direct {p0}, Latu;->E()V

    .line 397
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 409
    invoke-super {p0}, Late;->j()V

    .line 410
    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->release()V

    .line 411
    return-void
.end method

.method o()Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 499
    iget-object v1, p0, Latu;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 500
    :try_start_0
    iget-boolean v0, p0, Latu;->T:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Latu;->n:Z

    if-eqz v0, :cond_1

    .line 501
    iget v0, p0, Latu;->p:I

    if-ne v0, v2, :cond_0

    iget v0, p0, Latu;->q:I

    if-eq v0, v2, :cond_1

    .line 502
    :cond_0
    iget-object v0, p0, Latu;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 503
    iget-object v0, p0, Latu;->h:Laui;

    iget-object v2, p0, Latu;->o:Landroid/graphics/Rect;

    iget v3, p0, Latu;->l:F

    invoke-virtual {v0, v2, v3}, Laui;->a(Landroid/graphics/Rect;F)V

    .line 507
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public r()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 444
    iget-object v1, p0, Latu;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 445
    :try_start_0
    iget-boolean v0, p0, Latu;->i:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Latu;->q()V

    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->initializeGLContext()V

    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getOutputTextureName()I

    move-result v0

    iput v0, p0, Latu;->I:I

    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    iget v2, p0, Latu;->F:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->setDeviceOrientation(I)V

    .line 447
    :cond_0
    iget-boolean v0, p0, Latu;->O:Z

    if-eqz v0, :cond_1

    .line 448
    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    iget v2, p0, Latu;->F:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->setDeviceOrientation(I)V

    .line 449
    const/4 v0, 0x0

    iput-boolean v0, p0, Latu;->O:Z

    .line 452
    :cond_1
    iget v0, p0, Latu;->p:I

    if-ne v0, v4, :cond_4

    iget-boolean v0, p0, Latu;->J:Z

    if-eqz v0, :cond_4

    .line 453
    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z

    move-result v0

    .line 454
    if-eqz v0, :cond_4

    .line 457
    iget-object v0, p0, Latu;->P:Lauj;

    invoke-virtual {v0}, Lauj;->isAnimating()Z

    move-result v0

    if-nez v0, :cond_2

    .line 458
    iget-object v0, p0, Latu;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->encodeFrame()V

    .line 461
    :cond_2
    iget v0, p0, Latu;->q:I

    if-eq v0, v4, :cond_3

    .line 462
    const/4 v0, 0x2

    iput v0, p0, Latu;->q:I

    .line 463
    iget-object v0, p0, Latu;->h:Laui;

    iget-boolean v2, p0, Latu;->v:Z

    invoke-virtual {v0, v2}, Laui;->a(Z)V

    .line 466
    :cond_3
    iget v0, p0, Latu;->K:I

    iget v2, p0, Latu;->L:I

    iget v3, p0, Latu;->M:I

    invoke-virtual {p0, v0, v2, v3}, Latu;->a(III)Z

    .line 473
    :cond_4
    iget v0, p0, Latu;->p:I

    if-eq v0, v5, :cond_5

    iget v0, p0, Latu;->p:I

    if-ne v0, v4, :cond_a

    iget v0, p0, Latu;->q:I

    if-eq v0, v4, :cond_a

    .line 476
    :cond_5
    iget-boolean v0, p0, Latu;->z:Z

    if-nez v0, :cond_7

    .line 478
    iget v0, p0, Latu;->y:I

    if-eqz v0, :cond_6

    .line 479
    iget v0, p0, Latu;->y:I

    invoke-static {v0}, Lf;->i(I)V

    .line 482
    :cond_6
    invoke-virtual {p0}, Latu;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lf;->b(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Latu;->y:I

    .line 484
    :cond_7
    iget-boolean v0, p0, Latu;->z:Z

    if-eqz v0, :cond_8

    iget v0, p0, Latu;->q:I

    if-eq v0, v5, :cond_9

    .line 485
    :cond_8
    const/4 v0, 0x1

    iput v0, p0, Latu;->q:I

    .line 487
    iget-object v0, p0, Latu;->h:Laui;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Laui;->a(FF)V

    .line 488
    iget-object v0, p0, Latu;->h:Laui;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Laui;->a(Z)V

    .line 489
    iget v0, p0, Latu;->y:I

    invoke-virtual {p0}, Latu;->f()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Latu;->f()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {p0, v0, v2, v3}, Latu;->a(III)Z

    .line 491
    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p0, Latu;->z:Z

    .line 493
    :cond_a
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract z()Latz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Latu",
            "<TSelfRendererType;>.atz;"
        }
    .end annotation
.end method
