.class public final Lezd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lezd;


# instance fields
.field public b:Leze;

.field public c:Leza;

.field public d:Lezk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    new-array v0, v0, [Lezd;

    sput-object v0, Lezd;->a:[Lezd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 169
    invoke-direct {p0}, Lepn;-><init>()V

    .line 172
    iput-object v0, p0, Lezd;->b:Leze;

    .line 175
    iput-object v0, p0, Lezd;->c:Leza;

    .line 178
    iput-object v0, p0, Lezd;->d:Lezk;

    .line 169
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    iget-object v1, p0, Lezd;->b:Leze;

    if-eqz v1, :cond_0

    .line 200
    const/4 v0, 0x1

    iget-object v1, p0, Lezd;->b:Leze;

    .line 201
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 203
    :cond_0
    iget-object v1, p0, Lezd;->c:Leza;

    if-eqz v1, :cond_1

    .line 204
    const/4 v1, 0x2

    iget-object v2, p0, Lezd;->c:Leza;

    .line 205
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_1
    iget-object v1, p0, Lezd;->d:Lezk;

    if-eqz v1, :cond_2

    .line 208
    const/4 v1, 0x3

    iget-object v2, p0, Lezd;->d:Lezk;

    .line 209
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_2
    iget-object v1, p0, Lezd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    iput v0, p0, Lezd;->cachedSize:I

    .line 213
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 165
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lezd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lezd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lezd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lezd;->b:Leze;

    if-nez v0, :cond_2

    new-instance v0, Leze;

    invoke-direct {v0}, Leze;-><init>()V

    iput-object v0, p0, Lezd;->b:Leze;

    :cond_2
    iget-object v0, p0, Lezd;->b:Leze;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lezd;->c:Leza;

    if-nez v0, :cond_3

    new-instance v0, Leza;

    invoke-direct {v0}, Leza;-><init>()V

    iput-object v0, p0, Lezd;->c:Leza;

    :cond_3
    iget-object v0, p0, Lezd;->c:Leza;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lezd;->d:Lezk;

    if-nez v0, :cond_4

    new-instance v0, Lezk;

    invoke-direct {v0}, Lezk;-><init>()V

    iput-object v0, p0, Lezd;->d:Lezk;

    :cond_4
    iget-object v0, p0, Lezd;->d:Lezk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lezd;->b:Leze;

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x1

    iget-object v1, p0, Lezd;->b:Leze;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 186
    :cond_0
    iget-object v0, p0, Lezd;->c:Leza;

    if-eqz v0, :cond_1

    .line 187
    const/4 v0, 0x2

    iget-object v1, p0, Lezd;->c:Leza;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 189
    :cond_1
    iget-object v0, p0, Lezd;->d:Lezk;

    if-eqz v0, :cond_2

    .line 190
    const/4 v0, 0x3

    iget-object v1, p0, Lezd;->d:Lezk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 192
    :cond_2
    iget-object v0, p0, Lezd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 194
    return-void
.end method
