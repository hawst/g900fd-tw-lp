.class public final Ldnc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 328
    const/4 v0, 0x0

    new-array v0, v0, [Ldnc;

    sput-object v0, Ldnc;->a:[Ldnc;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 329
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 371
    iget-object v1, p0, Ldnc;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 372
    const/4 v0, 0x1

    iget-object v1, p0, Ldnc;->b:Ljava/lang/String;

    .line 373
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 375
    :cond_0
    iget-object v1, p0, Ldnc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 376
    const/4 v1, 0x2

    iget-object v2, p0, Ldnc;->c:Ljava/lang/String;

    .line 377
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_1
    iget-object v1, p0, Ldnc;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 380
    const/4 v1, 0x3

    iget-object v2, p0, Ldnc;->d:Ljava/lang/String;

    .line 381
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 383
    :cond_2
    iget-object v1, p0, Ldnc;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 384
    const/4 v1, 0x4

    iget-object v2, p0, Ldnc;->e:Ljava/lang/String;

    .line 385
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 387
    :cond_3
    iget-object v1, p0, Ldnc;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 388
    const/4 v1, 0x5

    iget-object v2, p0, Ldnc;->f:Ljava/lang/String;

    .line 389
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 391
    :cond_4
    iget-object v1, p0, Ldnc;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 392
    const/4 v1, 0x6

    iget-object v2, p0, Ldnc;->g:Ljava/lang/String;

    .line 393
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 395
    :cond_5
    iget-object v1, p0, Ldnc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 396
    iput v0, p0, Ldnc;->cachedSize:I

    .line 397
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 325
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnc;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnc;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnc;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnc;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnc;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Ldnc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 347
    const/4 v0, 0x1

    iget-object v1, p0, Ldnc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 349
    :cond_0
    iget-object v0, p0, Ldnc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 350
    const/4 v0, 0x2

    iget-object v1, p0, Ldnc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 352
    :cond_1
    iget-object v0, p0, Ldnc;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 353
    const/4 v0, 0x3

    iget-object v1, p0, Ldnc;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 355
    :cond_2
    iget-object v0, p0, Ldnc;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 356
    const/4 v0, 0x4

    iget-object v1, p0, Ldnc;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 358
    :cond_3
    iget-object v0, p0, Ldnc;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 359
    const/4 v0, 0x5

    iget-object v1, p0, Ldnc;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 361
    :cond_4
    iget-object v0, p0, Ldnc;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 362
    const/4 v0, 0x6

    iget-object v1, p0, Ldnc;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 364
    :cond_5
    iget-object v0, p0, Ldnc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 366
    return-void
.end method
