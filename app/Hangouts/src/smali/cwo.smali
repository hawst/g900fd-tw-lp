.class public final Lcwo;
.super Lcwj;
.source "PG"


# instance fields
.field private a:Lcwk;

.field private b:Ljava/lang/String;

.field private c:Lcwr;

.field private d:Lcwp;

.field private e:I

.field private f:Landroid/view/LayoutInflater;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcwk;Lcwr;Lcwp;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 56
    invoke-direct {p0, p1}, Lcwj;-><init>(Landroid/content/Context;)V

    .line 57
    iput p2, p0, Lcwo;->e:I

    .line 58
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcwo;->f:Landroid/view/LayoutInflater;

    .line 59
    iput-object p3, p0, Lcwo;->a:Lcwk;

    .line 60
    iput-object p4, p0, Lcwo;->c:Lcwr;

    .line 61
    iput-object p5, p0, Lcwo;->d:Lcwp;

    .line 62
    const/4 v0, 0x1

    new-array v0, v0, [I

    sget v1, Lf;->jf:I

    aput v1, v0, v3

    .line 64
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 65
    iget v1, v1, Landroid/util/TypedValue;->data:I

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->jg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 66
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcwo;->g:I

    .line 68
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 69
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcwo;->b:Ljava/lang/String;

    .line 100
    invoke-virtual {p0}, Lcwo;->notifyDataSetChanged()V

    .line 101
    return-void
.end method


# virtual methods
.method public a(Lcwc;)V
    .locals 1

    .prologue
    .line 91
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcwc;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcwo;->a(Ljava/lang/String;)V

    .line 92
    return-void

    .line 91
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcwo;->a(I)Lcwc;

    move-result-object v0

    invoke-interface {v0}, Lcwc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcwo;->a(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    .line 73
    if-nez p2, :cond_0

    .line 74
    iget-object v0, p0, Lcwo;->f:Landroid/view/LayoutInflater;

    iget v1, p0, Lcwo;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 76
    :cond_0
    invoke-virtual {p0, p1}, Lcwo;->a(I)Lcwc;

    move-result-object v3

    .line 77
    iget-object v4, p0, Lcwo;->a:Lcwk;

    iget-object v0, p0, Lcwo;->c:Lcwr;

    iget-object v5, p0, Lcwo;->d:Lcwp;

    iget-object v0, p0, Lcwo;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcwo;->b:Ljava/lang/String;

    .line 79
    invoke-interface {v3}, Lcwc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget v6, p0, Lcwo;->g:I

    .line 77
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v2, Lals;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lals;-><init>(B)V

    sget v0, Lg;->k:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lals;->e:Landroid/widget/TextView;

    sget v0, Lg;->E:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lals;->f:Landroid/widget/ImageView;

    sget v0, Lg;->h:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lals;->c:Landroid/widget/TextView;

    sget v0, Lg;->am:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lals;->d:Landroid/widget/ImageView;

    sget v0, Lg;->gQ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lals;->a:Landroid/widget/ImageView;

    sget v0, Lg;->I:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lals;->b:Landroid/widget/ImageView;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v2

    :goto_1
    iget-object v2, v0, Lcwq;->f:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcwq;->f:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v3}, Lcwc;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, v0, Lcwq;->f:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Lcwk;->a(Landroid/widget/ImageView;)V

    iget-object v2, v0, Lcwq;->f:Landroid/widget/ImageView;

    invoke-interface {v3}, Lcwc;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3}, Lcwc;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v2, v7, v8}, Lcwk;->a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_2
    const/4 v2, 0x0

    iget-object v4, v0, Lcwq;->e:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcwq;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-interface {v3}, Lcwc;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, v0, Lcwq;->e:Landroid/widget/TextView;

    invoke-interface {v3}, Lcwc;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v4, v0, Lcwq;->e:Landroid/widget/TextView;

    const/4 v7, 0x0

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_4
    invoke-virtual {v4, v7, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_2
    iget-object v1, v0, Lcwq;->c:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcwq;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    if-eqz v2, :cond_b

    iget-object v1, v0, Lcwq;->c:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcwq;->c:Landroid/widget/TextView;

    invoke-interface {v3}, Lcwc;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_5
    if-eqz v5, :cond_4

    invoke-interface {v5, v0, v3}, Lcwp;->a(Lcwq;Lcwc;)V

    .line 81
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwq;

    .line 82
    iget-object v1, v0, Lcwq;->d:Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    .line 83
    iget-object v1, v0, Lcwq;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lcwo;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcwo;->b:Ljava/lang/String;

    .line 84
    invoke-interface {v3}, Lcwc;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    .line 83
    :goto_6
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    :cond_5
    return-object p2

    .line 79
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    .line 77
    :cond_7
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwq;

    goto/16 :goto_1

    :cond_8
    iget-object v2, v0, Lcwq;->f:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Lcwk;->a(Landroid/widget/ImageView;)V

    iget-object v2, v0, Lcwq;->f:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcwk;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x1

    iget-object v4, v0, Lcwq;->e:Landroid/widget/TextView;

    invoke-interface {v3}, Lcwc;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_a
    const/4 v1, 0x0

    goto :goto_4

    :cond_b
    iget-object v1, v0, Lcwq;->c:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 84
    :cond_c
    const/16 v0, 0x8

    goto :goto_6
.end method
