.class final Lbzi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbzh;


# direct methods
.method constructor <init>(Lbzh;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lbzi;->a:Lbzh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 244
    :goto_0
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->a(Lbzh;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 245
    const/4 v0, 0x0

    .line 246
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 247
    iget-object v2, p0, Lbzi;->a:Lbzh;

    invoke-static {v2}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v4

    monitor-enter v4

    .line 248
    :try_start_0
    iget-object v2, p0, Lbzi;->a:Lbzh;

    invoke-static {v2}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 249
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzm;

    .line 250
    iget-object v2, v0, Lbzm;->a:Lyj;

    .line 251
    :goto_1
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lbzi;->a:Lbzh;

    .line 252
    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzm;

    iget-object v0, v0, Lbzm;->a:Lyj;

    invoke-virtual {v0, v2}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzm;

    iget-object v0, v0, Lbzm;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_0
    move-object v0, v2

    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 262
    invoke-static {v0, v3, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/ArrayList;Z)I

    move-result v0

    .line 263
    iget-object v2, p0, Lbzi;->a:Lbzh;

    monitor-enter v2

    .line 264
    :try_start_2
    iget-object v4, p0, Lbzi;->a:Lbzh;

    invoke-static {v4}, Lbzh;->c(Lbzh;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 265
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 270
    :cond_2
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v2

    monitor-enter v2

    .line 271
    :try_start_3
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 272
    const/4 v0, 0x1

    .line 274
    :goto_2
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 275
    if-eqz v0, :cond_3

    .line 277
    const-wide/16 v2, 0x64

    :try_start_4
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 283
    :cond_3
    :goto_3
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v2

    monitor-enter v2

    .line 284
    :try_start_5
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v0

    if-nez v0, :cond_4

    .line 286
    :try_start_6
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->b(Lbzh;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 290
    :cond_4
    :goto_4
    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 265
    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0

    .line 274
    :catchall_3
    move-exception v0

    monitor-exit v2

    throw v0

    .line 291
    :cond_5
    iget-object v0, p0, Lbzi;->a:Lbzh;

    invoke-static {v0}, Lbzh;->d(Lbzh;)Z

    .line 293
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_2
.end method
