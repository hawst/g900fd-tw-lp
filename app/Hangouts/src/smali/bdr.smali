.class public Lbdr;
.super Lbdp;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0}, Lbdp;-><init>()V

    .line 184
    iput p1, p0, Lbdr;->a:I

    .line 185
    iput-object p2, p0, Lbdr;->b:Ljava/lang/String;

    .line 186
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 190
    new-instance v0, Ldyr;

    invoke-direct {v0}, Ldyr;-><init>()V

    .line 191
    invoke-virtual {p0, p1, p2}, Lbdr;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldyr;->b:Ldvm;

    .line 192
    new-instance v1, Ldyq;

    invoke-direct {v1}, Ldyq;-><init>()V

    .line 193
    iget v2, p0, Lbdr;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldyq;->c:Ljava/lang/Integer;

    .line 194
    iget-object v2, p0, Lbdr;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 195
    iget-object v2, p0, Lbdr;->b:Ljava/lang/String;

    iput-object v2, v1, Ldyq;->i:Ljava/lang/String;

    .line 197
    :cond_0
    iput-object v1, v0, Ldyr;->c:Ldyq;

    .line 198
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const-string v0, "hangouts/add"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    const-string v0, "ui_queue"

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 208
    invoke-static {}, Lapx;->A()J

    move-result-wide v0

    return-wide v0
.end method
