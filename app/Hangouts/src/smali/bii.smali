.class public Lbii;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Ldwn;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3909
    iget-object v0, p1, Ldwn;->b:Ldvn;

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lbfz;-><init>(Ldvn;J)V

    .line 3912
    iget-object v2, p1, Ldwn;->c:[Ldvo;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 3913
    iget-object v5, v4, Ldvo;->c:Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    iget-object v5, v4, Ldvo;->b:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 3914
    iget-object v5, v4, Ldvo;->b:Ljava/lang/Integer;

    invoke-static {v5, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 3926
    :cond_0
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3916
    :pswitch_1
    iget-object v4, v4, Ldvo;->c:Ljava/lang/Boolean;

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lbii;->i:Ljava/lang/Boolean;

    goto :goto_1

    .line 3919
    :pswitch_2
    iget-object v4, v4, Ldvo;->c:Ljava/lang/Boolean;

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lbii;->h:Ljava/lang/Boolean;

    goto :goto_1

    .line 3922
    :pswitch_3
    iget-object v4, v4, Ldvo;->c:Ljava/lang/Boolean;

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lbii;->g:Ljava/lang/Boolean;

    goto :goto_1

    .line 3925
    :pswitch_4
    iget-object v4, v4, Ldvo;->c:Ljava/lang/Boolean;

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lbii;->j:Ljava/lang/Boolean;

    goto :goto_1

    .line 3932
    :cond_1
    return-void

    .line 3914
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3936
    new-instance v0, Ldwn;

    invoke-direct {v0}, Ldwn;-><init>()V

    .line 3937
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldwn;

    .line 3938
    iget-object v1, v0, Ldwn;->b:Ldvn;

    invoke-static {v1}, Lbii;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3939
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldwn;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3941
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbii;

    invoke-direct {v1, v0}, Lbii;-><init>(Ldwn;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 3950
    iget-object v0, p0, Lbii;->h:Ljava/lang/Boolean;

    return-object v0
.end method

.method public g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 3954
    iget-object v0, p0, Lbii;->i:Ljava/lang/Boolean;

    return-object v0
.end method

.method public h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 3958
    iget-object v0, p0, Lbii;->j:Ljava/lang/Boolean;

    return-object v0
.end method
