.class public Lbtd;
.super Lbuc;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private b:Landroid/os/Vibrator;

.field private c:Lbte;

.field private d:Lbuk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lbuc;-><init>()V

    .line 39
    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0, p2}, Lbtd;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbtd;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 138
    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {p0, p1}, Lbtd;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lbtd;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 140
    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 144
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lbte;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lbtd;->c:Lbte;

    .line 47
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 148
    invoke-super {p0, p1, p2, p3}, Lbuc;->onActivityResult(IILandroid/content/Intent;)V

    .line 149
    packed-switch p1, :pswitch_data_0

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 151
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 152
    iget-object v0, p0, Lbtd;->a:Lbtf;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lbtd;->a:Lbtf;

    const-string v1, "com.google.android.gms.people.profile.EXTRA_AVATAR_URL"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbtf;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 54
    invoke-super {p0, p1}, Lbuc;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lbtd;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lbtd;->b:Landroid/os/Vibrator;

    .line 57
    invoke-virtual {p0}, Lbtd;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    if-nez v1, :cond_1

    .line 60
    const-string v0, "Babel"

    const-string v1, "no account specified in fragment arguments"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lbtd;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    const-string v2, "show_general"

    .line 65
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 64
    const-string v2, "Babel"

    const-string v3, "initSettings"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lbub;

    invoke-virtual {p0}, Lbtd;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, p0, v3, v1}, Lbub;-><init>(Lbtz;Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v2, p0, Lbtd;->a:Lbtf;

    iget-object v2, p0, Lbtd;->a:Lbtf;

    invoke-virtual {v2}, Lbtf;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbtd;->a:Lbtf;

    invoke-virtual {v2}, Lbtf;->e()V

    iget-object v2, p0, Lbtd;->b:Landroid/os/Vibrator;

    invoke-virtual {v2}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v2

    if-nez v2, :cond_2

    sget v2, Lh;->ao:I

    sget v3, Lh;->aq:I

    invoke-direct {p0, v2, v3}, Lbtd;->a(II)V

    sget v2, Lh;->fs:I

    sget v3, Lh;->fh:I

    invoke-direct {p0, v2, v3}, Lbtd;->a(II)V

    :cond_2
    if-eqz v0, :cond_3

    sget v0, Lf;->ie:I

    invoke-virtual {p0, v0}, Lbtd;->addPreferencesFromResource(I)V

    invoke-static {}, Lbzd;->d()Z

    move-result v0

    if-nez v0, :cond_3

    sget v0, Lh;->lV:I

    sget v2, Lh;->dc:I

    invoke-direct {p0, v0, v2}, Lbtd;->a(II)V

    .line 68
    :cond_3
    const-string v0, "babel_enable_last_seen"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Lbuk;

    invoke-direct {v0, p0}, Lbuk;-><init>(Lbtz;)V

    iput-object v0, p0, Lbtd;->d:Lbuk;

    .line 71
    iget-object v0, p0, Lbtd;->d:Lbuk;

    invoke-virtual {v0, v1}, Lbuk;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lbuc;->onDestroy()V

    .line 102
    iget-object v0, p0, Lbtd;->d:Lbuk;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lbtd;->d:Lbuk;

    invoke-virtual {v0}, Lbuk;->a()V

    .line 105
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lbuc;->onPause()V

    .line 94
    iget-object v0, p0, Lbtd;->c:Lbte;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lbtd;->c:Lbte;

    invoke-interface {v0}, Lbte;->a()V

    .line 97
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Lbuc;->onResume()V

    .line 79
    iget-object v0, p0, Lbtd;->a:Lbtf;

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lbtd;->a:Lbtf;

    invoke-virtual {v0}, Lbtf;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lbtd;->a:Lbtf;

    invoke-virtual {v0}, Lbtf;->h()V

    .line 86
    :cond_2
    iget-object v0, p0, Lbtd;->c:Lbte;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lbtd;->c:Lbte;

    iget-object v1, p0, Lbtd;->a:Lbtf;

    invoke-virtual {v1}, Lbtf;->a()Lyj;

    move-result-object v1

    invoke-virtual {v1}, Lyj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbte;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
