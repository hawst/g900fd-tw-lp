.class public final Lbkb;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:[Ljava/lang/String;

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbkc;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbke;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/lang/Boolean;

.field private static f:Ljava/lang/Boolean;

.field private static g:Lbkf;

.field private static h:Lbkg;

.field private static i:Lbkd;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 62
    sget-object v0, Lbys;->k:Lcyp;

    sput-boolean v2, Lbkb;->a:Z

    .line 80
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Init"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "Pending"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Ready"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Err_net"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Err_gcm"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Err_svr"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Err_auth"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Err_upgrade"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Err_profile"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Err_oobe"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Err_transient"

    aput-object v2, v0, v1

    sput-object v0, Lbkb;->b:[Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lbkb;->c:Ljava/util/List;

    .line 102
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 107
    sput-object v3, Lbkb;->e:Ljava/lang/Boolean;

    .line 108
    sput-object v3, Lbkb;->f:Ljava/lang/Boolean;

    .line 181
    new-instance v0, Lbkf;

    invoke-direct {v0}, Lbkf;-><init>()V

    sput-object v0, Lbkb;->g:Lbkf;

    .line 265
    new-instance v0, Lbkg;

    invoke-direct {v0}, Lbkg;-><init>()V

    sput-object v0, Lbkb;->h:Lbkg;

    .line 318
    new-instance v0, Lbkd;

    invoke-direct {v0}, Lbkd;-><init>()V

    sput-object v0, Lbkb;->i:Lbkd;

    return-void
.end method

.method public static A()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2504
    const/16 v0, 0x10

    invoke-static {v0}, Lbkb;->a(I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public static B()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2528
    sget-object v0, Lbkb;->i:Lbkd;

    invoke-static {}, Lbkd;->a()Ljava/util/List;

    move-result-object v0

    .line 2529
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2530
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2531
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2533
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    sget v2, Lh;->lw:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2534
    return-object v1
.end method

.method public static C()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2566
    const/16 v0, 0x20

    invoke-static {v0}, Lbkb;->a(I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic D()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lbkb;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic E()Lbkd;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lbkb;->i:Lbkd;

    return-object v0
.end method

.method static synthetic F()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lbkb;->a:Z

    return v0
.end method

.method static synthetic G()Lbkf;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lbkb;->g:Lbkf;

    return-object v0
.end method

.method static synthetic H()Ljava/util/List;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lbkb;->c:Ljava/util/List;

    return-object v0
.end method

.method private static I()Lyj;
    .locals 2

    .prologue
    .line 1027
    invoke-static {}, Lbkb;->J()Lyj;

    move-result-object v0

    .line 1030
    if-nez v0, :cond_4

    .line 1032
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 1033
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1035
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->e(Z)Lyj;

    move-result-object v0

    .line 1037
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1039
    :cond_2
    const/4 v0, 0x1

    invoke-static {v0}, Lbkb;->e(Z)Lyj;

    move-result-object v0

    .line 1041
    :cond_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1042
    invoke-static {v0}, Lbkb;->d(Lyj;)V

    .line 1048
    :cond_4
    :goto_0
    return-object v0

    .line 1045
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static J()Lyj;
    .locals 3

    .prologue
    .line 1057
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1058
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v0

    .line 1059
    invoke-virtual {v0}, Lyj;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1063
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Z)Lbke;
    .locals 2

    .prologue
    .line 1686
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1687
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1688
    invoke-static {p0}, Lbkb;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1689
    const-string v0, "Babel"

    const-string v1, "Account is not valid"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    const/4 v0, 0x0

    .line 1694
    :cond_0
    :goto_0
    return-object v0

    .line 1692
    :cond_1
    invoke-static {p0}, Lbkb;->f(Ljava/lang/String;)Lbke;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 60
    sput-object p0, Lbkb;->f:Ljava/lang/Boolean;

    return-object p0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 609
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 610
    if-eqz v0, :cond_0

    iget-object v1, v0, Lbke;->b:Lbke;

    if-nez v1, :cond_1

    .line 613
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, v0, Lbke;->b:Lbke;

    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(I)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 680
    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    move v9, v0

    .line 681
    :goto_0
    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    move v8, v0

    .line 683
    :goto_1
    and-int/lit8 v0, p0, 0x40

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    move v7, v0

    .line 685
    :goto_2
    and-int/lit8 v0, p0, 0x4

    if-nez v0, :cond_a

    const/4 v0, 0x1

    move v1, v0

    .line 686
    :goto_3
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    sget v2, Lh;->lw:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 687
    const/4 v4, 0x0

    .line 689
    const/4 v3, 0x0

    .line 690
    new-instance v11, Ljava/util/ArrayList;

    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 692
    and-int/lit8 v0, p0, 0x20

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    move v6, v0

    .line 693
    :goto_4
    and-int/lit16 v0, p0, 0x80

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    move v2, v0

    .line 695
    :goto_5
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_6
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 696
    if-eqz v8, :cond_1

    invoke-virtual {v0}, Lbke;->m()Z

    move-result v5

    if-nez v5, :cond_0

    .line 697
    :cond_1
    if-nez v1, :cond_2

    invoke-virtual {v0}, Lbke;->t()Z

    move-result v5

    if-nez v5, :cond_0

    .line 700
    :cond_2
    if-eqz v9, :cond_3

    invoke-virtual {v0}, Lbke;->l()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 703
    :cond_3
    invoke-virtual {v0}, Lbke;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 706
    invoke-virtual {v0}, Lbke;->b()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v5

    invoke-virtual {v5}, Lyj;->O()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 709
    :cond_4
    if-nez v2, :cond_0

    .line 710
    if-eqz v7, :cond_5

    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v5

    invoke-virtual {v5}, Lyj;->e()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 713
    :cond_5
    if-eqz v6, :cond_6

    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v5

    invoke-virtual {v5}, Lyj;->v()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 717
    :cond_6
    invoke-virtual {v0}, Lbke;->l()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 722
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v0}, Lbke;->f()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v4, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 723
    invoke-virtual {v0}, Lbke;->n()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 724
    const/4 v0, 0x1

    move v3, v0

    move v4, v5

    goto :goto_6

    .line 680
    :cond_7
    const/4 v0, 0x0

    move v9, v0

    goto/16 :goto_0

    .line 681
    :cond_8
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_1

    .line 683
    :cond_9
    const/4 v0, 0x0

    move v7, v0

    goto/16 :goto_2

    .line 685
    :cond_a
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_3

    .line 692
    :cond_b
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_4

    .line 693
    :cond_c
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_5

    .line 726
    :cond_d
    if-nez v9, :cond_e

    .line 727
    invoke-virtual {v0}, Lbke;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    move v0, v4

    :goto_7
    move v4, v0

    .line 729
    goto/16 :goto_6

    .line 731
    :cond_f
    and-int/lit8 v0, p0, 0x8

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    .line 732
    :goto_8
    if-nez v0, :cond_10

    if-nez v3, :cond_10

    sget-object v1, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 733
    and-int/lit8 v0, p0, 0x10

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    .line 735
    :cond_10
    :goto_9
    if-eqz v0, :cond_12

    .line 736
    sget-boolean v0, Lbkb;->a:Z

    if-eqz v0, :cond_11

    .line 737
    const-string v0, "Babel"

    const-string v1, "internalGetSortedAccountNames couldn\'t find sms logged in account"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    :cond_11
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 742
    :cond_12
    return-object v11

    .line 731
    :cond_13
    const/4 v0, 0x0

    goto :goto_8

    .line 733
    :cond_14
    const/4 v0, 0x0

    goto :goto_9

    :cond_15
    move v0, v5

    goto :goto_7
.end method

.method public static a(ZZZ)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814
    const/4 v0, 0x0

    .line 815
    if-eqz p0, :cond_0

    .line 816
    const/4 v0, 0x2

    .line 818
    :cond_0
    if-eqz p1, :cond_1

    .line 819
    or-int/lit8 v0, v0, 0x20

    .line 821
    :cond_1
    if-eqz p2, :cond_2

    .line 822
    or-int/lit16 v0, v0, 0x80

    .line 824
    :cond_2
    invoke-static {v0}, Lbkb;->a(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbdk;)Lyj;
    .locals 3

    .prologue
    .line 1145
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1146
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v0

    .line 1147
    invoke-virtual {v0}, Lyj;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1148
    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v2

    invoke-virtual {v2, p0}, Lbdk;->a(Lbdk;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1152
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lbkc;)V
    .locals 2

    .prologue
    .line 1407
    sget-object v1, Lbkb;->c:Ljava/util/List;

    monitor-enter v1

    .line 1408
    :try_start_0
    sget-object v0, Lbkb;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1409
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/io/PrintWriter;)V
    .locals 5

    .prologue
    .line 1738
    const-string v0, "Account list:"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1739
    const-string v0, "Name,ID,jid,state"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1740
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1741
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v3

    .line 1742
    if-nez v3, :cond_0

    .line 1743
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "-, -,"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbke;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1744
    invoke-virtual {v0}, Lbke;->h()I

    move-result v0

    invoke-static {v0}, Lbkb;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1743
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 1748
    :cond_0
    :try_start_0
    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v1

    invoke-virtual {v1}, Lbdk;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1752
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1753
    invoke-virtual {v0}, Lbke;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1754
    invoke-virtual {v0}, Lbke;->h()I

    move-result v0

    invoke-static {v0}, Lbkb;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1752
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1750
    :catch_0
    move-exception v1

    const-string v1, "(unk)"

    goto :goto_1

    .line 1757
    :cond_1
    invoke-virtual {p0}, Ljava/io/PrintWriter;->println()V

    .line 1758
    return-void
.end method

.method public static a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1380
    const-string v0, "Babel"

    const-string v1, "onAccountSetSelfInfoBitFailed: "

    invoke-static {v0, v1, p0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1385
    return-void
.end method

.method public static a(Lyj;Lbdh;Ljava/util/Map;Lyk;[Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Lbdh;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbdm;",
            ">;",
            "Lyk;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 561
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 563
    invoke-static {v0, v10}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 564
    if-nez v0, :cond_1

    .line 598
    :cond_0
    return-void

    .line 568
    :cond_1
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Account selfinfo retrieved: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbke;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    iget-object v1, p1, Lbdh;->b:Lbdk;

    iget-object v2, p1, Lbdh;->e:Ljava/lang/String;

    iget-boolean v3, p1, Lbdh;->m:Z

    iget-object v4, p1, Lbdh;->u:Ljava/lang/String;

    iget-object v5, p1, Lbdh;->h:Ljava/lang/String;

    iget-boolean v8, p1, Lbdh;->n:Z

    move-object v6, p2

    move-object v7, p3

    move-object v9, p4

    invoke-virtual/range {v0 .. v9}, Lbke;->a(Lbdk;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Map;Lyk;Z[Ljava/lang/String;)V

    .line 577
    sget-object v1, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 578
    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 579
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbke;

    .line 580
    iget-object v2, v1, Lbke;->b:Lbke;

    if-ne v2, v0, :cond_2

    .line 582
    invoke-virtual {v1}, Lbke;->g()Lyj;

    move-result-object v4

    .line 583
    if-eqz p4, :cond_4

    .line 584
    array-length v5, p4

    move v2, v10

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v6, p4, v2

    .line 585
    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 586
    const/4 v2, 0x1

    .line 591
    :goto_2
    if-nez v2, :cond_2

    .line 592
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "removing orphaned plus page account:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbke;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 594
    invoke-static {v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;)V

    goto :goto_0

    .line 584
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    move v2, v10

    goto :goto_2
.end method

.method public static a(Lyj;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1353
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account registration failed"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1354
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbkb;->e(Ljava/lang/String;)Lbke;

    move-result-object v2

    invoke-virtual {v2}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1353
    invoke-static {v0, v1, p1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1356
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lbkb;->a(Lyj;Ljava/lang/Exception;Z)V

    .line 1357
    return-void
.end method

.method private static a(Lyj;Ljava/lang/Exception;Z)V
    .locals 2

    .prologue
    .line 1393
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1394
    if-nez v0, :cond_1

    .line 1401
    :cond_0
    :goto_0
    return-void

    .line 1398
    :cond_1
    invoke-virtual {v0, p1, p2}, Lbke;->a(Ljava/lang/Exception;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1399
    invoke-virtual {v0}, Lbke;->v()V

    goto :goto_0
.end method

.method public static a(Lyj;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0x66

    .line 1553
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1554
    if-nez v0, :cond_1

    .line 1565
    :cond_0
    :goto_0
    return-void

    .line 1558
    :cond_1
    invoke-virtual {v0}, Lbke;->h()I

    move-result v1

    .line 1559
    invoke-virtual {v0, p1}, Lbke;->a(Ljava/lang/String;)V

    .line 1560
    if-eq v1, v2, :cond_0

    .line 1561
    invoke-virtual {v0}, Lbke;->h()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1562
    sget-object v1, Lbkb;->g:Lbkf;

    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;)V

    .line 1563
    invoke-virtual {v0}, Lbke;->v()V

    goto :goto_0
.end method

.method public static a(Lyj;Z)V
    .locals 2

    .prologue
    .line 425
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 426
    if-nez v0, :cond_0

    .line 431
    :goto_0
    return-void

    .line 430
    :cond_0
    invoke-virtual {v0, p1}, Lbke;->b(Z)V

    goto :goto_0
.end method

.method public static a(Lyj;ZZZLjava/lang/String;ZZZZ)V
    .locals 4

    .prologue
    .line 633
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 634
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v1

    .line 635
    if-nez v1, :cond_0

    .line 636
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to update voice info for account "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 637
    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 636
    invoke-static {v1, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    :goto_0
    return-void

    .line 642
    :cond_0
    invoke-static/range {p0 .. p8}, Lym;->a(Lyj;ZZZLjava/lang/String;ZZZZ)V

    goto :goto_0
.end method

.method public static a(Z)V
    .locals 4

    .prologue
    .line 410
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 411
    invoke-virtual {v0}, Lbke;->h()I

    move-result v2

    const/16 v3, 0x64

    if-ne v2, v3, :cond_0

    .line 412
    invoke-virtual {v0}, Lbke;->t()Z

    move-result v2

    if-nez v2, :cond_0

    .line 413
    const/4 v2, 0x0

    invoke-virtual {v0, v2, p0}, Lbke;->b(ZZ)V

    goto :goto_0

    .line 416
    :cond_1
    return-void
.end method

.method public static a(Lcwc;)Z
    .locals 2

    .prologue
    .line 879
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 880
    if-nez v0, :cond_0

    .line 881
    const/4 v0, 0x0

    .line 887
    :goto_0
    return v0

    .line 884
    :cond_0
    invoke-interface {p0}, Lcwc;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 885
    invoke-interface {p0}, Lcwc;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 887
    :cond_1
    invoke-interface {p0}, Lcwc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Lyj;)Z
    .locals 1

    .prologue
    .line 912
    sget-object v0, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lyj;->N()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lyj;)I
    .locals 2

    .prologue
    .line 937
    invoke-virtual {p0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 939
    const/4 v0, 0x3

    .line 947
    :goto_0
    return v0

    .line 941
    :cond_0
    invoke-static {}, Lbkb;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 942
    invoke-virtual {v0}, Lyj;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 944
    const/4 v0, 0x0

    goto :goto_0

    .line 947
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b()J
    .locals 4

    .prologue
    .line 347
    const-string v0, "babel_ac_registration_renew_days"

    const-wide/16 v1, 0x7

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1731
    const/16 v0, 0x64

    if-lt p0, v0, :cond_0

    const/16 v0, 0x6e

    if-gt p0, v0, :cond_0

    .line 1732
    sget-object v0, Lbkb;->b:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x64

    aget-object v0, v0, v1

    .line 1734
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lyj;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1179
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1188
    :cond_0
    :goto_0
    return-object v0

    .line 1183
    :cond_1
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v1

    .line 1184
    if-eqz v1, :cond_0

    .line 1185
    invoke-virtual {v1}, Lbke;->g()Lyj;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lbkc;)V
    .locals 2

    .prologue
    .line 1416
    sget-object v1, Lbkb;->c:Ljava/util/List;

    monitor-enter v1

    .line 1417
    :try_start_0
    sget-object v0, Lbkb;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1418
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Lyj;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1367
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account self info failed"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbkb;->e(Ljava/lang/String;)Lbke;

    move-result-object v2

    invoke-virtual {v2}, Lbke;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1369
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lbkb;->a(Lyj;Ljava/lang/Exception;Z)V

    .line 1370
    return-void
.end method

.method public static b(Lyj;Z)V
    .locals 4

    .prologue
    .line 1575
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 1576
    sget-boolean v1, Lbkb;->a:Z

    if-eqz v1, :cond_0

    .line 1577
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BabelAccountManager.logOff "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " logoffFromAccountError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v1

    .line 1581
    if-nez v1, :cond_1

    .line 1595
    :goto_0
    return-void

    .line 1585
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lbke;->a(ZZ)V

    .line 1587
    invoke-virtual {p0}, Lyj;->w()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1590
    invoke-static {p0}, Lbwf;->a(Lyj;)V

    .line 1591
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v1

    invoke-static {v1}, Lbwf;->c(Lyj;)V

    .line 1594
    :cond_2
    sget-object v1, Lbkb;->i:Lbkd;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Z)V
    .locals 1

    .prologue
    .line 905
    invoke-static {}, Lbkb;->l()Z

    move-result v0

    if-eq p0, v0, :cond_0

    .line 906
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lbkb;->e:Ljava/lang/Boolean;

    .line 907
    invoke-static {}, Lcom/google/android/apps/hangouts/sms/SmsReceiver;->a()V

    .line 909
    :cond_0
    return-void
.end method

.method public static c()J
    .locals 4

    .prologue
    .line 354
    const-string v0, "babel_ac_setting_renew_minutes"

    const-wide/16 v1, 0x5a0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public static c(Lyj;)Lyj;
    .locals 3

    .prologue
    .line 979
    if-eqz p0, :cond_1

    .line 980
    invoke-virtual {p0}, Lyj;->v()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1000
    :cond_0
    :goto_0
    return-object p0

    .line 987
    :cond_1
    sget-object v0, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 988
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object p0

    .line 989
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lyj;->v()Z

    move-result v0

    if-nez v0, :cond_0

    .line 995
    :cond_2
    invoke-static {}, Lbkb;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 996
    invoke-virtual {v0}, Lyj;->N()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object p0, v0

    .line 997
    goto :goto_0

    .line 1000
    :cond_4
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static c(Z)V
    .locals 1

    .prologue
    .line 923
    sget-object v0, Lbkb;->h:Lbkg;

    invoke-static {p0}, Lbkg;->a(Z)V

    .line 924
    return-void
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1715
    invoke-static {}, Lbkb;->B()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1716
    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v1

    .line 1727
    :goto_0
    return v0

    .line 1720
    :cond_1
    invoke-static {p0, v2}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v4

    .line 1721
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lbke;->b()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1722
    iget-object v4, v4, Lbke;->b:Lbke;

    invoke-virtual {v4}, Lbke;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1723
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1727
    goto :goto_0
.end method

.method static synthetic d(Ljava/lang/String;)Lbke;
    .locals 1

    .prologue
    .line 60
    invoke-static {p0}, Lbkb;->f(Ljava/lang/String;)Lbke;

    move-result-object v0

    return-object v0
.end method

.method public static d()V
    .locals 8

    .prologue
    .line 364
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    const-string v0, "Babel"

    const-string v1, "loadAccounts"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_0
    invoke-static {}, Lym;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 368
    invoke-static {}, Lym;->e()V

    .line 378
    :cond_1
    sget-object v1, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v1

    .line 379
    :try_start_0
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 381
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    sget v2, Lh;->lw:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->f(Ljava/lang/String;)Lbke;

    .line 382
    sget-object v0, Lbkb;->i:Lbkd;

    invoke-static {}, Lbkd;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 383
    invoke-static {v0}, Lbkb;->f(Ljava/lang/String;)Lbke;

    move-result-object v3

    .line 384
    invoke-virtual {v3}, Lbke;->c()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 385
    invoke-virtual {v3}, Lbke;->c()[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    .line 386
    invoke-static {v6}, Lbkb;->f(Ljava/lang/String;)Lbke;

    move-result-object v6

    .line 387
    invoke-virtual {v6, v3}, Lbke;->a(Lbke;)V

    .line 388
    invoke-virtual {v6}, Lbke;->g()Lyj;

    move-result-object v7

    invoke-static {v7}, Lym;->m(Lyj;)V

    .line 389
    invoke-virtual {v6}, Lbke;->s()V

    .line 385
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 393
    :cond_3
    sget-boolean v0, Lbkb;->a:Z

    if-eqz v0, :cond_4

    .line 394
    const-string v0, "from loadAccounts"

    invoke-static {v0}, Lbkb;->g(Ljava/lang/String;)V

    .line 396
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d(Lyj;)V
    .locals 5

    .prologue
    .line 1082
    invoke-static {p0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 1084
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1085
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCarrierSmsAccount: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    :cond_0
    invoke-static {}, Lbkb;->J()Lyj;

    move-result-object v0

    if-ne p0, v0, :cond_2

    .line 1122
    :cond_1
    :goto_0
    return-void

    .line 1095
    :cond_2
    invoke-static {}, Lbkb;->J()Lyj;

    move-result-object v1

    .line 1096
    if-eqz v1, :cond_6

    invoke-static {v1}, Lym;->k(Lyj;)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v1

    .line 1102
    :goto_1
    invoke-static {p0}, Lym;->k(Lyj;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object v2, p0

    .line 1109
    :goto_2
    sget-object v3, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v3

    .line 1110
    if-eq v0, v2, :cond_3

    if-eqz v3, :cond_3

    .line 1111
    invoke-static {v0}, Lbwf;->a(Lyj;)V

    .line 1113
    :cond_3
    if-eq v1, p0, :cond_5

    .line 1114
    if-eqz v1, :cond_4

    .line 1115
    const/4 v4, 0x0

    invoke-static {v1, v4}, Lym;->e(Lyj;Z)V

    .line 1117
    :cond_4
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lym;->e(Lyj;Z)V

    .line 1119
    :cond_5
    if-eq v0, v2, :cond_1

    if-eqz v3, :cond_1

    .line 1120
    invoke-static {v2}, Lbwf;->c(Lyj;)V

    goto :goto_0

    .line 1099
    :cond_6
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v0

    goto :goto_1

    .line 1105
    :cond_7
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v2

    goto :goto_2
.end method

.method public static d(Z)V
    .locals 4

    .prologue
    .line 1223
    invoke-static {}, Lbld;->k()V

    .line 1224
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1227
    invoke-virtual {v0}, Lbke;->h()I

    move-result v2

    const/16 v3, 0x66

    if-eq v2, v3, :cond_1

    .line 1228
    invoke-virtual {v0}, Lbke;->h()I

    move-result v2

    const/16 v3, 0x6d

    if-eq v2, v3, :cond_1

    .line 1229
    invoke-virtual {v0}, Lbke;->u()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1230
    :cond_1
    if-eqz p0, :cond_2

    .line 1231
    invoke-virtual {v0}, Lbke;->r()V

    .line 1233
    :cond_2
    invoke-virtual {v0}, Lbke;->s()V

    .line 1235
    invoke-virtual {v0}, Lbke;->u()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1237
    invoke-static {v0}, Lbke;->b(Lbke;)V

    goto :goto_0

    .line 1241
    :cond_3
    return-void
.end method

.method private static e(Ljava/lang/String;)Lbke;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    return-object v0
.end method

.method public static e(Z)Lyj;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1298
    invoke-static {}, Lbkb;->B()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1299
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v4

    .line 1300
    if-nez v4, :cond_1

    .line 1301
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Account has not been setup yet. Skip:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1302
    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1301
    invoke-static {v4, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1305
    :cond_1
    invoke-virtual {v4}, Lbke;->g()Lyj;

    move-result-object v0

    .line 1307
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v1, v0

    .line 1308
    goto :goto_0

    .line 1309
    :cond_2
    if-nez p0, :cond_3

    invoke-virtual {v4}, Lbke;->t()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1313
    :cond_3
    :goto_1
    return-object v0

    :cond_4
    sget-object v0, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v1

    goto :goto_1

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method public static e()V
    .locals 1

    .prologue
    .line 400
    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->a(Z)V

    .line 401
    return-void
.end method

.method public static e(Lyj;)V
    .locals 1

    .prologue
    .line 1130
    if-eqz p0, :cond_0

    .line 1131
    invoke-virtual {p0}, Lyj;->j()I

    move-result v0

    invoke-static {v0}, Lym;->b(I)V

    .line 1135
    :goto_0
    return-void

    .line 1133
    :cond_0
    const/4 v0, -0x1

    invoke-static {v0}, Lym;->b(I)V

    goto :goto_0
.end method

.method public static f(Lyj;)I
    .locals 2

    .prologue
    .line 1427
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1428
    if-nez v0, :cond_0

    .line 1429
    const/16 v0, 0x64

    .line 1432
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lbke;->h()I

    move-result v0

    goto :goto_0
.end method

.method private static f(Ljava/lang/String;)Lbke;
    .locals 8

    .prologue
    .line 326
    invoke-static {p0}, Lbkb;->e(Ljava/lang/String;)Lbke;

    move-result-object v0

    .line 327
    if-eqz v0, :cond_0

    .line 343
    :goto_0
    return-object v0

    .line 334
    :cond_0
    sget-object v0, Lbkb;->i:Lbkd;

    new-instance v1, Lbke;

    invoke-static {p0}, Lym;->a(Ljava/lang/String;)Lyj;

    move-result-object v0

    invoke-direct {v1, v0}, Lbke;-><init>(Lyj;)V

    .line 335
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "created account "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lbke;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    sget-boolean v0, Lbkb;->a:Z

    if-eqz v0, :cond_1

    .line 339
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 340
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "    "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 343
    :cond_1
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Lbke;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lbke;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->e(Ljava/lang/String;)Lbke;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(Z)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2516
    if-eqz p0, :cond_0

    .line 2517
    const/4 v0, 0x5

    invoke-static {v0}, Lbkb;->a(I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2519
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->a(I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public static f()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 449
    sget-boolean v0, Lbkb;->a:Z

    if-eqz v0, :cond_0

    .line 450
    const-string v0, "before doing anything in accountsChanged"

    invoke-static {v0}, Lbkb;->g(Ljava/lang/String;)V

    .line 453
    :cond_0
    invoke-static {}, Lbkb;->B()Ljava/util/List;

    move-result-object v1

    .line 458
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-ne v0, v8, :cond_d

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_d

    .line 459
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "accountsChanged: just added "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " and now hiding the sms only account"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 464
    :goto_0
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 465
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 466
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 467
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v4

    .line 468
    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lbkb;->c(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 470
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "removing account per account change:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbke;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 472
    invoke-static {v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;)V

    goto :goto_1

    .line 476
    :cond_2
    invoke-virtual {v0}, Lbke;->h()I

    move-result v4

    const/16 v5, 0x66

    if-eq v4, v5, :cond_1

    .line 477
    invoke-virtual {v0}, Lbke;->t()Z

    move-result v4

    if-nez v4, :cond_1

    .line 478
    invoke-virtual {v0, v8, v8}, Lbke;->b(ZZ)V

    goto :goto_1

    .line 484
    :cond_3
    invoke-static {}, Lbkb;->B()Ljava/util/List;

    move-result-object v0

    .line 485
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 486
    sget-object v4, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 487
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "adding account per account change:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 488
    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 487
    invoke-static {v4, v5}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    invoke-static {v0, v8}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 490
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "adding account per account change:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbke;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-virtual {v0, v9, v8}, Lbke;->b(ZZ)V

    goto :goto_2

    .line 495
    :cond_5
    if-eqz v1, :cond_8

    .line 496
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 497
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 500
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;)V

    .line 503
    :cond_6
    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 505
    sget-object v1, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 506
    invoke-static {v0}, Lbkb;->d(Lyj;)V

    .line 508
    :cond_7
    invoke-static {v0}, Lbkb;->e(Lyj;)V

    .line 512
    :cond_8
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 513
    :cond_9
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 514
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 515
    iget-object v3, v0, Lbke;->b:Lbke;

    .line 516
    if-eqz v3, :cond_9

    sget-object v4, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 517
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v3

    .line 518
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "removing orphaned plus page account:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbke;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 520
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;)V

    goto :goto_3

    .line 527
    :cond_a
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a()V

    .line 529
    sget-object v0, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 530
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 531
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 535
    invoke-static {v0}, Lbwf;->b(Lyj;)V

    .line 542
    :cond_b
    sget-object v0, Lbkb;->i:Lbkd;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 544
    sget-boolean v0, Lbkb;->a:Z

    if-eqz v0, :cond_c

    .line 545
    const-string v0, "accountsChanged, on exit"

    invoke-static {v0}, Lbkb;->g(Ljava/lang/String;)V

    .line 547
    :cond_c
    return-void

    :cond_d
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public static g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780
    const/16 v0, 0x8

    invoke-static {v0}, Lbkb;->a(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static g(Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2549
    if-eqz p0, :cond_0

    .line 2550
    invoke-static {}, Lbkb;->B()Ljava/util/List;

    move-result-object v0

    .line 2552
    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0xc

    invoke-static {v0}, Lbkb;->a(I)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 434
    sget-boolean v0, Lbkb;->a:Z

    if-eqz v0, :cond_0

    .line 435
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dumpAccounts @ "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 437
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 439
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v0

    .line 440
    const-string v2, "Babel"

    invoke-virtual {v0}, Lyj;->Y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 443
    :cond_0
    return-void
.end method

.method public static g(Lyj;)V
    .locals 2

    .prologue
    .line 1463
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1464
    if-eqz v0, :cond_0

    .line 1465
    invoke-virtual {v0}, Lbke;->j()V

    .line 1467
    :cond_0
    return-void
.end method

.method public static h(Lyj;)Ljava/lang/Exception;
    .locals 2

    .prologue
    .line 1475
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1476
    if-nez v0, :cond_0

    .line 1477
    const/4 v0, 0x0

    .line 1480
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lbke;->i()Ljava/lang/Exception;

    move-result-object v0

    goto :goto_0
.end method

.method public static h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 801
    const/16 v0, 0x40

    invoke-static {v0}, Lbkb;->a(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static i(Lyj;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1519
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1520
    if-nez v0, :cond_0

    .line 1521
    const/4 v0, 0x0

    .line 1524
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lbke;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static i()Z
    .locals 3

    .prologue
    .line 831
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 832
    invoke-virtual {v0}, Lbke;->h()I

    move-result v0

    const/16 v2, 0x65

    if-ne v0, v2, :cond_0

    .line 833
    const/4 v0, 0x1

    .line 836
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Lyj;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1535
    invoke-static {p0}, Lbkb;->i(Lyj;)Ljava/lang/String;

    move-result-object v0

    .line 1536
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1537
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1538
    if-ltz v1, :cond_0

    .line 1539
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1542
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j()Lyj;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 859
    invoke-static {}, Lym;->f()I

    move-result v2

    .line 860
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 861
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v0

    .line 862
    invoke-virtual {v0}, Lyj;->j()I

    move-result v4

    if-ne v4, v2, :cond_0

    .line 863
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 866
    invoke-static {v1}, Lbkb;->e(Lyj;)V

    move-object v0, v1

    .line 872
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static k(Lyj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1598
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1599
    if-nez v0, :cond_0

    .line 1607
    :goto_0
    return-void

    .line 1603
    :cond_0
    invoke-virtual {v0, v1, v1}, Lbke;->a(ZZ)V

    .line 1606
    sget-object v0, Lbkb;->i:Lbkd;

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static k()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 892
    sget-object v1, Lbkb;->i:Lbkd;

    invoke-static {}, Lbkd;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 896
    sget-object v0, Lbkb;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 899
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lbkb;->e:Ljava/lang/Boolean;

    .line 901
    :cond_0
    sget-object v0, Lbkb;->e:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    return v0
.end method

.method public static l(Lyj;)Z
    .locals 2

    .prologue
    .line 1610
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1611
    if-nez v0, :cond_0

    .line 1612
    const/4 v0, 0x1

    .line 1615
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lbke;->t()Z

    move-result v0

    goto :goto_0
.end method

.method public static m()Z
    .locals 1

    .prologue
    .line 916
    sget-object v0, Lbkb;->h:Lbkg;

    invoke-static {}, Lbkg;->a()Z

    move-result v0

    return v0
.end method

.method public static m(Lyj;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1623
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v1

    .line 1624
    if-nez v1, :cond_0

    .line 1628
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Lbke;->u()Z

    move-result v0

    goto :goto_0
.end method

.method public static n()Lyj;
    .locals 3

    .prologue
    .line 957
    invoke-static {}, Lbkb;->I()Lyj;

    move-result-object v0

    .line 959
    if-nez v0, :cond_1

    .line 960
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v0

    .line 971
    :cond_0
    :goto_0
    return-object v0

    .line 967
    :cond_1
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v1

    .line 968
    invoke-virtual {v1}, Lbke;->h()I

    move-result v1

    const/16 v2, 0x66

    if-eq v1, v2, :cond_0

    .line 969
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v0

    goto :goto_0
.end method

.method public static n(Lyj;)V
    .locals 2

    .prologue
    .line 1637
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1638
    if-nez v0, :cond_0

    .line 1643
    :goto_0
    return-void

    .line 1642
    :cond_0
    invoke-virtual {v0}, Lbke;->p()V

    goto :goto_0
.end method

.method public static o(Lyj;)Lbki;
    .locals 2

    .prologue
    .line 1652
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Ljava/lang/String;Z)Lbke;

    move-result-object v0

    .line 1653
    if-nez v0, :cond_0

    .line 1654
    const/4 v0, 0x0

    .line 1657
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lbke;->d()Lbki;

    move-result-object v0

    goto :goto_0
.end method

.method public static o()Lyj;
    .locals 1

    .prologue
    .line 1010
    invoke-static {}, Lbkb;->I()Lyj;

    move-result-object v0

    .line 1013
    if-nez v0, :cond_0

    .line 1014
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v0

    .line 1016
    :cond_0
    return-object v0
.end method

.method public static p()Lyj;
    .locals 3

    .prologue
    .line 1067
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1068
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v0

    .line 1069
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1074
    :goto_0
    return-object v0

    .line 1073
    :cond_1
    const-string v0, "Can\'t find sms_only account - there should always be one"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 1074
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q()V
    .locals 5

    .prologue
    .line 1196
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1197
    invoke-virtual {v0}, Lbke;->h()I

    move-result v2

    const/16 v3, 0x65

    if-ne v2, v3, :cond_1

    .line 1198
    invoke-virtual {v0}, Lbke;->t()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1199
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lbke;->a(Z)V

    goto :goto_0

    .line 1200
    :cond_1
    invoke-virtual {v0}, Lbke;->h()I

    move-result v2

    const/16 v3, 0x66

    if-ne v2, v3, :cond_0

    .line 1203
    const-string v2, "Babel"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1204
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Renewing account registration after babel upgrade. Account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1205
    invoke-virtual {v0}, Lbke;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1204
    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    :cond_2
    invoke-virtual {v0}, Lbke;->q()V

    .line 1209
    invoke-static {v0}, Lbke;->b(Lbke;)V

    goto :goto_0

    .line 1212
    :cond_3
    return-void
.end method

.method public static r()V
    .locals 4

    .prologue
    .line 1248
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1250
    invoke-virtual {v0}, Lbke;->h()I

    move-result v2

    const/16 v3, 0x66

    if-ne v2, v3, :cond_0

    .line 1251
    invoke-static {v0}, Lbke;->b(Lbke;)V

    goto :goto_0

    .line 1254
    :cond_1
    return-void
.end method

.method public static s()V
    .locals 2

    .prologue
    .line 1261
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1262
    invoke-virtual {v0}, Lbke;->q()V

    goto :goto_0

    .line 1264
    :cond_0
    return-void
.end method

.method public static t()V
    .locals 6

    .prologue
    .line 1270
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1271
    const-string v0, "Babel"

    const-string v1, "Scheduling acount renewal"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    :cond_0
    sget-object v0, Lbkb;->g:Lbkf;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-string v2, "babel_ac_registration_renew_window_days"

    const-wide/16 v3, 0x3

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(JJ)V

    .line 1275
    return-void
.end method

.method public static u()Z
    .locals 2

    .prologue
    .line 1281
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1282
    invoke-virtual {v0}, Lbke;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1283
    const/4 v0, 0x1

    .line 1287
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lyj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1319
    new-instance v1, Ljava/util/ArrayList;

    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1321
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1322
    invoke-virtual {v0}, Lbke;->h()I

    move-result v3

    const/16 v4, 0x66

    if-ne v3, v4, :cond_0

    .line 1323
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1327
    :cond_1
    return-object v1
.end method

.method public static w()V
    .locals 3

    .prologue
    .line 1439
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1440
    invoke-virtual {v0}, Lbke;->t()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1441
    invoke-virtual {v0}, Lbke;->j()V

    goto :goto_0

    .line 1444
    :cond_1
    return-void
.end method

.method public static x()V
    .locals 4

    .prologue
    .line 1450
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 1451
    invoke-virtual {v0}, Lbke;->t()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lbke;->h()I

    move-result v2

    const/16 v3, 0x66

    if-le v2, v3, :cond_0

    .line 1452
    invoke-virtual {v0}, Lbke;->j()V

    goto :goto_0

    .line 1455
    :cond_1
    return-void
.end method

.method public static y()Z
    .locals 2

    .prologue
    .line 2475
    const-string v0, "babel_allowed_for_domain_bit"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static z()Z
    .locals 2

    .prologue
    .line 2481
    sget-object v0, Lbkb;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbke;

    .line 2482
    invoke-virtual {v0}, Lbke;->g()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2483
    const/4 v0, 0x1

    .line 2486
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
