.class public final Ldri;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldri;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18833
    const/4 v0, 0x0

    new-array v0, v0, [Ldri;

    sput-object v0, Ldri;->a:[Ldri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18834
    invoke-direct {p0}, Lepn;-><init>()V

    .line 18849
    const/4 v0, 0x0

    iput-object v0, p0, Ldri;->h:Ljava/lang/Integer;

    .line 18834
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 18881
    const/4 v0, 0x0

    .line 18882
    iget-object v1, p0, Ldri;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 18883
    const/4 v0, 0x1

    iget-object v1, p0, Ldri;->b:Ljava/lang/Boolean;

    .line 18884
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 18886
    :cond_0
    iget-object v1, p0, Ldri;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 18887
    const/4 v1, 0x2

    iget-object v2, p0, Ldri;->c:Ljava/lang/Boolean;

    .line 18888
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 18890
    :cond_1
    iget-object v1, p0, Ldri;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 18891
    const/4 v1, 0x3

    iget-object v2, p0, Ldri;->d:Ljava/lang/Boolean;

    .line 18892
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 18894
    :cond_2
    iget-object v1, p0, Ldri;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 18895
    const/4 v1, 0x4

    iget-object v2, p0, Ldri;->e:Ljava/lang/Boolean;

    .line 18896
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 18898
    :cond_3
    iget-object v1, p0, Ldri;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 18899
    const/4 v1, 0x5

    iget-object v2, p0, Ldri;->f:Ljava/lang/String;

    .line 18900
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18902
    :cond_4
    iget-object v1, p0, Ldri;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 18903
    const/4 v1, 0x6

    iget-object v2, p0, Ldri;->g:Ljava/lang/Boolean;

    .line 18904
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 18906
    :cond_5
    iget-object v1, p0, Ldri;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 18907
    const/4 v1, 0x7

    iget-object v2, p0, Ldri;->h:Ljava/lang/Integer;

    .line 18908
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 18910
    :cond_6
    iget-object v1, p0, Ldri;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18911
    iput v0, p0, Ldri;->cachedSize:I

    .line 18912
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 18830
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldri;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldri;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldri;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldri;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldri;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldri;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldri;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldri;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldri;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldri;->h:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldri;->h:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 18854
    iget-object v0, p0, Ldri;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 18855
    const/4 v0, 0x1

    iget-object v1, p0, Ldri;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 18857
    :cond_0
    iget-object v0, p0, Ldri;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 18858
    const/4 v0, 0x2

    iget-object v1, p0, Ldri;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 18860
    :cond_1
    iget-object v0, p0, Ldri;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 18861
    const/4 v0, 0x3

    iget-object v1, p0, Ldri;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 18863
    :cond_2
    iget-object v0, p0, Ldri;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 18864
    const/4 v0, 0x4

    iget-object v1, p0, Ldri;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 18866
    :cond_3
    iget-object v0, p0, Ldri;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 18867
    const/4 v0, 0x5

    iget-object v1, p0, Ldri;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 18869
    :cond_4
    iget-object v0, p0, Ldri;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 18870
    const/4 v0, 0x6

    iget-object v1, p0, Ldri;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 18872
    :cond_5
    iget-object v0, p0, Ldri;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 18873
    const/4 v0, 0x7

    iget-object v1, p0, Ldri;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 18875
    :cond_6
    iget-object v0, p0, Ldri;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 18877
    return-void
.end method
