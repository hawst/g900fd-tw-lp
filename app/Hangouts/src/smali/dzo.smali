.class public final Ldzo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzo;


# instance fields
.field public b:Ldzp;

.field public c:[I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldzo;

    sput-object v0, Ldzo;->a:[Ldzo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Ldzo;->b:Ldzp;

    .line 328
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldzo;->c:[I

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 358
    iget-object v0, p0, Ldzo;->b:Ldzp;

    if-eqz v0, :cond_4

    .line 359
    const/4 v0, 0x1

    iget-object v2, p0, Ldzo;->b:Ldzp;

    .line 360
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 362
    :goto_0
    iget-object v2, p0, Ldzo;->c:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldzo;->c:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 364
    iget-object v3, p0, Ldzo;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 366
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 364
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 368
    :cond_0
    add-int/2addr v0, v2

    .line 369
    iget-object v1, p0, Ldzo;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 371
    :cond_1
    iget-object v1, p0, Ldzo;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 372
    const/4 v1, 0x3

    iget-object v2, p0, Ldzo;->d:Ljava/lang/String;

    .line 373
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_2
    iget-object v1, p0, Ldzo;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 376
    const/4 v1, 0x4

    iget-object v2, p0, Ldzo;->e:Ljava/lang/String;

    .line 377
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_3
    iget-object v1, p0, Ldzo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 380
    iput v0, p0, Ldzo;->cachedSize:I

    .line 381
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzo;->b:Ldzp;

    if-nez v0, :cond_2

    new-instance v0, Ldzp;

    invoke-direct {v0}, Ldzp;-><init>()V

    iput-object v0, p0, Ldzo;->b:Ldzp;

    :cond_2
    iget-object v0, p0, Ldzo;->b:Ldzp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldzo;->c:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldzo;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldzo;->c:[I

    :goto_1
    iget-object v1, p0, Ldzo;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldzo;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Ldzo;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzo;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzo;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 337
    iget-object v0, p0, Ldzo;->b:Ldzp;

    if-eqz v0, :cond_0

    .line 338
    const/4 v0, 0x1

    iget-object v1, p0, Ldzo;->b:Ldzp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 340
    :cond_0
    iget-object v0, p0, Ldzo;->c:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldzo;->c:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 341
    iget-object v1, p0, Ldzo;->c:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 342
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 341
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 345
    :cond_1
    iget-object v0, p0, Ldzo;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 346
    const/4 v0, 0x3

    iget-object v1, p0, Ldzo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 348
    :cond_2
    iget-object v0, p0, Ldzo;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 349
    const/4 v0, 0x4

    iget-object v1, p0, Ldzo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 351
    :cond_3
    iget-object v0, p0, Ldzo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 353
    return-void
.end method
