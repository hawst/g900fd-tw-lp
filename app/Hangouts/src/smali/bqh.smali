.class public final Lbqh;
.super Lbmu;
.source "PG"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbqh;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbqh;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lyj;JJ)V
    .locals 2

    .prologue
    .line 81
    invoke-direct/range {p0 .. p5}, Lbmu;-><init>(Lyj;JJ)V

    .line 38
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbqh;->d:J

    .line 82
    return-void
.end method

.method public static a(Ljava/lang/String;)Lbqh;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lbqh;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqh;

    return-object v0
.end method

.method public static a(Lyj;)Lbqh;
    .locals 7

    .prologue
    .line 44
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v6

    .line 45
    sget-object v0, Lbqh;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqh;

    .line 46
    if-nez v0, :cond_0

    .line 47
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "babel_suggested_contact_lowmark_seconds"

    const v3, 0x11940

    .line 48
    invoke-static {v1, v2, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    .line 54
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_suggested_contact_highmark_seconds"

    const v4, 0x15180

    .line 53
    invoke-static {v0, v1, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    .line 59
    new-instance v0, Lbqh;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbqh;-><init>(Lyj;JJ)V

    .line 66
    sget-object v1, Lbqh;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lbqh;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqh;

    .line 69
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    .line 90
    iget-object v0, p0, Lbqh;->c:Lbnl;

    invoke-virtual {v0}, Lbnl;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 92
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_sc_max_favorites"

    const/16 v2, 0xf

    invoke-static {v0, v1, v2}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 96
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "babel_sc_max_you_hangout_with"

    const/4 v3, 0x0

    .line 95
    invoke-static {v0, v2, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 101
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "babel_sc_max_dismissed"

    const/16 v4, 0x64

    .line 100
    invoke-static {v0, v3, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 105
    iget-object v0, p0, Lbqh;->b:Lyj;

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    .line 106
    const-string v4, "hash_favorites"

    invoke-virtual {v0, v4}, Lbsx;->c(Ljava/lang/String;)[B

    move-result-object v4

    .line 108
    const-string v5, "hash_people_you_hangout_with"

    invoke-virtual {v0, v5}, Lbsx;->c(Ljava/lang/String;)[B

    move-result-object v5

    .line 110
    const-string v6, "hash_dismissed_contacts"

    invoke-virtual {v0, v6}, Lbsx;->c(Ljava/lang/String;)[B

    move-result-object v6

    .line 113
    iget-object v7, p0, Lbqh;->c:Lbnl;

    new-instance v0, Lbeo;

    invoke-direct/range {v0 .. v6}, Lbeo;-><init>(III[B[B[B)V

    invoke-virtual {v7, v0}, Lbnl;->a(Lbea;)V

    .line 116
    return-void
.end method

.method protected a(J)V
    .locals 4

    .prologue
    .line 144
    iput-wide p1, p0, Lbqh;->d:J

    .line 145
    new-instance v1, Lyt;

    iget-object v0, p0, Lbqh;->b:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 146
    invoke-virtual {v1}, Lyt;->a()V

    .line 148
    :try_start_0
    const-string v0, "last_suggested_contacts_time"

    iget-wide v2, p0, Lbqh;->d:J

    invoke-virtual {v1, v0, v2, v3}, Lyt;->h(Ljava/lang/String;J)V

    .line 150
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    invoke-virtual {v1}, Lyt;->c()V

    .line 153
    return-void

    .line 152
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0
.end method

.method protected i()J
    .locals 4

    .prologue
    .line 124
    iget-wide v0, p0, Lbqh;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 125
    new-instance v1, Lyt;

    iget-object v0, p0, Lbqh;->b:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 126
    invoke-virtual {v1}, Lyt;->a()V

    .line 128
    :try_start_0
    const-string v0, "last_suggested_contacts_time"

    invoke-virtual {v1, v0}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lbqh;->d:J

    .line 130
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    invoke-virtual {v1}, Lyt;->c()V

    .line 136
    :cond_0
    iget-wide v0, p0, Lbqh;->d:J

    return-wide v0

    .line 132
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0
.end method
