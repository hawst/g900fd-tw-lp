.class public final Lblz;
.super Lbly;
.source "PG"


# instance fields
.field final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final i:Landroid/content/Context;


# direct methods
.method constructor <init>(Lyj;Lbxz;ILjava/util/ArrayList;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Lbxz;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    const/16 v8, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move-object v3, p2

    move-wide v4, p5

    .line 299
    invoke-direct/range {v0 .. v5}, Lbly;-><init>(Lyj;ILbxz;J)V

    .line 295
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lblz;->i:Landroid/content/Context;

    .line 300
    const/4 v0, 0x2

    iput v0, p0, Lblz;->s:I

    .line 303
    iget-object v0, p0, Lblz;->i:Landroid/content/Context;

    sget v1, Lh;->cA:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 305
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v6

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 306
    iput-object p4, p0, Lblz;->h:Ljava/util/ArrayList;

    .line 308
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 309
    if-ne v0, v6, :cond_1

    .line 310
    invoke-virtual {p4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lblz;->b:Ljava/lang/String;

    .line 311
    invoke-virtual {p2}, Lbxz;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lblz;->g:Ljava/lang/String;

    .line 312
    iget-object v0, p0, Lblz;->i:Landroid/content/Context;

    sget v1, Lh;->eY:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lblz;->c:Ljava/lang/CharSequence;

    .line 313
    iget-object v0, p0, Lblz;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lblz;->a(Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lblz;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lblz;->b(Ljava/lang/String;)V

    .line 320
    :goto_1
    return-void

    :cond_0
    move v0, v7

    .line 305
    goto :goto_0

    .line 316
    :cond_1
    iget-object v2, p0, Lblz;->i:Landroid/content/Context;

    sget v3, Lh;->eZ:I

    new-array v4, v6, [Ljava/lang/Object;

    .line 317
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    .line 316
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lblz;->b:Ljava/lang/String;

    .line 318
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lblz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v7, v7, 0x1

    if-ne v7, v8, :cond_3

    iget-object v4, p0, Lblz;->h:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v8, :cond_3

    const-string v0, "..."

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iput-object v2, p0, Lblz;->c:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_3
    if-le v7, v6, :cond_4

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method


# virtual methods
.method protected a(Lbk;)Lbu;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    .line 324
    iget-object v0, p0, Lblz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    iget-object v1, p0, Lblz;->c:Ljava/lang/CharSequence;

    .line 325
    invoke-virtual {v0, v1}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    iget-object v1, p0, Lblz;->c:Ljava/lang/CharSequence;

    .line 326
    invoke-virtual {v0, v1}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    iget-wide v1, p0, Lblz;->e:J

    .line 327
    invoke-virtual {v0, v1, v2}, Lbk;->a(J)Lbk;

    .line 329
    new-instance v2, Lbm;

    invoke-direct {v2, p1}, Lbm;-><init>(Lbk;)V

    .line 330
    invoke-static {}, Lf;->u()I

    move-result v0

    if-le v0, v4, :cond_1

    iget-object v0, p0, Lblz;->m:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lbm;->a(Ljava/lang/CharSequence;)Lbm;

    :goto_0
    iget-object v0, p0, Lblz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v4, :cond_2

    iget-object v0, p0, Lblz;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lbm;->b(Ljava/lang/CharSequence;)Lbm;

    .line 331
    :cond_0
    :goto_1
    return-object v2

    .line 330
    :cond_1
    const-string v0, " "

    invoke-virtual {v2, v0}, Lbm;->a(Ljava/lang/CharSequence;)Lbm;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lblz;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    if-ne v1, v5, :cond_3

    if-le v3, v5, :cond_3

    const-string v0, "..."

    invoke-virtual {v2, v0}, Lbm;->b(Ljava/lang/CharSequence;)Lbm;

    goto :goto_1

    :cond_3
    invoke-virtual {v2, v0}, Lbm;->b(Ljava/lang/CharSequence;)Lbm;

    goto :goto_2
.end method

.method protected d()Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 336
    iget-object v0, p0, Lblz;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lblz;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 338
    iget-object v0, p0, Lblz;->m:Lyj;

    iget-object v1, p0, Lblz;->g:Ljava/lang/String;

    iget v2, p0, Lblz;->f:I

    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lblz;->d:Landroid/content/Intent;

    .line 340
    iget-object v0, p0, Lblz;->d:Landroid/content/Intent;

    const-string v1, "conversation_opened_impression"

    const/16 v2, 0x666

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 348
    :cond_0
    :goto_0
    iget-object v0, p0, Lblz;->d:Landroid/content/Intent;

    return-object v0

    .line 343
    :cond_1
    iget-object v0, p0, Lblz;->m:Lyj;

    invoke-static {v0}, Lbbl;->c(Lyj;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reset_hangout_notifications"

    .line 344
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x24000000

    .line 345
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lblz;->d:Landroid/content/Intent;

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 352
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cL:I

    return v0
.end method
