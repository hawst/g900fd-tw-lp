.class public final Ldto;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldto;


# instance fields
.field public b:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14210
    const/4 v0, 0x0

    new-array v0, v0, [Ldto;

    sput-object v0, Ldto;->a:[Ldto;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14211
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 14227
    const/4 v0, 0x0

    .line 14228
    iget-object v1, p0, Ldto;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 14229
    const/4 v0, 0x1

    iget-object v1, p0, Ldto;->b:Ljava/lang/Long;

    .line 14230
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepl;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14232
    :cond_0
    iget-object v1, p0, Ldto;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14233
    iput v0, p0, Ldto;->cachedSize:I

    .line 14234
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 14207
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldto;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldto;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldto;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldto;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 14218
    iget-object v0, p0, Ldto;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 14219
    const/4 v0, 0x1

    iget-object v1, p0, Ldto;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 14221
    :cond_0
    iget-object v0, p0, Ldto;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 14223
    return-void
.end method
