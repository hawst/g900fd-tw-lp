.class public final Ldfh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfh;


# instance fields
.field public b:Ldfi;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ldey;

.field public g:[Ldey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2958
    const/4 v0, 0x0

    new-array v0, v0, [Ldfh;

    sput-object v0, Ldfh;->a:[Ldfh;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2959
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2962
    iput-object v1, p0, Ldfh;->b:Ldfi;

    .line 2967
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldfh;->d:[Ljava/lang/String;

    .line 2972
    iput-object v1, p0, Ldfh;->f:Ldey;

    .line 2975
    sget-object v0, Ldey;->a:[Ldey;

    iput-object v0, p0, Ldfh;->g:[Ldey;

    .line 2959
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3011
    iget-object v0, p0, Ldfh;->b:Ldfi;

    if-eqz v0, :cond_7

    .line 3012
    const/4 v0, 0x1

    iget-object v2, p0, Ldfh;->b:Ldfi;

    .line 3013
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3015
    :goto_0
    iget-object v2, p0, Ldfh;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3016
    const/4 v2, 0x2

    iget-object v3, p0, Ldfh;->c:Ljava/lang/String;

    .line 3017
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3019
    :cond_0
    iget-object v2, p0, Ldfh;->d:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldfh;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 3021
    iget-object v4, p0, Ldfh;->d:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 3023
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 3021
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3025
    :cond_1
    add-int/2addr v0, v3

    .line 3026
    iget-object v2, p0, Ldfh;->d:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3028
    :cond_2
    iget-object v2, p0, Ldfh;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 3029
    const/4 v2, 0x4

    iget-object v3, p0, Ldfh;->e:Ljava/lang/String;

    .line 3030
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3032
    :cond_3
    iget-object v2, p0, Ldfh;->f:Ldey;

    if-eqz v2, :cond_4

    .line 3033
    const/4 v2, 0x5

    iget-object v3, p0, Ldfh;->f:Ldey;

    .line 3034
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3036
    :cond_4
    iget-object v2, p0, Ldfh;->g:[Ldey;

    if-eqz v2, :cond_6

    .line 3037
    iget-object v2, p0, Ldfh;->g:[Ldey;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 3038
    if-eqz v4, :cond_5

    .line 3039
    const/4 v5, 0x6

    .line 3040
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3037
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3044
    :cond_6
    iget-object v1, p0, Ldfh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3045
    iput v0, p0, Ldfh;->cachedSize:I

    .line 3046
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2955
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldfh;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldfh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldfh;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldfh;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldfh;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldfh;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfh;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldfh;->d:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldfh;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldfh;->d:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Ldfh;->d:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ldfh;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Ldfh;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfh;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldfh;->f:Ldey;

    if-nez v0, :cond_4

    new-instance v0, Ldey;

    invoke-direct {v0}, Ldey;-><init>()V

    iput-object v0, p0, Ldfh;->f:Ldey;

    :cond_4
    iget-object v0, p0, Ldfh;->f:Ldey;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldfh;->g:[Ldey;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldey;

    iget-object v3, p0, Ldfh;->g:[Ldey;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldfh;->g:[Ldey;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldfh;->g:[Ldey;

    :goto_3
    iget-object v2, p0, Ldfh;->g:[Ldey;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldfh;->g:[Ldey;

    new-instance v3, Ldey;

    invoke-direct {v3}, Ldey;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldfh;->g:[Ldey;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Ldfh;->g:[Ldey;

    array-length v0, v0

    goto :goto_2

    :cond_7
    iget-object v2, p0, Ldfh;->g:[Ldey;

    new-instance v3, Ldey;

    invoke-direct {v3}, Ldey;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldfh;->g:[Ldey;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2980
    iget-object v1, p0, Ldfh;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 2981
    const/4 v1, 0x1

    iget-object v2, p0, Ldfh;->b:Ldfi;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2983
    :cond_0
    iget-object v1, p0, Ldfh;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2984
    const/4 v1, 0x2

    iget-object v2, p0, Ldfh;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2986
    :cond_1
    iget-object v1, p0, Ldfh;->d:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2987
    iget-object v2, p0, Ldfh;->d:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2988
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 2987
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2991
    :cond_2
    iget-object v1, p0, Ldfh;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2992
    const/4 v1, 0x4

    iget-object v2, p0, Ldfh;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2994
    :cond_3
    iget-object v1, p0, Ldfh;->f:Ldey;

    if-eqz v1, :cond_4

    .line 2995
    const/4 v1, 0x5

    iget-object v2, p0, Ldfh;->f:Ldey;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2997
    :cond_4
    iget-object v1, p0, Ldfh;->g:[Ldey;

    if-eqz v1, :cond_6

    .line 2998
    iget-object v1, p0, Ldfh;->g:[Ldey;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 2999
    if-eqz v3, :cond_5

    .line 3000
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2998
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3004
    :cond_6
    iget-object v0, p0, Ldfh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3006
    return-void
.end method
