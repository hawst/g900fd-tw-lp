.class public final Leve;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leve;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Levt;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Leve;

    sput-object v0, Leve;->a:[Leve;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Leve;->c:Levt;

    .line 16
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x1

    iget-object v1, p0, Leve;->b:Ljava/lang/String;

    .line 48
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 49
    iget-object v1, p0, Leve;->c:Levt;

    if-eqz v1, :cond_0

    .line 50
    const/4 v1, 0x2

    iget-object v2, p0, Leve;->c:Levt;

    .line 51
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    :cond_0
    iget-object v1, p0, Leve;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 54
    const/4 v1, 0x3

    iget-object v2, p0, Leve;->d:Ljava/lang/String;

    .line 55
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_1
    iget-object v1, p0, Leve;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 58
    const/4 v1, 0x4

    iget-object v2, p0, Leve;->e:Ljava/lang/String;

    .line 59
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_2
    iget-object v1, p0, Leve;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    iput v0, p0, Leve;->cachedSize:I

    .line 63
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 12
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leve;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leve;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leve;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leve;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leve;->c:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Leve;->c:Levt;

    :cond_2
    iget-object v0, p0, Leve;->c:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leve;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leve;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 30
    const/4 v0, 0x1

    iget-object v1, p0, Leve;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 31
    iget-object v0, p0, Leve;->c:Levt;

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Leve;->c:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 34
    :cond_0
    iget-object v0, p0, Leve;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Leve;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 37
    :cond_1
    iget-object v0, p0, Leve;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 38
    const/4 v0, 0x4

    iget-object v1, p0, Leve;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 40
    :cond_2
    iget-object v0, p0, Leve;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 42
    return-void
.end method
