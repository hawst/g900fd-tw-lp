.class public final Lmu;
.super Lma;
.source "PG"

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field private d:Lma;

.field private e:Lme;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lma;Lme;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lma;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object p2, p0, Lmu;->d:Lma;

    .line 41
    iput-object p3, p0, Lmu;->e:Lme;

    .line 42
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lmu;->e:Lme;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmu;->e:Lme;

    invoke-virtual {v0}, Lme;->getItemId()I

    move-result v0

    .line 139
    :goto_0
    if-nez v0, :cond_1

    .line 140
    const/4 v0, 0x0

    .line 142
    :goto_1
    return-object v0

    .line 138
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 142
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lma;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Lmb;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lmu;->d:Lma;

    invoke-virtual {v0, p1}, Lma;->a(Lmb;)V

    .line 75
    return-void
.end method

.method public a(Lma;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lma;->a(Lma;Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmu;->d:Lma;

    invoke-virtual {v0, p1, p2}, Lma;->a(Lma;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lme;)Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lmu;->d:Lma;

    invoke-virtual {v0, p1}, Lma;->a(Lme;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lmu;->d:Lma;

    invoke-virtual {v0}, Lma;->b()Z

    move-result v0

    return v0
.end method

.method public b(Lme;)Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lmu;->d:Lma;

    invoke-virtual {v0, p1}, Lma;->b(Lme;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lmu;->d:Lma;

    invoke-virtual {v0}, Lma;->c()Z

    move-result v0

    return v0
.end method

.method public clearHeader()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public getItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lmu;->e:Lme;

    return-object v0
.end method

.method public r()Lma;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lmu;->d:Lma;

    return-object v0
.end method

.method public setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lmu;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-super {p0, v0}, Lma;->a(Landroid/graphics/drawable/Drawable;)Lma;

    .line 104
    return-object p0
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 98
    invoke-super {p0, p1}, Lma;->a(Landroid/graphics/drawable/Drawable;)Lma;

    .line 99
    return-object p0
.end method

.method public setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lmu;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lma;->a(Ljava/lang/CharSequence;)Lma;

    .line 114
    return-object p0
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 108
    invoke-super {p0, p1}, Lma;->a(Ljava/lang/CharSequence;)Lma;

    .line 109
    return-object p0
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 118
    invoke-super {p0, p1}, Lma;->a(Landroid/view/View;)Lma;

    .line 119
    return-object p0
.end method

.method public setIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lmu;->e:Lme;

    invoke-virtual {v0, p1}, Lme;->setIcon(I)Landroid/view/MenuItem;

    .line 94
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lmu;->e:Lme;

    invoke-virtual {v0, p1}, Lme;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 89
    return-object p0
.end method

.method public setQwertyMode(Z)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lmu;->d:Lma;

    invoke-virtual {v0, p1}, Lma;->setQwertyMode(Z)V

    .line 47
    return-void
.end method

.method public u()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lmu;->d:Lma;

    return-object v0
.end method
