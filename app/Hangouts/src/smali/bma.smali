.class public final Lbma;
.super Lbly;
.source "PG"


# instance fields
.field private final h:Z


# direct methods
.method constructor <init>(Lyj;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JI)V
    .locals 6

    .prologue
    .line 233
    invoke-static {p3}, Lbxz;->a(Ljava/lang/String;)Lbxz;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move-wide v4, p6

    invoke-direct/range {v0 .. v5}, Lbly;-><init>(Lyj;ILbxz;J)V

    .line 234
    const/4 v0, 0x1

    if-ne p8, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbma;->h:Z

    .line 235
    const/4 v0, 0x1

    iput v0, p0, Lbma;->s:I

    .line 236
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 238
    const/4 v0, 0x2

    if-ne p4, v0, :cond_2

    .line 239
    iget-boolean v0, p0, Lbma;->h:Z

    if-eqz v0, :cond_1

    sget v0, Lh;->oP:I

    :goto_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p5, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbma;->c:Ljava/lang/CharSequence;

    .line 248
    :goto_2
    iput-object p3, p0, Lbma;->g:Ljava/lang/String;

    .line 249
    iput-object p2, p0, Lbma;->b:Ljava/lang/String;

    .line 250
    return-void

    .line 234
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 239
    :cond_1
    sget v0, Lh;->eV:I

    goto :goto_1

    .line 244
    :cond_2
    iget-boolean v0, p0, Lbma;->h:Z

    if-eqz v0, :cond_3

    sget v0, Lh;->oQ:I

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbma;->c:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_3
    sget v0, Lh;->eW:I

    goto :goto_3
.end method


# virtual methods
.method protected a(Lbk;)Lbu;
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lbma;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    iget-object v1, p0, Lbma;->b:Ljava/lang/String;

    iget-object v2, p0, Lbma;->c:Ljava/lang/CharSequence;

    .line 255
    invoke-static {v1, v2}, Lbne;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    iget-object v1, p0, Lbma;->c:Ljava/lang/CharSequence;

    .line 257
    invoke-virtual {v0, v1}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    const/4 v1, 0x1

    .line 258
    invoke-virtual {v0, v1}, Lbk;->c(I)Lbk;

    move-result-object v0

    iget-wide v1, p0, Lbma;->e:J

    .line 259
    invoke-virtual {v0, v1, v2}, Lbk;->a(J)Lbk;

    .line 261
    new-instance v0, Lbj;

    invoke-direct {v0, p1}, Lbj;-><init>(Lbk;)V

    iget-object v1, p0, Lbma;->c:Ljava/lang/CharSequence;

    .line 262
    invoke-virtual {v0, v1}, Lbj;->b(Ljava/lang/CharSequence;)Lbj;

    move-result-object v0

    .line 263
    return-object v0
.end method

.method protected d()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Lbma;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lbma;->m:Lyj;

    iget-object v1, p0, Lbma;->g:Ljava/lang/String;

    iget v2, p0, Lbma;->f:I

    .line 270
    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reset_hangout_notifications"

    const/4 v2, 0x1

    .line 271
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x24000000

    .line 272
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lbma;->d:Landroid/content/Intent;

    .line 274
    :cond_0
    iget-object v0, p0, Lbma;->d:Landroid/content/Intent;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lbma;->h:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cO:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cK:I

    goto :goto_0
.end method
