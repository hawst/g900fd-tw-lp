.class public final Lbsc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbsl;
.implements Lcfv;
.implements Lcfw;
.implements Lcvp;


# static fields
.field private static final a:Z

.field private static b:Lbsc;

.field private static final t:Ljava/lang/Object;

.field private static u:Ljava/lang/Runnable;


# instance fields
.field private c:I

.field private d:Z

.field private e:Z

.field private final f:Landroid/os/ConditionVariable;

.field private final g:Lcvi;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbsi;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lbsh;

.field private j:Lcvx;

.field private k:Lcwf;

.field private l:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final n:Landroid/os/Handler;

.field private final o:Ljava/lang/Object;

.field private p:Z

.field private q:J

.field private r:Z

.field private s:Ljava/lang/String;

.field private final v:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lbys;->l:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbsc;->a:Z

    .line 226
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbsc;->t:Ljava/lang/Object;

    .line 232
    new-instance v0, Lbsd;

    invoke-direct {v0}, Lbsd;-><init>()V

    sput-object v0, Lbsc;->u:Ljava/lang/Runnable;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lbsc;->f:Landroid/os/ConditionVariable;

    .line 152
    new-instance v0, Lcvi;

    .line 153
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcvi;-><init>(Landroid/content/Context;Lcfv;Lcfw;)V

    iput-object v0, p0, Lbsc;->g:Lcvi;

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbsc;->h:Ljava/util/List;

    .line 169
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lbsc;->l:Les;

    .line 203
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lbsc;->m:Les;

    .line 206
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbsc;->n:Landroid/os/Handler;

    .line 209
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbsc;->o:Ljava/lang/Object;

    .line 213
    iput-boolean v2, p0, Lbsc;->p:Z

    .line 215
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbsc;->q:J

    .line 220
    iput-boolean v2, p0, Lbsc;->r:Z

    .line 223
    const-string v0, "N/A"

    iput-object v0, p0, Lbsc;->s:Ljava/lang/String;

    .line 307
    new-instance v0, Lbse;

    invoke-direct {v0, p0}, Lbse;-><init>(Lbsc;)V

    iput-object v0, p0, Lbsc;->v:Ljava/lang/Runnable;

    .line 269
    return-void
.end method

.method public static a()Lbsc;
    .locals 3

    .prologue
    .line 249
    sget-object v1, Lbsc;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 252
    :try_start_0
    sget-object v0, Lbsc;->b:Lbsc;

    if-eqz v0, :cond_0

    sget-object v0, Lbsc;->b:Lbsc;

    iget-object v0, v0, Lbsc;->o:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid lock ordering"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 256
    :cond_0
    :try_start_1
    sget-object v0, Lbsc;->b:Lbsc;

    if-nez v0, :cond_1

    .line 257
    new-instance v0, Lbsc;

    invoke-direct {v0}, Lbsc;-><init>()V

    sput-object v0, Lbsc;->b:Lbsc;

    .line 260
    :cond_1
    sget-object v0, Lbsc;->b:Lbsc;

    invoke-direct {v0}, Lbsc;->k()V

    .line 261
    sget-object v0, Lbsc;->b:Lbsc;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method static synthetic a(Lbsc;Lcvx;Lcwf;)V
    .locals 13

    .prologue
    .line 120
    new-instance v6, Les;

    invoke-direct {v6}, Les;-><init>()V

    new-instance v7, Les;

    invoke-direct {v7}, Les;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-string v0, "Babel"

    const-string v1, "GmsPeopleCache: Start building cache"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "md5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    if-eqz p1, :cond_9

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    invoke-virtual {p1}, Lcvx;->a()I

    move-result v0

    if-ge v3, v0, :cond_8

    invoke-virtual {p1, v3}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    invoke-interface {v0}, Lcvw;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-boolean v1, Lbsc;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "GmsPeopleCache: adding entry from gaia:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to person:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v6, v2}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v6, v2, v1}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-interface {v0}, Lcvw;->d()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwg;

    invoke-interface {v1}, Lcwg;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lbzd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-boolean v10, Lbsc;->a:Z

    if-eqz v10, :cond_4

    const-string v10, "Babel"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "GmsPeopleCache: adding entry from phone:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lcwg;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, " to person:"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, " key: "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v7, v9}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v7, v9, v1}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "md5 not available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const-string v9, "Babel"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "GmsPeopleCache_Error: duplicate entry for gaia:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, " name:"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    const/4 v10, 0x0

    invoke-static {v2, v10}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcvw;

    const-string v10, "Babel"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "GmsPeopleCache_Error: dup person name:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Lcvw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v10, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    check-cast v0, Lbtc;

    invoke-virtual {v0, v8}, Lbtc;->a(Ljava/security/MessageDigest;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-boolean v2, Lbsc;->a:Z

    if-eqz v2, :cond_9

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v9, "GmsPeopleCache: End building cache. Total: "

    invoke-direct {v3, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v0, v4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcvx;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " contacts."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v9, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v9

    :try_start_1
    iget-object v4, p0, Lbsc;->j:Lcvx;

    iget-object v5, p0, Lbsc;->k:Lcwf;

    new-instance v0, Lbsh;

    iget-object v1, p0, Lbsc;->h:Ljava/util/List;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lbsh;-><init>(Ljava/util/List;Lcvx;Lcwf;Lcvx;Lcwf;)V

    iput-object v0, p0, Lbsc;->i:Lbsh;

    iput-object p1, p0, Lbsc;->j:Lcvx;

    iput-object p2, p0, Lbsc;->k:Lcwf;

    iput-object v7, p0, Lbsc;->m:Les;

    iput-object v6, p0, Lbsc;->l:Les;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lyt;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-static {v1}, Lf;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsc;->s:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbsc;->p:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsc;->d:Z

    iget-object v0, p0, Lbsc;->f:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    new-instance v0, Lbsg;

    invoke-direct {v0, p0}, Lbsg;-><init>(Lbsc;)V

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->executeOnThreadPool(Ljava/lang/Runnable;)V

    iget-boolean v0, p0, Lbsc;->r:Z

    if-eqz v0, :cond_a

    const-string v0, "Babel"

    const-string v1, "GmsPeopleCache: Triggering queued refresh."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbsc;->r:Z

    invoke-direct {p0}, Lbsc;->m()V

    :cond_a
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lbsc;->j:Lcvx;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lbsc;->j:Lcvx;

    invoke-virtual {v0}, Lcvx;->a()I

    move-result v0

    if-lez v0, :cond_b

    iget-object v0, p0, Lbsc;->j:Lcvx;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcvx;->a(I)Ljava/lang/Object;

    :cond_b
    sget-boolean v0, Lbsc;->a:Z

    if-eqz v0, :cond_c

    const-string v0, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GmsPeopleCache: prefetchFirstPerson "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v2, v5, v2

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v0, p0, Lbsc;->n:Landroid/os/Handler;

    iget-object v1, p0, Lbsc;->i:Lbsh;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v9

    throw v0
.end method

.method public static a(Z)V
    .locals 2

    .prologue
    .line 299
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v0

    .line 301
    :try_start_0
    invoke-direct {v0, p0}, Lbsc;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    invoke-virtual {v0}, Lbsc;->b()V

    .line 304
    return-void

    .line 303
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lbsc;->b()V

    throw v1
.end method

.method static synthetic a(Lbsc;)Z
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lbsc;->l()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lbsc;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lbsc;->o:Ljava/lang/Object;

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 276
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 277
    :try_start_0
    iget-boolean v0, p0, Lbsc;->p:Z

    if-eqz v0, :cond_1

    .line 278
    if-eqz p1, :cond_0

    .line 279
    const-string v0, "Babel"

    const-string v2, "GmsPeopleCache: Load in progress. Queueing up refresh"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsc;->r:Z

    .line 288
    :goto_0
    monitor-exit v1

    return-void

    .line 282
    :cond_0
    const-string v0, "Babel"

    const-string v2, "GmsPeopleCache: Load in progress. Piggyback refresh"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 285
    :cond_1
    :try_start_1
    const-string v0, "Babel"

    const-string v2, "GmsPeopleCache: Refresh requested. Performing now"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-direct {p0}, Lbsc;->m()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method static synthetic c(Lbsc;)Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lbsc;->e:Z

    return v0
.end method

.method private d(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcvw;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 830
    if-nez p1, :cond_0

    move-object v0, v2

    .line 878
    :goto_0
    return-object v0

    .line 834
    :cond_0
    invoke-static {p1}, Lbzd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 835
    iget-object v0, p0, Lbsc;->m:Les;

    invoke-virtual {v0, v4}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 836
    if-eqz v0, :cond_4

    .line 837
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 839
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 846
    iget-object v1, p0, Lbsc;->j:Lcvx;

    .line 847
    invoke-static {v0, v8}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v6

    .line 846
    invoke-virtual {v1, v6}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcvw;

    .line 848
    invoke-interface {v1}, Lcvw;->d()Ljava/lang/Iterable;

    move-result-object v1

    .line 849
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwg;

    .line 850
    invoke-interface {v1}, Lcwg;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lbzd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 859
    invoke-interface {v1}, Lcwg;->b()Ljava/lang/String;

    move-result-object v1

    .line 858
    invoke-static {p1, v1}, Lbzd;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 861
    if-eqz v1, :cond_2

    .line 863
    iget-object v1, p0, Lbsc;->j:Lcvx;

    .line 864
    invoke-static {v0, v8}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 863
    invoke-virtual {v1, v0}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 873
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    move-object v0, v3

    .line 874
    goto :goto_0

    :cond_4
    move-object v0, v2

    .line 878
    goto :goto_0
.end method

.method public static d()V
    .locals 2

    .prologue
    .line 774
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v1

    .line 776
    :try_start_0
    invoke-virtual {v1}, Lbsc;->c()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 778
    invoke-virtual {v1}, Lbsc;->b()V

    .line 779
    return-void

    .line 778
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lbsc;->b()V

    throw v0
.end method

.method static synthetic d(Lbsc;)V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbsc;->b(Z)V

    return-void
.end method

.method public static e()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const-wide/16 v9, 0x0

    .line 1031
    invoke-static {}, Lbsc;->o()J

    move-result-wide v0

    cmp-long v0, v0, v9

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    const-wide/32 v0, 0x5265c00

    .line 1036
    :goto_1
    const-string v3, "refresh_contacts_cache_period"

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1040
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 1041
    sget-wide v5, Lf;->ix:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v5

    const-string v6, "GmsPeopleCache"

    invoke-virtual {v5, v6, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "last_refresh_timestamp"

    invoke-interface {v5, v6, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    sput-wide v5, Lf;->ix:J

    sget-boolean v5, Lbsc;->a:Z

    if-eqz v5, :cond_0

    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "GmsPeopleCache: Last refresh timestamp: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v7, Lf;->ix:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-wide v5, Lf;->ix:J

    .line 1042
    cmp-long v7, v0, v9

    if-eqz v7, :cond_4

    add-long v7, v5, v0

    cmp-long v7, v3, v7

    if-lez v7, :cond_4

    .line 1044
    const-string v0, "Babel"

    const-string v1, "GmsPeopleCache: Triggering contacts cache refresh."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    sput-wide v3, Lf;->ix:J

    .line 1056
    invoke-static {v2}, Lbsc;->a(Z)V

    .line 1070
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 1031
    goto :goto_0

    :cond_3
    const-wide/32 v0, 0x36ee80

    goto :goto_1

    .line 1058
    :cond_4
    sget-boolean v2, Lbsc;->a:Z

    if-eqz v2, :cond_1

    .line 1059
    cmp-long v2, v0, v9

    if-eqz v2, :cond_5

    .line 1060
    sub-long v2, v3, v5

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 1063
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GmsPeopleCache: next refresh in at least "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1066
    :cond_5
    const-string v0, "Babel"

    const-string v1, "GmsPeopleCache: Periodic refresh is off"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static f()V
    .locals 7

    .prologue
    .line 1076
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v1

    .line 1078
    :try_start_0
    const-string v0, "Babel"

    const-string v2, "GmsPeopleCache: refreshContactsDerivedData"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    invoke-virtual {v1}, Lbsc;->b()V

    .line 1084
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "GmsPeopleCache"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "last_refresh_thumbprint"

    const-string v3, "N/A"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1085
    iget-object v2, v1, Lbsc;->s:Ljava/lang/String;

    .line 1091
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GmsPeopleCache: old Thumbprint: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GmsPeopleCache: current Thumbprint: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    const-string v3, "refresh_contacts_derived_data_use_thumbprint"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v3

    .line 1099
    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1100
    const-string v0, "Babel"

    const-string v3, "GmsPeopleCache: Thumbprint same as last time. Do nothing"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sput-wide v3, Lf;->ix:J

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v5, "GmsPeopleCache"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "last_refresh_timestamp"

    invoke-interface {v0, v5, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v3, "last_refresh_thumbprint"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1112
    invoke-virtual {v1}, Lbsc;->b()V

    .line 1113
    return-void

    .line 1104
    :cond_0
    :try_start_1
    invoke-static {}, Lyt;->A()V

    .line 1105
    const-string v0, "Babel"

    const-string v3, "GmsPeopleCache: derived data computed"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1112
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lbsc;->b()V

    throw v0
.end method

.method static synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lbsc;->t:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic h()Lbsc;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lbsc;->b:Lbsc;

    return-object v0
.end method

.method static synthetic i()Lbsc;
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    sput-object v0, Lbsc;->b:Lbsc;

    return-object v0
.end method

.method public static synthetic j()Z
    .locals 1

    .prologue
    .line 120
    sget-boolean v0, Lbsc;->a:Z

    return v0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 366
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 367
    :try_start_0
    iget v0, p0, Lbsc;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbsc;->c:I

    .line 368
    sget-boolean v0, Lbsc;->a:Z

    if-eqz v0, :cond_0

    .line 369
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GmsPeopleCache: addRef  ref:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lbsc;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_0
    iget v0, p0, Lbsc;->c:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 374
    iget-object v0, p0, Lbsc;->n:Landroid/os/Handler;

    sget-object v2, Lbsc;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 377
    :cond_1
    iget-object v0, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 378
    iget-object v0, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 380
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private l()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 417
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 418
    :try_start_0
    iget v2, p0, Lbsc;->c:I

    if-eqz v2, :cond_0

    .line 422
    const/4 v0, 0x0

    monitor-exit v1

    .line 453
    :goto_0
    return v0

    .line 424
    :cond_0
    iget-object v2, p0, Lbsc;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    invoke-static {v2}, Lcwz;->a(Z)V

    .line 425
    const-string v2, "Babel"

    const-string v3, "GmsPeopleCache: close"

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iget-object v2, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v2}, Lcvi;->d()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v2}, Lcvi;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 428
    :cond_1
    iget-object v2, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v2}, Lcvi;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 429
    iget-object v2, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v2, p0}, Lcvi;->a(Lcvp;)V

    .line 431
    :cond_2
    iget-object v2, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v2}, Lcvi;->b()V

    .line 434
    :cond_3
    iget-object v2, p0, Lbsc;->j:Lcvx;

    if-eqz v2, :cond_4

    .line 435
    iget-object v2, p0, Lbsc;->j:Lcvx;

    invoke-virtual {v2}, Lcvx;->d()V

    .line 436
    const/4 v2, 0x0

    iput-object v2, p0, Lbsc;->j:Lcvx;

    .line 439
    :cond_4
    iget-object v2, p0, Lbsc;->k:Lcwf;

    if-eqz v2, :cond_5

    .line 440
    iget-object v2, p0, Lbsc;->k:Lcwf;

    invoke-virtual {v2}, Lcwf;->d()V

    .line 441
    const/4 v2, 0x0

    iput-object v2, p0, Lbsc;->k:Lcwf;

    .line 446
    :cond_5
    iget-object v2, p0, Lbsc;->n:Landroid/os/Handler;

    iget-object v3, p0, Lbsc;->v:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 448
    iget-object v2, p0, Lbsc;->m:Les;

    invoke-virtual {v2}, Les;->clear()V

    .line 449
    iget-object v2, p0, Lbsc;->l:Les;

    invoke-virtual {v2}, Les;->clear()V

    .line 451
    const/4 v2, 0x1

    iput-boolean v2, p0, Lbsc;->e:Z

    .line 453
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 454
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 495
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 497
    :try_start_0
    invoke-direct {p0}, Lbsc;->k()V

    .line 500
    iget-object v0, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 502
    const-string v0, "Babel"

    const-string v2, "GmsPeopleCache: disconnected in load(). Reconnecting"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lbsc;->g:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 504
    monitor-exit v1

    .line 513
    :goto_0
    return-void

    .line 507
    :cond_0
    const-string v0, "Babel"

    const-string v2, "GmsPeopleCache: PeopleLoader load() begins"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsc;->p:Z

    .line 509
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lbsc;->q:J

    .line 511
    new-instance v0, Lbsj;

    iget-object v2, p0, Lbsc;->g:Lcvi;

    invoke-direct {v0, v2, p0}, Lbsj;-><init>(Lcvi;Lbsl;)V

    .line 512
    invoke-virtual {v0}, Lbsj;->a()V

    .line 513
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 926
    iget-object v0, p0, Lbsc;->o:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 927
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid lock ordering"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 931
    :cond_0
    iget-object v0, p0, Lbsc;->f:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 932
    return-void
.end method

.method private static o()J
    .locals 3

    .prologue
    .line 946
    const-string v0, "gms_people_cache_notification_lag"

    const-wide/16 v1, 0x61a8

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Laea;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 801
    invoke-direct {p0}, Lbsc;->n()V

    .line 802
    iget-object v3, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v3

    .line 803
    :try_start_0
    iget-object v0, p0, Lbsc;->l:Les;

    invoke-virtual {v0, p1}, Les;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lbsc;->l:Les;

    invoke-virtual {v0, p1}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v5, p0, Lbsc;->j:Lcvx;

    const/4 v6, 0x0

    invoke-static {v0, v6}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    invoke-virtual {v5, v0}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 809
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_0
    move-object v0, v2

    .line 804
    :goto_1
    if-nez v0, :cond_2

    .line 805
    :try_start_1
    monitor-exit v3

    move-object v0, v1

    .line 808
    :goto_2
    return-object v0

    :cond_1
    move-object v0, v1

    .line 803
    goto :goto_1

    .line 808
    :cond_2
    invoke-static {v0}, Laea;->a(Ljava/util/ArrayList;)Laea;

    move-result-object v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public a(Lbsi;)V
    .locals 3

    .prologue
    .line 325
    invoke-static {}, Lcwz;->a()V

    .line 326
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 329
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 330
    :try_start_0
    iget-object v0, p0, Lbsc;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 331
    iget-object v0, p0, Lbsc;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    iget-object v0, p0, Lbsc;->i:Lbsh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbsc;->i:Lbsh;

    iget-boolean v0, v0, Lbsh;->a:Z

    if-nez v0, :cond_2

    .line 337
    :cond_0
    monitor-exit v1

    .line 349
    :cond_1
    :goto_0
    return-void

    .line 342
    :cond_2
    iget-object v0, p0, Lbsc;->j:Lcvx;

    .line 343
    iget-object v2, p0, Lbsc;->k:Lcwf;

    .line 344
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    if-nez v0, :cond_3

    if-eqz v2, :cond_1

    .line 347
    :cond_3
    invoke-interface {p1, v0, v2}, Lbsi;->a(Lcvx;Lcwf;)V

    goto :goto_0

    .line 344
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Lbsj;Lcvx;Lcwf;)V
    .locals 7

    .prologue
    .line 524
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 528
    :try_start_0
    sget-boolean v0, Lbsc;->a:Z

    if-eqz v0, :cond_0

    .line 529
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GmsPeopleCache: onPeopleReady "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 531
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lbsc;->q:J

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 529
    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :cond_0
    new-instance v0, Lbsf;

    invoke-direct {v0, p0, p2, p3}, Lbsf;-><init>(Lbsc;Lcvx;Lcwf;)V

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->executeOnThreadPool(Ljava/lang/Runnable;)V

    .line 550
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 956
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 957
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "GmsPeopleCache.onDataChanged. Account: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 958
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " gaiaId: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " scopes: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 957
    invoke-static {v1, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    :cond_0
    invoke-static {}, Lbsc;->o()J

    move-result-wide v0

    .line 963
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 964
    iget-object v2, p0, Lbsc;->n:Landroid/os/Handler;

    iget-object v3, p0, Lbsc;->v:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 965
    iget-object v2, p0, Lbsc;->n:Landroid/os/Handler;

    iget-object v3, p0, Lbsc;->v:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 967
    :cond_1
    return-void

    .line 958
    :cond_2
    invoke-static {p1}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcvw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 885
    invoke-direct {p0}, Lbsc;->n()V

    .line 886
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 887
    :try_start_0
    invoke-direct {p0, p1}, Lbsc;->d(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 888
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b()V
    .locals 5

    .prologue
    .line 388
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 389
    :try_start_0
    iget v0, p0, Lbsc;->c:I

    if-gtz v0, :cond_0

    .line 392
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "number of clients should be bigger than 0"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 395
    :cond_0
    :try_start_1
    iget v0, p0, Lbsc;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbsc;->c:I

    .line 397
    sget-boolean v0, Lbsc;->a:Z

    if-eqz v0, :cond_1

    .line 398
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GmsPeopleCache: releaseRef  ref:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lbsc;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :cond_1
    iget v0, p0, Lbsc;->c:I

    if-nez v0, :cond_2

    .line 407
    const-string v0, "Babel"

    const-string v2, "GmsPeopleCache: scheduling reset "

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget-object v0, p0, Lbsc;->n:Landroid/os/Handler;

    sget-object v2, Lbsc;->u:Ljava/lang/Runnable;

    const-wide/32 v3, 0xea60

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 410
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public b(Lbsi;)V
    .locals 2

    .prologue
    .line 352
    invoke-static {}, Lcwz;->a()V

    .line 353
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 354
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 356
    :try_start_0
    iget-object v0, p0, Lbsc;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 358
    iget-object v0, p0, Lbsc;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 359
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public c(Ljava/lang/String;)Lcvw;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 895
    invoke-direct {p0}, Lbsc;->n()V

    .line 896
    iget-object v3, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v3

    .line 897
    :try_start_0
    invoke-direct {p0, p1}, Lbsc;->d(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 899
    if-eqz v0, :cond_4

    .line 901
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    .line 902
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    monitor-exit v3

    .line 919
    :goto_0
    return-object v0

    .line 907
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 908
    invoke-interface {v0}, Lcvw;->e()Ljava/lang/String;

    move-result-object v5

    .line 909
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 910
    if-eqz v1, :cond_2

    .line 912
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v2

    goto :goto_0

    :cond_1
    move-object v0, v1

    :cond_2
    move-object v1, v0

    .line 916
    goto :goto_1

    .line 917
    :cond_3
    monitor-exit v3

    move-object v0, v1

    goto :goto_0

    .line 919
    :cond_4
    monitor-exit v3

    move-object v0, v2

    goto :goto_0

    .line 920
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 768
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 769
    :try_start_0
    iget-boolean v0, p0, Lbsc;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 770
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 462
    iget-object v1, p0, Lbsc;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 463
    :try_start_0
    iget-boolean v0, p0, Lbsc;->e:Z

    if-eqz v0, :cond_0

    .line 465
    monitor-exit v1

    .line 471
    :goto_0
    return-void

    .line 467
    :cond_0
    const-string v0, "Babel"

    const-string v2, "People client connected."

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    iget-object v0, p0, Lbsc;->g:Lcvi;

    const-string v2, "gms_people_cache_account"

    const/4 v3, 0x0

    const/16 v4, 0x8

    invoke-virtual {v0, p0, v2, v3, v4}, Lcvi;->a(Lcvp;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 470
    invoke-direct {p0}, Lbsc;->m()V

    .line 471
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 3

    .prologue
    .line 491
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "People client connection failure: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    return-void
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 481
    const-string v0, "Babel"

    const-string v1, "People client disconnected."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    return-void
.end method
