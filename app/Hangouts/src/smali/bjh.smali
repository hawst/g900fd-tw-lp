.class public final Lbjh;
.super Lbjg;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final b:I

.field public final f:I

.field public final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field

.field public final v:J

.field public final w:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldrr;IJLdsw;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1001
    invoke-direct {p0, p1, p2, p3, p4}, Lbjg;-><init>(Ldrr;IJ)V

    .line 1003
    iget-object v0, p5, Ldsw;->b:Ljava/lang/Integer;

    .line 1004
    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 1003
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbjh;->b:I

    .line 1005
    iget-object v0, p5, Ldsw;->h:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjh;->f:I

    .line 1006
    iget-object v0, p5, Ldsw;->c:[Ldui;

    invoke-static {v0, v2}, Lbdk;->a([Ldui;[Ldqh;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbjh;->u:Ljava/util/List;

    .line 1008
    iget-object v0, p5, Ldsw;->d:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbjh;->v:J

    .line 1009
    iget-object v0, p5, Ldsw;->e:Ldqf;

    if-eqz v0, :cond_0

    iget-object v0, p5, Ldsw;->e:Ldqf;

    iget-object v0, v0, Ldqf;->b:Ljava/lang/String;

    .line 1010
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1011
    iget-object v0, p5, Ldsw;->e:Ldqf;

    iget-object v0, v0, Ldqf;->b:Ljava/lang/String;

    iput-object v0, p0, Lbjh;->w:Ljava/lang/String;

    .line 1015
    :goto_0
    return-void

    .line 1013
    :cond_0
    iput-object v2, p0, Lbjh;->w:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1019
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lbjh;->u:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1020
    iget v1, p0, Lbjh;->b:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 1026
    iget-object v1, p0, Lbjh;->d:Lbdk;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1028
    :cond_0
    return-object v0
.end method
