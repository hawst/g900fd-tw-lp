.class public final Lbpp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:[B

.field public final e:Lbea;

.field public f:I

.field public g:I

.field public h:J

.field public final i:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lbea;J)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 830
    iput-object p1, p0, Lbpp;->b:Ljava/lang/String;

    .line 831
    iput-object p2, p0, Lbpp;->c:Ljava/lang/String;

    .line 832
    iput-object p3, p0, Lbpp;->e:Lbea;

    .line 833
    invoke-static {p3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbea;)[B

    move-result-object v0

    iput-object v0, p0, Lbpp;->d:[B

    .line 834
    iput-wide p4, p0, Lbpp;->i:J

    .line 835
    iput v1, p0, Lbpp;->f:I

    .line 836
    iput v1, p0, Lbpp;->g:I

    .line 837
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[BIIJ)V
    .locals 1

    .prologue
    .line 846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 847
    iput-object p1, p0, Lbpp;->b:Ljava/lang/String;

    .line 848
    iput-object p2, p0, Lbpp;->c:Ljava/lang/String;

    .line 849
    invoke-static {p3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b([B)Lbea;

    move-result-object v0

    iput-object v0, p0, Lbpp;->e:Lbea;

    .line 850
    iput-object p3, p0, Lbpp;->d:[B

    .line 851
    iput-wide p6, p0, Lbpp;->i:J

    .line 852
    iput p4, p0, Lbpp;->f:I

    .line 853
    iput p5, p0, Lbpp;->g:I

    .line 854
    return-void
.end method


# virtual methods
.method public a()Lbea;
    .locals 1

    .prologue
    .line 857
    iget-object v0, p0, Lbpp;->e:Lbea;

    return-object v0
.end method

.method public a(Lbph;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 875
    iget v1, p0, Lbpp;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbpp;->f:I

    .line 876
    invoke-virtual {p1}, Lbph;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 877
    iget v1, p0, Lbpp;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbpp;->g:I

    .line 881
    :cond_0
    iget-object v1, p0, Lbpp;->e:Lbea;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbpp;->e:Lbea;

    instance-of v1, v1, Lbfb;

    if-eqz v1, :cond_2

    .line 882
    invoke-virtual {p1}, Lbph;->e()I

    move-result v1

    const/16 v2, 0x65

    if-ne v1, v2, :cond_2

    .line 883
    const-string v1, "Babel_RequestWriter"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 884
    const-string v1, "Babel_RequestWriter"

    const-string v2, "Do not retry for SendChatMessageRequest if network is not available."

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    :cond_1
    :goto_0
    return v0

    .line 889
    :cond_2
    iget-object v1, p0, Lbpp;->e:Lbea;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbpp;->e:Lbea;

    iget-wide v2, p0, Lbpp;->i:J

    iget v4, p0, Lbpp;->f:I

    invoke-virtual {v1, v2, v3, v4}, Lbea;->a(JI)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lbpp;->g:I

    .line 890
    invoke-virtual {p1, v1}, Lbph;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0}, Lbpp;->a()Lbea;

    move-result-object v0

    .line 943
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 944
    invoke-virtual {p0}, Lbpp;->a()Lbea;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbea;->c(Ljava/lang/String;)Z

    move-result v0

    .line 946
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 861
    invoke-virtual {p0}, Lbpp;->a()Lbea;

    move-result-object v0

    invoke-virtual {v0}, Lbea;->o()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 865
    invoke-virtual {p0}, Lbpp;->a()Lbea;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 898
    invoke-virtual {p0}, Lbpp;->a()Lbea;

    move-result-object v1

    invoke-virtual {v1}, Lbea;->d()J

    move-result-wide v1

    .line 899
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    .line 900
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lbpp;->i:J

    add-long/2addr v1, v5

    cmp-long v1, v3, v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 904
    :cond_0
    return v0
.end method

.method public e()J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 913
    invoke-virtual {p0}, Lbpp;->a()Lbea;

    move-result-object v2

    invoke-virtual {v2}, Lbea;->d()J

    move-result-wide v2

    .line 914
    cmp-long v4, v2, v0

    if-lez v4, :cond_1

    .line 918
    iget-wide v0, p0, Lbpp;->i:J

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 924
    :cond_0
    :goto_0
    return-wide v0

    .line 919
    :cond_1
    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    .line 924
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 938
    iget-wide v0, p0, Lbpp;->h:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 931
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "REQ[fc:%d; creat:%d; type:%s]"

    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget v4, p0, Lbpp;->f:I

    .line 932
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-wide v4, p0, Lbpp;->i:J

    .line 933
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    iget-object v0, p0, Lbpp;->e:Lbea;

    if-nez v0, :cond_0

    const-string v0, "null"

    .line 934
    :goto_0
    aput-object v0, v3, v4

    .line 931
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 933
    :cond_0
    iget-object v0, p0, Lbpp;->e:Lbea;

    .line 934
    invoke-virtual {v0}, Lbea;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
