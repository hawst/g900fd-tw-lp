.class public final Ldve;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldve;


# instance fields
.field public b:Ldvn;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12751
    const/4 v0, 0x0

    new-array v0, v0, [Ldve;

    sput-object v0, Ldve;->a:[Ldve;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12752
    invoke-direct {p0}, Lepn;-><init>()V

    .line 12755
    iput-object v0, p0, Ldve;->b:Ldvn;

    .line 12758
    iput-object v0, p0, Ldve;->c:Ljava/lang/Integer;

    .line 12752
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 12795
    const/4 v0, 0x0

    .line 12796
    iget-object v1, p0, Ldve;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 12797
    const/4 v0, 0x1

    iget-object v1, p0, Ldve;->b:Ldvn;

    .line 12798
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12800
    :cond_0
    iget-object v1, p0, Ldve;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 12801
    const/4 v1, 0x2

    iget-object v2, p0, Ldve;->c:Ljava/lang/Integer;

    .line 12802
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12804
    :cond_1
    iget-object v1, p0, Ldve;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 12805
    const/4 v1, 0x3

    iget-object v2, p0, Ldve;->d:Ljava/lang/String;

    .line 12806
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12808
    :cond_2
    iget-object v1, p0, Ldve;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 12809
    const/4 v1, 0x5

    iget-object v2, p0, Ldve;->e:Ljava/lang/String;

    .line 12810
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12812
    :cond_3
    iget-object v1, p0, Ldve;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 12813
    const/4 v1, 0x6

    iget-object v2, p0, Ldve;->g:Ljava/lang/Integer;

    .line 12814
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12816
    :cond_4
    iget-object v1, p0, Ldve;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 12817
    const/4 v1, 0x7

    iget-object v2, p0, Ldve;->f:Ljava/lang/String;

    .line 12818
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12820
    :cond_5
    iget-object v1, p0, Ldve;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12821
    iput v0, p0, Ldve;->cachedSize:I

    .line 12822
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 12748
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldve;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldve;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldve;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldve;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldve;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldve;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldve;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldve;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldve;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldve;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldve;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldve;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 12771
    iget-object v0, p0, Ldve;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 12772
    const/4 v0, 0x1

    iget-object v1, p0, Ldve;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 12774
    :cond_0
    iget-object v0, p0, Ldve;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 12775
    const/4 v0, 0x2

    iget-object v1, p0, Ldve;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 12777
    :cond_1
    iget-object v0, p0, Ldve;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 12778
    const/4 v0, 0x3

    iget-object v1, p0, Ldve;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 12780
    :cond_2
    iget-object v0, p0, Ldve;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 12781
    const/4 v0, 0x5

    iget-object v1, p0, Ldve;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 12783
    :cond_3
    iget-object v0, p0, Ldve;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 12784
    const/4 v0, 0x6

    iget-object v1, p0, Ldve;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 12786
    :cond_4
    iget-object v0, p0, Ldve;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 12787
    const/4 v0, 0x7

    iget-object v1, p0, Ldve;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 12789
    :cond_5
    iget-object v0, p0, Ldve;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 12791
    return-void
.end method
