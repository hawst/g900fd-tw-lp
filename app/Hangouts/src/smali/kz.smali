.class Lkz;
.super Lkd;
.source "PG"


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Lke;

.field final c:Landroid/app/ActionBar;

.field d:Lao;

.field private e:Landroid/widget/ImageView;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lla;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lke;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lkz;-><init>(Landroid/app/Activity;Lke;Z)V

    .line 47
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lke;Z)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lkd;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkz;->f:Ljava/util/ArrayList;

    .line 50
    iput-object p1, p0, Lkz;->a:Landroid/app/Activity;

    .line 51
    iput-object p2, p0, Lkz;->b:Lke;

    .line 52
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    .line 54
    if-eqz p3, :cond_0

    .line 57
    invoke-virtual {p0}, Lkz;->b()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lkz;->c(Z)V

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 125
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1, p2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 165
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setLogo(Landroid/graphics/drawable/Drawable;)V

    .line 114
    return-void
.end method

.method public a(Landroid/view/View;Lkf;)V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v0, p2}, Landroid/app/ActionBar$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    iget v1, p2, Lkf;->a:I

    iput v1, v0, Landroid/app/ActionBar$LayoutParams;->gravity:I

    .line 88
    iget-object v1, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v1, p1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 89
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method

.method public a(Lkg;)V
    .locals 3

    .prologue
    .line 350
    if-eqz p1, :cond_0

    .line 351
    new-instance v0, Lla;

    invoke-direct {v0, p1}, Lla;-><init>(Lkg;)V

    .line 352
    iget-object v1, p0, Lkz;->f:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v1, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->addOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    .line 355
    :cond_0
    return-void
.end method

.method public a(Lkh;)V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    check-cast p1, Llb;

    iget-object v1, p1, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->removeTab(Landroid/app/ActionBar$Tab;)V

    .line 268
    return-void
.end method

.method public a(Lkh;Z)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    check-cast p1, Llb;

    iget-object v1, p1, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1, p2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 253
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 180
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 145
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 307
    invoke-virtual {p0}, Lkz;->k()Landroid/widget/ImageView;

    move-result-object v0

    .line 308
    if-eqz v0, :cond_1

    .line 309
    if-nez p1, :cond_0

    .line 310
    invoke-virtual {p0}, Lkz;->l()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 312
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 314
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 150
    return-void
.end method

.method public b(Lkh;)V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    check-cast p1, Llb;

    iget-object v1, p1, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    .line 283
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 190
    return-void
.end method

.method public c()Lkh;
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    .line 240
    new-instance v1, Llb;

    invoke-direct {v1, p0, v0}, Llb;-><init>(Lkz;Landroid/app/ActionBar$Tab;)V

    .line 241
    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    .line 242
    return-object v1
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 230
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 366
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getTabCount()I

    move-result v0

    return v0
.end method

.method public d(I)Lkh;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkh;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 336
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 341
    return-void
.end method

.method public h()Landroid/content/Context;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lkz;->c:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method i()Lao;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lkz;->d:Lao;

    if-nez v0, :cond_0

    .line 370
    iget-object v0, p0, Lkz;->b:Lke;

    invoke-interface {v0}, Lke;->e()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    invoke-virtual {v0}, Lao;->a()Lao;

    move-result-object v0

    iput-object v0, p0, Lkz;->d:Lao;

    .line 373
    :cond_0
    iget-object v0, p0, Lkz;->d:Lao;

    return-object v0
.end method

.method j()V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lkz;->d:Lao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkz;->d:Lao;

    invoke-virtual {v0}, Lao;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    iget-object v0, p0, Lkz;->d:Lao;

    invoke-virtual {v0}, Lao;->b()I

    .line 380
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lkz;->d:Lao;

    .line 381
    return-void
.end method

.method k()Landroid/widget/ImageView;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const v4, 0x102002c

    .line 384
    iget-object v0, p0, Lkz;->e:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    .line 385
    iget-object v0, p0, Lkz;->a:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 386
    if-nez v0, :cond_0

    move-object v0, v1

    .line 407
    :goto_0
    return-object v0

    .line 391
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 392
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 393
    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    move-object v0, v1

    .line 395
    goto :goto_0

    .line 398
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 399
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 400
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 402
    :goto_1
    instance-of v1, v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 404
    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lkz;->e:Landroid/widget/ImageView;

    .line 407
    :cond_2
    iget-object v0, p0, Lkz;->e:Landroid/widget/ImageView;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 400
    goto :goto_1
.end method

.method l()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 411
    iget-object v0, p0, Lkz;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x101030b

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 413
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 414
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 415
    return-object v1
.end method
