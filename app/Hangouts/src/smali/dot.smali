.class public final Ldot;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldot;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2585
    const/4 v0, 0x0

    new-array v0, v0, [Ldot;

    sput-object v0, Ldot;->a:[Ldot;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2586
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2622
    const/4 v0, 0x0

    .line 2623
    iget-object v1, p0, Ldot;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 2624
    const/4 v0, 0x1

    iget-object v1, p0, Ldot;->b:Ljava/lang/Boolean;

    .line 2625
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 2627
    :cond_0
    iget-object v1, p0, Ldot;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2628
    const/4 v1, 0x2

    iget-object v2, p0, Ldot;->c:Ljava/lang/Integer;

    .line 2629
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2631
    :cond_1
    iget-object v1, p0, Ldot;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2632
    const/4 v1, 0x3

    iget-object v2, p0, Ldot;->d:Ljava/lang/Integer;

    .line 2633
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2635
    :cond_2
    iget-object v1, p0, Ldot;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2636
    const/4 v1, 0x4

    iget-object v2, p0, Ldot;->e:Ljava/lang/Integer;

    .line 2637
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2639
    :cond_3
    iget-object v1, p0, Ldot;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2640
    const/4 v1, 0x5

    iget-object v2, p0, Ldot;->f:Ljava/lang/Integer;

    .line 2641
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2643
    :cond_4
    iget-object v1, p0, Ldot;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2644
    iput v0, p0, Ldot;->cachedSize:I

    .line 2645
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2582
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldot;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldot;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldot;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldot;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldot;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldot;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldot;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldot;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2601
    iget-object v0, p0, Ldot;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2602
    const/4 v0, 0x1

    iget-object v1, p0, Ldot;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 2604
    :cond_0
    iget-object v0, p0, Ldot;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2605
    const/4 v0, 0x2

    iget-object v1, p0, Ldot;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2607
    :cond_1
    iget-object v0, p0, Ldot;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2608
    const/4 v0, 0x3

    iget-object v1, p0, Ldot;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2610
    :cond_2
    iget-object v0, p0, Ldot;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2611
    const/4 v0, 0x4

    iget-object v1, p0, Ldot;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2613
    :cond_3
    iget-object v0, p0, Ldot;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2614
    const/4 v0, 0x5

    iget-object v1, p0, Ldot;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2616
    :cond_4
    iget-object v0, p0, Ldot;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2618
    return-void
.end method
