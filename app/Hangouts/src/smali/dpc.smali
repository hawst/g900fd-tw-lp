.class public final Ldpc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpc;


# instance fields
.field public b:[Ldpd;

.field public c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2328
    const/4 v0, 0x0

    new-array v0, v0, [Ldpc;

    sput-object v0, Ldpc;->a:[Ldpc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2329
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2332
    sget-object v0, Ldpd;->a:[Ldpd;

    iput-object v0, p0, Ldpc;->b:[Ldpd;

    .line 2329
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2356
    iget-object v1, p0, Ldpc;->b:[Ldpd;

    if-eqz v1, :cond_1

    .line 2357
    iget-object v2, p0, Ldpc;->b:[Ldpd;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2358
    if-eqz v4, :cond_0

    .line 2359
    const/4 v5, 0x1

    .line 2360
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2357
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2364
    :cond_1
    iget-object v1, p0, Ldpc;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2365
    const/4 v1, 0x2

    iget-object v2, p0, Ldpc;->c:Ljava/lang/Long;

    .line 2366
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2368
    :cond_2
    iget-object v1, p0, Ldpc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2369
    iput v0, p0, Ldpc;->cachedSize:I

    .line 2370
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2325
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldpc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldpc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldpc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpc;->b:[Ldpd;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldpd;

    iget-object v3, p0, Ldpc;->b:[Ldpd;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldpc;->b:[Ldpd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldpc;->b:[Ldpd;

    :goto_2
    iget-object v2, p0, Ldpc;->b:[Ldpd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldpc;->b:[Ldpd;

    new-instance v3, Ldpd;

    invoke-direct {v3}, Ldpd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpc;->b:[Ldpd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldpc;->b:[Ldpd;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldpc;->b:[Ldpd;

    new-instance v3, Ldpd;

    invoke-direct {v3}, Ldpd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpc;->b:[Ldpd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpc;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 2339
    iget-object v0, p0, Ldpc;->b:[Ldpd;

    if-eqz v0, :cond_1

    .line 2340
    iget-object v1, p0, Ldpc;->b:[Ldpd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2341
    if-eqz v3, :cond_0

    .line 2342
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2340
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2346
    :cond_1
    iget-object v0, p0, Ldpc;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2347
    const/4 v0, 0x2

    iget-object v1, p0, Ldpc;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 2349
    :cond_2
    iget-object v0, p0, Ldpc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2351
    return-void
.end method
