.class public final Lbqv;
.super Lbqu;
.source "PG"


# instance fields
.field private final l:I

.field private final m:I

.field private final n:I


# direct methods
.method public constructor <init>(Lbjk;)V
    .locals 16

    .prologue
    .line 259
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjk;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lbjk;->d:Lbdk;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lbjk;->e:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lbjk;->n:J

    move-object/from16 v0, p1

    iget-object v8, v0, Lbjk;->m:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lbjk;->l:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v10, v0, Lbjk;->g:I

    move-object/from16 v0, p1

    iget v11, v0, Lbjk;->h:I

    move-object/from16 v0, p1

    iget-wide v12, v0, Lbjk;->i:J

    const/4 v14, 0x4

    move-object/from16 v0, p1

    iget-object v15, v0, Lbjk;->t:[B

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v15}, Lbqu;-><init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;IIJI[B)V

    .line 265
    move-object/from16 v0, p1

    iget v1, v0, Lbjk;->b:I

    move-object/from16 v0, p0

    iput v1, v0, Lbqv;->l:I

    .line 266
    move-object/from16 v0, p1

    iget v1, v0, Lbjk;->f:I

    move-object/from16 v0, p0

    iput v1, v0, Lbqv;->m:I

    .line 267
    move-object/from16 v0, p1

    iget v1, v0, Lbjk;->v:I

    move-object/from16 v0, p0

    iput v1, v0, Lbqv;->n:I

    .line 268
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lbdk;JLjava/lang/String;I)V
    .locals 17

    .prologue
    .line 273
    const-wide/16 v7, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x1

    const-wide/16 v13, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-wide/from16 v5, p3

    move-object/from16 v9, p5

    invoke-direct/range {v2 .. v16}, Lbqu;-><init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;IIJI[B)V

    .line 276
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lbqv;->l:I

    .line 277
    move/from16 v0, p6

    move-object/from16 v1, p0

    iput v0, v1, Lbqv;->m:I

    .line 278
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lbqv;->n:I

    .line 279
    return-void
.end method

.method private d(Lyt;)V
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 317
    iget v0, p0, Lbqv;->m:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v5, 0x9

    .line 322
    :goto_0
    iget-object v1, p0, Lbqv;->f:Ljava/lang/String;

    iget-object v2, p0, Lbqv;->a:Ljava/lang/String;

    iget v3, p0, Lbqv;->k:I

    iget-object v4, p0, Lbqv;->g:Lbdk;

    iget-wide v6, p0, Lbqv;->h:J

    iget-wide v8, p0, Lbqv;->i:J

    iget v10, p0, Lbqv;->c:I

    move-object v0, p1

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;)J

    .line 325
    invoke-virtual {p0, p1}, Lbqv;->a(Lyt;)V

    .line 326
    return-void

    .line 317
    :cond_0
    const/16 v5, 0xa

    goto :goto_0
.end method


# virtual methods
.method public b(Lyt;)V
    .locals 6

    .prologue
    .line 290
    invoke-virtual {p1}, Lyt;->a()V

    .line 292
    :try_start_0
    invoke-direct {p0, p1}, Lbqv;->d(Lyt;)V

    .line 293
    iget v1, p0, Lbqv;->m:I

    iget v2, p0, Lbqv;->n:I

    iget-wide v3, p0, Lbqv;->h:J

    iget-object v5, p0, Lbqv;->f:Ljava/lang/String;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lyt;->a(IIJLjava/lang/String;)V

    .line 295
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    invoke-virtual {p1}, Lyt;->c()V

    .line 300
    iget-object v0, p0, Lbqv;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 301
    return-void

    .line 297
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
.end method

.method public c(Lyt;)V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0, p1}, Lbqv;->d(Lyt;)V

    .line 313
    return-void
.end method
