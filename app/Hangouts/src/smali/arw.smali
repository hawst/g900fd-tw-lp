.class final Larw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lapx;

.field private final b:Z

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;"
        }
    .end annotation
.end field

.field private e:J

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lary;

.field private final h:Landroid/os/Handler;

.field private final i:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lapx;ZLjava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapx;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Larw;->h:Landroid/os/Handler;

    .line 40
    new-instance v0, Larx;

    invoke-direct {v0, p0}, Larx;-><init>(Larw;)V

    iput-object v0, p0, Larw;->i:Ljava/lang/Runnable;

    .line 50
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 51
    iget-object v2, v0, Lbdh;->b:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v0, v0, Lbdh;->q:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcwz;->a(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 53
    :cond_2
    iput-object p1, p0, Larw;->a:Lapx;

    .line 54
    iput-boolean p2, p0, Larw;->b:Z

    .line 55
    iput-object p3, p0, Larw;->c:Ljava/util/List;

    .line 56
    iput-object p4, p0, Larw;->d:Ljava/util/List;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Larw;->f:Ljava/util/ArrayList;

    .line 58
    return-void
.end method

.method private static a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Ljava/util/List;)Lbdh;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)",
            "Lbdh;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 130
    instance-of v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    if-eqz v0, :cond_2

    .line 131
    check-cast p0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 132
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 133
    iget-object v3, v0, Lbdh;->b:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lbdh;->b:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    .line 134
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    .line 138
    goto :goto_0

    .line 140
    :cond_2
    instance-of v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_5

    .line 141
    check-cast p0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    .line 142
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 143
    iget-object v3, v0, Lbdh;->b:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, v0, Lbdh;->q:Ljava/lang/String;

    .line 144
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getPstnJid()Ljava/lang/String;

    move-result-object v4

    .line 143
    invoke-static {v3, v4}, Lf;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 148
    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 150
    goto :goto_0
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Larw;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 218
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 219
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Larw;->f:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Larw;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Ljava/util/List;)Lbdh;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_0

    .line 225
    iget-object v1, p0, Larw;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 226
    invoke-direct {p0}, Larw;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p2}, Larw;->a(ZLjava/lang/Integer;)V

    goto :goto_0
.end method

.method a(ZLjava/lang/Integer;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 234
    iget-object v0, p0, Larw;->h:Landroid/os/Handler;

    iget-object v2, p0, Larw;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 235
    iget-object v0, p0, Larw;->a:Lapx;

    invoke-virtual {v0}, Lapx;->q()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 236
    invoke-virtual {p0}, Larw;->i()V

    .line 239
    iget-object v0, p0, Larw;->a:Lapx;

    invoke-virtual {v0}, Lapx;->D()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Larw;->a:Lapx;

    .line 240
    invoke-virtual {v0}, Lapx;->y()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 241
    :goto_0
    if-eqz v0, :cond_0

    .line 242
    iget-object v2, p0, Larw;->a:Lapx;

    if-nez p1, :cond_2

    const/16 v0, 0x3f2

    :goto_1
    invoke-virtual {v2, v0}, Lapx;->c(I)V

    .line 244
    :cond_0
    iget-object v0, p0, Larw;->a:Lapx;

    invoke-virtual {v0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    :cond_1
    move v0, v1

    .line 240
    goto :goto_0

    .line 242
    :cond_2
    invoke-static {p2, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    const/16 v0, 0x3f6

    goto :goto_1

    :cond_3
    const/16 v0, 0x3f3

    goto :goto_1

    .line 247
    :cond_4
    return-void
.end method

.method a()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Larw;->b:Z

    return v0
.end method

.method a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Larw;->f:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Larw;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Ljava/util/List;)Lbdh;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Larw;->c:Ljava/util/List;

    return-object v0
.end method

.method b(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 190
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isRinging()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->b(Z)V

    .line 192
    iget-object v0, p0, Larw;->f:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Larw;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Ljava/util/List;)Lbdh;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_2

    .line 194
    iget-object v2, p0, Larw;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 195
    invoke-direct {p0}, Larw;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 196
    invoke-virtual {p0, v1, v6}, Larw;->a(ZLjava/lang/Integer;)V

    .line 198
    :cond_1
    invoke-virtual {p0}, Larw;->i()V

    .line 201
    :cond_2
    iget-boolean v0, p0, Larw;->b:Z

    if-eqz v0, :cond_3

    iget-wide v2, p0, Larw;->e:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    .line 206
    invoke-virtual {p0, v1, v6}, Larw;->a(ZLjava/lang/Integer;)V

    .line 208
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 190
    goto :goto_0
.end method

.method c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Larw;->d:Ljava/util/List;

    return-object v0
.end method

.method d()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Larw;->e:J

    return-wide v0
.end method

.method e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Larw;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method f()V
    .locals 4

    .prologue
    .line 81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Larw;->e:J

    .line 82
    iget-object v0, p0, Larw;->g:Lary;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 83
    iget-boolean v0, p0, Larw;->b:Z

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Larw;->a:Lapx;

    invoke-virtual {v0}, Lapx;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Lary;

    iget-object v1, p0, Larw;->a:Lapx;

    invoke-virtual {v1}, Lapx;->K()Z

    move-result v1

    invoke-direct {v0, v1}, Lary;-><init>(Z)V

    iput-object v0, p0, Larw;->g:Lary;

    .line 86
    iget-object v0, p0, Larw;->g:Lary;

    invoke-virtual {v0}, Lary;->a()V

    .line 88
    :cond_0
    iget-object v0, p0, Larw;->a:Lapx;

    invoke-virtual {v0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Larw;->a:Lapx;

    invoke-virtual {v0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 93
    iget-object v2, p0, Larw;->c:Ljava/util/List;

    invoke-virtual {v0}, Lapo;->c()V

    goto :goto_1

    .line 99
    :cond_2
    iget-object v0, p0, Larw;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    iget v0, v0, Lbdh;->a:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_4

    .line 100
    iget-object v0, p0, Larw;->h:Landroid/os/Handler;

    iget-object v1, p0, Larw;->i:Ljava/lang/Runnable;

    iget-object v2, p0, Larw;->a:Lapx;

    .line 101
    invoke-static {}, Lapx;->z()J

    move-result-wide v2

    .line 100
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 103
    :cond_4
    return-void

    .line 99
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method g()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Larw;->h:Landroid/os/Handler;

    iget-object v1, p0, Larw;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 107
    invoke-virtual {p0}, Larw;->i()V

    .line 108
    return-void
.end method

.method h()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Larw;->h:Landroid/os/Handler;

    iget-object v1, p0, Larw;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 112
    invoke-virtual {p0}, Larw;->i()V

    .line 113
    return-void
.end method

.method i()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Larw;->g:Lary;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Larw;->g:Lary;

    invoke-virtual {v0}, Lary;->b()V

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Larw;->g:Lary;

    .line 266
    iget-boolean v0, p0, Larw;->b:Z

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Larw;->a:Lapx;

    invoke-virtual {v0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 268
    invoke-virtual {v0}, Lapo;->a()V

    goto :goto_0

    .line 272
    :cond_0
    return-void
.end method
