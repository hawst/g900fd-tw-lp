.class public final Ldvy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvy;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12887
    const/4 v0, 0x0

    new-array v0, v0, [Ldvy;

    sput-object v0, Ldvy;->a:[Ldvy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12888
    invoke-direct {p0}, Lepn;-><init>()V

    .line 12891
    const/4 v0, 0x0

    iput-object v0, p0, Ldvy;->b:Ldvm;

    .line 12888
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 12925
    const/4 v0, 0x0

    .line 12926
    iget-object v1, p0, Ldvy;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 12927
    const/4 v0, 0x1

    iget-object v1, p0, Ldvy;->b:Ldvm;

    .line 12928
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12930
    :cond_0
    iget-object v1, p0, Ldvy;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 12931
    const/4 v1, 0x2

    iget-object v2, p0, Ldvy;->c:Ljava/lang/Boolean;

    .line 12932
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12934
    :cond_1
    iget-object v1, p0, Ldvy;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 12935
    const/4 v1, 0x3

    iget-object v2, p0, Ldvy;->d:Ljava/lang/String;

    .line 12936
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12938
    :cond_2
    iget-object v1, p0, Ldvy;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 12939
    const/4 v1, 0x4

    iget-object v2, p0, Ldvy;->e:Ljava/lang/Integer;

    .line 12940
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12942
    :cond_3
    iget-object v1, p0, Ldvy;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 12943
    const/4 v1, 0x5

    iget-object v2, p0, Ldvy;->f:Ljava/lang/Boolean;

    .line 12944
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12946
    :cond_4
    iget-object v1, p0, Ldvy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12947
    iput v0, p0, Ldvy;->cachedSize:I

    .line 12948
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 12884
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvy;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvy;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldvy;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldvy;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldvy;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvy;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvy;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldvy;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 12904
    iget-object v0, p0, Ldvy;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 12905
    const/4 v0, 0x1

    iget-object v1, p0, Ldvy;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 12907
    :cond_0
    iget-object v0, p0, Ldvy;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 12908
    const/4 v0, 0x2

    iget-object v1, p0, Ldvy;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 12910
    :cond_1
    iget-object v0, p0, Ldvy;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 12911
    const/4 v0, 0x3

    iget-object v1, p0, Ldvy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 12913
    :cond_2
    iget-object v0, p0, Ldvy;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 12914
    const/4 v0, 0x4

    iget-object v1, p0, Ldvy;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 12916
    :cond_3
    iget-object v0, p0, Ldvy;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 12917
    const/4 v0, 0x5

    iget-object v1, p0, Ldvy;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 12919
    :cond_4
    iget-object v0, p0, Ldvy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 12921
    return-void
.end method
