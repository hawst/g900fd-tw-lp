.class final Larq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/Html$ImageGetter;


# instance fields
.field final synthetic a:Larp;

.field private final b:I


# direct methods
.method private constructor <init>(Larp;I)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Larq;->a:Larp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p2, p0, Larq;->b:I

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Larp;IB)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Larq;-><init>(Larp;I)V

    return-void
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 41
    const-string v0, "ic_ignore_gray"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Larq;->a:Larp;

    invoke-virtual {v0}, Larp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bJ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 47
    :goto_0
    iget v1, p0, Larq;->b:I

    iget v2, p0, Larq;->b:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 48
    return-object v0

    .line 44
    :cond_0
    const-string v0, "ic_exit_red"

    invoke-static {p1, v0}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Larq;->a:Larp;

    invoke-virtual {v0}, Larp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bv:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
