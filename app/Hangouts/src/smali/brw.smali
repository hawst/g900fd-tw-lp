.class public final Lbrw;
.super Landroid/database/AbstractCursor;
.source "PG"


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private a:Lcvx;

.field private b:Lcwf;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "aggregated_person"

    aput-object v2, v0, v1

    sput-object v0, Lbrw;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcvx;Lcwf;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 37
    iput-object p1, p0, Lbrw;->a:Lcvx;

    .line 38
    iput-object p2, p0, Lbrw;->b:Lcwf;

    .line 39
    return-void
.end method


# virtual methods
.method public a()Lcvw;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lbrw;->a:Lcvx;

    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x0

    .line 105
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbrw;->a:Lcvx;

    invoke-virtual {p0}, Lbrw;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    goto :goto_0
.end method

.method public b()Lcwe;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lbrw;->b:Lcwf;

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 116
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbrw;->b:Lcwf;

    invoke-virtual {p0}, Lbrw;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcwf;->b(I)Lcwe;

    move-result-object v0

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lbrw;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lbrw;->a:Lcvx;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lbrw;->a:Lcvx;

    invoke-virtual {v0}, Lcvx;->a()I

    move-result v0

    .line 54
    :goto_0
    return v0

    .line 50
    :cond_0
    iget-object v0, p0, Lbrw;->b:Lcwf;

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lbrw;->b:Lcwf;

    invoke-virtual {v0}, Lcwf;->a()I

    move-result v0

    goto :goto_0

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 1

    .prologue
    .line 89
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getFloat(I)F
    .locals 1

    .prologue
    .line 84
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getInt(I)I
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getLong(I)J
    .locals 1

    .prologue
    .line 79
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getShort(I)S
    .locals 1

    .prologue
    .line 69
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method
