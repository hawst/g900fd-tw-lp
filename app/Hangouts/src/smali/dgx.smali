.class public final Ldgx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgx;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Ldhi;

.field public d:[Ldhl;

.field public e:[Ldig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4024
    const/4 v0, 0x0

    new-array v0, v0, [Ldgx;

    sput-object v0, Ldgx;->a:[Ldgx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4025
    invoke-direct {p0}, Lepn;-><init>()V

    .line 4030
    sget-object v0, Ldhi;->a:[Ldhi;

    iput-object v0, p0, Ldgx;->c:[Ldhi;

    .line 4033
    sget-object v0, Ldhl;->a:[Ldhl;

    iput-object v0, p0, Ldgx;->d:[Ldhl;

    .line 4036
    sget-object v0, Ldig;->a:[Ldig;

    iput-object v0, p0, Ldgx;->e:[Ldig;

    .line 4025
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 4072
    iget-object v0, p0, Ldgx;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 4073
    const/4 v0, 0x1

    iget-object v2, p0, Ldgx;->b:Ljava/lang/String;

    .line 4074
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4076
    :goto_0
    iget-object v2, p0, Ldgx;->c:[Ldhi;

    if-eqz v2, :cond_1

    .line 4077
    iget-object v3, p0, Ldgx;->c:[Ldhi;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 4078
    if-eqz v5, :cond_0

    .line 4079
    const/4 v6, 0x2

    .line 4080
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4077
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4084
    :cond_1
    iget-object v2, p0, Ldgx;->d:[Ldhl;

    if-eqz v2, :cond_3

    .line 4085
    iget-object v3, p0, Ldgx;->d:[Ldhl;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 4086
    if-eqz v5, :cond_2

    .line 4087
    const/4 v6, 0x3

    .line 4088
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4085
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 4092
    :cond_3
    iget-object v2, p0, Ldgx;->e:[Ldig;

    if-eqz v2, :cond_5

    .line 4093
    iget-object v2, p0, Ldgx;->e:[Ldig;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 4094
    if-eqz v4, :cond_4

    .line 4095
    const/4 v5, 0x4

    .line 4096
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4093
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 4100
    :cond_5
    iget-object v1, p0, Ldgx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4101
    iput v0, p0, Ldgx;->cachedSize:I

    .line 4102
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4021
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldgx;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldgx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldgx;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgx;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgx;->c:[Ldhi;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldhi;

    iget-object v3, p0, Ldgx;->c:[Ldhi;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldgx;->c:[Ldhi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldgx;->c:[Ldhi;

    :goto_2
    iget-object v2, p0, Ldgx;->c:[Ldhi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldgx;->c:[Ldhi;

    new-instance v3, Ldhi;

    invoke-direct {v3}, Ldhi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgx;->c:[Ldhi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldgx;->c:[Ldhi;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldgx;->c:[Ldhi;

    new-instance v3, Ldhi;

    invoke-direct {v3}, Ldhi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgx;->c:[Ldhi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgx;->d:[Ldhl;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldhl;

    iget-object v3, p0, Ldgx;->d:[Ldhl;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldgx;->d:[Ldhl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldgx;->d:[Ldhl;

    :goto_4
    iget-object v2, p0, Ldgx;->d:[Ldhl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldgx;->d:[Ldhl;

    new-instance v3, Ldhl;

    invoke-direct {v3}, Ldhl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgx;->d:[Ldhl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ldgx;->d:[Ldhl;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldgx;->d:[Ldhl;

    new-instance v3, Ldhl;

    invoke-direct {v3}, Ldhl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgx;->d:[Ldhl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgx;->e:[Ldig;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ldig;

    iget-object v3, p0, Ldgx;->e:[Ldig;

    if-eqz v3, :cond_8

    iget-object v3, p0, Ldgx;->e:[Ldig;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Ldgx;->e:[Ldig;

    :goto_6
    iget-object v2, p0, Ldgx;->e:[Ldig;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Ldgx;->e:[Ldig;

    new-instance v3, Ldig;

    invoke-direct {v3}, Ldig;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgx;->e:[Ldig;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Ldgx;->e:[Ldig;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Ldgx;->e:[Ldig;

    new-instance v3, Ldig;

    invoke-direct {v3}, Ldig;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgx;->e:[Ldig;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 4041
    iget-object v1, p0, Ldgx;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4042
    const/4 v1, 0x1

    iget-object v2, p0, Ldgx;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 4044
    :cond_0
    iget-object v1, p0, Ldgx;->c:[Ldhi;

    if-eqz v1, :cond_2

    .line 4045
    iget-object v2, p0, Ldgx;->c:[Ldhi;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 4046
    if-eqz v4, :cond_1

    .line 4047
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 4045
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4051
    :cond_2
    iget-object v1, p0, Ldgx;->d:[Ldhl;

    if-eqz v1, :cond_4

    .line 4052
    iget-object v2, p0, Ldgx;->d:[Ldhl;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 4053
    if-eqz v4, :cond_3

    .line 4054
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 4052
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4058
    :cond_4
    iget-object v1, p0, Ldgx;->e:[Ldig;

    if-eqz v1, :cond_6

    .line 4059
    iget-object v1, p0, Ldgx;->e:[Ldig;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 4060
    if-eqz v3, :cond_5

    .line 4061
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 4059
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4065
    :cond_6
    iget-object v0, p0, Ldgx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4067
    return-void
.end method
