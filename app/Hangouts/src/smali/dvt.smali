.class public final Ldvt;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvt;


# instance fields
.field public b:Ldpu;

.field public c:Ldut;

.field public d:Ldrg;

.field public e:Ldqx;

.field public f:Ldqy;

.field public g:Ldvq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16298
    const/4 v0, 0x0

    new-array v0, v0, [Ldvt;

    sput-object v0, Ldvt;->a:[Ldvt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16299
    invoke-direct {p0}, Lepn;-><init>()V

    .line 16302
    iput-object v0, p0, Ldvt;->b:Ldpu;

    .line 16305
    iput-object v0, p0, Ldvt;->c:Ldut;

    .line 16308
    iput-object v0, p0, Ldvt;->d:Ldrg;

    .line 16311
    iput-object v0, p0, Ldvt;->e:Ldqx;

    .line 16314
    iput-object v0, p0, Ldvt;->f:Ldqy;

    .line 16317
    iput-object v0, p0, Ldvt;->g:Ldvq;

    .line 16299
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 16346
    const/4 v0, 0x0

    .line 16347
    iget-object v1, p0, Ldvt;->b:Ldpu;

    if-eqz v1, :cond_0

    .line 16348
    const/4 v0, 0x1

    iget-object v1, p0, Ldvt;->b:Ldpu;

    .line 16349
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16351
    :cond_0
    iget-object v1, p0, Ldvt;->c:Ldut;

    if-eqz v1, :cond_1

    .line 16352
    const/4 v1, 0x2

    iget-object v2, p0, Ldvt;->c:Ldut;

    .line 16353
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16355
    :cond_1
    iget-object v1, p0, Ldvt;->d:Ldrg;

    if-eqz v1, :cond_2

    .line 16356
    const/4 v1, 0x3

    iget-object v2, p0, Ldvt;->d:Ldrg;

    .line 16357
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16359
    :cond_2
    iget-object v1, p0, Ldvt;->e:Ldqx;

    if-eqz v1, :cond_3

    .line 16360
    const/4 v1, 0x4

    iget-object v2, p0, Ldvt;->e:Ldqx;

    .line 16361
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16363
    :cond_3
    iget-object v1, p0, Ldvt;->f:Ldqy;

    if-eqz v1, :cond_4

    .line 16364
    const/4 v1, 0x5

    iget-object v2, p0, Ldvt;->f:Ldqy;

    .line 16365
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16367
    :cond_4
    iget-object v1, p0, Ldvt;->g:Ldvq;

    if-eqz v1, :cond_5

    .line 16368
    const/4 v1, 0x6

    iget-object v2, p0, Ldvt;->g:Ldvq;

    .line 16369
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16371
    :cond_5
    iget-object v1, p0, Ldvt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16372
    iput v0, p0, Ldvt;->cachedSize:I

    .line 16373
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 16295
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvt;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvt;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvt;->b:Ldpu;

    if-nez v0, :cond_2

    new-instance v0, Ldpu;

    invoke-direct {v0}, Ldpu;-><init>()V

    iput-object v0, p0, Ldvt;->b:Ldpu;

    :cond_2
    iget-object v0, p0, Ldvt;->b:Ldpu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldvt;->c:Ldut;

    if-nez v0, :cond_3

    new-instance v0, Ldut;

    invoke-direct {v0}, Ldut;-><init>()V

    iput-object v0, p0, Ldvt;->c:Ldut;

    :cond_3
    iget-object v0, p0, Ldvt;->c:Ldut;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldvt;->d:Ldrg;

    if-nez v0, :cond_4

    new-instance v0, Ldrg;

    invoke-direct {v0}, Ldrg;-><init>()V

    iput-object v0, p0, Ldvt;->d:Ldrg;

    :cond_4
    iget-object v0, p0, Ldvt;->d:Ldrg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldvt;->e:Ldqx;

    if-nez v0, :cond_5

    new-instance v0, Ldqx;

    invoke-direct {v0}, Ldqx;-><init>()V

    iput-object v0, p0, Ldvt;->e:Ldqx;

    :cond_5
    iget-object v0, p0, Ldvt;->e:Ldqx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldvt;->f:Ldqy;

    if-nez v0, :cond_6

    new-instance v0, Ldqy;

    invoke-direct {v0}, Ldqy;-><init>()V

    iput-object v0, p0, Ldvt;->f:Ldqy;

    :cond_6
    iget-object v0, p0, Ldvt;->f:Ldqy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldvt;->g:Ldvq;

    if-nez v0, :cond_7

    new-instance v0, Ldvq;

    invoke-direct {v0}, Ldvq;-><init>()V

    iput-object v0, p0, Ldvt;->g:Ldvq;

    :cond_7
    iget-object v0, p0, Ldvt;->g:Ldvq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 16322
    iget-object v0, p0, Ldvt;->b:Ldpu;

    if-eqz v0, :cond_0

    .line 16323
    const/4 v0, 0x1

    iget-object v1, p0, Ldvt;->b:Ldpu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16325
    :cond_0
    iget-object v0, p0, Ldvt;->c:Ldut;

    if-eqz v0, :cond_1

    .line 16326
    const/4 v0, 0x2

    iget-object v1, p0, Ldvt;->c:Ldut;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16328
    :cond_1
    iget-object v0, p0, Ldvt;->d:Ldrg;

    if-eqz v0, :cond_2

    .line 16329
    const/4 v0, 0x3

    iget-object v1, p0, Ldvt;->d:Ldrg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16331
    :cond_2
    iget-object v0, p0, Ldvt;->e:Ldqx;

    if-eqz v0, :cond_3

    .line 16332
    const/4 v0, 0x4

    iget-object v1, p0, Ldvt;->e:Ldqx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16334
    :cond_3
    iget-object v0, p0, Ldvt;->f:Ldqy;

    if-eqz v0, :cond_4

    .line 16335
    const/4 v0, 0x5

    iget-object v1, p0, Ldvt;->f:Ldqy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16337
    :cond_4
    iget-object v0, p0, Ldvt;->g:Ldvq;

    if-eqz v0, :cond_5

    .line 16338
    const/4 v0, 0x6

    iget-object v1, p0, Ldvt;->g:Ldvq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16340
    :cond_5
    iget-object v0, p0, Ldvt;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 16342
    return-void
.end method
