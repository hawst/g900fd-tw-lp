.class public final Leom;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leom;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Leon;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    new-array v0, v0, [Leom;

    sput-object v0, Leom;->a:[Leom;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lepn;-><init>()V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Leom;->c:Leon;

    .line 104
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 186
    const/4 v0, 0x0

    .line 187
    iget-object v1, p0, Leom;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 188
    const/4 v0, 0x1

    iget-object v1, p0, Leom;->b:Ljava/lang/Boolean;

    .line 189
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 191
    :cond_0
    iget-object v1, p0, Leom;->c:Leon;

    if-eqz v1, :cond_1

    .line 192
    const/4 v1, 0x2

    iget-object v2, p0, Leom;->c:Leon;

    .line 193
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_1
    iget-object v1, p0, Leom;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    iput v0, p0, Leom;->cachedSize:I

    .line 197
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 100
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leom;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leom;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leom;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leom;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leom;->c:Leon;

    if-nez v0, :cond_2

    new-instance v0, Leon;

    invoke-direct {v0}, Leon;-><init>()V

    iput-object v0, p0, Leom;->c:Leon;

    :cond_2
    iget-object v0, p0, Leom;->c:Leon;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Leom;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 175
    const/4 v0, 0x1

    iget-object v1, p0, Leom;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 177
    :cond_0
    iget-object v0, p0, Leom;->c:Leon;

    if-eqz v0, :cond_1

    .line 178
    const/4 v0, 0x2

    iget-object v1, p0, Leom;->c:Leon;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 180
    :cond_1
    iget-object v0, p0, Leom;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 182
    return-void
.end method
