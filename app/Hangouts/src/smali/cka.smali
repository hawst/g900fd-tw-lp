.class public interface abstract Lcka;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Landroid/location/Location;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract a(JZLandroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;Lcjx;Ljava/lang/String;)V
.end method

.method public abstract a(Landroid/location/Location;)V
.end method

.method public abstract a(Landroid/location/Location;I)V
.end method

.method public abstract a(Lcjx;Ljava/lang/String;)V
.end method

.method public abstract a(Lcog;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nh;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nh;Lcog;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nt;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lcog;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lcog;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/internal/nt;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nt;Lckl;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/internal/nt;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/internal/nt;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/internal/nt;Lckl;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/internal/nt;Lckl;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/location/places/UserDataType;",
            "Lcom/google/android/gms/maps/model/LatLngBounds;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/nt;",
            "Lckl;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nt;Lckl;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nt;Lckl;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nt;Lckl;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/internal/nt;Lckl;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/internal/nt;Lckl;)V
.end method

.method public abstract a(Ljava/util/List;Landroid/app/PendingIntent;Lcjx;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/nj;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Lcjx;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Z)V
.end method

.method public abstract a([Ljava/lang/String;Lcjx;Ljava/lang/String;)V
.end method

.method public abstract b()Landroid/os/IBinder;
.end method

.method public abstract b(Ljava/lang/String;)Lcom/google/android/gms/location/LocationStatus;
.end method

.method public abstract b(Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Lcom/google/android/gms/internal/nt;Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/gms/internal/nt;Lckl;)V
.end method

.method public abstract c()Landroid/os/IBinder;
.end method
