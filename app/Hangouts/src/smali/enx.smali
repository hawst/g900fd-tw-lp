.class public final Lenx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lenx;


# instance fields
.field public b:Lenw;

.field public c:Lenw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    new-array v0, v0, [Lenx;

    sput-object v0, Lenx;->a:[Lenx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Lepn;-><init>()V

    .line 86
    iput-object v0, p0, Lenx;->b:Lenw;

    .line 89
    iput-object v0, p0, Lenx;->c:Lenw;

    .line 83
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 106
    const/4 v0, 0x0

    .line 107
    iget-object v1, p0, Lenx;->b:Lenw;

    if-eqz v1, :cond_0

    .line 108
    const/4 v0, 0x1

    iget-object v1, p0, Lenx;->b:Lenw;

    .line 109
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 111
    :cond_0
    iget-object v1, p0, Lenx;->c:Lenw;

    if-eqz v1, :cond_1

    .line 112
    const/4 v1, 0x2

    iget-object v2, p0, Lenx;->c:Lenw;

    .line 113
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_1
    iget-object v1, p0, Lenx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    iput v0, p0, Lenx;->cachedSize:I

    .line 117
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lenx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lenx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lenx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lenx;->b:Lenw;

    if-nez v0, :cond_2

    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lenx;->b:Lenw;

    :cond_2
    iget-object v0, p0, Lenx;->b:Lenw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lenx;->c:Lenw;

    if-nez v0, :cond_3

    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lenx;->c:Lenw;

    :cond_3
    iget-object v0, p0, Lenx;->c:Lenw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lenx;->b:Lenw;

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    iget-object v1, p0, Lenx;->b:Lenw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 97
    :cond_0
    iget-object v0, p0, Lenx;->c:Lenw;

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x2

    iget-object v1, p0, Lenx;->c:Lenw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 100
    :cond_1
    iget-object v0, p0, Lenx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 102
    return-void
.end method
