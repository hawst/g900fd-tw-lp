.class public Lbfl;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:I

.field public final b:Z


# direct methods
.method public constructor <init>(ZI)V
    .locals 0

    .prologue
    .line 4221
    invoke-direct {p0}, Lbep;-><init>()V

    .line 4222
    iput p2, p0, Lbfl;->a:I

    .line 4223
    iput-boolean p1, p0, Lbfl;->b:Z

    .line 4224
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4228
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 4229
    const-string v2, "Babel_RequestWriter"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "SetInCallPresenceRequest build protobuf "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lbfl;->a:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v0, p0, Lbfl;->b:Z

    if-eqz v0, :cond_1

    const-string v0, " in a call."

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4233
    :cond_0
    new-instance v2, Ldwk;

    invoke-direct {v2}, Ldwk;-><init>()V

    .line 4235
    new-instance v3, Ldta;

    invoke-direct {v3}, Ldta;-><init>()V

    .line 4237
    iget-boolean v0, p0, Lbfl;->b:Z

    if-eqz v0, :cond_3

    .line 4238
    iget v0, p0, Lbfl;->a:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 4239
    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldta;->b:Ljava/lang/Integer;

    .line 4240
    iget v0, p0, Lbfl;->a:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v3, Ldta;->c:Ljava/lang/Long;

    .line 4245
    :goto_2
    iput-object v3, v2, Ldwk;->g:Ldta;

    .line 4246
    invoke-virtual {p0, p1, p2}, Lbfl;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v2, Ldwk;->b:Ldvm;

    .line 4247
    return-object v2

    .line 4229
    :cond_1
    const-string v0, " NOT in a call."

    goto :goto_0

    :cond_2
    move v0, v1

    .line 4238
    goto :goto_1

    .line 4242
    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldta;->b:Ljava/lang/Integer;

    goto :goto_2
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 4262
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4252
    const-string v0, "presence/setpresence"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4257
    const-string v0, "event_queue"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 4267
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v1, p0, Lbfl;->a:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
