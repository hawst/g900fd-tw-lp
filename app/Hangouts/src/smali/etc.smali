.class public final Letc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Letc;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:[Letd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 565
    const/4 v0, 0x0

    new-array v0, v0, [Letc;

    sput-object v0, Letc;->a:[Letc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 566
    invoke-direct {p0}, Lepn;-><init>()V

    .line 645
    sget-object v0, Letd;->a:[Letd;

    iput-object v0, p0, Letc;->c:[Letd;

    .line 566
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 667
    iget-object v0, p0, Letc;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 668
    const/4 v0, 0x1

    iget-object v2, p0, Letc;->b:Ljava/lang/Boolean;

    .line 669
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 671
    :goto_0
    iget-object v2, p0, Letc;->c:[Letd;

    if-eqz v2, :cond_1

    .line 672
    iget-object v2, p0, Letc;->c:[Letd;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 673
    if-eqz v4, :cond_0

    .line 674
    const/4 v5, 0x2

    .line 675
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 672
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 679
    :cond_1
    iget-object v1, p0, Letc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 680
    iput v0, p0, Letc;->cachedSize:I

    .line 681
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 562
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Letc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Letc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Letc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Letc;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Letc;->c:[Letd;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Letd;

    iget-object v3, p0, Letc;->c:[Letd;

    if-eqz v3, :cond_2

    iget-object v3, p0, Letc;->c:[Letd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Letc;->c:[Letd;

    :goto_2
    iget-object v2, p0, Letc;->c:[Letd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Letc;->c:[Letd;

    new-instance v3, Letd;

    invoke-direct {v3}, Letd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Letc;->c:[Letd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Letc;->c:[Letd;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Letc;->c:[Letd;

    new-instance v3, Letd;

    invoke-direct {v3}, Letd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Letc;->c:[Letd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 650
    iget-object v0, p0, Letc;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 651
    const/4 v0, 0x1

    iget-object v1, p0, Letc;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 653
    :cond_0
    iget-object v0, p0, Letc;->c:[Letd;

    if-eqz v0, :cond_2

    .line 654
    iget-object v1, p0, Letc;->c:[Letd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 655
    if-eqz v3, :cond_1

    .line 656
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 654
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 660
    :cond_2
    iget-object v0, p0, Letc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 662
    return-void
.end method
