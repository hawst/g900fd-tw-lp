.class final Lkx;
.super Lkv;
.source "PG"

# interfaces
.implements Lol;


# instance fields
.field final d:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

.field e:Landroid/view/ActionMode;


# direct methods
.method public constructor <init>(Lkj;Lke;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lkv;-><init>(Lkj;Lke;)V

    .line 35
    sget v0, Lf;->D:I

    invoke-virtual {p1, v0}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    iput-object v0, p0, Lkx;->d:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    .line 39
    iget-object v0, p0, Lkx;->d:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lkx;->d:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;->a(Lol;)V

    .line 42
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode$Callback;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lky;

    invoke-direct {v0, p0, p1}, Lky;-><init>(Lkx;Landroid/view/ActionMode$Callback;)V

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lkv;->f()V

    .line 53
    iget-object v0, p0, Lkx;->e:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lkx;->e:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 56
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lkv;->g()V

    .line 61
    iget-object v0, p0, Lkx;->e:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lkx;->e:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 64
    :cond_0
    return-void
.end method

.method o()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lkx;->e:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    invoke-super {p0}, Lkv;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
