.class public final Lbxl;
.super Ljava/lang/Exception;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public a:Landroid/content/Intent;

.field public b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lbxl;->a:Landroid/content/Intent;

    .line 99
    iput p1, p0, Lbxl;->b:I

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 88
    iput-object p1, p0, Lbxl;->a:Landroid/content/Intent;

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lbxl;->b:I

    .line 91
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lbxl;->b:I

    .line 112
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lbxl;->b:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 108
    return-void
.end method
