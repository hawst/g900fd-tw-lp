.class final Lats;
.super Latf;
.source "PG"


# instance fields
.field final synthetic b:Latp;


# direct methods
.method private constructor <init>(Latp;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lats;->b:Latp;

    invoke-direct {p0, p1}, Latf;-><init>(Late;)V

    return-void
.end method

.method synthetic constructor <init>(Latp;B)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lats;-><init>(Latp;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lats;->b:Latp;

    iget-object v0, v0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lats;->b:Latp;

    iget-object v0, v0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    .line 94
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getPstnJid()Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-static {v0, p1}, Lf;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lats;->b:Latp;

    invoke-static {v0}, Latp;->b(Latp;)V

    .line 97
    :cond_0
    return-void
.end method

.method public onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;

    if-eqz v0, :cond_1

    move-object v0, p2

    .line 62
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;

    .line 63
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;->getBlocker()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lats;->b:Latp;

    iget-object v1, v1, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-ne v0, v1, :cond_1

    .line 65
    iget-object v0, p0, Lats;->b:Latp;

    invoke-static {v0}, Latp;->a(Latp;)V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v0, p0, Lats;->b:Latp;

    iget-object v0, v0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-ne p1, v0, :cond_0

    .line 74
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;

    if-eqz v0, :cond_3

    .line 75
    check-cast p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;

    .line 76
    iget v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->type:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    iget v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 78
    :cond_2
    iget-object v0, p0, Lats;->b:Latp;

    invoke-static {v0}, Latp;->a(Latp;)V

    goto :goto_0

    .line 80
    :cond_3
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/VideoMuteEvent;

    if-eqz v0, :cond_4

    .line 81
    iget-object v0, p0, Lats;->b:Latp;

    invoke-static {v0}, Latp;->a(Latp;)V

    goto :goto_0

    .line 82
    :cond_4
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;

    if-eqz v0, :cond_5

    .line 83
    iget-object v0, p0, Lats;->b:Latp;

    invoke-static {v0}, Latp;->a(Latp;)V

    goto :goto_0

    .line 84
    :cond_5
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;

    if-eqz v0, :cond_6

    .line 85
    iget-object v0, p0, Lats;->b:Latp;

    invoke-virtual {v0}, Latp;->z()V

    goto :goto_0

    .line 86
    :cond_6
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lats;->b:Latp;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Latp;->c(Z)V

    goto :goto_0
.end method
