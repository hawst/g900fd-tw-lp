.class final Lxk;
.super Landroid/os/Handler;
.source "PG"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Lxk;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11

    .prologue
    const-wide/16 v9, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 573
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 577
    :pswitch_0
    invoke-static {}, Lxj;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    invoke-static {}, Lxj;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    :cond_1
    move v2, v4

    .line 585
    :cond_2
    :goto_1
    invoke-static {}, Lxj;->l()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 588
    invoke-static {}, Lxj;->l()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 589
    invoke-static {}, Lxj;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 590
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v4

    .line 591
    :goto_2
    if-ge v5, v6, :cond_2

    .line 592
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxj;

    .line 594
    invoke-static {v1}, Lxj;->a(Lxj;)J

    move-result-wide v7

    cmp-long v7, v7, v9

    if-eqz v7, :cond_3

    invoke-static {v1}, Lxj;->b(Lxj;)I

    move-result v7

    const/4 v8, 0x3

    if-eq v7, v8, :cond_3

    .line 595
    invoke-static {v1}, Lxj;->b(Lxj;)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_4

    .line 596
    :cond_3
    invoke-static {v1}, Lxj;->c(Lxj;)V

    .line 591
    :goto_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    .line 598
    :cond_4
    invoke-static {}, Lxj;->k()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :pswitch_1
    move v2, v3

    .line 607
    :cond_5
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v5

    .line 611
    invoke-static {}, Lxj;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v1, v4

    .line 612
    :goto_4
    if-ge v1, v7, :cond_7

    .line 613
    invoke-static {}, Lxj;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 614
    invoke-static {v0, v5, v6}, Lxj;->a(Lxj;J)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 615
    invoke-static {}, Lxj;->m()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 612
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 618
    :cond_7
    invoke-static {}, Lxj;->m()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 619
    if-lez v7, :cond_9

    move v1, v4

    .line 620
    :goto_5
    if-ge v1, v7, :cond_8

    .line 621
    invoke-static {}, Lxj;->m()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 622
    invoke-static {v0}, Lxj;->c(Lxj;)V

    .line 623
    invoke-static {}, Lxj;->k()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 620
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 625
    :cond_8
    invoke-static {}, Lxj;->m()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 630
    :cond_9
    invoke-static {}, Lxj;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v1, v4

    .line 631
    :goto_6
    if-ge v1, v7, :cond_b

    .line 632
    invoke-static {}, Lxj;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 633
    invoke-static {v0, v5, v6}, Lxj;->b(Lxj;J)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 634
    invoke-static {}, Lxj;->n()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 637
    :cond_b
    invoke-static {}, Lxj;->n()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 638
    :goto_7
    invoke-static {}, Lxj;->n()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_c

    .line 639
    invoke-static {}, Lxj;->n()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    invoke-static {v0}, Lxj;->d(Lxj;)V

    .line 638
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 641
    :cond_c
    invoke-static {}, Lxj;->n()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 646
    :cond_d
    if-eqz v2, :cond_0

    invoke-static {}, Lxj;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {}, Lxj;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 647
    :cond_e
    invoke-static {}, Lxj;->o()J

    move-result-wide v0

    .line 648
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v7

    sub-long v4, v7, v5

    sub-long/2addr v0, v4

    .line 647
    invoke-static {v9, v10, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-virtual {p0, v3, v0, v1}, Lxk;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_f
    move v2, v3

    goto/16 :goto_1

    .line 573
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
