.class public Lbgf;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:J

.field private final h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldqv;)V
    .locals 7

    .prologue
    .line 3615
    iget-object v0, p1, Ldqv;->b:Ldvn;

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 3616
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3617
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DeleteConversationResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3619
    :cond_0
    const/4 v0, 0x0

    .line 3620
    const-wide/16 v1, -0x1

    .line 3621
    iget-object v3, p1, Ldqv;->c:Ldqs;

    if-eqz v3, :cond_3

    .line 3622
    iget-object v3, p1, Ldqv;->c:Ldqs;

    iget-object v3, v3, Ldqs;->b:Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 3624
    iget-object v0, p1, Ldqv;->c:Ldqs;

    iget-object v0, v0, Ldqs;->e:[Ljava/lang/String;

    .line 3626
    :cond_1
    iget-object v3, p1, Ldqv;->c:Ldqs;

    iget-object v3, v3, Ldqs;->d:Ljava/lang/Long;

    if-eqz v3, :cond_2

    .line 3627
    iget-object v1, p1, Ldqv;->c:Ldqs;

    iget-object v1, v1, Ldqs;->d:Ljava/lang/Long;

    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    move-wide v5, v1

    move-object v2, v0

    move-wide v0, v5

    .line 3631
    :goto_0
    iput-wide v0, p0, Lbgf;->g:J

    .line 3632
    iput-object v2, p0, Lbgf;->h:[Ljava/lang/String;

    .line 3633
    return-void

    :cond_2
    move-wide v5, v1

    move-object v2, v0

    move-wide v0, v5

    goto :goto_0

    :cond_3
    move-wide v5, v1

    move-object v2, v0

    move-wide v0, v5

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3637
    new-instance v0, Ldqv;

    invoke-direct {v0}, Ldqv;-><init>()V

    .line 3638
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldqv;

    .line 3639
    iget-object v1, v0, Ldqv;->b:Ldvn;

    invoke-static {v1}, Lbgf;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3640
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldqv;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3642
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgf;

    invoke-direct {v1, v0}, Lbgf;-><init>(Ldqv;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 5

    .prologue
    .line 3649
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 3651
    invoke-virtual {p1}, Lyt;->a()V

    .line 3653
    :try_start_0
    iget-object v0, p0, Lbgf;->b:Lbea;

    check-cast v0, Lbef;

    .line 3655
    new-instance v1, Lbku;

    iget-object v0, v0, Lbef;->c:Ljava/lang/String;

    iget-wide v2, p0, Lbgf;->g:J

    iget-object v4, p0, Lbgf;->h:[Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lbku;-><init>(Ljava/lang/String;J[Ljava/lang/String;)V

    .line 3658
    invoke-virtual {v1, p1}, Lbku;->a(Lyt;)V

    .line 3659
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3661
    invoke-virtual {p1}, Lyt;->c()V

    .line 3662
    return-void

    .line 3661
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
.end method
