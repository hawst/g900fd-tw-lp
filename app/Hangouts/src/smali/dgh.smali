.class public final Ldgh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgh;


# instance fields
.field public b:Ldfi;

.field public c:Lden;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6236
    const/4 v0, 0x0

    new-array v0, v0, [Ldgh;

    sput-object v0, Ldgh;->a:[Ldgh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6237
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6264
    iput-object v0, p0, Ldgh;->b:Ldfi;

    .line 6267
    iput-object v0, p0, Ldgh;->c:Lden;

    .line 6272
    iput-object v0, p0, Ldgh;->e:Ljava/lang/Integer;

    .line 6237
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 6295
    const/4 v0, 0x0

    .line 6296
    iget-object v1, p0, Ldgh;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 6297
    const/4 v0, 0x1

    iget-object v1, p0, Ldgh;->b:Ldfi;

    .line 6298
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6300
    :cond_0
    iget-object v1, p0, Ldgh;->c:Lden;

    if-eqz v1, :cond_1

    .line 6301
    const/4 v1, 0x2

    iget-object v2, p0, Ldgh;->c:Lden;

    .line 6302
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6304
    :cond_1
    iget-object v1, p0, Ldgh;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6305
    const/4 v1, 0x3

    iget-object v2, p0, Ldgh;->d:Ljava/lang/String;

    .line 6306
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6308
    :cond_2
    iget-object v1, p0, Ldgh;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 6309
    const/4 v1, 0x4

    iget-object v2, p0, Ldgh;->e:Ljava/lang/Integer;

    .line 6310
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6312
    :cond_3
    iget-object v1, p0, Ldgh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6313
    iput v0, p0, Ldgh;->cachedSize:I

    .line 6314
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6233
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldgh;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldgh;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldgh;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldgh;->c:Lden;

    if-nez v0, :cond_3

    new-instance v0, Lden;

    invoke-direct {v0}, Lden;-><init>()V

    iput-object v0, p0, Ldgh;->c:Lden;

    :cond_3
    iget-object v0, p0, Ldgh;->c:Lden;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgh;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0xf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x10

    if-eq v0, v1, :cond_4

    const/16 v1, 0x11

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12

    if-eq v0, v1, :cond_4

    const/16 v1, 0x13

    if-eq v0, v1, :cond_4

    const/16 v1, 0x14

    if-eq v0, v1, :cond_4

    const/16 v1, 0x15

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgh;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgh;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 6277
    iget-object v0, p0, Ldgh;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 6278
    const/4 v0, 0x1

    iget-object v1, p0, Ldgh;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6280
    :cond_0
    iget-object v0, p0, Ldgh;->c:Lden;

    if-eqz v0, :cond_1

    .line 6281
    const/4 v0, 0x2

    iget-object v1, p0, Ldgh;->c:Lden;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6283
    :cond_1
    iget-object v0, p0, Ldgh;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6284
    const/4 v0, 0x3

    iget-object v1, p0, Ldgh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6286
    :cond_2
    iget-object v0, p0, Ldgh;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 6287
    const/4 v0, 0x4

    iget-object v1, p0, Ldgh;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6289
    :cond_3
    iget-object v0, p0, Ldgh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6291
    return-void
.end method
