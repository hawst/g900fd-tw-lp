.class public final Ldll;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldll;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1793
    const/4 v0, 0x0

    new-array v0, v0, [Ldll;

    sput-object v0, Ldll;->a:[Ldll;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1794
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1802
    const/4 v0, 0x0

    iput-object v0, p0, Ldll;->b:Ljava/lang/Integer;

    .line 1794
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1869
    const/4 v0, 0x1

    iget-object v1, p0, Ldll;->b:Ljava/lang/Integer;

    .line 1871
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1872
    iget-object v1, p0, Ldll;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1873
    const/4 v1, 0x2

    iget-object v2, p0, Ldll;->c:Ljava/lang/String;

    .line 1874
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1876
    :cond_0
    iget-object v1, p0, Ldll;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1877
    const/4 v1, 0x3

    iget-object v2, p0, Ldll;->d:Ljava/lang/String;

    .line 1878
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1880
    :cond_1
    iget-object v1, p0, Ldll;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1881
    const/4 v1, 0x4

    iget-object v2, p0, Ldll;->e:Ljava/lang/String;

    .line 1882
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1884
    :cond_2
    iget-object v1, p0, Ldll;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1885
    const/4 v1, 0x5

    iget-object v2, p0, Ldll;->f:Ljava/lang/String;

    .line 1886
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1888
    :cond_3
    iget-object v1, p0, Ldll;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1889
    const/4 v1, 0x6

    iget-object v2, p0, Ldll;->g:Ljava/lang/String;

    .line 1890
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1892
    :cond_4
    iget-object v1, p0, Ldll;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1893
    const/4 v1, 0x7

    iget-object v2, p0, Ldll;->h:Ljava/lang/String;

    .line 1894
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1896
    :cond_5
    iget-object v1, p0, Ldll;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1897
    const/16 v1, 0x8

    iget-object v2, p0, Ldll;->i:Ljava/lang/String;

    .line 1898
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1900
    :cond_6
    iget-object v1, p0, Ldll;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1901
    const/16 v1, 0x9

    iget-object v2, p0, Ldll;->j:Ljava/lang/String;

    .line 1902
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1904
    :cond_7
    iget-object v1, p0, Ldll;->k:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 1905
    const/16 v1, 0xa

    iget-object v2, p0, Ldll;->k:Ljava/lang/String;

    .line 1906
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1908
    :cond_8
    iget-object v1, p0, Ldll;->l:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1909
    const/16 v1, 0xb

    iget-object v2, p0, Ldll;->l:Ljava/lang/String;

    .line 1910
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1912
    :cond_9
    iget-object v1, p0, Ldll;->m:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 1913
    const/16 v1, 0xc

    iget-object v2, p0, Ldll;->m:Ljava/lang/String;

    .line 1914
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1916
    :cond_a
    iget-object v1, p0, Ldll;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1917
    iput v0, p0, Ldll;->cachedSize:I

    .line 1918
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1790
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldll;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldll;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldll;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldll;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldll;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldll;->m:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1829
    const/4 v0, 0x1

    iget-object v1, p0, Ldll;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1830
    iget-object v0, p0, Ldll;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1831
    const/4 v0, 0x2

    iget-object v1, p0, Ldll;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1833
    :cond_0
    iget-object v0, p0, Ldll;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1834
    const/4 v0, 0x3

    iget-object v1, p0, Ldll;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1836
    :cond_1
    iget-object v0, p0, Ldll;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1837
    const/4 v0, 0x4

    iget-object v1, p0, Ldll;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1839
    :cond_2
    iget-object v0, p0, Ldll;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1840
    const/4 v0, 0x5

    iget-object v1, p0, Ldll;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1842
    :cond_3
    iget-object v0, p0, Ldll;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1843
    const/4 v0, 0x6

    iget-object v1, p0, Ldll;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1845
    :cond_4
    iget-object v0, p0, Ldll;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1846
    const/4 v0, 0x7

    iget-object v1, p0, Ldll;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1848
    :cond_5
    iget-object v0, p0, Ldll;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1849
    const/16 v0, 0x8

    iget-object v1, p0, Ldll;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1851
    :cond_6
    iget-object v0, p0, Ldll;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1852
    const/16 v0, 0x9

    iget-object v1, p0, Ldll;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1854
    :cond_7
    iget-object v0, p0, Ldll;->k:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1855
    const/16 v0, 0xa

    iget-object v1, p0, Ldll;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1857
    :cond_8
    iget-object v0, p0, Ldll;->l:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1858
    const/16 v0, 0xb

    iget-object v1, p0, Ldll;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1860
    :cond_9
    iget-object v0, p0, Ldll;->m:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1861
    const/16 v0, 0xc

    iget-object v1, p0, Ldll;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1863
    :cond_a
    iget-object v0, p0, Ldll;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1865
    return-void
.end method
