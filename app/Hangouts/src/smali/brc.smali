.class public final Lbrc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static volatile b:Z

.field private static volatile c:Lbrc;


# instance fields
.field private d:Ljava/lang/String;

.field private final e:Landroid/os/Handler;

.field private volatile f:J

.field private volatile g:J

.field private volatile h:J

.field private volatile i:J

.field private volatile j:J

.field private volatile k:I

.field private volatile l:I

.field private volatile m:I

.field private volatile n:J

.field private volatile o:I

.field private volatile p:I

.field private volatile q:J

.field private volatile r:Z

.field private volatile s:Z

.field private volatile t:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    sget-object v0, Lbys;->l:Lcyp;

    sput-boolean v1, Lbrc;->a:Z

    .line 37
    sput-boolean v1, Lbrc;->b:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x32

    const-wide/32 v2, 0x240c8400

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbrc;->e:Landroid/os/Handler;

    .line 46
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lbrc;->f:J

    .line 51
    const-wide/32 v0, 0x36ee80

    iput-wide v0, p0, Lbrc;->g:J

    .line 56
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lbrc;->h:J

    .line 60
    iput-wide v2, p0, Lbrc;->i:J

    .line 66
    const-wide/32 v0, 0xa4cb800

    iput-wide v0, p0, Lbrc;->j:J

    .line 71
    iput v4, p0, Lbrc;->k:I

    .line 74
    const/16 v0, 0x64

    iput v0, p0, Lbrc;->l:I

    .line 77
    const/16 v0, 0x1f4

    iput v0, p0, Lbrc;->m:I

    .line 81
    iput-wide v2, p0, Lbrc;->n:J

    .line 86
    iput v4, p0, Lbrc;->o:I

    .line 89
    const/16 v0, 0x7d0

    iput v0, p0, Lbrc;->p:I

    .line 93
    iput-wide v2, p0, Lbrc;->q:J

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbrc;->s:Z

    .line 247
    return-void
.end method

.method public static a(Lyj;)Lbrc;
    .locals 7

    .prologue
    const-wide/32 v5, 0x14997000

    const/4 v4, 0x5

    const/4 v3, 0x2

    .line 171
    new-instance v0, Lbrc;

    invoke-direct {v0}, Lbrc;-><init>()V

    .line 173
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lbrc;->f:J

    .line 174
    const-wide/32 v1, 0x36ee80

    iput-wide v1, v0, Lbrc;->g:J

    .line 175
    const-wide/32 v1, 0x1ee62800

    iput-wide v1, v0, Lbrc;->h:J

    .line 176
    const-wide/32 v1, 0xa4cb800

    iput-wide v1, v0, Lbrc;->j:J

    .line 177
    iput v3, v0, Lbrc;->k:I

    .line 178
    iput v4, v0, Lbrc;->m:I

    .line 179
    iput-wide v5, v0, Lbrc;->n:J

    .line 180
    iput v3, v0, Lbrc;->o:I

    .line 181
    iput v4, v0, Lbrc;->p:I

    .line 182
    iput-wide v5, v0, Lbrc;->q:J

    .line 183
    iput v3, v0, Lbrc;->l:I

    .line 185
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbrc;->d:Ljava/lang/String;

    .line 186
    return-object v0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-static {v0}, Lbrc;->b(Z)V

    .line 107
    return-void
.end method

.method static synthetic a(Lbrc;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lbrc;->g()V

    return-void
.end method

.method private a(Lyt;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 534
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 544
    :goto_0
    return v0

    .line 538
    :cond_0
    invoke-virtual {p1}, Lyt;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 539
    invoke-direct {p0, p1, v0}, Lbrc;->a(Lyt;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 540
    goto :goto_0

    .line 544
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lyt;II)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 456
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 524
    :goto_0
    return v0

    .line 460
    :cond_0
    invoke-virtual {p1}, Lyt;->a()V

    .line 465
    :try_start_0
    iget v0, p0, Lbrc;->m:I

    invoke-virtual {p1, v0, p2}, Lyt;->a(II)Ljava/util/List;

    move-result-object v7

    .line 469
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p3, :cond_1

    .line 470
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    invoke-virtual {p1}, Lyt;->c()V

    move v0, v2

    goto :goto_0

    .line 475
    :cond_1
    add-int/lit8 v0, p3, -0x1

    .line 476
    :try_start_1
    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 475
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 480
    invoke-virtual {p1}, Lyt;->s()J

    move-result-wide v3

    .line 481
    const-wide/16 v8, 0x0

    cmp-long v0, v3, v8

    if-eqz v0, :cond_6

    cmp-long v0, v5, v3

    if-lez v0, :cond_6

    .line 486
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v8, p0, Lbrc;->n:J

    sub-long/2addr v5, v8

    const-wide/16 v8, 0x3e8

    mul-long/2addr v5, v8

    .line 488
    cmp-long v0, v3, v5

    if-lez v0, :cond_5

    .line 492
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    iget v3, p0, Lbrc;->m:I

    if-lt v0, v3, :cond_4

    .line 493
    iget v0, p0, Lbrc;->m:I

    add-int/lit8 v0, v0, -0x1

    .line 494
    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 493
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    .line 497
    cmp-long v0, v5, v3

    if-gez v0, :cond_4

    .line 503
    :goto_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 502
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 504
    cmp-long v0, v3, v5

    if-gez v0, :cond_2

    .line 508
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 509
    invoke-virtual {p1}, Lyt;->c()V

    move v0, v2

    goto :goto_0

    .line 513
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v0

    if-nez v0, :cond_3

    .line 514
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 515
    invoke-virtual {p1}, Lyt;->c()V

    move v0, v1

    goto/16 :goto_0

    .line 518
    :cond_3
    :try_start_3
    invoke-virtual {p1, v3, v4, p2}, Lyt;->a(JI)Z

    .line 519
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 521
    invoke-virtual {p1}, Lyt;->c()V

    move v0, v2

    .line 524
    goto/16 :goto_0

    .line 521
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0

    :cond_4
    move-wide v3, v5

    goto :goto_3

    :cond_5
    move-wide v5, v3

    goto :goto_2

    :cond_6
    move-wide v3, v5

    goto :goto_1
.end method

.method private a(Lyt;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 554
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 626
    :goto_0
    return v0

    .line 558
    :cond_0
    invoke-virtual {p1}, Lyt;->a()V

    .line 563
    :try_start_0
    iget v0, p0, Lbrc;->p:I

    invoke-virtual {p1, p2, v0}, Lyt;->d(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v7

    .line 567
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    iget v3, p0, Lbrc;->o:I

    if-gt v0, v3, :cond_1

    .line 568
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 569
    invoke-virtual {p1}, Lyt;->c()V

    move v0, v2

    goto :goto_0

    .line 573
    :cond_1
    :try_start_1
    iget v0, p0, Lbrc;->o:I

    add-int/lit8 v0, v0, -0x1

    .line 574
    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 573
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 578
    invoke-virtual {p1, p2}, Lyt;->T(Ljava/lang/String;)J

    move-result-wide v3

    .line 579
    const-wide/16 v8, 0x0

    cmp-long v0, v3, v8

    if-eqz v0, :cond_7

    cmp-long v0, v5, v3

    if-lez v0, :cond_7

    .line 584
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v8, p0, Lbrc;->q:J

    sub-long/2addr v5, v8

    const-wide/16 v8, 0x3e8

    mul-long/2addr v5, v8

    .line 585
    cmp-long v0, v3, v5

    if-lez v0, :cond_6

    .line 589
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    iget v3, p0, Lbrc;->p:I

    if-lt v0, v3, :cond_5

    .line 590
    iget v0, p0, Lbrc;->p:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    .line 594
    cmp-long v0, v5, v3

    if-gez v0, :cond_5

    .line 600
    :goto_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 599
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 601
    cmp-long v0, v3, v5

    if-gez v0, :cond_2

    .line 605
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 606
    invoke-virtual {p1}, Lyt;->c()V

    move v0, v2

    goto :goto_0

    .line 610
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v0

    if-nez v0, :cond_3

    .line 611
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 612
    invoke-virtual {p1}, Lyt;->c()V

    move v0, v1

    goto/16 :goto_0

    .line 615
    :cond_3
    :try_start_3
    invoke-virtual {p1, p2, v3, v4}, Lyt;->l(Ljava/lang/String;J)I

    move-result v0

    .line 616
    sget-boolean v1, Lbrc;->a:Z

    if-eqz v1, :cond_4

    .line 617
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Deleted "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " old messages. conversationId="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :cond_4
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 623
    invoke-virtual {p1}, Lyt;->c()V

    move v0, v2

    .line 626
    goto/16 :goto_0

    .line 623
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0

    :cond_5
    move-wide v3, v5

    goto :goto_3

    :cond_6
    move-wide v5, v3

    goto :goto_2

    :cond_7
    move-wide v3, v5

    goto/16 :goto_1
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    sput-boolean v0, Lbrc;->b:Z

    .line 111
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f()V

    .line 112
    return-void
.end method

.method private static b(Z)V
    .locals 6

    .prologue
    const-wide/32 v4, 0xea60

    .line 120
    sget-boolean v0, Lbrc;->b:Z

    if-eqz v0, :cond_0

    if-eqz p0, :cond_2

    .line 121
    :cond_0
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Scheduling database cleanup. forceReschedule="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_1
    const-string v0, "babel_gc_next_start"

    const/16 v1, 0x3c

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v4

    .line 129
    const-string v2, "babel_gc_interval"

    const/16 v3, 0x5a0

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v4

    .line 134
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 133
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(JJ)V

    .line 137
    const/4 v0, 0x1

    sput-boolean v0, Lbrc;->b:Z

    .line 139
    :cond_2
    return-void
.end method

.method public static c()Lbrc;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lbrc;->c:Lbrc;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lbrc;

    invoke-direct {v0}, Lbrc;-><init>()V

    .line 146
    sput-object v0, Lbrc;->c:Lbrc;

    invoke-direct {v0}, Lbrc;->g()V

    .line 149
    new-instance v0, Lbrd;

    invoke-direct {v0}, Lbrd;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 157
    :cond_0
    sget-object v0, Lbrc;->c:Lbrc;

    return-object v0
.end method

.method static synthetic f()Lbrc;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lbrc;->c:Lbrc;

    return-object v0
.end method

.method private g()V
    .locals 6

    .prologue
    const/16 v5, 0x32

    const/16 v4, 0x2760

    const-wide/32 v2, 0xea60

    .line 191
    const-string v0, "babel_gc_minimum_idle"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbrc;->f:J

    .line 196
    const-string v0, "babel_gc_message_expiration_slop"

    const/16 v1, 0x3c

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbrc;->g:J

    .line 201
    const-string v0, "babel_gc_empty_conversation_slop"

    const/16 v1, 0x5a0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbrc;->h:J

    .line 206
    const-string v0, "babel_gc_expired_invitation_slop"

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbrc;->i:J

    .line 211
    const-string v0, "babel_gc_recent_scroll_slot"

    const/16 v1, 0xb40

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbrc;->j:J

    .line 216
    const-string v0, "babel_gc_min_conversations_to_keep"

    invoke-static {v0, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbrc;->k:I

    .line 219
    const-string v0, "babel_gc_max_conversations_to_keep"

    const/16 v1, 0x1f4

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbrc;->m:I

    .line 222
    const-string v0, "babel_gc_recent_conversation_slop"

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbrc;->n:J

    .line 226
    const-string v0, "babel_gc_min_invites_to_keep"

    const/16 v1, 0x64

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbrc;->l:I

    .line 230
    const-string v0, "babel_gc_min_messages_to_keep"

    invoke-static {v0, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbrc;->o:I

    .line 233
    const-string v0, "babel_gc_max_messages_to_keep"

    const/16 v1, 0x7d0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbrc;->p:I

    .line 236
    const-string v0, "babel_gc_recent_message_slop"

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbrc;->q:J

    .line 242
    const/4 v0, 0x1

    invoke-static {v0}, Lbrc;->b(Z)V

    .line 243
    return-void
.end method

.method private h()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 343
    iget-boolean v1, p0, Lbrc;->s:Z

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lbrc;->t:J

    iget-wide v5, p0, Lbrc;->f:J

    add-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    .line 345
    :cond_0
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 346
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Database clean up service okToRun check failed. mOkToRun="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lbrc;->s:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mLastBabelIdleTimestamp="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lbrc;->t:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_1
    invoke-static {v0}, Lbrc;->b(Z)V

    .line 352
    const/4 v0, 0x0

    .line 355
    :cond_2
    return v0
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    .line 332
    iput-boolean p1, p0, Lbrc;->s:Z

    .line 334
    if-eqz p1, :cond_0

    .line 335
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbrc;->t:J

    .line 337
    :cond_0
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 250
    iget-wide v0, p0, Lbrc;->j:J

    return-wide v0
.end method

.method public e()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 258
    iget-boolean v0, p0, Lbrc;->r:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lbrc;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iput-boolean v3, p0, Lbrc;->r:Z

    .line 265
    iget-object v0, p0, Lbrc;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 266
    invoke-static {}, Lbkb;->B()Ljava/util/List;

    move-result-object v0

    .line 273
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 274
    sget-boolean v1, Lbrc;->a:Z

    if-eqz v1, :cond_3

    .line 275
    const-string v1, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Starting database clean up for account: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_3
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 279
    if-eqz v1, :cond_2

    .line 280
    new-instance v5, Lyt;

    invoke-direct {v5, v1}, Lyt;-><init>(Lyj;)V

    .line 285
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_3
    if-eqz v1, :cond_d

    .line 286
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v1

    if-nez v1, :cond_7

    move v1, v2

    :goto_4
    if-eqz v1, :cond_d

    .line 290
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    if-eqz v1, :cond_d

    .line 294
    invoke-direct {p0}, Lbrc;->h()Z

    move-result v1

    if-nez v1, :cond_b

    move v1, v2

    :goto_6
    if-eqz v1, :cond_d

    .line 298
    const/4 v1, 0x2

    iget v6, p0, Lbrc;->k:I

    invoke-direct {p0, v5, v1, v6}, Lbrc;->a(Lyt;II)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 304
    iget v1, p0, Lbrc;->l:I

    invoke-direct {p0, v5, v3, v1}, Lbrc;->a(Lyt;II)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 312
    invoke-direct {p0, v5}, Lbrc;->a(Lyt;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 316
    sget-boolean v1, Lbrc;->a:Z

    if-eqz v1, :cond_2

    .line 320
    const-string v1, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Finished database clean up for account: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 269
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 270
    iget-object v1, p0, Lbrc;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 285
    :cond_5
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lyt;->i(J)I

    move-result v1

    sget-boolean v6, Lbrc;->a:Z

    if-eqz v6, :cond_6

    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Deleted "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " expired event suggestions."

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v1, v3

    goto/16 :goto_3

    .line 286
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lbrc;->g:J

    sub-long/2addr v6, v8

    mul-long/2addr v6, v10

    invoke-virtual {v5, v6, v7}, Lyt;->h(J)I

    move-result v1

    sget-boolean v6, Lbrc;->a:Z

    if-eqz v6, :cond_8

    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Deleted "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " expired OTR messages."

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move v1, v3

    goto/16 :goto_4

    .line 290
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lbrc;->h:J

    sub-long/2addr v6, v8

    mul-long/2addr v6, v10

    invoke-virtual {v5, v6, v7}, Lyt;->j(J)I

    move-result v1

    sget-boolean v6, Lbrc;->a:Z

    if-eqz v6, :cond_a

    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Deleted "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " empty conversations."

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move v1, v3

    goto/16 :goto_5

    .line 294
    :cond_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lbrc;->i:J

    sub-long/2addr v6, v8

    mul-long/2addr v6, v10

    invoke-virtual {v5, v6, v7}, Lyt;->k(J)I

    move-result v1

    sget-boolean v6, Lbrc;->a:Z

    if-eqz v6, :cond_c

    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Deleted "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " expired invitations."

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move v1, v3

    goto/16 :goto_6

    .line 324
    :cond_d
    iput-boolean v2, p0, Lbrc;->r:Z

    goto/16 :goto_0
.end method
