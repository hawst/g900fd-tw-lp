.class public Lbgb;
.super Lbgj;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbcw;",
            ">;"
        }
    .end annotation
.end field

.field private final i:J


# direct methods
.method private constructor <init>(Ldpi;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1026
    iget-object v1, p1, Ldpi;->b:Ldvn;

    iget-object v0, p1, Ldpi;->d:Ldrr;

    iget-object v0, v0, Ldrr;->d:Ljava/lang/Long;

    .line 1027
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v2

    iget-object v0, p1, Ldpi;->d:Ldrr;

    iget-object v0, v0, Ldrr;->o:Ljava/lang/Long;

    .line 1028
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    iget-object v0, p1, Ldpi;->d:Ldrr;

    iget-object v6, v0, Ldrr;->e:Ljava/lang/String;

    move-object v0, p0

    .line 1026
    invoke-direct/range {v0 .. v6}, Lbgj;-><init>(Ldvn;JJLjava/lang/String;)V

    .line 1030
    iget-object v0, p1, Ldpi;->c:[Ldtg;

    invoke-static {v0}, Lbcw;->a([Ldtg;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbgb;->h:Ljava/util/List;

    .line 1031
    iget-object v0, p1, Ldpi;->d:Ldrr;

    iget-object v0, v0, Ldrr;->o:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbgb;->i:J

    .line 1033
    iget-object v0, p1, Ldpi;->d:Ldrr;

    iget-object v0, v0, Ldrr;->i:Ldtw;

    if-eqz v0, :cond_1

    .line 1034
    iget-object v0, p1, Ldpi;->d:Ldrr;

    iget-object v0, v0, Ldrr;->i:Ldtw;

    iget-object v0, v0, Ldtw;->c:[Ldui;

    invoke-static {v0, v7}, Lbdk;->a([Ldui;[Ldqh;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbgb;->g:Ljava/util/List;

    .line 1040
    :goto_0
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 1041
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AddUserResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    :cond_0
    return-void

    .line 1037
    :cond_1
    iput-object v7, p0, Lbgb;->g:Ljava/util/List;

    goto :goto_0
.end method

.method private a(Lyt;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 1141
    const/4 v0, 0x0

    .line 1143
    new-instance v11, Ljava/util/ArrayList;

    iget-object v1, p0, Lbgb;->h:Ljava/util/List;

    .line 1144
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v11, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1145
    iget-object v1, p0, Lbgb;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v1, v0

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbcw;

    .line 1146
    iget v0, v6, Lbcw;->b:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    .line 1147
    const/4 v10, 0x1

    .line 1148
    iget-object v0, v6, Lbcw;->a:Lbcx;

    iget-object v0, v0, Lbcx;->a:Ljava/lang/String;

    iget-object v1, v6, Lbcw;->a:Lbcx;

    iget-object v1, v1, Lbcx;->b:Ljava/lang/String;

    iget-object v2, v6, Lbcw;->a:Lbcx;

    iget-object v2, v2, Lbcx;->c:Ljava/lang/String;

    iget-object v3, v6, Lbcw;->a:Lbcx;

    iget-object v3, v3, Lbcx;->d:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, v6, Lbcw;->a:Lbcx;

    iget-object v6, v6, Lbcx;->e:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 1158
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 1148
    invoke-static/range {v0 .. v9}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v10

    :goto_1
    move v1, v0

    .line 1161
    goto :goto_0

    .line 1162
    :cond_0
    if-nez v1, :cond_1

    .line 1167
    :goto_2
    return-void

    .line 1165
    :cond_1
    invoke-static {p1, p2, v11}, Lyp;->a(Lyt;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static parseFrom([B)Lbfz;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1047
    new-instance v0, Ldpi;

    invoke-direct {v0}, Ldpi;-><init>()V

    .line 1048
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldpi;

    .line 1049
    iget-object v1, v0, Ldpi;->b:Ldvn;

    iget-object v1, v1, Ldvn;->b:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    if-ne v1, v5, :cond_1

    iget-object v1, v0, Ldpi;->c:[Ldtg;

    invoke-static {v1}, Lbcw;->a([Ldtg;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbcw;

    iget v1, v1, Lbcw;->b:I

    if-ne v1, v5, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    .line 1054
    iget-object v1, v0, Ldpi;->b:Ldvn;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldvn;->b:Ljava/lang/Integer;

    .line 1056
    new-instance v1, Ldrr;

    invoke-direct {v1}, Ldrr;-><init>()V

    iput-object v1, v0, Ldpi;->d:Ldrr;

    .line 1057
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    .line 1059
    iget-object v3, v0, Ldpi;->d:Ldrr;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Ldrr;->d:Ljava/lang/Long;

    .line 1060
    iget-object v3, v0, Ldpi;->d:Ldrr;

    const-wide v4, 0x141dd76000L

    add-long/2addr v1, v4

    .line 1061
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v3, Ldrr;->o:Ljava/lang/Long;

    .line 1062
    new-instance v1, Lbgb;

    invoke-direct {v1, v0}, Lbgb;-><init>(Ldpi;)V

    move-object v0, v1

    .line 1066
    :goto_1
    return-object v0

    :cond_1
    move v1, v3

    .line 1049
    goto :goto_0

    .line 1063
    :cond_2
    iget-object v1, v0, Ldpi;->b:Ldvn;

    invoke-static {v1}, Lbgb;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1064
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldpi;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    goto :goto_1

    .line 1066
    :cond_3
    new-instance v1, Lbgb;

    invoke-direct {v1, v0}, Lbgb;-><init>(Ldpi;)V

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 10

    .prologue
    .line 1096
    invoke-super {p0, p1, p2}, Lbgj;->a(Lyt;Lbnl;)V

    .line 1097
    sget-boolean v0, Lyp;->a:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 1098
    :cond_0
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processAddUserResponse ClientContactError Count: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbgb;->h:Ljava/util/List;

    .line 1099
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1098
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    :cond_1
    iget-object v0, p0, Lbgb;->b:Lbea;

    check-cast v0, Lbeb;

    iget-object v1, v0, Lbeb;->c:Ljava/lang/String;

    .line 1102
    iget-object v0, p0, Lbgb;->b:Lbea;

    check-cast v0, Lbeb;

    iget-wide v2, v0, Lbeb;->b:J

    .line 1103
    invoke-virtual {p1}, Lyt;->a()V

    .line 1105
    if-nez v1, :cond_3

    .line 1106
    :try_start_0
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "attempt to process invite response for a nonexistant conversation id ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1126
    invoke-virtual {p1}, Lyt;->c()V

    .line 1127
    invoke-static {p1, v1}, Lyp;->c(Lyt;Ljava/lang/String;)V

    .line 1128
    invoke-static {p1}, Lyp;->d(Lyt;)V

    .line 1129
    invoke-direct {p0, p1, v1}, Lbgb;->a(Lyt;Ljava/lang/String;)V

    .line 1130
    return-void

    .line 1109
    :cond_3
    :try_start_1
    iget-object v0, p0, Lbgb;->g:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 1110
    iget-object v0, p0, Lbgb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 1111
    const/4 v5, 0x1

    invoke-virtual {p1, v1, v0, v5}, Lyt;->a(Ljava/lang/String;Lbdk;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1126
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    .line 1127
    invoke-static {p1, v1}, Lyp;->c(Lyt;Ljava/lang/String;)V

    .line 1128
    invoke-static {p1}, Lyp;->d(Lyt;)V

    .line 1129
    invoke-direct {p0, p1, v1}, Lbgb;->a(Lyt;Ljava/lang/String;)V

    throw v0

    .line 1115
    :cond_4
    :try_start_2
    iget-wide v4, p0, Lbgb;->d:J

    iget-wide v6, p0, Lbgb;->i:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Lyt;->a(Ljava/lang/String;JJJ)V

    .line 1117
    iget-object v0, p0, Lbgb;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 1119
    iget-object v3, p0, Lbgb;->h:Ljava/util/List;

    iget-wide v4, p0, Lbgb;->d:J

    .line 1120
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iget-wide v6, p0, Lbgb;->d:J

    iget-wide v8, p0, Lbgb;->i:J

    move-object v2, p1

    move-object v4, v1

    .line 1119
    invoke-static/range {v2 .. v9}, Lyp;->a(Lyt;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
