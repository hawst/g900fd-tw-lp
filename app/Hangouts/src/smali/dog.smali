.class public final Ldog;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldog;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1438
    const/4 v0, 0x0

    new-array v0, v0, [Ldog;

    sput-object v0, Ldog;->a:[Ldog;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1439
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1470
    const/4 v0, 0x0

    .line 1471
    iget-object v1, p0, Ldog;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1472
    const/4 v0, 0x1

    iget-object v1, p0, Ldog;->b:Ljava/lang/Integer;

    .line 1473
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1475
    :cond_0
    iget-object v1, p0, Ldog;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1476
    const/4 v1, 0x2

    iget-object v2, p0, Ldog;->c:Ljava/lang/Integer;

    .line 1477
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1479
    :cond_1
    iget-object v1, p0, Ldog;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1480
    const/4 v1, 0x3

    iget-object v2, p0, Ldog;->d:Ljava/lang/Integer;

    .line 1481
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1483
    :cond_2
    iget-object v1, p0, Ldog;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1484
    const/4 v1, 0x4

    iget-object v2, p0, Ldog;->e:Ljava/lang/Integer;

    .line 1485
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1487
    :cond_3
    iget-object v1, p0, Ldog;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1488
    iput v0, p0, Ldog;->cachedSize:I

    .line 1489
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1435
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldog;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldog;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldog;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldog;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldog;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldog;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldog;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1452
    iget-object v0, p0, Ldog;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1453
    const/4 v0, 0x1

    iget-object v1, p0, Ldog;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1455
    :cond_0
    iget-object v0, p0, Ldog;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1456
    const/4 v0, 0x2

    iget-object v1, p0, Ldog;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1458
    :cond_1
    iget-object v0, p0, Ldog;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1459
    const/4 v0, 0x3

    iget-object v1, p0, Ldog;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1461
    :cond_2
    iget-object v0, p0, Ldog;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1462
    const/4 v0, 0x4

    iget-object v1, p0, Ldog;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1464
    :cond_3
    iget-object v0, p0, Ldog;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1466
    return-void
.end method
