.class public final Lawd;
.super Lbor;
.source "PG"


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lyh;

.field final synthetic d:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;ILjava/lang/String;Lyh;)V
    .locals 0

    .prologue
    .line 1468
    iput-object p1, p0, Lawd;->d:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    iput p2, p0, Lawd;->a:I

    iput-object p3, p0, Lawd;->b:Ljava/lang/String;

    iput-object p4, p0, Lawd;->c:Lyh;

    invoke-direct {p0}, Lbor;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILbkr;Lbos;)V
    .locals 5

    .prologue
    .line 1473
    iget v0, p0, Lawd;->a:I

    if-eq p1, v0, :cond_0

    .line 1503
    :goto_0
    return-void

    .line 1476
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 1477
    if-nez p2, :cond_1

    .line 1478
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "creating conversation with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lawd;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resulting in null ConversationResult"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1485
    :cond_1
    new-instance v0, Lahc;

    iget-object v1, p2, Lbkr;->a:Ljava/lang/String;

    iget-object v2, p0, Lawd;->d:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    .line 1488
    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lyj;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 1490
    const/4 v1, 0x1

    iput-boolean v1, v0, Lahc;->b:Z

    .line 1491
    iget-object v1, p0, Lawd;->c:Lyh;

    iput-object v1, v0, Lahc;->e:Lyh;

    .line 1495
    iget-object v1, p0, Lawd;->d:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1496
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1497
    const-string v3, "conversation_id"

    iget-object v4, v0, Lahc;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1498
    const-string v3, "android.intent.extra.TEXT"

    iget-object v4, p0, Lawd;->c:Lyh;

    iget-object v4, v4, Lyh;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1499
    const-string v3, "draft_subject"

    iget-object v4, p0, Lawd;->c:Lyh;

    iget-object v4, v4, Lyh;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1500
    const-string v3, "share_intent"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1502
    iget-object v1, p0, Lawd;->d:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Lahc;)V

    goto :goto_0
.end method
