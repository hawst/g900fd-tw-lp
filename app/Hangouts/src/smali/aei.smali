.class public final Laei;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Laee;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Laea;

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Laee;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Laei;-><init>(Landroid/content/Context;Ljava/util/List;Laea;Z)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Laea;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Laee;",
            ">;",
            "Laea;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 40
    sget v0, Lf;->ey:I

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 41
    iput-object p3, p0, Laei;->a:Laea;

    .line 42
    iput-boolean p4, p0, Laei;->b:Z

    .line 43
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/16 v10, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 47
    invoke-virtual {p0}, Laei;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 48
    sget v1, Lf;->ez:I

    invoke-virtual {v0, v1, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 50
    sget v0, Lg;->hA:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 52
    sget v1, Lg;->hj:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 54
    sget v2, Lg;->gT:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 57
    invoke-virtual {p0, p1}, Laei;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laee;

    .line 58
    instance-of v4, v3, Laef;

    if-eqz v4, :cond_1

    move-object v4, v3

    check-cast v4, Laef;

    iget-object v4, v4, Laef;->a:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    instance-of v0, v3, Laef;

    if-eqz v0, :cond_6

    move-object v0, v3

    check-cast v0, Laef;

    iget-object v4, v0, Laef;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Laei;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v4, Lh;->aJ:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 60
    :cond_0
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 61
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    :goto_2
    iget-boolean v0, p0, Laei;->b:Z

    if-eqz v0, :cond_b

    instance-of v0, v3, Laeh;

    if-eqz v0, :cond_b

    .line 67
    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 72
    :goto_3
    return-object v6

    .line 58
    :cond_1
    instance-of v4, v3, Laeh;

    if-eqz v4, :cond_3

    instance-of v4, v3, Laed;

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Laei;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v7, Lh;->aB:I

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    move-object v4, v3

    check-cast v4, Laeh;

    iget-object v4, v4, Laeh;->a:Ljava/lang/String;

    goto :goto_0

    :cond_3
    instance-of v4, v3, Laeg;

    if-eqz v4, :cond_c

    iget-object v4, p0, Laei;->a:Laea;

    invoke-static {v4}, Lcwz;->b(Ljava/lang/Object;)V

    iget-object v4, p0, Laei;->a:Laea;

    invoke-virtual {v4}, Laea;->k()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_4

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laef;

    iget-object v4, v4, Laef;->a:Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget-object v4, p0, Laei;->a:Laea;

    invoke-virtual {v4}, Laea;->e()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 59
    :cond_5
    invoke-virtual {p0}, Laei;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lh;->aI:I

    new-array v7, v8, [Ljava/lang/Object;

    iget-object v0, v0, Laef;->d:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v9

    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_6
    instance-of v0, v3, Laeh;

    if-eqz v0, :cond_9

    instance-of v0, v3, Laed;

    if-eqz v0, :cond_7

    const-string v5, ""

    goto :goto_1

    :cond_7
    move-object v0, v3

    check-cast v0, Laeh;

    iget-object v4, v0, Laeh;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {p0}, Laei;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v4, Lh;->ba:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0}, Laei;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lh;->aZ:I

    new-array v7, v8, [Ljava/lang/Object;

    iget-object v0, v0, Laeh;->d:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v9

    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_9
    instance-of v0, v3, Laeg;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laei;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v4, Lh;->aK:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 63
    :cond_a
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 69
    :cond_b
    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_c
    move-object v4, v5

    goto/16 :goto_0
.end method
