.class public Lbic;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldwb;)V
    .locals 4

    .prologue
    .line 1913
    iget-object v0, p1, Ldwb;->b:Ldvn;

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1915
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1916
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetConfigurationBitResponse debugUrl: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Ldwb;->b:Ldvn;

    iget-object v2, v2, Ldvn;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918
    iget-object v0, p1, Ldwb;->c:[Ldpx;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 1919
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetConfigurationBitResponse error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Ldwb;->c:[Ldpx;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, Ldpx;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1923
    :cond_0
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 1924
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetConfigurationBitResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1927
    :cond_1
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1931
    new-instance v0, Ldwb;

    invoke-direct {v0}, Ldwb;-><init>()V

    .line 1932
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldwb;

    .line 1933
    iget-object v1, v0, Ldwb;->b:Ldvn;

    invoke-static {v1}, Lbic;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1934
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldwb;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1936
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbic;

    invoke-direct {v1, v0}, Lbic;-><init>(Ldwb;)V

    move-object v0, v1

    goto :goto_0
.end method
