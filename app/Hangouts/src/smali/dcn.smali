.class public final Ldcn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldcn;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 501
    const/4 v0, 0x0

    new-array v0, v0, [Ldcn;

    sput-object v0, Ldcn;->a:[Ldcn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 548
    const/4 v0, 0x0

    .line 549
    iget-object v1, p0, Ldcn;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 550
    const/4 v0, 0x1

    iget-object v1, p0, Ldcn;->b:Ljava/lang/String;

    .line 551
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 553
    :cond_0
    iget-object v1, p0, Ldcn;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 554
    const/4 v1, 0x2

    iget-object v2, p0, Ldcn;->c:Ljava/lang/String;

    .line 555
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 557
    :cond_1
    iget-object v1, p0, Ldcn;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 558
    const/4 v1, 0x3

    iget-object v2, p0, Ldcn;->d:Ljava/lang/String;

    .line 559
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 561
    :cond_2
    iget-object v1, p0, Ldcn;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 562
    const/4 v1, 0x4

    iget-object v2, p0, Ldcn;->e:Ljava/lang/String;

    .line 563
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 565
    :cond_3
    iget-object v1, p0, Ldcn;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 566
    const/4 v1, 0x5

    iget-object v2, p0, Ldcn;->f:Ljava/lang/String;

    .line 567
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 569
    :cond_4
    iget-object v1, p0, Ldcn;->g:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 570
    const/4 v1, 0x6

    iget-object v2, p0, Ldcn;->g:Ljava/lang/Long;

    .line 571
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 573
    :cond_5
    iget-object v1, p0, Ldcn;->h:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 574
    const/4 v1, 0x7

    iget-object v2, p0, Ldcn;->h:Ljava/lang/Long;

    .line 575
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 577
    :cond_6
    iget-object v1, p0, Ldcn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 578
    iput v0, p0, Ldcn;->cachedSize:I

    .line 579
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 498
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldcn;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldcn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldcn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcn;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcn;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcn;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcn;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcn;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldcn;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldcn;->h:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 521
    iget-object v0, p0, Ldcn;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 522
    const/4 v0, 0x1

    iget-object v1, p0, Ldcn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 524
    :cond_0
    iget-object v0, p0, Ldcn;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 525
    const/4 v0, 0x2

    iget-object v1, p0, Ldcn;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 527
    :cond_1
    iget-object v0, p0, Ldcn;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 528
    const/4 v0, 0x3

    iget-object v1, p0, Ldcn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 530
    :cond_2
    iget-object v0, p0, Ldcn;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 531
    const/4 v0, 0x4

    iget-object v1, p0, Ldcn;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 533
    :cond_3
    iget-object v0, p0, Ldcn;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 534
    const/4 v0, 0x5

    iget-object v1, p0, Ldcn;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 536
    :cond_4
    iget-object v0, p0, Ldcn;->g:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 537
    const/4 v0, 0x6

    iget-object v1, p0, Ldcn;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 539
    :cond_5
    iget-object v0, p0, Ldcn;->h:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 540
    const/4 v0, 0x7

    iget-object v1, p0, Ldcn;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 542
    :cond_6
    iget-object v0, p0, Ldcn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 544
    return-void
.end method
