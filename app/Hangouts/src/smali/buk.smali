.class public final Lbuk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lbtz;

.field b:Lyj;

.field final c:Landroid/app/Activity;

.field d:Lach;

.field e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private final g:Landroid/content/res/Resources;

.field private final h:Lbor;


# direct methods
.method public constructor <init>(Lbtz;)V
    .locals 2

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    new-instance v0, Lbum;

    invoke-direct {v0, p0}, Lbum;-><init>(Lbuk;)V

    iput-object v0, p0, Lbuk;->h:Lbor;

    .line 134
    iput-object p1, p0, Lbuk;->a:Lbtz;

    .line 135
    iget-object v0, p0, Lbuk;->a:Lbtz;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 137
    iget-object v0, p0, Lbuk;->a:Lbtz;

    invoke-interface {v0}, Lbtz;->c()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lbuk;->c:Landroid/app/Activity;

    .line 138
    iget-object v0, p0, Lbuk;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lbuk;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbuk;->g:Landroid/content/res/Resources;

    .line 142
    iget-object v0, p0, Lbuk;->g:Landroid/content/res/Resources;

    sget v1, Lh;->fV:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbuk;->e:Ljava/lang/String;

    .line 143
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lbuk;->d:Lach;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lbuk;->d:Lach;

    invoke-interface {v0}, Lach;->a()V

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lbuk;->d:Lach;

    .line 190
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lbuk;->c:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 147
    const-string v0, "Babel"

    const-string v1, "RichStatusSettingsUi: empty context. Abort."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :goto_0
    return-void

    .line 150
    :cond_0
    iput-object p1, p0, Lbuk;->f:Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lbuk;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 152
    const-string v0, "Babel"

    const-string v1, "RichStatusSettingsUi: no account specified. Abort."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lbuk;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lbuk;->f:Ljava/lang/String;

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lbuk;->b:Lyj;

    .line 158
    iget-object v0, p0, Lbuk;->a:Lbtz;

    iget-object v1, p0, Lbuk;->b:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/String;)V

    .line 160
    new-instance v0, Lbul;

    invoke-direct {v0, p0}, Lbul;-><init>(Lbuk;)V

    .line 177
    iget-object v1, p0, Lbuk;->a:Lbtz;

    iget-object v2, p0, Lbuk;->e:Ljava/lang/String;

    invoke-interface {v1, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 179
    iget-object v0, p0, Lbuk;->h:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 182
    iget-object v0, p0, Lbuk;->b:Lyj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Z)I

    goto :goto_0
.end method
