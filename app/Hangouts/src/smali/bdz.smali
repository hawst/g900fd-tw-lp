.class public Lbdz;
.super Lbdp;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;Ldzj;)V
    .locals 1

    .prologue
    .line 374
    invoke-direct {p0}, Lbdp;-><init>()V

    .line 375
    iput-object p1, p0, Lbdz;->a:Ljava/lang/String;

    .line 376
    new-instance v0, Ldzh;

    invoke-direct {v0}, Ldzh;-><init>()V

    .line 377
    iput-object p2, v0, Ldzh;->d:Ldzj;

    .line 378
    invoke-static {v0}, Ldzh;->toByteArray(Lepr;)[B

    move-result-object v0

    iput-object v0, p0, Lbdz;->b:[B

    .line 379
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 2

    .prologue
    .line 385
    :try_start_0
    new-instance v0, Ldzh;

    invoke-direct {v0}, Ldzh;-><init>()V

    iget-object v1, p0, Lbdz;->b:[B

    invoke-static {v0, v1}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldzh;

    .line 387
    invoke-virtual {p0, p1, p2}, Lbdz;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldzh;->b:Ldvm;

    .line 388
    iget-object v1, p0, Lbdz;->a:Ljava/lang/String;

    iput-object v1, v0, Ldzh;->c:Ljava/lang/String;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :goto_0
    return-object v0

    .line 390
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 397
    const-string v0, "media_sessions/log"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 402
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
