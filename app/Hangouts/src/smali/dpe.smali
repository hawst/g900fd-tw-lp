.class public final Ldpe;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpe;


# instance fields
.field public b:[Ljava/lang/String;

.field public c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 964
    const/4 v0, 0x0

    new-array v0, v0, [Ldpe;

    sput-object v0, Ldpe;->a:[Ldpe;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 965
    invoke-direct {p0}, Lepn;-><init>()V

    .line 968
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldpe;->b:[Ljava/lang/String;

    .line 971
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldpe;->c:[Ljava/lang/String;

    .line 965
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 993
    iget-object v0, p0, Ldpe;->b:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldpe;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 995
    iget-object v3, p0, Ldpe;->b:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 997
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 995
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1000
    :cond_0
    iget-object v0, p0, Ldpe;->b:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 1002
    :goto_1
    iget-object v2, p0, Ldpe;->c:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldpe;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1004
    iget-object v3, p0, Ldpe;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 1006
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1004
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1008
    :cond_1
    add-int/2addr v0, v2

    .line 1009
    iget-object v1, p0, Ldpe;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1011
    :cond_2
    iget-object v1, p0, Ldpe;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1012
    iput v0, p0, Ldpe;->cachedSize:I

    .line 1013
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 961
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldpe;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldpe;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldpe;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldpe;->b:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldpe;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldpe;->b:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldpe;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldpe;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldpe;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldpe;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldpe;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldpe;->c:[Ljava/lang/String;

    :goto_2
    iget-object v1, p0, Ldpe;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldpe;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Ldpe;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 976
    iget-object v1, p0, Ldpe;->b:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 977
    iget-object v2, p0, Ldpe;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 978
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 977
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 981
    :cond_0
    iget-object v1, p0, Ldpe;->c:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 982
    iget-object v1, p0, Ldpe;->c:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 983
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 982
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 986
    :cond_1
    iget-object v0, p0, Ldpe;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 988
    return-void
.end method
