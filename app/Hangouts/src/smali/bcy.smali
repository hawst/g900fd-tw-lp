.class public final Lbcy;
.super Lcom/google/api/client/http/AbstractHttpContent;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Lepr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lbys;->h:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbcy;->a:Z

    return-void
.end method

.method protected constructor <init>(Lepr;)V
    .locals 1

    .prologue
    .line 26
    const-string v0, "application/x-protobuf"

    invoke-direct {p0, v0}, Lcom/google/api/client/http/AbstractHttpContent;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lbcy;->b:Lepr;

    .line 28
    return-void
.end method


# virtual methods
.method public getLength()J
    .locals 4

    .prologue
    .line 32
    iget-object v0, p0, Lbcy;->b:Lepr;

    invoke-virtual {v0}, Lepr;->getSerializedSize()I

    move-result v0

    .line 33
    sget-boolean v1, Lbcy;->a:Z

    if-eqz v1, :cond_0

    .line 34
    const-string v1, "Babel_protos"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NanoProtoHttpContent serialized size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " proto="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lbcy;->b:Lepr;

    .line 35
    invoke-virtual {v3}, Lepr;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 34
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    :cond_0
    int-to-long v0, v0

    return-wide v0
.end method

.method public bridge synthetic setMediaType(Lcom/google/api/client/http/HttpMediaType;)Lcom/google/api/client/http/AbstractHttpContent;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/google/api/client/http/AbstractHttpContent;->setMediaType(Lcom/google/api/client/http/HttpMediaType;)Lcom/google/api/client/http/AbstractHttpContent;

    move-result-object v0

    check-cast v0, Lbcy;

    return-object v0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 41
    iget-object v0, p0, Lbcy;->b:Lepr;

    invoke-static {v0}, Lepr;->toByteArray(Lepr;)[B

    move-result-object v0

    .line 42
    sget-boolean v1, Lbcy;->a:Z

    if-eqz v1, :cond_0

    .line 43
    const-string v1, "Babel_protos"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NanoProtoHttpContent write size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " proto="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 44
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 43
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_0
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 47
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 48
    return-void
.end method
