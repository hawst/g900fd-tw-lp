.class public final Ldqa;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqa;


# instance fields
.field public b:Ldqf;

.field public c:Ldsb;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ldqb;

.field public h:[Ldxt;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:[Ldui;

.field public m:[Ldqh;

.field public n:Ljava/lang/Boolean;

.field public o:Ljava/lang/Boolean;

.field public p:[I

.field public q:Ljava/lang/Integer;

.field public r:[[B

.field public s:[[B

.field public t:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3403
    const/4 v0, 0x0

    new-array v0, v0, [Ldqa;

    sput-object v0, Ldqa;->a:[Ldqa;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3404
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3832
    iput-object v1, p0, Ldqa;->b:Ldqf;

    .line 3835
    iput-object v1, p0, Ldqa;->c:Ldsb;

    .line 3838
    iput-object v1, p0, Ldqa;->d:Ljava/lang/Integer;

    .line 3845
    iput-object v1, p0, Ldqa;->g:Ldqb;

    .line 3848
    sget-object v0, Ldxt;->a:[Ldxt;

    iput-object v0, p0, Ldqa;->h:[Ldxt;

    .line 3853
    iput-object v1, p0, Ldqa;->j:Ljava/lang/Integer;

    .line 3856
    iput-object v1, p0, Ldqa;->k:Ljava/lang/Integer;

    .line 3859
    sget-object v0, Ldui;->a:[Ldui;

    iput-object v0, p0, Ldqa;->l:[Ldui;

    .line 3862
    sget-object v0, Ldqh;->a:[Ldqh;

    iput-object v0, p0, Ldqa;->m:[Ldqh;

    .line 3869
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldqa;->p:[I

    .line 3872
    iput-object v1, p0, Ldqa;->q:Ljava/lang/Integer;

    .line 3875
    sget-object v0, Lept;->k:[[B

    iput-object v0, p0, Ldqa;->r:[[B

    .line 3878
    sget-object v0, Lept;->k:[[B

    iput-object v0, p0, Ldqa;->s:[[B

    .line 3404
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3967
    iget-object v0, p0, Ldqa;->b:Ldqf;

    if-eqz v0, :cond_18

    .line 3968
    const/4 v0, 0x1

    iget-object v2, p0, Ldqa;->b:Ldqf;

    .line 3969
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3971
    :goto_0
    iget-object v2, p0, Ldqa;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 3972
    const/4 v2, 0x2

    iget-object v3, p0, Ldqa;->d:Ljava/lang/Integer;

    .line 3973
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3975
    :cond_0
    iget-object v2, p0, Ldqa;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 3976
    const/4 v2, 0x3

    iget-object v3, p0, Ldqa;->e:Ljava/lang/String;

    .line 3977
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3979
    :cond_1
    iget-object v2, p0, Ldqa;->g:Ldqb;

    if-eqz v2, :cond_2

    .line 3980
    const/4 v2, 0x4

    iget-object v3, p0, Ldqa;->g:Ldqb;

    .line 3981
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3983
    :cond_2
    iget-object v2, p0, Ldqa;->r:[[B

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldqa;->r:[[B

    array-length v2, v2

    if-lez v2, :cond_4

    .line 3985
    iget-object v4, p0, Ldqa;->r:[[B

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 3987
    invoke-static {v6}, Lepl;->b([B)I

    move-result v6

    add-int/2addr v3, v6

    .line 3985
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3989
    :cond_3
    add-int/2addr v0, v3

    .line 3990
    iget-object v2, p0, Ldqa;->r:[[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3992
    :cond_4
    iget-object v2, p0, Ldqa;->s:[[B

    if-eqz v2, :cond_6

    iget-object v2, p0, Ldqa;->s:[[B

    array-length v2, v2

    if-lez v2, :cond_6

    .line 3994
    iget-object v4, p0, Ldqa;->s:[[B

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_5

    aget-object v6, v4, v2

    .line 3996
    invoke-static {v6}, Lepl;->b([B)I

    move-result v6

    add-int/2addr v3, v6

    .line 3994
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 3998
    :cond_5
    add-int/2addr v0, v3

    .line 3999
    iget-object v2, p0, Ldqa;->s:[[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4001
    :cond_6
    iget-object v2, p0, Ldqa;->h:[Ldxt;

    if-eqz v2, :cond_8

    .line 4002
    iget-object v3, p0, Ldqa;->h:[Ldxt;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_8

    aget-object v5, v3, v2

    .line 4003
    if-eqz v5, :cond_7

    .line 4004
    const/16 v6, 0x8

    .line 4005
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4002
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 4009
    :cond_8
    iget-object v2, p0, Ldqa;->i:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 4010
    const/16 v2, 0x9

    iget-object v3, p0, Ldqa;->i:Ljava/lang/Boolean;

    .line 4011
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4013
    :cond_9
    iget-object v2, p0, Ldqa;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    .line 4014
    const/16 v2, 0xa

    iget-object v3, p0, Ldqa;->j:Ljava/lang/Integer;

    .line 4015
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4017
    :cond_a
    iget-object v2, p0, Ldqa;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    .line 4018
    const/16 v2, 0xb

    iget-object v3, p0, Ldqa;->k:Ljava/lang/Integer;

    .line 4019
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4021
    :cond_b
    iget-object v2, p0, Ldqa;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    .line 4022
    const/16 v2, 0xc

    iget-object v3, p0, Ldqa;->t:Ljava/lang/Boolean;

    .line 4023
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4025
    :cond_c
    iget-object v2, p0, Ldqa;->l:[Ldui;

    if-eqz v2, :cond_e

    .line 4026
    iget-object v3, p0, Ldqa;->l:[Ldui;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_e

    aget-object v5, v3, v2

    .line 4027
    if-eqz v5, :cond_d

    .line 4028
    const/16 v6, 0xd

    .line 4029
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4026
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 4033
    :cond_e
    iget-object v2, p0, Ldqa;->m:[Ldqh;

    if-eqz v2, :cond_10

    .line 4034
    iget-object v3, p0, Ldqa;->m:[Ldqh;

    array-length v4, v3

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_10

    aget-object v5, v3, v2

    .line 4035
    if-eqz v5, :cond_f

    .line 4036
    const/16 v6, 0xe

    .line 4037
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4034
    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 4041
    :cond_10
    iget-object v2, p0, Ldqa;->n:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    .line 4042
    const/16 v2, 0xf

    iget-object v3, p0, Ldqa;->n:Ljava/lang/Boolean;

    .line 4043
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4045
    :cond_11
    iget-object v2, p0, Ldqa;->o:Ljava/lang/Boolean;

    if-eqz v2, :cond_12

    .line 4046
    const/16 v2, 0x10

    iget-object v3, p0, Ldqa;->o:Ljava/lang/Boolean;

    .line 4047
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4049
    :cond_12
    iget-object v2, p0, Ldqa;->c:Ldsb;

    if-eqz v2, :cond_13

    .line 4050
    const/16 v2, 0x11

    iget-object v3, p0, Ldqa;->c:Ldsb;

    .line 4051
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4053
    :cond_13
    iget-object v2, p0, Ldqa;->p:[I

    if-eqz v2, :cond_15

    iget-object v2, p0, Ldqa;->p:[I

    array-length v2, v2

    if-lez v2, :cond_15

    .line 4055
    iget-object v3, p0, Ldqa;->p:[I

    array-length v4, v3

    move v2, v1

    :goto_6
    if-ge v1, v4, :cond_14

    aget v5, v3, v1

    .line 4057
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 4055
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 4059
    :cond_14
    add-int/2addr v0, v2

    .line 4060
    iget-object v1, p0, Ldqa;->p:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 4062
    :cond_15
    iget-object v1, p0, Ldqa;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_16

    .line 4063
    const/16 v1, 0x13

    iget-object v2, p0, Ldqa;->q:Ljava/lang/Integer;

    .line 4064
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4066
    :cond_16
    iget-object v1, p0, Ldqa;->f:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 4067
    const/16 v1, 0x14

    iget-object v2, p0, Ldqa;->f:Ljava/lang/String;

    .line 4068
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4070
    :cond_17
    iget-object v1, p0, Ldqa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4071
    iput v0, p0, Ldqa;->cachedSize:I

    .line 4072
    return v0

    :cond_18
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3400
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldqa;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldqa;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldqa;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqa;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldqa;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldqa;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    if-eq v0, v4, :cond_3

    if-ne v0, v5, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqa;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqa;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldqa;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldqa;->g:Ldqb;

    if-nez v0, :cond_5

    new-instance v0, Ldqb;

    invoke-direct {v0}, Ldqb;-><init>()V

    iput-object v0, p0, Ldqa;->g:Ldqb;

    :cond_5
    iget-object v0, p0, Ldqa;->g:Ldqb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqa;->r:[[B

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [[B

    iget-object v3, p0, Ldqa;->r:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldqa;->r:[[B

    :goto_1
    iget-object v2, p0, Ldqa;->r:[[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldqa;->r:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldqa;->r:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqa;->s:[[B

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [[B

    iget-object v3, p0, Ldqa;->s:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldqa;->s:[[B

    :goto_2
    iget-object v2, p0, Ldqa;->s:[[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldqa;->s:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v2, p0, Ldqa;->s:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqa;->h:[Ldxt;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldxt;

    iget-object v3, p0, Ldqa;->h:[Ldxt;

    if-eqz v3, :cond_8

    iget-object v3, p0, Ldqa;->h:[Ldxt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Ldqa;->h:[Ldxt;

    :goto_4
    iget-object v2, p0, Ldqa;->h:[Ldxt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Ldqa;->h:[Ldxt;

    new-instance v3, Ldxt;

    invoke-direct {v3}, Ldxt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqa;->h:[Ldxt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    iget-object v0, p0, Ldqa;->h:[Ldxt;

    array-length v0, v0

    goto :goto_3

    :cond_a
    iget-object v2, p0, Ldqa;->h:[Ldxt;

    new-instance v3, Ldxt;

    invoke-direct {v3}, Ldxt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqa;->h:[Ldxt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldqa;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_b

    if-ne v0, v5, :cond_c

    :cond_b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqa;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqa;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_d

    if-ne v0, v5, :cond_e

    :cond_d
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqa;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqa;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldqa;->t:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqa;->l:[Ldui;

    if-nez v0, :cond_10

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ldui;

    iget-object v3, p0, Ldqa;->l:[Ldui;

    if-eqz v3, :cond_f

    iget-object v3, p0, Ldqa;->l:[Ldui;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    iput-object v2, p0, Ldqa;->l:[Ldui;

    :goto_6
    iget-object v2, p0, Ldqa;->l:[Ldui;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    iget-object v2, p0, Ldqa;->l:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqa;->l:[Ldui;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_10
    iget-object v0, p0, Ldqa;->l:[Ldui;

    array-length v0, v0

    goto :goto_5

    :cond_11
    iget-object v2, p0, Ldqa;->l:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqa;->l:[Ldui;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x72

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqa;->m:[Ldqh;

    if-nez v0, :cond_13

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ldqh;

    iget-object v3, p0, Ldqa;->m:[Ldqh;

    if-eqz v3, :cond_12

    iget-object v3, p0, Ldqa;->m:[Ldqh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    iput-object v2, p0, Ldqa;->m:[Ldqh;

    :goto_8
    iget-object v2, p0, Ldqa;->m:[Ldqh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_14

    iget-object v2, p0, Ldqa;->m:[Ldqh;

    new-instance v3, Ldqh;

    invoke-direct {v3}, Ldqh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqa;->m:[Ldqh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_13
    iget-object v0, p0, Ldqa;->m:[Ldqh;

    array-length v0, v0

    goto :goto_7

    :cond_14
    iget-object v2, p0, Ldqa;->m:[Ldqh;

    new-instance v3, Ldqh;

    invoke-direct {v3}, Ldqh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqa;->m:[Ldqh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldqa;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldqa;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Ldqa;->c:Ldsb;

    if-nez v0, :cond_15

    new-instance v0, Ldsb;

    invoke-direct {v0}, Ldsb;-><init>()V

    iput-object v0, p0, Ldqa;->c:Ldsb;

    :cond_15
    iget-object v0, p0, Ldqa;->c:Ldsb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_11
    const/16 v0, 0x90

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqa;->p:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Ldqa;->p:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldqa;->p:[I

    :goto_9
    iget-object v2, p0, Ldqa;->p:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_16

    iget-object v2, p0, Ldqa;->p:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_16
    iget-object v2, p0, Ldqa;->p:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_17

    if-eq v0, v4, :cond_17

    if-eq v0, v5, :cond_17

    const/4 v2, 0x3

    if-ne v0, v2, :cond_18

    :cond_17
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqa;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqa;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldqa;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x8a -> :sswitch_10
        0x90 -> :sswitch_11
        0x98 -> :sswitch_12
        0xa2 -> :sswitch_13
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3885
    iget-object v1, p0, Ldqa;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 3886
    const/4 v1, 0x1

    iget-object v2, p0, Ldqa;->b:Ldqf;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3888
    :cond_0
    iget-object v1, p0, Ldqa;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3889
    const/4 v1, 0x2

    iget-object v2, p0, Ldqa;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 3891
    :cond_1
    iget-object v1, p0, Ldqa;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3892
    const/4 v1, 0x3

    iget-object v2, p0, Ldqa;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 3894
    :cond_2
    iget-object v1, p0, Ldqa;->g:Ldqb;

    if-eqz v1, :cond_3

    .line 3895
    const/4 v1, 0x4

    iget-object v2, p0, Ldqa;->g:Ldqb;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3897
    :cond_3
    iget-object v1, p0, Ldqa;->r:[[B

    if-eqz v1, :cond_4

    .line 3898
    iget-object v2, p0, Ldqa;->r:[[B

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 3899
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->a(I[B)V

    .line 3898
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3902
    :cond_4
    iget-object v1, p0, Ldqa;->s:[[B

    if-eqz v1, :cond_5

    .line 3903
    iget-object v2, p0, Ldqa;->s:[[B

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 3904
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lepl;->a(I[B)V

    .line 3903
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3907
    :cond_5
    iget-object v1, p0, Ldqa;->h:[Ldxt;

    if-eqz v1, :cond_7

    .line 3908
    iget-object v2, p0, Ldqa;->h:[Ldxt;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 3909
    if-eqz v4, :cond_6

    .line 3910
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 3908
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3914
    :cond_7
    iget-object v1, p0, Ldqa;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 3915
    const/16 v1, 0x9

    iget-object v2, p0, Ldqa;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3917
    :cond_8
    iget-object v1, p0, Ldqa;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 3918
    const/16 v1, 0xa

    iget-object v2, p0, Ldqa;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 3920
    :cond_9
    iget-object v1, p0, Ldqa;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 3921
    const/16 v1, 0xb

    iget-object v2, p0, Ldqa;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 3923
    :cond_a
    iget-object v1, p0, Ldqa;->t:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 3924
    const/16 v1, 0xc

    iget-object v2, p0, Ldqa;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3926
    :cond_b
    iget-object v1, p0, Ldqa;->l:[Ldui;

    if-eqz v1, :cond_d

    .line 3927
    iget-object v2, p0, Ldqa;->l:[Ldui;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 3928
    if-eqz v4, :cond_c

    .line 3929
    const/16 v5, 0xd

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 3927
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 3933
    :cond_d
    iget-object v1, p0, Ldqa;->m:[Ldqh;

    if-eqz v1, :cond_f

    .line 3934
    iget-object v2, p0, Ldqa;->m:[Ldqh;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_f

    aget-object v4, v2, v1

    .line 3935
    if-eqz v4, :cond_e

    .line 3936
    const/16 v5, 0xe

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 3934
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3940
    :cond_f
    iget-object v1, p0, Ldqa;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    .line 3941
    const/16 v1, 0xf

    iget-object v2, p0, Ldqa;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3943
    :cond_10
    iget-object v1, p0, Ldqa;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 3944
    const/16 v1, 0x10

    iget-object v2, p0, Ldqa;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3946
    :cond_11
    iget-object v1, p0, Ldqa;->c:Ldsb;

    if-eqz v1, :cond_12

    .line 3947
    const/16 v1, 0x11

    iget-object v2, p0, Ldqa;->c:Ldsb;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3949
    :cond_12
    iget-object v1, p0, Ldqa;->p:[I

    if-eqz v1, :cond_13

    iget-object v1, p0, Ldqa;->p:[I

    array-length v1, v1

    if-lez v1, :cond_13

    .line 3950
    iget-object v1, p0, Ldqa;->p:[I

    array-length v2, v1

    :goto_5
    if-ge v0, v2, :cond_13

    aget v3, v1, v0

    .line 3951
    const/16 v4, 0x12

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 3950
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 3954
    :cond_13
    iget-object v0, p0, Ldqa;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    .line 3955
    const/16 v0, 0x13

    iget-object v1, p0, Ldqa;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3957
    :cond_14
    iget-object v0, p0, Ldqa;->f:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 3958
    const/16 v0, 0x14

    iget-object v1, p0, Ldqa;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3960
    :cond_15
    iget-object v0, p0, Ldqa;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3962
    return-void
.end method
