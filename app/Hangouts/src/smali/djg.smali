.class public final Ldjg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldjg;


# instance fields
.field public b:Ldkj;

.field public c:Ldjh;

.field public d:[Ldkj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 719
    const/4 v0, 0x0

    new-array v0, v0, [Ldjg;

    sput-object v0, Ldjg;->a:[Ldjg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 720
    invoke-direct {p0}, Lepn;-><init>()V

    .line 723
    iput-object v0, p0, Ldjg;->b:Ldkj;

    .line 726
    iput-object v0, p0, Ldjg;->c:Ldjh;

    .line 729
    sget-object v0, Ldkj;->a:[Ldkj;

    iput-object v0, p0, Ldjg;->d:[Ldkj;

    .line 720
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 754
    iget-object v0, p0, Ldjg;->b:Ldkj;

    if-eqz v0, :cond_3

    .line 755
    const/4 v0, 0x1

    iget-object v2, p0, Ldjg;->b:Ldkj;

    .line 756
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 758
    :goto_0
    iget-object v2, p0, Ldjg;->c:Ldjh;

    if-eqz v2, :cond_0

    .line 759
    const/4 v2, 0x2

    iget-object v3, p0, Ldjg;->c:Ldjh;

    .line 760
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 762
    :cond_0
    iget-object v2, p0, Ldjg;->d:[Ldkj;

    if-eqz v2, :cond_2

    .line 763
    iget-object v2, p0, Ldjg;->d:[Ldkj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 764
    if-eqz v4, :cond_1

    .line 765
    const/4 v5, 0x3

    .line 766
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 763
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 770
    :cond_2
    iget-object v1, p0, Ldjg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 771
    iput v0, p0, Ldjg;->cachedSize:I

    .line 772
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 716
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldjg;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldjg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldjg;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldjg;->b:Ldkj;

    if-nez v0, :cond_2

    new-instance v0, Ldkj;

    invoke-direct {v0}, Ldkj;-><init>()V

    iput-object v0, p0, Ldjg;->b:Ldkj;

    :cond_2
    iget-object v0, p0, Ldjg;->b:Ldkj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldjg;->c:Ldjh;

    if-nez v0, :cond_3

    new-instance v0, Ldjh;

    invoke-direct {v0}, Ldjh;-><init>()V

    iput-object v0, p0, Ldjg;->c:Ldjh;

    :cond_3
    iget-object v0, p0, Ldjg;->c:Ldjh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldjg;->d:[Ldkj;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldkj;

    iget-object v3, p0, Ldjg;->d:[Ldkj;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldjg;->d:[Ldkj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldjg;->d:[Ldkj;

    :goto_2
    iget-object v2, p0, Ldjg;->d:[Ldkj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldjg;->d:[Ldkj;

    new-instance v3, Ldkj;

    invoke-direct {v3}, Ldkj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldjg;->d:[Ldkj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldjg;->d:[Ldkj;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldjg;->d:[Ldkj;

    new-instance v3, Ldkj;

    invoke-direct {v3}, Ldkj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldjg;->d:[Ldkj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 734
    iget-object v0, p0, Ldjg;->b:Ldkj;

    if-eqz v0, :cond_0

    .line 735
    const/4 v0, 0x1

    iget-object v1, p0, Ldjg;->b:Ldkj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 737
    :cond_0
    iget-object v0, p0, Ldjg;->c:Ldjh;

    if-eqz v0, :cond_1

    .line 738
    const/4 v0, 0x2

    iget-object v1, p0, Ldjg;->c:Ldjh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 740
    :cond_1
    iget-object v0, p0, Ldjg;->d:[Ldkj;

    if-eqz v0, :cond_3

    .line 741
    iget-object v1, p0, Ldjg;->d:[Ldkj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 742
    if-eqz v3, :cond_2

    .line 743
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 741
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 747
    :cond_3
    iget-object v0, p0, Ldjg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 749
    return-void
.end method
