.class public abstract Lbep;
.super Lbea;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 427
    invoke-direct {p0}, Lbea;-><init>()V

    return-void
.end method

.method private a(Lbpk;J)Lbfz;
    .locals 6

    .prologue
    const/4 v2, 0x3

    .line 525
    const-string v1, "Babel_RequestWriter"

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 526
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UploadService: starting upload "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    :cond_0
    new-instance v2, Lbwz;

    invoke-direct {v2, p1}, Lbwz;-><init>(Lbpk;)V

    .line 533
    :try_start_0
    move-object v0, p0

    check-cast v0, Lbcp;

    move-object v1, v0

    invoke-virtual {v2, p2, p3, v1}, Lbwz;->a(JLbcp;)V
    :try_end_0
    .catch Lbxf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxh; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbxi; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lbxg; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lbxe; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 576
    invoke-virtual {v2}, Lbwz;->a()V

    .line 578
    const/4 v1, 0x0

    return-object v1

    .line 534
    :catch_0
    move-exception v1

    .line 535
    :try_start_1
    const-string v3, "Babel_RequestWriter"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 536
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FAIL request; media unavailable: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 538
    :cond_1
    new-instance v3, Lbph;

    const/16 v4, 0x72

    invoke-direct {v3, v4, v1}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576
    :catchall_0
    move-exception v1

    invoke-virtual {v2}, Lbwz;->a()V

    throw v1

    .line 540
    :catch_1
    move-exception v1

    .line 541
    :try_start_2
    const-string v3, "Babel_RequestWriter"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 542
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PAUSE request; transient error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    :cond_2
    new-instance v3, Lbph;

    const/16 v4, 0x69

    invoke-direct {v3, v4, v1}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v3

    .line 545
    :catch_2
    move-exception v1

    .line 546
    const-string v3, "Babel_RequestWriter"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 547
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FAIL request; unauthorized: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 549
    :cond_3
    new-instance v3, Lbph;

    const/16 v4, 0x68

    invoke-direct {v3, v4, v1}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v3

    .line 551
    :catch_3
    move-exception v1

    .line 552
    const-string v3, "Babel_RequestWriter"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 553
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FAIL request; quota exceeded: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 555
    :cond_4
    new-instance v3, Lbph;

    const/16 v4, 0x73

    invoke-direct {v3, v4, v1}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v3

    .line 557
    :catch_4
    move-exception v1

    .line 558
    const-string v3, "Babel_RequestWriter"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 559
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FAIL request: local IO Exception "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 561
    :cond_5
    new-instance v3, Lbph;

    const/16 v4, 0x72

    invoke-direct {v3, v4, v1}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v3

    .line 563
    :catch_5
    move-exception v1

    .line 564
    const-string v3, "Babel_RequestWriter"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 565
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PAUSE request; retryable exception: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 567
    :cond_6
    new-instance v3, Lbph;

    const/16 v4, 0x66

    invoke-direct {v3, v4, v1}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v3

    .line 568
    :catch_6
    move-exception v1

    .line 569
    const-string v3, "Babel_RequestWriter"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 570
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FAIL request: network error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 572
    :cond_7
    new-instance v1, Lbph;

    const/16 v3, 0x6c

    invoke-direct {v1, v3}, Lbph;-><init>(I)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private a(Ljava/lang/String;Lava;I)Lbfz;
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 584
    const-string v3, "Babel_RequestWriter"

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 585
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sendRequestProto "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for account "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 586
    invoke-static {p1}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 585
    invoke-static {v3, v4}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :cond_0
    invoke-virtual {p0}, Lbep;->B_()Lcom/google/api/client/http/GenericUrl;

    move-result-object v5

    .line 590
    invoke-virtual {p0}, Lbep;->s()V

    .line 593
    const-string v3, "alt"

    const-string v4, "proto"

    invoke-virtual {v5, v3, v4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    sget-boolean v3, Lbea;->d:Z

    if-eqz v3, :cond_1

    .line 596
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "sendRequestProto to "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    :cond_1
    :try_start_0
    invoke-static {}, Lbjs;->a()Lbjs;

    .line 602
    invoke-virtual {p0}, Lbep;->A_()Z

    move-result v6

    if-nez p1, :cond_3

    move-object v0, v1

    .line 604
    :goto_0
    if-eqz v0, :cond_d

    .line 605
    iget-object v1, v0, Lbfz;->c:Lbht;

    iget v2, v1, Lbht;->b:I

    .line 606
    iget-wide v3, v0, Lbfz;->e:J

    .line 607
    sget-boolean v1, Lbea;->d:Z

    if-eqz v1, :cond_2

    .line 608
    const-string v6, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "ServerRequest sent "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " got responseStatus "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " desc "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v7, v0, Lbfz;->c:Lbht;

    iget-object v7, v7, Lbht;->a:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " debug_url "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v1, v0, Lbfz;->c:Lbht;

    if-eqz v1, :cond_c

    iget-object v1, v0, Lbfz;->c:Lbht;

    iget-object v1, v1, Lbht;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :cond_2
    packed-switch v2, :pswitch_data_0

    .line 657
    const-string v1, "Babel_RequestWriter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "got unknown ResponseStatus in response header "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "; debugUrl is "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, v0, Lbfz;->c:Lbht;

    iget-object v6, v6, Lbht;->c:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    new-instance v1, Lbph;

    const/16 v2, 0x6c

    iget-object v0, v0, Lbfz;->c:Lbht;

    iget-object v0, v0, Lbht;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lbph;-><init>(IJLjava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 673
    :catch_0
    move-exception v0

    .line 674
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error making http request: url "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x190

    if-lt v1, v2, :cond_f

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x1f4

    if-ge v1, v2, :cond_f

    .line 678
    new-instance v1, Lbph;

    const/16 v2, 0x68

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 602
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lbep;->v()I

    move-result v7

    if-eqz v7, :cond_4

    move v4, v0

    :goto_2
    invoke-static {p1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    invoke-static {v3}, Lbkb;->i(Lyj;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    const/16 v8, 0x2f

    invoke-virtual {v3, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ltz v8, :cond_5

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    invoke-virtual {p0, v3, p3}, Lbep;->b(Ljava/lang/String;I)Lcom/google/api/client/http/HttpContent;

    move-result-object v3

    if-eqz v3, :cond_10

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lbep;->u()Lcom/google/api/client/http/AbstractInputStreamContent;

    move-result-object v4

    if-nez v4, :cond_6

    new-instance v0, Lbph;

    const/16 v1, 0x7a

    invoke-direct {v0, v1}, Lbph;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 683
    :catch_1
    move-exception v0

    .line 684
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EOFException making http request (retryable): url "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    new-instance v1, Lbph;

    const/16 v2, 0x67

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    :cond_4
    move v4, v2

    .line 602
    goto :goto_2

    :cond_5
    move-object v3, v1

    goto :goto_3

    :cond_6
    if-ne v7, v0, :cond_8

    :goto_4
    :try_start_2
    new-instance v2, Lczq;

    invoke-direct {v2}, Lczq;-><init>()V

    invoke-static {p1, p2}, Lbjs;->a(Ljava/lang/String;Lava;)Lcom/google/api/client/http/HttpRequestInitializer;

    move-result-object v6

    new-instance v7, Lczk;

    invoke-direct {v7, v4, v2, v6}, Lczk;-><init>(Lcom/google/api/client/http/AbstractInputStreamContent;Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/http/HttpRequestInitializer;)V

    if-eqz v0, :cond_7

    invoke-virtual {v7}, Lczk;->b()Lczk;

    :cond_7
    invoke-virtual {v7, v3}, Lczk;->a(Lcom/google/api/client/http/HttpContent;)Lczk;

    invoke-virtual {v7, v5}, Lczk;->a(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v0

    invoke-static {v0}, Lbjs;->a(Lcom/google/api/client/http/HttpResponse;)[B

    move-result-object v0

    move-object v2, v0

    :goto_5
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpi;

    if-eqz v0, :cond_b

    iget-object v0, v0, Lbpi;->b:Ljava/lang/reflect/Method;

    :goto_6
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lbjs;->a([BLjava/lang/Class;Ljava/lang/reflect/Method;)Lbfz;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto :goto_4

    :cond_9
    new-instance v0, Lczq;

    invoke-direct {v0}, Lczq;-><init>()V

    invoke-static {p1, p2}, Lbjs;->a(Ljava/lang/String;Lava;)Lcom/google/api/client/http/HttpRequestInitializer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lczq;->createRequestFactory(Lcom/google/api/client/http/HttpRequestInitializer;)Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v0

    if-eqz v6, :cond_a

    invoke-virtual {v0, v5, v3}, Lcom/google/api/client/http/HttpRequestFactory;->buildPostRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    :goto_7
    invoke-virtual {v0}, Lcom/google/api/client/http/HttpRequest;->execute()Lcom/google/api/client/http/HttpResponse;

    move-result-object v0

    invoke-static {v0}, Lbjs;->a(Lcom/google/api/client/http/HttpResponse;)[B

    move-result-object v0

    move-object v2, v0

    goto :goto_5

    :cond_a
    invoke-virtual {v0, v5}, Lcom/google/api/client/http/HttpRequestFactory;->buildGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    goto :goto_7

    :cond_b
    move-object v0, v1

    goto :goto_6

    .line 608
    :cond_c
    const-string v1, "(null)"

    goto/16 :goto_1

    .line 618
    :pswitch_0
    new-instance v1, Lbph;

    const/16 v2, 0x6b

    iget-object v0, v0, Lbfz;->c:Lbht;

    iget-object v0, v0, Lbht;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lbph;-><init>(IJLjava/lang/String;)V

    throw v1
    :try_end_2
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 689
    :catch_2
    move-exception v0

    .line 690
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error making http request: url "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 691
    new-instance v1, Lbph;

    const/16 v2, 0x66

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 623
    :pswitch_1
    :try_start_3
    new-instance v1, Lbph;

    const/16 v2, 0x6c

    iget-object v0, v0, Lbfz;->c:Lbht;

    iget-object v0, v0, Lbht;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lbph;-><init>(IJLjava/lang/String;)V

    throw v1

    .line 628
    :pswitch_2
    new-instance v1, Lbph;

    const/16 v2, 0x6f

    iget-object v0, v0, Lbfz;->c:Lbht;

    iget-object v0, v0, Lbht;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lbph;-><init>(IJLjava/lang/String;)V

    throw v1

    .line 633
    :pswitch_3
    new-instance v1, Lbph;

    const/16 v2, 0x6d

    iget-object v0, v0, Lbfz;->c:Lbht;

    iget-object v0, v0, Lbht;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lbph;-><init>(IJLjava/lang/String;)V

    throw v1

    .line 647
    :pswitch_4
    new-instance v1, Lbph;

    const/16 v2, 0x70

    iget-object v0, v0, Lbfz;->c:Lbht;

    iget-object v0, v0, Lbht;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lbph;-><init>(IJLjava/lang/String;)V

    throw v1

    .line 652
    :pswitch_5
    new-instance v1, Lbph;

    const/16 v2, 0x71

    iget-object v0, v0, Lbfz;->c:Lbht;

    iget-object v0, v0, Lbht;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lbph;-><init>(IJLjava/lang/String;)V

    throw v1

    .line 669
    :cond_d
    const-string v0, "Babel_RequestWriter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 670
    const-string v0, "Babel"

    const-string v2, "received null response"

    invoke-static {v0, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_e
    move-object v0, v1

    .line 694
    :pswitch_6
    return-object v0

    .line 681
    :cond_f
    new-instance v1, Lbph;

    const/16 v2, 0x69

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    :cond_10
    move-object v2, v1

    goto/16 :goto_5

    .line 614
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/content/Context;Lbpp;Lbpk;)Lbfz;
    .locals 19

    .prologue
    .line 433
    sget-boolean v3, Lbea;->d:Z

    if-eqz v3, :cond_0

    .line 434
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[SEND] "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lbpp;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :cond_0
    move-object/from16 v0, p2

    iget-object v0, v0, Lbpp;->c:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 437
    invoke-static/range {v17 .. v17}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    .line 438
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lyj;->u()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 442
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[SEND] skipping for sms only account: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    new-instance v3, Lbph;

    const/16 v4, 0x6f

    const-string v5, "Cannot send request for SMS only account"

    invoke-direct {v3, v4, v5}, Lbph;-><init>(ILjava/lang/String;)V

    throw v3

    .line 453
    :cond_1
    sget-object v4, Lbea;->e:Ljava/lang/String;

    invoke-static {v4}, Lauz;->a(Ljava/lang/String;)Lava;

    move-result-object v18

    .line 457
    invoke-virtual/range {p2 .. p2}, Lbpp;->b()Z

    move-result v15

    .line 459
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v15

    :goto_0
    const/4 v5, 0x2

    move/from16 v0, v16

    if-ge v0, v5, :cond_8

    .line 461
    :try_start_0
    move-object/from16 v0, p2

    iget-wide v5, v0, Lbpp;->i:J

    move-object/from16 v0, p2

    iget v7, v0, Lbpp;->f:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7}, Lbep;->b(JI)Lbph;

    move-result-object v5

    .line 463
    if-eqz v5, :cond_3

    .line 464
    throw v5
    :try_end_0
    .catch Lbph; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    :catch_0
    move-exception v5

    .line 480
    if-nez v16, :cond_7

    invoke-virtual {v5}, Lbph;->a()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 483
    invoke-virtual {v5}, Lbph;->c()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 484
    const/4 v4, 0x1

    move v15, v4

    .line 488
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lbpp;->a()Lbea;

    move-result-object v4

    instance-of v4, v4, Lbfb;

    if-eqz v4, :cond_2

    .line 490
    invoke-virtual/range {p2 .. p2}, Lbpp;->a()Lbea;

    move-result-object v4

    move-object v13, v4

    check-cast v13, Lbfb;

    .line 491
    iget-object v4, v13, Lbfb;->i:Ljava/lang/String;

    invoke-static {v4}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v11

    .line 495
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    const/16 v6, 0xa

    const/16 v7, 0x12d

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    iget-object v13, v13, Lbfb;->c:Ljava/lang/String;

    const/4 v14, 0x0

    .line 493
    invoke-static/range {v3 .. v14}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 459
    :cond_2
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    move v4, v15

    goto :goto_0

    .line 466
    :cond_3
    if-eqz v4, :cond_4

    .line 467
    :try_start_1
    invoke-static {}, Lbjs;->a()Lbjs;

    invoke-static/range {v17 .. v17}, Lbkb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lava;->d(Ljava/lang/String;)V

    .line 469
    :cond_4
    move-object/from16 v0, p2

    iget-wide v5, v0, Lbpp;->a:J

    move-object/from16 v0, p2

    iget v7, v0, Lbpp;->g:I

    invoke-virtual/range {p0 .. p0}, Lbep;->e()Z

    move-result v8

    if-eqz v8, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v5, v6}, Lbep;->a(Lbpk;J)Lbfz;

    move-result-object v5

    .line 472
    :goto_2
    move-object/from16 v0, p2

    iget-wide v6, v0, Lbpp;->i:J

    move-object/from16 v0, p2

    iget v8, v0, Lbpp;->f:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v8}, Lbep;->c(JI)Lbph;

    move-result-object v6

    .line 475
    if-eqz v6, :cond_6

    .line 476
    throw v6

    .line 469
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v7}, Lbep;->a(Ljava/lang/String;Lava;I)Lbfz;
    :try_end_1
    .catch Lbph; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto :goto_2

    :cond_6
    move-object v3, v5

    .line 509
    :goto_3
    return-object v3

    .line 506
    :cond_7
    throw v5

    .line 509
    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    :cond_9
    move v15, v4

    goto :goto_1
.end method

.method public u()Lcom/google/api/client/http/AbstractInputStreamContent;
    .locals 1

    .prologue
    .line 703
    const/4 v0, 0x0

    return-object v0
.end method

.method public v()I
    .locals 1

    .prologue
    .line 711
    const/4 v0, 0x0

    return v0
.end method
