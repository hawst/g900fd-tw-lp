.class public Lbin;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private g:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Ldxk;)V
    .locals 3

    .prologue
    .line 2177
    iget-object v0, p1, Ldxk;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 2178
    iget-object v0, p1, Ldxk;->c:[Ldui;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lbin;->g:[Ljava/lang/String;

    .line 2179
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbin;->g:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2180
    iget-object v1, p0, Lbin;->g:[Ljava/lang/String;

    iget-object v2, p1, Ldxk;->c:[Ldui;

    aget-object v2, v2, v0

    iget-object v2, v2, Ldui;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    .line 2179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2182
    :cond_0
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2183
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UndismissSuggestedContactsResponse debugUrl: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Ldxk;->b:Ldvn;

    iget-object v2, v2, Ldvn;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2186
    :cond_1
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 2190
    new-instance v0, Ldxk;

    invoke-direct {v0}, Ldxk;-><init>()V

    .line 2191
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxk;

    .line 2192
    iget-object v1, v0, Ldxk;->b:Ldvn;

    invoke-static {v1}, Lbin;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2193
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldxk;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 2195
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbin;

    invoke-direct {v1, v0}, Lbin;-><init>(Ldxk;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 4

    .prologue
    .line 2202
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 2206
    invoke-virtual {p1}, Lyt;->a()V

    .line 2208
    :try_start_0
    iget-object v1, p0, Lbin;->g:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2209
    invoke-virtual {p1, v3}, Lyt;->H(Ljava/lang/String;)V

    .line 2208
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2211
    :cond_0
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2213
    invoke-virtual {p1}, Lyt;->c()V

    .line 2216
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "hash_dismissed_contacts"

    invoke-virtual {v0, v1}, Lbsx;->e(Ljava/lang/String;)V

    .line 2218
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Z)I

    .line 2219
    return-void

    .line 2213
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
.end method
