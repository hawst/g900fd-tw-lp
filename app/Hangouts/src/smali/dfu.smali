.class public final Ldfu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfu;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4285
    const/4 v0, 0x0

    new-array v0, v0, [Ldfu;

    sput-object v0, Ldfu;->a:[Ldfu;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4286
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 4347
    const/4 v0, 0x0

    .line 4348
    iget-object v1, p0, Ldfu;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4349
    const/4 v0, 0x1

    iget-object v1, p0, Ldfu;->b:Ljava/lang/String;

    .line 4350
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4352
    :cond_0
    iget-object v1, p0, Ldfu;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4353
    const/4 v1, 0x2

    iget-object v2, p0, Ldfu;->c:Ljava/lang/String;

    .line 4354
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4356
    :cond_1
    iget-object v1, p0, Ldfu;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4357
    const/4 v1, 0x3

    iget-object v2, p0, Ldfu;->d:Ljava/lang/String;

    .line 4358
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4360
    :cond_2
    iget-object v1, p0, Ldfu;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 4361
    const/4 v1, 0x4

    iget-object v2, p0, Ldfu;->e:Ljava/lang/Integer;

    .line 4362
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4364
    :cond_3
    iget-object v1, p0, Ldfu;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 4365
    const/4 v1, 0x5

    iget-object v2, p0, Ldfu;->f:Ljava/lang/Integer;

    .line 4366
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4368
    :cond_4
    iget-object v1, p0, Ldfu;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 4369
    const/4 v1, 0x6

    iget-object v2, p0, Ldfu;->g:Ljava/lang/String;

    .line 4370
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4372
    :cond_5
    iget-object v1, p0, Ldfu;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 4373
    const/4 v1, 0x7

    iget-object v2, p0, Ldfu;->h:Ljava/lang/Boolean;

    .line 4374
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4376
    :cond_6
    iget-object v1, p0, Ldfu;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 4377
    const/16 v1, 0x8

    iget-object v2, p0, Ldfu;->i:Ljava/lang/String;

    .line 4378
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4380
    :cond_7
    iget-object v1, p0, Ldfu;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 4381
    const/16 v1, 0x9

    iget-object v2, p0, Ldfu;->j:Ljava/lang/Integer;

    .line 4382
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4384
    :cond_8
    iget-object v1, p0, Ldfu;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 4385
    const/16 v1, 0xa

    iget-object v2, p0, Ldfu;->k:Ljava/lang/Integer;

    .line 4386
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4388
    :cond_9
    iget-object v1, p0, Ldfu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4389
    iput v0, p0, Ldfu;->cachedSize:I

    .line 4390
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4282
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldfu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldfu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldfu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfu;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfu;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfu;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfu;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfu;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfu;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldfu;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfu;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfu;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfu;->k:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 4311
    iget-object v0, p0, Ldfu;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4312
    const/4 v0, 0x1

    iget-object v1, p0, Ldfu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4314
    :cond_0
    iget-object v0, p0, Ldfu;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4315
    const/4 v0, 0x2

    iget-object v1, p0, Ldfu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4317
    :cond_1
    iget-object v0, p0, Ldfu;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 4318
    const/4 v0, 0x3

    iget-object v1, p0, Ldfu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4320
    :cond_2
    iget-object v0, p0, Ldfu;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 4321
    const/4 v0, 0x4

    iget-object v1, p0, Ldfu;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4323
    :cond_3
    iget-object v0, p0, Ldfu;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 4324
    const/4 v0, 0x5

    iget-object v1, p0, Ldfu;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4326
    :cond_4
    iget-object v0, p0, Ldfu;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 4327
    const/4 v0, 0x6

    iget-object v1, p0, Ldfu;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4329
    :cond_5
    iget-object v0, p0, Ldfu;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 4330
    const/4 v0, 0x7

    iget-object v1, p0, Ldfu;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 4332
    :cond_6
    iget-object v0, p0, Ldfu;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 4333
    const/16 v0, 0x8

    iget-object v1, p0, Ldfu;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4335
    :cond_7
    iget-object v0, p0, Ldfu;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 4336
    const/16 v0, 0x9

    iget-object v1, p0, Ldfu;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4338
    :cond_8
    iget-object v0, p0, Ldfu;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 4339
    const/16 v0, 0xa

    iget-object v1, p0, Ldfu;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4341
    :cond_9
    iget-object v0, p0, Ldfu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4343
    return-void
.end method
