.class public final Ldti;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldti;


# instance fields
.field public b:Ldtl;

.field public c:Ldtj;

.field public d:Ldtk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1196
    const/4 v0, 0x0

    new-array v0, v0, [Ldti;

    sput-object v0, Ldti;->a:[Ldti;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1197
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1384
    iput-object v0, p0, Ldti;->b:Ldtl;

    .line 1387
    iput-object v0, p0, Ldti;->c:Ldtj;

    .line 1390
    iput-object v0, p0, Ldti;->d:Ldtk;

    .line 1197
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1410
    const/4 v0, 0x0

    .line 1411
    iget-object v1, p0, Ldti;->b:Ldtl;

    if-eqz v1, :cond_0

    .line 1412
    const/4 v0, 0x1

    iget-object v1, p0, Ldti;->b:Ldtl;

    .line 1413
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1415
    :cond_0
    iget-object v1, p0, Ldti;->c:Ldtj;

    if-eqz v1, :cond_1

    .line 1416
    const/4 v1, 0x2

    iget-object v2, p0, Ldti;->c:Ldtj;

    .line 1417
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1419
    :cond_1
    iget-object v1, p0, Ldti;->d:Ldtk;

    if-eqz v1, :cond_2

    .line 1420
    const/4 v1, 0x3

    iget-object v2, p0, Ldti;->d:Ldtk;

    .line 1421
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1423
    :cond_2
    iget-object v1, p0, Ldti;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1424
    iput v0, p0, Ldti;->cachedSize:I

    .line 1425
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1193
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldti;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldti;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldti;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldti;->b:Ldtl;

    if-nez v0, :cond_2

    new-instance v0, Ldtl;

    invoke-direct {v0}, Ldtl;-><init>()V

    iput-object v0, p0, Ldti;->b:Ldtl;

    :cond_2
    iget-object v0, p0, Ldti;->b:Ldtl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldti;->c:Ldtj;

    if-nez v0, :cond_3

    new-instance v0, Ldtj;

    invoke-direct {v0}, Ldtj;-><init>()V

    iput-object v0, p0, Ldti;->c:Ldtj;

    :cond_3
    iget-object v0, p0, Ldti;->c:Ldtj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldti;->d:Ldtk;

    if-nez v0, :cond_4

    new-instance v0, Ldtk;

    invoke-direct {v0}, Ldtk;-><init>()V

    iput-object v0, p0, Ldti;->d:Ldtk;

    :cond_4
    iget-object v0, p0, Ldti;->d:Ldtk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1395
    iget-object v0, p0, Ldti;->b:Ldtl;

    if-eqz v0, :cond_0

    .line 1396
    const/4 v0, 0x1

    iget-object v1, p0, Ldti;->b:Ldtl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1398
    :cond_0
    iget-object v0, p0, Ldti;->c:Ldtj;

    if-eqz v0, :cond_1

    .line 1399
    const/4 v0, 0x2

    iget-object v1, p0, Ldti;->c:Ldtj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1401
    :cond_1
    iget-object v0, p0, Ldti;->d:Ldtk;

    if-eqz v0, :cond_2

    .line 1402
    const/4 v0, 0x3

    iget-object v1, p0, Ldti;->d:Ldtk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1404
    :cond_2
    iget-object v0, p0, Ldti;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1406
    return-void
.end method
