.class public final Ldxp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxp;


# instance fields
.field public b:Ldxq;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23711
    const/4 v0, 0x0

    new-array v0, v0, [Ldxp;

    sput-object v0, Ldxp;->a:[Ldxp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23712
    invoke-direct {p0}, Lepn;-><init>()V

    .line 23719
    iput-object v0, p0, Ldxp;->b:Ldxq;

    .line 23724
    iput-object v0, p0, Ldxp;->d:Ljava/lang/Integer;

    .line 23712
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 23744
    const/4 v0, 0x0

    .line 23745
    iget-object v1, p0, Ldxp;->b:Ldxq;

    if-eqz v1, :cond_0

    .line 23746
    const/4 v0, 0x1

    iget-object v1, p0, Ldxp;->b:Ldxq;

    .line 23747
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23749
    :cond_0
    iget-object v1, p0, Ldxp;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 23750
    const/4 v1, 0x2

    iget-object v2, p0, Ldxp;->c:Ljava/lang/String;

    .line 23751
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23753
    :cond_1
    iget-object v1, p0, Ldxp;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 23754
    const/4 v1, 0x3

    iget-object v2, p0, Ldxp;->d:Ljava/lang/Integer;

    .line 23755
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23757
    :cond_2
    iget-object v1, p0, Ldxp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23758
    iput v0, p0, Ldxp;->cachedSize:I

    .line 23759
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 23708
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldxp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldxp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldxp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxp;->b:Ldxq;

    if-nez v0, :cond_2

    new-instance v0, Ldxq;

    invoke-direct {v0}, Ldxq;-><init>()V

    iput-object v0, p0, Ldxp;->b:Ldxq;

    :cond_2
    iget-object v0, p0, Ldxp;->b:Ldxq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxp;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxp;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxp;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 23729
    iget-object v0, p0, Ldxp;->b:Ldxq;

    if-eqz v0, :cond_0

    .line 23730
    const/4 v0, 0x1

    iget-object v1, p0, Ldxp;->b:Ldxq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 23732
    :cond_0
    iget-object v0, p0, Ldxp;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 23733
    const/4 v0, 0x2

    iget-object v1, p0, Ldxp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 23735
    :cond_1
    iget-object v0, p0, Ldxp;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 23736
    const/4 v0, 0x3

    iget-object v1, p0, Ldxp;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 23738
    :cond_2
    iget-object v0, p0, Ldxp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 23740
    return-void
.end method
