.class public final Lbqw;
.super Lbmu;
.source "PG"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbqw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private d:J

.field private final e:[Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbqw;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lyj;JJ)V
    .locals 7

    .prologue
    .line 90
    invoke-direct/range {p0 .. p5}, Lbmu;-><init>(Lyj;JJ)V

    .line 45
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbqw;->d:J

    .line 91
    const/16 v0, 0xe

    new-array v0, v0, [Lbfy;

    iput-object v0, p0, Lbqw;->e:[Lbfy;

    .line 92
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/4 v1, 0x0

    new-instance v2, Lbfy;

    const/4 v3, 0x1

    const/16 v4, 0xa

    const/4 v5, 0x1

    const-string v6, "sent_sms_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 98
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/4 v1, 0x1

    new-instance v2, Lbfy;

    const/4 v3, 0x1

    const/16 v4, 0x9

    const/4 v5, 0x1

    const-string v6, "received_sms_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 103
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/4 v1, 0x2

    new-instance v2, Lbfy;

    const/4 v3, 0x1

    const/16 v4, 0xa

    const/4 v5, 0x2

    const-string v6, "sent_mms_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 108
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/4 v1, 0x3

    new-instance v2, Lbfy;

    const/4 v3, 0x1

    const/16 v4, 0x9

    const/4 v5, 0x2

    const-string v6, "received_mms_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 113
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/4 v1, 0x4

    new-instance v2, Lbfy;

    const/4 v3, 0x3

    const/4 v4, 0x6

    const/4 v5, 0x0

    const-string v6, "shown_sms_promo_screen_launch_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 118
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/4 v1, 0x5

    new-instance v2, Lbfy;

    const/4 v3, 0x3

    const/4 v4, 0x7

    const/4 v5, 0x0

    const-string v6, "accepted_sms_promo_screen_launch_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 125
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/4 v1, 0x6

    new-instance v2, Lbfy;

    const/4 v3, 0x3

    const/16 v4, 0x8

    const/4 v5, 0x0

    const-string v6, "declined_sms_promo_screen_launch_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 132
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/4 v1, 0x7

    new-instance v2, Lbfy;

    const/4 v3, 0x4

    const/4 v4, 0x6

    const/4 v5, 0x0

    const-string v6, "shown_sms_promo_screen_notify_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 137
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/16 v1, 0x8

    new-instance v2, Lbfy;

    const/4 v3, 0x4

    const/4 v4, 0x7

    const/4 v5, 0x0

    const-string v6, "accepted_sms_promo_screen_notify_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 144
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/16 v1, 0x9

    new-instance v2, Lbfy;

    const/4 v3, 0x4

    const/16 v4, 0x8

    const/4 v5, 0x0

    const-string v6, "declined_sms_promo_screen_notify_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 151
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/16 v1, 0xa

    new-instance v2, Lbfy;

    const/4 v3, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x0

    const-string v6, "shown_sms_promo_banner_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 156
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/16 v1, 0xb

    new-instance v2, Lbfy;

    const/4 v3, 0x5

    const/4 v4, 0x7

    const/4 v5, 0x0

    const-string v6, "accepted_sms_promo_banner_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 161
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/16 v1, 0xc

    new-instance v2, Lbfy;

    const/4 v3, 0x5

    const/16 v4, 0x8

    const/4 v5, 0x0

    const-string v6, "declined_sms_promo_banner_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 166
    iget-object v0, p0, Lbqw;->e:[Lbfy;

    const/16 v1, 0xd

    new-instance v2, Lbfy;

    const/4 v3, 0x6

    const/4 v4, 0x6

    const/4 v5, 0x0

    const-string v6, "shown_sms_promo_notify_count_since_last_upload"

    invoke-direct {v2, v3, v4, v5, v6}, Lbfy;-><init>(IIILjava/lang/String;)V

    aput-object v2, v0, v1

    .line 171
    return-void
.end method

.method public static a(Lyj;)Lbqw;
    .locals 7

    .prologue
    .line 54
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v6

    .line 55
    sget-object v0, Lbqw;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqw;

    .line 56
    if-nez v0, :cond_0

    .line 58
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_upload_analytics_lomark_seconds"

    const/16 v2, 0x7080

    .line 57
    invoke-static {v0, v1, v2}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v2, v0

    .line 63
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_upload_analytics_highmark_seconds"

    const v4, 0xa8c0

    .line 62
    invoke-static {v0, v1, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    .line 68
    new-instance v0, Lbqw;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbqw;-><init>(Lyj;JJ)V

    .line 75
    sget-object v1, Lbqw;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lbqw;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqw;

    .line 78
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v0, 0x0

    const-wide/16 v6, 0x0

    .line 179
    iget-object v1, p0, Lbqw;->c:Lbnl;

    invoke-virtual {v1}, Lbnl;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    move v1, v0

    .line 183
    :goto_0
    iget-object v2, p0, Lbqw;->e:[Lbfy;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 184
    iget-object v2, p0, Lbqw;->e:[Lbfy;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lbfy;->a()Ljava/lang/String;

    move-result-object v2

    .line 185
    iget-object v3, p0, Lbqw;->b:Lyj;

    invoke-static {v3}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v3

    invoke-virtual {v3, v2, v6, v7}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v3

    .line 186
    iget-object v5, p0, Lbqw;->e:[Lbfy;

    aget-object v5, v5, v0

    invoke-virtual {v5, v3, v4}, Lbfy;->a(J)V

    .line 187
    cmp-long v3, v3, v6

    if-lez v3, :cond_0

    .line 190
    iget-object v3, p0, Lbqw;->b:Lyj;

    invoke-static {v3}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v3

    invoke-virtual {v3, v2, v6, v7}, Lbsx;->b(Ljava/lang/String;J)V

    .line 191
    add-int/lit8 v1, v1, 0x1

    .line 183
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 198
    new-instance v2, Lyt;

    iget-object v3, p0, Lbqw;->b:Lyj;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    .line 199
    invoke-virtual {v2, v0}, Lyt;->x(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 202
    if-gtz v1, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 203
    :cond_2
    new-instance v3, Lbfw;

    .line 204
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    const/4 v0, 0x0

    :cond_3
    iget-object v4, p0, Lbqw;->e:[Lbfy;

    invoke-direct {v3, v0, v4, v1, v2}, Lbfw;-><init>(Ljava/lang/String;[Lbfy;ILjava/util/List;)V

    .line 206
    iget-object v0, p0, Lbqw;->c:Lbnl;

    invoke-virtual {v0, v3}, Lbnl;->a(Lbea;)V

    .line 214
    :goto_1
    return-void

    .line 208
    :cond_4
    const-string v0, "Babel"

    invoke-static {v0, v8}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 209
    const-string v0, "Babel"

    const-string v1, "Skip UploadAnalyticsOperation since there is nothing to upload"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_5
    invoke-virtual {p0, v8}, Lbqw;->a(I)V

    goto :goto_1
.end method

.method protected a(J)V
    .locals 2

    .prologue
    .line 234
    iput-wide p1, p0, Lbqw;->d:J

    .line 235
    iget-object v0, p0, Lbqw;->b:Lyj;

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "last_upload_analytics_time"

    invoke-virtual {v0, v1, p1, p2}, Lbsx;->b(Ljava/lang/String;J)V

    .line 237
    return-void
.end method

.method protected i()J
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 222
    iget-wide v0, p0, Lbqw;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lbqw;->b:Lyj;

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "last_upload_analytics_time"

    invoke-virtual {v0, v1, v2, v3}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lbqw;->d:J

    .line 226
    :cond_0
    iget-wide v0, p0, Lbqw;->d:J

    return-wide v0
.end method
