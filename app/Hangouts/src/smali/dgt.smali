.class public final Ldgt;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgt;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Integer;

.field public i:[Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    new-array v0, v0, [Ldgt;

    sput-object v0, Ldgt;->a:[Ldgt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 273
    invoke-direct {p0}, Lepn;-><init>()V

    .line 288
    const/4 v0, 0x0

    iput-object v0, p0, Ldgt;->e:Ljava/lang/Integer;

    .line 297
    sget-object v0, Lept;->n:[Ljava/lang/Long;

    iput-object v0, p0, Ldgt;->i:[Ljava/lang/Long;

    .line 273
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 335
    iget-object v0, p0, Ldgt;->b:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 336
    const/4 v0, 0x1

    iget-object v2, p0, Ldgt;->b:Ljava/lang/Long;

    .line 337
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lepl;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 339
    :goto_0
    iget-object v2, p0, Ldgt;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 340
    const/4 v2, 0x2

    iget-object v3, p0, Ldgt;->c:Ljava/lang/Long;

    .line 341
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 343
    :cond_0
    iget-object v2, p0, Ldgt;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 344
    const/4 v2, 0x3

    iget-object v3, p0, Ldgt;->d:Ljava/lang/Boolean;

    .line 345
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 347
    :cond_1
    iget-object v2, p0, Ldgt;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 348
    const/4 v2, 0x4

    iget-object v3, p0, Ldgt;->e:Ljava/lang/Integer;

    .line 349
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 351
    :cond_2
    iget-object v2, p0, Ldgt;->f:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 352
    const/4 v2, 0x5

    iget-object v3, p0, Ldgt;->f:Ljava/lang/Long;

    .line 353
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 355
    :cond_3
    iget-object v2, p0, Ldgt;->g:Ljava/lang/Long;

    if-eqz v2, :cond_4

    .line 356
    const/4 v2, 0x6

    iget-object v3, p0, Ldgt;->g:Ljava/lang/Long;

    .line 357
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 359
    :cond_4
    iget-object v2, p0, Ldgt;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 360
    const/4 v2, 0x7

    iget-object v3, p0, Ldgt;->h:Ljava/lang/Integer;

    .line 361
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 363
    :cond_5
    iget-object v2, p0, Ldgt;->i:[Ljava/lang/Long;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ldgt;->i:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 365
    iget-object v3, p0, Ldgt;->i:[Ljava/lang/Long;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 367
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lepl;->c(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 365
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 369
    :cond_6
    add-int/2addr v0, v2

    .line 370
    iget-object v1, p0, Ldgt;->i:[Ljava/lang/Long;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 372
    :cond_7
    iget-object v1, p0, Ldgt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 373
    iput v0, p0, Ldgt;->cachedSize:I

    .line 374
    return v0

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 269
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgt;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgt;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgt;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgt;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgt;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgt;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgt;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgt;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgt;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgt;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldgt;->i:[Ljava/lang/Long;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    iget-object v2, p0, Ldgt;->i:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldgt;->i:[Ljava/lang/Long;

    :goto_1
    iget-object v1, p0, Ldgt;->i:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Ldgt;->i:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Ldgt;->i:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 7

    .prologue
    .line 302
    iget-object v0, p0, Ldgt;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 303
    const/4 v0, 0x1

    iget-object v1, p0, Ldgt;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 305
    :cond_0
    iget-object v0, p0, Ldgt;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 306
    const/4 v0, 0x2

    iget-object v1, p0, Ldgt;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 308
    :cond_1
    iget-object v0, p0, Ldgt;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 309
    const/4 v0, 0x3

    iget-object v1, p0, Ldgt;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 311
    :cond_2
    iget-object v0, p0, Ldgt;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 312
    const/4 v0, 0x4

    iget-object v1, p0, Ldgt;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 314
    :cond_3
    iget-object v0, p0, Ldgt;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 315
    const/4 v0, 0x5

    iget-object v1, p0, Ldgt;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 317
    :cond_4
    iget-object v0, p0, Ldgt;->g:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 318
    const/4 v0, 0x6

    iget-object v1, p0, Ldgt;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 320
    :cond_5
    iget-object v0, p0, Ldgt;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 321
    const/4 v0, 0x7

    iget-object v1, p0, Ldgt;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 323
    :cond_6
    iget-object v0, p0, Ldgt;->i:[Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 324
    iget-object v1, p0, Ldgt;->i:[Ljava/lang/Long;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 325
    const/16 v4, 0x8

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {p1, v4, v5, v6}, Lepl;->b(IJ)V

    .line 324
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 328
    :cond_7
    iget-object v0, p0, Ldgt;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 330
    return-void
.end method
