.class public final Lexv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lexv;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Lexs;

.field public d:Lexs;

.field public e:Lexs;

.field public f:Levt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    new-array v0, v0, [Lexv;

    sput-object v0, Lexv;->a:[Lexv;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 155
    invoke-direct {p0}, Lepn;-><init>()V

    .line 160
    sget-object v0, Lexs;->a:[Lexs;

    iput-object v0, p0, Lexv;->c:[Lexs;

    .line 163
    iput-object v1, p0, Lexv;->d:Lexs;

    .line 166
    iput-object v1, p0, Lexv;->e:Lexs;

    .line 169
    iput-object v1, p0, Lexv;->f:Levt;

    .line 155
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 197
    const/4 v0, 0x1

    iget-object v1, p0, Lexv;->b:Ljava/lang/String;

    .line 199
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 200
    iget-object v1, p0, Lexv;->c:[Lexs;

    if-eqz v1, :cond_1

    .line 201
    iget-object v2, p0, Lexv;->c:[Lexs;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 202
    if-eqz v4, :cond_0

    .line 203
    const/4 v5, 0x2

    .line 204
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 201
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :cond_1
    iget-object v1, p0, Lexv;->d:Lexs;

    if-eqz v1, :cond_2

    .line 209
    const/4 v1, 0x3

    iget-object v2, p0, Lexv;->d:Lexs;

    .line 210
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_2
    iget-object v1, p0, Lexv;->e:Lexs;

    if-eqz v1, :cond_3

    .line 213
    const/4 v1, 0x4

    iget-object v2, p0, Lexv;->e:Lexs;

    .line 214
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_3
    iget-object v1, p0, Lexv;->f:Levt;

    if-eqz v1, :cond_4

    .line 217
    const/4 v1, 0x5

    iget-object v2, p0, Lexv;->f:Levt;

    .line 218
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_4
    iget-object v1, p0, Lexv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    iput v0, p0, Lexv;->cachedSize:I

    .line 222
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 151
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lexv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lexv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lexv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexv;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexv;->c:[Lexs;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lexs;

    iget-object v3, p0, Lexv;->c:[Lexs;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lexv;->c:[Lexs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lexv;->c:[Lexs;

    :goto_2
    iget-object v2, p0, Lexv;->c:[Lexs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lexv;->c:[Lexs;

    new-instance v3, Lexs;

    invoke-direct {v3}, Lexs;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexv;->c:[Lexs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lexv;->c:[Lexs;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lexv;->c:[Lexs;

    new-instance v3, Lexs;

    invoke-direct {v3}, Lexs;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexv;->c:[Lexs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lexv;->d:Lexs;

    if-nez v0, :cond_5

    new-instance v0, Lexs;

    invoke-direct {v0}, Lexs;-><init>()V

    iput-object v0, p0, Lexv;->d:Lexs;

    :cond_5
    iget-object v0, p0, Lexv;->d:Lexs;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lexv;->e:Lexs;

    if-nez v0, :cond_6

    new-instance v0, Lexs;

    invoke-direct {v0}, Lexs;-><init>()V

    iput-object v0, p0, Lexv;->e:Lexs;

    :cond_6
    iget-object v0, p0, Lexv;->e:Lexs;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lexv;->f:Levt;

    if-nez v0, :cond_7

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lexv;->f:Levt;

    :cond_7
    iget-object v0, p0, Lexv;->f:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 174
    const/4 v0, 0x1

    iget-object v1, p0, Lexv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 175
    iget-object v0, p0, Lexv;->c:[Lexs;

    if-eqz v0, :cond_1

    .line 176
    iget-object v1, p0, Lexv;->c:[Lexs;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 177
    if-eqz v3, :cond_0

    .line 178
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 176
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_1
    iget-object v0, p0, Lexv;->d:Lexs;

    if-eqz v0, :cond_2

    .line 183
    const/4 v0, 0x3

    iget-object v1, p0, Lexv;->d:Lexs;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 185
    :cond_2
    iget-object v0, p0, Lexv;->e:Lexs;

    if-eqz v0, :cond_3

    .line 186
    const/4 v0, 0x4

    iget-object v1, p0, Lexv;->e:Lexs;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 188
    :cond_3
    iget-object v0, p0, Lexv;->f:Levt;

    if-eqz v0, :cond_4

    .line 189
    const/4 v0, 0x5

    iget-object v1, p0, Lexv;->f:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 191
    :cond_4
    iget-object v0, p0, Lexv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 193
    return-void
.end method
