.class public final Lahh;
.super Laxs;
.source "PG"


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final d:Lyj;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 7571
    invoke-direct {p0, p1}, Laxs;-><init>(Ljava/lang/String;)V

    .line 7572
    iput-object p2, p0, Lahh;->b:Ljava/lang/String;

    .line 7573
    iput-object p3, p0, Lahh;->d:Lyj;

    .line 7574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahh;->a:Z

    .line 7575
    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 7579
    new-instance v8, Lyt;

    iget-object v0, p0, Lahh;->d:Lyj;

    invoke-direct {v8, v0}, Lyt;-><init>(Lyj;)V

    .line 7581
    invoke-virtual {v8}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v1, "conversation_participants_view"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "gaia_id"

    aput-object v3, v2, v9

    const-string v3, "chat_id"

    aput-object v3, v2, v6

    const-string v3, "conversation_id=?"

    new-array v4, v6, [Ljava/lang/String;

    iget-object v5, p0, Lahh;->b:Ljava/lang/String;

    aput-object v5, v4, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 7595
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 7596
    const-string v0, "status"

    .line 7597
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 7596
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7598
    const-string v3, "inviter_affinity"

    iget-boolean v0, p0, Lahh;->a:Z

    if-eqz v0, :cond_2

    move v0, v6

    .line 7600
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 7598
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7605
    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7606
    const-string v0, "inviter_gaia_id"

    .line 7608
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 7606
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7609
    const-string v0, "inviter_chat_id"

    .line 7611
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 7609
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7613
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7615
    const-string v0, "self_watermark"

    .line 7616
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 7615
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7617
    const-string v0, "chat_watermark"

    .line 7619
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 7617
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7621
    const-string v0, "hangout_watermark"

    .line 7623
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 7621
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7626
    iget-boolean v0, p0, Lahh;->a:Z

    if-eqz v0, :cond_1

    .line 7627
    const-string v0, "notification_level"

    const/16 v1, 0x1e

    .line 7628
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 7627
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7631
    :cond_1
    invoke-virtual {v8}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v1, "conversations"

    const-string v3, "conversation_id=?"

    new-array v4, v6, [Ljava/lang/String;

    iget-object v5, p0, Lahh;->b:Ljava/lang/String;

    aput-object v5, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 7636
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "modifying "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lahh;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pretend invite status with inviter_id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "inviter_chat_id"

    .line 7639
    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " updated "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " rows"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7636
    invoke-static {v1, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7643
    iget-object v0, p0, Lahh;->d:Lyj;

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 7644
    iget-object v0, p0, Lahh;->d:Lyj;

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2, v6, v9}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    .line 7646
    return-void

    :cond_2
    move v0, v7

    .line 7598
    goto/16 :goto_0
.end method
