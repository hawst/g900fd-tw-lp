.class public final Ldtm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtm;


# instance fields
.field public b:Ldui;

.field public c:Ldrq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3322
    const/4 v0, 0x0

    new-array v0, v0, [Ldtm;

    sput-object v0, Ldtm;->a:[Ldtm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3323
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3326
    iput-object v0, p0, Ldtm;->b:Ldui;

    .line 3329
    iput-object v0, p0, Ldtm;->c:Ldrq;

    .line 3323
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3346
    const/4 v0, 0x0

    .line 3347
    iget-object v1, p0, Ldtm;->b:Ldui;

    if-eqz v1, :cond_0

    .line 3348
    const/4 v0, 0x1

    iget-object v1, p0, Ldtm;->b:Ldui;

    .line 3349
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3351
    :cond_0
    iget-object v1, p0, Ldtm;->c:Ldrq;

    if-eqz v1, :cond_1

    .line 3352
    const/4 v1, 0x2

    iget-object v2, p0, Ldtm;->c:Ldrq;

    .line 3353
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3355
    :cond_1
    iget-object v1, p0, Ldtm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3356
    iput v0, p0, Ldtm;->cachedSize:I

    .line 3357
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3319
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldtm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldtm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldtm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldtm;->b:Ldui;

    if-nez v0, :cond_2

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldtm;->b:Ldui;

    :cond_2
    iget-object v0, p0, Ldtm;->b:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldtm;->c:Ldrq;

    if-nez v0, :cond_3

    new-instance v0, Ldrq;

    invoke-direct {v0}, Ldrq;-><init>()V

    iput-object v0, p0, Ldtm;->c:Ldrq;

    :cond_3
    iget-object v0, p0, Ldtm;->c:Ldrq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3334
    iget-object v0, p0, Ldtm;->b:Ldui;

    if-eqz v0, :cond_0

    .line 3335
    const/4 v0, 0x1

    iget-object v1, p0, Ldtm;->b:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3337
    :cond_0
    iget-object v0, p0, Ldtm;->c:Ldrq;

    if-eqz v0, :cond_1

    .line 3338
    const/4 v0, 0x2

    iget-object v1, p0, Ldtm;->c:Ldrq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3340
    :cond_1
    iget-object v0, p0, Ldtm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3342
    return-void
.end method
