.class final Lara;
.super Landroid/view/animation/Animation;
.source "PG"


# instance fields
.field final synthetic a:Landroid/widget/RelativeLayout$LayoutParams;

.field final synthetic b:I

.field final synthetic c:Laqv;


# direct methods
.method constructor <init>(Laqv;Landroid/widget/RelativeLayout$LayoutParams;I)V
    .locals 0

    .prologue
    .line 617
    iput-object p1, p0, Lara;->c:Laqv;

    iput-object p2, p0, Lara;->a:Landroid/widget/RelativeLayout$LayoutParams;

    iput p3, p0, Lara;->b:I

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 620
    iget-object v0, p0, Lara;->a:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lara;->b:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x0

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 621
    iget-object v0, p0, Lara;->c:Laqv;

    iget-object v0, v0, Laqv;->i:Landroid/view/ViewGroup;

    iget-object v1, p0, Lara;->a:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 622
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 623
    iget-object v0, p0, Lara;->c:Laqv;

    sub-float v1, v2, p1

    invoke-virtual {v0, v1}, Laqv;->a(F)V

    .line 625
    :cond_0
    iget-object v0, p0, Lara;->c:Laqv;

    iget-object v0, v0, Laqv;->j:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->e()V

    .line 626
    cmpl-float v0, p1, v2

    if-nez v0, :cond_1

    .line 627
    iget-object v0, p0, Lara;->c:Laqv;

    invoke-virtual {v0}, Laqv;->b()V

    .line 629
    :cond_1
    return-void
.end method
