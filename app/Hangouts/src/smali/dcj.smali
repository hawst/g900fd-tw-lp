.class public final Ldcj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldcj;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Ldck;

.field public d:[Ldce;

.field public e:Ldcl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 754
    const/4 v0, 0x0

    new-array v0, v0, [Ldcj;

    sput-object v0, Ldcj;->a:[Ldcj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 755
    invoke-direct {p0}, Lepn;-><init>()V

    .line 760
    sget-object v0, Ldck;->a:[Ldck;

    iput-object v0, p0, Ldcj;->c:[Ldck;

    .line 763
    sget-object v0, Ldce;->a:[Ldce;

    iput-object v0, p0, Ldcj;->d:[Ldce;

    .line 766
    const/4 v0, 0x0

    iput-object v0, p0, Ldcj;->e:Ldcl;

    .line 755
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 798
    iget-object v0, p0, Ldcj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 799
    const/4 v0, 0x1

    iget-object v2, p0, Ldcj;->b:Ljava/lang/Integer;

    .line 800
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 802
    :goto_0
    iget-object v2, p0, Ldcj;->c:[Ldck;

    if-eqz v2, :cond_1

    .line 803
    iget-object v3, p0, Ldcj;->c:[Ldck;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 804
    if-eqz v5, :cond_0

    .line 805
    const/4 v6, 0x2

    .line 806
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 803
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 810
    :cond_1
    iget-object v2, p0, Ldcj;->d:[Ldce;

    if-eqz v2, :cond_3

    .line 811
    iget-object v2, p0, Ldcj;->d:[Ldce;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 812
    if-eqz v4, :cond_2

    .line 813
    const/4 v5, 0x3

    .line 814
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 811
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 818
    :cond_3
    iget-object v1, p0, Ldcj;->e:Ldcl;

    if-eqz v1, :cond_4

    .line 819
    const/4 v1, 0x4

    iget-object v2, p0, Ldcj;->e:Ldcl;

    .line 820
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 822
    :cond_4
    iget-object v1, p0, Ldcj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 823
    iput v0, p0, Ldcj;->cachedSize:I

    .line 824
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 751
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldcj;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldcj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldcj;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldcj;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldcj;->c:[Ldck;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldck;

    iget-object v3, p0, Ldcj;->c:[Ldck;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldcj;->c:[Ldck;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldcj;->c:[Ldck;

    :goto_2
    iget-object v2, p0, Ldcj;->c:[Ldck;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldcj;->c:[Ldck;

    new-instance v3, Ldck;

    invoke-direct {v3}, Ldck;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldcj;->c:[Ldck;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldcj;->c:[Ldck;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldcj;->c:[Ldck;

    new-instance v3, Ldck;

    invoke-direct {v3}, Ldck;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldcj;->c:[Ldck;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldcj;->d:[Ldce;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldce;

    iget-object v3, p0, Ldcj;->d:[Ldce;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldcj;->d:[Ldce;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldcj;->d:[Ldce;

    :goto_4
    iget-object v2, p0, Ldcj;->d:[Ldce;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldcj;->d:[Ldce;

    new-instance v3, Ldce;

    invoke-direct {v3}, Ldce;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldcj;->d:[Ldce;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ldcj;->d:[Ldce;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldcj;->d:[Ldce;

    new-instance v3, Ldce;

    invoke-direct {v3}, Ldce;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldcj;->d:[Ldce;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Ldcj;->e:Ldcl;

    if-nez v0, :cond_8

    new-instance v0, Ldcl;

    invoke-direct {v0}, Ldcl;-><init>()V

    iput-object v0, p0, Ldcj;->e:Ldcl;

    :cond_8
    iget-object v0, p0, Ldcj;->e:Ldcl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 771
    iget-object v1, p0, Ldcj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 772
    const/4 v1, 0x1

    iget-object v2, p0, Ldcj;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 774
    :cond_0
    iget-object v1, p0, Ldcj;->c:[Ldck;

    if-eqz v1, :cond_2

    .line 775
    iget-object v2, p0, Ldcj;->c:[Ldck;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 776
    if-eqz v4, :cond_1

    .line 777
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 775
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 781
    :cond_2
    iget-object v1, p0, Ldcj;->d:[Ldce;

    if-eqz v1, :cond_4

    .line 782
    iget-object v1, p0, Ldcj;->d:[Ldce;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 783
    if-eqz v3, :cond_3

    .line 784
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 782
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 788
    :cond_4
    iget-object v0, p0, Ldcj;->e:Ldcl;

    if-eqz v0, :cond_5

    .line 789
    const/4 v0, 0x4

    iget-object v1, p0, Ldcj;->e:Ldcl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 791
    :cond_5
    iget-object v0, p0, Ldcj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 793
    return-void
.end method
