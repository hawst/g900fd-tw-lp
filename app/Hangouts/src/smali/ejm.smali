.class public final Lejm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lejm;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Lejd;

.field public d:[Ljava/lang/Long;

.field public e:[Ljava/lang/Boolean;

.field public f:[Leje;

.field public g:Ljava/lang/String;

.field public h:Lejn;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1132
    const/4 v0, 0x0

    new-array v0, v0, [Lejm;

    sput-object v0, Lejm;->a:[Lejm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1133
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1138
    sget-object v0, Lejd;->a:[Lejd;

    iput-object v0, p0, Lejm;->c:[Lejd;

    .line 1141
    sget-object v0, Lept;->n:[Ljava/lang/Long;

    iput-object v0, p0, Lejm;->d:[Ljava/lang/Long;

    .line 1144
    sget-object v0, Lept;->q:[Ljava/lang/Boolean;

    iput-object v0, p0, Lejm;->e:[Ljava/lang/Boolean;

    .line 1147
    sget-object v0, Leje;->a:[Leje;

    iput-object v0, p0, Lejm;->f:[Leje;

    .line 1152
    const/4 v0, 0x0

    iput-object v0, p0, Lejm;->h:Lejn;

    .line 1133
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1207
    iget-object v0, p0, Lejm;->c:[Lejd;

    if-eqz v0, :cond_1

    .line 1208
    iget-object v3, p0, Lejm;->c:[Lejd;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1209
    if-eqz v5, :cond_0

    .line 1210
    const/4 v6, 0x1

    .line 1211
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1208
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1215
    :cond_2
    iget-object v2, p0, Lejm;->g:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1216
    const/4 v2, 0x2

    iget-object v3, p0, Lejm;->g:Ljava/lang/String;

    .line 1217
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1219
    :cond_3
    iget-object v2, p0, Lejm;->h:Lejn;

    if-eqz v2, :cond_4

    .line 1220
    const/4 v2, 0x3

    iget-object v3, p0, Lejm;->h:Lejn;

    .line 1221
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1223
    :cond_4
    iget-object v2, p0, Lejm;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 1224
    const/4 v2, 0x4

    iget-object v3, p0, Lejm;->i:Ljava/lang/Integer;

    .line 1225
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1227
    :cond_5
    iget-object v2, p0, Lejm;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 1228
    const/4 v2, 0x5

    iget-object v3, p0, Lejm;->j:Ljava/lang/Integer;

    .line 1229
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1231
    :cond_6
    iget-object v2, p0, Lejm;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 1232
    const/4 v2, 0x6

    iget-object v3, p0, Lejm;->b:Ljava/lang/Integer;

    .line 1233
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1235
    :cond_7
    iget-object v2, p0, Lejm;->d:[Ljava/lang/Long;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lejm;->d:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 1237
    iget-object v4, p0, Lejm;->d:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_8

    aget-object v6, v4, v2

    .line 1239
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lepl;->c(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 1237
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1241
    :cond_8
    add-int/2addr v0, v3

    .line 1242
    iget-object v2, p0, Lejm;->d:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1244
    :cond_9
    iget-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 1245
    iget-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    .line 1246
    add-int/2addr v0, v2

    .line 1247
    iget-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1249
    :cond_a
    iget-object v2, p0, Lejm;->f:[Leje;

    if-eqz v2, :cond_c

    .line 1250
    iget-object v2, p0, Lejm;->f:[Leje;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 1251
    if-eqz v4, :cond_b

    .line 1252
    const/16 v5, 0x9

    .line 1253
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1250
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1257
    :cond_c
    iget-object v1, p0, Lejm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1258
    iput v0, p0, Lejm;->cachedSize:I

    .line 1259
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1129
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lejm;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lejm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lejm;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lejm;->c:[Lejd;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lejd;

    iget-object v3, p0, Lejm;->c:[Lejd;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lejm;->c:[Lejd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lejm;->c:[Lejd;

    :goto_2
    iget-object v2, p0, Lejm;->c:[Lejd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lejm;->c:[Lejd;

    new-instance v3, Lejd;

    invoke-direct {v3}, Lejd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lejm;->c:[Lejd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lejm;->c:[Lejd;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lejm;->c:[Lejd;

    new-instance v3, Lejd;

    invoke-direct {v3}, Lejd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lejm;->c:[Lejd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lejm;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lejm;->h:Lejn;

    if-nez v0, :cond_5

    new-instance v0, Lejn;

    invoke-direct {v0}, Lejn;-><init>()V

    iput-object v0, p0, Lejm;->h:Lejn;

    :cond_5
    iget-object v0, p0, Lejm;->h:Lejn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lejm;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lejm;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lejm;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lejm;->d:[Ljava/lang/Long;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    iget-object v3, p0, Lejm;->d:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lejm;->d:[Ljava/lang/Long;

    :goto_3
    iget-object v2, p0, Lejm;->d:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lejm;->d:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v2, p0, Lejm;->d:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lejm;->e:[Ljava/lang/Boolean;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Boolean;

    iget-object v3, p0, Lejm;->e:[Ljava/lang/Boolean;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    :goto_4
    iget-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    invoke-virtual {p1}, Lepk;->i()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    invoke-virtual {p1}, Lepk;->i()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lejm;->f:[Leje;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Leje;

    iget-object v3, p0, Lejm;->f:[Leje;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lejm;->f:[Leje;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lejm;->f:[Leje;

    :goto_6
    iget-object v2, p0, Lejm;->f:[Leje;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lejm;->f:[Leje;

    new-instance v3, Leje;

    invoke-direct {v3}, Leje;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lejm;->f:[Leje;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lejm;->f:[Leje;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Lejm;->f:[Leje;

    new-instance v3, Leje;

    invoke-direct {v3}, Leje;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lejm;->f:[Leje;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1161
    iget-object v1, p0, Lejm;->c:[Lejd;

    if-eqz v1, :cond_1

    .line 1162
    iget-object v2, p0, Lejm;->c:[Lejd;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1163
    if-eqz v4, :cond_0

    .line 1164
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 1162
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1168
    :cond_1
    iget-object v1, p0, Lejm;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1169
    const/4 v1, 0x2

    iget-object v2, p0, Lejm;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 1171
    :cond_2
    iget-object v1, p0, Lejm;->h:Lejn;

    if-eqz v1, :cond_3

    .line 1172
    const/4 v1, 0x3

    iget-object v2, p0, Lejm;->h:Lejn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 1174
    :cond_3
    iget-object v1, p0, Lejm;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1175
    const/4 v1, 0x4

    iget-object v2, p0, Lejm;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 1177
    :cond_4
    iget-object v1, p0, Lejm;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1178
    const/4 v1, 0x5

    iget-object v2, p0, Lejm;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 1180
    :cond_5
    iget-object v1, p0, Lejm;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 1181
    const/4 v1, 0x6

    iget-object v2, p0, Lejm;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 1183
    :cond_6
    iget-object v1, p0, Lejm;->d:[Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 1184
    iget-object v2, p0, Lejm;->d:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 1185
    const/4 v5, 0x7

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Lepl;->b(IJ)V

    .line 1184
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1188
    :cond_7
    iget-object v1, p0, Lejm;->e:[Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 1189
    iget-object v2, p0, Lejm;->e:[Ljava/lang/Boolean;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 1190
    const/16 v5, 0x8

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(IZ)V

    .line 1189
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1193
    :cond_8
    iget-object v1, p0, Lejm;->f:[Leje;

    if-eqz v1, :cond_a

    .line 1194
    iget-object v1, p0, Lejm;->f:[Leje;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 1195
    if-eqz v3, :cond_9

    .line 1196
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1194
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1200
    :cond_a
    iget-object v0, p0, Lejm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1202
    return-void
.end method
