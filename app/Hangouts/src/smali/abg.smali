.class public final Labg;
.super Lpi;
.source "PG"

# interfaces
.implements Lcaz;


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:Z

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lyj;

.field private final f:I

.field private final g:Lt;

.field private final h:Labi;

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lxs;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lxo;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laea;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laea;",
            ">;"
        }
    .end annotation
.end field

.field private m:Landroid/database/Cursor;

.field private final n:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Laea;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lbak;

.field private p:Lbak;

.field private final q:Lamd;

.field private final r:Lcvr;

.field private s:Z

.field private final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcax;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcvx;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77
    sget-object v0, Lbys;->d:Lcyp;

    sput-boolean v3, Labg;->b:Z

    .line 103
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "first_name"

    aput-object v1, v0, v4

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "chat_id"

    aput-object v1, v0, v6

    const-string v1, "profile_photo_url"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "suggestion_type"

    aput-object v2, v0, v1

    sput-object v0, Labg;->a:[Ljava/lang/String;

    .line 119
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v4

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "chat_id"

    aput-object v1, v0, v6

    const-string v1, "name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "first_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "photo_url"

    aput-object v2, v0, v1

    sput-object v0, Labg;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lyj;Lt;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 249
    invoke-direct {p0, p1, v0}, Lpi;-><init>(Landroid/content/Context;B)V

    .line 181
    new-instance v1, Les;

    invoke-direct {v1}, Les;-><init>()V

    iput-object v1, p0, Labg;->n:Les;

    .line 190
    new-instance v1, Lamd;

    invoke-direct {v1}, Lamd;-><init>()V

    iput-object v1, p0, Labg;->q:Lamd;

    .line 193
    new-instance v1, Labh;

    invoke-direct {v1, p0}, Labh;-><init>(Labg;)V

    iput-object v1, p0, Labg;->r:Lcvr;

    .line 230
    iput-boolean v0, p0, Labg;->s:Z

    .line 250
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 251
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Labg;->a(Z)V

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_0
    iput-object p1, p0, Labg;->d:Landroid/content/Context;

    .line 255
    iput-object p2, p0, Labg;->e:Lyj;

    .line 256
    iput p4, p0, Labg;->f:I

    .line 257
    iput-object p3, p0, Labg;->g:Lt;

    .line 258
    check-cast p3, Labi;

    iput-object p3, p0, Labg;->h:Labi;

    .line 259
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Labg;->i:Ljava/util/Map;

    .line 260
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Labg;->j:Ljava/util/Map;

    .line 261
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Labg;->k:Ljava/util/Map;

    .line 262
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Labg;->l:Ljava/util/Map;

    .line 263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Labg;->t:Ljava/util/List;

    .line 264
    return-void
.end method

.method private a(Ljava/lang/String;)Laea;
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Labg;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laea;

    return-object v0
.end method

.method static synthetic a(Labg;)Lamd;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Labg;->q:Lamd;

    return-object v0
.end method

.method private a(Lbcx;)Z
    .locals 2

    .prologue
    .line 999
    iget-object v0, p0, Labg;->i:Ljava/util/Map;

    invoke-virtual {p1}, Lbcx;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Labg;)V
    .locals 0

    .prologue
    .line 74
    invoke-virtual {p0}, Labg;->c()V

    return-void
.end method

.method public static g()V
    .locals 0

    .prologue
    .line 267
    return-void
.end method

.method public static m()V
    .locals 0

    .prologue
    .line 749
    return-void
.end method

.method public static n()V
    .locals 0

    .prologue
    .line 760
    return-void
.end method

.method static synthetic q()Z
    .locals 1

    .prologue
    .line 74
    sget-boolean v0, Labg;->b:Z

    return v0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 1005
    iget-object v0, p0, Labg;->e:Lyj;

    invoke-virtual {v0}, Lyj;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1006
    iget-object v0, p0, Labg;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1007
    iget-object v0, p0, Labg;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1008
    iget-object v0, p0, Labg;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1009
    iget-object v0, p0, Labg;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1011
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(II)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 401
    packed-switch p1, :pswitch_data_0

    .line 421
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid partition for AudienceAdapter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 403
    :pswitch_0
    invoke-virtual {p0, p1}, Labg;->c(I)Landroid/database/Cursor;

    move-result-object v0

    .line 404
    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 405
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 409
    :pswitch_1
    invoke-virtual {p0, p1}, Labg;->c(I)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lbrw;

    .line 410
    invoke-virtual {v0, p2}, Lbrw;->moveToPosition(I)Z

    .line 411
    invoke-virtual {v0}, Lbrw;->a()Lcvw;

    move-result-object v0

    .line 412
    if-eqz v0, :cond_1

    .line 413
    invoke-interface {v0}, Lcvw;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcvw;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 416
    goto :goto_0

    .line 401
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(ILandroid/database/Cursor;ILandroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 471
    if-eqz p4, :cond_0

    invoke-virtual {p0, p1, p3}, Labg;->a(II)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    .line 477
    :goto_1
    invoke-virtual {p0, p4, p1, p2, p3}, Labg;->a(Landroid/view/View;ILandroid/database/Cursor;I)V

    .line 478
    return-object p4

    .line 471
    :pswitch_0
    instance-of v0, p4, Lceb;

    goto :goto_0

    :pswitch_1
    instance-of v0, p4, Lceb;

    goto :goto_0

    :pswitch_2
    instance-of v0, p4, Lcbn;

    goto :goto_0

    .line 474
    :cond_0
    iget-object v0, p0, Labg;->d:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, p3}, Labg;->a(Landroid/content/Context;II)Landroid/view/View;

    move-result-object p4

    goto :goto_1

    .line 471
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;II)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 510
    invoke-virtual {p0, p2, p3}, Labg;->a(II)I

    move-result v0

    .line 512
    packed-switch v0, :pswitch_data_0

    .line 525
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid partition for AudienceAdapter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 515
    :pswitch_0
    iget-object v0, p0, Labg;->g:Lt;

    iget v1, p0, Labg;->f:I

    invoke-static {p1, v0, v4, v1}, Lceb;->createInstance(Landroid/content/Context;Lt;ZI)Lceb;

    move-result-object v0

    goto :goto_0

    .line 520
    :pswitch_1
    new-instance v0, Lcbn;

    iget-object v2, p0, Labg;->g:Lt;

    iget-object v3, p0, Labg;->e:Lyj;

    iget v5, p0, Labg;->f:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcbn;-><init>(Landroid/content/Context;Lt;Lyj;ZI)V

    goto :goto_0

    .line 512
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 487
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 488
    sget v1, Lf;->eb:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 490
    sget v0, Lg;->hz:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 492
    packed-switch p2, :pswitch_data_0

    .line 501
    :goto_0
    return-object v1

    .line 494
    :pswitch_0
    sget v2, Lh;->cU:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 497
    :pswitch_1
    sget v2, Lh;->u:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 492
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)Ljava/util/HashSet;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 311
    const-string v0, "Babel"

    const-string v2, "setContacts"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iput-object p1, p0, Labg;->m:Landroid/database/Cursor;

    .line 314
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 316
    if-nez p1, :cond_0

    move-object v0, v2

    .line 365
    :goto_0
    return-object v0

    .line 320
    :cond_0
    new-instance v0, Lbak;

    sget-object v3, Labg;->c:[Ljava/lang/String;

    .line 321
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lbak;-><init>([Ljava/lang/String;I)V

    iput-object v0, p0, Labg;->o:Lbak;

    .line 322
    new-instance v0, Lbak;

    sget-object v3, Labg;->c:[Ljava/lang/String;

    .line 323
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lbak;-><init>([Ljava/lang/String;I)V

    iput-object v0, p0, Labg;->p:Lbak;

    .line 327
    iget-object v0, p0, Labg;->n:Les;

    invoke-virtual {v0}, Les;->clear()V

    .line 328
    iget-object v0, p0, Labg;->n:Les;

    iget-object v3, p0, Labg;->e:Lyj;

    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v3

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v6}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 330
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 331
    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 332
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 334
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 338
    iget-object v4, p0, Labg;->n:Les;

    invoke-virtual {v4, v3, v6}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 343
    add-int/lit16 v5, v0, 0x3e8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    const/4 v5, 0x3

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    const/4 v5, 0x1

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    const/4 v5, 0x4

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    const/4 v5, 0x5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 361
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "g:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 353
    :pswitch_0
    iget-object v5, p0, Labg;->o:Lbak;

    invoke-virtual {v5, v4}, Lbak;->a(Ljava/lang/Iterable;)V

    goto :goto_2

    .line 357
    :pswitch_1
    iget-object v5, p0, Labg;->p:Lbak;

    invoke-virtual {v5, v4}, Lbak;->a(Ljava/lang/Iterable;)V

    goto :goto_2

    .line 364
    :cond_2
    iget-object v0, p0, Labg;->o:Lbak;

    invoke-virtual {p0, v1, v0}, Labg;->a(ILandroid/database/Cursor;)V

    move-object v0, v2

    .line 365
    goto/16 :goto_0

    .line 351
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;I)V
    .locals 11

    .prologue
    move-object v7, p1

    .line 537
    check-cast v7, Lcax;

    .line 540
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcax;->setOnItemCheckedChangeListener(Lcaz;)V

    .line 541
    invoke-virtual {v7}, Lcax;->reset()V

    .line 546
    invoke-virtual {p0, p2, p4}, Labg;->a(II)I

    move-result v0

    .line 547
    packed-switch v0, :pswitch_data_0

    .line 725
    :goto_0
    invoke-virtual {v7, p0}, Lcax;->setOnItemCheckedChangeListener(Lcaz;)V

    .line 731
    return-void

    .line 549
    :pswitch_0
    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 550
    const/4 v0, 0x6

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 551
    const/4 v0, 0x5

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 553
    const/4 v3, 0x0

    .line 555
    invoke-direct {p0, v4}, Labg;->a(Ljava/lang/String;)Laea;

    move-result-object v0

    .line 558
    if-nez v0, :cond_16

    .line 564
    iget-object v1, p0, Labg;->n:Les;

    invoke-virtual {v1, v4}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_16

    .line 565
    new-instance v1, Laea;

    iget-object v0, p0, Labg;->n:Les;

    .line 566
    invoke-virtual {v0, v4}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laea;

    invoke-direct {v1, v0}, Laea;-><init>(Laea;)V

    move-object v8, v1

    .line 570
    :goto_1
    const/4 v0, 0x0

    .line 571
    iget v1, p0, Labg;->f:I

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_15

    .line 572
    if-eqz v8, :cond_15

    invoke-virtual {v8}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_15

    .line 573
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v1

    .line 574
    iget-object v5, p0, Labg;->e:Lyj;

    invoke-virtual {v1, v4, v5}, Lbzh;->a(Ljava/lang/String;Lyj;)Lbdn;

    move-result-object v1

    .line 575
    if-eqz v1, :cond_15

    iget-boolean v5, v1, Lbdn;->b:Z

    if-eqz v5, :cond_15

    iget-boolean v1, v1, Lbdn;->a:Z

    if-nez v1, :cond_15

    .line 576
    invoke-virtual {v8}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    .line 577
    invoke-static {v0}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 578
    invoke-static {v0}, Lbdk;->d(Ljava/lang/String;)Lbdk;

    move-result-object v1

    .line 579
    invoke-static {v1, v0, v10}, Lbcx;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;)Lbcx;

    move-result-object v1

    .line 580
    const/4 v0, 0x1

    move v9, v0

    .line 585
    :goto_3
    if-nez v1, :cond_0

    .line 586
    invoke-static {v4, v10}, Lbcx;->a(Ljava/lang/String;Ljava/lang/String;)Lbcx;

    move-result-object v1

    :cond_0
    move-object v0, p1

    .line 589
    check-cast v0, Lceb;

    .line 590
    invoke-direct {p0, v1}, Labg;->a(Lbcx;)Z

    move-result v3

    .line 591
    iget-object v4, p0, Labg;->e:Lyj;

    iget-boolean v5, p0, Labg;->s:Z

    if-nez p2, :cond_3

    const/4 v6, 0x1

    :goto_4
    invoke-virtual/range {v0 .. v6}, Lceb;->setInviteeId(Lbcx;Ljava/lang/String;ZLyj;ZZ)V

    .line 594
    invoke-virtual {v0, v10}, Lceb;->setContactName(Ljava/lang/String;)V

    .line 595
    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2}, Lceb;->setChecked(ZZ)V

    .line 598
    if-eqz v8, :cond_4

    .line 599
    invoke-virtual {v8}, Laea;->l()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v8}, Laea;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 600
    :cond_1
    invoke-virtual {v0, v8, v9}, Lceb;->setContactDetails(Laea;Z)V

    .line 610
    :goto_5
    invoke-virtual {v0}, Lceb;->updateContentDescription()V

    .line 611
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lceb;->setDismissable(Z)V

    goto/16 :goto_0

    .line 571
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 591
    :cond_3
    const/4 v6, 0x0

    goto :goto_4

    .line 602
    :cond_4
    iget-object v3, p0, Labg;->q:Lamd;

    iget-object v2, p0, Labg;->g:Lt;

    check-cast v2, Ladw;

    .line 605
    invoke-virtual {v1}, Lbcx;->d()Ljava/lang/String;

    move-result-object v1

    .line 603
    invoke-virtual {v3, v2, v1}, Lamd;->a(Ladw;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 602
    invoke-virtual {v0, v1, v2}, Lceb;->setDetails(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 615
    :pswitch_1
    check-cast p3, Lbrw;

    .line 616
    invoke-virtual {p3, p4}, Lbrw;->moveToPosition(I)Z

    .line 617
    invoke-virtual {p3}, Lbrw;->a()Lcvw;

    move-result-object v10

    .line 618
    invoke-virtual {p3}, Lbrw;->b()Lcwe;

    move-result-object v1

    .line 620
    if-eqz v10, :cond_7

    invoke-interface {v10}, Lcvw;->e()Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    .line 622
    :goto_6
    if-eqz v10, :cond_8

    invoke-interface {v10}, Lcvw;->h()Ljava/lang/String;

    move-result-object v2

    .line 624
    :goto_7
    if-eqz v10, :cond_9

    invoke-interface {v10}, Lcvw;->a()Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    .line 627
    :goto_8
    new-instance v0, Lbdk;

    const/4 v1, 0x0

    invoke-direct {v0, v9, v1}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    const/4 v1, 0x0

    invoke-static {v0, v1, v8}, Lbcx;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;)Lbcx;

    move-result-object v1

    .line 630
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 631
    const-string v0, "Babel"

    const-string v3, "G+ contact should have a name"

    invoke-static {v0, v3}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v0, p1

    .line 635
    check-cast v0, Lceb;

    .line 636
    invoke-direct {p0, v1}, Labg;->a(Lbcx;)Z

    move-result v3

    iget-object v4, p0, Labg;->e:Lyj;

    iget-boolean v5, p0, Labg;->s:Z

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lceb;->setInviteeId(Lbcx;Ljava/lang/String;ZLyj;ZZ)V

    .line 638
    invoke-virtual {v0, v8}, Lceb;->setContactName(Ljava/lang/String;)V

    .line 639
    invoke-direct {p0, v1}, Labg;->a(Lbcx;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lceb;->setChecked(ZZ)V

    .line 643
    invoke-direct {p0, v9}, Labg;->a(Ljava/lang/String;)Laea;

    move-result-object v2

    .line 644
    if-nez v2, :cond_6

    .line 645
    if-eqz v10, :cond_a

    .line 646
    invoke-static {v10}, Laea;->a(Lcvw;)Laea;

    move-result-object v2

    .line 651
    :cond_6
    :goto_9
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Laea;->l()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 652
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lceb;->setContactDetails(Laea;Z)V

    .line 662
    :goto_a
    invoke-virtual {v0}, Lceb;->updateContentDescription()V

    .line 664
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lceb;->setDismissable(Z)V

    goto/16 :goto_0

    .line 621
    :cond_7
    invoke-interface {v1}, Lcwe;->e()Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    goto :goto_6

    .line 623
    :cond_8
    invoke-interface {v1}, Lcwe;->h()Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    .line 625
    :cond_9
    invoke-interface {v1}, Lcwe;->a()Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    goto :goto_8

    .line 646
    :cond_a
    const/4 v2, 0x0

    goto :goto_9

    .line 654
    :cond_b
    iget-object v3, p0, Labg;->q:Lamd;

    iget-object v2, p0, Labg;->g:Lt;

    check-cast v2, Ladw;

    .line 657
    invoke-virtual {v1}, Lbcx;->d()Ljava/lang/String;

    move-result-object v1

    .line 655
    invoke-virtual {v3, v2, v1}, Lamd;->a(Ladw;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 654
    invoke-virtual {v0, v1, v2}, Lceb;->setDetails(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 668
    :pswitch_2
    check-cast p3, Lbrw;

    .line 669
    invoke-virtual {p3, p4}, Lbrw;->moveToPosition(I)Z

    .line 670
    invoke-virtual {p3}, Lbrw;->a()Lcvw;

    move-result-object v6

    .line 674
    const/4 v1, 0x0

    .line 675
    const/4 v3, 0x0

    .line 676
    invoke-interface {v6}, Lcvw;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 678
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 679
    invoke-static {v0}, Lbrz;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v3

    .line 680
    :cond_c
    if-nez v1, :cond_d

    .line 685
    invoke-interface {v6}, Lcvw;->g()Ljava/lang/String;

    move-result-object v1

    .line 686
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 688
    const-string v0, "Babel"

    const-string v1, "Found a local contact with empty contact id and qualified id"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 694
    :cond_d
    invoke-interface {v6}, Lcvw;->a()Ljava/lang/String;

    move-result-object v2

    .line 695
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 697
    invoke-static {v6}, Laea;->c(Lcvw;)Ljava/lang/String;

    move-result-object v2

    .line 700
    :cond_e
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 701
    const-string v0, "Babel"

    const-string v4, "Local phone contact should have a name or a fallback name"

    invoke-static {v0, v4}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    :cond_f
    const/4 v5, 0x0

    .line 705
    iget-object v0, p0, Labg;->k:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laea;

    if-nez v0, :cond_10

    move-object v4, v0

    .line 710
    :goto_b
    if-nez v4, :cond_13

    .line 711
    invoke-static {v6}, Laea;->a(Lcvw;)Laea;

    move-result-object v4

    .line 712
    invoke-virtual {v4}, Laea;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_14

    .line 714
    const-string v0, "Babel"

    const-string v1, "Contact with no contact details should have been filtered out."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 705
    :cond_10
    invoke-virtual {v0}, Laea;->n()Laee;

    move-result-object v4

    if-eqz v4, :cond_12

    invoke-virtual {v4}, Laee;->c()Lbdh;

    move-result-object v8

    if-eqz v8, :cond_11

    iget-object v9, v8, Lbdh;->b:Lbdk;

    if-eqz v9, :cond_11

    invoke-static {v8}, Lbcx;->a(Lbdh;)Lbcx;

    move-result-object v4

    invoke-direct {p0, v4}, Labg;->a(Lbcx;)Z

    move-result v4

    if-eqz v4, :cond_12

    move-object v4, v0

    goto :goto_b

    :cond_11
    instance-of v8, v4, Laeh;

    if-eqz v8, :cond_12

    check-cast v4, Laeh;

    iget-object v4, v4, Laeh;->a:Ljava/lang/String;

    invoke-static {v4}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v4

    invoke-direct {p0, v4}, Labg;->a(Lbcx;)Z

    move-result v4

    if-eqz v4, :cond_12

    move-object v4, v0

    goto :goto_b

    :cond_12
    const/4 v0, 0x0

    move-object v4, v0

    goto :goto_b

    .line 719
    :cond_13
    const/4 v5, 0x1

    :cond_14
    move-object v0, p1

    .line 722
    check-cast v0, Lcbn;

    .line 723
    iget-boolean v6, p0, Labg;->s:Z

    invoke-virtual/range {v0 .. v6}, Lcbn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Laea;ZZ)V

    goto/16 :goto_0

    :cond_15
    move v9, v0

    move-object v1, v3

    goto/16 :goto_3

    :cond_16
    move-object v8, v0

    goto/16 :goto_1

    .line 547
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcax;Z)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 767
    invoke-direct {p0}, Labg;->r()V

    .line 768
    instance-of v0, p1, Lcba;

    if-eqz v0, :cond_3

    .line 769
    check-cast p1, Lcba;

    .line 770
    invoke-virtual {p1}, Lcba;->a()Ljava/lang/String;

    move-result-object v0

    .line 771
    if-eqz p2, :cond_2

    .line 772
    invoke-static {}, Lxo;->newBuilder()Lxp;

    move-result-object v1

    .line 773
    invoke-virtual {v1, v0}, Lxp;->a(Ljava/lang/String;)Lxp;

    .line 774
    invoke-virtual {p1}, Lcba;->c()I

    move-result v2

    invoke-static {v2}, Lxq;->a(I)Lxq;

    move-result-object v2

    invoke-virtual {v1, v2}, Lxp;->a(Lxq;)Lxp;

    .line 775
    invoke-virtual {p1}, Lcba;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lxp;->b(Ljava/lang/String;)Lxp;

    .line 776
    invoke-virtual {p1}, Lcba;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Lxp;->a(I)Lxp;

    .line 777
    invoke-virtual {v1}, Lxp;->a()Lxo;

    move-result-object v1

    invoke-direct {p0}, Labg;->r()V

    iget-object v2, p0, Labg;->j:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 912
    :cond_0
    :goto_0
    iget-object v0, p0, Labg;->h:Labi;

    if-eqz v0, :cond_1

    .line 913
    iget-object v0, p0, Labg;->h:Labi;

    invoke-interface {v0}, Labi;->a()V

    .line 916
    :cond_1
    invoke-virtual {p0}, Labg;->notifyDataSetChanged()V

    .line 917
    :goto_1
    return-void

    .line 779
    :cond_2
    iget-object v1, p0, Labg;->j:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 781
    :cond_3
    instance-of v0, p1, Lceb;

    if-eqz v0, :cond_a

    .line 785
    check-cast p1, Lceb;

    .line 786
    invoke-virtual {p1}, Lceb;->getInviteeId()Lbcx;

    move-result-object v2

    .line 788
    if-eqz p2, :cond_9

    .line 789
    invoke-virtual {p1}, Lceb;->getContactName()Ljava/lang/String;

    move-result-object v5

    .line 790
    invoke-virtual {p1}, Lceb;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v6

    .line 793
    invoke-virtual {p1}, Lceb;->getContactDetails()Laea;

    move-result-object v3

    .line 798
    iget v0, p0, Labg;->f:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 799
    if-eqz v3, :cond_1b

    .line 802
    invoke-virtual {v3}, Laea;->n()Laee;

    move-result-object v0

    .line 803
    if-eqz v0, :cond_1a

    instance-of v4, v0, Laeh;

    if-eqz v4, :cond_1a

    .line 804
    check-cast v0, Laeh;

    move-object v4, v0

    .line 808
    :goto_2
    if-nez v4, :cond_4

    .line 809
    invoke-virtual {v3}, Laea;->h()Laeh;

    move-result-object v4

    .line 813
    :cond_4
    :goto_3
    if-nez v4, :cond_5

    .line 814
    const-string v0, "Babel"

    const-string v7, "Phone should be selected in SMS mode"

    invoke-static {v0, v7}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    :cond_5
    iget-object v0, v4, Laeh;->a:Ljava/lang/String;

    invoke-static {v0}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v0

    .line 821
    :goto_4
    if-eqz v0, :cond_8

    :goto_5
    invoke-static {v0, v5, v6}, Lxs;->a(Lbcx;Ljava/lang/String;Ljava/lang/String;)Lxs;

    move-result-object v0

    .line 824
    if-eqz v4, :cond_6

    move-object v1, v3

    :cond_6
    invoke-direct {p0}, Labg;->r()V

    if-eqz v1, :cond_7

    iget-object v3, p0, Labg;->l:Ljava/util/Map;

    iget-object v4, v2, Lbcx;->a:Ljava/lang/String;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    iget-object v1, p0, Labg;->i:Ljava/util/Map;

    invoke-virtual {v2}, Lbcx;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_8
    move-object v0, v2

    .line 821
    goto :goto_5

    .line 826
    :cond_9
    iget-object v0, p0, Labg;->l:Ljava/util/Map;

    iget-object v1, v2, Lbcx;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Labg;->i:Ljava/util/Map;

    invoke-virtual {v2}, Lbcx;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 828
    :cond_a
    instance-of v0, p1, Lcbn;

    if-eqz v0, :cond_0

    .line 833
    check-cast p1, Lcbn;

    .line 834
    invoke-virtual {p1}, Lcbn;->b()Ljava/lang/String;

    move-result-object v6

    .line 835
    invoke-virtual {p1}, Lcbn;->a()Laea;

    move-result-object v7

    .line 836
    invoke-virtual {v7}, Laea;->n()Laee;

    move-result-object v0

    .line 840
    if-eqz v0, :cond_13

    .line 841
    iget v2, p0, Labg;->f:I

    invoke-static {v2}, Lf;->d(I)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 843
    invoke-virtual {v0}, Laee;->c()Lbdh;

    move-result-object v0

    .line 844
    if-nez v0, :cond_b

    .line 847
    const-string v0, "Babel"

    const-string v1, "Matching entity should not be null"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 851
    :cond_b
    iget-object v2, v0, Lbdh;->b:Lbdk;

    .line 852
    if-eqz v2, :cond_c

    invoke-virtual {v2}, Lbdk;->a()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 853
    :cond_c
    invoke-virtual {v0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_d

    .line 854
    const-string v0, "Babel"

    const-string v1, "Participant id for matching entity should not be empty"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    move-object v3, v0

    move-object v0, v1

    .line 884
    :goto_6
    if-nez v3, :cond_e

    if-eqz v0, :cond_f

    .line 885
    :cond_e
    if-eqz v3, :cond_14

    invoke-static {v3}, Lbcx;->a(Lbdh;)Lbcx;

    move-result-object v0

    move-object v2, v0

    .line 888
    :goto_7
    if-eqz p2, :cond_17

    .line 892
    if-eqz v3, :cond_16

    .line 893
    iget-object v0, v3, Lbdh;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, v3, Lbdh;->e:Ljava/lang/String;

    .line 895
    :goto_8
    iget-object v1, v3, Lbdh;->h:Ljava/lang/String;

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 901
    :goto_9
    invoke-static {v2, v1, v0}, Lxs;->a(Lbcx;Ljava/lang/String;Ljava/lang/String;)Lxs;

    move-result-object v0

    .line 902
    invoke-direct {p0}, Labg;->r()V

    iget-object v1, p0, Labg;->k:Ljava/util/Map;

    invoke-interface {v1, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Labg;->i:Ljava/util/Map;

    invoke-virtual {v2}, Lbcx;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 909
    :cond_f
    :goto_a
    invoke-virtual {p1}, Lcbn;->e()V

    goto/16 :goto_0

    .line 857
    :cond_10
    iget v2, p0, Labg;->f:I

    invoke-static {v2}, Lf;->c(I)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 859
    check-cast v0, Laeh;

    move-object v3, v1

    goto :goto_6

    .line 863
    :cond_11
    invoke-virtual {v0}, Laee;->c()Lbdh;

    move-result-object v2

    .line 864
    if-nez v2, :cond_19

    .line 865
    instance-of v3, v0, Laeh;

    if-eqz v3, :cond_12

    .line 866
    check-cast v0, Laeh;

    move-object v3, v2

    goto :goto_6

    .line 867
    :cond_12
    instance-of v3, v0, Laeg;

    if-eqz v3, :cond_19

    .line 868
    check-cast v0, Laeg;

    iget-object v2, v0, Laeg;->a:Ljava/lang/String;

    .line 869
    invoke-virtual {p1}, Lcbn;->c()Ljava/lang/String;

    move-result-object v3

    .line 870
    invoke-virtual {p1}, Lcbn;->d()Ljava/lang/String;

    move-result-object v4

    .line 871
    new-instance v0, Lbdk;

    invoke-direct {v0, v2, v1, v1}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    move-object v5, v1

    invoke-static/range {v0 .. v5}, Lbdh;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    .line 874
    goto :goto_6

    .line 877
    :cond_13
    if-eqz v7, :cond_18

    .line 880
    invoke-virtual {v7}, Laea;->h()Laeh;

    move-result-object v0

    move-object v3, v1

    goto :goto_6

    .line 885
    :cond_14
    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    .line 886
    invoke-static {v0}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v0

    move-object v2, v0

    goto :goto_7

    .line 893
    :cond_15
    iget-object v0, v3, Lbdh;->g:Ljava/lang/String;

    goto :goto_8

    .line 897
    :cond_16
    invoke-virtual {p1}, Lcbn;->c()Ljava/lang/String;

    move-result-object v1

    .line 898
    invoke-virtual {p1}, Lcbn;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 904
    :cond_17
    invoke-virtual {v7, v1}, Laea;->a(Laee;)V

    .line 905
    iget-object v0, p0, Labg;->k:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Labg;->i:Ljava/util/Map;

    invoke-virtual {v2}, Lbcx;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    :cond_18
    move-object v0, v1

    move-object v3, v1

    goto/16 :goto_6

    :cond_19
    move-object v0, v1

    move-object v3, v2

    goto/16 :goto_6

    :cond_1a
    move-object v4, v1

    goto/16 :goto_2

    :cond_1b
    move-object v4, v1

    goto/16 :goto_3

    :cond_1c
    move-object v0, v1

    move-object v4, v1

    goto/16 :goto_4
.end method

.method public a(Lcvx;Lcwf;)V
    .locals 2

    .prologue
    .line 378
    const-string v0, "Babel"

    const-string v1, "setLocalContacts"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    :goto_0
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Labg;->u:Lcvx;

    invoke-static {v0, p1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 386
    iput-object p1, p0, Labg;->u:Lcvx;

    .line 387
    const/4 v0, 0x1

    new-instance v1, Lbrw;

    invoke-direct {v1, p1, p2}, Lbrw;-><init>(Lcvx;Lcwf;)V

    invoke-virtual {p0, v0, v1}, Labg;->a(ILandroid/database/Cursor;)V

    goto :goto_0
.end method

.method public a(Lxm;)V
    .locals 4

    .prologue
    .line 982
    iget-object v0, p0, Labg;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 983
    iget-object v0, p0, Labg;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 984
    if-eqz p1, :cond_1

    .line 985
    invoke-virtual {p1}, Lxm;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxo;

    .line 986
    iget-object v2, p0, Labg;->j:Ljava/util/Map;

    invoke-virtual {v0}, Lxo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 989
    :cond_0
    invoke-virtual {p1}, Lxm;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxs;

    .line 990
    invoke-virtual {v0}, Lxs;->b()Lbcx;

    move-result-object v2

    .line 992
    iget-object v3, p0, Labg;->i:Ljava/util/Map;

    invoke-virtual {v2}, Lbcx;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 995
    :cond_1
    invoke-virtual {p0}, Labg;->notifyDataSetChanged()V

    .line 996
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x3

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Labg;->f:I

    return v0
.end method

.method public i()Lyj;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Labg;->e:Lyj;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Labg;->m:Landroid/database/Cursor;

    if-nez v0, :cond_0

    iget-object v0, p0, Labg;->e:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Labg;->o:Lbak;

    return-object v0
.end method

.method public l()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Labg;->p:Lbak;

    return-object v0
.end method

.method public o()Lxm;
    .locals 3

    .prologue
    .line 968
    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v1

    .line 969
    iget-object v0, p0, Labg;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxo;

    .line 970
    invoke-virtual {v1, v0}, Lxn;->a(Lxo;)Lxn;

    goto :goto_0

    .line 972
    :cond_0
    iget-object v0, p0, Labg;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxs;

    .line 973
    invoke-virtual {v1, v0}, Lxn;->a(Lxs;)Lxn;

    goto :goto_1

    .line 975
    :cond_1
    invoke-virtual {v1}, Lxn;->a()Lxm;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcvr;
    .locals 1

    .prologue
    .line 1056
    iget-object v0, p0, Labg;->r:Lcvr;

    return-object v0
.end method
