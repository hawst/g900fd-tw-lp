.class public Lbij;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldwq;)V
    .locals 3

    .prologue
    .line 1527
    iget-object v0, p1, Ldwq;->b:Ldvn;

    iget-object v1, p1, Ldwq;->c:Ljava/lang/Long;

    .line 1528
    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    .line 1527
    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1529
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 1530
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetTypingResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1532
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1536
    new-instance v0, Ldwq;

    invoke-direct {v0}, Ldwq;-><init>()V

    .line 1537
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldwq;

    .line 1538
    iget-object v1, v0, Ldwq;->b:Ldvn;

    invoke-static {v1}, Lbij;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1539
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldwq;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1541
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbij;

    invoke-direct {v1, v0}, Lbij;-><init>(Ldwq;)V

    move-object v0, v1

    goto :goto_0
.end method
