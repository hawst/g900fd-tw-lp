.class public Lbez;
.super Lbfp;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[B

.field public final c:J

.field public final h:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;[BJZ)V
    .locals 0

    .prologue
    .line 1291
    invoke-direct {p0}, Lbfp;-><init>()V

    .line 1292
    iput-object p1, p0, Lbez;->a:Ljava/lang/String;

    .line 1293
    iput-object p2, p0, Lbez;->b:[B

    .line 1294
    iput-wide p3, p0, Lbez;->c:J

    .line 1295
    iput-boolean p5, p0, Lbez;->h:Z

    .line 1296
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lbpp;Lbpk;)Lbfz;
    .locals 6

    .prologue
    const/16 v5, 0x89

    .line 1302
    :try_start_0
    iget-object v0, p0, Lbez;->b:[B

    iget-object v1, p0, Lbez;->a:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lbvv;->a(Landroid/content/Context;[BLjava/lang/String;)Lsa;

    move-result-object v0

    .line 1308
    invoke-static {p1, v0}, Lbvx;->a(Landroid/content/Context;Lsa;)Landroid/net/Uri;

    move-result-object v0

    .line 1309
    if-nez v0, :cond_0

    .line 1311
    const-string v0, "Babel_RequestWriter"

    const-string v1, "RetrieveMmsRequest: failed to persist message into telephony"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    new-instance v0, Lbph;

    const/16 v1, 0x86

    const-string v2, "Failed to persist retrieved mms message"

    invoke-direct {v0, v1, v2}, Lbph;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lbvu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbvp; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lrb; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1320
    :catch_0
    move-exception v0

    .line 1321
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RetrieveMmsRequest: failed to retrieve message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1322
    new-instance v1, Lbph;

    const/16 v2, 0x76

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 1317
    :cond_0
    const/4 v1, 0x1

    .line 1318
    :try_start_1
    invoke-static {v0}, Lbvx;->c(Landroid/net/Uri;)J

    move-result-wide v2

    .line 1317
    invoke-static {v1, v2, v3}, Lbwf;->a(IJ)V

    .line 1319
    new-instance v1, Lbhu;

    iget-wide v2, p0, Lbez;->c:J

    iget-boolean v4, p0, Lbez;->h:Z

    invoke-direct {v1, v0, v2, v3, v4}, Lbhu;-><init>(Landroid/net/Uri;JZ)V
    :try_end_1
    .catch Lbvu; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lbvp; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lrb; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    return-object v1

    .line 1324
    :catch_1
    move-exception v0

    .line 1325
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RetrieveMmsRequest: failed to retrieve message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1326
    new-instance v1, Lbph;

    iget v2, v0, Lbvp;->a:I

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 1327
    :catch_2
    move-exception v0

    .line 1328
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RetrieveMmsRequest: failed to retrieve message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1329
    new-instance v1, Lbph;

    invoke-direct {v1, v5, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 1331
    :catch_3
    move-exception v0

    .line 1332
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RetrieveMmsRequest: invalid message to retrieve "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1333
    new-instance v1, Lbph;

    invoke-direct {v1, v5, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1
.end method

.method public a(Lyj;Lbph;)V
    .locals 5

    .prologue
    .line 1345
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 1350
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lbez;->c:J

    iget-boolean v3, p0, Lbez;->h:Z

    .line 1351
    invoke-virtual {p2}, Lbph;->e()I

    move-result v4

    .line 1350
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;JZI)V

    .line 1352
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lbph;->e()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1353
    const/16 v0, 0x5f4

    invoke-static {p1, v0}, Lbez;->a(Lyj;I)V

    .line 1355
    :cond_0
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1340
    const-string v0, "mms_recv_queue"

    return-object v0
.end method
