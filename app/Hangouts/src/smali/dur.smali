.class public final Ldur;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldur;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24272
    const/4 v0, 0x0

    new-array v0, v0, [Ldur;

    sput-object v0, Ldur;->a:[Ldur;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24273
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 24294
    const/4 v0, 0x0

    .line 24295
    iget-object v1, p0, Ldur;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 24296
    const/4 v0, 0x1

    iget-object v1, p0, Ldur;->b:Ljava/lang/Boolean;

    .line 24297
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 24299
    :cond_0
    iget-object v1, p0, Ldur;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 24300
    const/4 v1, 0x2

    iget-object v2, p0, Ldur;->c:Ljava/lang/Boolean;

    .line 24301
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 24303
    :cond_1
    iget-object v1, p0, Ldur;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24304
    iput v0, p0, Ldur;->cachedSize:I

    .line 24305
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 24269
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldur;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldur;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldur;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldur;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldur;->c:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 24282
    iget-object v0, p0, Ldur;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 24283
    const/4 v0, 0x1

    iget-object v1, p0, Ldur;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 24285
    :cond_0
    iget-object v0, p0, Ldur;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 24286
    const/4 v0, 0x2

    iget-object v1, p0, Ldur;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 24288
    :cond_1
    iget-object v0, p0, Ldur;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 24290
    return-void
.end method
