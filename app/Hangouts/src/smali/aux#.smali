.class public final Laux;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private b:Lcws;

.field private c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lbys;->b:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Laux;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcws;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcws;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Laux;-><init>(Lcws;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcws;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcws;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Laux;-><init>(Lcws;)V

    .line 48
    return-void
.end method

.method private constructor <init>(Lcws;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Laux;->b:Lcws;

    .line 64
    iget-object v0, p0, Laux;->b:Lcws;

    invoke-virtual {v0}, Lcws;->a()V

    .line 65
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Laux;->c:J

    .line 66
    return-void
.end method

.method private b(Leab;J)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 136
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    .line 137
    iget-object v0, p0, Laux;->b:Lcws;

    const-string v1, "Shem"

    invoke-static {p1}, Leab;->toByteArray(Lepr;)[B

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcws;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 141
    :goto_0
    sget-boolean v0, Laux;->a:Z

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "Clearcut"

    invoke-virtual {p1}, Leab;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Laux;->b:Lcws;

    const-string v3, "Shem"

    invoke-static {p1}, Leab;->toByteArray(Lepr;)[B

    move-result-object v4

    new-array v5, v5, [Ljava/lang/String;

    move-wide v1, p2

    invoke-virtual/range {v0 .. v5}, Lcws;->a(JLjava/lang/String;[B[Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Laux;->c:J

    .line 86
    return-void
.end method

.method public a(Leab;J)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0, p1, p2, p3}, Laux;->b(Leab;J)V

    .line 166
    return-void
.end method

.method public a(Ljava/lang/Integer;)V
    .locals 6

    .prologue
    .line 184
    new-instance v1, Leab;

    invoke-direct {v1}, Leab;-><init>()V

    const/4 v0, 0x0

    invoke-static {}, Lapk;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_4

    iget-object v2, v1, Leab;->b:Ldzy;

    if-nez v2, :cond_1

    new-instance v2, Ldzy;

    invoke-direct {v2}, Ldzy;-><init>()V

    iput-object v2, v1, Leab;->b:Ldzy;

    :cond_1
    iget-object v2, v1, Leab;->b:Ldzy;

    iget-object v2, v2, Ldzy;->b:Ldzx;

    if-nez v2, :cond_2

    iget-object v2, v1, Leab;->b:Ldzy;

    new-instance v3, Ldzx;

    invoke-direct {v3}, Ldzx;-><init>()V

    iput-object v3, v2, Ldzy;->b:Ldzx;

    :cond_2
    iget-object v2, v1, Leab;->b:Ldzy;

    iget-object v2, v2, Ldzy;->b:Ldzx;

    invoke-virtual {v0}, Lapx;->j()Lyj;

    move-result-object v3

    invoke-static {v3}, Lbkb;->j(Lyj;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Ldzx;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lapx;->k()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Ldzx;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getHangoutRequest()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v0

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Ldzx;->d:Ljava/lang/String;

    new-instance v4, Leah;

    invoke-direct {v4}, Leah;-><init>()V

    iput-object v4, v2, Ldzx;->g:Leah;

    iget-object v4, v2, Ldzx;->g:Leah;

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Leah;->b:Ljava/lang/String;

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getJidNickname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Ldzx;->e:Ljava/lang/String;

    :cond_4
    iget-object v0, v1, Leab;->b:Ldzy;

    if-nez v0, :cond_5

    new-instance v0, Ldzy;

    invoke-direct {v0}, Ldzy;-><init>()V

    iput-object v0, v1, Leab;->b:Ldzy;

    :cond_5
    iget-object v0, v1, Leab;->b:Ldzy;

    iget-object v0, v0, Ldzy;->j:Ldzw;

    if-nez v0, :cond_6

    iget-object v0, v1, Leab;->b:Ldzy;

    new-instance v2, Ldzw;

    invoke-direct {v2}, Ldzw;-><init>()V

    iput-object v2, v0, Ldzy;->j:Ldzw;

    :cond_6
    iget-object v0, v1, Leab;->b:Ldzy;

    iget-object v0, v0, Ldzy;->j:Ldzw;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Laux;->c:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Ldzw;->b:Ljava/lang/Long;

    .line 185
    iget-object v0, v1, Leab;->b:Ldzy;

    if-nez v0, :cond_7

    .line 186
    new-instance v0, Ldzy;

    invoke-direct {v0}, Ldzy;-><init>()V

    iput-object v0, v1, Leab;->b:Ldzy;

    .line 188
    :cond_7
    iget-object v0, v1, Leab;->b:Ldzy;

    new-instance v2, Ldzz;

    invoke-direct {v2}, Ldzz;-><init>()V

    iput-object v2, v0, Ldzy;->i:Ldzz;

    .line 189
    iget-object v0, v1, Leab;->b:Ldzy;

    iget-object v0, v0, Ldzy;->i:Ldzz;

    iput-object p1, v0, Ldzz;->b:Ljava/lang/Integer;

    .line 190
    const-wide/16 v2, 0x0

    invoke-direct {p0, v1, v2, v3}, Laux;->b(Leab;J)V

    .line 191
    return-void
.end method
