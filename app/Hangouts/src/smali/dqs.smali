.class public final Ldqs;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqs;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21529
    const/4 v0, 0x0

    new-array v0, v0, [Ldqs;

    sput-object v0, Ldqs;->a:[Ldqs;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21530
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21533
    const/4 v0, 0x0

    iput-object v0, p0, Ldqs;->b:Ljava/lang/Integer;

    .line 21540
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldqs;->e:[Ljava/lang/String;

    .line 21530
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 21566
    iget-object v0, p0, Ldqs;->c:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 21567
    const/4 v0, 0x1

    iget-object v2, p0, Ldqs;->c:Ljava/lang/Long;

    .line 21568
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lepl;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21570
    :goto_0
    iget-object v2, p0, Ldqs;->d:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 21571
    const/4 v2, 0x2

    iget-object v3, p0, Ldqs;->d:Ljava/lang/Long;

    .line 21572
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 21574
    :cond_0
    iget-object v2, p0, Ldqs;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 21575
    const/4 v2, 0x3

    iget-object v3, p0, Ldqs;->b:Ljava/lang/Integer;

    .line 21576
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 21578
    :cond_1
    iget-object v2, p0, Ldqs;->e:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ldqs;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 21580
    iget-object v3, p0, Ldqs;->e:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 21582
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 21580
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 21584
    :cond_2
    add-int/2addr v0, v2

    .line 21585
    iget-object v1, p0, Ldqs;->e:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 21587
    :cond_3
    iget-object v1, p0, Ldqs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21588
    iput v0, p0, Ldqs;->cachedSize:I

    .line 21589
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 21526
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqs;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqs;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqs;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqs;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqs;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqs;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldqs;->e:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldqs;->e:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldqs;->e:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldqs;->e:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Ldqs;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Ldqs;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 21545
    iget-object v0, p0, Ldqs;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 21546
    const/4 v0, 0x1

    iget-object v1, p0, Ldqs;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 21548
    :cond_0
    iget-object v0, p0, Ldqs;->d:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 21549
    const/4 v0, 0x2

    iget-object v1, p0, Ldqs;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 21551
    :cond_1
    iget-object v0, p0, Ldqs;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 21552
    const/4 v0, 0x3

    iget-object v1, p0, Ldqs;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 21554
    :cond_2
    iget-object v0, p0, Ldqs;->e:[Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 21555
    iget-object v1, p0, Ldqs;->e:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 21556
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 21555
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21559
    :cond_3
    iget-object v0, p0, Ldqs;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 21561
    return-void
.end method
