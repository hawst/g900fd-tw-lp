.class public Lbhi;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(Ldcy;)V
    .locals 1

    .prologue
    .line 4365
    iget-object v0, p1, Ldcy;->b:Ldcw;

    invoke-direct {p0, v0}, Lbfz;-><init>(Ldcw;)V

    .line 4366
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4370
    new-instance v0, Ldcy;

    invoke-direct {v0}, Ldcy;-><init>()V

    .line 4371
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldcy;

    .line 4372
    iget-object v1, v0, Ldcy;->b:Ldcw;

    invoke-static {v1}, Lbhi;->a(Ldcw;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4373
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldcy;->b:Ldcw;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldcw;)V

    move-object v0, v1

    .line 4375
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhi;

    invoke-direct {v1, v0}, Lbhi;-><init>(Ldcy;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4382
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 4384
    invoke-virtual {p0}, Lbhi;->b()Lbea;

    move-result-object v0

    check-cast v0, Lbde;

    .line 4386
    iget-object v1, v0, Lbde;->a:Ljava/lang/String;

    iget-object v2, v0, Lbde;->b:Ljava/lang/String;

    iget-boolean v2, v0, Lbde;->c:Z

    invoke-virtual {p1, v1, v3, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 4388
    iget-boolean v1, v0, Lbde;->c:Z

    if-eqz v1, :cond_0

    .line 4389
    iget-object v1, v0, Lbde;->a:Ljava/lang/String;

    iget-object v0, v0, Lbde;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v3, v0, v3}, Lyt;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4397
    :goto_0
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Z)I

    .line 4399
    return-void

    .line 4392
    :cond_0
    iget-object v0, v0, Lbde;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lyt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
