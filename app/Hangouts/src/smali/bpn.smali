.class public final Lbpn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field public final b:Ljava/lang/Thread;

.field c:Z

.field final d:Ljava/lang/Object;

.field e:Lbpp;

.field f:Ljava/lang/String;

.field public g:J

.field final synthetic h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

.field private i:Z

.field private final j:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lbpp;",
            ">;"
        }
    .end annotation
.end field

.field private k:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1132
    iput-object p1, p0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 958
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    .line 960
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbpn;->d:Ljava/lang/Object;

    .line 970
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lbpn;->g:J

    .line 1133
    iput-object p2, p0, Lbpn;->a:Ljava/lang/String;

    .line 1134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbpn;->c:Z

    .line 1135
    new-instance v0, Lbpo;

    invoke-direct {v0, p0, p1}, Lbpo;-><init>(Lbpn;Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)V

    iput-object v0, p0, Lbpn;->b:Ljava/lang/Thread;

    .line 1145
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1146
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "### starting network thread for queue \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    :cond_0
    iget-object v0, p0, Lbpn;->b:Ljava/lang/Thread;

    iget-object v1, p0, Lbpn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1149
    iget-object v0, p0, Lbpn;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1150
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 979
    iget-object v3, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v3

    .line 980
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 981
    cmp-long v2, p1, v0

    if-nez v2, :cond_1

    const-string v2, "FOREVER"

    .line 984
    :goto_0
    const-string v4, "Babel_RequestWriter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "pausing queue "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lbpn;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]; duration "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 988
    :cond_0
    :try_start_1
    iget-boolean v2, p0, Lbpn;->c:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_2

    .line 989
    :try_start_2
    monitor-exit v3

    .line 1002
    :goto_1
    return-void

    .line 983
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 991
    :cond_2
    cmp-long v2, p1, v0

    if-nez v2, :cond_4

    .line 992
    :goto_2
    :try_start_3
    iput-wide v0, p0, Lbpn;->k:J

    .line 995
    iget-object v0, p0, Lbpn;->d:Ljava/lang/Object;

    invoke-virtual {v0, p1, p2}, Ljava/lang/Object;->wait(J)V

    .line 996
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbpn;->i:Z

    .line 997
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 998
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/pause "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbpn;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1002
    :cond_3
    :goto_3
    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 992
    :cond_4
    :try_start_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-wide v0

    add-long/2addr v0, p1

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3
.end method


# virtual methods
.method a()V
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 1031
    iget-object v6, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v6

    .line 1032
    :try_start_0
    iget-boolean v0, p0, Lbpn;->c:Z

    if-eqz v0, :cond_0

    .line 1034
    monitor-exit v6

    .line 1075
    :goto_0
    return-void

    .line 1038
    :cond_0
    iget-wide v2, p0, Lbpn;->g:J

    .line 1040
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 1042
    :goto_1
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1043
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpp;

    .line 1045
    invoke-virtual {v0}, Lbpp;->e()J

    move-result-wide v4

    .line 1046
    cmp-long v9, v4, v10

    if-gtz v9, :cond_2

    .line 1049
    iget-object v4, p0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    sget-object v4, Lbph;->a:Lbph;

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbpp;Lbph;)V

    .line 1052
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1053
    const-string v4, "Babel_RequestWriter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "pausing queue: remove expired request:"

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    :cond_1
    invoke-virtual {p0, v0}, Lbpn;->a(Lbpp;)V

    .line 1056
    add-int/lit8 v0, v1, -0x1

    move-wide v1, v2

    .line 1042
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move-wide v12, v1

    move-wide v2, v12

    move v1, v0

    goto :goto_1

    .line 1057
    :cond_2
    cmp-long v0, v4, v2

    if-gez v0, :cond_5

    move v0, v1

    move-wide v1, v4

    .line 1058
    goto :goto_2

    .line 1064
    :cond_3
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1065
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpp;

    .line 1066
    invoke-virtual {v0}, Lbpp;->f()J

    move-result-wide v0

    .line 1067
    cmp-long v4, v0, v10

    if-lez v4, :cond_4

    .line 1068
    sub-long/2addr v0, v7

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1073
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbpn;->i:Z

    .line 1074
    invoke-direct {p0, v2, v3}, Lbpn;->a(J)V

    .line 1075
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_5
    move v0, v1

    move-wide v12, v2

    move-wide v1, v12

    goto :goto_2
.end method

.method a(Lbpp;)V
    .locals 4

    .prologue
    .line 1007
    iget-object v1, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1008
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1009
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Removing request:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " queue_size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lbpn;->j:Ljava/util/LinkedList;

    .line 1010
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1009
    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    :cond_0
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1013
    iget-object v0, p0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Lbpk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbpk;->b(Lbpp;)V

    .line 1015
    iget-object v0, p0, Lbpn;->e:Lbpp;

    if-ne p1, v0, :cond_1

    .line 1016
    const/4 v0, 0x0

    iput-object v0, p0, Lbpn;->e:Lbpp;

    .line 1017
    const/4 v0, 0x0

    iput-object v0, p0, Lbpn;->f:Ljava/lang/String;

    .line 1019
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Lbpp;Z)V
    .locals 8

    .prologue
    .line 1446
    iget-object v1, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1447
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1448
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "queueRequest: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; length is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1450
    :cond_0
    iget-boolean v0, p0, Lbpn;->c:Z

    if-eqz v0, :cond_1

    .line 1451
    const-string v0, "Babel_RequestWriter"

    const-string v2, "can\'t call queueRequest after queue is finished"

    invoke-static {v0, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452
    monitor-exit v1

    .line 1506
    :goto_0
    return-void

    .line 1454
    :cond_1
    invoke-virtual {p1}, Lbpp;->a()Lbea;

    move-result-object v0

    .line 1455
    if-nez v0, :cond_2

    .line 1457
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1506
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1460
    :cond_2
    :try_start_1
    invoke-virtual {p0, p1}, Lbpn;->b(Lbpp;)Z

    move-result v2

    .line 1461
    if-nez v2, :cond_3

    invoke-virtual {p1}, Lbpp;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1463
    :cond_3
    if-nez v2, :cond_4

    .line 1464
    iget-object v0, p0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    sget-object v0, Lbph;->a:Lbph;

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbpp;Lbph;)V

    .line 1467
    :cond_4
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1468
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Removing expired request while queueing: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    :cond_5
    invoke-virtual {p0, p1}, Lbpn;->a(Lbpp;)V

    .line 1474
    monitor-exit v1

    goto :goto_0

    .line 1477
    :cond_6
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v2

    if-eqz v2, :cond_7

    instance-of v2, v0, Lbfb;

    if-eqz v2, :cond_7

    .line 1478
    check-cast v0, Lbfb;

    .line 1479
    invoke-virtual {v0}, Lbfb;->h()Ljava/lang/String;

    move-result-object v0

    .line 1480
    if-eqz v0, :cond_7

    .line 1481
    const-string v2, "Babel_Stress"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Queueing stress message request in RequestWriter"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1486
    :cond_7
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 1488
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_8

    iget-boolean v0, p0, Lbpn;->i:Z

    if-nez v0, :cond_8

    .line 1489
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbpn;->a(Z)V

    .line 1492
    :cond_8
    iget-boolean v0, p0, Lbpn;->i:Z

    if-eqz v0, :cond_a

    .line 1493
    if-nez p2, :cond_9

    .line 1494
    invoke-virtual {p1}, Lbpp;->e()J

    move-result-wide v2

    .line 1500
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 1501
    add-long/2addr v2, v4

    iget-wide v6, p0, Lbpn;->k:J

    cmp-long v0, v2, v6

    if-lez v0, :cond_9

    iget-wide v2, p0, Lbpn;->k:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_a

    .line 1502
    :cond_9
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbpn;->a(Z)V

    .line 1506
    :cond_a
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1589
    iget-object v2, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 1590
    :try_start_0
    iget-boolean v0, p0, Lbpn;->c:Z

    if-eqz v0, :cond_0

    .line 1591
    const-string v0, "Babel_RequestWriter"

    const-string v1, "Should not cancel a request why request writer is finished"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 1594
    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1595
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpp;

    .line 1596
    invoke-virtual {v0, p1, p2}, Lbpp;->a(Ljava/lang/Class;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1597
    if-nez v1, :cond_1

    iget-object v3, p0, Lbpn;->e:Lbpp;

    if-eqz v3, :cond_1

    .line 1599
    iput-object p2, p0, Lbpn;->f:Ljava/lang/String;

    move v0, v1

    .line 1594
    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 1601
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1602
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cancel request: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    :cond_2
    iget-object v3, p0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    sget-object v3, Lbph;->b:Lbph;

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbpp;Lbph;)V

    .line 1606
    invoke-virtual {p0, v0}, Lbpn;->a(Lbpp;)V

    .line 1608
    add-int/lit8 v0, v1, -0x1

    .line 1610
    invoke-static {p2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1614
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 1092
    iget-object v1, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1093
    if-eqz p1, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lbpn;->i:Z

    if-nez v0, :cond_0

    .line 1094
    monitor-exit v1

    .line 1101
    :goto_0
    return-void

    .line 1096
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1097
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "resuming queue "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbpn;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1099
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbpn;->i:Z

    .line 1100
    iget-object v0, p0, Lbpn;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1101
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b()J
    .locals 8

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 1108
    iget-object v5, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v5

    .line 1109
    :try_start_0
    iget-object v2, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 1110
    monitor-exit v5

    .line 1124
    :goto_0
    return-wide v0

    .line 1116
    :cond_0
    iget-object v2, p0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v0, p0, Lbpn;->g:J

    .line 1117
    :cond_1
    const/4 v2, 0x0

    move v4, v2

    move-wide v2, v0

    :goto_1
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 1118
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpp;

    .line 1119
    invoke-virtual {v0}, Lbpp;->e()J

    move-result-wide v0

    .line 1120
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-ltz v6, :cond_3

    cmp-long v6, v0, v2

    if-gez v6, :cond_3

    .line 1117
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_1

    .line 1124
    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v0, v2

    goto :goto_0

    .line 1125
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_3
    move-wide v0, v2

    goto :goto_2
.end method

.method b(Lbpp;)Z
    .locals 6

    .prologue
    .line 1510
    iget-object v2, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 1511
    :try_start_0
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1512
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpp;

    .line 1513
    if-eq p1, v0, :cond_1

    .line 1515
    iget-object v3, p1, Lbpp;->c:Ljava/lang/String;

    iget-object v4, v0, Lbpp;->c:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1519
    invoke-virtual {p1}, Lbpp;->a()Lbea;

    move-result-object v3

    .line 1522
    invoke-virtual {v0}, Lbpp;->a()Lbea;

    move-result-object v0

    .line 1523
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_1

    .line 1525
    invoke-virtual {v0, v3}, Lbea;->a(Lbea;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1528
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1529
    const-string v1, "Babel_RequestWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "not sending replaced request"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    :cond_0
    const/4 v0, 0x1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1534
    :goto_1
    return v0

    .line 1511
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1534
    :cond_2
    const/4 v0, 0x0

    monitor-exit v2

    goto :goto_1

    .line 1535
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method d()Lbpp;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1423
    iget-object v1, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1424
    :try_start_0
    iget-boolean v2, p0, Lbpn;->c:Z

    if-nez v2, :cond_1

    .line 1425
    iget-object v2, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 1426
    const-wide v2, 0x7fffffffffffffffL

    invoke-direct {p0, v2, v3}, Lbpn;->a(J)V

    .line 1428
    :cond_0
    iget-object v2, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 1429
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpp;

    iput-object v0, p0, Lbpn;->e:Lbpp;

    .line 1430
    const/4 v0, 0x0

    iput-object v0, p0, Lbpn;->f:Ljava/lang/String;

    .line 1431
    iget-object v0, p0, Lbpn;->e:Lbpp;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1434
    :goto_0
    return-object v0

    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 1435
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public e()V
    .locals 10

    .prologue
    .line 1542
    iget-object v1, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1543
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1544
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "### finish "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbpn;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbpn;->c:Z

    .line 1547
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbpn;->a(Z)V

    .line 1548
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1553
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1555
    iget-object v2, p0, Lbpn;->b:Ljava/lang/Thread;

    const-wide/16 v3, 0x2710

    invoke-virtual {v2, v3, v4}, Ljava/lang/Thread;->join(J)V

    .line 1556
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1557
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lbpn;->b:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1558
    :cond_1
    const-string v4, "Babel_RequestWriter"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "NetworkQueue.finish [%s] took %d ms; isAlive returning %b"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lbpn;->a:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    sub-long v0, v2, v0

    .line 1560
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v0, 0x2

    iget-object v1, p0, Lbpn;->b:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v7, v0

    .line 1558
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1564
    :cond_2
    :goto_0
    return-void

    .line 1548
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1564
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1568
    iget-object v1, p0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1569
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1570
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "NetworkQueue("

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbpn;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ") pending count == "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1571
    iget-object v0, p0, Lbpn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpp;

    .line 1572
    invoke-virtual {v0}, Lbpp;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1573
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1576
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1575
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method
