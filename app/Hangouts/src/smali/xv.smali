.class public abstract Lxv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcym;


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lxv;->a:Z

    .line 55
    iput-object p1, p0, Lxv;->b:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lxv;->c:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lxv;->d:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcyj;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Lcyj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 128
    const-class v0, Lxv;

    if-ne p2, v0, :cond_1

    .line 129
    const-class v0, Lxv;

    invoke-virtual {p3, v0, p0}, Lcyj;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0}, Lxv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v0, p0, Lxv;->a:Z

    :goto_1
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0, p2, p3}, Lxv;->a(Ljava/lang/Class;Lcyj;)V

    goto :goto_0

    .line 130
    :cond_2
    invoke-virtual {p0}, Lxv;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lxv;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lxv;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lxv;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1
.end method

.method public abstract a(Ljava/lang/Class;Lcyj;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcyj;",
            ")V"
        }
    .end annotation
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lxv;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "MODULE_ENABLED."

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lxv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxv;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lxv;->c:Ljava/lang/String;

    goto :goto_0
.end method
