.class public final Ldlx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldlx;


# instance fields
.field public b:Ldlt;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Ldlf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4788
    const/4 v0, 0x0

    new-array v0, v0, [Ldlx;

    sput-object v0, Ldlx;->a:[Ldlx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4789
    invoke-direct {p0}, Lepn;-><init>()V

    .line 4792
    const/4 v0, 0x0

    iput-object v0, p0, Ldlx;->b:Ldlt;

    .line 4799
    sget-object v0, Ldlf;->a:[Ldlf;

    iput-object v0, p0, Ldlx;->e:[Ldlf;

    .line 4789
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4827
    iget-object v0, p0, Ldlx;->b:Ldlt;

    if-eqz v0, :cond_4

    .line 4828
    const/4 v0, 0x1

    iget-object v2, p0, Ldlx;->b:Ldlt;

    .line 4829
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4831
    :goto_0
    iget-object v2, p0, Ldlx;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 4832
    const/4 v2, 0x2

    iget-object v3, p0, Ldlx;->c:Ljava/lang/String;

    .line 4833
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4835
    :cond_0
    iget-object v2, p0, Ldlx;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 4836
    const/4 v2, 0x3

    iget-object v3, p0, Ldlx;->d:Ljava/lang/String;

    .line 4837
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4839
    :cond_1
    iget-object v2, p0, Ldlx;->e:[Ldlf;

    if-eqz v2, :cond_3

    .line 4840
    iget-object v2, p0, Ldlx;->e:[Ldlf;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 4841
    if-eqz v4, :cond_2

    .line 4842
    const/4 v5, 0x4

    .line 4843
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4840
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4847
    :cond_3
    iget-object v1, p0, Ldlx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4848
    iput v0, p0, Ldlx;->cachedSize:I

    .line 4849
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4785
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldlx;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldlx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldlx;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldlx;->b:Ldlt;

    if-nez v0, :cond_2

    new-instance v0, Ldlt;

    invoke-direct {v0}, Ldlt;-><init>()V

    iput-object v0, p0, Ldlx;->b:Ldlt;

    :cond_2
    iget-object v0, p0, Ldlx;->b:Ldlt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldlx;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldlx;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldlx;->e:[Ldlf;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldlf;

    iget-object v3, p0, Ldlx;->e:[Ldlf;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldlx;->e:[Ldlf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldlx;->e:[Ldlf;

    :goto_2
    iget-object v2, p0, Ldlx;->e:[Ldlf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldlx;->e:[Ldlf;

    new-instance v3, Ldlf;

    invoke-direct {v3}, Ldlf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldlx;->e:[Ldlf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldlx;->e:[Ldlf;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldlx;->e:[Ldlf;

    new-instance v3, Ldlf;

    invoke-direct {v3}, Ldlf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldlx;->e:[Ldlf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 4804
    iget-object v0, p0, Ldlx;->b:Ldlt;

    if-eqz v0, :cond_0

    .line 4805
    const/4 v0, 0x1

    iget-object v1, p0, Ldlx;->b:Ldlt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4807
    :cond_0
    iget-object v0, p0, Ldlx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4808
    const/4 v0, 0x2

    iget-object v1, p0, Ldlx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4810
    :cond_1
    iget-object v0, p0, Ldlx;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 4811
    const/4 v0, 0x3

    iget-object v1, p0, Ldlx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4813
    :cond_2
    iget-object v0, p0, Ldlx;->e:[Ldlf;

    if-eqz v0, :cond_4

    .line 4814
    iget-object v1, p0, Ldlx;->e:[Ldlf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 4815
    if-eqz v3, :cond_3

    .line 4816
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 4814
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4820
    :cond_4
    iget-object v0, p0, Ldlx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4822
    return-void
.end method
