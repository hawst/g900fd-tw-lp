.class public final Ldwg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwg;


# instance fields
.field public b:Ldvm;

.field public c:Ldqf;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7032
    const/4 v0, 0x0

    new-array v0, v0, [Ldwg;

    sput-object v0, Ldwg;->a:[Ldwg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7033
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7036
    iput-object v0, p0, Ldwg;->b:Ldvm;

    .line 7039
    iput-object v0, p0, Ldwg;->c:Ldqf;

    .line 7042
    iput-object v0, p0, Ldwg;->d:Ljava/lang/Integer;

    .line 7033
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 7072
    const/4 v0, 0x0

    .line 7073
    iget-object v1, p0, Ldwg;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 7074
    const/4 v0, 0x1

    iget-object v1, p0, Ldwg;->b:Ldvm;

    .line 7075
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7077
    :cond_0
    iget-object v1, p0, Ldwg;->c:Ldqf;

    if-eqz v1, :cond_1

    .line 7078
    const/4 v1, 0x2

    iget-object v2, p0, Ldwg;->c:Ldqf;

    .line 7079
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7081
    :cond_1
    iget-object v1, p0, Ldwg;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 7082
    const/4 v1, 0x3

    iget-object v2, p0, Ldwg;->d:Ljava/lang/Integer;

    .line 7083
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7085
    :cond_2
    iget-object v1, p0, Ldwg;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 7086
    const/4 v1, 0x4

    iget-object v2, p0, Ldwg;->e:Ljava/lang/Integer;

    .line 7087
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7089
    :cond_3
    iget-object v1, p0, Ldwg;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 7090
    const/4 v1, 0x6

    iget-object v2, p0, Ldwg;->f:Ljava/lang/Boolean;

    .line 7091
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7093
    :cond_4
    iget-object v1, p0, Ldwg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7094
    iput v0, p0, Ldwg;->cachedSize:I

    .line 7095
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 7029
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwg;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldwg;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldwg;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldwg;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldwg;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldwg;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwg;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwg;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwg;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldwg;->f:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 7051
    iget-object v0, p0, Ldwg;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 7052
    const/4 v0, 0x1

    iget-object v1, p0, Ldwg;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7054
    :cond_0
    iget-object v0, p0, Ldwg;->c:Ldqf;

    if-eqz v0, :cond_1

    .line 7055
    const/4 v0, 0x2

    iget-object v1, p0, Ldwg;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7057
    :cond_1
    iget-object v0, p0, Ldwg;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 7058
    const/4 v0, 0x3

    iget-object v1, p0, Ldwg;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7060
    :cond_2
    iget-object v0, p0, Ldwg;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 7061
    const/4 v0, 0x4

    iget-object v1, p0, Ldwg;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 7063
    :cond_3
    iget-object v0, p0, Ldwg;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 7064
    const/4 v0, 0x6

    iget-object v1, p0, Ldwg;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7066
    :cond_4
    iget-object v0, p0, Ldwg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7068
    return-void
.end method
