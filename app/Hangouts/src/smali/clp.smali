.class final Lclp;
.super Lcht;

# interfaces
.implements Lcuv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lchr",
        "<",
        "Lclb;",
        ">.cht<",
        "Lcgk",
        "<",
        "Lcuv;",
        ">;>;",
        "Lcuv;"
    }
.end annotation


# instance fields
.field final synthetic b:Lclj;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Lcvz;


# direct methods
.method public constructor <init>(Lclj;Lcgk;Lcom/google/android/gms/common/api/Status;Lcvz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcuv;",
            ">;",
            "Lcom/google/android/gms/common/api/Status;",
            "Lcvz;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lclp;->b:Lclj;

    invoke-direct {p0, p1, p2}, Lcht;-><init>(Lchr;Ljava/lang/Object;)V

    iput-object p3, p0, Lclp;->c:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lclp;->d:Lcvz;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    invoke-virtual {p0}, Lclp;->b()V

    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcgk;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcgk;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lclp;->d:Lcvz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lclp;->d:Lcvz;

    invoke-virtual {v0}, Lchj;->b()V

    :cond_0
    return-void
.end method

.method public c()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lclp;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public g()Lcvz;
    .locals 1

    iget-object v0, p0, Lclp;->d:Lcvz;

    return-object v0
.end method
