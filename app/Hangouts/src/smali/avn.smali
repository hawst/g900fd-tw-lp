.class public final Lavn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V
    .locals 0

    .prologue
    .line 837
    iput-object p1, p0, Lavn;->a:Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 840
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    iget-object v0, p0, Lavn;->a:Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->d(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V

    .line 845
    :cond_0
    :try_start_0
    iget-object v0, p0, Lavn;->a:Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->e(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)Lavo;

    move-result-object v0

    invoke-static {v0}, Lbkb;->a(Lbkc;)V

    .line 846
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 847
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 848
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 849
    if-eqz v0, :cond_1

    .line 850
    iget-object v1, p0, Lavn;->a:Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->a(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;Lyj;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    .line 862
    :goto_0
    return-void

    .line 854
    :catch_0
    move-exception v0

    const-string v0, "Babel"

    const-string v1, "Account creation process canceled"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_1
    :goto_1
    iget-object v0, p0, Lavn;->a:Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->d(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V

    goto :goto_0

    .line 856
    :catch_1
    move-exception v0

    const-string v0, "Babel"

    const-string v1, "Authenticator experienced an I/O problem"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 858
    :catch_2
    move-exception v0

    const-string v0, "Babel"

    const-string v1, "Authenticator not setup correctly"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
