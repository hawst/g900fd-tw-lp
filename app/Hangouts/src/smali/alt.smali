.class public final Lalt;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lalu;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lalu;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V
    .locals 2

    .prologue
    .line 495
    iput-object p1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 509
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lalt;->b:Ljava/util/List;

    .line 511
    new-instance v0, Lalu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lalu;-><init>(Lalt;B)V

    iput-object v0, p0, Lalt;->c:Lalu;

    .line 709
    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;B)V
    .locals 0

    .prologue
    .line 495
    invoke-direct {p0, p1}, Lalt;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V

    return-void
.end method

.method static synthetic a(Lalt;Lyj;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 495
    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v7, p0, Lalt;->b:Ljava/util/List;

    new-instance v0, Lalu;

    sget v2, Lh;->ge:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aE:I

    const/16 v4, 0x6c

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lalu;-><init>(Lalt;IIIB)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {}, Lbzd;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    new-instance v1, Lalu;

    sget v2, Lh;->lw:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->cn:I

    invoke-direct {v1, p0, v2, v3}, Lalu;-><init>(Lalt;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    iget-object v1, p0, Lalt;->c:Lalu;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lalt;->b:Ljava/util/List;

    new-instance v0, Lalu;

    sget v2, Lh;->gq:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->bj:I

    const/16 v4, 0x65

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lalu;-><init>(Lalt;IIIB)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lalt;->b:Ljava/util/List;

    new-instance v0, Lalu;

    sget v2, Lh;->gf:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aG:I

    const/16 v4, 0x66

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lalu;-><init>(Lalt;IIIB)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lalt;->b:Ljava/util/List;

    new-instance v0, Lalu;

    sget v2, Lh;->gg:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->cj:I

    const/16 v4, 0x67

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lalu;-><init>(Lalt;IIIB)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    iget-object v1, p0, Lalt;->c:Lalu;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lalt;->b:Ljava/util/List;

    new-instance v0, Lalu;

    sget v2, Lh;->ch:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->bZ:I

    const/16 v4, 0x68

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lalu;-><init>(Lalt;IIIB)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "babel_richstatus"

    invoke-static {v0, v6}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v6

    :goto_0
    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->kI:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v6, p0, Lalt;->b:Ljava/util/List;

    new-instance v0, Lalu;

    sget v2, Lh;->gt:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->bL:I

    const/16 v4, 0x6a

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lalu;-><init>(Lalt;IIIB)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_1
    iget-object v6, p0, Lalt;->b:Ljava/util/List;

    new-instance v0, Lalu;

    sget v2, Lh;->ks:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->cl:I

    const/16 v4, 0x69

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lalu;-><init>(Lalt;IIIB)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lalt;->b:Ljava/util/List;

    new-instance v0, Lalu;

    sget v2, Lh;->fx:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->bH:I

    const/16 v4, 0x6b

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lalu;-><init>(Lalt;IIIB)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lalt;->notifyDataSetChanged()V

    return-void

    :cond_4
    move v0, v5

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lalt;->b:Ljava/util/List;

    new-instance v2, Lalu;

    sget v3, Lh;->gi:I

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->bL:I

    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v6

    invoke-virtual {v0, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-virtual {v6, v0}, Lccc;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, p0, v3, v4, v0}, Lalu;-><init>(Lalt;IILjava/lang/Integer;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 571
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 597
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0}, Lcwo;->getCount()I

    move-result v0

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->d(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v1

    invoke-virtual {v1}, Lcwo;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lalt;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 602
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0}, Lcwo;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 603
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcwo;->a(I)Lcwc;

    move-result-object v0

    .line 607
    :goto_0
    return-object v0

    .line 604
    :cond_0
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 605
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->d(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v1

    invoke-virtual {v1}, Lcwo;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcwo;->a(I)Lcwc;

    move-result-object v0

    goto :goto_0

    .line 607
    :cond_1
    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 612
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0}, Lcwo;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 613
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcwo;->getItemId(I)J

    move-result-wide v0

    .line 617
    :goto_0
    return-wide v0

    .line 614
    :cond_0
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 615
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->d(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v1

    invoke-virtual {v1}, Lcwo;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcwo;->getItemId(I)J

    move-result-wide v0

    goto :goto_0

    .line 617
    :cond_1
    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalu;

    iget v0, v0, Lalu;->c:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 692
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 693
    const/4 v0, 0x0

    .line 695
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalu;

    iget v0, v0, Lalu;->e:I

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 627
    if-eqz p2, :cond_0

    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 628
    sget v0, Lg;->h:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 629
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 631
    :cond_0
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0}, Lcwo;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 632
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcwo;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 687
    :cond_1
    :goto_0
    return-object p2

    .line 634
    :cond_2
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v0

    if-ge p1, v0, :cond_3

    .line 635
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->d(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v1

    invoke-virtual {v1}, Lcwo;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1, p2, p3}, Lcwo;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 637
    :cond_3
    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalu;

    .line 638
    iget v1, v0, Lalu;->e:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 639
    if-nez p2, :cond_1

    .line 640
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->fY:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 646
    :cond_4
    iget v1, v0, Lalu;->c:I

    const/16 v2, 0x6d

    if-ne v1, v2, :cond_7

    .line 647
    if-nez p2, :cond_5

    .line 648
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->dY:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 651
    :cond_5
    sget v0, Lg;->E:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 652
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 653
    sget v0, Lg;->I:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 654
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 655
    sget v0, Lg;->gQ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 656
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 657
    sget v0, Lg;->k:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 658
    sget v1, Lh;->lw:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 659
    sget v0, Lg;->h:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 660
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 661
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662
    sget v0, Lg;->am:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 663
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v1

    .line 664
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v2

    .line 663
    invoke-virtual {v1, v2}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 665
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 667
    :cond_6
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 672
    :cond_7
    if-nez p2, :cond_8

    .line 673
    iget-object v1, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {v1}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lf;->fZ:I

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 676
    :cond_8
    sget v1, Lg;->hn:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 677
    iget v2, v0, Lalu;->a:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 678
    sget v1, Lg;->du:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 679
    iget v2, v0, Lalu;->b:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 680
    sget v1, Lg;->dv:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 681
    iget-object v2, v0, Lalu;->d:Ljava/lang/Integer;

    if-nez v2, :cond_9

    .line 682
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 684
    :cond_9
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 685
    iget-object v0, v0, Lalu;->d:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 701
    const/4 v0, 0x3

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 622
    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0}, Lcwo;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->d(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0}, Lcwo;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lalt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 576
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I

    move-result v0

    .line 577
    if-lt p1, v0, :cond_0

    iget-object v2, p0, Lalt;->b:Ljava/util/List;

    sub-int v0, p1, v0

    .line 578
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalu;

    iget v0, v0, Lalu;->e:I

    if-eq v0, v1, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 583
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 584
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcwo;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 585
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->d(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcwo;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 586
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 590
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 591
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcwo;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 592
    iget-object v0, p0, Lalt;->a:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->d(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcwo;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 593
    return-void
.end method
