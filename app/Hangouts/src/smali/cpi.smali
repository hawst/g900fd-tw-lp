.class public final Lcpi;
.super Lciu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lciu",
        "<",
        "Lcph;",
        ">;"
    }
.end annotation


# instance fields
.field protected d:Lcjg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcjg",
            "<",
            "Lcph;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lt;

.field private f:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Lt;)V
    .locals 0

    invoke-direct {p0}, Lciu;-><init>()V

    iput-object p1, p0, Lcpi;->e:Lt;

    return-void
.end method

.method public static synthetic a(Lcpi;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcpi;->f:Landroid/app/Activity;

    invoke-virtual {p0}, Lcpi;->g()V

    return-void
.end method


# virtual methods
.method protected a(Lcjg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcjg",
            "<",
            "Lcph;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcpi;->d:Lcjg;

    invoke-virtual {p0}, Lcpi;->g()V

    return-void
.end method

.method public g()V
    .locals 4

    iget-object v0, p0, Lcpi;->f:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcpi;->d:Lcjg;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcpi;->a()Lcit;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcpi;->f:Landroid/app/Activity;

    invoke-static {v0}, Lf;->d(Landroid/content/Context;)I

    iget-object v0, p0, Lcpi;->f:Landroid/app/Activity;

    invoke-static {v0}, Lcst;->a(Landroid/content/Context;)Lcqt;

    move-result-object v0

    iget-object v1, p0, Lcpi;->f:Landroid/app/Activity;

    invoke-static {v1}, Lcjf;->a(Ljava/lang/Object;)Lcjc;

    move-result-object v1

    invoke-interface {v0, v1}, Lcqt;->b(Lcjc;)Lcpv;

    move-result-object v0

    iget-object v1, p0, Lcpi;->d:Lcjg;

    new-instance v2, Lcph;

    iget-object v3, p0, Lcpi;->e:Lt;

    invoke-direct {v2, v3, v0}, Lcph;-><init>(Lt;Lcpv;)V

    invoke-interface {v1, v2}, Lcjg;->a(Lcit;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcfx; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
