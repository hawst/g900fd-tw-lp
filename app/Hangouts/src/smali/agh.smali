.class public final Lagh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahg;


# instance fields
.field a:Z

.field final synthetic b:Z

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;ZII)V
    .locals 1

    .prologue
    .line 6647
    iput-object p1, p0, Lagh;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iput-boolean p2, p0, Lagh;->b:Z

    iput p3, p0, Lagh;->c:I

    iput p4, p0, Lagh;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6648
    const/4 v0, 0x0

    iput-boolean v0, p0, Lagh;->a:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 6684
    iget-boolean v0, p0, Lagh;->a:Z

    if-nez v0, :cond_0

    .line 6685
    new-instance v0, Lbbw;

    iget-object v1, p0, Lagh;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 6686
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lagh;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v2

    iget-object v3, p0, Lagh;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lagh;->b:Z

    iget v5, p0, Lagh;->c:I

    iget v7, p0, Lagh;->d:I

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lbbw;-><init>(Landroid/app/Activity;Lyj;Ljava/lang/String;ZIZIZ)V

    .line 6689
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbbw;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 6691
    :cond_0
    return-void
.end method

.method public a(Lajk;)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x1

    .line 6652
    iget-object v0, p1, Lajk;->b:Lbdh;

    if-nez v0, :cond_1

    .line 6679
    :cond_0
    :goto_0
    return v6

    .line 6655
    :cond_1
    iget-object v0, p1, Lajk;->b:Lbdh;

    invoke-virtual {v0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6660
    iget-object v0, p1, Lajk;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 6664
    iget-object v0, p1, Lajk;->g:Lyv;

    iget v0, v0, Lyv;->c:I

    if-ne v0, v6, :cond_0

    .line 6670
    iget-object v0, p1, Lajk;->a:Ljava/lang/String;

    iget-object v1, p0, Lagh;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aA(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 6673
    new-instance v0, Lbbw;

    iget-object v1, p0, Lagh;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 6674
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lagh;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v2

    iget-object v3, p1, Lajk;->a:Ljava/lang/String;

    iget-boolean v4, p0, Lagh;->b:Z

    iget v5, p0, Lagh;->c:I

    iget v7, p0, Lagh;->d:I

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lbbw;-><init>(Landroid/app/Activity;Lyj;Ljava/lang/String;ZIZIZ)V

    .line 6677
    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbbw;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 6678
    iput-boolean v6, p0, Lagh;->a:Z

    move v6, v9

    .line 6679
    goto :goto_0
.end method
