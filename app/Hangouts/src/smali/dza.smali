.class public final Ldza;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldza;


# instance fields
.field public b:Ldvm;

.field public c:Ldyp;

.field public d:[Ldth;

.field public e:Ldzc;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1838
    const/4 v0, 0x0

    new-array v0, v0, [Ldza;

    sput-object v0, Ldza;->a:[Ldza;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1839
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1842
    iput-object v1, p0, Ldza;->b:Ldvm;

    .line 1845
    iput-object v1, p0, Ldza;->c:Ldyp;

    .line 1848
    sget-object v0, Ldth;->a:[Ldth;

    iput-object v0, p0, Ldza;->d:[Ldth;

    .line 1851
    iput-object v1, p0, Ldza;->e:Ldzc;

    .line 1856
    iput-object v1, p0, Ldza;->g:Ljava/lang/Integer;

    .line 1839
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1890
    iget-object v0, p0, Ldza;->b:Ldvm;

    if-eqz v0, :cond_6

    .line 1891
    const/4 v0, 0x1

    iget-object v2, p0, Ldza;->b:Ldvm;

    .line 1892
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1894
    :goto_0
    iget-object v2, p0, Ldza;->c:Ldyp;

    if-eqz v2, :cond_0

    .line 1895
    const/4 v2, 0x2

    iget-object v3, p0, Ldza;->c:Ldyp;

    .line 1896
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1898
    :cond_0
    iget-object v2, p0, Ldza;->d:[Ldth;

    if-eqz v2, :cond_2

    .line 1899
    iget-object v2, p0, Ldza;->d:[Ldth;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1900
    if-eqz v4, :cond_1

    .line 1901
    const/4 v5, 0x3

    .line 1902
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1899
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1906
    :cond_2
    iget-object v1, p0, Ldza;->e:Ldzc;

    if-eqz v1, :cond_3

    .line 1907
    const/4 v1, 0x4

    iget-object v2, p0, Ldza;->e:Ldzc;

    .line 1908
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1910
    :cond_3
    iget-object v1, p0, Ldza;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1911
    const/4 v1, 0x5

    iget-object v2, p0, Ldza;->f:Ljava/lang/Boolean;

    .line 1912
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1914
    :cond_4
    iget-object v1, p0, Ldza;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1915
    const/4 v1, 0x6

    iget-object v2, p0, Ldza;->g:Ljava/lang/Integer;

    .line 1916
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1918
    :cond_5
    iget-object v1, p0, Ldza;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1919
    iput v0, p0, Ldza;->cachedSize:I

    .line 1920
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1835
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldza;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldza;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldza;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldza;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldza;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldza;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldza;->c:Ldyp;

    if-nez v0, :cond_3

    new-instance v0, Ldyp;

    invoke-direct {v0}, Ldyp;-><init>()V

    iput-object v0, p0, Ldza;->c:Ldyp;

    :cond_3
    iget-object v0, p0, Ldza;->c:Ldyp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldza;->d:[Ldth;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldth;

    iget-object v3, p0, Ldza;->d:[Ldth;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldza;->d:[Ldth;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldza;->d:[Ldth;

    :goto_2
    iget-object v2, p0, Ldza;->d:[Ldth;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldza;->d:[Ldth;

    new-instance v3, Ldth;

    invoke-direct {v3}, Ldth;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldza;->d:[Ldth;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldza;->d:[Ldth;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldza;->d:[Ldth;

    new-instance v3, Ldth;

    invoke-direct {v3}, Ldth;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldza;->d:[Ldth;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Ldza;->e:Ldzc;

    if-nez v0, :cond_7

    new-instance v0, Ldzc;

    invoke-direct {v0}, Ldzc;-><init>()V

    iput-object v0, p0, Ldza;->e:Ldzc;

    :cond_7
    iget-object v0, p0, Ldza;->e:Ldzc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldza;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_8

    const/4 v2, 0x2

    if-eq v0, v2, :cond_8

    const/4 v2, 0x3

    if-ne v0, v2, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldza;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldza;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 1861
    iget-object v0, p0, Ldza;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 1862
    const/4 v0, 0x1

    iget-object v1, p0, Ldza;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1864
    :cond_0
    iget-object v0, p0, Ldza;->c:Ldyp;

    if-eqz v0, :cond_1

    .line 1865
    const/4 v0, 0x2

    iget-object v1, p0, Ldza;->c:Ldyp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1867
    :cond_1
    iget-object v0, p0, Ldza;->d:[Ldth;

    if-eqz v0, :cond_3

    .line 1868
    iget-object v1, p0, Ldza;->d:[Ldth;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1869
    if-eqz v3, :cond_2

    .line 1870
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1868
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1874
    :cond_3
    iget-object v0, p0, Ldza;->e:Ldzc;

    if-eqz v0, :cond_4

    .line 1875
    const/4 v0, 0x4

    iget-object v1, p0, Ldza;->e:Ldzc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1877
    :cond_4
    iget-object v0, p0, Ldza;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1878
    const/4 v0, 0x5

    iget-object v1, p0, Ldza;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1880
    :cond_5
    iget-object v0, p0, Ldza;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1881
    const/4 v0, 0x6

    iget-object v1, p0, Ldza;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1883
    :cond_6
    iget-object v0, p0, Ldza;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1885
    return-void
.end method
