.class public final Lcdx;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 313
    iput-object p1, p0, Lcdx;->a:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    .line 314
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 318
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 319
    iget-object v0, p0, Lcdx;->a:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->d(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)Z

    .line 320
    new-instance v1, Lcdu;

    iget-object v0, p0, Lcdx;->a:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget-object v2, p0, Lcdx;->a:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    .line 321
    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->e(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)Ljava/util/Queue;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-direct {v1, v0, v2}, Lcdu;-><init>(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;I)V

    .line 323
    :goto_0
    iget-object v0, p0, Lcdx;->a:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->e(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 324
    iget-object v2, p0, Lcdx;->a:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget-object v0, p0, Lcdx;->a:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    .line 325
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->e(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcea;

    .line 324
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;Lcea;Lcdu;)V

    goto :goto_0

    .line 328
    :cond_0
    return-void
.end method
