.class public final Lapb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lapu;


# instance fields
.field private final a:Lapk;

.field private final b:Lapc;

.field private final c:Landroid/content/Context;

.field private d:Lapv;

.field private e:Laqe;

.field private f:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lapb;->a:Lapk;

    .line 59
    new-instance v0, Lapc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lapc;-><init>(Lapb;B)V

    iput-object v0, p0, Lapb;->b:Lapc;

    .line 67
    iput-object p1, p0, Lapb;->c:Landroid/content/Context;

    .line 68
    return-void
.end method

.method static synthetic a(Lapb;)Lapv;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lapb;->d:Lapv;

    return-object v0
.end method

.method static synthetic a(Lapb;Laqe;)Laqe;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lapb;->e:Laqe;

    return-object p1
.end method

.method static synthetic b(Lapb;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 104
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lapb;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 105
    iget-object v1, p0, Lapb;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 106
    iget-object v2, p0, Lapb;->e:Laqe;

    invoke-virtual {v2}, Laqe;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 107
    sget v2, Lh;->hZ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 108
    sget v2, Lh;->dJ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 109
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 110
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 111
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 113
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    .line 114
    iget-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 115
    return-void
.end method

.method static synthetic c(Lapb;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic d(Lapb;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lapb;->b()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public a(Lapv;)V
    .locals 2

    .prologue
    .line 72
    iput-object p1, p0, Lapb;->d:Lapv;

    .line 73
    iget-object v0, p0, Lapb;->a:Lapk;

    iget-object v1, p0, Lapb;->b:Lapc;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 74
    iget-object v0, p0, Lapb;->d:Lapv;

    invoke-virtual {v0}, Lapv;->c()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lapb;->a:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    invoke-virtual {v0}, Lapx;->H()Laqe;

    move-result-object v0

    iput-object v0, p0, Lapb;->e:Laqe;

    .line 79
    iget-object v0, p0, Lapb;->e:Laqe;

    if-eqz v0, :cond_0

    .line 80
    invoke-direct {p0}, Lapb;->b()V

    goto :goto_0
.end method

.method public m_()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lapb;->a:Lapk;

    iget-object v1, p0, Lapb;->b:Lapc;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 87
    iget-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    .line 93
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lapb;->a:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 125
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 126
    iget-object v1, p0, Lapb;->e:Laqe;

    invoke-virtual {v1}, Laqe;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapx;->e(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 133
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 131
    const/16 v1, 0x3ec

    invoke-virtual {v0, v1}, Lapx;->c(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lapb;->f:Landroid/app/AlertDialog;

    if-ne p1, v0, :cond_0

    .line 139
    const/4 v0, -0x2

    invoke-virtual {p0, p1, v0}, Lapb;->onClick(Landroid/content/DialogInterface;I)V

    .line 143
    :cond_0
    return-void
.end method
