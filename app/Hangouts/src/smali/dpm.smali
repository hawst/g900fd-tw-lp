.class public final Ldpm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpm;


# instance fields
.field public b:[Ldwv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25289
    const/4 v0, 0x0

    new-array v0, v0, [Ldpm;

    sput-object v0, Ldpm;->a:[Ldpm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25290
    invoke-direct {p0}, Lepn;-><init>()V

    .line 25293
    sget-object v0, Ldwv;->a:[Ldwv;

    iput-object v0, p0, Ldpm;->b:[Ldwv;

    .line 25290
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 25312
    iget-object v1, p0, Ldpm;->b:[Ldwv;

    if-eqz v1, :cond_1

    .line 25313
    iget-object v2, p0, Ldpm;->b:[Ldwv;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 25314
    if-eqz v4, :cond_0

    .line 25315
    const/4 v5, 0x1

    .line 25316
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 25313
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 25320
    :cond_1
    iget-object v1, p0, Ldpm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25321
    iput v0, p0, Ldpm;->cachedSize:I

    .line 25322
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 25286
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldpm;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldpm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldpm;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpm;->b:[Ldwv;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldwv;

    iget-object v3, p0, Ldpm;->b:[Ldwv;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldpm;->b:[Ldwv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldpm;->b:[Ldwv;

    :goto_2
    iget-object v2, p0, Ldpm;->b:[Ldwv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldpm;->b:[Ldwv;

    new-instance v3, Ldwv;

    invoke-direct {v3}, Ldwv;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpm;->b:[Ldwv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldpm;->b:[Ldwv;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldpm;->b:[Ldwv;

    new-instance v3, Ldwv;

    invoke-direct {v3}, Ldwv;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpm;->b:[Ldwv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 25298
    iget-object v0, p0, Ldpm;->b:[Ldwv;

    if-eqz v0, :cond_1

    .line 25299
    iget-object v1, p0, Ldpm;->b:[Ldwv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 25300
    if-eqz v3, :cond_0

    .line 25301
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 25299
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25305
    :cond_1
    iget-object v0, p0, Ldpm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 25307
    return-void
.end method
