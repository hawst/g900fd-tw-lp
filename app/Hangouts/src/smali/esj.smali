.class public final Lesj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lesj;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Lesj;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Lesl;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lesj;

    sput-object v0, Lesj;->a:[Lesj;

    .line 13
    const v0, 0x279e4a4

    new-instance v1, Lesk;

    invoke-direct {v1}, Lesk;-><init>()V

    .line 14
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Lesj;->b:Lepo;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 19
    iput-object v0, p0, Lesj;->d:Ljava/lang/Integer;

    .line 22
    iput-object v0, p0, Lesj;->e:Lesl;

    .line 27
    iput-object v0, p0, Lesj;->g:Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Lesj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 55
    const/4 v0, 0x1

    iget-object v1, p0, Lesj;->d:Ljava/lang/Integer;

    .line 56
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 58
    :cond_0
    iget-object v1, p0, Lesj;->e:Lesl;

    if-eqz v1, :cond_1

    .line 59
    const/4 v1, 0x2

    iget-object v2, p0, Lesj;->e:Lesl;

    .line 60
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_1
    iget-object v1, p0, Lesj;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 63
    const/4 v1, 0x3

    iget-object v2, p0, Lesj;->f:Ljava/lang/Boolean;

    .line 64
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 66
    :cond_2
    iget-object v1, p0, Lesj;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 67
    const/4 v1, 0x5

    iget-object v2, p0, Lesj;->g:Ljava/lang/Integer;

    .line 68
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_3
    iget-object v1, p0, Lesj;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 71
    const/4 v1, 0x6

    iget-object v2, p0, Lesj;->c:Ljava/lang/String;

    .line 72
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_4
    iget-object v1, p0, Lesj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    iput v0, p0, Lesj;->cachedSize:I

    .line 76
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lesj;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lesj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lesj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x64

    if-eq v0, v1, :cond_2

    const/16 v1, 0x66

    if-eq v0, v1, :cond_2

    const/16 v1, 0x67

    if-eq v0, v1, :cond_2

    const/16 v1, 0x68

    if-eq v0, v1, :cond_2

    const/16 v1, 0x69

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x78

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x77

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x86

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x87

    if-eq v0, v1, :cond_2

    const/16 v1, 0x79

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x80

    if-eq v0, v1, :cond_2

    const/16 v1, 0x81

    if-eq v0, v1, :cond_2

    const/16 v1, 0x82

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83

    if-eq v0, v1, :cond_2

    const/16 v1, 0x89

    if-eq v0, v1, :cond_2

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x70

    if-eq v0, v1, :cond_2

    const/16 v1, 0x71

    if-eq v0, v1, :cond_2

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_2

    const/16 v1, 0x75

    if-eq v0, v1, :cond_2

    const/16 v1, 0x76

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x85

    if-eq v0, v1, :cond_2

    const/16 v1, 0x88

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x92

    if-eq v0, v1, :cond_2

    const/16 v1, 0x93

    if-eq v0, v1, :cond_2

    const/16 v1, 0x95

    if-eq v0, v1, :cond_2

    const/16 v1, 0x97

    if-eq v0, v1, :cond_2

    const/16 v1, 0x98

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x90

    if-eq v0, v1, :cond_2

    const/16 v1, 0x91

    if-eq v0, v1, :cond_2

    const/16 v1, 0x99

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x94

    if-eq v0, v1, :cond_2

    const/16 v1, 0x96

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesj;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesj;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Lesj;->e:Lesl;

    if-nez v0, :cond_4

    new-instance v0, Lesl;

    invoke-direct {v0}, Lesl;-><init>()V

    iput-object v0, p0, Lesj;->e:Lesl;

    :cond_4
    iget-object v0, p0, Lesj;->e:Lesl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lesj;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_5

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesj;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesj;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesj;->c:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lesj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, 0x1

    iget-object v1, p0, Lesj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 35
    :cond_0
    iget-object v0, p0, Lesj;->e:Lesl;

    if-eqz v0, :cond_1

    .line 36
    const/4 v0, 0x2

    iget-object v1, p0, Lesj;->e:Lesl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 38
    :cond_1
    iget-object v0, p0, Lesj;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 39
    const/4 v0, 0x3

    iget-object v1, p0, Lesj;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 41
    :cond_2
    iget-object v0, p0, Lesj;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 42
    const/4 v0, 0x5

    iget-object v1, p0, Lesj;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 44
    :cond_3
    iget-object v0, p0, Lesj;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 45
    const/4 v0, 0x6

    iget-object v1, p0, Lesj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 47
    :cond_4
    iget-object v0, p0, Lesj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 49
    return-void
.end method
