.class public final Ldjl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldjl;


# instance fields
.field public b:Ldkj;

.field public c:[Ldkj;

.field public d:Ldjh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602
    const/4 v0, 0x0

    new-array v0, v0, [Ldjl;

    sput-object v0, Ldjl;->a:[Ldjl;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 603
    invoke-direct {p0}, Lepn;-><init>()V

    .line 606
    iput-object v1, p0, Ldjl;->b:Ldkj;

    .line 609
    sget-object v0, Ldkj;->a:[Ldkj;

    iput-object v0, p0, Ldjl;->c:[Ldkj;

    .line 612
    iput-object v1, p0, Ldjl;->d:Ldjh;

    .line 603
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 637
    iget-object v0, p0, Ldjl;->b:Ldkj;

    if-eqz v0, :cond_3

    .line 638
    const/4 v0, 0x4

    iget-object v2, p0, Ldjl;->b:Ldkj;

    .line 639
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 641
    :goto_0
    iget-object v2, p0, Ldjl;->c:[Ldkj;

    if-eqz v2, :cond_1

    .line 642
    iget-object v2, p0, Ldjl;->c:[Ldkj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 643
    if-eqz v4, :cond_0

    .line 644
    const/4 v5, 0x5

    .line 645
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 642
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 649
    :cond_1
    iget-object v1, p0, Ldjl;->d:Ldjh;

    if-eqz v1, :cond_2

    .line 650
    const/4 v1, 0x6

    iget-object v2, p0, Ldjl;->d:Ldjh;

    .line 651
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 653
    :cond_2
    iget-object v1, p0, Ldjl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 654
    iput v0, p0, Ldjl;->cachedSize:I

    .line 655
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 599
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldjl;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldjl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldjl;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldjl;->b:Ldkj;

    if-nez v0, :cond_2

    new-instance v0, Ldkj;

    invoke-direct {v0}, Ldkj;-><init>()V

    iput-object v0, p0, Ldjl;->b:Ldkj;

    :cond_2
    iget-object v0, p0, Ldjl;->b:Ldkj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldjl;->c:[Ldkj;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldkj;

    iget-object v3, p0, Ldjl;->c:[Ldkj;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldjl;->c:[Ldkj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldjl;->c:[Ldkj;

    :goto_2
    iget-object v2, p0, Ldjl;->c:[Ldkj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldjl;->c:[Ldkj;

    new-instance v3, Ldkj;

    invoke-direct {v3}, Ldkj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldjl;->c:[Ldkj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldjl;->c:[Ldkj;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldjl;->c:[Ldkj;

    new-instance v3, Ldkj;

    invoke-direct {v3}, Ldkj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldjl;->c:[Ldkj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldjl;->d:Ldjh;

    if-nez v0, :cond_6

    new-instance v0, Ldjh;

    invoke-direct {v0}, Ldjh;-><init>()V

    iput-object v0, p0, Ldjl;->d:Ldjh;

    :cond_6
    iget-object v0, p0, Ldjl;->d:Ldjh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x2a -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 617
    iget-object v0, p0, Ldjl;->b:Ldkj;

    if-eqz v0, :cond_0

    .line 618
    const/4 v0, 0x4

    iget-object v1, p0, Ldjl;->b:Ldkj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 620
    :cond_0
    iget-object v0, p0, Ldjl;->c:[Ldkj;

    if-eqz v0, :cond_2

    .line 621
    iget-object v1, p0, Ldjl;->c:[Ldkj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 622
    if-eqz v3, :cond_1

    .line 623
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 621
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 627
    :cond_2
    iget-object v0, p0, Ldjl;->d:Ldjh;

    if-eqz v0, :cond_3

    .line 628
    const/4 v0, 0x6

    iget-object v1, p0, Ldjl;->d:Ldjh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 630
    :cond_3
    iget-object v0, p0, Ldjl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 632
    return-void
.end method
