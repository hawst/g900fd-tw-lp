.class public Lxu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lxu;->a:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lm;->b(Z)V

    .line 36
    iget v0, p0, Lxu;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lxu;->a:I

    .line 37
    iget v0, p0, Lxu;->a:I

    .line 38
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lxu;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lm;->b(Z)V

    .line 45
    iget v0, p0, Lxu;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lxu;->a:I

    .line 46
    iget v0, p0, Lxu;->a:I

    .line 47
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    iget v0, p0, Lxu;->c:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lm;->b(Z)V

    .line 81
    iget v0, p0, Lxu;->c:I

    iget v3, p0, Lxu;->b:I

    if-gt v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, Lm;->b(Z)V

    .line 82
    iget v0, p0, Lxu;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lxu;->c:I

    .line 83
    return-void

    :cond_0
    move v0, v2

    .line 80
    goto :goto_0

    :cond_1
    move v1, v2

    .line 81
    goto :goto_1
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 73
    iget v0, p0, Lxu;->c:I

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lm;->b(Z)V

    .line 74
    iget v0, p0, Lxu;->c:I

    iget v3, p0, Lxu;->b:I

    if-ge v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, Lm;->b(Z)V

    .line 75
    iget v0, p0, Lxu;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lxu;->c:I

    .line 76
    return-void

    :cond_0
    move v0, v2

    .line 73
    goto :goto_0

    :cond_1
    move v1, v2

    .line 74
    goto :goto_1
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 53
    iget v0, p0, Lxu;->b:I

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lm;->b(Z)V

    .line 54
    iget v0, p0, Lxu;->b:I

    iget v3, p0, Lxu;->a:I

    if-ge v0, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lm;->b(Z)V

    .line 55
    iget v0, p0, Lxu;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lxu;->b:I

    .line 56
    iget v0, p0, Lxu;->b:I

    if-ne v0, v1, :cond_1

    .line 57
    invoke-virtual {p0}, Lxu;->a()V

    .line 59
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 53
    goto :goto_0
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    iget v0, p0, Lxu;->b:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lm;->b(Z)V

    .line 64
    iget v0, p0, Lxu;->b:I

    iget v3, p0, Lxu;->a:I

    if-gt v0, v3, :cond_2

    :goto_1
    invoke-static {v1}, Lm;->b(Z)V

    .line 65
    iget v0, p0, Lxu;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lxu;->b:I

    .line 66
    iget v0, p0, Lxu;->b:I

    if-nez v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lxu;->b()V

    .line 69
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 63
    goto :goto_0

    :cond_2
    move v1, v2

    .line 64
    goto :goto_1
.end method
