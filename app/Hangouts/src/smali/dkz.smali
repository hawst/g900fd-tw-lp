.class public final Ldkz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldkz;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ldkv;

.field public d:Ljava/lang/Integer;

.field public e:Ldkx;

.field public f:Ldla;

.field public g:Ldkw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    new-array v0, v0, [Ldkz;

    sput-object v0, Ldkz;->a:[Ldkz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 176
    invoke-direct {p0}, Lepn;-><init>()V

    .line 189
    iput-object v0, p0, Ldkz;->c:Ldkv;

    .line 192
    iput-object v0, p0, Ldkz;->d:Ljava/lang/Integer;

    .line 195
    iput-object v0, p0, Ldkz;->e:Ldkx;

    .line 198
    iput-object v0, p0, Ldkz;->f:Ldla;

    .line 201
    iput-object v0, p0, Ldkz;->g:Ldkw;

    .line 176
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 231
    iget-object v1, p0, Ldkz;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 232
    const/4 v0, 0x1

    iget-object v1, p0, Ldkz;->b:Ljava/lang/String;

    .line 233
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 235
    :cond_0
    iget-object v1, p0, Ldkz;->c:Ldkv;

    if-eqz v1, :cond_1

    .line 236
    const/4 v1, 0x2

    iget-object v2, p0, Ldkz;->c:Ldkv;

    .line 237
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_1
    iget-object v1, p0, Ldkz;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 240
    const/4 v1, 0x3

    iget-object v2, p0, Ldkz;->d:Ljava/lang/Integer;

    .line 241
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    :cond_2
    iget-object v1, p0, Ldkz;->e:Ldkx;

    if-eqz v1, :cond_3

    .line 244
    const/4 v1, 0x4

    iget-object v2, p0, Ldkz;->e:Ldkx;

    .line 245
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 247
    :cond_3
    iget-object v1, p0, Ldkz;->f:Ldla;

    if-eqz v1, :cond_4

    .line 248
    const/4 v1, 0x5

    iget-object v2, p0, Ldkz;->f:Ldla;

    .line 249
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    :cond_4
    iget-object v1, p0, Ldkz;->g:Ldkw;

    if-eqz v1, :cond_5

    .line 252
    const/4 v1, 0x6

    iget-object v2, p0, Ldkz;->g:Ldkw;

    .line 253
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_5
    iget-object v1, p0, Ldkz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 256
    iput v0, p0, Ldkz;->cachedSize:I

    .line 257
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 172
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldkz;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldkz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldkz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkz;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldkz;->c:Ldkv;

    if-nez v0, :cond_2

    new-instance v0, Ldkv;

    invoke-direct {v0}, Ldkv;-><init>()V

    iput-object v0, p0, Ldkz;->c:Ldkv;

    :cond_2
    iget-object v0, p0, Ldkz;->c:Ldkv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkz;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkz;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldkz;->e:Ldkx;

    if-nez v0, :cond_5

    new-instance v0, Ldkx;

    invoke-direct {v0}, Ldkx;-><init>()V

    iput-object v0, p0, Ldkz;->e:Ldkx;

    :cond_5
    iget-object v0, p0, Ldkz;->e:Ldkx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldkz;->f:Ldla;

    if-nez v0, :cond_6

    new-instance v0, Ldla;

    invoke-direct {v0}, Ldla;-><init>()V

    iput-object v0, p0, Ldkz;->f:Ldla;

    :cond_6
    iget-object v0, p0, Ldkz;->f:Ldla;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldkz;->g:Ldkw;

    if-nez v0, :cond_7

    new-instance v0, Ldkw;

    invoke-direct {v0}, Ldkw;-><init>()V

    iput-object v0, p0, Ldkz;->g:Ldkw;

    :cond_7
    iget-object v0, p0, Ldkz;->g:Ldkw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Ldkz;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 207
    const/4 v0, 0x1

    iget-object v1, p0, Ldkz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 209
    :cond_0
    iget-object v0, p0, Ldkz;->c:Ldkv;

    if-eqz v0, :cond_1

    .line 210
    const/4 v0, 0x2

    iget-object v1, p0, Ldkz;->c:Ldkv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 212
    :cond_1
    iget-object v0, p0, Ldkz;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 213
    const/4 v0, 0x3

    iget-object v1, p0, Ldkz;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 215
    :cond_2
    iget-object v0, p0, Ldkz;->e:Ldkx;

    if-eqz v0, :cond_3

    .line 216
    const/4 v0, 0x4

    iget-object v1, p0, Ldkz;->e:Ldkx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 218
    :cond_3
    iget-object v0, p0, Ldkz;->f:Ldla;

    if-eqz v0, :cond_4

    .line 219
    const/4 v0, 0x5

    iget-object v1, p0, Ldkz;->f:Ldla;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 221
    :cond_4
    iget-object v0, p0, Ldkz;->g:Ldkw;

    if-eqz v0, :cond_5

    .line 222
    const/4 v0, 0x6

    iget-object v1, p0, Ldkz;->g:Ldkw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 224
    :cond_5
    iget-object v0, p0, Ldkz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 226
    return-void
.end method
