.class public final Ldoi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldoi;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x0

    new-array v0, v0, [Ldoi;

    sput-object v0, Ldoi;->a:[Ldoi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 511
    invoke-direct {p0}, Lepn;-><init>()V

    .line 516
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldoi;->c:[Ljava/lang/Integer;

    .line 511
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 535
    const/4 v0, 0x0

    .line 536
    iget-object v1, p0, Ldoi;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 537
    const/16 v0, 0x3e

    iget-object v1, p0, Ldoi;->b:Ljava/lang/String;

    .line 538
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 540
    :cond_0
    iget-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 541
    iget-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 542
    add-int/2addr v0, v1

    .line 543
    iget-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 545
    :cond_1
    iget-object v1, p0, Ldoi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 546
    iput v0, p0, Ldoi;->cachedSize:I

    .line 547
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 507
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldoi;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldoi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldoi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoi;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1fd

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldoi;->c:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Ldoi;->c:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f2 -> :sswitch_1
        0x1fd -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 521
    iget-object v0, p0, Ldoi;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 522
    const/16 v0, 0x3e

    iget-object v1, p0, Ldoi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 524
    :cond_0
    iget-object v0, p0, Ldoi;->c:[Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 525
    iget-object v1, p0, Ldoi;->c:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 526
    const/16 v4, 0x3f

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->b(II)V

    .line 525
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 529
    :cond_1
    iget-object v0, p0, Ldoi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 531
    return-void
.end method
