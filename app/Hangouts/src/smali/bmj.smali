.class final Lbmj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Z

.field final d:I

.field final e:I

.field final f:Ljava/lang/String;

.field final g:J

.field final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmo;",
            ">;"
        }
    .end annotation
.end field

.field i:I

.field final j:Z

.field final k:Ljava/lang/String;

.field final l:I

.field final m:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JZILjava/lang/String;IJ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object p1, p0, Lbmj;->a:Ljava/lang/String;

    .line 257
    iput-object p2, p0, Lbmj;->b:Ljava/lang/String;

    .line 258
    iput p3, p0, Lbmj;->e:I

    .line 259
    iget v0, p0, Lbmj;->e:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbmj;->c:Z

    .line 260
    iput p8, p0, Lbmj;->d:I

    .line 261
    iput-object p4, p0, Lbmj;->f:Ljava/lang/String;

    .line 262
    iput-wide p5, p0, Lbmj;->g:J

    .line 263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbmj;->h:Ljava/util/List;

    .line 264
    iput v1, p0, Lbmj;->i:I

    .line 265
    iput-boolean p7, p0, Lbmj;->j:Z

    .line 266
    iput-object p9, p0, Lbmj;->k:Ljava/lang/String;

    .line 267
    iput p10, p0, Lbmj;->l:I

    .line 268
    iput-wide p11, p0, Lbmj;->m:J

    .line 269
    sget-boolean v0, Lbmf;->a:Z

    if-eqz v0, :cond_0

    .line 270
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Conversation Line: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbmj;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbmj;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 259
    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 278
    iget-object v0, p0, Lbmj;->h:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmo;

    iget v0, v0, Lbmo;->g:I

    .line 280
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 284
    iget v1, p0, Lbmj;->l:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
