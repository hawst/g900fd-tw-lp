.class public final Lcfa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/widget/PopupWindow;

.field b:Z

.field private c:Landroid/view/LayoutInflater;

.field private d:Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

.field private e:Z

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcfa;->c:Landroid/view/LayoutInflater;

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dD:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcfa;->f:I

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dG:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcfa;->g:I

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dE:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcfa;->h:I

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dF:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcfa;->i:I

    .line 47
    iput-boolean p3, p0, Lcfa;->e:Z

    .line 48
    iput-object p2, p0, Lcfa;->j:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcfa;->b:Z

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 97
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x2

    const/4 v5, 0x1

    .line 52
    iget-boolean v0, p0, Lcfa;->b:Z

    .line 53
    iput-boolean v5, p0, Lcfa;->b:Z

    .line 54
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 55
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 57
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 59
    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcfa;->d:Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a(I)V

    .line 91
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcfa;->c:Landroid/view/LayoutInflater;

    sget v3, Lf;->gK:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

    iput-object v0, p0, Lcfa;->d:Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

    .line 64
    iget-object v0, p0, Lcfa;->d:Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a(I)V

    .line 65
    iget-object v0, p0, Lcfa;->d:Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

    iget-object v2, p0, Lcfa;->j:Ljava/lang/String;

    iget-boolean v3, p0, Lcfa;->e:Z

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a(Ljava/lang/String;Z)V

    .line 67
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcfa;->d:Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

    invoke-direct {v0, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    .line 70
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 71
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 73
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 74
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v7}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 75
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_1

    .line 76
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    sget v2, Lf;->hV:I

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 80
    :goto_1
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    new-instance v2, Lcfb;

    invoke-direct {v2, p0}, Lcfb;-><init>(Lcfa;)V

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 87
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    const/16 v2, 0x33

    aget v3, v1, v7

    iget v4, p0, Lcfa;->h:I

    sub-int/2addr v3, v4

    aget v1, v1, v5

    iget v4, p0, Lcfa;->f:I

    sub-int/2addr v1, v4

    iget v4, p0, Lcfa;->g:I

    sub-int/2addr v1, v4

    invoke-virtual {v0, p1, v2, v3, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0

    .line 78
    :cond_1
    iget-object v0, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    sget v2, Lf;->hW:I

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    goto :goto_1
.end method

.method public b(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 107
    iget-boolean v0, p0, Lcfa;->b:Z

    if-nez v0, :cond_0

    .line 117
    :goto_0
    return-void

    .line 110
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 111
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 112
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 113
    iget-object v2, p0, Lcfa;->d:Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a(I)V

    .line 114
    iget-object v1, p0, Lcfa;->a:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    aget v2, v0, v2

    iget v3, p0, Lcfa;->h:I

    sub-int/2addr v2, v3

    const/4 v3, 0x1

    aget v0, v0, v3

    iget v3, p0, Lcfa;->f:I

    sub-int/2addr v0, v3

    iget v3, p0, Lcfa;->g:I

    sub-int/2addr v0, v3

    invoke-virtual {v1, v2, v0, v4, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcfa;->b:Z

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcfa;->j:Ljava/lang/String;

    return-object v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcfa;->d:Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;

    return-object v0
.end method
