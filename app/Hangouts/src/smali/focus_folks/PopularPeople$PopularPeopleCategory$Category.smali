.class public interface abstract Lfocus_folks/PopularPeople$PopularPeopleCategory$Category;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final ANIMALS:I = 0x21

.field public static final ART_AND_DESIGN:I = 0x1

.field public static final AUTO:I = 0x1c

.field public static final BOOKS:I = 0x2

.field public static final BUSINESS:I = 0x3

.field public static final CHARITY:I = 0x4

.field public static final CONNECT_WITH_GOOGLE:I = 0x18

.field public static final EDUCATION:I = 0x1a

.field public static final ENTERTAINMENT:I = 0x5

.field public static final FAMILY:I = 0x6

.field public static final FASHION:I = 0x7

.field public static final FOOD_AND_DRINK:I = 0x8

.field public static final FUN_AND_INTERESTING:I = 0x9

.field public static final GAMES:I = 0x16

.field public static final GOOGLERS:I = 0x12

.field public static final GOVERNMENT:I = 0xa

.field public static final HEALTH:I = 0xb

.field public static final HISTORY_AND_MUSEUMS:I = 0x26

.field public static final HOME_AND_GARDEN:I = 0x20

.field public static final HOW_TO:I = 0x1b

.field public static final HUMOR:I = 0x25

.field public static final LIFESTYLE:I = 0x17

.field public static final MEDIA:I = 0x13

.field public static final MUSIC:I = 0xc

.field public static final NEWS:I = 0xd

.field public static final PHOTOGRAPHY:I = 0x1d

.field public static final POPULAR_IN_LOCALE:I = 0x15

.field public static final SCIENCE:I = 0xe

.field public static final SHOPPING_AND_BRANDS:I = 0x24

.field public static final SOCCER:I = 0x27

.field public static final SPIRITUALITY:I = 0x1f

.field public static final SPORTS:I = 0xf

.field public static final TECHNOLOGY:I = 0x10

.field public static final TRAVEL:I = 0x11

.field public static final TV_SHOWS:I = 0x23

.field public static final WEDDINGS:I = 0x22

.field public static final WORLD:I = 0x14

.field public static final YOUTUBE_PARTNERS:I = 0x19

.field public static final YOUTUBE_SUBSCRIPTIONS:I = 0x1e
