.class public final Ldqm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqm;


# instance fields
.field public b:Ldqf;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17083
    const/4 v0, 0x0

    new-array v0, v0, [Ldqm;

    sput-object v0, Ldqm;->a:[Ldqm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17084
    invoke-direct {p0}, Lepn;-><init>()V

    .line 17087
    iput-object v0, p0, Ldqm;->b:Ldqf;

    .line 17090
    iput-object v0, p0, Ldqm;->c:Ljava/lang/Integer;

    .line 17093
    iput-object v0, p0, Ldqm;->d:Ljava/lang/Integer;

    .line 17084
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 17113
    const/4 v0, 0x0

    .line 17114
    iget-object v1, p0, Ldqm;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 17115
    const/4 v0, 0x1

    iget-object v1, p0, Ldqm;->b:Ldqf;

    .line 17116
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17118
    :cond_0
    iget-object v1, p0, Ldqm;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 17119
    const/4 v1, 0x2

    iget-object v2, p0, Ldqm;->c:Ljava/lang/Integer;

    .line 17120
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17122
    :cond_1
    iget-object v1, p0, Ldqm;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 17123
    const/4 v1, 0x3

    iget-object v2, p0, Ldqm;->d:Ljava/lang/Integer;

    .line 17124
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17126
    :cond_2
    iget-object v1, p0, Ldqm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17127
    iput v0, p0, Ldqm;->cachedSize:I

    .line 17128
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17080
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqm;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldqm;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldqm;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    if-eq v0, v3, :cond_3

    if-ne v0, v4, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqm;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqm;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-ne v0, v4, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqm;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqm;->d:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 17098
    iget-object v0, p0, Ldqm;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 17099
    const/4 v0, 0x1

    iget-object v1, p0, Ldqm;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 17101
    :cond_0
    iget-object v0, p0, Ldqm;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 17102
    const/4 v0, 0x2

    iget-object v1, p0, Ldqm;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 17104
    :cond_1
    iget-object v0, p0, Ldqm;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 17105
    const/4 v0, 0x3

    iget-object v1, p0, Ldqm;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 17107
    :cond_2
    iget-object v0, p0, Ldqm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 17109
    return-void
.end method
