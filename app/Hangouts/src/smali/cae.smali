.class public final Lcae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/AudienceView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcae;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 227
    check-cast p1, Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 228
    iget-object v1, p0, Lcae;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->c(Lcom/google/android/apps/hangouts/views/AudienceView;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 229
    iget-object v0, p0, Lcae;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->d(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    .line 230
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 220
    check-cast p1, Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 221
    iget-object v1, p0, Lcae;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->c(Lcom/google/android/apps/hangouts/views/AudienceView;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 222
    iget-object v0, p0, Lcae;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->d(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    .line 223
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 216
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 212
    return-void
.end method
