.class public Lbfc;
.super Lbfq;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:[Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:Ljava/lang/String;

.field public final n:I

.field public final o:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1163
    invoke-direct {p0, p2, p1, p9, p10}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1164
    iput-object p3, p0, Lbfc;->a:[Ljava/lang/String;

    .line 1165
    iput-object p4, p0, Lbfc;->b:Ljava/lang/String;

    .line 1166
    iput-object v1, p0, Lbfc;->c:Ljava/lang/String;

    .line 1167
    iput-object v1, p0, Lbfc;->h:Ljava/lang/String;

    .line 1168
    iput-object v1, p0, Lbfc;->i:Ljava/lang/String;

    .line 1169
    iput v0, p0, Lbfc;->j:I

    .line 1170
    iput v0, p0, Lbfc;->k:I

    .line 1171
    iput v0, p0, Lbfc;->l:I

    .line 1172
    iput-object p5, p0, Lbfc;->m:Ljava/lang/String;

    .line 1173
    iput p6, p0, Lbfc;->n:I

    .line 1174
    iput-wide p7, p0, Lbfc;->o:J

    .line 1175
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJ)V
    .locals 2

    .prologue
    .line 1145
    invoke-direct {p0, p2, p1, p11, p12}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1146
    iput-object p3, p0, Lbfc;->a:[Ljava/lang/String;

    .line 1147
    iput-object p4, p0, Lbfc;->b:Ljava/lang/String;

    .line 1148
    iput-object p5, p0, Lbfc;->c:Ljava/lang/String;

    .line 1149
    iput-object p6, p0, Lbfc;->h:Ljava/lang/String;

    .line 1150
    iput-object p7, p0, Lbfc;->i:Ljava/lang/String;

    .line 1151
    iput p8, p0, Lbfc;->j:I

    .line 1152
    iput p9, p0, Lbfc;->k:I

    .line 1153
    iput p10, p0, Lbfc;->l:I

    .line 1154
    const/4 v0, 0x0

    iput-object v0, p0, Lbfc;->m:Ljava/lang/String;

    .line 1155
    const/4 v0, 0x0

    iput v0, p0, Lbfc;->n:I

    .line 1156
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbfc;->o:J

    .line 1157
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lbpp;Lbpk;)Lbfz;
    .locals 12

    .prologue
    const/16 v11, 0x88

    .line 1190
    :try_start_0
    iget-object v0, p0, Lbfc;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1195
    iget-object v1, p0, Lbfc;->a:[Ljava/lang/String;

    iget-object v2, p0, Lbfc;->b:Ljava/lang/String;

    iget-object v3, p0, Lbfc;->m:Ljava/lang/String;

    iget v0, p0, Lbfc;->n:I

    iget-wide v4, p0, Lbfc;->p:J

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lbvv;->a(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Lbvw;

    move-result-object v0

    move-object v2, v0

    .line 1222
    :goto_0
    new-instance v6, Lbfd;

    invoke-direct {v6, p0}, Lbfd;-><init>(Lbfc;)V

    .line 1239
    iget-object v0, v2, Lbvw;->a:Lsc;

    iget-object v1, v2, Lbvw;->b:Lsb;

    .line 1240
    invoke-static {p1, v0, v1, v6}, Lbvx;->a(Landroid/content/Context;Lsc;Lsb;Lrw;)Landroid/net/Uri;

    move-result-object v1

    .line 1242
    if-nez v1, :cond_1

    .line 1245
    const-string v0, "Babel_RequestWriter"

    const-string v1, "SendMmsRequest: failed to persist message into telephony"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    new-instance v0, Lbph;

    const/16 v1, 0x86

    const-string v2, "Failed to persist sent mms message"

    invoke-direct {v0, v1, v2}, Lbph;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lbvu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lrb; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lbvp; {:try_start_0 .. :try_end_0} :catch_3

    .line 1258
    :catch_0
    move-exception v0

    .line 1259
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SendMmsRequest: failed to send message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1260
    new-instance v1, Lbph;

    const/16 v2, 0x76

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 1206
    :cond_0
    :try_start_1
    iget-object v1, p0, Lbfc;->a:[Ljava/lang/String;

    iget-object v2, p0, Lbfc;->b:Ljava/lang/String;

    iget-object v3, p0, Lbfc;->c:Ljava/lang/String;

    iget-object v4, p0, Lbfc;->h:Ljava/lang/String;

    iget-object v5, p0, Lbfc;->i:Ljava/lang/String;

    iget v6, p0, Lbfc;->j:I

    iget v7, p0, Lbfc;->k:I

    iget v8, p0, Lbfc;->l:I

    iget-wide v9, p0, Lbfc;->p:J

    move-object v0, p1

    invoke-static/range {v0 .. v10}, Lbvv;->a(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJ)Lbvw;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 1251
    :cond_1
    const/4 v0, 0x1

    .line 1252
    invoke-static {v1}, Lbvx;->c(Landroid/net/Uri;)J

    move-result-wide v3

    .line 1251
    invoke-static {v0, v3, v4}, Lbwf;->a(IJ)V

    .line 1253
    new-instance v0, Lbhx;

    iget-object v2, v2, Lbvw;->a:Lsc;

    .line 1255
    invoke-virtual {v2}, Lsc;->h()J

    move-result-wide v2

    iget-wide v4, p0, Lbfc;->p:J

    .line 1257
    invoke-interface {v6}, Lrw;->a()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lbhx;-><init>(Landroid/net/Uri;JJJ)V
    :try_end_1
    .catch Lbvu; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lrb; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lbvp; {:try_start_1 .. :try_end_1} :catch_3

    return-object v0

    .line 1262
    :catch_1
    move-exception v0

    .line 1263
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SendMmsRequest: failed to send message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1264
    new-instance v1, Lbph;

    invoke-direct {v1, v11, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 1266
    :catch_2
    move-exception v0

    .line 1267
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SendMmsRequest: invalid message to send "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1268
    new-instance v1, Lbph;

    invoke-direct {v1, v11, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 1270
    :catch_3
    move-exception v0

    .line 1271
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SendMmsRequest: failed to send message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1272
    new-instance v1, Lbph;

    iget v2, v0, Lbvp;->a:I

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1181
    const-string v0, "mms_queue"

    return-object v0
.end method
