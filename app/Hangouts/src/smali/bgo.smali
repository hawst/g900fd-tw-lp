.class public Lbgo;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:Z

.field private final k:I


# direct methods
.method private constructor <init>(Lete;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 4701
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 4702
    iget-object v1, p1, Lete;->e:Ljava/lang/Integer;

    invoke-static {v1, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    iput v1, p0, Lbgo;->k:I

    .line 4705
    iget-object v1, p1, Lete;->c:Ljava/lang/Integer;

    invoke-static {v1, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p1, Lete;->d:Letf;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lete;->d:Letf;

    iget-object v1, v1, Letf;->d:Lesw;

    if-eqz v1, :cond_0

    .line 4708
    iget-object v1, p1, Lete;->d:Letf;

    iget-object v1, v1, Letf;->d:Lesw;

    .line 4712
    :goto_0
    if-eqz v1, :cond_2

    .line 4713
    iget-object v3, v1, Lesw;->d:Ljava/lang/String;

    iput-object v3, p0, Lbgo;->i:Ljava/lang/String;

    .line 4714
    iget-object v1, v1, Lesw;->b:Ljava/lang/Long;

    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_1

    .line 4715
    iput-boolean v0, p0, Lbgo;->j:Z

    .line 4726
    :goto_1
    const v1, 0xea60

    .line 4727
    iget-boolean v3, p0, Lbgo;->j:Z

    if-nez v3, :cond_6

    iget-object v3, p1, Lete;->f:[Letb;

    if-eqz v3, :cond_6

    .line 4728
    iget-object v4, p1, Lete;->f:[Letb;

    array-length v5, v4

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_6

    aget-object v6, v4, v3

    .line 4729
    iget-object v7, v6, Letb;->b:Ljava/lang/Integer;

    invoke-static {v7, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v7

    if-ne v7, v0, :cond_4

    .line 4730
    iget-object v3, v6, Letb;->c:Ljava/lang/Boolean;

    invoke-static {v3, v2}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 4735
    :goto_3
    iget-object v3, p1, Lete;->d:Letf;

    if-eqz v3, :cond_5

    iget-object v3, p1, Lete;->d:Letf;

    iget-object v3, v3, Letf;->c:Ljava/lang/Integer;

    if-eqz v3, :cond_5

    .line 4737
    iget-object v1, p1, Lete;->d:Letf;

    iget-object v1, v1, Letf;->c:Ljava/lang/Integer;

    .line 4738
    invoke-static {v1, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    move v8, v1

    move v1, v0

    move v0, v8

    .line 4744
    :goto_4
    iput v1, p0, Lbgo;->g:I

    .line 4745
    iput v0, p0, Lbgo;->h:I

    .line 4746
    return-void

    .line 4710
    :cond_0
    iget-object v1, p1, Lete;->b:Lesw;

    goto :goto_0

    .line 4717
    :cond_1
    iput-boolean v2, p0, Lbgo;->j:Z

    goto :goto_1

    .line 4720
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lbgo;->i:Ljava/lang/String;

    .line 4721
    iput-boolean v2, p0, Lbgo;->j:Z

    goto :goto_1

    .line 4733
    :cond_3
    const/4 v0, 0x2

    goto :goto_3

    .line 4728
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_4

    :cond_6
    move v0, v1

    move v1, v2

    goto :goto_4
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4750
    if-eqz p0, :cond_0

    .line 4751
    new-instance v0, Lete;

    invoke-direct {v0}, Lete;-><init>()V

    .line 4752
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Lete;

    .line 4753
    if-eqz v0, :cond_0

    .line 4754
    new-instance v1, Lbgo;

    invoke-direct {v1, v0}, Lbgo;-><init>(Lete;)V

    move-object v0, v1

    .line 4758
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public f()I
    .locals 1

    .prologue
    .line 4767
    iget v0, p0, Lbgo;->g:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 4772
    iget v0, p0, Lbgo;->h:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4781
    iget-object v0, p0, Lbgo;->i:Ljava/lang/String;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 4786
    iget-boolean v0, p0, Lbgo;->j:Z

    return v0
.end method
