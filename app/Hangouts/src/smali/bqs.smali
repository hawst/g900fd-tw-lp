.class public final Lbqs;
.super Lbqu;
.source "PG"


# instance fields
.field private final A:I

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:J

.field private final E:Z

.field private F:I

.field private final G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:I

.field private J:I

.field private K:I

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbiz;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbiw;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Z

.field private final o:I

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:J

.field private final s:J

.field private final t:J

.field private u:J

.field private final v:Z

.field private final w:J

.field private final x:I

.field private final y:J

.field private final z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbiv;ZJJJJ)V
    .locals 18

    .prologue
    .line 437
    move-object/from16 v0, p1

    iget-object v4, v0, Lbiv;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lbiv;->d:Lbdk;

    move-object/from16 v0, p1

    iget-wide v6, v0, Lbiv;->e:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lbiv;->n:J

    move-object/from16 v0, p1

    iget-object v10, v0, Lbiv;->m:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lbiv;->l:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v12, v0, Lbiv;->g:I

    move-object/from16 v0, p1

    iget v13, v0, Lbiv;->h:I

    move-object/from16 v0, p1

    iget-wide v14, v0, Lbiv;->i:J

    const/16 v16, 0x4

    move-object/from16 v0, p1

    iget-object v0, v0, Lbiv;->t:[B

    move-object/from16 v17, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v17}, Lbqu;-><init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;IIJI[B)V

    .line 423
    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbqs;->D:J

    .line 425
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lbqs;->F:I

    .line 442
    move-object/from16 v0, p1

    iget-object v3, v0, Lbiv;->b:Ljava/util/List;

    move-object/from16 v0, p0

    iput-object v3, v0, Lbqs;->l:Ljava/util/List;

    .line 443
    move-object/from16 v0, p1

    iget-object v3, v0, Lbiv;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iput-object v3, v0, Lbqs;->m:Ljava/util/List;

    .line 444
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lbiv;->p:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbqs;->n:Z

    .line 445
    move-object/from16 v0, p1

    iget v3, v0, Lbiv;->r:I

    move-object/from16 v0, p0

    iput v3, v0, Lbqs;->o:I

    .line 446
    move-object/from16 v0, p1

    iget-object v3, v0, Lbiv;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lbqs;->p:Ljava/lang/String;

    .line 447
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lbqs;->q:Ljava/lang/String;

    .line 448
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbqs;->w:J

    .line 449
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lbqs;->x:I

    .line 450
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbqs;->y:J

    .line 451
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lbqs;->z:Ljava/lang/String;

    .line 452
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iput v3, v0, Lbqs;->A:I

    .line 453
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lbiv;->q:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbqs;->E:Z

    .line 454
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lbqs;->G:Ljava/lang/String;

    .line 455
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbqs;->v:Z

    .line 456
    move-wide/from16 v0, p3

    move-object/from16 v2, p0

    iput-wide v0, v2, Lbqs;->r:J

    .line 457
    move-wide/from16 v0, p5

    move-object/from16 v2, p0

    iput-wide v0, v2, Lbqs;->s:J

    .line 458
    move-wide/from16 v0, p7

    move-object/from16 v2, p0

    iput-wide v0, v2, Lbqs;->t:J

    .line 459
    move-wide/from16 v0, p9

    move-object/from16 v2, p0

    iput-wide v0, v2, Lbqs;->u:J

    .line 460
    invoke-virtual/range {p1 .. p1}, Lbiv;->d()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lbqs;->K:I

    .line 461
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lbdk;JLjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZIIILjava/lang/String;Ljava/lang/String;JIJLjava/lang/String;ILjava/lang/String;IJ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lbdk;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbiz;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbiw;",
            ">;ZIII",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JIJ",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 473
    const-wide/16 v8, -0x1

    const/4 v11, 0x0

    const/4 v12, -0x1

    const/4 v13, 0x1

    const-wide/16 v14, 0x0

    const/16 v17, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move-object/from16 v10, p5

    move/from16 v16, p10

    invoke-direct/range {v3 .. v17}, Lbqu;-><init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;IIJI[B)V

    .line 423
    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbqs;->D:J

    .line 425
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lbqs;->F:I

    .line 476
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lbqs;->G:Ljava/lang/String;

    .line 477
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lbqs;->l:Ljava/util/List;

    .line 478
    if-eqz p6, :cond_0

    .line 479
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbiz;

    .line 480
    move-object/from16 v0, p0

    iget-object v5, v0, Lbqs;->l:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 483
    :cond_0
    if-eqz p8, :cond_1

    .line 484
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lbqs;->m:Ljava/util/List;

    .line 485
    invoke-interface/range {p8 .. p8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbiw;

    .line 486
    move-object/from16 v0, p0

    iget-object v5, v0, Lbqs;->m:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 489
    :cond_1
    move/from16 v0, p9

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lbqs;->n:Z

    .line 490
    if-eqz p9, :cond_2

    .line 491
    const-wide v3, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbqs;->i:J

    .line 495
    :goto_2
    move/from16 v0, p12

    move-object/from16 v1, p0

    iput v0, v1, Lbqs;->o:I

    .line 496
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, Lbqs;->p:Ljava/lang/String;

    .line 497
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbqs;->v:Z

    .line 498
    move-wide/from16 v0, p24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lbqs;->r:J

    .line 499
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbqs;->s:J

    .line 500
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbqs;->t:J

    .line 502
    move-object/from16 v0, p14

    move-object/from16 v1, p0

    iput-object v0, v1, Lbqs;->q:Ljava/lang/String;

    .line 503
    move-wide/from16 v0, p15

    move-object/from16 v2, p0

    iput-wide v0, v2, Lbqs;->w:J

    .line 504
    move/from16 v0, p17

    move-object/from16 v1, p0

    iput v0, v1, Lbqs;->x:I

    .line 505
    move-wide/from16 v0, p18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lbqs;->y:J

    .line 506
    move-object/from16 v0, p20

    move-object/from16 v1, p0

    iput-object v0, v1, Lbqs;->z:Ljava/lang/String;

    .line 507
    move/from16 v0, p21

    move-object/from16 v1, p0

    iput v0, v1, Lbqs;->A:I

    .line 508
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbqs;->E:Z

    .line 509
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, Lbqs;->H:Ljava/lang/String;

    .line 510
    move/from16 v0, p23

    move-object/from16 v1, p0

    iput v0, v1, Lbqs;->I:I

    .line 511
    move/from16 v0, p11

    move-object/from16 v1, p0

    iput v0, v1, Lbqs;->J:I

    .line 512
    return-void

    .line 493
    :cond_2
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbqs;->i:J

    goto :goto_2
.end method

.method private c(Lyt;Lbnl;)V
    .locals 35

    .prologue
    .line 583
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbqs;->g:Lbdk;

    .line 584
    invoke-virtual {v3, v4}, Lbdk;->a(Lbdk;)Z

    move-result v24

    .line 586
    const/16 v21, 0x0

    .line 587
    const/16 v20, 0x0

    .line 588
    const/4 v15, 0x0

    .line 589
    const/16 v18, 0x0

    .line 590
    const/16 v16, 0x0

    .line 591
    const/4 v3, 0x0

    .line 592
    const/4 v14, 0x0

    .line 593
    const/4 v13, 0x0

    .line 594
    const-wide/16 v6, 0x0

    .line 595
    const-wide/16 v4, 0x0

    .line 596
    const/4 v12, 0x0

    .line 597
    const/4 v11, 0x0

    .line 598
    const/4 v10, 0x0

    .line 599
    const/4 v9, 0x0

    .line 600
    const/16 v17, 0x0

    .line 603
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->m:Ljava/util/List;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->m:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v22, v19

    .line 606
    :goto_0
    const/16 v19, 0x1

    move/from16 v0, v22

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 608
    move-object/from16 v0, p0

    iget-object v3, v0, Lbqs;->m:Ljava/util/List;

    const/4 v12, 0x0

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbiw;

    .line 609
    iget-object v0, v3, Lbiw;->f:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 610
    iget-object v15, v3, Lbiw;->h:Ljava/lang/String;

    .line 611
    iget-object v12, v3, Lbiw;->g:Ljava/lang/String;

    .line 612
    iget-object v14, v3, Lbiw;->d:Ljava/lang/String;

    .line 613
    iget-object v13, v3, Lbiw;->e:Ljava/lang/String;

    .line 614
    instance-of v0, v3, Lbiy;

    move/from16 v22, v0

    if-eqz v22, :cond_2

    .line 615
    check-cast v3, Lbiy;

    .line 616
    iget-object v0, v3, Lbiy;->i:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 617
    iget-object v0, v3, Lbiy;->j:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 618
    iget v0, v3, Lbiy;->k:I

    move/from16 v16, v0

    .line 619
    iget v11, v3, Lbiy;->l:I

    .line 620
    iget v9, v3, Lbiy;->m:I

    .line 621
    iget-object v10, v3, Lbiy;->n:Ljava/lang/String;

    .line 622
    iget-object v3, v3, Lbiy;->o:Ljava/lang/String;

    move-object/from16 v21, v20

    move-object/from16 v20, v18

    move/from16 v18, v11

    move-object v11, v13

    move-object/from16 v13, v19

    move/from16 v19, v16

    move-object/from16 v16, v14

    move-object/from16 v30, v10

    move-object v10, v15

    move-wide v14, v6

    move-object/from16 v6, v30

    move-object v7, v12

    move-wide/from16 v31, v4

    move-object v5, v3

    move v4, v9

    move-object v3, v8

    move-wide/from16 v8, v31

    .line 671
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lbqs;->G:Ljava/lang/String;

    if-eqz v12, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lbqs;->G:Ljava/lang/String;

    .line 675
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v23, v0

    .line 674
    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;)Z

    move-result v25

    .line 678
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lyt;->m(Ljava/lang/String;)Lzc;

    move-result-object v22

    .line 679
    move-object/from16 v0, v22

    iget-wide v0, v0, Lzc;->a:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->h:J

    move-wide/from16 v26, v0

    cmp-long v22, v22, v26

    if-gtz v22, :cond_8

    const/16 v22, 0x1

    .line 680
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lyt;->t(Ljava/lang/String;)J

    move-result-wide v26

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->h:J

    move-wide/from16 v28, v0

    cmp-long v23, v28, v26

    if-gtz v23, :cond_9

    const/16 v23, 0x1

    .line 682
    :goto_4
    new-instance v26, Lzf;

    invoke-direct/range {v26 .. v26}, Lzf;-><init>()V

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    iput-object v0, v1, Lzf;->a:Ljava/lang/String;

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->b:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    iput-object v0, v1, Lzf;->b:Ljava/lang/String;

    .line 685
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    iput-object v0, v1, Lzf;->c:Ljava/lang/String;

    .line 686
    move-object/from16 v0, p0

    iget v0, v0, Lbqs;->o:I

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lzf;->d:I

    .line 687
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->p:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    iput-object v0, v1, Lzf;->e:Ljava/lang/String;

    .line 688
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->g:Lbdk;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    iput-object v0, v1, Lzf;->f:Lbdk;

    .line 689
    move-object/from16 v0, v26

    iput-object v12, v0, Lzf;->g:Ljava/lang/String;

    .line 690
    move-object/from16 v0, p0

    iget v0, v0, Lbqs;->k:I

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput v0, v1, Lzf;->h:I

    .line 691
    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->h:J

    move-wide/from16 v27, v0

    move-wide/from16 v0, v27

    move-object/from16 v2, v26

    iput-wide v0, v2, Lzf;->i:J

    .line 692
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    iput-object v0, v1, Lzf;->j:Ljava/lang/String;

    .line 693
    move-object/from16 v0, v20

    move-object/from16 v1, v26

    iput-object v0, v1, Lzf;->k:Ljava/lang/String;

    .line 694
    move-object/from16 v0, v26

    iput-object v13, v0, Lzf;->m:Ljava/lang/String;

    .line 695
    move-object/from16 v0, v16

    move-object/from16 v1, v26

    iput-object v0, v1, Lzf;->n:Ljava/lang/String;

    .line 696
    move-object/from16 v0, v26

    iput-object v11, v0, Lzf;->o:Ljava/lang/String;

    .line 697
    move-object/from16 v0, v26

    iput-wide v14, v0, Lzf;->p:D

    .line 698
    move-object/from16 v0, v26

    iput-wide v8, v0, Lzf;->q:D

    .line 699
    move-object/from16 v0, v26

    iput-object v7, v0, Lzf;->r:Ljava/lang/String;

    .line 700
    move-object/from16 v0, v26

    iput-object v6, v0, Lzf;->s:Ljava/lang/String;

    .line 701
    move-object/from16 v0, v26

    iput-object v5, v0, Lzf;->t:Ljava/lang/String;

    .line 702
    move-object/from16 v0, v26

    iput v4, v0, Lzf;->l:I

    .line 704
    move/from16 v0, v24

    move-object/from16 v1, v26

    iput-boolean v0, v1, Lzf;->u:Z

    .line 705
    move-object/from16 v0, p0

    iget v4, v0, Lbqs;->c:I

    move-object/from16 v0, v26

    iput v4, v0, Lzf;->v:I

    .line 706
    move-object/from16 v0, p0

    iget-wide v4, v0, Lbqs;->i:J

    move-object/from16 v0, v26

    iput-wide v4, v0, Lzf;->w:J

    .line 707
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lbqs;->n:Z

    move-object/from16 v0, v26

    iput-boolean v4, v0, Lzf;->x:Z

    .line 708
    move/from16 v0, v19

    move-object/from16 v1, v26

    iput v0, v1, Lzf;->y:I

    .line 709
    move/from16 v0, v18

    move-object/from16 v1, v26

    iput v0, v1, Lzf;->z:I

    .line 710
    move-object/from16 v0, v26

    iput-object v10, v0, Lzf;->A:Ljava/lang/String;

    .line 711
    move-object/from16 v0, p0

    iget-object v4, v0, Lbqs;->q:Ljava/lang/String;

    move-object/from16 v0, v26

    iput-object v4, v0, Lzf;->B:Ljava/lang/String;

    .line 712
    move-object/from16 v0, p0

    iget-wide v4, v0, Lbqs;->w:J

    move-object/from16 v0, v26

    iput-wide v4, v0, Lzf;->C:J

    .line 713
    move-object/from16 v0, p0

    iget v4, v0, Lbqs;->x:I

    move-object/from16 v0, v26

    iput v4, v0, Lzf;->D:I

    .line 714
    move-object/from16 v0, p0

    iget-wide v4, v0, Lbqs;->y:J

    move-object/from16 v0, v26

    iput-wide v4, v0, Lzf;->E:J

    .line 715
    move-object/from16 v0, p0

    iget-object v4, v0, Lbqs;->z:Ljava/lang/String;

    move-object/from16 v0, v26

    iput-object v4, v0, Lzf;->F:Ljava/lang/String;

    .line 716
    move-object/from16 v0, p0

    iget-object v4, v0, Lbqs;->B:Ljava/lang/String;

    move-object/from16 v0, v26

    iput-object v4, v0, Lzf;->G:Ljava/lang/String;

    .line 717
    move-object/from16 v0, p0

    iget-object v4, v0, Lbqs;->C:Ljava/lang/String;

    move-object/from16 v0, v26

    iput-object v4, v0, Lzf;->H:Ljava/lang/String;

    .line 718
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lbqs;->E:Z

    move-object/from16 v0, v26

    iput-boolean v4, v0, Lzf;->I:Z

    .line 719
    const/4 v4, -0x1

    move-object/from16 v0, v26

    iput v4, v0, Lzf;->J:I

    .line 720
    move-object/from16 v0, p0

    iget v4, v0, Lbqs;->F:I

    move-object/from16 v0, v26

    iput v4, v0, Lzf;->K:I

    .line 721
    move-object/from16 v0, p0

    iget-object v4, v0, Lbqs;->H:Ljava/lang/String;

    move-object/from16 v0, v26

    iput-object v4, v0, Lzf;->L:Ljava/lang/String;

    .line 722
    move-object/from16 v0, p0

    iget v4, v0, Lbqs;->I:I

    move-object/from16 v0, v26

    iput v4, v0, Lzf;->M:I

    .line 723
    move-object/from16 v0, p0

    iget v4, v0, Lbqs;->J:I

    move-object/from16 v0, v26

    iput v4, v0, Lzf;->N:I

    .line 724
    move/from16 v0, v17

    move-object/from16 v1, v26

    iput v0, v1, Lzf;->O:I

    .line 726
    if-nez v24, :cond_0

    .line 730
    if-eqz v22, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lbqs;->c:I

    const/16 v5, 0xa

    if-eq v4, v5, :cond_0

    if-nez v23, :cond_0

    .line 733
    if-eqz v25, :cond_a

    .line 734
    move-object/from16 v0, p0

    iget-wide v4, v0, Lbqs;->h:J

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lbnl;->b(J)V

    .line 740
    :cond_0
    :goto_5
    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lyt;->a(Lzf;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lbqs;->D:J

    .line 743
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lzh;

    .line 744
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lyt;->a(Lzh;)J

    goto :goto_6

    .line 605
    :cond_1
    const/16 v19, 0x0

    move/from16 v22, v19

    goto/16 :goto_0

    .line 623
    :cond_2
    instance-of v0, v3, Lbja;

    move/from16 v22, v0

    if-eqz v22, :cond_3

    .line 624
    check-cast v3, Lbja;

    .line 625
    iget-object v0, v3, Lbja;->i:Ljava/lang/String;

    move-object/from16 v21, v0

    .line 626
    iget v0, v3, Lbja;->j:I

    move/from16 v17, v0

    move-object v3, v8

    move/from16 v30, v9

    move-wide v8, v4

    move-object v5, v10

    move/from16 v4, v30

    move-object v10, v15

    move-object/from16 v31, v11

    move-object v11, v13

    move-object/from16 v13, v19

    move/from16 v19, v18

    move/from16 v18, v16

    move-object/from16 v16, v14

    move-wide v14, v6

    move-object/from16 v6, v31

    move-object v7, v12

    .line 627
    goto/16 :goto_1

    :cond_3
    instance-of v0, v3, Lbix;

    move/from16 v22, v0

    if-eqz v22, :cond_14

    .line 628
    check-cast v3, Lbix;

    .line 629
    iget-wide v5, v3, Lbix;->i:D

    .line 630
    iget-wide v3, v3, Lbix;->j:D

    :goto_7
    move-object v7, v12

    move/from16 v30, v9

    move-object/from16 v31, v10

    move-object v10, v15

    move-object/from16 v32, v11

    move-object v11, v13

    move-object/from16 v13, v19

    move/from16 v19, v18

    move/from16 v18, v16

    move-object/from16 v16, v14

    move-wide v14, v5

    move-object/from16 v6, v32

    move-object/from16 v5, v31

    move-wide/from16 v33, v3

    move-object v3, v8

    move/from16 v4, v30

    move-wide/from16 v8, v33

    .line 637
    goto/16 :goto_1

    :cond_4
    const/16 v19, 0x1

    move/from16 v0, v22

    move/from16 v1, v19

    if-le v0, v1, :cond_13

    .line 641
    const-string v15, "multipart/mixed"

    .line 642
    new-instance v8, Ljava/util/ArrayList;

    move/from16 v0, v22

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 645
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    .line 646
    const/4 v3, 0x0

    move/from16 v19, v3

    :goto_8
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_6

    .line 647
    if-lez v19, :cond_5

    .line 648
    const/16 v3, 0x7c

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 650
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lbqs;->m:Ljava/util/List;

    move/from16 v0, v19

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbiy;

    .line 651
    const/16 v25, 0x4

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    iget-object v0, v3, Lbiy;->f:Ljava/lang/String;

    move-object/from16 v27, v0

    aput-object v27, v25, v26

    const/16 v26, 0x1

    iget-object v0, v3, Lbiy;->h:Ljava/lang/String;

    move-object/from16 v27, v0

    aput-object v27, v25, v26

    const/16 v26, 0x2

    iget v0, v3, Lbiy;->k:I

    move/from16 v27, v0

    .line 653
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x3

    iget v0, v3, Lbiy;->l:I

    move/from16 v27, v0

    .line 654
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    .line 651
    invoke-static/range {v25 .. v25}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    new-instance v25, Lzh;

    invoke-direct/range {v25 .. v25}, Lzh;-><init>()V

    .line 658
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lzh;->a:Ljava/lang/String;

    .line 659
    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lzh;->b:Ljava/lang/String;

    .line 660
    iget-object v0, v3, Lbiy;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lzh;->c:Ljava/lang/String;

    .line 661
    iget-object v0, v3, Lbiy;->h:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lzh;->d:Ljava/lang/String;

    .line 662
    iget v0, v3, Lbiy;->k:I

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lzh;->e:I

    .line 663
    iget v3, v3, Lbiy;->l:I

    move-object/from16 v0, v25

    iput v3, v0, Lzh;->f:I

    .line 664
    move-object/from16 v0, v25

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    add-int/lit8 v3, v19, 0x1

    move/from16 v19, v3

    goto/16 :goto_8

    .line 666
    :cond_6
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move/from16 v19, v18

    move/from16 v18, v16

    move-object/from16 v16, v14

    move-object/from16 v30, v11

    move-object v11, v13

    move-object v13, v3

    move-object v3, v8

    move-wide/from16 v31, v6

    move-object/from16 v6, v30

    move-object v7, v12

    move-object/from16 v33, v10

    move-object v10, v15

    move-wide/from16 v14, v31

    move/from16 v34, v9

    move-wide v8, v4

    move/from16 v4, v34

    move-object/from16 v5, v33

    goto/16 :goto_1

    .line 671
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lbqs;->l:Ljava/util/List;

    .line 672
    invoke-static {v12, v7}, Lf;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 679
    :cond_8
    const/16 v22, 0x0

    goto/16 :goto_3

    .line 680
    :cond_9
    const/16 v23, 0x0

    goto/16 :goto_4

    .line 736
    :cond_a
    move-object/from16 v0, p0

    iget-wide v4, v0, Lbqs;->h:J

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lbnl;->a(J)V

    goto/16 :goto_5

    .line 747
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lbqs;->h:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lbqs;->i:J

    move-object/from16 v0, p0

    iget v9, v0, Lbqs;->A:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lbqs;->g:Lbdk;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lbqs;->D:J

    move-object/from16 v0, p0

    iget v0, v0, Lbqs;->F:I

    move/from16 v16, v0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v17}, Lyt;->a(Ljava/lang/String;JJILjava/lang/String;Lbdk;Ljava/lang/String;Ljava/lang/String;JII)Z

    .line 751
    invoke-virtual/range {p0 .. p1}, Lbqs;->a(Lyt;)V

    .line 753
    const-string v3, "babel_transport_events"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "babel_enable_clearcut_transport_events"

    const/4 v4, 0x0

    .line 756
    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 759
    :cond_c
    move-object/from16 v0, p0

    iget-wide v3, v0, Lbqs;->e:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v13, v3, v5

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Lyj;)J

    move-result-wide v4

    new-instance v15, Lyu;

    invoke-direct {v15}, Lyu;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lbnl;->e()I

    move-result v3

    iput v3, v15, Lyu;->a:I

    move/from16 v0, v22

    iput-boolean v0, v15, Lyu;->b:Z

    if-nez v23, :cond_f

    const/4 v3, 0x1

    :goto_9
    iput-boolean v3, v15, Lyu;->c:Z

    cmp-long v3, v13, v4

    if-gtz v3, :cond_10

    const/4 v3, 0x1

    :goto_a
    iput-boolean v3, v15, Lyu;->d:Z

    move/from16 v0, v25

    iput-boolean v0, v15, Lyu;->e:Z

    move-object/from16 v0, p0

    iget v3, v0, Lbqs;->d:I

    iput v3, v15, Lyu;->f:I

    move-object/from16 v0, p0

    iget v3, v0, Lbqs;->c:I

    iput v3, v15, Lyu;->g:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lbqs;->b:Ljava/lang/String;

    invoke-static {v3}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-wide v4, v0, Lbqs;->D:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lbqs;->e:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lbqs;->a:Ljava/lang/String;

    const/4 v11, 0x4

    const/16 v12, 0x65

    move-object/from16 v0, p0

    iget v0, v0, Lbqs;->K:I

    move/from16 v16, v0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v16}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbqs;->r:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_d

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->D:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->e:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    const/16 v25, 0x65

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->r:J

    move-wide/from16 v26, v0

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lbqs;->K:I

    move/from16 v29, v0

    move-object/from16 v16, p1

    move-wide/from16 v21, v8

    invoke-virtual/range {v16 .. v29}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    :cond_d
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbqs;->v:Z

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->r:J

    move-wide/from16 v17, v0

    const/16 v19, 0x1

    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->e:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-wide/from16 v24, v8

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->s:J

    move-wide/from16 v17, v0

    const/16 v19, 0xa

    const/16 v20, 0xce

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->e:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-wide/from16 v24, v8

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->t:J

    move-wide/from16 v17, v0

    const/16 v19, 0xa

    const/16 v20, 0xcf

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->e:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-wide/from16 v24, v8

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->u:J

    move-wide/from16 v17, v0

    const/16 v19, 0xa

    const/16 v20, 0x6b

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->e:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-wide/from16 v24, v8

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    const/16 v19, 0x4

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->e:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    move-wide/from16 v17, v13

    move-wide/from16 v24, v8

    move-object/from16 v27, v15

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 762
    :cond_e
    :goto_b
    return-void

    .line 759
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_9

    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_a

    :cond_11
    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->r:J

    move-wide/from16 v17, v0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lbqs;->K:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-wide/from16 v24, v8

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->t:J

    move-wide/from16 v17, v0

    const/16 v19, 0xa

    const/16 v20, 0xcc

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-wide/from16 v24, v8

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbqs;->u:J

    move-wide/from16 v17, v0

    const/16 v19, 0xa

    const/16 v20, 0x6c

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-wide/from16 v24, v8

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    const/16 v19, 0x4

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbqs;->f:Ljava/lang/String;

    move-object/from16 v26, v0

    move-wide/from16 v17, v13

    move-wide/from16 v24, v8

    move-object/from16 v27, v15

    invoke-static/range {v16 .. v27}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    goto/16 :goto_b

    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lbqs;->b:Ljava/lang/String;

    if-nez v3, :cond_e

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbqs;->r:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lbqs;->a:Ljava/lang/String;

    invoke-static {v3}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-wide v4, v0, Lbqs;->D:J

    const-wide/16 v6, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xb

    const/16 v12, 0x65

    move-object/from16 v0, p0

    iget-wide v13, v0, Lbqs;->r:J

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lbqs;->K:I

    move/from16 v16, v0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v16}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    goto/16 :goto_b

    :cond_13
    move/from16 v19, v18

    move/from16 v18, v16

    move-object/from16 v16, v14

    move-object/from16 v30, v11

    move-object v11, v13

    move-object v13, v15

    move-wide v14, v6

    move-object/from16 v6, v30

    move-object v7, v12

    move-object/from16 v31, v10

    move-object v10, v3

    move-object v3, v8

    move/from16 v32, v9

    move-wide v8, v4

    move-object/from16 v5, v31

    move/from16 v4, v32

    goto/16 :goto_1

    :cond_14
    move-wide/from16 v30, v4

    move-wide/from16 v3, v30

    move-wide/from16 v32, v6

    move-wide/from16 v5, v32

    goto/16 :goto_7
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 530
    iget-wide v0, p0, Lbqs;->D:J

    return-wide v0
.end method

.method public a(I)Lbqs;
    .locals 0

    .prologue
    .line 525
    iput p1, p0, Lbqs;->F:I

    .line 526
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbqs;
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Lbqs;->B:Ljava/lang/String;

    .line 516
    return-object p0
.end method

.method public a(Lyt;Lbnl;)V
    .locals 4

    .prologue
    .line 549
    invoke-virtual {p1}, Lyt;->a()V

    .line 550
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbqs;->u:J

    .line 552
    :try_start_0
    iget v0, p0, Lbqs;->o:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 556
    iget-object v0, p0, Lbqs;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lyt;->c(Ljava/lang/String;I)V

    .line 558
    :cond_0
    invoke-direct {p0, p1, p2}, Lbqs;->c(Lyt;Lbnl;)V

    .line 559
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 561
    invoke-virtual {p1}, Lyt;->c()V

    .line 564
    iget-object v0, p0, Lbqs;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 565
    return-void

    .line 561
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
.end method

.method public b(Ljava/lang/String;)Lbqs;
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lbqs;->C:Ljava/lang/String;

    .line 521
    return-object p0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 534
    const/4 v0, 0x2

    iput v0, p0, Lbqs;->k:I

    .line 535
    return-void
.end method

.method public b(Lyt;Lbnl;)V
    .locals 0

    .prologue
    .line 578
    invoke-direct {p0, p1, p2}, Lbqs;->c(Lyt;Lbnl;)V

    .line 579
    return-void
.end method
