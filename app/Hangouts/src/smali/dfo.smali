.class public final Ldfo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfo;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Boolean;

.field public d:Ldeh;

.field public e:Ljava/lang/Integer;

.field public f:Ldff;

.field public g:Ldin;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5979
    const/4 v0, 0x0

    new-array v0, v0, [Ldfo;

    sput-object v0, Ldfo;->a:[Ldfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5980
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5994
    iput-object v0, p0, Ldfo;->b:Ljava/lang/Integer;

    .line 5999
    iput-object v0, p0, Ldfo;->d:Ldeh;

    .line 6002
    iput-object v0, p0, Ldfo;->e:Ljava/lang/Integer;

    .line 6005
    iput-object v0, p0, Ldfo;->f:Ldff;

    .line 6008
    iput-object v0, p0, Ldfo;->g:Ldin;

    .line 5980
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 6037
    const/4 v0, 0x0

    .line 6038
    iget-object v1, p0, Ldfo;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 6039
    const/4 v0, 0x1

    iget-object v1, p0, Ldfo;->b:Ljava/lang/Integer;

    .line 6040
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6042
    :cond_0
    iget-object v1, p0, Ldfo;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 6043
    const/4 v1, 0x2

    iget-object v2, p0, Ldfo;->c:Ljava/lang/Boolean;

    .line 6044
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6046
    :cond_1
    iget-object v1, p0, Ldfo;->d:Ldeh;

    if-eqz v1, :cond_2

    .line 6047
    const/4 v1, 0x3

    iget-object v2, p0, Ldfo;->d:Ldeh;

    .line 6048
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6050
    :cond_2
    iget-object v1, p0, Ldfo;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 6051
    const/4 v1, 0x4

    iget-object v2, p0, Ldfo;->e:Ljava/lang/Integer;

    .line 6052
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6054
    :cond_3
    iget-object v1, p0, Ldfo;->f:Ldff;

    if-eqz v1, :cond_4

    .line 6055
    const/4 v1, 0x5

    iget-object v2, p0, Ldfo;->f:Ldff;

    .line 6056
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6058
    :cond_4
    iget-object v1, p0, Ldfo;->g:Ldin;

    if-eqz v1, :cond_5

    .line 6059
    const/4 v1, 0x6

    iget-object v2, p0, Ldfo;->g:Ldin;

    .line 6060
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6062
    :cond_5
    iget-object v1, p0, Ldfo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6063
    iput v0, p0, Ldfo;->cachedSize:I

    .line 6064
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5976
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldfo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldfo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldfo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfo;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfo;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldfo;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldfo;->d:Ldeh;

    if-nez v0, :cond_4

    new-instance v0, Ldeh;

    invoke-direct {v0}, Ldeh;-><init>()V

    iput-object v0, p0, Ldfo;->d:Ldeh;

    :cond_4
    iget-object v0, p0, Ldfo;->d:Ldeh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_5

    if-ne v0, v3, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfo;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfo;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldfo;->f:Ldff;

    if-nez v0, :cond_7

    new-instance v0, Ldff;

    invoke-direct {v0}, Ldff;-><init>()V

    iput-object v0, p0, Ldfo;->f:Ldff;

    :cond_7
    iget-object v0, p0, Ldfo;->f:Ldff;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldfo;->g:Ldin;

    if-nez v0, :cond_8

    new-instance v0, Ldin;

    invoke-direct {v0}, Ldin;-><init>()V

    iput-object v0, p0, Ldfo;->g:Ldin;

    :cond_8
    iget-object v0, p0, Ldfo;->g:Ldin;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 6013
    iget-object v0, p0, Ldfo;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 6014
    const/4 v0, 0x1

    iget-object v1, p0, Ldfo;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6016
    :cond_0
    iget-object v0, p0, Ldfo;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 6017
    const/4 v0, 0x2

    iget-object v1, p0, Ldfo;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6019
    :cond_1
    iget-object v0, p0, Ldfo;->d:Ldeh;

    if-eqz v0, :cond_2

    .line 6020
    const/4 v0, 0x3

    iget-object v1, p0, Ldfo;->d:Ldeh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6022
    :cond_2
    iget-object v0, p0, Ldfo;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 6023
    const/4 v0, 0x4

    iget-object v1, p0, Ldfo;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6025
    :cond_3
    iget-object v0, p0, Ldfo;->f:Ldff;

    if-eqz v0, :cond_4

    .line 6026
    const/4 v0, 0x5

    iget-object v1, p0, Ldfo;->f:Ldff;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6028
    :cond_4
    iget-object v0, p0, Ldfo;->g:Ldin;

    if-eqz v0, :cond_5

    .line 6029
    const/4 v0, 0x6

    iget-object v1, p0, Ldfo;->g:Ldin;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6031
    :cond_5
    iget-object v0, p0, Ldfo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6033
    return-void
.end method
