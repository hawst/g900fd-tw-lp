.class public final Layl;
.super Layf;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 30
    sget v0, Lf;->ev:I

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lg;->ae:I

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lg;->ad:I

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, Layf;-><init>(I[I)V

    .line 31
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x5

    return v0
.end method

.method protected a(I)V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Layl;->c:Landroid/app/Activity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 70
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "call_promo_shown"

    const/4 v2, 0x1

    .line 71
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 72
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 73
    sget v0, Lg;->ad:I

    if-ne p1, v0, :cond_0

    .line 74
    iget-object v0, p0, Layl;->c:Landroid/app/Activity;

    invoke-static {v0}, Lf;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Layl;->c:Landroid/app/Activity;

    iget-object v1, p0, Layl;->b:Lyj;

    invoke-static {v1}, Lbbl;->o(Lyj;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 87
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Layf;->a(I)V

    .line 88
    return-void

    .line 79
    :cond_1
    const-string v0, "babel_voip_pstn_calling_required_package_name"

    const-string v1, "com.google.android.apps.hangoutsdialer"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    iget-object v1, p0, Layl;->c:Landroid/app/Activity;

    invoke-static {v1, v0}, Lbbl;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    const-string v0, "Babel"

    const-string v1, "Unable to launch play store intent."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lyj;Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 60
    if-eqz p1, :cond_1

    const-string v2, "babel_voip_pstn_calling"

    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1, p2}, Layi;->b(Lyj;Landroid/app/Activity;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "fmf_promo_shown"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "call_promo_shown"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-lt v2, v3, :cond_2

    invoke-static {}, Lbzd;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p2}, Lf;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    invoke-virtual {p1}, Lyj;->ai()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p1}, Lf;->d(Lyj;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    if-nez v0, :cond_3

    .line 65
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 60
    goto :goto_0

    .line 64
    :cond_3
    invoke-super {p0, p1, p2}, Layf;->a(Lyj;Landroid/app/Activity;)V

    goto :goto_1
.end method

.method public b()Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 40
    invoke-super {p0}, Layf;->b()Landroid/view/View;

    move-result-object v1

    .line 44
    sget v0, Lg;->ad:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 45
    iget-object v2, p0, Layl;->c:Landroid/app/Activity;

    invoke-static {v2}, Lf;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 46
    sget v2, Lh;->Y:I

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 55
    :cond_0
    :goto_0
    return-object v1

    .line 47
    :cond_1
    const-string v2, "com.google.android.apps.hangouts.phone.recentcalls"

    iget-object v3, p0, Layl;->c:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    sget v2, Lg;->ae:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 49
    sget v2, Lg;->ac:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 50
    sget v2, Lh;->W:I

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 51
    sget v0, Lg;->af:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lh;->X:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
