.class public final Lewh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewh;


# instance fields
.field public b:Lexn;

.field public c:[Lewg;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Levt;

.field public g:Lexc;

.field public h:Lewb;

.field public i:Lewe;

.field public j:Lewi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 641
    const/4 v0, 0x0

    new-array v0, v0, [Lewh;

    sput-object v0, Lewh;->a:[Lewh;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 642
    invoke-direct {p0}, Lepn;-><init>()V

    .line 645
    iput-object v1, p0, Lewh;->b:Lexn;

    .line 648
    sget-object v0, Lewg;->a:[Lewg;

    iput-object v0, p0, Lewh;->c:[Lewg;

    .line 655
    iput-object v1, p0, Lewh;->f:Levt;

    .line 658
    iput-object v1, p0, Lewh;->g:Lexc;

    .line 661
    iput-object v1, p0, Lewh;->h:Lewb;

    .line 664
    iput-object v1, p0, Lewh;->i:Lewe;

    .line 667
    iput-object v1, p0, Lewh;->j:Lewi;

    .line 642
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 710
    iget-object v0, p0, Lewh;->b:Lexn;

    if-eqz v0, :cond_9

    .line 711
    const/4 v0, 0x1

    iget-object v2, p0, Lewh;->b:Lexn;

    .line 712
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 714
    :goto_0
    iget-object v2, p0, Lewh;->c:[Lewg;

    if-eqz v2, :cond_1

    .line 715
    iget-object v2, p0, Lewh;->c:[Lewg;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 716
    if-eqz v4, :cond_0

    .line 717
    const/4 v5, 0x2

    .line 718
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 715
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 722
    :cond_1
    iget-object v1, p0, Lewh;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 723
    const/4 v1, 0x3

    iget-object v2, p0, Lewh;->d:Ljava/lang/Integer;

    .line 724
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 726
    :cond_2
    iget-object v1, p0, Lewh;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 727
    const/4 v1, 0x4

    iget-object v2, p0, Lewh;->e:Ljava/lang/Integer;

    .line 728
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 730
    :cond_3
    iget-object v1, p0, Lewh;->f:Levt;

    if-eqz v1, :cond_4

    .line 731
    const/4 v1, 0x5

    iget-object v2, p0, Lewh;->f:Levt;

    .line 732
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 734
    :cond_4
    iget-object v1, p0, Lewh;->g:Lexc;

    if-eqz v1, :cond_5

    .line 735
    const/4 v1, 0x6

    iget-object v2, p0, Lewh;->g:Lexc;

    .line 736
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 738
    :cond_5
    iget-object v1, p0, Lewh;->h:Lewb;

    if-eqz v1, :cond_6

    .line 739
    const/4 v1, 0x7

    iget-object v2, p0, Lewh;->h:Lewb;

    .line 740
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    :cond_6
    iget-object v1, p0, Lewh;->i:Lewe;

    if-eqz v1, :cond_7

    .line 743
    const/16 v1, 0x3c

    iget-object v2, p0, Lewh;->i:Lewe;

    .line 744
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 746
    :cond_7
    iget-object v1, p0, Lewh;->j:Lewi;

    if-eqz v1, :cond_8

    .line 747
    const/16 v1, 0x3d

    iget-object v2, p0, Lewh;->j:Lewi;

    .line 748
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 750
    :cond_8
    iget-object v1, p0, Lewh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 751
    iput v0, p0, Lewh;->cachedSize:I

    .line 752
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 638
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lewh;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lewh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lewh;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lewh;->b:Lexn;

    if-nez v0, :cond_2

    new-instance v0, Lexn;

    invoke-direct {v0}, Lexn;-><init>()V

    iput-object v0, p0, Lewh;->b:Lexn;

    :cond_2
    iget-object v0, p0, Lewh;->b:Lexn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lewh;->c:[Lewg;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lewg;

    iget-object v3, p0, Lewh;->c:[Lewg;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lewh;->c:[Lewg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lewh;->c:[Lewg;

    :goto_2
    iget-object v2, p0, Lewh;->c:[Lewg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lewh;->c:[Lewg;

    new-instance v3, Lewg;

    invoke-direct {v3}, Lewg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewh;->c:[Lewg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lewh;->c:[Lewg;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lewh;->c:[Lewg;

    new-instance v3, Lewg;

    invoke-direct {v3}, Lewg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewh;->c:[Lewg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewh;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewh;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lewh;->f:Levt;

    if-nez v0, :cond_6

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lewh;->f:Levt;

    :cond_6
    iget-object v0, p0, Lewh;->f:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lewh;->g:Lexc;

    if-nez v0, :cond_7

    new-instance v0, Lexc;

    invoke-direct {v0}, Lexc;-><init>()V

    iput-object v0, p0, Lewh;->g:Lexc;

    :cond_7
    iget-object v0, p0, Lewh;->g:Lexc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lewh;->h:Lewb;

    if-nez v0, :cond_8

    new-instance v0, Lewb;

    invoke-direct {v0}, Lewb;-><init>()V

    iput-object v0, p0, Lewh;->h:Lewb;

    :cond_8
    iget-object v0, p0, Lewh;->h:Lewb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lewh;->i:Lewe;

    if-nez v0, :cond_9

    new-instance v0, Lewe;

    invoke-direct {v0}, Lewe;-><init>()V

    iput-object v0, p0, Lewh;->i:Lewe;

    :cond_9
    iget-object v0, p0, Lewh;->i:Lewe;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lewh;->j:Lewi;

    if-nez v0, :cond_a

    new-instance v0, Lewi;

    invoke-direct {v0}, Lewi;-><init>()V

    iput-object v0, p0, Lewh;->j:Lewi;

    :cond_a
    iget-object v0, p0, Lewh;->j:Lewi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x1e2 -> :sswitch_8
        0x1ea -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 672
    iget-object v0, p0, Lewh;->b:Lexn;

    if-eqz v0, :cond_0

    .line 673
    const/4 v0, 0x1

    iget-object v1, p0, Lewh;->b:Lexn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 675
    :cond_0
    iget-object v0, p0, Lewh;->c:[Lewg;

    if-eqz v0, :cond_2

    .line 676
    iget-object v1, p0, Lewh;->c:[Lewg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 677
    if-eqz v3, :cond_1

    .line 678
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 676
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 682
    :cond_2
    iget-object v0, p0, Lewh;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 683
    const/4 v0, 0x3

    iget-object v1, p0, Lewh;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 685
    :cond_3
    iget-object v0, p0, Lewh;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 686
    const/4 v0, 0x4

    iget-object v1, p0, Lewh;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 688
    :cond_4
    iget-object v0, p0, Lewh;->f:Levt;

    if-eqz v0, :cond_5

    .line 689
    const/4 v0, 0x5

    iget-object v1, p0, Lewh;->f:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 691
    :cond_5
    iget-object v0, p0, Lewh;->g:Lexc;

    if-eqz v0, :cond_6

    .line 692
    const/4 v0, 0x6

    iget-object v1, p0, Lewh;->g:Lexc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 694
    :cond_6
    iget-object v0, p0, Lewh;->h:Lewb;

    if-eqz v0, :cond_7

    .line 695
    const/4 v0, 0x7

    iget-object v1, p0, Lewh;->h:Lewb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 697
    :cond_7
    iget-object v0, p0, Lewh;->i:Lewe;

    if-eqz v0, :cond_8

    .line 698
    const/16 v0, 0x3c

    iget-object v1, p0, Lewh;->i:Lewe;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 700
    :cond_8
    iget-object v0, p0, Lewh;->j:Lewi;

    if-eqz v0, :cond_9

    .line 701
    const/16 v0, 0x3d

    iget-object v1, p0, Lewh;->j:Lewi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 703
    :cond_9
    iget-object v0, p0, Lewh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 705
    return-void
.end method
