.class public final Lchb;
.super Lt;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Law;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lt;",
        "Landroid/content/DialogInterface$OnCancelListener;",
        "Law",
        "<",
        "Lcft;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:I

.field private c:Lcft;

.field private final d:Landroid/os/Handler;

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lchd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lt;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lchb;->b:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lchb;->d:Landroid/os/Handler;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lchb;->e:Landroid/util/SparseArray;

    return-void
.end method

.method public static a(Ly;)Lchb;
    .locals 4

    const-string v0, "Must be called from main thread of process"

    invoke-static {v0}, Lg;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Ly;->e()Lae;

    move-result-object v1

    :try_start_0
    const-string v0, "GmsSupportLifecycleFragment"

    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Lchb;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lchb;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    new-instance v0, Lchb;

    invoke-direct {v0}, Lchb;-><init>()V

    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v2

    const-string v3, "GmsSupportLifecycleFragment"

    invoke-virtual {v2, v0, v3}, Lao;->a(Lt;Ljava/lang/String;)Lao;

    move-result-object v2

    invoke-virtual {v2}, Lao;->b()I

    invoke-virtual {v1}, Lae;->b()Z

    :cond_1
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Fragment with tag GmsSupportLifecycleFragment is not a SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lchb;->a:Z

    const/4 v1, -0x1

    iput v1, p0, Lchb;->b:I

    iput-object v4, p0, Lchb;->c:Lcft;

    invoke-virtual {p0}, Lchb;->getLoaderManager()Lav;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-direct {p0, v2}, Lchb;->b(I)Lchc;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lchc;->b()V

    :cond_0
    invoke-virtual {v1, v2, v4, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(ILcft;)V
    .locals 2

    iget-object v0, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchd;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lchb;->getLoaderManager()Lav;

    move-result-object v1

    invoke-virtual {v1, p1}, Lav;->a(I)V

    iget-object v1, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, v0, Lchd;->b:Lcfw;

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lcfw;->onConnectionFailed(Lcft;)V

    :cond_0
    invoke-direct {p0}, Lchb;->a()V

    return-void
.end method

.method static synthetic a(Lchb;)V
    .locals 0

    invoke-direct {p0}, Lchb;->a()V

    return-void
.end method

.method static synthetic a(Lchb;ILcft;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lchb;->a(ILcft;)V

    return-void
.end method

.method private b(I)Lchc;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lchb;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, p1}, Lav;->b(I)Ldg;

    move-result-object v0

    check-cast v0, Lchc;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown loader in SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public a(I)Lcgl;
    .locals 1

    invoke-virtual {p0}, Lchb;->getActivity()Ly;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lchb;->b(I)Lchc;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lchc;->a:Lcgl;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILcgl;Lcfw;)V
    .locals 4

    const/4 v1, 0x0

    const-string v0, "GoogleApiClient instance cannot be null"

    invoke-static {p2, v0}, Lg;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Already managing a GoogleApiClient with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lg;->a(ZLjava/lang/Object;)V

    new-instance v0, Lchd;

    invoke-direct {v0, p2, p3, v1}, Lchd;-><init>(Lcgl;Lcfw;B)V

    iget-object v1, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lchb;->getActivity()Ly;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lchb;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lchb;->a()V

    :goto_1
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lchb;->getActivity()Ly;

    move-result-object v2

    invoke-static {v2}, Lcfz;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :pswitch_1
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    goto :goto_0

    :cond_1
    iget v0, p0, Lchb;->b:I

    iget-object v1, p0, Lchb;->c:Lcft;

    invoke-direct {p0, v0, v1}, Lchb;->a(ILcft;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lt;->onAttach(Landroid/app/Activity;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-direct {p0, v2}, Lchb;->b(I)Lchc;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchd;

    iget-object v0, v0, Lchd;->a:Lcgl;

    iget-object v3, v3, Lchc;->a:Lcgl;

    if-eq v0, v3, :cond_0

    invoke-virtual {p0}, Lchb;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lchb;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget v0, p0, Lchb;->b:I

    iget-object v1, p0, Lchb;->c:Lcft;

    invoke-direct {p0, v0, v1}, Lchb;->a(ILcft;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lchb;->a:Z

    const-string v0, "failed_client_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lchb;->b:I

    iget v0, p0, Lchb;->b:I

    if-ltz v0, :cond_0

    new-instance v1, Lcft;

    const-string v0, "failed_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "failed_resolution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v0}, Lcft;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v1, p0, Lchb;->c:Lcft;

    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Lcft;",
            ">;"
        }
    .end annotation

    new-instance v1, Lchc;

    invoke-virtual {p0}, Lchb;->getActivity()Ly;

    move-result-object v2

    iget-object v0, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchd;

    iget-object v0, v0, Lchd;->a:Lcgl;

    invoke-direct {v1, v2, v0}, Lchc;-><init>(Landroid/content/Context;Lcgl;)V

    return-object v1
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcft;

    invoke-virtual {p2}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    iget v1, p0, Lchb;->b:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lchb;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    iget-boolean v1, p0, Lchb;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lchb;->a:Z

    iput v0, p0, Lchb;->b:I

    iput-object p2, p0, Lchb;->c:Lcft;

    iget-object v1, p0, Lchb;->d:Landroid/os/Handler;

    new-instance v2, Lche;

    invoke-direct {v2, p0, v0, p2}, Lche;-><init>(Lchb;ILcft;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onLoaderReset(Ldg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Lcft;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    iget v1, p0, Lchb;->b:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lchb;->a()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lt;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "resolving_error"

    iget-boolean v1, p0, Lchb;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v0, p0, Lchb;->b:I

    if-ltz v0, :cond_0

    const-string v0, "failed_client_id"

    iget v1, p0, Lchb;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_status"

    iget-object v1, p0, Lchb;->c:Lcft;

    invoke-virtual {v1}, Lcft;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_resolution"

    iget-object v1, p0, Lchb;->c:Lcft;

    invoke-virtual {v1}, Lcft;->d()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 4

    invoke-super {p0}, Lt;->onStart()V

    iget-boolean v0, p0, Lchb;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lchb;->getLoaderManager()Lav;

    move-result-object v1

    iget-object v2, p0, Lchb;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
