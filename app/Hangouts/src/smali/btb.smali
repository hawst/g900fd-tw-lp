.class public final Lbtb;
.super Lcvx;
.source "PG"


# instance fields
.field private b:Lcvx;

.field private final c:Ljava/lang/Object;

.field private d:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Lcvx;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcvx;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbtb;->c:Ljava/lang/Object;

    .line 35
    invoke-virtual {p1}, Lcvx;->e()Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 36
    iput-object p1, p0, Lbtb;->b:Lcvx;

    .line 37
    return-void
.end method

.method static synthetic a(J)[B
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 28
    new-array v1, v4, [B

    const/4 v0, 0x7

    :goto_0
    if-ltz v0, :cond_0

    const-wide/16 v2, 0xff

    and-long/2addr v2, p0

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    shr-long/2addr p0, v4

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private b(I)Lbtc;
    .locals 3

    .prologue
    .line 41
    iget-object v1, p0, Lbtb;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 42
    :try_start_0
    new-instance v2, Lbtc;

    iget-object v0, p0, Lbtb;->b:Lcvx;

    invoke-virtual {v0, p1}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    invoke-direct {v2, v0}, Lbtc;-><init>(Lcvw;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v2

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 48
    iget-object v1, p0, Lbtb;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 49
    :try_start_0
    iget-object v0, p0, Lbtb;->b:Lcvx;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Already released"

    iget-object v3, p0, Lbtb;->d:Ljava/lang/Throwable;

    invoke-direct {v0, v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 52
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbtb;->b:Lcvx;

    invoke-virtual {v0}, Lcvx;->a()I

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method public synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lbtb;->b(I)Lbtc;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Lcvx;->b()V

    .line 59
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "ThreadSafeAggregatedPersonBuffer released"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbtb;->d:Ljava/lang/Throwable;

    .line 60
    iget-object v1, p0, Lbtb;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    :try_start_0
    iget-object v0, p0, Lbtb;->b:Lcvx;

    invoke-virtual {v0}, Lcvx;->d()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lbtb;->b:Lcvx;

    .line 63
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
