.class public abstract Lwz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lxa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lwz;->a:Ljava/util/ArrayList;

    .line 200
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public abstract a(J)V
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public abstract a(Lwt;)V
.end method

.method public a(Lxa;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lwz;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lwz;->a:Ljava/util/ArrayList;

    .line 104
    :cond_0
    iget-object v0, p0, Lwz;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public b(Lxa;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lwz;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lwz;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 118
    iget-object v0, p0, Lwz;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lwz;->a:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lwz;->e()Lwz;

    move-result-object v0

    return-object v0
.end method

.method public abstract d()Z
.end method

.method public e()Lwz;
    .locals 6

    .prologue
    .line 148
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    .line 149
    iget-object v1, p0, Lwz;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 150
    iget-object v2, p0, Lwz;->a:Ljava/util/ArrayList;

    .line 151
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lwz;->a:Ljava/util/ArrayList;

    .line 152
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 153
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 154
    iget-object v4, v0, Lwz;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 157
    :cond_0
    return-object v0
.end method
