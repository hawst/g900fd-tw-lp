.class public final Leup;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leup;


# instance fields
.field public b:Lexn;

.field public c:[Leuk;

.field public d:[Levt;

.field public e:Levt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    new-array v0, v0, [Leup;

    sput-object v0, Leup;->a:[Leup;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Lepn;-><init>()V

    .line 94
    iput-object v1, p0, Leup;->b:Lexn;

    .line 97
    sget-object v0, Leuk;->a:[Leuk;

    iput-object v0, p0, Leup;->c:[Leuk;

    .line 100
    sget-object v0, Levt;->a:[Levt;

    iput-object v0, p0, Leup;->d:[Levt;

    .line 103
    iput-object v1, p0, Leup;->e:Levt;

    .line 91
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-object v0, p0, Leup;->b:Lexn;

    if-eqz v0, :cond_5

    .line 136
    const/4 v0, 0x1

    iget-object v2, p0, Leup;->b:Lexn;

    .line 137
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 139
    :goto_0
    iget-object v2, p0, Leup;->c:[Leuk;

    if-eqz v2, :cond_1

    .line 140
    iget-object v3, p0, Leup;->c:[Leuk;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 141
    if-eqz v5, :cond_0

    .line 142
    const/4 v6, 0x2

    .line 143
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 140
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 147
    :cond_1
    iget-object v2, p0, Leup;->d:[Levt;

    if-eqz v2, :cond_3

    .line 148
    iget-object v2, p0, Leup;->d:[Levt;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 149
    if-eqz v4, :cond_2

    .line 150
    const/4 v5, 0x3

    .line 151
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 148
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 155
    :cond_3
    iget-object v1, p0, Leup;->e:Levt;

    if-eqz v1, :cond_4

    .line 156
    const/4 v1, 0x4

    iget-object v2, p0, Leup;->e:Levt;

    .line 157
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_4
    iget-object v1, p0, Leup;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    iput v0, p0, Leup;->cachedSize:I

    .line 161
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 87
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leup;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leup;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leup;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leup;->b:Lexn;

    if-nez v0, :cond_2

    new-instance v0, Lexn;

    invoke-direct {v0}, Lexn;-><init>()V

    iput-object v0, p0, Leup;->b:Lexn;

    :cond_2
    iget-object v0, p0, Leup;->b:Lexn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leup;->c:[Leuk;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leuk;

    iget-object v3, p0, Leup;->c:[Leuk;

    if-eqz v3, :cond_3

    iget-object v3, p0, Leup;->c:[Leuk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Leup;->c:[Leuk;

    :goto_2
    iget-object v2, p0, Leup;->c:[Leuk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Leup;->c:[Leuk;

    new-instance v3, Leuk;

    invoke-direct {v3}, Leuk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leup;->c:[Leuk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Leup;->c:[Leuk;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Leup;->c:[Leuk;

    new-instance v3, Leuk;

    invoke-direct {v3}, Leuk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leup;->c:[Leuk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leup;->d:[Levt;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Levt;

    iget-object v3, p0, Leup;->d:[Levt;

    if-eqz v3, :cond_6

    iget-object v3, p0, Leup;->d:[Levt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Leup;->d:[Levt;

    :goto_4
    iget-object v2, p0, Leup;->d:[Levt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Leup;->d:[Levt;

    new-instance v3, Levt;

    invoke-direct {v3}, Levt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leup;->d:[Levt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Leup;->d:[Levt;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Leup;->d:[Levt;

    new-instance v3, Levt;

    invoke-direct {v3}, Levt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leup;->d:[Levt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Leup;->e:Levt;

    if-nez v0, :cond_9

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Leup;->e:Levt;

    :cond_9
    iget-object v0, p0, Leup;->e:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 108
    iget-object v1, p0, Leup;->b:Lexn;

    if-eqz v1, :cond_0

    .line 109
    const/4 v1, 0x1

    iget-object v2, p0, Leup;->b:Lexn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 111
    :cond_0
    iget-object v1, p0, Leup;->c:[Leuk;

    if-eqz v1, :cond_2

    .line 112
    iget-object v2, p0, Leup;->c:[Leuk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 113
    if-eqz v4, :cond_1

    .line 114
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 112
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 118
    :cond_2
    iget-object v1, p0, Leup;->d:[Levt;

    if-eqz v1, :cond_4

    .line 119
    iget-object v1, p0, Leup;->d:[Levt;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 120
    if-eqz v3, :cond_3

    .line 121
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 119
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 125
    :cond_4
    iget-object v0, p0, Leup;->e:Levt;

    if-eqz v0, :cond_5

    .line 126
    const/4 v0, 0x4

    iget-object v1, p0, Leup;->e:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 128
    :cond_5
    iget-object v0, p0, Leup;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 130
    return-void
.end method
