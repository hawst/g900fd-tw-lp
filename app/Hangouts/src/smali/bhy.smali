.class public Lbhy;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldvx;)V
    .locals 3

    .prologue
    .line 4433
    iget-object v0, p1, Ldvx;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 4434
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 4435
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SendOffnetworkInvitationResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4438
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4442
    new-instance v0, Ldvx;

    invoke-direct {v0}, Ldvx;-><init>()V

    .line 4443
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldvx;

    .line 4444
    iget-object v1, v0, Ldvx;->b:Ldvn;

    invoke-static {v1}, Lbhy;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4445
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldvx;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 4447
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhy;

    invoke-direct {v1, v0}, Lbhy;-><init>(Ldvx;)V

    move-object v0, v1

    goto :goto_0
.end method
