.class public Lbhl;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbju;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ldva;)V
    .locals 6

    .prologue
    .line 3430
    iget-object v0, p1, Ldva;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 3431
    iget-object v1, p1, Ldva;->c:[Lduw;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    iget-object v5, v4, Lduw;->b:Ldui;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lduw;->b:Ldui;

    iget-object v5, v5, Ldui;->c:Ljava/lang/String;

    if-nez v5, :cond_1

    :cond_0
    const-string v4, "Babel"

    const-string v5, "Received empty gaiaid in parseClientUserPresenceList."

    invoke-static {v4, v5}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v5, Lbju;

    invoke-direct {v5, v4}, Lbju;-><init>(Lduw;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iput-object v2, p0, Lbhl;->g:Ljava/util/List;

    .line 3433
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_3

    .line 3434
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QueryPresenceResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3436
    :cond_3
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3453
    new-instance v0, Ldva;

    invoke-direct {v0}, Ldva;-><init>()V

    .line 3454
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldva;

    .line 3455
    iget-object v1, v0, Ldva;->b:Ldvn;

    invoke-static {v1}, Lbhl;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3456
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldva;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3458
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhl;

    invoke-direct {v1, v0}, Lbhl;-><init>(Ldva;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lbea;)V
    .locals 5

    .prologue
    .line 3440
    invoke-super {p0, p1}, Lbfz;->a(Lbea;)V

    .line 3442
    check-cast p1, Lbet;

    .line 3443
    iget-object v0, p1, Lbet;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 3444
    iget-object v0, p0, Lbhl;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbhl;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 3445
    :goto_0
    if-eq v1, v0, :cond_0

    .line 3446
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Queried presence for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", but only get response for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 3449
    :cond_0
    return-void

    .line 3444
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbju;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3463
    iget-object v0, p0, Lbhl;->g:Ljava/util/List;

    return-object v0
.end method
