.class public final Ldsq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsq;


# instance fields
.field public b:Ldvn;

.field public c:Ldro;

.field public d:Ljava/lang/Boolean;

.field public e:[Ldpu;

.field public f:Ldrg;

.field public g:Ldqx;

.field public h:Ldqy;

.field public i:Lduk;

.field public j:[Ldpw;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/Boolean;

.field public m:Ldqz;

.field public n:Ldvq;

.field public o:Ljava/lang/Boolean;

.field public p:Ljava/lang/Boolean;

.field public q:Ljava/lang/Integer;

.field public r:[Ldui;

.field public s:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17431
    const/4 v0, 0x0

    new-array v0, v0, [Ldsq;

    sput-object v0, Ldsq;->a:[Ldsq;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17432
    invoke-direct {p0}, Lepn;-><init>()V

    .line 17442
    iput-object v1, p0, Ldsq;->b:Ldvn;

    .line 17445
    iput-object v1, p0, Ldsq;->c:Ldro;

    .line 17450
    sget-object v0, Ldpu;->a:[Ldpu;

    iput-object v0, p0, Ldsq;->e:[Ldpu;

    .line 17453
    iput-object v1, p0, Ldsq;->f:Ldrg;

    .line 17456
    iput-object v1, p0, Ldsq;->g:Ldqx;

    .line 17459
    iput-object v1, p0, Ldsq;->h:Ldqy;

    .line 17462
    iput-object v1, p0, Ldsq;->i:Lduk;

    .line 17465
    sget-object v0, Ldpw;->a:[Ldpw;

    iput-object v0, p0, Ldsq;->j:[Ldpw;

    .line 17472
    iput-object v1, p0, Ldsq;->m:Ldqz;

    .line 17475
    iput-object v1, p0, Ldsq;->n:Ldvq;

    .line 17482
    iput-object v1, p0, Ldsq;->q:Ljava/lang/Integer;

    .line 17485
    sget-object v0, Ldui;->a:[Ldui;

    iput-object v0, p0, Ldsq;->r:[Ldui;

    .line 17488
    iput-object v1, p0, Ldsq;->s:Ljava/lang/Integer;

    .line 17432
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 17566
    iget-object v0, p0, Ldsq;->b:Ldvn;

    if-eqz v0, :cond_14

    .line 17567
    const/4 v0, 0x1

    iget-object v2, p0, Ldsq;->b:Ldvn;

    .line 17568
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17570
    :goto_0
    iget-object v2, p0, Ldsq;->c:Ldro;

    if-eqz v2, :cond_0

    .line 17571
    const/4 v2, 0x2

    iget-object v3, p0, Ldsq;->c:Ldro;

    .line 17572
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17574
    :cond_0
    iget-object v2, p0, Ldsq;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 17575
    const/4 v2, 0x3

    iget-object v3, p0, Ldsq;->d:Ljava/lang/Boolean;

    .line 17576
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 17578
    :cond_1
    iget-object v2, p0, Ldsq;->e:[Ldpu;

    if-eqz v2, :cond_3

    .line 17579
    iget-object v3, p0, Ldsq;->e:[Ldpu;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 17580
    if-eqz v5, :cond_2

    .line 17581
    const/4 v6, 0x4

    .line 17582
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 17579
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 17586
    :cond_3
    iget-object v2, p0, Ldsq;->f:Ldrg;

    if-eqz v2, :cond_4

    .line 17587
    const/4 v2, 0x5

    iget-object v3, p0, Ldsq;->f:Ldrg;

    .line 17588
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17590
    :cond_4
    iget-object v2, p0, Ldsq;->g:Ldqx;

    if-eqz v2, :cond_5

    .line 17591
    const/4 v2, 0x6

    iget-object v3, p0, Ldsq;->g:Ldqx;

    .line 17592
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17594
    :cond_5
    iget-object v2, p0, Ldsq;->i:Lduk;

    if-eqz v2, :cond_6

    .line 17595
    const/4 v2, 0x7

    iget-object v3, p0, Ldsq;->i:Lduk;

    .line 17596
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17598
    :cond_6
    iget-object v2, p0, Ldsq;->j:[Ldpw;

    if-eqz v2, :cond_8

    .line 17599
    iget-object v3, p0, Ldsq;->j:[Ldpw;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_8

    aget-object v5, v3, v2

    .line 17600
    if-eqz v5, :cond_7

    .line 17601
    const/16 v6, 0x8

    .line 17602
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 17599
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 17606
    :cond_8
    iget-object v2, p0, Ldsq;->h:Ldqy;

    if-eqz v2, :cond_9

    .line 17607
    const/16 v2, 0x9

    iget-object v3, p0, Ldsq;->h:Ldqy;

    .line 17608
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17610
    :cond_9
    iget-object v2, p0, Ldsq;->k:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    .line 17611
    const/16 v2, 0xa

    iget-object v3, p0, Ldsq;->k:Ljava/lang/Boolean;

    .line 17612
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 17614
    :cond_a
    iget-object v2, p0, Ldsq;->m:Ldqz;

    if-eqz v2, :cond_b

    .line 17615
    const/16 v2, 0xb

    iget-object v3, p0, Ldsq;->m:Ldqz;

    .line 17616
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17618
    :cond_b
    iget-object v2, p0, Ldsq;->n:Ldvq;

    if-eqz v2, :cond_c

    .line 17619
    const/16 v2, 0xc

    iget-object v3, p0, Ldsq;->n:Ldvq;

    .line 17620
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17622
    :cond_c
    iget-object v2, p0, Ldsq;->l:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    .line 17623
    const/16 v2, 0xd

    iget-object v3, p0, Ldsq;->l:Ljava/lang/Boolean;

    .line 17624
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 17626
    :cond_d
    iget-object v2, p0, Ldsq;->o:Ljava/lang/Boolean;

    if-eqz v2, :cond_e

    .line 17627
    const/16 v2, 0xe

    iget-object v3, p0, Ldsq;->o:Ljava/lang/Boolean;

    .line 17628
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 17630
    :cond_e
    iget-object v2, p0, Ldsq;->p:Ljava/lang/Boolean;

    if-eqz v2, :cond_f

    .line 17631
    const/16 v2, 0xf

    iget-object v3, p0, Ldsq;->p:Ljava/lang/Boolean;

    .line 17632
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 17634
    :cond_f
    iget-object v2, p0, Ldsq;->q:Ljava/lang/Integer;

    if-eqz v2, :cond_10

    .line 17635
    const/16 v2, 0x10

    iget-object v3, p0, Ldsq;->q:Ljava/lang/Integer;

    .line 17636
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 17638
    :cond_10
    iget-object v2, p0, Ldsq;->r:[Ldui;

    if-eqz v2, :cond_12

    .line 17639
    iget-object v2, p0, Ldsq;->r:[Ldui;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_12

    aget-object v4, v2, v1

    .line 17640
    if-eqz v4, :cond_11

    .line 17641
    const/16 v5, 0x11

    .line 17642
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 17639
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 17646
    :cond_12
    iget-object v1, p0, Ldsq;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    .line 17647
    const/16 v1, 0x12

    iget-object v2, p0, Ldsq;->s:Ljava/lang/Integer;

    .line 17648
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17650
    :cond_13
    iget-object v1, p0, Ldsq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17651
    iput v0, p0, Ldsq;->cachedSize:I

    .line 17652
    return v0

    :cond_14
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 17428
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldsq;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldsq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldsq;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsq;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldsq;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldsq;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldsq;->c:Ldro;

    if-nez v0, :cond_3

    new-instance v0, Ldro;

    invoke-direct {v0}, Ldro;-><init>()V

    iput-object v0, p0, Ldsq;->c:Ldro;

    :cond_3
    iget-object v0, p0, Ldsq;->c:Ldro;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsq;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsq;->e:[Ldpu;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldpu;

    iget-object v3, p0, Ldsq;->e:[Ldpu;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldsq;->e:[Ldpu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldsq;->e:[Ldpu;

    :goto_2
    iget-object v2, p0, Ldsq;->e:[Ldpu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldsq;->e:[Ldpu;

    new-instance v3, Ldpu;

    invoke-direct {v3}, Ldpu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsq;->e:[Ldpu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldsq;->e:[Ldpu;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldsq;->e:[Ldpu;

    new-instance v3, Ldpu;

    invoke-direct {v3}, Ldpu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsq;->e:[Ldpu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldsq;->f:Ldrg;

    if-nez v0, :cond_7

    new-instance v0, Ldrg;

    invoke-direct {v0}, Ldrg;-><init>()V

    iput-object v0, p0, Ldsq;->f:Ldrg;

    :cond_7
    iget-object v0, p0, Ldsq;->f:Ldrg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldsq;->g:Ldqx;

    if-nez v0, :cond_8

    new-instance v0, Ldqx;

    invoke-direct {v0}, Ldqx;-><init>()V

    iput-object v0, p0, Ldsq;->g:Ldqx;

    :cond_8
    iget-object v0, p0, Ldsq;->g:Ldqx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ldsq;->i:Lduk;

    if-nez v0, :cond_9

    new-instance v0, Lduk;

    invoke-direct {v0}, Lduk;-><init>()V

    iput-object v0, p0, Ldsq;->i:Lduk;

    :cond_9
    iget-object v0, p0, Ldsq;->i:Lduk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsq;->j:[Ldpw;

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldpw;

    iget-object v3, p0, Ldsq;->j:[Ldpw;

    if-eqz v3, :cond_a

    iget-object v3, p0, Ldsq;->j:[Ldpw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Ldsq;->j:[Ldpw;

    :goto_4
    iget-object v2, p0, Ldsq;->j:[Ldpw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Ldsq;->j:[Ldpw;

    new-instance v3, Ldpw;

    invoke-direct {v3}, Ldpw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsq;->j:[Ldpw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    iget-object v0, p0, Ldsq;->j:[Ldpw;

    array-length v0, v0

    goto :goto_3

    :cond_c
    iget-object v2, p0, Ldsq;->j:[Ldpw;

    new-instance v3, Ldpw;

    invoke-direct {v3}, Ldpw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsq;->j:[Ldpw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldsq;->h:Ldqy;

    if-nez v0, :cond_d

    new-instance v0, Ldqy;

    invoke-direct {v0}, Ldqy;-><init>()V

    iput-object v0, p0, Ldsq;->h:Ldqy;

    :cond_d
    iget-object v0, p0, Ldsq;->h:Ldqy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsq;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldsq;->m:Ldqz;

    if-nez v0, :cond_e

    new-instance v0, Ldqz;

    invoke-direct {v0}, Ldqz;-><init>()V

    iput-object v0, p0, Ldsq;->m:Ldqz;

    :cond_e
    iget-object v0, p0, Ldsq;->m:Ldqz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ldsq;->n:Ldvq;

    if-nez v0, :cond_f

    new-instance v0, Ldvq;

    invoke-direct {v0}, Ldvq;-><init>()V

    iput-object v0, p0, Ldsq;->n:Ldvq;

    :cond_f
    iget-object v0, p0, Ldsq;->n:Ldvq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsq;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsq;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsq;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_10

    if-eq v0, v4, :cond_10

    if-ne v0, v5, :cond_11

    :cond_10
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsq;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsq;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_11
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsq;->r:[Ldui;

    if-nez v0, :cond_13

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ldui;

    iget-object v3, p0, Ldsq;->r:[Ldui;

    if-eqz v3, :cond_12

    iget-object v3, p0, Ldsq;->r:[Ldui;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    iput-object v2, p0, Ldsq;->r:[Ldui;

    :goto_6
    iget-object v2, p0, Ldsq;->r:[Ldui;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_14

    iget-object v2, p0, Ldsq;->r:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsq;->r:[Ldui;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_13
    iget-object v0, p0, Ldsq;->r:[Ldui;

    array-length v0, v0

    goto :goto_5

    :cond_14
    iget-object v2, p0, Ldsq;->r:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsq;->r:[Ldui;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_15

    if-eq v0, v4, :cond_15

    if-eq v0, v5, :cond_15

    const/4 v2, 0x3

    if-ne v0, v2, :cond_16

    :cond_15
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsq;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_16
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsq;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 17493
    iget-object v1, p0, Ldsq;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 17494
    const/4 v1, 0x1

    iget-object v2, p0, Ldsq;->b:Ldvn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17496
    :cond_0
    iget-object v1, p0, Ldsq;->c:Ldro;

    if-eqz v1, :cond_1

    .line 17497
    const/4 v1, 0x2

    iget-object v2, p0, Ldsq;->c:Ldro;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17499
    :cond_1
    iget-object v1, p0, Ldsq;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 17500
    const/4 v1, 0x3

    iget-object v2, p0, Ldsq;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 17502
    :cond_2
    iget-object v1, p0, Ldsq;->e:[Ldpu;

    if-eqz v1, :cond_4

    .line 17503
    iget-object v2, p0, Ldsq;->e:[Ldpu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 17504
    if-eqz v4, :cond_3

    .line 17505
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 17503
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 17509
    :cond_4
    iget-object v1, p0, Ldsq;->f:Ldrg;

    if-eqz v1, :cond_5

    .line 17510
    const/4 v1, 0x5

    iget-object v2, p0, Ldsq;->f:Ldrg;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17512
    :cond_5
    iget-object v1, p0, Ldsq;->g:Ldqx;

    if-eqz v1, :cond_6

    .line 17513
    const/4 v1, 0x6

    iget-object v2, p0, Ldsq;->g:Ldqx;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17515
    :cond_6
    iget-object v1, p0, Ldsq;->i:Lduk;

    if-eqz v1, :cond_7

    .line 17516
    const/4 v1, 0x7

    iget-object v2, p0, Ldsq;->i:Lduk;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17518
    :cond_7
    iget-object v1, p0, Ldsq;->j:[Ldpw;

    if-eqz v1, :cond_9

    .line 17519
    iget-object v2, p0, Ldsq;->j:[Ldpw;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 17520
    if-eqz v4, :cond_8

    .line 17521
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 17519
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 17525
    :cond_9
    iget-object v1, p0, Ldsq;->h:Ldqy;

    if-eqz v1, :cond_a

    .line 17526
    const/16 v1, 0x9

    iget-object v2, p0, Ldsq;->h:Ldqy;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17528
    :cond_a
    iget-object v1, p0, Ldsq;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 17529
    const/16 v1, 0xa

    iget-object v2, p0, Ldsq;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 17531
    :cond_b
    iget-object v1, p0, Ldsq;->m:Ldqz;

    if-eqz v1, :cond_c

    .line 17532
    const/16 v1, 0xb

    iget-object v2, p0, Ldsq;->m:Ldqz;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17534
    :cond_c
    iget-object v1, p0, Ldsq;->n:Ldvq;

    if-eqz v1, :cond_d

    .line 17535
    const/16 v1, 0xc

    iget-object v2, p0, Ldsq;->n:Ldvq;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17537
    :cond_d
    iget-object v1, p0, Ldsq;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 17538
    const/16 v1, 0xd

    iget-object v2, p0, Ldsq;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 17540
    :cond_e
    iget-object v1, p0, Ldsq;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 17541
    const/16 v1, 0xe

    iget-object v2, p0, Ldsq;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 17543
    :cond_f
    iget-object v1, p0, Ldsq;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    .line 17544
    const/16 v1, 0xf

    iget-object v2, p0, Ldsq;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 17546
    :cond_10
    iget-object v1, p0, Ldsq;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 17547
    const/16 v1, 0x10

    iget-object v2, p0, Ldsq;->q:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 17549
    :cond_11
    iget-object v1, p0, Ldsq;->r:[Ldui;

    if-eqz v1, :cond_13

    .line 17550
    iget-object v1, p0, Ldsq;->r:[Ldui;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_13

    aget-object v3, v1, v0

    .line 17551
    if-eqz v3, :cond_12

    .line 17552
    const/16 v4, 0x11

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 17550
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 17556
    :cond_13
    iget-object v0, p0, Ldsq;->s:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    .line 17557
    const/16 v0, 0x12

    iget-object v1, p0, Ldsq;->s:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 17559
    :cond_14
    iget-object v0, p0, Ldsq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 17561
    return-void
.end method
