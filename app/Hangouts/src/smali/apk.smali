.class public final Lapk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static a:Lapk;


# instance fields
.field final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lapo;",
            ">;"
        }
    .end annotation
.end field

.field c:Lapx;

.field private final d:Landroid/telephony/PhoneStateListener;


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lapk;->b:Ljava/util/LinkedList;

    .line 234
    new-instance v0, Lapl;

    invoke-direct {v0, p0}, Lapl;-><init>(Lapk;)V

    iput-object v0, p0, Lapk;->d:Landroid/telephony/PhoneStateListener;

    .line 253
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    new-instance v1, Lapn;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lapn;-><init>(Lapk;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->addListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V

    .line 254
    return-void
.end method

.method static synthetic a(Lapk;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 65
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->t()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    const-string v0, "conversation"

    iget-object v1, p0, Lapk;->c:Lapx;

    invoke-virtual {v1}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKeyType()Ljava/lang/String;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/google/android/libraries/hangouts/video/VideoChat;->inviteConversationParticipants(IZ)V

    :goto_0
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->x()V

    return-void

    :cond_0
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->r()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lapk;->c:Lapx;

    invoke-virtual {v1}, Lapx;->s()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lapk;->b(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 257
    sget-object v0, Lapk;->a:Lapk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lyj;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 328
    sget-object v0, Lapk;->a:Lapk;

    if-eqz v0, :cond_0

    sget-object v0, Lapk;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 329
    sget-object v0, Lapk;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    .line 330
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    const/4 v0, 0x1

    .line 336
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Lapk;
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lapk;->a:Lapk;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 273
    sget-object v0, Lapk;->a:Lapk;

    return-object v0
.end method

.method private static b(Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 362
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v5

    .line 363
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 364
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 365
    iget-object v4, v0, Lbdh;->b:Lbdk;

    iget-object v4, v4, Lbdk;->a:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 366
    iget-object v0, v0, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 367
    :cond_1
    iget v4, v0, Lbdh;->a:I

    const/4 v6, 0x3

    if-ne v4, v6, :cond_0

    .line 368
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v4

    iget-object v6, v0, Lbdh;->q:Ljava/lang/String;

    .line 369
    invoke-virtual {v0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 368
    invoke-virtual {v4, v6, v0, v5}, Lcom/google/android/libraries/hangouts/video/VideoChat;->invitePstn(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 373
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 387
    :goto_2
    return-void

    .line 377
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 378
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 380
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    move v4, v5

    .line 381
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 382
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxo;

    invoke-virtual {v0}, Lxo;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 381
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    .line 385
    :cond_4
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    const/4 v6, 0x0

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/VideoChat;->inviteUsers([Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 774
    iget-object v0, p0, Lapk;->c:Lapx;

    if-nez v0, :cond_0

    .line 775
    const-string v0, "Babel"

    const-string v1, "Got Mesi push notification with no joined call"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    :goto_0
    return-void

    .line 777
    :cond_0
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->T()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lapm;

    invoke-direct {v1, p0, p1}, Lapm;-><init>(Lapk;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public a(Lapo;)V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lapk;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 278
    return-void
.end method

.method a(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 3

    .prologue
    .line 727
    iget-object v0, p0, Lapk;->c:Lapx;

    if-nez v0, :cond_0

    .line 747
    :goto_0
    return-void

    .line 731
    :cond_0
    invoke-virtual {p0}, Lapk;->j()V

    .line 732
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 733
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lapk;->d:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 736
    :cond_1
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1}, Lapx;->a(Lcom/google/android/libraries/hangouts/video/CallState;)V

    .line 738
    invoke-virtual {p0}, Lapk;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 739
    iget-object v2, p0, Lapk;->c:Lapx;

    invoke-virtual {v0, v2}, Lapo;->a(Lapx;)V

    goto :goto_1

    .line 742
    :cond_2
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->sendCallCompleteIntent()V

    .line 743
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 744
    sget v0, Lf;->hL:I

    invoke-static {v0}, Lf;->f(I)V

    .line 746
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lapk;->c:Lapx;

    goto :goto_0
.end method

.method public a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Lbdh;IIJ)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/HangoutRequest;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Lbdh;",
            "IIJ)V"
        }
    .end annotation

    .prologue
    .line 343
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 345
    new-instance v0, Lapx;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-wide/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lapx;-><init>(Lcom/google/android/libraries/hangouts/video/HangoutRequest;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Lbdh;IIJ)V

    iput-object v0, p0, Lapk;->c:Lapx;

    .line 348
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->b()V

    .line 349
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    .line 350
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 351
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 354
    iget-object v0, p0, Lapk;->c:Lapx;

    const/16 v1, 0x3f5

    invoke-virtual {v0, v1}, Lapx;->c(I)V

    .line 356
    :cond_0
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lapk;->d:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 359
    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 405
    iget-object v0, p0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    invoke-static {p1, p2}, Lapk;->b(Ljava/util/List;Ljava/util/List;)V

    .line 407
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1, p2}, Lapx;->a(Ljava/util/List;Ljava/util/List;)V

    .line 409
    :cond_0
    return-void
.end method

.method public b(Lapo;)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lapk;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 282
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 283
    return-void
.end method

.method public c()Lapx;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lapk;->c:Lapx;

    return-object v0
.end method

.method public d()Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lapk;->c:Lapx;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->D()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;
    .locals 3

    .prologue
    .line 311
    invoke-virtual {p0}, Lapk;->d()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    .line 313
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    .line 314
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 315
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 316
    instance-of v1, v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    if-eqz v1, :cond_0

    .line 317
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 321
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lapo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 717
    iget-object v0, p0, Lapk;->b:Ljava/util/LinkedList;

    .line 718
    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 719
    return-object v0
.end method

.method j()V
    .locals 1

    .prologue
    .line 762
    iget-object v0, p0, Lapk;->c:Lapx;

    if-nez v0, :cond_0

    .line 770
    :goto_0
    return-void

    .line 766
    :cond_0
    iget-object v0, p0, Lapk;->c:Lapx;

    .line 767
    invoke-virtual {v0}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 766
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 769
    invoke-static {v0}, Lyp;->a(Lyj;)V

    goto :goto_0
.end method
