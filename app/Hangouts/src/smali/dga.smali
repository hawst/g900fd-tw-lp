.class public final Ldga;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldga;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ldej;

.field public e:Ldek;

.field public f:Ldgk;

.field public g:Ldfo;

.field public h:Ljava/lang/String;

.field public i:Ldfz;

.field public j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7735
    const/4 v0, 0x0

    new-array v0, v0, [Ldga;

    sput-object v0, Ldga;->a:[Ldga;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7736
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7747
    iput-object v0, p0, Ldga;->c:Ljava/lang/Integer;

    .line 7750
    iput-object v0, p0, Ldga;->d:Ldej;

    .line 7753
    iput-object v0, p0, Ldga;->e:Ldek;

    .line 7756
    iput-object v0, p0, Ldga;->f:Ldgk;

    .line 7759
    iput-object v0, p0, Ldga;->g:Ldfo;

    .line 7764
    iput-object v0, p0, Ldga;->i:Ldfz;

    .line 7736
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 7804
    const/4 v0, 0x0

    .line 7805
    iget-object v1, p0, Ldga;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7806
    const/4 v0, 0x1

    iget-object v1, p0, Ldga;->b:Ljava/lang/String;

    .line 7807
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7809
    :cond_0
    iget-object v1, p0, Ldga;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 7810
    const/4 v1, 0x2

    iget-object v2, p0, Ldga;->c:Ljava/lang/Integer;

    .line 7811
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7813
    :cond_1
    iget-object v1, p0, Ldga;->d:Ldej;

    if-eqz v1, :cond_2

    .line 7814
    const/4 v1, 0x3

    iget-object v2, p0, Ldga;->d:Ldej;

    .line 7815
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7817
    :cond_2
    iget-object v1, p0, Ldga;->e:Ldek;

    if-eqz v1, :cond_3

    .line 7818
    const/4 v1, 0x4

    iget-object v2, p0, Ldga;->e:Ldek;

    .line 7819
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7821
    :cond_3
    iget-object v1, p0, Ldga;->f:Ldgk;

    if-eqz v1, :cond_4

    .line 7822
    const/4 v1, 0x5

    iget-object v2, p0, Ldga;->f:Ldgk;

    .line 7823
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7825
    :cond_4
    iget-object v1, p0, Ldga;->g:Ldfo;

    if-eqz v1, :cond_5

    .line 7826
    const/4 v1, 0x6

    iget-object v2, p0, Ldga;->g:Ldfo;

    .line 7827
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7829
    :cond_5
    iget-object v1, p0, Ldga;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 7830
    const/4 v1, 0x7

    iget-object v2, p0, Ldga;->h:Ljava/lang/String;

    .line 7831
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7833
    :cond_6
    iget-object v1, p0, Ldga;->i:Ldfz;

    if-eqz v1, :cond_7

    .line 7834
    const/16 v1, 0x8

    iget-object v2, p0, Ldga;->i:Ldfz;

    .line 7835
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7837
    :cond_7
    iget-object v1, p0, Ldga;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 7838
    const/16 v1, 0x9

    iget-object v2, p0, Ldga;->j:Ljava/lang/String;

    .line 7839
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7841
    :cond_8
    iget-object v1, p0, Ldga;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7842
    iput v0, p0, Ldga;->cachedSize:I

    .line 7843
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 7732
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldga;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldga;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldga;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldga;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldga;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldga;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldga;->d:Ldej;

    if-nez v0, :cond_4

    new-instance v0, Ldej;

    invoke-direct {v0}, Ldej;-><init>()V

    iput-object v0, p0, Ldga;->d:Ldej;

    :cond_4
    iget-object v0, p0, Ldga;->d:Ldej;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldga;->e:Ldek;

    if-nez v0, :cond_5

    new-instance v0, Ldek;

    invoke-direct {v0}, Ldek;-><init>()V

    iput-object v0, p0, Ldga;->e:Ldek;

    :cond_5
    iget-object v0, p0, Ldga;->e:Ldek;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldga;->f:Ldgk;

    if-nez v0, :cond_6

    new-instance v0, Ldgk;

    invoke-direct {v0}, Ldgk;-><init>()V

    iput-object v0, p0, Ldga;->f:Ldgk;

    :cond_6
    iget-object v0, p0, Ldga;->f:Ldgk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldga;->g:Ldfo;

    if-nez v0, :cond_7

    new-instance v0, Ldfo;

    invoke-direct {v0}, Ldfo;-><init>()V

    iput-object v0, p0, Ldga;->g:Ldfo;

    :cond_7
    iget-object v0, p0, Ldga;->g:Ldfo;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldga;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldga;->i:Ldfz;

    if-nez v0, :cond_8

    new-instance v0, Ldfz;

    invoke-direct {v0}, Ldfz;-><init>()V

    iput-object v0, p0, Ldga;->i:Ldfz;

    :cond_8
    iget-object v0, p0, Ldga;->i:Ldfz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldga;->j:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 7771
    iget-object v0, p0, Ldga;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 7772
    const/4 v0, 0x1

    iget-object v1, p0, Ldga;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7774
    :cond_0
    iget-object v0, p0, Ldga;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 7775
    const/4 v0, 0x2

    iget-object v1, p0, Ldga;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7777
    :cond_1
    iget-object v0, p0, Ldga;->d:Ldej;

    if-eqz v0, :cond_2

    .line 7778
    const/4 v0, 0x3

    iget-object v1, p0, Ldga;->d:Ldej;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7780
    :cond_2
    iget-object v0, p0, Ldga;->e:Ldek;

    if-eqz v0, :cond_3

    .line 7781
    const/4 v0, 0x4

    iget-object v1, p0, Ldga;->e:Ldek;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7783
    :cond_3
    iget-object v0, p0, Ldga;->f:Ldgk;

    if-eqz v0, :cond_4

    .line 7784
    const/4 v0, 0x5

    iget-object v1, p0, Ldga;->f:Ldgk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7786
    :cond_4
    iget-object v0, p0, Ldga;->g:Ldfo;

    if-eqz v0, :cond_5

    .line 7787
    const/4 v0, 0x6

    iget-object v1, p0, Ldga;->g:Ldfo;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7789
    :cond_5
    iget-object v0, p0, Ldga;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 7790
    const/4 v0, 0x7

    iget-object v1, p0, Ldga;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7792
    :cond_6
    iget-object v0, p0, Ldga;->i:Ldfz;

    if-eqz v0, :cond_7

    .line 7793
    const/16 v0, 0x8

    iget-object v1, p0, Ldga;->i:Ldfz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7795
    :cond_7
    iget-object v0, p0, Ldga;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 7796
    const/16 v0, 0x9

    iget-object v1, p0, Ldga;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7798
    :cond_8
    iget-object v0, p0, Ldga;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7800
    return-void
.end method
