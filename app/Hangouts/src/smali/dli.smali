.class public final Ldli;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldli;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Ldlt;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1022
    const/4 v0, 0x0

    new-array v0, v0, [Ldli;

    sput-object v0, Ldli;->a:[Ldli;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1023
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1026
    const/4 v0, 0x0

    iput-object v0, p0, Ldli;->b:Ljava/lang/Integer;

    .line 1029
    sget-object v0, Ldlt;->a:[Ldlt;

    iput-object v0, p0, Ldli;->c:[Ldlt;

    .line 1023
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1061
    iget-object v0, p0, Ldli;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1062
    const/4 v0, 0x1

    iget-object v2, p0, Ldli;->b:Ljava/lang/Integer;

    .line 1063
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1065
    :goto_0
    iget-object v2, p0, Ldli;->c:[Ldlt;

    if-eqz v2, :cond_1

    .line 1066
    iget-object v2, p0, Ldli;->c:[Ldlt;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1067
    if-eqz v4, :cond_0

    .line 1068
    const/4 v5, 0x2

    .line 1069
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1066
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1073
    :cond_1
    iget-object v1, p0, Ldli;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1074
    const/4 v1, 0x3

    iget-object v2, p0, Ldli;->d:Ljava/lang/Integer;

    .line 1075
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1077
    :cond_2
    iget-object v1, p0, Ldli;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1078
    const/4 v1, 0x4

    iget-object v2, p0, Ldli;->e:Ljava/lang/String;

    .line 1079
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1081
    :cond_3
    iget-object v1, p0, Ldli;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1082
    iput v0, p0, Ldli;->cachedSize:I

    .line 1083
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1019
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldli;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldli;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldli;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/16 v2, 0x64

    if-eq v0, v2, :cond_2

    const/16 v2, 0x65

    if-eq v0, v2, :cond_2

    const/16 v2, 0x66

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc8

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xca

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcd

    if-eq v0, v2, :cond_2

    const/16 v2, 0xce

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcf

    if-eq v0, v2, :cond_2

    const/16 v2, 0x12c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x191

    if-eq v0, v2, :cond_2

    const/16 v2, 0x192

    if-eq v0, v2, :cond_2

    const/16 v2, 0x1f4

    if-eq v0, v2, :cond_2

    const/16 v2, 0x258

    if-eq v0, v2, :cond_2

    const/16 v2, 0x259

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25b

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25d

    if-eq v0, v2, :cond_2

    const/16 v2, 0x25e

    if-eq v0, v2, :cond_2

    const/16 v2, 0x2bc

    if-eq v0, v2, :cond_2

    const/16 v2, 0x2bd

    if-eq v0, v2, :cond_2

    const/16 v2, 0x320

    if-eq v0, v2, :cond_2

    const/16 v2, 0x321

    if-eq v0, v2, :cond_2

    const/16 v2, 0x322

    if-eq v0, v2, :cond_2

    const/16 v2, 0x323

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldli;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldli;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldli;->c:[Ldlt;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldlt;

    iget-object v3, p0, Ldli;->c:[Ldlt;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldli;->c:[Ldlt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldli;->c:[Ldlt;

    :goto_2
    iget-object v2, p0, Ldli;->c:[Ldlt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldli;->c:[Ldlt;

    new-instance v3, Ldlt;

    invoke-direct {v3}, Ldlt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldli;->c:[Ldlt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldli;->c:[Ldlt;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldli;->c:[Ldlt;

    new-instance v3, Ldlt;

    invoke-direct {v3}, Ldlt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldli;->c:[Ldlt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldli;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldli;->e:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 1038
    iget-object v0, p0, Ldli;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1039
    const/4 v0, 0x1

    iget-object v1, p0, Ldli;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1041
    :cond_0
    iget-object v0, p0, Ldli;->c:[Ldlt;

    if-eqz v0, :cond_2

    .line 1042
    iget-object v1, p0, Ldli;->c:[Ldlt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1043
    if-eqz v3, :cond_1

    .line 1044
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1042
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1048
    :cond_2
    iget-object v0, p0, Ldli;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1049
    const/4 v0, 0x3

    iget-object v1, p0, Ldli;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1051
    :cond_3
    iget-object v0, p0, Ldli;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1052
    const/4 v0, 0x4

    iget-object v1, p0, Ldli;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1054
    :cond_4
    iget-object v0, p0, Ldli;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1056
    return-void
.end method
