.class public final Ldss;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldss;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18071
    const/4 v0, 0x0

    new-array v0, v0, [Ldss;

    sput-object v0, Ldss;->a:[Ldss;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18072
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 18093
    const/4 v0, 0x0

    .line 18094
    iget-object v1, p0, Ldss;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 18095
    const/4 v0, 0x1

    iget-object v1, p0, Ldss;->b:Ljava/lang/Integer;

    .line 18096
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18098
    :cond_0
    iget-object v1, p0, Ldss;->c:[B

    if-eqz v1, :cond_1

    .line 18099
    const/4 v1, 0x2

    iget-object v2, p0, Ldss;->c:[B

    .line 18100
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 18102
    :cond_1
    iget-object v1, p0, Ldss;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18103
    iput v0, p0, Ldss;->cachedSize:I

    .line 18104
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 18068
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldss;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldss;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldss;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldss;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldss;->c:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 18081
    iget-object v0, p0, Ldss;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 18082
    const/4 v0, 0x1

    iget-object v1, p0, Ldss;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 18084
    :cond_0
    iget-object v0, p0, Ldss;->c:[B

    if-eqz v0, :cond_1

    .line 18085
    const/4 v0, 0x2

    iget-object v1, p0, Ldss;->c:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 18087
    :cond_1
    iget-object v0, p0, Ldss;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 18089
    return-void
.end method
