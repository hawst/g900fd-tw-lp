.class public final Lbcj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Lbdk;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ldqh;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v1, Lbdk;

    iget-object v2, p1, Ldqh;->b:Ldui;

    invoke-direct {v1, v2, v0}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    iput-object v1, p0, Lbcj;->a:Lbdk;

    .line 21
    iget-object v1, p1, Ldqh;->c:Ljava/lang/String;

    iput-object v1, p0, Lbcj;->c:Ljava/lang/String;

    .line 22
    iget-object v1, p1, Ldqh;->e:Leir;

    if-nez v1, :cond_0

    :goto_0
    iput-object v0, p0, Lbcj;->b:Ljava/lang/String;

    .line 24
    return-void

    .line 22
    :cond_0
    iget-object v0, p1, Ldqh;->e:Leir;

    iget-object v0, v0, Leir;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a([Ldqh;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ldqh;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbcj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 31
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 32
    new-instance v4, Lbcj;

    invoke-direct {v4, v3}, Lbcj;-><init>(Ldqh;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    return-object v1
.end method
