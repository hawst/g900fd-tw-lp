.class public final Ldwf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwf;


# instance fields
.field public b:Ldqf;

.field public c:Ldui;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7236
    const/4 v0, 0x0

    new-array v0, v0, [Ldwf;

    sput-object v0, Ldwf;->a:[Ldwf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7237
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7240
    iput-object v0, p0, Ldwf;->b:Ldqf;

    .line 7243
    iput-object v0, p0, Ldwf;->c:Ldui;

    .line 7248
    iput-object v0, p0, Ldwf;->e:Ljava/lang/Integer;

    .line 7237
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 7276
    const/4 v0, 0x0

    .line 7277
    iget-object v1, p0, Ldwf;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 7278
    const/4 v0, 0x1

    iget-object v1, p0, Ldwf;->b:Ldqf;

    .line 7279
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7281
    :cond_0
    iget-object v1, p0, Ldwf;->c:Ldui;

    if-eqz v1, :cond_1

    .line 7282
    const/4 v1, 0x2

    iget-object v2, p0, Ldwf;->c:Ldui;

    .line 7283
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7285
    :cond_1
    iget-object v1, p0, Ldwf;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 7286
    const/4 v1, 0x3

    iget-object v2, p0, Ldwf;->d:Ljava/lang/Long;

    .line 7287
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7289
    :cond_2
    iget-object v1, p0, Ldwf;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 7290
    const/4 v1, 0x4

    iget-object v2, p0, Ldwf;->e:Ljava/lang/Integer;

    .line 7291
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7293
    :cond_3
    iget-object v1, p0, Ldwf;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 7294
    const/4 v1, 0x5

    iget-object v2, p0, Ldwf;->f:Ljava/lang/Integer;

    .line 7295
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7297
    :cond_4
    iget-object v1, p0, Ldwf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7298
    iput v0, p0, Ldwf;->cachedSize:I

    .line 7299
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 7233
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwf;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldwf;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldwf;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldwf;->c:Ldui;

    if-nez v0, :cond_3

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldwf;->c:Ldui;

    :cond_3
    iget-object v0, p0, Ldwf;->c:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldwf;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwf;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwf;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwf;->f:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 7255
    iget-object v0, p0, Ldwf;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 7256
    const/4 v0, 0x1

    iget-object v1, p0, Ldwf;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7258
    :cond_0
    iget-object v0, p0, Ldwf;->c:Ldui;

    if-eqz v0, :cond_1

    .line 7259
    const/4 v0, 0x2

    iget-object v1, p0, Ldwf;->c:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7261
    :cond_1
    iget-object v0, p0, Ldwf;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 7262
    const/4 v0, 0x3

    iget-object v1, p0, Ldwf;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 7264
    :cond_2
    iget-object v0, p0, Ldwf;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 7265
    const/4 v0, 0x4

    iget-object v1, p0, Ldwf;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7267
    :cond_3
    iget-object v0, p0, Ldwf;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 7268
    const/4 v0, 0x5

    iget-object v1, p0, Ldwf;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 7270
    :cond_4
    iget-object v0, p0, Ldwf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7272
    return-void
.end method
