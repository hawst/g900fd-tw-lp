.class public Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/BPReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private isAudioUsbDeviceValid(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    if-nez p2, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v2

    .line 34
    :cond_1
    const-string v0, "device"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbDevice;

    .line 35
    if-eqz v0, :cond_0

    move v1, v2

    .line 36
    :goto_1
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 37
    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v4

    .line 38
    if-eqz v4, :cond_2

    .line 39
    invoke-virtual {v4}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    move-result v5

    if-ne v5, v3, :cond_2

    invoke-virtual {v4}, Landroid/hardware/usb/UsbInterface;->getInterfaceSubclass()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    move v2, v3

    .line 40
    goto :goto_0

    .line 36
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private updateWhetherAudioUsbDeviceAttached(Lcom/samsung/android/sdk/professionalaudio/SapaService;Z)V
    .locals 0

    .prologue
    .line 121
    if-eqz p1, :cond_0

    .line 122
    invoke-virtual {p1, p2}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->setAudioUSBDeviceAttached(Z)Z

    .line 124
    :cond_0
    return-void
.end method

.method private useUSB(Landroid/content/Context;Lcom/samsung/android/sdk/professionalaudio/SapaService;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 127
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->isStarted()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 128
    if-ne p3, v1, :cond_1

    .line 129
    invoke-virtual {p2}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->getSettings()Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->getUSBControl()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 131
    invoke-virtual {p2, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->setUseUSBEnabled(Z)Z

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->setUseUSBEnabled(Z)Z

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 57
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/BPReceiver;->isAudioUsbDeviceValid(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 72
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/Sapa;-><init>()V

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->initialize(Landroid/content/Context;)V

    .line 73
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;-><init>()V

    .line 74
    if-eqz v0, :cond_0

    .line 75
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/BPReceiver;->updateWhetherAudioUsbDeviceAttached(Lcom/samsung/android/sdk/professionalaudio/SapaService;Z)V

    .line 76
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/BPReceiver;->useUSB(Landroid/content/Context;Lcom/samsung/android/sdk/professionalaudio/SapaService;Z)V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/BPReceiver;->isAudioUsbDeviceValid(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 90
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/Sapa;-><init>()V

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->initialize(Landroid/content/Context;)V

    .line 91
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;-><init>()V

    .line 92
    if-eqz v0, :cond_0

    .line 93
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/BPReceiver;->updateWhetherAudioUsbDeviceAttached(Lcom/samsung/android/sdk/professionalaudio/SapaService;Z)V

    .line 94
    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/BPReceiver;->useUSB(Landroid/content/Context;Lcom/samsung/android/sdk/professionalaudio/SapaService;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    goto :goto_0

    .line 115
    :catch_1
    move-exception v0

    goto :goto_0
.end method
