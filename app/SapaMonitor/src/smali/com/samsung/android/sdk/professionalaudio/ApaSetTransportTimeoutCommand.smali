.class final Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportTimeoutCommand;
.super Lcom/samsung/android/sdk/professionalaudio/ApaCommand;
.source "SourceFile"


# direct methods
.method public constructor <init>(D)V
    .locals 1

    .prologue
    .line 29
    const-string v0, "sync_set_timeout"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/ApaCommand;-><init>(Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportTimeoutCommand;->setTimeout(D)Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportTimeoutCommand;

    .line 31
    return-void
.end method


# virtual methods
.method public setTimeout(D)Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportTimeoutCommand;
    .locals 3

    .prologue
    .line 39
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportTimeoutCommand;->mInputs:Lorg/json/JSONArray;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "timeout"

    invoke-virtual {v1, v2, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-object p0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
