.class public final Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final AUDIO_APP_CONNECTION_SERVICE_FULL_NAME:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.app.audioconnectionservice.AudioConnectionService"

.field private static final TAG:Ljava/lang/String; = "professionalaudioconnection:library:j:SapaAppService "

.field private static counter:I


# instance fields
.field private mActionDefiner:Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;

.field private mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mConnectionListeners:Ljava/util/ArrayList;

.field private mContext:Ljava/lang/ref/WeakReference;

.field private mIsConnected:Z

.field private mRemoteListener:Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;

.field private mStateListeners:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->counter:I

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;-><init>(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnection:Landroid/content/ServiceConnection;

    .line 635
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;-><init>(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mRemoteListener:Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;

    .line 105
    :try_start_0
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Library version v. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mIsConnected:Z

    .line 110
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    .line 111
    iput-object v6, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnectionListeners:Ljava/util/ArrayList;

    .line 113
    iput-object v6, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mActionDefiner:Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;

    .line 115
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    const-string v1, "Version could not be determined"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mIsConnected:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mRemoteListener:Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;Z)V
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mIsConnected:Z

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->notifyConnectionSet()V

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->notifyConnectionClosed()V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mActionDefiner:Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;

    return-object v0
.end method

.method private isConnectedToRemote()Z
    .locals 1

    .prologue
    .line 584
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mIsConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    if-nez v0, :cond_1

    .line 585
    :cond_0
    const/4 v0, 0x0

    .line 587
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private notifyConnectionClosed()V
    .locals 3

    .prologue
    .line 128
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "notifyConnectionClosed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mIsConnected:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnectionListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 135
    :cond_0
    return-void

    .line 131
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaServiceConnectListener;

    .line 132
    invoke-interface {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaServiceConnectListener;->onServiceDisconnected()V

    goto :goto_0
.end method

.method private notifyConnectionSet()V
    .locals 3

    .prologue
    .line 118
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "notifyConnectionSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mIsConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnectionListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    :cond_0
    return-void

    .line 121
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaServiceConnectListener;

    .line 122
    invoke-interface {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaServiceConnectListener;->onServiceConnected()V

    goto :goto_0
.end method


# virtual methods
.method public activateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;
    .locals 3

    .prologue
    .line 444
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 445
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 449
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 450
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->putCallerPackageName(Ljava/lang/String;)V

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 453
    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->activateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    .line 455
    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 457
    :cond_2
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Application can not be added"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 458
    :catch_0
    move-exception v0

    .line 459
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 460
    const-string v2, "Getting active app configuration was not possible due to RemoteException"

    .line 459
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0
.end method

.method public addActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 3

    .prologue
    .line 153
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 154
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    :cond_0
    new-instance v0, Ljava/security/InvalidParameterException;

    .line 156
    const-string v1, "You can not use addActiveApp method to activate other app"

    .line 155
    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_2

    .line 159
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 162
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->addActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 165
    const-string v2, "Adding active app was not possible due to RemoteException"

    .line 164
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addAppStateListener(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;)V
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addAppStateListener "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    return-void
.end method

.method public addConnectionListener(Lcom/samsung/android/sdk/professionalaudio/app/SapaServiceConnectListener;)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mIsConnected:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 227
    invoke-interface {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaServiceConnectListener;->onServiceConnected()V

    .line 229
    :cond_0
    return-void
.end method

.method public callAction(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 511
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 512
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 515
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 516
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    move-result-object v0

    .line 517
    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getActionInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;

    move-result-object v0

    if-nez v0, :cond_2

    .line 518
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaUndeclaredActionException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 519
    const-string v2, "] was not declared on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 518
    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaUndeclaredActionException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    :catch_0
    move-exception v0

    .line 524
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 525
    const-string v2, " Calling action was not possible due to RemoteException"

    .line 524
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :goto_0
    return-void

    .line 521
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, p2}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->runAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public changeAppInfo(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 3

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 282
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 284
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->changeAppInfo(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V

    .line 293
    :goto_0
    return-void

    .line 286
    :cond_1
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    const-string v1, "Not allowed to change the other app info"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 288
    :catch_0
    move-exception v0

    .line 289
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 290
    const-string v2, "Changing app state was not possible due to RemoteException"

    .line 289
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public connect()V
    .locals 4

    .prologue
    .line 200
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connect "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 201
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 200
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    new-instance v1, Landroid/content/Intent;

    .line 204
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.audioconnectionservice.AudioConnectionService"

    .line 203
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnection:Landroid/content/ServiceConnection;

    .line 207
    const/4 v3, 0x1

    .line 206
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 209
    :cond_0
    sget v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->counter:I

    if-lez v0, :cond_1

    .line 210
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 211
    const-string v1, "Only one SapaAppService from a package can be connected to Audio Connection Service"

    .line 210
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_1
    sget v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->counter:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->counter:I

    .line 215
    return-void
.end method

.method public deactivateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V
    .locals 3

    .prologue
    .line 480
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 481
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 484
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->deactivateApp(Ljava/lang/String;)Z

    move-result v0

    .line 485
    if-nez v0, :cond_1

    .line 486
    new-instance v0, Ljava/lang/IllegalAccessException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Application "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not responsive or is not active"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 488
    :catch_0
    move-exception v0

    .line 489
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 490
    const-string v2, "Deactivating app was not possible due to RemoteException"

    .line 489
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_1
    return-void
.end method

.method public declareTransportMaster()V
    .locals 3

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 599
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 602
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 603
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->declareBeingTransportMaster(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 611
    return-void

    .line 604
    :catch_0
    move-exception v0

    .line 605
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 606
    const-string v2, "Declaring being transport master was not possible due to RemoteException"

    .line 605
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0
.end method

.method public disconnect()V
    .locals 3

    .prologue
    .line 175
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "disconnect "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 176
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 175
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    if-eqz v0, :cond_0

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 180
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->removeRemoteListener(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 190
    :cond_1
    sget v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->counter:I

    if-lez v0, :cond_2

    sget v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->counter:I

    add-int/lit8 v0, v0, -0x1

    :goto_1
    sput v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->counter:I

    .line 191
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 183
    const-string v2, "Removing remote state listener was not possible due to RemoteException"

    .line 182
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 190
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 3

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 391
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 392
    :catch_0
    move-exception v0

    .line 393
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 394
    const-string v2, "Getting active app info was not possible due to RemoteException"

    .line 393
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0
.end method

.method public getAllActiveApp()Ljava/util/List;
    .locals 3

    .prologue
    .line 337
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 341
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 342
    invoke-interface {v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->getAllActiveApp()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 343
    return-object v0

    .line 344
    :catch_0
    move-exception v0

    .line 345
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 346
    const-string v2, "Getting active app list was not possible due to RemoteException"

    .line 345
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0
.end method

.method public getAllInstalledApp()Ljava/util/List;
    .locals 3

    .prologue
    .line 362
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 363
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 366
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    invoke-interface {v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->getAllInstalledApp()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 367
    :catch_0
    move-exception v0

    .line 368
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 369
    const-string v2, "Getting installed app list was not possible due to RemoteException"

    .line 368
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0
.end method

.method public getInstalledApp(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 3

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 416
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 417
    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->getInstalledAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 416
    return-object v0

    .line 418
    :catch_0
    move-exception v0

    .line 419
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 420
    const-string v2, "Getting installed app info was not possible due to RemoteException"

    .line 419
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0
.end method

.method public getLaunchIntent(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 566
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 567
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 570
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 571
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 572
    :cond_1
    new-instance v0, Ljava/lang/IllegalAccessException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Application "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not responsive or is not active"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    :catch_0
    move-exception v0

    .line 576
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 577
    const-string v2, "Getting launch intent was not possible due to RemoteException"

    .line 576
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 574
    :cond_2
    return-object v0
.end method

.method getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getTransportMaster()Ljava/lang/String;
    .locals 3

    .prologue
    .line 622
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 623
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 626
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    invoke-interface {v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->getTransportMaster()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 627
    :catch_0
    move-exception v0

    .line 628
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 629
    const-string v2, "Getting transport master was not possible due to RemoteException"

    .line 628
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0
.end method

.method public removeActionDefiner()V
    .locals 1

    .prologue
    .line 546
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mActionDefiner:Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;

    .line 547
    return-void
.end method

.method public removeAppStateListener(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;)V
    .locals 3

    .prologue
    .line 260
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeAppStateListener "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 260
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 263
    return-void
.end method

.method public removeConnectionListener(Lcom/samsung/android/sdk/professionalaudio/app/SapaServiceConnectListener;)V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 239
    return-void
.end method

.method public removeFromActiveApps(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V
    .locals 3

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaConnectionNotSetException;-><init>()V

    throw v0

    .line 312
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 313
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    move-result-object v0

    .line 314
    if-eqz v0, :cond_1

    .line 315
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 316
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 315
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 316
    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    .line 318
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->removeFromActiveApps(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    :cond_1
    :goto_0
    return-void

    .line 320
    :catch_0
    move-exception v0

    .line 321
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    .line 322
    const-string v2, "Removing app from active was not possible due to RemoteException"

    .line 321
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    const-string v1, "professionalaudioconnection:library:j:SapaAppService "

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setActionDefiner(Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mActionDefiner:Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;

    .line 539
    return-void
.end method
