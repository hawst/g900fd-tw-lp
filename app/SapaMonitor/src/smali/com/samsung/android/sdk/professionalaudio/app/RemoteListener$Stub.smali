.class public abstract Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

.field static final TRANSACTION_check:I = 0x7

.field static final TRANSACTION_onActionCalled:I = 0x6

.field static final TRANSACTION_onAppActivated:I = 0x3

.field static final TRANSACTION_onAppChanged:I = 0x5

.field static final TRANSACTION_onAppDeactivated:I = 0x4

.field static final TRANSACTION_onAppInstalled:I = 0x1

.field static final TRANSACTION_onAppUninstalled:I = 0x2

.field static final TRANSACTION_onTransportMasterChanged:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    .locals 2

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 118
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 42
    :sswitch_0
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->onAppInstalled(Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :sswitch_2
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->onAppUninstalled(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :sswitch_3
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->onAppActivated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :sswitch_4
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 78
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->onAppDeactivated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :sswitch_5
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 88
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->onAppChanged(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :sswitch_6
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 100
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->onActionCalled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :sswitch_7
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->check()V

    goto :goto_0

    .line 111
    :sswitch_8
    const-string v1, "com.samsung.android.sdk.professionalaudio.app.RemoteListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->onTransportMasterChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
