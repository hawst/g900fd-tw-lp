.class Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mHandler:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mHandler:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mHandler:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1$1;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/net/LocalSocket;)Ljava/lang/Integer;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 91
    .line 92
    const/16 v0, 0x91

    :try_start_0
    new-array v0, v0, [C

    .line 93
    new-instance v1, Ljava/io/InputStreamReader;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 94
    const/4 v2, 0x0

    const/16 v3, 0x91

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/InputStreamReader;->read([CII)I

    move-result v1

    .line 95
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Ljava/lang/String;-><init>([CII)V

    .line 99
    const-string v0, "start_jackd_normal"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 100
    const-string v0, "start_jackd_without_usb"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->showToast(Ljava/lang/String;)V

    .line 138
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 101
    :cond_0
    const-string v0, "usb_detached"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 102
    const-string v0, "usb_detached_from_jack"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->showToast(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 103
    :cond_1
    :try_start_1
    const-string v0, "usb_attached"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_2

    .line 104
    const-string v0, "usb_attached_to_jack"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->showToast(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 135
    :catch_1
    move-exception v0

    .line 136
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 105
    :cond_2
    :try_start_2
    const-string v0, "start_jackd_with_usb"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_3

    .line 106
    const-string v0, "start_jackd_with_usb"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :cond_3
    const-string v0, "[{\"command\":\"updateusbdeviceinfo\"}"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v4, :cond_4

    .line 108
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_4
    const-string v0, "remove_jackd_running_notification"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_5

    .line 111
    const-string v0, "SapaMonitor"

    const-string v1, "notification removed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mContext:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->access$100(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->cancelNotification(Landroid/content/Context;)V

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mContext:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->access$100(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->stopSocketRunnable(Landroid/content/Context;)V

    goto :goto_0

    .line 127
    :cond_5
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->showToast(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    check-cast p1, [Landroid/net/LocalSocket;

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->doInBackground([Landroid/net/LocalSocket;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
