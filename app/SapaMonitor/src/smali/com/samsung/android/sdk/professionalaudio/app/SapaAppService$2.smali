.class Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;
.super Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;
.source "SourceFile"


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    .line 635
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public check()V
    .locals 0

    .prologue
    .line 736
    return-void
.end method

.method public onActionCalled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 720
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    const-string v1, "onActionCalled "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mActionDefiner:Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$8(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mActionDefiner:Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$8(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;

    move-result-object v0

    .line 723
    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v1, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionDefinerInterface;->getActionDefinition(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v0

    .line 724
    if-eqz v0, :cond_1

    .line 725
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 726
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 732
    :cond_0
    :goto_0
    return-void

    .line 728
    :cond_1
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] undefined on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 729
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 728
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onAppActivated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 670
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppActivated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 671
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 670
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 673
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    .line 674
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppActivated. Listeners number: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 675
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v2

    .line 676
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 675
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 674
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 673
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 681
    :cond_0
    return-void

    .line 677
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;

    .line 678
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v2, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;->onAppActivated(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V

    goto :goto_0
.end method

.method public onAppChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 703
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    .line 704
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppChanged "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 705
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 704
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 703
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 707
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    .line 708
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppChanged. Listeners number: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 709
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v2

    .line 710
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 709
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 708
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 707
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 715
    :cond_0
    return-void

    .line 711
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;

    .line 712
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v2, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;->onAppChanged(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V

    goto :goto_0
.end method

.method public onAppDeactivated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 686
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppDeactivated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 687
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 686
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 689
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    .line 690
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppDeactivated. Listeners number: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 691
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v2

    .line 692
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 691
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 690
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 689
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 698
    :cond_0
    return-void

    .line 693
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;

    .line 694
    const-string v2, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "listener.onAppDeactivated("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v2, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;->onAppDeactivated(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V

    goto :goto_0
.end method

.method public onAppInstalled(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 639
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppInstalled"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 640
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 639
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 642
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    .line 643
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppInstalled. Listeners number: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 644
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v2

    .line 645
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 644
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 643
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 642
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 650
    :cond_0
    return-void

    .line 646
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;

    .line 647
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v2, p1, p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;->onAppInstalled(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V

    goto :goto_0
.end method

.method public onAppUninstalled(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 654
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppUninstalled"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 655
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 654
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 657
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    .line 658
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoteStateListener onAppUninstalled. Listeners number: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 659
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v2

    .line 660
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 659
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 658
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 657
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 665
    :cond_0
    return-void

    .line 661
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;

    .line 662
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v2, p1, p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;->onAppUninstalled(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V

    goto :goto_0
.end method

.method public onTransportMasterChanged(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 740
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 741
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mStateListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$7(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 745
    :cond_0
    return-void

    .line 741
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;

    .line 742
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v2, p1, p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;->onTransportMasterChanged(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V

    goto :goto_0
.end method
