.class public final Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final BUFFER_SIZE_240:I = 0xf0

.field public static final BUFFER_SIZE_480:I = 0x1e0

.field public static final BUFFER_SIZE_960:I = 0x3c0

.field public static final LATENCY_HIGH:Ljava/lang/String; = "high"

.field public static final LATENCY_LOW:Ljava/lang/String; = "low"

.field public static final LATENCY_MID:Ljava/lang/String; = "mid"

.field public static final SAMPLE_RATE_44100:I = 0xac44

.field public static final SAMPLE_RATE_48000:I = 0xbb80


# instance fields
.field private mAvailableSapaProcessorCount:I

.field private mBufferSize:I

.field private mLatency:Ljava/lang/String;

.field private mSampleRate:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    return-void
.end method


# virtual methods
.method public getAvailableSapaProcessorCount()I
    .locals 1

    .prologue
    .line 396
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mAvailableSapaProcessorCount:I

    return v0
.end method

.method public getBufferSize()I
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mBufferSize:I

    return v0
.end method

.method public getLatency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mLatency:Ljava/lang/String;

    return-object v0
.end method

.method public getSampleRate()I
    .locals 1

    .prologue
    .line 370
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mSampleRate:I

    return v0
.end method

.method setAvailableSapaProcessorCount(I)V
    .locals 0

    .prologue
    .line 400
    iput p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mAvailableSapaProcessorCount:I

    .line 401
    return-void
.end method

.method setBufferSize(I)V
    .locals 0

    .prologue
    .line 387
    iput p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mBufferSize:I

    .line 388
    return-void
.end method

.method setLatency(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 352
    if-nez p1, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    const-string v0, "high"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    const-string v0, "high"

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mLatency:Ljava/lang/String;

    goto :goto_0

    .line 357
    :cond_2
    const-string v0, "mid"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358
    const-string v0, "mid"

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mLatency:Ljava/lang/String;

    goto :goto_0

    .line 359
    :cond_3
    const-string v0, "low"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    const-string v0, "low"

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mLatency:Ljava/lang/String;

    goto :goto_0
.end method

.method setSampleRate(I)V
    .locals 0

    .prologue
    .line 374
    iput p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->mSampleRate:I

    .line 375
    return-void
.end method
