.class Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field mJsonString:Ljava/lang/String;

.field private mResult:Z


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;->mJsonString:Ljava/lang/String;

    .line 29
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    invoke-direct {v0, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONArray;

    .line 30
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "result"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;->mResult:Z

    .line 38
    :goto_0
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;->mResult:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public result()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;->mResult:Z

    return v0
.end method
