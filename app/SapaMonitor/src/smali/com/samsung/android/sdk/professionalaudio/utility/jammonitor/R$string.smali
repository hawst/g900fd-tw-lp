.class public final Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/R$string;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final action_settings:I = 0x7f060000

.field public static final app_name:I = 0x7f060001

.field public static final enable_dummy_daemon:I = 0x7f060002

.field public static final enable_dummy_daemon_description:I = 0x7f060003

.field public static final jackd_running:I = 0x7f060004

.field public static final jackd_stopped:I = 0x7f060005

.field public static final latency_setting_description:I = 0x7f060006

.field public static final latency_setting_item_high_description:I = 0x7f060007

.field public static final latency_setting_item_high_title:I = 0x7f060008

.field public static final latency_setting_item_low_description:I = 0x7f060009

.field public static final latency_setting_item_low_title:I = 0x7f06000a

.field public static final latency_setting_item_mid_description:I = 0x7f06000b

.field public static final latency_setting_item_mid_title:I = 0x7f06000c

.field public static final latency_setting_title:I = 0x7f06000d

.field public static final not_support_settings:I = 0x7f06000e

.field public static final noti_message:I = 0x7f06000f

.field public static final noti_title:I = 0x7f060010

.field public static final professionalaudio_client_status:I = 0x7f060011

.field public static final professionalaudio_status:I = 0x7f060012

.field public static final s_professional_audio:I = 0x7f060013

.field public static final service_permission_description:I = 0x7f060014

.field public static final service_permission_label:I = 0x7f060015

.field public static final service_stop_confirm_dialog_message:I = 0x7f060016

.field public static final service_stop_confirm_dialog_negative:I = 0x7f060017

.field public static final service_stop_confirm_dialog_positive:I = 0x7f060018

.field public static final service_stop_confirm_dialog_title:I = 0x7f060019

.field public static final setting_activity_name:I = 0x7f06001a

.field public static final settings_description:I = 0x7f06001b

.field public static final stop:I = 0x7f06001c

.field public static final stop_by_force_kill_all_of_client:I = 0x7f06001d

.field public static final stop_daemon_button_text:I = 0x7f06001e

.field public static final stop_service_button_text:I = 0x7f06001f

.field public static final title_daemon_setting:I = 0x7f060020

.field public static final usb_audio_setting_description:I = 0x7f060021

.field public static final usb_audio_setting_title:I = 0x7f060022

.field public static final usb_setting_item_android_use_usb_description:I = 0x7f060023

.field public static final usb_setting_item_android_use_usb_title:I = 0x7f060024

.field public static final usb_setting_item_sapa_use_usb_description:I = 0x7f060025

.field public static final usb_setting_item_sapa_use_usb_title:I = 0x7f060026

.field public static final use_usb_noti_message:I = 0x7f060027

.field public static final use_usb_noti_title:I = 0x7f060028


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
