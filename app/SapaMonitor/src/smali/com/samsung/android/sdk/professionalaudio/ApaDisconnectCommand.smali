.class final Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectCommand;
.super Lcom/samsung/android/sdk/professionalaudio/ApaCommand;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "disconnect"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/ApaCommand;-><init>(Ljava/lang/String;)V

    .line 27
    return-void
.end method


# virtual methods
.method public put(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectCommand;
    .locals 3

    .prologue
    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectCommand;->mInputs:Lorg/json/JSONArray;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "output"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "input"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-object p0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
