.class public abstract Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

.field static final TRANSACTION_activateApp:I = 0x3

.field static final TRANSACTION_addActiveApp:I = 0x8

.field static final TRANSACTION_addRemoteListener:I = 0xb

.field static final TRANSACTION_changeAppInfo:I = 0xa

.field static final TRANSACTION_deactivateApp:I = 0x4

.field static final TRANSACTION_declareBeingTransportMaster:I = 0xe

.field static final TRANSACTION_getActiveAppInfo:I = 0x6

.field static final TRANSACTION_getAllActiveApp:I = 0x2

.field static final TRANSACTION_getAllInstalledApp:I = 0x1

.field static final TRANSACTION_getInstalledAppInfo:I = 0x5

.field static final TRANSACTION_getLaunchIntent:I = 0xd

.field static final TRANSACTION_getTransportMaster:I = 0xf

.field static final TRANSACTION_removeFromActiveApps:I = 0x9

.field static final TRANSACTION_removeRemoteListener:I = 0xc

.field static final TRANSACTION_runAction:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;
    .locals 2

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 220
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 45
    :sswitch_0
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->getAllInstalledApp()Ljava/util/List;

    move-result-object v0

    .line 52
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 53
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 58
    :sswitch_2
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->getAllActiveApp()Ljava/util/List;

    move-result-object v0

    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 66
    :sswitch_3
    const-string v2, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 74
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->activateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :sswitch_4
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->deactivateApp(Ljava/lang/String;)Z

    move-result v0

    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 91
    :sswitch_5
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->getInstalledAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    move-result-object v0

    .line 95
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 96
    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    invoke-virtual {v0, p3, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 101
    :cond_2
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 107
    :sswitch_6
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    move-result-object v0

    .line 111
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 112
    if-eqz v0, :cond_3

    .line 113
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    invoke-virtual {v0, p3, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 117
    :cond_3
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 123
    :sswitch_7
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 130
    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->runAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 135
    :sswitch_8
    const-string v2, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4

    .line 138
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 143
    :cond_4
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->addActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V

    goto/16 :goto_0

    .line 148
    :sswitch_9
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->removeFromActiveApps(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 156
    :sswitch_a
    const-string v2, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 160
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_5

    .line 161
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 166
    :cond_5
    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->changeAppInfo(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V

    goto/16 :goto_0

    .line 171
    :sswitch_b
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;

    move-result-object v2

    .line 176
    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->addRemoteListener(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V

    goto/16 :goto_0

    .line 181
    :sswitch_c
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 184
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->removeRemoteListener(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 189
    :sswitch_d
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 192
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 193
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 194
    if-eqz v0, :cond_6

    .line 195
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 196
    invoke-virtual {v0, p3, v1}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 199
    :cond_6
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 205
    :sswitch_e
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 208
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->declareBeingTransportMaster(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 213
    :sswitch_f
    const-string v0, "com.samsung.android.sdk.professionalaudio.app.AudioAppConnectionService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->getTransportMaster()Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 216
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
