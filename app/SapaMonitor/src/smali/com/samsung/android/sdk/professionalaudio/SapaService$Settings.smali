.class public final Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final DEFAULT_LATENCY_HIGH:I = 0x1

.field public static final DEFAULT_LATENCY_LOW:I = 0x2

.field public static final DEFAULT_LATENCY_MID:I = 0x0

.field public static final USB_ASK_ALWAYS:I = 0x0

.field public static final USB_USE_ANDROID:I = 0x2

.field public static final USB_USE_SAPA:I = 0x1


# instance fields
.field private mDefaultLatency:I

.field private mUSBControl:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292
    return-void
.end method


# virtual methods
.method public getDefaultLatency()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->mDefaultLatency:I

    return v0
.end method

.method public getUSBControl()I
    .locals 1

    .prologue
    .line 272
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->mUSBControl:I

    return v0
.end method

.method public setDefaultLatency(I)V
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 277
    if-eqz p1, :cond_0

    .line 278
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 279
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->mDefaultLatency:I

    .line 281
    :cond_1
    return-void
.end method

.method public setUSBControl(I)V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 285
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 286
    if-nez p1, :cond_1

    .line 287
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->mUSBControl:I

    .line 289
    :cond_1
    return-void
.end method
