.class public Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final MSG_LATENCY_SETTING_CHANGED:I = 0x2711

.field private static final MSG_USB_SETTING_CHANGED:I = 0x2710


# instance fields
.field private commandHandler:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;

.field latencyDataSetObserver:Landroid/database/DataSetObserver;

.field latencyListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private latencyListView:Landroid/widget/ListView;

.field offButton:Landroid/widget/Button;

.field offButtonListener:Landroid/view/View$OnClickListener;

.field service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

.field settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

.field usbDataSetObserver:Landroid/database/DataSetObserver;

.field usbListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private usbListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 190
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$4;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->offButtonListener:Landroid/view/View$OnClickListener;

    .line 263
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$5;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbDataSetObserver:Landroid/database/DataSetObserver;

    .line 285
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyDataSetObserver:Landroid/database/DataSetObserver;

    .line 303
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$7;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 313
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$8;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method private ShowServiceStopConfirmDialog()V
    .locals 4

    .prologue
    .line 78
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 79
    const v1, 0x7f060016

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060018

    new-instance v3, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$2;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060017

    new-instance v3, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$1;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 93
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 95
    const v1, 0x7f060019

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 98
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->commandHandler:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;)Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->commandHandler:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->setOffButtonDisable()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->ShowServiceStopConfirmDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    return-object v0
.end method

.method private setOffButtonDisable()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->offButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 260
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->offButton:Landroid/widget/Button;

    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    .line 261
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 111
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$LooperThread;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$LooperThread;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$LooperThread;->start()V

    .line 114
    :try_start_0
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/Sapa;-><init>()V

    .line 115
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->initialize(Landroid/content/Context;)V

    .line 117
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    .line 118
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->getSettings()Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    if-eqz v0, :cond_1

    .line 122
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->setContentView(I)V

    .line 124
    const v0, 0x7f090005

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    .line 125
    iget-object v6, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v3, 0x7f060024

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const v3, 0x7f060026

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v4, 0x7f060023

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    const v4, 0x7f060025

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    new-array v4, v1, [I

    fill-array-data v4, :array_0

    iget-object v5, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbDataSetObserver:Landroid/database/DataSetObserver;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;[ILandroid/database/DataSetObserver;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 138
    const v0, 0x7f090006

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    .line 139
    iget-object v6, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v3, 0x7f06000a

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const v3, 0x7f06000c

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const v3, 0x7f060008

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v4, 0x7f060009

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    const v4, 0x7f06000b

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    const v4, 0x7f060007

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x3

    new-array v4, v1, [I

    fill-array-data v4, :array_1

    iget-object v5, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyDataSetObserver:Landroid/database/DataSetObserver;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;[ILandroid/database/DataSetObserver;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 155
    const v0, 0x7f090004

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->offButton:Landroid/widget/Button;

    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->offButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->offButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 178
    :goto_0
    const v0, 0x7f090003

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 179
    if-eqz v0, :cond_0

    .line 180
    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$3;

    invoke-direct {v1, p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$3;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;Landroid/widget/ScrollView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 188
    :cond_0
    return-void

    .line 163
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->finish()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->finish()V

    goto :goto_0

    .line 168
    :catch_1
    move-exception v0

    .line 169
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 170
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->finish()V

    goto :goto_0

    .line 125
    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data

    .line 139
    :array_1
    .array-data 4
        0x2
        0x0
        0x1
    .end array-data
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 201
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;

    .line 215
    if-eqz v0, :cond_1

    .line 216
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->getUSBControl()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->initializeValues(I)V

    .line 218
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 219
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->getAllItemHeight(Landroid/view/ViewGroup;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 220
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->usbListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;

    .line 227
    if-eqz v0, :cond_2

    .line 228
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->getDefaultLatency()I

    move-result v1

    .line 229
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    if-eqz v2, :cond_6

    .line 230
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->getParameters()Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;

    move-result-object v2

    .line 231
    if-eqz v2, :cond_6

    .line 232
    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->getLatency()Ljava/lang/String;

    move-result-object v3

    const-string v4, "high"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 233
    const/4 v1, 0x1

    move v2, v1

    .line 241
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->initializeValues(I)V

    .line 243
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 244
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->getAllItemHeight(Landroid/view/ViewGroup;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->getCount()I

    move-result v0

    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 245
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    :cond_2
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaGhost;->isNotificationNeed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 252
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->setOffButtonDisable()V

    .line 256
    :cond_3
    return-void

    .line 234
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->getLatency()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mid"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 235
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1

    .line 236
    :cond_5
    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->getLatency()Ljava/lang/String;

    move-result-object v2

    const-string v3, "low"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 237
    const/4 v1, 0x2

    move v2, v1

    goto :goto_1

    .line 207
    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_6
    move v2, v1

    goto :goto_1
.end method
