.class public final Lcom/samsung/android/sdk/professionalaudio/SapaPort;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final API_LEVEL:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final INOUT_TYPE_IN:I = 0x2

.field public static final INOUT_TYPE_OUT:I = 0x1

.field public static final SIGNAL_TYPE_AUDIO:I = 0x1

.field public static final SIGNAL_TYPE_MIDI:I = 0x2


# instance fields
.field private mFullName:Ljava/lang/String;

.field private mInOutType:I

.field private mPortName:Ljava/lang/String;

.field private mSignalType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->$assertionsDisabled:Z

    .line 214
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 225
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mPortName:Ljava/lang/String;

    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mSignalType:I

    .line 205
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mInOutType:I

    .line 206
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/sdk/professionalaudio/SapaPort;)V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    if-eq p2, v0, :cond_0

    if-eq p2, v1, :cond_0

    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The signalType parameter is invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    if-eq p3, v1, :cond_1

    if-eq p3, v0, :cond_1

    .line 110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The inOutType parameter is invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    .line 113
    iput p2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mSignalType:I

    .line 114
    iput p3, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mInOutType:I

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mPortName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 123
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "the fullName parameter is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :catch_1
    move-exception v0

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "the fullName parameter is invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 181
    const/16 v0, 0x2711

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 138
    if-ne p1, p0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v0

    .line 141
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 142
    goto :goto_0

    .line 145
    :cond_3
    instance-of v2, p1, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    if-nez v2, :cond_4

    move v0, v1

    .line 146
    goto :goto_0

    .line 149
    :cond_4
    check-cast p1, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 150
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    if-eq v2, v3, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    .line 151
    iget-object v3, p1, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 152
    :cond_5
    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mSignalType:I

    iget v3, p1, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mSignalType:I

    if-ne v2, v3, :cond_6

    .line 153
    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mInOutType:I

    iget v3, p1, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mInOutType:I

    if-eq v2, v3, :cond_0

    :cond_6
    move v0, v1

    .line 150
    goto :goto_0
.end method

.method public getFullName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    return-object v0
.end method

.method public getInOutType()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mInOutType:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mPortName:Ljava/lang/String;

    return-object v0
.end method

.method getPortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mPortName:Ljava/lang/String;

    return-object v0
.end method

.method public getSignalType()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mSignalType:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 166
    sget-boolean v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 167
    :cond_0
    const/16 v0, 0x29

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mFullName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mPortName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 197
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mSignalType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 198
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->mInOutType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    return-void
.end method
