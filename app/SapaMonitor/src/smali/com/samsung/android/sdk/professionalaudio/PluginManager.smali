.class Lcom/samsung/android/sdk/professionalaudio/PluginManager;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final DEFAULT_PLUGIN_MODULE_FILENAME:Ljava/lang/String; = "libwave.so"

.field static final DEFAULT_PLUGIN_NAME:Ljava/lang/String; = "CUSTOM"

.field protected static final EXPLANATION_INDEX:I = 0x5

.field private static final META_DATA_SEPERATOR:Ljava/lang/String; = ";"

.field protected static final MODULE_FILENAME_INDEX:I = 0x3

.field protected static final NAME_INDEX:I = 0x2

.field protected static final PLUGIN_TYPE_MAX_INDEX:I = 0x6

.field protected static final SETUP_ARGUMENTS_INDEX:I = 0x4

.field protected static final VERSION_INDEX:I = 0x0

.field protected static final VERSION_NAME_INDEX:I = 0x1


# instance fields
.field private mCallerPackageName:Ljava/lang/String;

.field mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/PluginManager;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/PluginManager;->mCallerPackageName:Ljava/lang/String;

    .line 64
    return-void
.end method


# virtual methods
.method public getPluginList()Ljava/util/List;
    .locals 15

    .prologue
    const/4 v2, 0x0

    .line 71
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 73
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;

    const-string v1, "CUSTOM"

    const-string v3, "UNKNOWN"

    iget-object v4, p0, Lcom/samsung/android/sdk/professionalaudio/PluginManager;->mCallerPackageName:Ljava/lang/String;

    const-string v5, "libwave.so"

    const/4 v6, 0x0

    const-string v7, "the module what you made"

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 78
    const-string v1, "com.samsung.android.sdk.professionalaudio.plugin.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/PluginManager;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 82
    if-eqz v12, :cond_1

    .line 83
    :cond_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 143
    :cond_1
    return-object v11

    .line 85
    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 86
    if-eqz v0, :cond_0

    .line 88
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/PluginManager;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v3}, Landroid/content/pm/ServiceInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 89
    if-eqz v1, :cond_0

    .line 91
    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 92
    if-eqz v13, :cond_0

    .line 94
    array-length v14, v13

    move v1, v2

    .line 96
    :goto_0
    if-ge v1, v14, :cond_0

    .line 98
    :try_start_0
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 99
    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/PluginManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v4, 0x80

    invoke-virtual {v3, v7, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 100
    if-nez v3, :cond_4

    .line 96
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    :cond_4
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 103
    if-eqz v3, :cond_3

    .line 105
    aget-object v4, v13, v1

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 106
    if-eqz v3, :cond_3

    .line 109
    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 111
    if-eqz v3, :cond_3

    .line 114
    array-length v4, v3

    const/4 v5, 0x6

    if-lt v4, v5, :cond_3

    .line 118
    const/4 v4, 0x0

    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 119
    const/4 v4, 0x2

    aget-object v4, v3, v4

    .line 120
    const/4 v6, 0x1

    aget-object v6, v3, v6

    .line 121
    const/4 v8, 0x3

    aget-object v8, v3, v8

    .line 122
    const/4 v9, 0x5

    aget-object v10, v3, v9

    .line 123
    const/4 v9, 0x4

    aget-object v9, v3, v9

    .line 131
    new-instance v3, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;

    invoke-direct/range {v3 .. v10}, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 133
    :catch_0
    move-exception v3

    goto :goto_1

    .line 136
    :catch_1
    move-exception v3

    goto :goto_1
.end method
