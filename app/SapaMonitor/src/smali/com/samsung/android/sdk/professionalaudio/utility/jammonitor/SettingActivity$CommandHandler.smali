.class final Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$1;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 40
    if-eqz p1, :cond_0

    .line 41
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x2710

    if-ne v0, v1, :cond_1

    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v1, v1, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->setSettings(Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;)V

    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->isAudioUSBDeviceAttached()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->setGlobalUSBSettingValueChanged(I)V

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x2711

    if-ne v0, v1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v1, v1, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->setSettings(Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;)V

    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->service:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->setGlobalLatencySettingValueChanged(I)V

    goto :goto_0
.end method
