.class public interface abstract Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract check()V
.end method

.method public abstract onActionCalled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAppActivated(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAppChanged(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAppDeactivated(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAppInstalled(Ljava/lang/String;)V
.end method

.method public abstract onAppUninstalled(Ljava/lang/String;)V
.end method

.method public abstract onTransportMasterChanged(Ljava/lang/String;)V
.end method
