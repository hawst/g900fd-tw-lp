.class Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 689
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    .line 690
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 692
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 697
    iget v0, p1, Landroid/os/Message;->arg2:I

    .line 698
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 729
    const-string v0, "SapaServiceFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    :goto_0
    return-void

    .line 700
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListenerLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->access$0(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 701
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListener:Ljava/util/Map;
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->access$1(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$MessageListener;

    .line 702
    if-eqz v0, :cond_0

    .line 703
    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$MessageListener;->onMessageReceived(ILjava/lang/String;)V

    .line 700
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 709
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientStatusListenerLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->access$2(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 710
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientStatusListener:Ljava/util/Map;
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->access$3(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;

    .line 711
    if-eqz v0, :cond_1

    .line 712
    invoke-interface {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;->onKilled()V

    .line 709
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 719
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListenerLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->access$0(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 720
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->this$0:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListener:Ljava/util/Map;
    invoke-static {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->access$1(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$MessageListener;

    .line 721
    if-eqz v0, :cond_2

    .line 722
    iget v3, p1, Landroid/os/Message;->arg1:I

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/nio/ByteBuffer;

    invoke-interface {v0, v3, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$MessageListener;->onDataReceived(ILjava/nio/ByteBuffer;)V

    .line 719
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    .line 698
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
        0x462 -> :sswitch_2
    .end sparse-switch
.end method
