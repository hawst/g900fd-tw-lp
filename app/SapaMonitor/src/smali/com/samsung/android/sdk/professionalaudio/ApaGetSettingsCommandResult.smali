.class final Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommandResult;
.super Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public getSettings()Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommandResult;->result()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 63
    :goto_0
    return-object v0

    .line 43
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    invoke-direct {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;-><init>()V

    .line 45
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommandResult;->mJsonString:Ljava/lang/String;

    invoke-direct {v0, v3}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONArray;

    .line 47
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 48
    const/4 v3, 0x1

    :goto_1
    if-lt v3, v4, :cond_1

    move-object v0, v2

    .line 63
    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 50
    const-string v6, "defaultlatency"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 51
    const-string v6, "defaultlatency"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->setDefaultLatency(I)V

    .line 53
    :cond_2
    const-string v6, "usbcontrol"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 54
    const-string v6, "usbcontrol"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->setUSBControl(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 59
    :catch_0
    move-exception v0

    .line 60
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object v0, v1

    .line 61
    goto :goto_0
.end method

.method newResult(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommandResult;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommandResult;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
