.class public Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/BReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "SapaMonitor"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 43
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.professionalaudio.action.STOP_DAEMON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/Sapa;-><init>()V

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->initialize(Landroid/content/Context;)V

    .line 51
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;-><init>()V

    .line 52
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaGhost;->isNotificationNeed()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 53
    const-string v0, "SapaMonitor"

    const-string v1, "ApaService Daemon is stopping by user request!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaGhost;->KillAllClientAndDaemon()Z

    .line 57
    invoke-static {p1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->stopSocketRunnable(Landroid/content/Context;)V

    .line 61
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->cancelNotification(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_1
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    goto :goto_0

    .line 95
    :catch_1
    move-exception v0

    goto :goto_0
.end method
