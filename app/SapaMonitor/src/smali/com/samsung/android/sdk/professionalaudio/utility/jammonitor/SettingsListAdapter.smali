.class public Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private description:[Ljava/lang/String;

.field private inflater:Landroid/view/LayoutInflater;

.field private radioButtons:Ljava/util/Vector;

.field selectedSettingValue:I

.field private settingValues:[I

.field private title:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;[ILandroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 31
    iput-object p2, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->title:[Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->description:[Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->settingValues:[I

    .line 34
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 36
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->radioButtons:Ljava/util/Vector;

    .line 38
    invoke-virtual {p0, p5}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 39
    return-void
.end method

.method private changeSetting(I)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->settingValues:[I

    aget v0, v0, p1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->selectedSettingValue:I

    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->notifyDataSetChanged()V

    .line 74
    return-void
.end method


# virtual methods
.method getAllItemHeight(Landroid/view/ViewGroup;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 125
    move v0, v1

    move v2, v1

    .line 126
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 127
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, p1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 128
    if-eqz v3, :cond_0

    .line 129
    invoke-virtual {v3, v1, v1}, Landroid/view/View;->measure(II)V

    .line 130
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 126
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 141
    :cond_1
    return v2
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->title:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->title:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 104
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedSettingValue()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->selectedSettingValue:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 146
    .line 148
    if-nez p2, :cond_0

    .line 149
    :try_start_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 150
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->inflater:Landroid/view/LayoutInflater;

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030001

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 162
    :cond_0
    :goto_0
    if-nez p2, :cond_2

    move-object p2, v0

    .line 218
    :goto_1
    return-object p2

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->inflater:Landroid/view/LayoutInflater;

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->inflater:Landroid/view/LayoutInflater;

    const/high16 v2, 0x7f030000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    goto :goto_0

    .line 159
    :catch_0
    move-exception v1

    move-object p2, v0

    .line 160
    goto :goto_1

    .line 165
    :cond_2
    const/high16 v0, 0x7f090000

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 166
    if-eqz v0, :cond_3

    .line 167
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->title:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    :cond_3
    const v0, 0x7f090001

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 170
    if-eqz v0, :cond_4

    .line 171
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->description:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    :cond_4
    const v0, 0x7f090002

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 174
    if-eqz v0, :cond_5

    .line 175
    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setFocusable(Z)V

    .line 176
    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 177
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->settingValues:[I

    aget v1, v1, p1

    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->selectedSettingValue:I

    if-ne v1, v2, :cond_6

    .line 178
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 182
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->radioButtons:Ljava/util/Vector;

    if-eqz v1, :cond_5

    .line 183
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->radioButtons:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 192
    :cond_5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 180
    :cond_6
    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2
.end method

.method public initializeValues(I)V
    .locals 0

    .prologue
    .line 47
    iput p1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->selectedSettingValue:I

    .line 67
    return-void
.end method

.method public setSelectedItem(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 81
    if-nez p1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->radioButtons:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 87
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 89
    :cond_1
    const v0, 0x7f090002

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 90
    if-eqz v0, :cond_2

    .line 91
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 93
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 94
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->changeSetting(I)V

    goto :goto_0
.end method
