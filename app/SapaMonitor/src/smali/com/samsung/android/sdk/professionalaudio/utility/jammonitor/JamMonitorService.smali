.class public Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static isNotificationPosting:Z = false

.field private static final notificationID:I = 0x2156


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mPostRunnable:Ljava/lang/Runnable;

.field private final mRefreshTimeMs:I

.field private final mRefreshWaitingTimeMs:I

.field mRunnable:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

.field mService:Lcom/samsung/android/sdk/professionalaudio/SapaService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->isNotificationPosting:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "JamMonitor"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;-><init>(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 33
    const v0, 0xea60

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mRefreshTimeMs:I

    .line 35
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mRefreshWaitingTimeMs:I

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mService:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    .line 397
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService$1;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mPostRunnable:Ljava/lang/Runnable;

    .line 26
    return-void
.end method

.method static cancelNotification(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 199
    if-nez p0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 203
    if-eqz v0, :cond_0

    .line 204
    const/16 v1, 0x2156

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 205
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->isNotificationPosting:Z

    goto :goto_0
.end method

.method private notifyDaemonAlive3()V
    .locals 7

    .prologue
    const v6, 0x7f06000f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 263
    sget-boolean v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->isNotificationPosting:Z

    if-ne v0, v5, :cond_1

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 269
    if-eqz v0, :cond_0

    .line 274
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 275
    const-string v2, "from"

    const-string v3, "sapamonitor"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    const v2, 0x24008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 280
    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 282
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f060010

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f020002

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    new-instance v2, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v2}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 292
    const/16 v2, 0x2156

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 293
    sput-boolean v5, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->isNotificationPosting:Z

    goto :goto_0
.end method

.method public static stopSocketRunnable(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 416
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    .prologue
    .line 436
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 437
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mHandler:Landroid/os/Handler;

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mService:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    if-nez v0, :cond_1

    .line 440
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/Sapa;-><init>()V

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->initialize(Landroid/content/Context;)V

    .line 441
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mService:Lcom/samsung/android/sdk/professionalaudio/SapaService;

    .line 445
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mRunnable:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    if-nez v0, :cond_2

    .line 447
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mRunnable:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    .line 450
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mRunnable:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 460
    :cond_2
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 461
    :goto_0
    return-void

    .line 453
    :catch_0
    move-exception v0

    .line 454
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 456
    :catch_1
    move-exception v0

    .line 457
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 346
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.professionalaudio.action.START_MONITOR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 350
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 355
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaGhost;->isNotificationNeed()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 356
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->notifyDaemonAlive3()V

    .line 357
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mPostRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 359
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->mPostRunnable:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 362
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamMonitorService;->cancelNotification(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 378
    :catch_0
    move-exception v0

    .line 379
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 381
    :catch_1
    move-exception v0

    .line 382
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
