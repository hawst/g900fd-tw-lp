.class Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;
.super Landroid/database/DataSetObserver;
.source "SourceFile"


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    const/16 v2, 0x2711

    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v1, v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->latencyListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->access$500(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingsListAdapter;->getSelectedSettingValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->setDefaultLatency(I)V

    .line 292
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->commandHandler:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->commandHandler:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->removeMessages(I)V

    .line 294
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->commandHandler:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 295
    iput v2, v0, Landroid/os/Message;->what:I

    .line 296
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    iget-object v1, v1, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->settings:Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->getDefaultLatency()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 297
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$6;->this$0:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->commandHandler:Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;
    invoke-static {v1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;->access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity;)Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/SettingActivity$CommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 299
    :cond_0
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 300
    return-void
.end method
