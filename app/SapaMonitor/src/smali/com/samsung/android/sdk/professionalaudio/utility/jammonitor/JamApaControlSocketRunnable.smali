.class public Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final SOCKET_NAME:Ljava/lang/String; = "professionalaudio.apaservice.apacontrol"

.field private static final TAG:Ljava/lang/String; = "SapaMonitor"


# instance fields
.field private mContext:Ljava/lang/ref/WeakReference;

.field private mFlag:Z

.field private mHandler:Ljava/lang/ref/WeakReference;

.field private mServerSocket:Landroid/net/LocalServerSocket;


# direct methods
.method constructor <init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mServerSocket:Landroid/net/LocalServerSocket;

    .line 21
    iput-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mContext:Ljava/lang/ref/WeakReference;

    .line 22
    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mFlag:Z

    .line 27
    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mFlag:Z

    .line 28
    iput-object p2, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mHandler:Ljava/lang/ref/WeakReference;

    .line 29
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mContext:Ljava/lang/ref/WeakReference;

    .line 31
    :try_start_0
    new-instance v0, Landroid/net/LocalServerSocket;

    const-string v1, "professionalaudio.apaservice.apacontrol"

    invoke-direct {v0, v1}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mServerSocket:Landroid/net/LocalServerSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    goto :goto_0

    .line 32
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mHandler:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mContext:Ljava/lang/ref/WeakReference;

    return-object v0
.end method


# virtual methods
.method handleAMessage(Landroid/net/LocalSocket;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 68
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;-><init>(Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;)V

    new-array v1, v3, [Landroid/net/LocalSocket;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 142
    return v3
.end method

.method public run()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mServerSocket:Landroid/net/LocalServerSocket;

    if-nez v0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mFlag:Z

    if-nez v0, :cond_0

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mServerSocket:Landroid/net/LocalServerSocket;

    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->handleAMessage(Landroid/net/LocalSocket;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 57
    :catch_0
    move-exception v0

    goto :goto_0

    .line 59
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public setToStop()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/utility/jammonitor/JamApaControlSocketRunnable;->mFlag:Z

    .line 41
    return-void
.end method
