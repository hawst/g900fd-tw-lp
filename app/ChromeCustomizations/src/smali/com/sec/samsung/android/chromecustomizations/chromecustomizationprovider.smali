.class public Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;
.super Landroid/content/ContentProvider;
.source "chromecustomizationprovider.java"


# static fields
.field private static HOMEPAGE_URI:Ljava/lang/String; = null

.field private static final URI_MATCHER:Landroid/content/UriMatcher;

.field private static final URI_MATCH_DISABLE_BOOKMARKEDITING:I = 0x2

.field private static final URI_MATCH_DISABLE_INCOGNITO_MODE:I = 0x1

.field private static final URI_MATCH_HOMEPAGE:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    const-string v0, "chrome://newtab/#most_visited"

    sput-object v0, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->HOMEPAGE_URI:Ljava/lang/String;

    .line 27
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 29
    sget-object v0, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.android.partnerbrowsercustomizations"

    const-string v2, "homepage"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 31
    sget-object v0, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.android.partnerbrowsercustomizations"

    const-string v2, "disableincognitomode"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 33
    sget-object v0, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.android.partnerbrowsercustomizations"

    const-string v2, "disablebookmarksediting"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 35
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private getHomepageUri()Ljava/lang/String;
    .locals 4

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ChromeCustomizationProviderPref"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 149
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "ChromeHomepage"

    sget-object v2, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->HOMEPAGE_URI:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private readBookmarksEditing()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ChromeCustomizationProviderPref"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 159
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "DisableBookmarksEditing"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method private readIncognito()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 153
    invoke-virtual {p0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ChromeCustomizationProviderPref"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 154
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "DisableIncognitoMode"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private writeBookmark(I)V
    .locals 5
    .param p1, "value"    # I

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "ChromeCustomizationProviderPref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 142
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 143
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "DisableBookmarksEditing"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 144
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 145
    return-void
.end method

.method private writeIncognito(I)V
    .locals 5
    .param p1, "value"    # I

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "ChromeCustomizationProviderPref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 133
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 134
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "DisableIncognitoMode"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 135
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 105
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 41
    sget-object v0, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 49
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 43
    :pswitch_0
    const-string v0, "vnd.android.cursor.item/partnerhomepage"

    goto :goto_0

    .line 45
    :pswitch_1
    const-string v0, "vnd.android.cursor.item/partnerdisableincognitomode"

    goto :goto_0

    .line 47
    :pswitch_2
    const-string v0, "vnd.android.cursor.item/disablebookmarksediting"

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 110
    sget-object v1, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 127
    const/4 p1, 0x0

    .end local p1    # "uri":Landroid/net/Uri;
    :goto_0
    return-object p1

    .line 114
    .restart local p1    # "uri":Landroid/net/Uri;
    :pswitch_0
    const/4 v0, -0x1

    .line 115
    .local v0, "value":I
    const-string v1, "user_mode"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 116
    invoke-direct {p0, v0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->writeIncognito(I)V

    goto :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 58
    .local v0, "homeUrl":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Web_SetHomepageURL"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    sput-object v0, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->HOMEPAGE_URI:Ljava/lang/String;

    .line 63
    :cond_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Web_SetHomepageURLChrome"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 65
    sput-object v0, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->HOMEPAGE_URI:Ljava/lang/String;

    .line 67
    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 73
    sget-object v1, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 93
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 76
    :pswitch_0
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "homepage"

    aput-object v2, v1, v4

    invoke-direct {v0, v1, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 77
    .local v0, "cursor":Landroid/database/MatrixCursor;
    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->getHomepageUri()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    .end local v0    # "cursor":Landroid/database/MatrixCursor;
    :pswitch_1
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "disableincognitomode"

    aput-object v2, v1, v4

    invoke-direct {v0, v1, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 83
    .restart local v0    # "cursor":Landroid/database/MatrixCursor;
    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->readIncognito()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    .end local v0    # "cursor":Landroid/database/MatrixCursor;
    :pswitch_2
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "disablebookmarksediting"

    aput-object v2, v1, v4

    invoke-direct {v0, v1, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 89
    .restart local v0    # "cursor":Landroid/database/MatrixCursor;
    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/samsung/android/chromecustomizations/chromecustomizationprovider;->readBookmarksEditing()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 100
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
