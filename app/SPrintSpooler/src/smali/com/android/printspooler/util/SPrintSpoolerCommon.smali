.class public Lcom/android/printspooler/util/SPrintSpoolerCommon;
.super Ljava/lang/Object;
.source "SPrintSpoolerCommon.java"


# direct methods
.method public static clearSelectedPrinter(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const-string v1, "current_printer"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 58
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 59
    return-void
.end method

.method public static readSelectedPrinter(Landroid/content/Context;)Landroid/print/PrinterInfo;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 40
    const-string v4, "current_printer"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 42
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v4, "selected_printer_name"

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 44
    .local v3, "printerName":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 52
    :goto_0
    return-object v1

    .line 46
    :cond_0
    new-instance v2, Landroid/print/PrinterId;

    const-string v4, "selected_printer_component"

    const-string v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    const-string v5, "selected_printer_ID"

    const-string v6, ""

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Landroid/print/PrinterId;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 49
    .local v2, "printerId":Landroid/print/PrinterId;
    new-instance v4, Landroid/print/PrinterInfo$Builder;

    const/4 v5, 0x1

    invoke-direct {v4, v2, v3, v5}, Landroid/print/PrinterInfo$Builder;-><init>(Landroid/print/PrinterId;Ljava/lang/String;I)V

    invoke-virtual {v4}, Landroid/print/PrinterInfo$Builder;->build()Landroid/print/PrinterInfo;

    move-result-object v1

    .line 51
    .local v1, "printer":Landroid/print/PrinterInfo;
    invoke-static {p0}, Lcom/android/printspooler/util/SPrintSpoolerCommon;->clearSelectedPrinter(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static saveSelectedPrinter(Landroid/content/Context;Landroid/print/PrinterInfo;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 27
    const-string v3, "current_printer"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 29
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v2

    .line 30
    .local v2, "printerId":Landroid/print/PrinterId;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 31
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "selected_printer_name"

    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "selected_printer_ID"

    invoke-virtual {v2}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "selected_printer_component"

    invoke-virtual {v2}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 36
    const/4 v3, 0x1

    return v3
.end method
