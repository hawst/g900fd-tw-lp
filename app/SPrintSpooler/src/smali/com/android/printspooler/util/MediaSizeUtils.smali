.class public final Lcom/android/printspooler/util/MediaSizeUtils;
.super Ljava/lang/Object;
.source "MediaSizeUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;
    }
.end annotation


# static fields
.field private static sMediaSizeToStandardMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/print/PrintAttributes$MediaSize;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic access$000(Landroid/content/Context;Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/print/PrintAttributes$MediaSize;

    .prologue
    .line 31
    invoke-static {p0, p1}, Lcom/android/printspooler/util/MediaSizeUtils;->getStandardForMediaSize(Landroid/content/Context;Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDefault(Landroid/content/Context;)Landroid/print/PrintAttributes$MediaSize;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const v1, 0x7f090004

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "mediaSizeId":Ljava/lang/String;
    invoke-static {v0}, Landroid/print/PrintAttributes$MediaSize;->getStandardMediaSizeById(Ljava/lang/String;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v1

    return-object v1
.end method

.method private static getStandardForMediaSize(Landroid/content/Context;Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mediaSize"    # Landroid/print/PrintAttributes$MediaSize;

    .prologue
    .line 51
    sget-object v7, Lcom/android/printspooler/util/MediaSizeUtils;->sMediaSizeToStandardMap:Ljava/util/Map;

    if-nez v7, :cond_0

    .line 52
    new-instance v7, Landroid/util/ArrayMap;

    invoke-direct {v7}, Landroid/util/ArrayMap;-><init>()V

    sput-object v7, Lcom/android/printspooler/util/MediaSizeUtils;->sMediaSizeToStandardMap:Ljava/util/Map;

    .line 53
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/high16 v8, 0x7f040000

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 55
    .local v4, "mediaSizeToStandardMapValues":[Ljava/lang/String;
    array-length v3, v4

    .line 56
    .local v3, "mediaSizeToStandardCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 57
    aget-object v2, v4, v0

    .line 58
    .local v2, "mediaSizeId":Ljava/lang/String;
    invoke-static {v2}, Landroid/print/PrintAttributes$MediaSize;->getStandardMediaSizeById(Ljava/lang/String;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v1

    .line 59
    .local v1, "key":Landroid/print/PrintAttributes$MediaSize;
    add-int/lit8 v7, v0, 0x1

    aget-object v6, v4, v7

    .line 60
    .local v6, "value":Ljava/lang/String;
    sget-object v7, Lcom/android/printspooler/util/MediaSizeUtils;->sMediaSizeToStandardMap:Ljava/util/Map;

    invoke-interface {v7, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 63
    .end local v0    # "i":I
    .end local v1    # "key":Landroid/print/PrintAttributes$MediaSize;
    .end local v2    # "mediaSizeId":Ljava/lang/String;
    .end local v3    # "mediaSizeToStandardCount":I
    .end local v4    # "mediaSizeToStandardMapValues":[Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_0
    sget-object v7, Lcom/android/printspooler/util/MediaSizeUtils;->sMediaSizeToStandardMap:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 64
    .local v5, "standard":Ljava/lang/String;
    if-eqz v5, :cond_1

    .end local v5    # "standard":Ljava/lang/String;
    :goto_1
    return-object v5

    .restart local v5    # "standard":Ljava/lang/String;
    :cond_1
    const/high16 v7, 0x7f090000

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method
