.class public final Lcom/android/printspooler/util/PageRangeUtils;
.super Ljava/lang/Object;
.source "PageRangeUtils.java"


# static fields
.field private static final ALL_PAGES_RANGE:[Landroid/print/PageRange;

.field private static final sComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/print/PageRange;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/print/PageRange;

    const/4 v1, 0x0

    sget-object v2, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    .line 32
    new-instance v0, Lcom/android/printspooler/util/PageRangeUtils$1;

    invoke-direct {v0}, Lcom/android/printspooler/util/PageRangeUtils$1;-><init>()V

    sput-object v0, Lcom/android/printspooler/util/PageRangeUtils;->sComparator:Ljava/util/Comparator;

    return-void
.end method

.method public static asAbsoluteRange(Landroid/print/PageRange;I)Landroid/print/PageRange;
    .locals 2
    .param p0, "pageRange"    # Landroid/print/PageRange;
    .param p1, "pageCount"    # I

    .prologue
    .line 198
    sget-object v0, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    invoke-virtual {v0, p0}, Landroid/print/PageRange;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    if-gtz p1, :cond_0

    .line 200
    const/4 p1, 0x1

    .line 201
    :cond_0
    new-instance p0, Landroid/print/PageRange;

    .end local p0    # "pageRange":Landroid/print/PageRange;
    const/4 v0, 0x0

    add-int/lit8 v1, p1, -0x1

    invoke-direct {p0, v0, v1}, Landroid/print/PageRange;-><init>(II)V

    .line 203
    :cond_1
    return-object p0
.end method

.method public static computePrintedPages([Landroid/print/PageRange;[Landroid/print/PageRange;I)[Landroid/print/PageRange;
    .locals 2
    .param p0, "requestedPages"    # [Landroid/print/PageRange;
    .param p1, "writtenPages"    # [Landroid/print/PageRange;
    .param p2, "pageCount"    # I

    .prologue
    .line 242
    sget-object v1, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    invoke-static {p0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 244
    sget-object p0, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    .line 269
    .end local p0    # "requestedPages":[Landroid/print/PageRange;
    :cond_0
    :goto_0
    return-object p0

    .line 245
    .restart local p0    # "requestedPages":[Landroid/print/PageRange;
    :cond_1
    invoke-static {p1, p0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 248
    sget-object p0, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    goto :goto_0

    .line 249
    :cond_2
    sget-object v1, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    invoke-static {p1, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    invoke-static {p1, p0, p2}, Lcom/android/printspooler/util/PageRangeUtils;->contains([Landroid/print/PageRange;[Landroid/print/PageRange;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 258
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Landroid/print/PageRange;->getStart()I

    move-result v1

    neg-int v0, v1

    .line 259
    .local v0, "offset":I
    invoke-static {p0, v0}, Lcom/android/printspooler/util/PageRangeUtils;->offset([Landroid/print/PageRange;I)V

    goto :goto_0

    .line 261
    .end local v0    # "offset":I
    :cond_3
    sget-object v1, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    invoke-static {p0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p1, p2}, Lcom/android/printspooler/util/PageRangeUtils;->isAllPages([Landroid/print/PageRange;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 266
    sget-object p0, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    goto :goto_0

    .line 269
    :cond_4
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static contains([Landroid/print/PageRange;I)Z
    .locals 4
    .param p0, "pageRanges"    # [Landroid/print/PageRange;
    .param p1, "pageIndex"    # I

    .prologue
    .line 51
    array-length v2, p0

    .line 52
    .local v2, "rangeCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 53
    aget-object v1, p0, v0

    .line 54
    .local v1, "pageRange":Landroid/print/PageRange;
    invoke-virtual {v1, p1}, Landroid/print/PageRange;->contains(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    const/4 v3, 0x1

    .line 58
    .end local v1    # "pageRange":Landroid/print/PageRange;
    :goto_1
    return v3

    .line 52
    .restart local v1    # "pageRange":Landroid/print/PageRange;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    .end local v1    # "pageRange":Landroid/print/PageRange;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static contains([Landroid/print/PageRange;[Landroid/print/PageRange;I)Z
    .locals 10
    .param p0, "ourRanges"    # [Landroid/print/PageRange;
    .param p1, "otherRanges"    # [Landroid/print/PageRange;
    .param p2, "pageCount"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 70
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v6, v7

    .line 104
    :cond_1
    :goto_0
    return v6

    .line 74
    :cond_2
    sget-object v8, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    invoke-static {p0, v8}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 78
    sget-object v8, Lcom/android/printspooler/util/PageRangeUtils;->ALL_PAGES_RANGE:[Landroid/print/PageRange;

    invoke-static {p1, v8}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 79
    new-instance v8, Landroid/print/PageRange;

    add-int/lit8 v9, p2, -0x1

    invoke-direct {v8, v7, v9}, Landroid/print/PageRange;-><init>(II)V

    aput-object v8, p1, v7

    .line 82
    :cond_3
    invoke-static {p0}, Lcom/android/printspooler/util/PageRangeUtils;->normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;

    move-result-object p0

    .line 83
    invoke-static {p1}, Lcom/android/printspooler/util/PageRangeUtils;->normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;

    move-result-object p1

    .line 88
    const/4 v2, 0x0

    .line 89
    .local v2, "otherRangeIdx":I
    array-length v4, p0

    .line 90
    .local v4, "ourRangeCount":I
    array-length v1, p1

    .line 91
    .local v1, "otherRangeCount":I
    const/4 v5, 0x0

    .local v5, "ourRangeIdx":I
    :goto_1
    if-ge v5, v4, :cond_8

    .line 92
    aget-object v3, p0, v5

    .line 93
    .local v3, "ourRange":Landroid/print/PageRange;
    :goto_2
    if-ge v2, v1, :cond_4

    .line 94
    aget-object v0, p1, v2

    .line 95
    .local v0, "otherRange":Landroid/print/PageRange;
    invoke-virtual {v0}, Landroid/print/PageRange;->getStart()I

    move-result v8

    invoke-virtual {v3}, Landroid/print/PageRange;->getEnd()I

    move-result v9

    if-le v8, v9, :cond_5

    .line 91
    .end local v0    # "otherRange":Landroid/print/PageRange;
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 98
    .restart local v0    # "otherRange":Landroid/print/PageRange;
    :cond_5
    invoke-virtual {v0}, Landroid/print/PageRange;->getStart()I

    move-result v8

    invoke-virtual {v3}, Landroid/print/PageRange;->getStart()I

    move-result v9

    if-lt v8, v9, :cond_6

    invoke-virtual {v0}, Landroid/print/PageRange;->getEnd()I

    move-result v8

    invoke-virtual {v3}, Landroid/print/PageRange;->getEnd()I

    move-result v9

    if-le v8, v9, :cond_7

    :cond_6
    move v6, v7

    .line 100
    goto :goto_0

    .line 93
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 104
    .end local v0    # "otherRange":Landroid/print/PageRange;
    .end local v3    # "ourRange":Landroid/print/PageRange;
    :cond_8
    if-ge v2, v1, :cond_1

    move v6, v7

    goto :goto_0
.end method

.method public static getNormalizedPageCount([Landroid/print/PageRange;I)I
    .locals 5
    .param p0, "pageRanges"    # [Landroid/print/PageRange;
    .param p1, "layoutPageCount"    # I

    .prologue
    .line 183
    const/4 v1, 0x0

    .line 184
    .local v1, "pageCount":I
    if-eqz p0, :cond_1

    .line 185
    array-length v3, p0

    .line 186
    .local v3, "pageRangeCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 187
    aget-object v2, p0, v0

    .line 188
    .local v2, "pageRange":Landroid/print/PageRange;
    sget-object v4, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    invoke-virtual {v4, v2}, Landroid/print/PageRange;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 194
    .end local v0    # "i":I
    .end local v2    # "pageRange":Landroid/print/PageRange;
    .end local v3    # "pageRangeCount":I
    .end local p1    # "layoutPageCount":I
    :goto_1
    return p1

    .line 191
    .restart local v0    # "i":I
    .restart local v2    # "pageRange":Landroid/print/PageRange;
    .restart local v3    # "pageRangeCount":I
    .restart local p1    # "layoutPageCount":I
    :cond_0
    invoke-virtual {v2}, Landroid/print/PageRange;->getSize()I

    move-result v4

    add-int/2addr v1, v4

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v0    # "i":I
    .end local v2    # "pageRange":Landroid/print/PageRange;
    .end local v3    # "pageRangeCount":I
    :cond_1
    move p1, v1

    .line 194
    goto :goto_1
.end method

.method public static isAllPages(Landroid/print/PageRange;)Z
    .locals 1
    .param p0, "pageRange"    # Landroid/print/PageRange;

    .prologue
    .line 218
    sget-object v0, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    invoke-virtual {v0, p0}, Landroid/print/PageRange;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isAllPages(Landroid/print/PageRange;I)Z
    .locals 2
    .param p0, "pageRanges"    # Landroid/print/PageRange;
    .param p1, "pageCount"    # I

    .prologue
    .line 233
    invoke-virtual {p0}, Landroid/print/PageRange;->getStart()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/print/PageRange;->getEnd()I

    move-result v0

    add-int/lit8 v1, p1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAllPages([Landroid/print/PageRange;)Z
    .locals 4
    .param p0, "pageRanges"    # [Landroid/print/PageRange;

    .prologue
    .line 207
    array-length v2, p0

    .line 208
    .local v2, "pageRangeCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 209
    aget-object v1, p0, v0

    .line 210
    .local v1, "pageRange":Landroid/print/PageRange;
    invoke-static {v1}, Lcom/android/printspooler/util/PageRangeUtils;->isAllPages(Landroid/print/PageRange;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    const/4 v3, 0x1

    .line 214
    .end local v1    # "pageRange":Landroid/print/PageRange;
    :goto_1
    return v3

    .line 208
    .restart local v1    # "pageRange":Landroid/print/PageRange;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    .end local v1    # "pageRange":Landroid/print/PageRange;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static isAllPages([Landroid/print/PageRange;I)Z
    .locals 4
    .param p0, "pageRanges"    # [Landroid/print/PageRange;
    .param p1, "pageCount"    # I

    .prologue
    .line 222
    array-length v2, p0

    .line 223
    .local v2, "pageRangeCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 224
    aget-object v1, p0, v0

    .line 225
    .local v1, "pageRange":Landroid/print/PageRange;
    invoke-static {v1, p1}, Lcom/android/printspooler/util/PageRangeUtils;->isAllPages(Landroid/print/PageRange;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 226
    const/4 v3, 0x1

    .line 229
    .end local v1    # "pageRange":Landroid/print/PageRange;
    :goto_1
    return v3

    .line 223
    .restart local v1    # "pageRange":Landroid/print/PageRange;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    .end local v1    # "pageRange":Landroid/print/PageRange;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;
    .locals 13
    .param p0, "pageRanges"    # [Landroid/print/PageRange;

    .prologue
    const/4 v6, 0x0

    .line 116
    if-nez p0, :cond_1

    move-object p0, v6

    .line 154
    .end local p0    # "pageRanges":[Landroid/print/PageRange;
    .local v7, "oldRangeCount":I
    :cond_0
    :goto_0
    return-object p0

    .line 120
    .end local v7    # "oldRangeCount":I
    .restart local p0    # "pageRanges":[Landroid/print/PageRange;
    :cond_1
    array-length v7, p0

    .line 121
    .restart local v7    # "oldRangeCount":I
    const/4 v8, 0x1

    if-le v7, v8, :cond_0

    .line 125
    sget-object v8, Lcom/android/printspooler/util/PageRangeUtils;->sComparator:Ljava/util/Comparator;

    invoke-static {p0, v8}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 127
    const/4 v2, 0x1

    .line 128
    .local v2, "newRangeCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    add-int/lit8 v8, v7, -0x1

    if-ge v1, v8, :cond_3

    .line 129
    aget-object v0, p0, v1

    .line 130
    .local v0, "currentRange":Landroid/print/PageRange;
    add-int/lit8 v8, v1, 0x1

    aget-object v3, p0, v8

    .line 131
    .local v3, "nextRange":Landroid/print/PageRange;
    invoke-virtual {v0}, Landroid/print/PageRange;->getEnd()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v3}, Landroid/print/PageRange;->getStart()I

    move-result v9

    if-lt v8, v9, :cond_2

    .line 132
    aput-object v6, p0, v1

    .line 133
    add-int/lit8 v8, v1, 0x1

    new-instance v9, Landroid/print/PageRange;

    invoke-virtual {v0}, Landroid/print/PageRange;->getStart()I

    move-result v10

    invoke-virtual {v0}, Landroid/print/PageRange;->getEnd()I

    move-result v11

    invoke-virtual {v3}, Landroid/print/PageRange;->getEnd()I

    move-result v12

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-direct {v9, v10, v11}, Landroid/print/PageRange;-><init>(II)V

    aput-object v9, p0, v8

    .line 128
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 136
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 140
    .end local v0    # "currentRange":Landroid/print/PageRange;
    .end local v3    # "nextRange":Landroid/print/PageRange;
    :cond_3
    if-eq v2, v7, :cond_0

    .line 144
    const/4 v5, 0x0

    .line 145
    .local v5, "normalRangeIndex":I
    new-array v6, v2, [Landroid/print/PageRange;

    .line 146
    .local v6, "normalRanges":[Landroid/print/PageRange;
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v7, :cond_5

    .line 147
    aget-object v4, p0, v1

    .line 148
    .local v4, "normalRange":Landroid/print/PageRange;
    if-eqz v4, :cond_4

    .line 149
    aput-object v4, v6, v5

    .line 150
    add-int/lit8 v5, v5, 0x1

    .line 146
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .end local v4    # "normalRange":Landroid/print/PageRange;
    :cond_5
    move-object p0, v6

    .line 154
    goto :goto_0
.end method

.method public static offset([Landroid/print/PageRange;I)V
    .locals 5
    .param p0, "pageRanges"    # [Landroid/print/PageRange;
    .param p1, "offset"    # I

    .prologue
    .line 164
    if-nez p1, :cond_1

    .line 173
    :cond_0
    return-void

    .line 167
    :cond_1
    array-length v2, p0

    .line 168
    .local v2, "pageRangeCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 169
    aget-object v4, p0, v1

    invoke-virtual {v4}, Landroid/print/PageRange;->getStart()I

    move-result v4

    add-int v3, v4, p1

    .line 170
    .local v3, "start":I
    aget-object v4, p0, v1

    invoke-virtual {v4}, Landroid/print/PageRange;->getEnd()I

    move-result v4

    add-int v0, v4, p1

    .line 171
    .local v0, "end":I
    new-instance v4, Landroid/print/PageRange;

    invoke-direct {v4, v3, v0}, Landroid/print/PageRange;-><init>(II)V

    aput-object v4, p0, v1

    .line 168
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
