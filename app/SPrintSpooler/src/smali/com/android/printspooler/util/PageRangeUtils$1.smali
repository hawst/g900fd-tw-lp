.class final Lcom/android/printspooler/util/PageRangeUtils$1;
.super Ljava/lang/Object;
.source "PageRangeUtils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/util/PageRangeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/print/PageRange;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/print/PageRange;Landroid/print/PageRange;)I
    .locals 2
    .param p1, "lhs"    # Landroid/print/PageRange;
    .param p2, "rhs"    # Landroid/print/PageRange;

    .prologue
    .line 35
    invoke-virtual {p1}, Landroid/print/PageRange;->getStart()I

    move-result v0

    invoke-virtual {p2}, Landroid/print/PageRange;->getStart()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p1, Landroid/print/PageRange;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/print/PageRange;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/android/printspooler/util/PageRangeUtils$1;->compare(Landroid/print/PageRange;Landroid/print/PageRange;)I

    move-result v0

    return v0
.end method
