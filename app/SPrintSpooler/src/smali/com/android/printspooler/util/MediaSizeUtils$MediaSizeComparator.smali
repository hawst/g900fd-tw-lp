.class public final Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;
.super Ljava/lang/Object;
.source "MediaSizeUtils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/util/MediaSizeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MediaSizeComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/print/PrintAttributes$MediaSize;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;->mContext:Landroid/content/Context;

    .line 78
    return-void
.end method


# virtual methods
.method public compare(Landroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$MediaSize;)I
    .locals 5
    .param p1, "lhs"    # Landroid/print/PrintAttributes$MediaSize;
    .param p2, "rhs"    # Landroid/print/PrintAttributes$MediaSize;

    .prologue
    .line 82
    iget-object v3, p0, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;->mContext:Landroid/content/Context;

    const v4, 0x7f090005

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "currentStandard":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;->mContext:Landroid/content/Context;

    # invokes: Lcom/android/printspooler/util/MediaSizeUtils;->getStandardForMediaSize(Landroid/content/Context;Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;
    invoke-static {v3, p1}, Lcom/android/printspooler/util/MediaSizeUtils;->access$000(Landroid/content/Context;Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "lhsStandard":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;->mContext:Landroid/content/Context;

    # invokes: Lcom/android/printspooler/util/MediaSizeUtils;->getStandardForMediaSize(Landroid/content/Context;Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;
    invoke-static {v3, p2}, Lcom/android/printspooler/util/MediaSizeUtils;->access$000(Landroid/content/Context;Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "rhsStandard":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 89
    const/4 v3, -0x1

    .line 100
    :goto_0
    return v3

    .line 91
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 92
    const/4 v3, 0x1

    goto :goto_0

    .line 95
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 97
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 100
    :cond_2
    iget-object v3, p0, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/print/PrintAttributes$MediaSize;->getLabel(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/print/PrintAttributes$MediaSize;->getLabel(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 73
    check-cast p1, Landroid/print/PrintAttributes$MediaSize;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/print/PrintAttributes$MediaSize;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;->compare(Landroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$MediaSize;)I

    move-result v0

    return v0
.end method
