.class public final Lcom/android/printspooler/util/BitmapSerializeUtils;
.super Ljava/lang/Object;
.source "BitmapSerializeUtils.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "printspooler_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method private static native nativeReadBitmapPixels(Landroid/graphics/Bitmap;I)V
.end method

.method private static native nativeWriteBitmapPixels(Landroid/graphics/Bitmap;I)V
.end method

.method public static readBitmapPixels(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "source"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/printspooler/util/BitmapSerializeUtils;->nativeReadBitmapPixels(Landroid/graphics/Bitmap;I)V

    .line 45
    return-void
.end method

.method public static writeBitmapPixels(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "destination"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 54
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/printspooler/util/BitmapSerializeUtils;->nativeWriteBitmapPixels(Landroid/graphics/Bitmap;I)V

    .line 55
    return-void
.end method
