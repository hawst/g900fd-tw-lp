.class Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;
.super Landroid/os/AsyncTask;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->open(Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

.field final synthetic val$callback:Ljava/lang/Runnable;

.field final synthetic val$source:Landroid/os/ParcelFileDescriptor;


# direct methods
.method constructor <init>(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 467
    iput-object p1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    iput-object p2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->val$source:Landroid/os/ParcelFileDescriptor;

    iput-object p3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 477
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$800(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 478
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mRenderer:Lcom/android/printspooler/renderer/IPdfRenderer;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$900(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/renderer/IPdfRenderer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    .line 480
    :try_start_1
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$800(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 481
    :catch_0
    move-exception v1

    goto :goto_0

    .line 486
    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mRenderer:Lcom/android/printspooler/renderer/IPdfRenderer;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$900(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/renderer/IPdfRenderer;

    move-result-object v1

    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->val$source:Landroid/os/ParcelFileDescriptor;

    invoke-interface {v1, v3}, Lcom/android/printspooler/renderer/IPdfRenderer;->openDocument(Landroid/os/ParcelFileDescriptor;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 493
    :try_start_3
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->val$source:Landroid/os/ParcelFileDescriptor;

    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    return-object v1

    .line 487
    :catch_1
    move-exception v0

    .line 488
    .local v0, "re":Landroid/os/RemoteException;
    :try_start_4
    const-string v1, "PageContentRepository"

    const-string v3, "Cannot open PDF document"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    const/4 v1, -0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v1

    .line 493
    :try_start_5
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->val$source:Landroid/os/ParcelFileDescriptor;

    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    monitor-exit v2

    goto :goto_1

    .line 495
    .end local v0    # "re":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v1

    .line 493
    :catchall_1
    move-exception v1

    :try_start_6
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->val$source:Landroid/os/ParcelFileDescriptor;

    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 467
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "pageCount"    # Ljava/lang/Integer;

    .prologue
    .line 500
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    .line 501
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mOnMalformedPdfFileListener:Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1000(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;->onMalformedPdfFile()V

    .line 502
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    const/4 v1, -0x1

    # setter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I
    invoke-static {v0, v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1102(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;I)I

    .line 503
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x3

    if-ne v0, v1, :cond_2

    .line 504
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mOnMalformedPdfFileListener:Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1000(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;->onRendererBusy()V

    .line 508
    :goto_0
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->val$callback:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 509
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 511
    :cond_1
    return-void

    .line 506
    :cond_2
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # setter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I
    invoke-static {v0, v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1102(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;I)I

    goto :goto_0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 467
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 470
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.printspooler.renderer.ACTION_GET_RENDERER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 471
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$700(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/android/printspooler/renderer/PdfManipulationService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 472
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$700(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 473
    return-void
.end method
