.class public Lcom/android/printspooler/model/PrintSpoolerProvider;
.super Ljava/lang/Object;
.source "PrintSpoolerProvider.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private mBound:Z

.field private final mCallback:Ljava/lang/Runnable;

.field private final mContext:Landroid/content/Context;

.field private mSpooler:Lcom/android/printspooler/model/PrintSpoolerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Ljava/lang/Runnable;

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-boolean v3, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mBound:Z

    .line 36
    iput-object p1, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mCallback:Ljava/lang/Runnable;

    .line 38
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0, p0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 40
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 47
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mSpooler:Lcom/android/printspooler/model/PrintSpoolerService;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mBound:Z

    if-eqz v1, :cond_0

    .line 49
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mBound:Z

    .line 55
    :cond_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "ie":Ljava/lang/IllegalArgumentException;
    const-string v1, "PrintSpoolerProvider"

    const-string v2, "The service is already disconnected"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getSpooler()Lcom/android/printspooler/model/PrintSpoolerService;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mSpooler:Lcom/android/printspooler/model/PrintSpoolerService;

    return-object v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 59
    check-cast p2, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->getService()Lcom/android/printspooler/model/PrintSpoolerService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mSpooler:Lcom/android/printspooler/model/PrintSpoolerService;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mBound:Z

    .line 61
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mSpooler:Lcom/android/printspooler/model/PrintSpoolerService;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mCallback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 64
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/model/PrintSpoolerProvider;->mBound:Z

    .line 70
    return-void
.end method
