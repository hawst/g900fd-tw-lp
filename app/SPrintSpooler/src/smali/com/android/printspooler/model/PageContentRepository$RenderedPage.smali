.class final Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
.super Ljava/lang/Object;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PageContentRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RenderedPage"
.end annotation


# instance fields
.field final content:Landroid/graphics/drawable/BitmapDrawable;

.field renderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

.field state:I


# direct methods
.method constructor <init>(Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 1
    .param p1, "content"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    .line 403
    iput-object p1, p0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->content:Landroid/graphics/drawable/BitmapDrawable;

    .line 404
    return-void
.end method


# virtual methods
.method public erase()V
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->content:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 412
    return-void
.end method

.method public getSizeInBytes()I
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->content:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    return v0
.end method
