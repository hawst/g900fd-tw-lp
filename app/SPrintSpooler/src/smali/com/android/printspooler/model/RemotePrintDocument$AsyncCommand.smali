.class abstract Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
.super Ljava/lang/Object;
.source "RemotePrintDocument.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "AsyncCommand"
.end annotation


# static fields
.field private static sSequenceCounter:I


# instance fields
.field protected final mAdapter:Landroid/print/IPrintDocumentAdapter;

.field protected mCancellation:Landroid/os/ICancellationSignal;

.field protected final mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

.field protected final mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

.field private mError:Ljava/lang/CharSequence;

.field protected final mSequence:I

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;)V
    .locals 2
    .param p1, "adapter"    # Landroid/print/IPrintDocumentAdapter;
    .param p2, "document"    # Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    .param p3, "doneCallback"    # Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    .prologue
    .line 538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525
    sget v0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->sSequenceCounter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->sSequenceCounter:I

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mSequence:I

    .line 535
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    .line 539
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mAdapter:Landroid/print/IPrintDocumentAdapter;

    .line 540
    iput-object p2, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    .line 541
    iput-object p3, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    .line 542
    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 3

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 556
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->canceling()V

    .line 557
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mCancellation:Landroid/os/ICancellationSignal;

    if-eqz v1, :cond_0

    .line 559
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mCancellation:Landroid/os/ICancellationSignal;

    invoke-interface {v1}, Landroid/os/ICancellationSignal;->cancel()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :cond_0
    :goto_0
    return-void

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "RemotePrintDocument"

    const-string v2, "Error while canceling"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 565
    .end local v0    # "re":Landroid/os/RemoteException;
    :cond_1
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->canceled()V

    .line 568
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    invoke-interface {v1}, Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;->onDone()V

    goto :goto_0
.end method

.method protected final canceled()V
    .locals 2

    .prologue
    .line 580
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 581
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not canceling."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    .line 584
    return-void
.end method

.method protected final canceling()V
    .locals 2

    .prologue
    .line 573
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 574
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Command not pending or running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    .line 577
    return-void
.end method

.method protected final completed()V
    .locals 2

    .prologue
    .line 602
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 603
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 605
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    .line 606
    return-void
.end method

.method protected final failed(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/CharSequence;

    .prologue
    .line 613
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 615
    const-string v0, "RemotePrintDocument"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "called failed while "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not running"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    .line 619
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mError:Ljava/lang/CharSequence;

    .line 620
    return-void
.end method

.method public abstract getCommandType()Ljava/lang/String;
.end method

.method public getError()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mError:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final isCanceled()Z
    .locals 2

    .prologue
    .line 551
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isCanceling()Z
    .locals 2

    .prologue
    .line 547
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isCompleted()Z
    .locals 2

    .prologue
    .line 609
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFailed()Z
    .locals 2

    .prologue
    .line 623
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPending()Z
    .locals 1

    .prologue
    .line 587
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isRunning()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 598
    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final running()V
    .locals 2

    .prologue
    .line 591
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    if-eqz v0, :cond_0

    .line 592
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not pending."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 594
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    .line 595
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->getCommandType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [mSequence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mSequence:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mError:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->mState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
