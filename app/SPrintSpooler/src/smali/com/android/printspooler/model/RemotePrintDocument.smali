.class public final Lcom/android/printspooler/model/RemotePrintDocument;
.super Ljava/lang/Object;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/model/RemotePrintDocument$PrintDocumentAdapterObserver;,
        Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;,
        Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;,
        Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;,
        Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;,
        Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;,
        Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;,
        Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;,
        Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;
    }
.end annotation


# instance fields
.field private final mAdapterDeathObserver:Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;

.field private final mCommandResultCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

.field private final mContext:Landroid/content/Context;

.field private mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

.field private final mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

.field private final mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

.field private final mLooper:Landroid/os/Looper;

.field private mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

.field private final mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

.field private mState:I

.field private final mUpdateCallbacks:Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;

.field private final mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/MutexFileProvider;Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adapter"    # Landroid/print/IPrintDocumentAdapter;
    .param p3, "fileProvider"    # Lcom/android/printspooler/model/MutexFileProvider;
    .param p4, "deathObserver"    # Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;
    .param p5, "callbacks"    # Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    invoke-direct {v0}, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    .line 81
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$1;

    invoke-direct {v0, p0}, Lcom/android/printspooler/model/RemotePrintDocument$1;-><init>(Lcom/android/printspooler/model/RemotePrintDocument;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCommandResultCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    .line 139
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$2;

    invoke-direct {v0, p0}, Lcom/android/printspooler/model/RemotePrintDocument$2;-><init>(Lcom/android/printspooler/model/RemotePrintDocument;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    .line 146
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    .line 164
    iput-object p2, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    .line 165
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mLooper:Landroid/os/Looper;

    .line 166
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mContext:Landroid/content/Context;

    .line 167
    iput-object p4, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mAdapterDeathObserver:Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;

    .line 168
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    invoke-direct {v0}, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    .line 169
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-object p3, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->fileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    .line 170
    iput-object p5, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateCallbacks:Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;

    .line 171
    invoke-direct {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->connectToRemoteDocument()V

    .line 172
    return-void
.end method

.method static synthetic access$000(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/printspooler/model/RemotePrintDocument;Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;
    .param p1, "x1"    # Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/printspooler/model/RemotePrintDocument;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->runPendingCommand()V

    return-void
.end method

.method static synthetic access$102(Lcom/android/printspooler/model/RemotePrintDocument;Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;
    .param p1, "x1"    # Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/printspooler/model/RemotePrintDocument;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/RemotePrintDocument;->notifyUpdateFailed(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/printspooler/model/RemotePrintDocument;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->notifyUpdateCanceled()V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/printspooler/model/RemotePrintDocument;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->notifyPrintingAppDied()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mAdapterDeathObserver:Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/printspooler/model/RemotePrintDocument;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/printspooler/model/RemotePrintDocument;)Landroid/os/Looper;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mLooper:Landroid/os/Looper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/printspooler/model/RemotePrintDocument;)Landroid/print/IPrintDocumentAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCommandResultCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/printspooler/model/RemotePrintDocument;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    return v0
.end method

.method static synthetic access$802(Lcom/android/printspooler/model/RemotePrintDocument;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;
    .param p1, "x1"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    return p1
.end method

.method static synthetic access$900(Lcom/android/printspooler/model/RemotePrintDocument;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->notifyUpdateCompleted()V

    return-void
.end method

.method private connectToRemoteDocument()V
    .locals 4

    .prologue
    .line 392
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    invoke-interface {v1}, Landroid/print/IPrintDocumentAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    :try_start_1
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    new-instance v2, Lcom/android/printspooler/model/RemotePrintDocument$PrintDocumentAdapterObserver;

    invoke-direct {v2, p0}, Lcom/android/printspooler/model/RemotePrintDocument$PrintDocumentAdapterObserver;-><init>(Lcom/android/printspooler/model/RemotePrintDocument;)V

    invoke-interface {v1, v2}, Landroid/print/IPrintDocumentAdapter;->setObserver(Landroid/print/IPrintDocumentAdapterObserver;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 405
    :goto_0
    return-void

    .line 393
    :catch_0
    move-exception v0

    .line 394
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "RemotePrintDocument"

    const-string v2, "The printing process is dead."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->destroy()V

    goto :goto_0

    .line 401
    .end local v0    # "re":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 402
    .restart local v0    # "re":Landroid/os/RemoteException;
    const-string v1, "RemotePrintDocument"

    const-string v2, "Error setting observer to the print adapter."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->destroy()V

    goto :goto_0
.end method

.method private disconnectFromRemoteDocument()V
    .locals 4

    .prologue
    .line 409
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/print/IPrintDocumentAdapter;->setObserver(Landroid/print/IPrintDocumentAdapterObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 415
    :goto_0
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    invoke-interface {v1}, Landroid/print/IPrintDocumentAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 416
    return-void

    .line 410
    :catch_0
    move-exception v0

    .line 411
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "RemotePrintDocument"

    const-string v2, "Error setting observer to the print adapter."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private notifyPrintingAppDied()V
    .locals 2

    .prologue
    .line 1138
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mLooper:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/android/printspooler/model/RemotePrintDocument$3;

    invoke-direct {v1, p0}, Lcom/android/printspooler/model/RemotePrintDocument$3;-><init>(Lcom/android/printspooler/model/RemotePrintDocument;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1144
    return-void
.end method

.method private notifyUpdateCanceled()V
    .locals 2

    .prologue
    .line 371
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLING] onUpdateCanceled()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateCallbacks:Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;

    invoke-interface {v0}, Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;->onUpdateCanceled()V

    .line 374
    return-void
.end method

.method private notifyUpdateCompleted()V
    .locals 2

    .prologue
    .line 378
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLING] onUpdateCompleted()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateCallbacks:Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    invoke-interface {v0, v1}, Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;->onUpdateCompleted(Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;)V

    .line 381
    return-void
.end method

.method private notifyUpdateFailed(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/CharSequence;

    .prologue
    .line 385
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLING] onUpdateCompleted()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateCallbacks:Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;

    invoke-interface {v0, p1}, Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;->onUpdateFailed(Ljava/lang/CharSequence;)V

    .line 388
    return-void
.end method

.method private runPendingCommand()V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .line 431
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    if-eqz v0, :cond_3

    .line 435
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isPending()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 436
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->run()V

    .line 438
    :cond_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    .line 442
    :goto_0
    return-void

    .line 440
    :cond_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    goto :goto_0
.end method

.method private scheduleCommand(Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;)V
    .locals 1
    .param p1, "command"    # Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .prologue
    .line 419
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    if-nez v0, :cond_0

    .line 420
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .line 424
    :goto_0
    return-void

    .line 422
    :cond_0
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    goto :goto_0
.end method

.method private static stateToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 445
    packed-switch p0, :pswitch_data_0

    .line 471
    const-string v0, "STATE_UNKNOWN"

    :goto_0
    return-object v0

    .line 447
    :pswitch_0
    const-string v0, "STATE_FINISHED"

    goto :goto_0

    .line 450
    :pswitch_1
    const-string v0, "STATE_FAILED"

    goto :goto_0

    .line 453
    :pswitch_2
    const-string v0, "STATE_STARTED"

    goto :goto_0

    .line 456
    :pswitch_3
    const-string v0, "STATE_UPDATING"

    goto :goto_0

    .line 459
    :pswitch_4
    const-string v0, "STATE_UPDATED"

    goto :goto_0

    .line 462
    :pswitch_5
    const-string v0, "STATE_CANCELING"

    goto :goto_0

    .line 465
    :pswitch_6
    const-string v0, "STATE_CANCELED"

    goto :goto_0

    .line 468
    :pswitch_7
    const-string v0, "STATE_DESTROYED"

    goto :goto_0

    .line 445
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 283
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLED] cancel()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    if-ne v0, v2, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 291
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot cancel in state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    invoke-static {v2}, Lcom/android/printspooler/model/RemotePrintDocument;->stateToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294
    :cond_2
    iput v2, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    .line 296
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isRunning()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isPending()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    :cond_3
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->cancel()V

    goto :goto_0
.end method

.method public clearUpdateError()V
    .locals 2

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->hasUpdateError()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No update error to clear"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    .line 336
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 303
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLED] destroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    if-ne v0, v2, :cond_0

    .line 306
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot destroy in state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    invoke-static {v2}, Lcom/android/printspooler/model/RemotePrintDocument;->stateToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :cond_0
    iput v2, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    .line 311
    invoke-direct {p0}, Lcom/android/printspooler/model/RemotePrintDocument;->disconnectFromRemoteDocument()V

    .line 312
    return-void
.end method

.method public finish()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 264
    const-string v1, "RemotePrintDocument"

    const-string v2, "[CALLED] finish()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    if-eq v1, v3, :cond_0

    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 269
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot finish in state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    invoke-static {v3}, Lcom/android/printspooler/model/RemotePrintDocument;->stateToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 273
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    invoke-interface {v1}, Landroid/print/IPrintDocumentAdapter;->finish()V

    .line 274
    const/4 v1, 0x5

    iput v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_0
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "RemotePrintDocument"

    const-string v2, "Error calling finish()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 277
    iput v3, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    goto :goto_0
.end method

.method public getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    return-object v0
.end method

.method public hasLaidOutPages()Z
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-virtual {v0}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUpdateError()Z
    .locals 2

    .prologue
    .line 323
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDestroyed()Z
    .locals 2

    .prologue
    .line 319
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdating()Z
    .locals 2

    .prologue
    .line 315
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 176
    const-string v1, "RemotePrintDocument"

    const-string v2, "[CALLED] start()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    if-eqz v1, :cond_0

    .line 179
    const-string v1, "RemotePrintDocument"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot start in state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    invoke-static {v3}, Lcom/android/printspooler/model/RemotePrintDocument;->stateToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". skipped..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iput v4, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    .line 191
    :goto_0
    return-void

    .line 185
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    invoke-interface {v1}, Landroid/print/IPrintDocumentAdapter;->start()V

    .line 186
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "RemotePrintDocument"

    const-string v2, "Error calling start()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 189
    iput v4, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    goto :goto_0
.end method

.method public update(Landroid/print/PrintAttributes;[Landroid/print/PageRange;Z)Z
    .locals 18
    .param p1, "attributes"    # Landroid/print/PrintAttributes;
    .param p2, "pages"    # [Landroid/print/PageRange;
    .param p3, "preview"    # Z

    .prologue
    .line 197
    const-string v4, "RemotePrintDocument"

    const-string v5, "[CALLED] update()"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/model/RemotePrintDocument;->hasUpdateError()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 201
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Cannot update without a clearing the failure"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 204
    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 205
    :cond_1
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot update in state:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    invoke-static {v6}, Lcom/android/printspooler/model/RemotePrintDocument;->stateToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 209
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->hasSameConstraints(Landroid/print/PrintAttributes;Z)Z

    move-result v4

    if-nez v4, :cond_6

    .line 210
    const/16 v17, 0x1

    .line 214
    .local v17, "willUpdate":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v4}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isRunning()Z

    move-result v4

    if-nez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v4}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isPending()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 216
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v4}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->cancel()V

    .line 220
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v4, v4, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->attributes:Landroid/print/PrintAttributes;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v7, v4, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->attributes:Landroid/print/PrintAttributes;

    .line 222
    .local v7, "oldAttributes":Landroid/print/PrintAttributes;
    :goto_0
    new-instance v3, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mLooper:Landroid/os/Looper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCommandResultCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    move-object/from16 v8, p1

    move/from16 v9, p3

    invoke-direct/range {v3 .. v10}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;-><init>(Landroid/os/Looper;Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;ZLcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;)V

    .line 224
    .local v3, "command":Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/printspooler/model/RemotePrintDocument;->scheduleCommand(Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;)V

    .line 226
    const/4 v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    .line 255
    .end local v3    # "command":Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    .end local v7    # "oldAttributes":Landroid/print/PrintAttributes;
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    move-object/from16 v0, p1

    move/from16 v1, p3

    move-object/from16 v2, p2

    invoke-virtual {v4, v0, v1, v2}, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->update(Landroid/print/PrintAttributes;Z[Landroid/print/PageRange;)V

    .line 257
    invoke-direct/range {p0 .. p0}, Lcom/android/printspooler/model/RemotePrintDocument;->runPendingCommand()V

    .line 259
    return v17

    .line 220
    :cond_5
    new-instance v4, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v4}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v4}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v7

    goto :goto_0

    .line 228
    .end local v17    # "willUpdate":Z
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    instance-of v4, v4, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v4}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isPending()Z

    move-result v4

    if-nez v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v4}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isRunning()Z

    move-result v4

    if-nez v4, :cond_a

    :cond_7
    if-eqz p2, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    iget-object v4, v4, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->pages:[Landroid/print/PageRange;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v5, v5, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-virtual {v5}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v5

    move-object/from16 v0, p2

    invoke-static {v4, v0, v5}, Lcom/android/printspooler/util/PageRangeUtils;->contains([Landroid/print/PageRange;[Landroid/print/PageRange;I)Z

    move-result v4

    if-nez v4, :cond_a

    .line 232
    const/16 v17, 0x1

    .line 235
    .restart local v17    # "willUpdate":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    instance-of v4, v4, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v4}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isPending()Z

    move-result v4

    if-nez v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v4}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 237
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    invoke-virtual {v4}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->cancel()V

    .line 241
    :cond_9
    new-instance v3, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mLooper:Landroid/os/Looper;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v4, v4, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-virtual {v4}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v15, v4, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->fileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mCommandResultCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    move-object/from16 v16, v0

    move-object v8, v3

    move-object/from16 v14, p2

    invoke-direct/range {v8 .. v16}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;-><init>(Landroid/content/Context;Landroid/os/Looper;Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;I[Landroid/print/PageRange;Lcom/android/printspooler/model/MutexFileProvider;Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;)V

    .line 244
    .restart local v3    # "command":Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/printspooler/model/RemotePrintDocument;->scheduleCommand(Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;)V

    .line 246
    const/4 v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/printspooler/model/RemotePrintDocument;->mState:I

    goto/16 :goto_1

    .line 248
    .end local v3    # "command":Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    .end local v17    # "willUpdate":Z
    :cond_a
    const/16 v17, 0x0

    .line 250
    .restart local v17    # "willUpdate":Z
    const-string v4, "RemotePrintDocument"

    const-string v5, "[SKIPPING] No update needed"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public writeContent(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 9
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 343
    const/4 v2, 0x0

    .line 344
    .local v2, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 345
    .local v3, "in":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 347
    .local v5, "out":Ljava/io/OutputStream;
    :try_start_0
    iget-object v7, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v7, v7, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->fileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/android/printspooler/model/MutexFileProvider;->acquireFile(Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;)Ljava/io/File;

    move-result-object v2

    .line 348
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    .end local v3    # "in":Ljava/io/InputStream;
    .local v4, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p1, p2}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v5

    .line 350
    const/16 v7, 0x2000

    new-array v0, v7, [B

    .line 352
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    .line 353
    .local v6, "readByteCount":I
    if-gez v6, :cond_1

    .line 361
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 362
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 363
    if-eqz v2, :cond_3

    .line 364
    iget-object v7, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v7, v7, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->fileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    invoke-virtual {v7}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    move-object v3, v4

    .line 367
    .end local v0    # "buffer":[B
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v6    # "readByteCount":I
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_1
    return-void

    .line 356
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v0    # "buffer":[B
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v6    # "readByteCount":I
    :cond_1
    const/4 v7, 0x0

    :try_start_2
    invoke-virtual {v5, v0, v7, v6}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 358
    .end local v0    # "buffer":[B
    .end local v6    # "readByteCount":I
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 359
    .end local v4    # "in":Ljava/io/InputStream;
    .local v1, "e":Ljava/io/IOException;
    .restart local v3    # "in":Ljava/io/InputStream;
    :goto_2
    :try_start_3
    const-string v7, "RemotePrintDocument"

    const-string v8, "Error writing document content."

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 361
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 362
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 363
    if-eqz v2, :cond_0

    .line 364
    iget-object v7, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v7, v7, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->fileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    invoke-virtual {v7}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    goto :goto_1

    .line 361
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 362
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 363
    if-eqz v2, :cond_2

    .line 364
    iget-object v8, p0, Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v8, v8, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->fileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    invoke-virtual {v8}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    :cond_2
    throw v7

    .line 361
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 358
    :catch_1
    move-exception v1

    goto :goto_2

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v0    # "buffer":[B
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v6    # "readByteCount":I
    :cond_3
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method
