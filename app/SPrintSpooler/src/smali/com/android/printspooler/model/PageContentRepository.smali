.class public final Lcom/android/printspooler/model/PageContentRepository;
.super Ljava/lang/Object;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;,
        Lcom/android/printspooler/model/PageContentRepository$RenderedPage;,
        Lcom/android/printspooler/model/PageContentRepository$RenderSpec;,
        Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;,
        Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;,
        Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;,
        Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;
    }
.end annotation


# instance fields
.field private final mCloseGuard:Ldalvik/system/CloseGuard;

.field private mLastRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

.field private final mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

.field private mScheduledPreloadFirstShownPage:I

.field private mScheduledPreloadLastShownPage:I

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "malformedPdfFileListener"    # Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;

    .prologue
    const/4 v1, -0x1

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mCloseGuard:Ldalvik/system/CloseGuard;

    .line 71
    iput v1, p0, Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadFirstShownPage:I

    .line 72
    iput v1, p0, Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadLastShownPage:I

    .line 87
    new-instance v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-direct {v0, p1, p2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;-><init>(Landroid/content/Context;Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;)V

    iput-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    .line 90
    const-string v0, "PageContentRepository"

    const-string v1, "STATE_CLOSED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mCloseGuard:Ldalvik/system/CloseGuard;

    const-string v1, "destroy"

    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method static synthetic access$100(Lcom/android/printspooler/model/PageContentRepository;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/printspooler/model/PageContentRepository;->throwIfDestroyed()V

    return-void
.end method

.method static synthetic access$202(Lcom/android/printspooler/model/PageContentRepository;Lcom/android/printspooler/model/PageContentRepository$RenderSpec;)Lcom/android/printspooler/model/PageContentRepository$RenderSpec;
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository;
    .param p1, "x1"    # Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/printspooler/model/PageContentRepository;->mLastRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/printspooler/model/PageContentRepository;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository;

    .prologue
    .line 50
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadFirstShownPage:I

    return v0
.end method

.method static synthetic access$302(Lcom/android/printspooler/model/PageContentRepository;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadFirstShownPage:I

    return p1
.end method

.method static synthetic access$400(Lcom/android/printspooler/model/PageContentRepository;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository;

    .prologue
    .line 50
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadLastShownPage:I

    return v0
.end method

.method static synthetic access$402(Lcom/android/printspooler/model/PageContentRepository;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadLastShownPage:I

    return p1
.end method

.method static synthetic access$500(Lcom/android/printspooler/model/PageContentRepository;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository;

    .prologue
    .line 50
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    return v0
.end method

.method static synthetic access$600(Lcom/android/printspooler/model/PageContentRepository;)Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    return-object v0
.end method

.method private doDestroy(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 175
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    .line 177
    const-string v0, "PageContentRepository"

    const-string v1, "STATE_DESTROYED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-virtual {v0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->destroy(Ljava/lang/Runnable;)V

    .line 180
    return-void
.end method

.method private throwIfDestroyed()V
    .locals 2

    .prologue
    .line 195
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 196
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_0
    return-void
.end method

.method private throwIfNotClosed()V
    .locals 2

    .prologue
    .line 189
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    if-eqz v0, :cond_0

    .line 190
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_0
    return-void
.end method

.method private throwIfNotOpened()V
    .locals 2

    .prologue
    .line 183
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 184
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not opened"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    return-void
.end method


# virtual methods
.method public acquirePageContentProvider(ILandroid/view/View;)Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;
    .locals 3
    .param p1, "pageIndex"    # I
    .param p2, "owner"    # Landroid/view/View;

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/android/printspooler/model/PageContentRepository;->throwIfDestroyed()V

    .line 146
    const-string v0, "PageContentRepository"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Acquiring provider for page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    new-instance v0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;-><init>(Lcom/android/printspooler/model/PageContentRepository;ILandroid/view/View;)V

    return-object v0
.end method

.method public close(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/android/printspooler/model/PageContentRepository;->throwIfNotOpened()V

    .line 106
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    .line 108
    const-string v0, "PageContentRepository"

    const-string v1, "STATE_CLOSED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-virtual {v0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->close(Ljava/lang/Runnable;)V

    .line 112
    return-void
.end method

.method public destroy(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/android/printspooler/model/PageContentRepository;->throwIfNotClosed()V

    .line 116
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    .line 118
    const-string v0, "PageContentRepository"

    const-string v1, "STATE_DESTROYED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PageContentRepository;->doDestroy(Ljava/lang/Runnable;)V

    .line 121
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 165
    :try_start_0
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 166
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mCloseGuard:Ldalvik/system/CloseGuard;

    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/printspooler/model/PageContentRepository;->doDestroy(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 172
    return-void

    .line 170
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getFilePageCount()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-virtual {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->getPageCount()I

    move-result v0

    return v0
.end method

.method public open(Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "source"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/printspooler/model/PageContentRepository;->throwIfNotClosed()V

    .line 97
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    .line 99
    const-string v0, "PageContentRepository"

    const-string v1, "STATE_OPENED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->open(Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V

    .line 102
    return-void
.end method

.method public releasePageContentProvider(Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;)V
    .locals 3
    .param p1, "provider"    # Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/android/printspooler/model/PageContentRepository;->throwIfDestroyed()V

    .line 156
    const-string v0, "PageContentRepository"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Releasing provider for page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->mPageIndex:I
    invoke-static {p1}, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->access$000(Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {p1}, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->cancelLoad()V

    .line 160
    return-void
.end method

.method public startPreload(II)V
    .locals 2
    .param p1, "firstShownPage"    # I
    .param p2, "lastShownPage"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mLastRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    if-nez v0, :cond_1

    .line 127
    iput p1, p0, Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadFirstShownPage:I

    .line 128
    iput p2, p0, Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadLastShownPage:I

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository;->mLastRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->startPreload(IILcom/android/printspooler/model/PageContentRepository$RenderSpec;)V

    goto :goto_0
.end method

.method public stopPreload()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-virtual {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->stopPreload()V

    .line 136
    return-void
.end method
