.class final Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;
.super Ljava/lang/Object;
.source "PageContentRepository.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PageContentRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AsyncRenderer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    }
.end annotation


# instance fields
.field private mBoundToService:Z

.field private final mContext:Landroid/content/Context;

.field private final mLock:Ljava/lang/Object;

.field private final mOnMalformedPdfFileListener:Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;

.field private final mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

.field private mPageCount:I

.field private final mPageToRenderTaskMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;",
            ">;"
        }
    .end annotation
.end field

.field private mRenderer:Lcom/android/printspooler/renderer/IPdfRenderer;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "malformedPdfFileListener"    # Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;

    .prologue
    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mLock:Ljava/lang/Object;

    .line 422
    new-instance v2, Landroid/util/ArrayMap;

    invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V

    iput-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    .line 426
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I

    .line 434
    iput-object p1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mContext:Landroid/content/Context;

    .line 435
    iput-object p2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mOnMalformedPdfFileListener:Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;

    .line 437
    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mContext:Landroid/content/Context;

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 439
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v2

    const/high16 v3, 0x100000

    mul-int/2addr v2, v3

    div-int/lit8 v1, v2, 0x4

    .line 440
    .local v1, "cacheSizeInBytes":I
    new-instance v2, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    invoke-direct {v2, v1}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;-><init>(I)V

    iput-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    .line 441
    return-void
.end method

.method static synthetic access$1000(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mOnMalformedPdfFileListener:Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;
    .param p1, "x1"    # I

    .prologue
    .line 415
    iput p1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I

    return p1
.end method

.method static synthetic access$1200(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    .prologue
    .line 415
    iget-boolean v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mBoundToService:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;
    .param p1, "x1"    # Z

    .prologue
    .line 415
    iput-boolean p1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mBoundToService:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Landroid/util/ArrayMap;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/renderer/IPdfRenderer;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mRenderer:Lcom/android/printspooler/renderer/IPdfRenderer;

    return-object v0
.end method

.method private cancelAllRendering()V
    .locals 4

    .prologue
    .line 692
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v2

    .line 693
    .local v2, "taskCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 694
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;

    .line 695
    .local v1, "task":Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    invoke-virtual {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 696
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->cancel(Z)Z

    .line 693
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 699
    .end local v1    # "task":Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    :cond_1
    return-void
.end method


# virtual methods
.method public cancelRendering(I)V
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    .line 685
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;

    .line 686
    .local v0, "task":Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 687
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->cancel(Z)Z

    .line 689
    :cond_0
    return-void
.end method

.method public close(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->cancelAllRendering()V

    .line 518
    new-instance v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;

    invoke-direct {v0, p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;-><init>(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;Ljava/lang/Runnable;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 539
    return-void
.end method

.method public destroy(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 542
    new-instance v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;

    invoke-direct {v0, p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;-><init>(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;Ljava/lang/Runnable;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 566
    return-void
.end method

.method public getCachedPage(ILcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V
    .locals 4
    .param p1, "pageIndex"    # I
    .param p2, "renderSpec"    # Lcom/android/printspooler/model/PageContentRepository$RenderSpec;
    .param p3, "callback"    # Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    .prologue
    .line 620
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    invoke-virtual {v1, p1}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getRenderedPage(I)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    move-result-object v0

    .line 621
    .local v0, "renderedPage":Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->renderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    invoke-virtual {v1, p2}, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 624
    const-string v1, "PageContentRepository"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cache hit for page: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    if-eqz p3, :cond_0

    .line 629
    iget-object v1, v0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->content:Landroid/graphics/drawable/BitmapDrawable;

    invoke-interface {p3, v1}, Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;->onPageContentAvailable(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 632
    :cond_0
    return-void
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 615
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I

    return v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 445
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mBoundToService:Z

    .line 446
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 447
    :try_start_0
    invoke-static {p2}, Lcom/android/printspooler/renderer/IPdfRenderer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/printspooler/renderer/IPdfRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mRenderer:Lcom/android/printspooler/renderer/IPdfRenderer;

    .line 448
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 449
    monitor-exit v1

    .line 450
    return-void

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 454
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 455
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mRenderer:Lcom/android/printspooler/renderer/IPdfRenderer;

    .line 456
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mBoundToService:Z

    .line 457
    monitor-exit v1

    .line 458
    return-void

    .line 457
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public open(Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "source"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 465
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    invoke-virtual {v0}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->invalidate()V

    .line 467
    new-instance v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;-><init>(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 513
    return-void
.end method

.method public renderPage(ILcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V
    .locals 5
    .param p1, "pageIndex"    # I
    .param p2, "renderSpec"    # Lcom/android/printspooler/model/PageContentRepository$RenderSpec;
    .param p3, "callback"    # Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    .prologue
    .line 637
    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    invoke-virtual {v2, p1}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getRenderedPage(I)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    move-result-object v1

    .line 638
    .local v1, "renderedPage":Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    if-eqz v1, :cond_2

    iget v2, v1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    if-nez v2, :cond_2

    .line 640
    iget-object v2, v1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->renderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    invoke-virtual {v2, p2}, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 642
    const-string v2, "PageContentRepository"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cache hit for page: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    if-eqz p3, :cond_0

    .line 647
    iget-object v2, v1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->content:Landroid/graphics/drawable/BitmapDrawable;

    invoke-interface {p3, v2}, Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;->onPageContentAvailable(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 652
    :cond_1
    const/4 v2, 0x2

    iput v2, v1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    .line 657
    :cond_2
    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;

    .line 658
    .local v0, "renderTask":Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 660
    iget-object v2, v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    invoke-virtual {v2, p2}, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 661
    iget-object v2, v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mCallback:Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    if-eqz v2, :cond_3

    .line 663
    if-eqz p3, :cond_0

    iget-object v2, v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mCallback:Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    if-eq v2, p3, :cond_0

    .line 664
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Page rendering not cancelled"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 669
    :cond_3
    iput-object p3, v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mCallback:Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    goto :goto_0

    .line 674
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->cancel(Z)Z

    .line 679
    :cond_5
    new-instance v0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;

    .end local v0    # "renderTask":Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;-><init>(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;ILcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V

    .line 680
    .restart local v0    # "renderTask":Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 681
    sget-object v2, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v2, v3}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public startPreload(IILcom/android/printspooler/model/PageContentRepository$RenderSpec;)V
    .locals 11
    .param p1, "firstShownPage"    # I
    .param p2, "lastShownPage"    # I
    .param p3, "renderSpec"    # Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    .prologue
    .line 570
    const-string v8, "PageContentRepository"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Preloading pages around ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    iget v8, p3, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapWidth:I

    iget v9, p3, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapHeight:I

    mul-int/2addr v8, v9

    mul-int/lit8 v0, v8, 0x4

    .line 576
    .local v0, "bitmapSizeInBytes":I
    iget-object v8, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    invoke-virtual {v8}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getMaxSizeInBytes()I

    move-result v8

    div-int v6, v8, v0

    .line 578
    .local v6, "maxCachedPageCount":I
    sub-int v8, p2, p1

    sub-int v8, v6, v8

    div-int/lit8 v8, v8, 0x2

    add-int/lit8 v4, v8, -0x1

    .line 582
    .local v4, "halfPreloadCount":I
    sub-int v8, p1, v4

    if-gez v8, :cond_0

    .line 583
    sub-int v2, v4, p1

    .line 589
    .local v2, "excessFromStart":I
    :goto_0
    add-int v8, p2, v4

    iget v9, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I

    if-lt v8, v9, :cond_1

    .line 590
    add-int v8, p2, v4

    iget v9, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I

    sub-int v1, v8, v9

    .line 595
    .local v1, "excessFromEnd":I
    :goto_1
    sub-int v8, p1, v4

    sub-int/2addr v8, v1

    const/4 v9, 0x0

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 596
    .local v3, "fromIndex":I
    add-int v8, p2, v4

    add-int/2addr v8, v2

    iget v9, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I

    add-int/lit8 v9, v9, -0x1

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 599
    .local v7, "toIndex":I
    move v5, v3

    .local v5, "i":I
    :goto_2
    if-gt v5, v7, :cond_2

    .line 600
    const/4 v8, 0x0

    invoke-virtual {p0, v5, p3, v8}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->renderPage(ILcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V

    .line 599
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 585
    .end local v1    # "excessFromEnd":I
    .end local v2    # "excessFromStart":I
    .end local v3    # "fromIndex":I
    .end local v5    # "i":I
    .end local v7    # "toIndex":I
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "excessFromStart":I
    goto :goto_0

    .line 592
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "excessFromEnd":I
    goto :goto_1

    .line 602
    .restart local v3    # "fromIndex":I
    .restart local v5    # "i":I
    .restart local v7    # "toIndex":I
    :cond_2
    return-void
.end method

.method public stopPreload()V
    .locals 4

    .prologue
    .line 605
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v2

    .line 606
    .local v2, "taskCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 607
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;

    .line 608
    .local v1, "task":Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    invoke-virtual {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->isPreload()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 609
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->cancel(Z)Z

    .line 606
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 612
    .end local v1    # "task":Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
    :cond_1
    return-void
.end method
