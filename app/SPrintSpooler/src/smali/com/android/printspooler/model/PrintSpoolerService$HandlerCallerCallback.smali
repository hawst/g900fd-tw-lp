.class final Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;
.super Ljava/lang/Object;
.source "PrintSpoolerService.java"

# interfaces
.implements Lcom/android/internal/os/HandlerCaller$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PrintSpoolerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HandlerCallerCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/PrintSpoolerService;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/model/PrintSpoolerService;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/model/PrintSpoolerService;Lcom/android/printspooler/model/PrintSpoolerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;
    .param p2, "x1"    # Lcom/android/printspooler/model/PrintSpoolerService$1;

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;-><init>(Lcom/android/printspooler/model/PrintSpoolerService;)V

    return-void
.end method


# virtual methods
.method public executeMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 191
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 193
    :pswitch_0
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$200(Lcom/android/printspooler/model/PrintSpoolerService;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 194
    :try_start_0
    iget-object v6, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/print/IPrintSpoolerClient;

    # setter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v6, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$302(Lcom/android/printspooler/model/PrintSpoolerService;Landroid/print/IPrintSpoolerClient;)Landroid/print/IPrintSpoolerClient;

    .line 195
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 196
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$400(Lcom/android/printspooler/model/PrintSpoolerService;)Lcom/android/internal/os/HandlerCaller;

    move-result-object v4

    const/4 v6, 0x5

    invoke-virtual {v4, v6}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 198
    .local v0, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$400(Lcom/android/printspooler/model/PrintSpoolerService;)Lcom/android/internal/os/HandlerCaller;

    move-result-object v4

    const-wide/16 v6, 0x1388

    invoke-virtual {v4, v0, v6, v7}, Lcom/android/internal/os/HandlerCaller;->sendMessageDelayed(Landroid/os/Message;J)V

    .line 201
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 205
    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/print/PrintJobInfo;

    .line 206
    .local v1, "printJob":Landroid/print/PrintJobInfo;
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 208
    :try_start_1
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    invoke-interface {v4, v1}, Landroid/print/IPrintSpoolerClient;->onPrintJobQueued(Landroid/print/PrintJobInfo;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v2

    .line 210
    .local v2, "re":Landroid/os/RemoteException;
    const-string v4, "PrintSpoolerService"

    const-string v5, "Error notify for a queued print job."

    invoke-static {v4, v5, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 216
    .end local v1    # "printJob":Landroid/print/PrintJobInfo;
    .end local v2    # "re":Landroid/os/RemoteException;
    :pswitch_2
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/content/ComponentName;

    .line 217
    .local v3, "service":Landroid/content/ComponentName;
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 219
    :try_start_2
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/print/IPrintSpoolerClient;->onAllPrintJobsForServiceHandled(Landroid/content/ComponentName;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 220
    :catch_1
    move-exception v2

    .line 221
    .restart local v2    # "re":Landroid/os/RemoteException;
    const-string v4, "PrintSpoolerService"

    const-string v5, "Error notify for all print jobs per service handled."

    invoke-static {v4, v5, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 228
    .end local v2    # "re":Landroid/os/RemoteException;
    .end local v3    # "service":Landroid/content/ComponentName;
    :pswitch_3
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 230
    :try_start_3
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    invoke-interface {v4}, Landroid/print/IPrintSpoolerClient;->onAllPrintJobsHandled()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 231
    :catch_2
    move-exception v2

    .line 232
    .restart local v2    # "re":Landroid/os/RemoteException;
    const-string v4, "PrintSpoolerService"

    const-string v5, "Error notify for all print job handled."

    invoke-static {v4, v5, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 238
    .end local v2    # "re":Landroid/os/RemoteException;
    :pswitch_4
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->checkAllPrintJobsHandled()V

    goto/16 :goto_0

    .line 242
    :pswitch_5
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 243
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/print/PrintJobInfo;

    .line 245
    .restart local v1    # "printJob":Landroid/print/PrintJobInfo;
    :try_start_4
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;
    invoke-static {v4}, Lcom/android/printspooler/model/PrintSpoolerService;->access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;

    move-result-object v4

    invoke-interface {v4, v1}, Landroid/print/IPrintSpoolerClient;->onPrintJobStateChanged(Landroid/print/PrintJobInfo;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 246
    :catch_3
    move-exception v2

    .line 247
    .restart local v2    # "re":Landroid/os/RemoteException;
    const-string v4, "PrintSpoolerService"

    const-string v5, "Error notify for print job state change."

    invoke-static {v4, v5, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
