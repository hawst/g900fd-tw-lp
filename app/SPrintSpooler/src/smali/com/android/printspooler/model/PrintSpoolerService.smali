.class public final Lcom/android/printspooler/model/PrintSpoolerService;
.super Landroid/app/Service;
.source "PrintSpoolerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;,
        Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;,
        Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/printspooler/model/PrintSpoolerService;

.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private mClient:Landroid/print/IPrintSpoolerClient;

.field private mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

.field private final mLock:Ljava/lang/Object;

.field private mNotificationController:Lcom/android/printspooler/model/NotificationController;

.field private mPersistanceManager:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

.field private final mPrintJobs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/print/PrintJobInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/printspooler/model/PrintSpoolerService;->sLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 91
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    .line 1216
    return-void
.end method

.method static synthetic access$1000(Lcom/android/printspooler/model/PrintSpoolerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->removeObsoletePrintJobs()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/printspooler/model/PrintSpoolerService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/printspooler/model/PrintSpoolerService;)Landroid/print/IPrintSpoolerClient;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/printspooler/model/PrintSpoolerService;Landroid/print/IPrintSpoolerClient;)Landroid/print/IPrintSpoolerClient;
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;
    .param p1, "x1"    # Landroid/print/IPrintSpoolerClient;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mClient:Landroid/print/IPrintSpoolerClient;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/printspooler/model/PrintSpoolerService;)Lcom/android/internal/os/HandlerCaller;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/printspooler/model/PrintSpoolerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->sendOnAllPrintJobsHandled()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/printspooler/model/PrintSpoolerService;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/printspooler/model/PrintSpoolerService;Landroid/print/PrintJobInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;
    .param p1, "x1"    # Landroid/print/PrintJobInfo;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService;->shouldPersistPrintJob(Landroid/print/PrintJobInfo;)Z

    move-result v0

    return v0
.end method

.method private addPrintJobLocked(Landroid/print/PrintJobInfo;)V
    .locals 3
    .param p1, "printJob"    # Landroid/print/PrintJobInfo;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 439
    const-string v0, "PrintSpoolerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ADD] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    return-void
.end method

.method public static generateFileForPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)Ljava/io/File;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "printJobId"    # Landroid/print/PrintJobId;

    .prologue
    .line 432
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "print_job_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/print/PrintJobId;->flattenToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "pdf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private handleReadPrintJobsLocked()V
    .locals 14

    .prologue
    .line 326
    const/4 v2, 0x0

    .line 327
    .local v2, "fileForJobMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Landroid/print/PrintJobId;Ljava/io/File;>;"
    invoke-virtual {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 328
    .local v3, "files":[Ljava/io/File;
    if-eqz v3, :cond_2

    .line 329
    array-length v1, v3

    .line 330
    .local v1, "fileCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_2

    .line 331
    aget-object v0, v3, v4

    .line 332
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "print_job_"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 333
    if-nez v2, :cond_0

    .line 334
    new-instance v2, Landroid/util/ArrayMap;

    .end local v2    # "fileForJobMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Landroid/print/PrintJobId;Ljava/io/File;>;"
    invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V

    .line 336
    .restart local v2    # "fileForJobMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Landroid/print/PrintJobId;Ljava/io/File;>;"
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "print_job_"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x2e

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 339
    .local v9, "printJobIdString":Ljava/lang/String;
    invoke-static {v9}, Landroid/print/PrintJobId;->unflattenFromString(Ljava/lang/String;)Landroid/print/PrintJobId;

    move-result-object v8

    .line 341
    .local v8, "printJobId":Landroid/print/PrintJobId;
    invoke-virtual {v2, v8, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    .end local v8    # "printJobId":Landroid/print/PrintJobId;
    .end local v9    # "printJobIdString":Ljava/lang/String;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 346
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "fileCount":I
    .end local v4    # "i":I
    :cond_2
    iget-object v10, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v7

    .line 347
    .local v7, "printJobCount":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    if-ge v4, v7, :cond_4

    .line 348
    iget-object v10, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/print/PrintJobInfo;

    .line 351
    .local v6, "printJob":Landroid/print/PrintJobInfo;
    if-eqz v2, :cond_3

    .line 352
    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :cond_3
    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getState()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 347
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 363
    :pswitch_0
    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v10

    const/4 v11, 0x6

    const v12, 0x7f090034

    invoke-virtual {p0, v12}, Lcom/android/printspooler/model/PrintSpoolerService;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v10, v11, v12}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z

    goto :goto_2

    .line 369
    .end local v6    # "printJob":Landroid/print/PrintJobInfo;
    :cond_4
    iget-object v10, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    .line 371
    iget-object v10, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mNotificationController:Lcom/android/printspooler/model/NotificationController;

    iget-object v11, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-virtual {v10, v11}, Lcom/android/printspooler/model/NotificationController;->onUpdateNotifications(Ljava/util/List;)V

    .line 375
    :cond_5
    if-eqz v2, :cond_6

    .line 376
    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v5

    .line 377
    .local v5, "orphanFileCount":I
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v5, :cond_6

    .line 378
    invoke-virtual {v2, v4}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 379
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 377
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 382
    .end local v0    # "file":Ljava/io/File;
    .end local v5    # "orphanFileCount":I
    :cond_6
    return-void

    .line 355
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isActiveState(I)Z
    .locals 2
    .param p1, "printJobState"    # I

    .prologue
    const/4 v0, 0x1

    .line 572
    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isObsoleteState(I)Z
    .locals 1
    .param p1, "printJobState"    # I

    .prologue
    .line 561
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService;->isTerminalState(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isScheduledState(I)Z
    .locals 1
    .param p1, "printJobState"    # I

    .prologue
    .line 566
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isStateVisibleToUser(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService;->isActiveState(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTerminalState(I)Z
    .locals 1
    .param p1, "printJobState"    # I

    .prologue
    .line 579
    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyOnAllPrintJobsHandled()V
    .locals 3

    .prologue
    .line 652
    new-instance v1, Lcom/android/printspooler/model/PrintSpoolerService$2;

    invoke-direct {v1, p0}, Lcom/android/printspooler/model/PrintSpoolerService$2;-><init>(Lcom/android/printspooler/model/PrintSpoolerService;)V

    sget-object v2, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, Lcom/android/printspooler/model/PrintSpoolerService$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 659
    return-void
.end method

.method private removeObsoletePrintJobs()V
    .locals 8

    .prologue
    .line 444
    iget-object v5, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 445
    const/4 v1, 0x0

    .line 446
    .local v1, "persistState":Z
    :try_start_0
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 447
    .local v3, "printJobCount":I
    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 448
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/print/PrintJobInfo;

    .line 449
    .local v2, "printJob":Landroid/print/PrintJobInfo;
    invoke-virtual {v2}, Landroid/print/PrintJobInfo;->getState()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->isObsoleteState(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 450
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 452
    const-string v4, "PrintSpoolerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[REMOVE] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v7

    invoke-virtual {v7}, Landroid/print/PrintJobId;->flattenToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    invoke-virtual {v2}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->removePrintJobFileLocked(Landroid/print/PrintJobId;)V

    .line 455
    const/4 v1, 0x1

    .line 447
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 458
    .end local v2    # "printJob":Landroid/print/PrintJobInfo;
    :cond_1
    if-eqz v1, :cond_2

    .line 459
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPersistanceManager:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    invoke-virtual {v4}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->writeStateLocked()V

    .line 461
    :cond_2
    monitor-exit v5

    .line 462
    return-void

    .line 461
    .end local v0    # "i":I
    .end local v3    # "printJobCount":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method private removePrintJobFileLocked(Landroid/print/PrintJobId;)V
    .locals 4
    .param p1, "printJobId"    # Landroid/print/PrintJobId;

    .prologue
    .line 465
    invoke-static {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService;->generateFileForPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)Ljava/io/File;

    move-result-object v0

    .line 466
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 467
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 469
    const-string v1, "PrintSpoolerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[REMOVE FILE FOR] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_0
    return-void
.end method

.method private sendOnAllPrintJobsForServiceHandled(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "service"    # Landroid/content/ComponentName;

    .prologue
    .line 170
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 172
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    .line 173
    return-void
.end method

.method private sendOnAllPrintJobsHandled()V
    .locals 3

    .prologue
    .line 176
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 178
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    .line 179
    return-void
.end method

.method private sendOnPrintJobQueued(Landroid/print/PrintJobInfo;)V
    .locals 3
    .param p1, "printJob"    # Landroid/print/PrintJobInfo;

    .prologue
    .line 164
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 166
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    .line 167
    return-void
.end method

.method private shouldPersistPrintJob(Landroid/print/PrintJobInfo;)Z
    .locals 2
    .param p1, "printJob"    # Landroid/print/PrintJobInfo;

    .prologue
    .line 644
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public checkAllPrintJobsHandled()V
    .locals 2

    .prologue
    .line 385
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 386
    :try_start_0
    invoke-virtual {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->hasActivePrintJobsLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    invoke-direct {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->notifyOnAllPrintJobsHandled()V

    .line 389
    :cond_0
    monitor-exit v1

    .line 390
    return-void

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public createPrintJob(Landroid/print/PrintJobInfo;)V
    .locals 5
    .param p1, "printJob"    # Landroid/print/PrintJobInfo;

    .prologue
    .line 309
    iget-object v2, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 310
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService;->addPrintJobLocked(Landroid/print/PrintJobInfo;)V

    .line 311
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v3, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z

    .line 313
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 316
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    .line 317
    monitor-exit v2

    .line 318
    return-void

    .line 317
    .end local v0    # "message":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 11
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 137
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v9

    .line 138
    :try_start_0
    array-length v8, p3

    if-lez v8, :cond_0

    const/4 v8, 0x0

    aget-object v4, p3, v8

    .line 139
    .local v4, "prefix":Ljava/lang/String;
    :goto_0
    const-string v7, "  "

    .line 141
    .local v7, "tab":Ljava/lang/String;
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    const-string v10, "print jobs:"

    invoke-virtual {v8, v10}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    .line 142
    iget-object v8, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v6

    .line 143
    .local v6, "printJobCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v6, :cond_1

    .line 144
    iget-object v8, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/print/PrintJobInfo;

    .line 145
    .local v5, "printJob":Landroid/print/PrintJobInfo;
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    invoke-virtual {v5}, Landroid/print/PrintJobInfo;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 146
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 143
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 138
    .end local v3    # "i":I
    .end local v4    # "prefix":Ljava/lang/String;
    .end local v5    # "printJob":Landroid/print/PrintJobInfo;
    .end local v6    # "printJobCount":I
    .end local v7    # "tab":Ljava/lang/String;
    :cond_0
    const-string v4, ""

    goto :goto_0

    .line 149
    .restart local v3    # "i":I
    .restart local v4    # "prefix":Ljava/lang/String;
    .restart local v6    # "printJobCount":I
    .restart local v7    # "tab":Ljava/lang/String;
    :cond_1
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    const-string v10, "print job files:"

    invoke-virtual {v8, v10}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    .line 150
    invoke-virtual {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 151
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_3

    .line 152
    array-length v1, v2

    .line 153
    .local v1, "fileCount":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_3

    .line 154
    aget-object v0, v2, v3

    .line 155
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v10, "print_job_"

    invoke-virtual {v8, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 156
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    .line 153
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 160
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "fileCount":I
    :cond_3
    monitor-exit v9

    .line 161
    return-void

    .line 160
    .end local v2    # "files":[Ljava/io/File;
    .end local v3    # "i":I
    .end local v4    # "prefix":Ljava/lang/String;
    .end local v6    # "printJobCount":I
    .end local v7    # "tab":Ljava/lang/String;
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8
.end method

.method public getPrintJobInfo(Landroid/print/PrintJobId;I)Landroid/print/PrintJobInfo;
    .locals 5
    .param p1, "printJobId"    # Landroid/print/PrintJobId;
    .param p2, "appId"    # I

    .prologue
    .line 294
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 295
    :try_start_0
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 296
    .local v2, "printJobCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 297
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrintJobInfo;

    .line 298
    .local v1, "printJob":Landroid/print/PrintJobInfo;
    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/print/PrintJobId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, -0x2

    if-eq p2, v3, :cond_0

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getAppId()I

    move-result v3

    if-ne p2, v3, :cond_1

    .line 301
    :cond_0
    monitor-exit v4

    .line 304
    .end local v1    # "printJob":Landroid/print/PrintJobInfo;
    :goto_1
    return-object v1

    .line 296
    .restart local v1    # "printJob":Landroid/print/PrintJobInfo;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 304
    .end local v1    # "printJob":Landroid/print/PrintJobInfo;
    :cond_2
    const/4 v1, 0x0

    monitor-exit v4

    goto :goto_1

    .line 305
    .end local v0    # "i":I
    .end local v2    # "printJobCount":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getPrintJobInfos(Landroid/content/ComponentName;II)Ljava/util/List;
    .locals 11
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "state"    # I
    .param p3, "appId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ComponentName;",
            "II)",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrintJobInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 258
    .local v0, "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    iget-object v10, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v10

    .line 259
    :try_start_0
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 260
    .local v4, "printJobCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    move-object v1, v0

    .end local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .local v1, "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    :goto_0
    if-ge v2, v4, :cond_8

    .line 261
    :try_start_1
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/print/PrintJobInfo;

    .line 262
    .local v3, "printJob":Landroid/print/PrintJobInfo;
    invoke-virtual {v3}, Landroid/print/PrintJobInfo;->getPrinterId()Landroid/print/PrinterId;

    move-result-object v5

    .line 263
    .local v5, "printerId":Landroid/print/PrinterId;
    if-eqz p1, :cond_0

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    :cond_0
    const/4 v7, 0x1

    .line 266
    .local v7, "sameComponent":Z
    :goto_1
    const/4 v9, -0x2

    if-eq p3, v9, :cond_1

    invoke-virtual {v3}, Landroid/print/PrintJobInfo;->getAppId()I

    move-result v9

    if-ne v9, p3, :cond_6

    :cond_1
    const/4 v6, 0x1

    .line 268
    .local v6, "sameAppId":Z
    :goto_2
    invoke-virtual {v3}, Landroid/print/PrintJobInfo;->getState()I

    move-result v9

    if-eq p2, v9, :cond_4

    const/4 v9, -0x1

    if-eq p2, v9, :cond_4

    const/4 v9, -0x2

    if-ne p2, v9, :cond_2

    invoke-virtual {v3}, Landroid/print/PrintJobInfo;->getState()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/android/printspooler/model/PrintSpoolerService;->isStateVisibleToUser(I)Z

    move-result v9

    if-nez v9, :cond_4

    :cond_2
    const/4 v9, -0x3

    if-ne p2, v9, :cond_3

    invoke-virtual {v3}, Landroid/print/PrintJobInfo;->getState()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/android/printspooler/model/PrintSpoolerService;->isActiveState(I)Z

    move-result v9

    if-nez v9, :cond_4

    :cond_3
    const/4 v9, -0x4

    if-ne p2, v9, :cond_7

    invoke-virtual {v3}, Landroid/print/PrintJobInfo;->getState()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/android/printspooler/model/PrintSpoolerService;->isScheduledState(I)Z

    move-result v9

    if-eqz v9, :cond_7

    :cond_4
    const/4 v8, 0x1

    .line 276
    .local v8, "sameState":Z
    :goto_3
    if-eqz v7, :cond_a

    if-eqz v6, :cond_a

    if-eqz v8, :cond_a

    .line 277
    if-nez v1, :cond_9

    .line 278
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 280
    .end local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    :goto_4
    :try_start_2
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 260
    :goto_5
    add-int/lit8 v2, v2, 0x1

    move-object v1, v0

    .end local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    goto :goto_0

    .line 263
    .end local v6    # "sameAppId":Z
    .end local v7    # "sameComponent":Z
    .end local v8    # "sameState":Z
    :cond_5
    const/4 v7, 0x0

    goto :goto_1

    .line 266
    .restart local v7    # "sameComponent":Z
    :cond_6
    const/4 v6, 0x0

    goto :goto_2

    .line 268
    .restart local v6    # "sameAppId":Z
    :cond_7
    const/4 v8, 0x0

    goto :goto_3

    .line 283
    .end local v3    # "printJob":Landroid/print/PrintJobInfo;
    .end local v5    # "printerId":Landroid/print/PrinterId;
    .end local v6    # "sameAppId":Z
    .end local v7    # "sameComponent":Z
    :cond_8
    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 284
    return-object v1

    .line 283
    .end local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .end local v2    # "i":I
    .end local v4    # "printJobCount":I
    .restart local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    :catchall_0
    move-exception v9

    :goto_6
    :try_start_4
    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v9

    .end local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v2    # "i":I
    .restart local v4    # "printJobCount":I
    :catchall_1
    move-exception v9

    move-object v0, v1

    .end local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    goto :goto_6

    .end local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v3    # "printJob":Landroid/print/PrintJobInfo;
    .restart local v5    # "printerId":Landroid/print/PrinterId;
    .restart local v6    # "sameAppId":Z
    .restart local v7    # "sameComponent":Z
    .restart local v8    # "sameState":Z
    :cond_9
    move-object v0, v1

    .end local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    goto :goto_4

    .end local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    :cond_a
    move-object v0, v1

    .end local v1    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v0    # "foundPrintJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    goto :goto_5
.end method

.method public hasActivePrintJobsForServiceLocked(Landroid/content/ComponentName;)Z
    .locals 4
    .param p1, "service"    # Landroid/content/ComponentName;

    .prologue
    .line 549
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 550
    .local v2, "printJobCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 551
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrintJobInfo;

    .line 552
    .local v1, "printJob":Landroid/print/PrintJobInfo;
    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getState()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/android/printspooler/model/PrintSpoolerService;->isActiveState(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getPrinterId()Landroid/print/PrinterId;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getPrinterId()Landroid/print/PrinterId;

    move-result-object v3

    invoke-virtual {v3}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 554
    const/4 v3, 0x1

    .line 557
    .end local v1    # "printJob":Landroid/print/PrintJobInfo;
    :goto_1
    return v3

    .line 550
    .restart local v1    # "printJob":Landroid/print/PrintJobInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 557
    .end local v1    # "printJob":Landroid/print/PrintJobInfo;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public hasActivePrintJobsLocked()Z
    .locals 4

    .prologue
    .line 538
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 539
    .local v2, "printJobCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 540
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrintJobInfo;

    .line 541
    .local v1, "printJob":Landroid/print/PrintJobInfo;
    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getState()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/android/printspooler/model/PrintSpoolerService;->isActiveState(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 542
    const/4 v3, 0x1

    .line 545
    .end local v1    # "printJob":Landroid/print/PrintJobInfo;
    :goto_1
    return v3

    .line 539
    .restart local v1    # "printJob":Landroid/print/PrintJobInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 545
    .end local v1    # "printJob":Landroid/print/PrintJobInfo;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 132
    new-instance v0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;

    invoke-direct {v0, p0}, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;-><init>(Lcom/android/printspooler/model/PrintSpoolerService;)V

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 113
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 114
    new-instance v0, Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;

    invoke-direct {v2, p0, v4}, Lcom/android/printspooler/model/PrintSpoolerService$HandlerCallerCallback;-><init>(Lcom/android/printspooler/model/PrintSpoolerService;Lcom/android/printspooler/model/PrintSpoolerService$1;)V

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/HandlerCaller;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/internal/os/HandlerCaller$Callback;Z)V

    iput-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    .line 117
    new-instance v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    invoke-direct {v0, p0, v4}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;-><init>(Lcom/android/printspooler/model/PrintSpoolerService;Lcom/android/printspooler/model/PrintSpoolerService$1;)V

    iput-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPersistanceManager:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    .line 118
    new-instance v0, Lcom/android/printspooler/model/NotificationController;

    invoke-direct {v0, p0}, Lcom/android/printspooler/model/NotificationController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mNotificationController:Lcom/android/printspooler/model/NotificationController;

    .line 120
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPersistanceManager:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    invoke-virtual {v0}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->readStateLocked()V

    .line 122
    invoke-direct {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->handleReadPrintJobsLocked()V

    .line 123
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    sget-object v1, Lcom/android/printspooler/model/PrintSpoolerService;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 126
    :try_start_1
    sput-object p0, Lcom/android/printspooler/model/PrintSpoolerService;->sInstance:Lcom/android/printspooler/model/PrintSpoolerService;

    .line 127
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 128
    return-void

    .line 123
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 127
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public setPrintJobCancelling(Landroid/print/PrintJobId;Z)V
    .locals 5
    .param p1, "printJobId"    # Landroid/print/PrintJobId;
    .param p2, "cancelling"    # Z

    .prologue
    .line 606
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 607
    const/4 v2, -0x2

    :try_start_0
    invoke-virtual {p0, p1, v2}, Lcom/android/printspooler/model/PrintSpoolerService;->getPrintJobInfo(Landroid/print/PrintJobId;I)Landroid/print/PrintJobInfo;

    move-result-object v1

    .line 608
    .local v1, "printJob":Landroid/print/PrintJobInfo;
    if-eqz v1, :cond_1

    .line 609
    invoke-virtual {v1, p2}, Landroid/print/PrintJobInfo;->setCancelling(Z)V

    .line 610
    invoke-direct {p0, v1}, Lcom/android/printspooler/model/PrintSpoolerService;->shouldPersistPrintJob(Landroid/print/PrintJobInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 611
    iget-object v2, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPersistanceManager:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    invoke-virtual {v2}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->writeStateLocked()V

    .line 613
    :cond_0
    iget-object v2, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mNotificationController:Lcom/android/printspooler/model/NotificationController;

    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-virtual {v2, v4}, Lcom/android/printspooler/model/NotificationController;->onUpdateNotifications(Ljava/util/List;)V

    .line 615
    iget-object v2, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    const/4 v4, 0x6

    invoke-virtual {v2, v4, v1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 618
    .local v0, "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v2, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    .line 620
    .end local v0    # "message":Landroid/os/Message;
    :cond_1
    monitor-exit v3

    .line 621
    return-void

    .line 620
    .end local v1    # "printJob":Landroid/print/PrintJobInfo;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z
    .locals 10
    .param p1, "printJobId"    # Landroid/print/PrintJobId;
    .param p2, "state"    # I
    .param p3, "error"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 475
    const/4 v5, 0x0

    .line 477
    .local v5, "success":Z
    iget-object v7, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 478
    const/4 v8, -0x2

    :try_start_0
    invoke-virtual {p0, p1, v8}, Lcom/android/printspooler/model/PrintSpoolerService;->getPrintJobInfo(Landroid/print/PrintJobId;I)Landroid/print/PrintJobInfo;

    move-result-object v2

    .line 479
    .local v2, "printJob":Landroid/print/PrintJobInfo;
    if-eqz v2, :cond_4

    .line 480
    invoke-virtual {v2}, Landroid/print/PrintJobInfo;->getState()I

    move-result v1

    .line 481
    .local v1, "oldState":I
    if-ne v1, p2, :cond_0

    .line 482
    monitor-exit v7

    .line 534
    .end local v1    # "oldState":I
    :goto_0
    return v6

    .line 485
    .restart local v1    # "oldState":I
    :cond_0
    const/4 v5, 0x1

    .line 487
    invoke-virtual {v2, p2}, Landroid/print/PrintJobInfo;->setState(I)V

    .line 488
    invoke-virtual {v2, p3}, Landroid/print/PrintJobInfo;->setStateReason(Ljava/lang/String;)V

    .line 489
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/print/PrintJobInfo;->setCancelling(Z)V

    .line 492
    const-string v6, "PrintSpoolerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[STATE CHANGED] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    packed-switch p2, :pswitch_data_0

    .line 517
    :cond_1
    :goto_1
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/android/printspooler/model/PrintSpoolerService;->shouldPersistPrintJob(Landroid/print/PrintJobInfo;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 518
    iget-object v6, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPersistanceManager:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    invoke-virtual {v6}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->writeStateLocked()V

    .line 521
    :cond_2
    invoke-virtual {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->hasActivePrintJobsLocked()Z

    move-result v6

    if-nez v6, :cond_3

    .line 522
    invoke-direct {p0}, Lcom/android/printspooler/model/PrintSpoolerService;->notifyOnAllPrintJobsHandled()V

    .line 525
    :cond_3
    iget-object v6, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    const/4 v8, 0x6

    invoke-virtual {v6, v8, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 528
    .local v0, "message":Landroid/os/Message;
    iget-object v6, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v6, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    .line 530
    iget-object v6, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mNotificationController:Lcom/android/printspooler/model/NotificationController;

    iget-object v8, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-virtual {v6, v8}, Lcom/android/printspooler/model/NotificationController;->onUpdateNotifications(Ljava/util/List;)V

    .line 532
    .end local v0    # "message":Landroid/os/Message;
    .end local v1    # "oldState":I
    :cond_4
    monitor-exit v7

    move v6, v5

    .line 534
    goto :goto_0

    .line 498
    .restart local v1    # "oldState":I
    :pswitch_1
    iget-object v6, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 499
    invoke-virtual {v2}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/printspooler/model/PrintSpoolerService;->removePrintJobFileLocked(Landroid/print/PrintJobId;)V

    .line 503
    :pswitch_2
    invoke-virtual {v2}, Landroid/print/PrintJobInfo;->getPrinterId()Landroid/print/PrinterId;

    move-result-object v3

    .line 504
    .local v3, "printerId":Landroid/print/PrinterId;
    if-eqz v3, :cond_1

    .line 505
    invoke-virtual {v3}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v4

    .line 506
    .local v4, "service":Landroid/content/ComponentName;
    invoke-virtual {p0, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->hasActivePrintJobsForServiceLocked(Landroid/content/ComponentName;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 507
    invoke-direct {p0, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->sendOnAllPrintJobsForServiceHandled(Landroid/content/ComponentName;)V

    goto :goto_1

    .line 532
    .end local v1    # "oldState":I
    .end local v2    # "printJob":Landroid/print/PrintJobInfo;
    .end local v3    # "printerId":Landroid/print/PrinterId;
    .end local v4    # "service":Landroid/content/ComponentName;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 513
    .restart local v1    # "oldState":I
    .restart local v2    # "printJob":Landroid/print/PrintJobInfo;
    :pswitch_3
    :try_start_1
    new-instance v6, Landroid/print/PrintJobInfo;

    invoke-direct {v6, v2}, Landroid/print/PrintJobInfo;-><init>(Landroid/print/PrintJobInfo;)V

    invoke-direct {p0, v6}, Lcom/android/printspooler/model/PrintSpoolerService;->sendOnPrintJobQueued(Landroid/print/PrintJobInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 495
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setPrintJobTag(Landroid/print/PrintJobId;Ljava/lang/String;)Z
    .locals 5
    .param p1, "printJobId"    # Landroid/print/PrintJobId;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 584
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 585
    const/4 v4, -0x2

    :try_start_0
    invoke-virtual {p0, p1, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->getPrintJobInfo(Landroid/print/PrintJobId;I)Landroid/print/PrintJobInfo;

    move-result-object v0

    .line 586
    .local v0, "printJob":Landroid/print/PrintJobInfo;
    if-eqz v0, :cond_3

    .line 587
    invoke-virtual {v0}, Landroid/print/PrintJobInfo;->getTag()Ljava/lang/String;

    move-result-object v1

    .line 588
    .local v1, "printJobTag":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 589
    if-nez p2, :cond_1

    .line 590
    monitor-exit v3

    .line 602
    .end local v1    # "printJobTag":Ljava/lang/String;
    :goto_0
    return v2

    .line 592
    .restart local v1    # "printJobTag":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 593
    monitor-exit v3

    goto :goto_0

    .line 601
    .end local v0    # "printJob":Landroid/print/PrintJobInfo;
    .end local v1    # "printJobTag":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 595
    .restart local v0    # "printJob":Landroid/print/PrintJobInfo;
    .restart local v1    # "printJobTag":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual {v0, p2}, Landroid/print/PrintJobInfo;->setTag(Ljava/lang/String;)V

    .line 596
    invoke-direct {p0, v0}, Lcom/android/printspooler/model/PrintSpoolerService;->shouldPersistPrintJob(Landroid/print/PrintJobInfo;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 597
    iget-object v2, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPersistanceManager:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    invoke-virtual {v2}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->writeStateLocked()V

    .line 599
    :cond_2
    const/4 v2, 0x1

    monitor-exit v3

    goto :goto_0

    .line 601
    .end local v1    # "printJobTag":Ljava/lang/String;
    :cond_3
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public updatePrintJobUserConfigurableOptionsNoPersistence(Landroid/print/PrintJobInfo;)V
    .locals 7
    .param p1, "printJob"    # Landroid/print/PrintJobInfo;

    .prologue
    .line 624
    iget-object v4, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 625
    :try_start_0
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 626
    .local v2, "printJobCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 627
    iget-object v3, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintJobInfo;

    .line 628
    .local v0, "cachedPrintJob":Landroid/print/PrintJobInfo;
    invoke-virtual {v0}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v3

    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/print/PrintJobId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 629
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getPrinterId()Landroid/print/PrinterId;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/print/PrintJobInfo;->setPrinterId(Landroid/print/PrinterId;)V

    .line 630
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getPrinterName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/print/PrintJobInfo;->setPrinterName(Ljava/lang/String;)V

    .line 631
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getCopies()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/print/PrintJobInfo;->setCopies(I)V

    .line 632
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getDocumentInfo()Landroid/print/PrintDocumentInfo;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/print/PrintJobInfo;->setDocumentInfo(Landroid/print/PrintDocumentInfo;)V

    .line 633
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getPages()[Landroid/print/PageRange;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/print/PrintJobInfo;->setPages([Landroid/print/PageRange;)V

    .line 634
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/print/PrintJobInfo;->setAttributes(Landroid/print/PrintAttributes;)V

    .line 635
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getAdvancedOptions()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/print/PrintJobInfo;->setAdvancedOptions(Landroid/os/Bundle;)V

    .line 636
    monitor-exit v4

    return-void

    .line 626
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 639
    .end local v0    # "cachedPrintJob":Landroid/print/PrintJobInfo;
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No print job with id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 640
    .end local v1    # "i":I
    .end local v2    # "printJobCount":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public writePrintJobData(Landroid/os/ParcelFileDescriptor;Landroid/print/PrintJobId;)V
    .locals 4
    .param p1, "fd"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "printJobId"    # Landroid/print/PrintJobId;

    .prologue
    .line 394
    iget-object v2, p0, Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 395
    const/4 v1, -0x2

    :try_start_0
    invoke-virtual {p0, p2, v1}, Lcom/android/printspooler/model/PrintSpoolerService;->getPrintJobInfo(Landroid/print/PrintJobId;I)Landroid/print/PrintJobInfo;

    move-result-object v0

    .line 396
    .local v0, "printJob":Landroid/print/PrintJobInfo;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    new-instance v2, Lcom/android/printspooler/model/PrintSpoolerService$1;

    invoke-direct {v2, p0, v0, p2, p1}, Lcom/android/printspooler/model/PrintSpoolerService$1;-><init>(Lcom/android/printspooler/model/PrintSpoolerService;Landroid/print/PrintJobInfo;Landroid/print/PrintJobId;Landroid/os/ParcelFileDescriptor;)V

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v1}, Lcom/android/printspooler/model/PrintSpoolerService$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 429
    return-void

    .line 396
    .end local v0    # "printJob":Landroid/print/PrintJobInfo;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
