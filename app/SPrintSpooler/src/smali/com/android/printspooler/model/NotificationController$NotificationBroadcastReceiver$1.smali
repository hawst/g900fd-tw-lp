.class Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;
.super Landroid/os/AsyncTask;
.source "NotificationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;->handleCancelPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;

.field final synthetic val$printJobId:Landroid/print/PrintJobId;

.field final synthetic val$wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>(Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;Landroid/print/PrintJobId;Landroid/os/PowerManager$WakeLock;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->this$0:Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;

    iput-object p2, p0, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->val$printJobId:Landroid/print/PrintJobId;

    iput-object p3, p0, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->val$wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 349
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 360
    :try_start_0
    const-string v2, "print"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/print/IPrintManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/print/IPrintManager;

    move-result-object v0

    .line 362
    .local v0, "printManager":Landroid/print/IPrintManager;
    iget-object v2, p0, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->val$printJobId:Landroid/print/PrintJobId;

    const/4 v3, -0x2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-interface {v0, v2, v3, v4}, Landroid/print/IPrintManager;->cancelPrintJob(Landroid/print/PrintJobId;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    iget-object v2, p0, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->val$wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 369
    .end local v0    # "printManager":Landroid/print/IPrintManager;
    :goto_0
    const/4 v2, 0x0

    return-object v2

    .line 364
    :catch_0
    move-exception v1

    .line 365
    .local v1, "re":Landroid/os/RemoteException;
    :try_start_1
    const-string v2, "NotificationBroadcastReceiver"

    const-string v3, "Error requesting print job cancellation"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 367
    iget-object v2, p0, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->val$wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .end local v1    # "re":Landroid/os/RemoteException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->val$wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v2
.end method
