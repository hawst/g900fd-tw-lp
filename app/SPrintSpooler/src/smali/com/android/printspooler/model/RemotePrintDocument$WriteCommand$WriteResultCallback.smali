.class final Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;
.super Landroid/print/IWriteResultCallback$Stub;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WriteResultCallback"
.end annotation


# instance fields
.field private final mWeakHandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 1095
    invoke-direct {p0}, Landroid/print/IWriteResultCallback$Stub;-><init>()V

    .line 1096
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    .line 1097
    return-void
.end method


# virtual methods
.method public onWriteCanceled(I)V
    .locals 3
    .param p1, "sequence"    # I

    .prologue
    .line 1128
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 1129
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 1130
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1133
    :cond_0
    return-void
.end method

.method public onWriteFailed(Ljava/lang/CharSequence;I)V
    .locals 3
    .param p1, "error"    # Ljava/lang/CharSequence;
    .param p2, "sequence"    # I

    .prologue
    .line 1119
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 1120
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 1121
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1124
    :cond_0
    return-void
.end method

.method public onWriteFinished([Landroid/print/PageRange;I)V
    .locals 3
    .param p1, "pages"    # [Landroid/print/PageRange;
    .param p2, "sequence"    # I

    .prologue
    .line 1110
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 1111
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 1112
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1115
    :cond_0
    return-void
.end method

.method public onWriteStarted(Landroid/os/ICancellationSignal;I)V
    .locals 3
    .param p1, "cancellation"    # Landroid/os/ICancellationSignal;
    .param p2, "sequence"    # I

    .prologue
    .line 1101
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 1102
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 1103
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1106
    :cond_0
    return-void
.end method
