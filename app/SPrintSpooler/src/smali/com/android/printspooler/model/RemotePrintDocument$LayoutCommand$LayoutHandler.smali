.class final Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;
.super Landroid/os/Handler;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LayoutHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;Landroid/os/Looper;)V
    .locals 2
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 798
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;

    .line 799
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 800
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    .line 804
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 829
    :goto_0
    return-void

    .line 806
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/ICancellationSignal;

    .line 807
    .local v0, "cancellation":Landroid/os/ICancellationSignal;
    iget v4, p1, Landroid/os/Message;->arg1:I

    .line 808
    .local v4, "sequence":I
    iget-object v5, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutStarted(Landroid/os/ICancellationSignal;I)V
    invoke-static {v5, v0, v4}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->access$1400(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;Landroid/os/ICancellationSignal;I)V

    goto :goto_0

    .line 812
    .end local v0    # "cancellation":Landroid/os/ICancellationSignal;
    .end local v4    # "sequence":I
    :pswitch_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/print/PrintDocumentInfo;

    .line 813
    .local v3, "info":Landroid/print/PrintDocumentInfo;
    iget v5, p1, Landroid/os/Message;->arg1:I

    if-ne v5, v1, :cond_0

    .line 814
    .local v1, "changed":Z
    :goto_1
    iget v4, p1, Landroid/os/Message;->arg2:I

    .line 815
    .restart local v4    # "sequence":I
    iget-object v5, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutFinished(Landroid/print/PrintDocumentInfo;ZI)V
    invoke-static {v5, v3, v1, v4}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->access$1500(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;Landroid/print/PrintDocumentInfo;ZI)V

    goto :goto_0

    .line 813
    .end local v1    # "changed":Z
    .end local v4    # "sequence":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 819
    .end local v3    # "info":Landroid/print/PrintDocumentInfo;
    :pswitch_2
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    .line 820
    .local v2, "error":Ljava/lang/CharSequence;
    iget v4, p1, Landroid/os/Message;->arg1:I

    .line 821
    .restart local v4    # "sequence":I
    iget-object v5, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutFailed(Ljava/lang/CharSequence;I)V
    invoke-static {v5, v2, v4}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->access$1600(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 825
    .end local v2    # "error":Ljava/lang/CharSequence;
    .end local v4    # "sequence":I
    :pswitch_3
    iget v4, p1, Landroid/os/Message;->arg1:I

    .line 826
    .restart local v4    # "sequence":I
    iget-object v5, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutCanceled(I)V
    invoke-static {v5, v4}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->access$1700(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;I)V

    goto :goto_0

    .line 804
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
