.class public final Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;
.super Ljava/lang/Object;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PageContentRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PageContentProvider"
.end annotation


# instance fields
.field private mOwner:Landroid/view/View;

.field private final mPageIndex:I

.field final synthetic this$0:Lcom/android/printspooler/model/PageContentRepository;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/model/PageContentRepository;ILandroid/view/View;)V
    .locals 0
    .param p2, "pageIndex"    # I
    .param p3, "owner"    # Landroid/view/View;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    iput p2, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->mPageIndex:I

    .line 206
    iput-object p3, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->mOwner:Landroid/view/View;

    .line 207
    return-void
.end method

.method static synthetic access$000(Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    .prologue
    .line 200
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->mPageIndex:I

    return v0
.end method


# virtual methods
.method cancelLoad()V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # invokes: Lcom/android/printspooler/model/PageContentRepository;->throwIfDestroyed()V
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$100(Lcom/android/printspooler/model/PageContentRepository;)V

    .line 241
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mState:I
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$500(Lcom/android/printspooler/model/PageContentRepository;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 242
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$600(Lcom/android/printspooler/model/PageContentRepository;)Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->mPageIndex:I

    invoke-virtual {v0, v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->cancelRendering(I)V

    .line 244
    :cond_0
    return-void
.end method

.method public getPageContent(Lcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V
    .locals 4
    .param p1, "renderSpec"    # Lcom/android/printspooler/model/PageContentRepository$RenderSpec;
    .param p2, "callback"    # Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    .prologue
    const/4 v3, -0x1

    .line 218
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # invokes: Lcom/android/printspooler/model/PageContentRepository;->throwIfDestroyed()V
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$100(Lcom/android/printspooler/model/PageContentRepository;)V

    .line 220
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # setter for: Lcom/android/printspooler/model/PageContentRepository;->mLastRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;
    invoke-static {v0, p1}, Lcom/android/printspooler/model/PageContentRepository;->access$202(Lcom/android/printspooler/model/PageContentRepository;Lcom/android/printspooler/model/PageContentRepository$RenderSpec;)Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    .line 224
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadFirstShownPage:I
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$300(Lcom/android/printspooler/model/PageContentRepository;)I

    move-result v0

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadLastShownPage:I
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$400(Lcom/android/printspooler/model/PageContentRepository;)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 226
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadFirstShownPage:I
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository;->access$300(Lcom/android/printspooler/model/PageContentRepository;)I

    move-result v1

    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadLastShownPage:I
    invoke-static {v2}, Lcom/android/printspooler/model/PageContentRepository;->access$400(Lcom/android/printspooler/model/PageContentRepository;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/printspooler/model/PageContentRepository;->startPreload(II)V

    .line 227
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # setter for: Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadFirstShownPage:I
    invoke-static {v0, v3}, Lcom/android/printspooler/model/PageContentRepository;->access$302(Lcom/android/printspooler/model/PageContentRepository;I)I

    .line 228
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # setter for: Lcom/android/printspooler/model/PageContentRepository;->mScheduledPreloadLastShownPage:I
    invoke-static {v0, v3}, Lcom/android/printspooler/model/PageContentRepository;->access$402(Lcom/android/printspooler/model/PageContentRepository;I)I

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mState:I
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$500(Lcom/android/printspooler/model/PageContentRepository;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 232
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$600(Lcom/android/printspooler/model/PageContentRepository;)Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->mPageIndex:I

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->renderPage(ILcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->this$0:Lcom/android/printspooler/model/PageContentRepository;

    # getter for: Lcom/android/printspooler/model/PageContentRepository;->mRenderer:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository;->access$600(Lcom/android/printspooler/model/PageContentRepository;)Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->mPageIndex:I

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->getCachedPage(ILcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V

    goto :goto_0
.end method
