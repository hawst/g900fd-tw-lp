.class final Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;
.super Ljava/lang/Object;
.source "PrintSpoolerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PrintSpoolerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PersistenceManager"
.end annotation


# instance fields
.field private final mStatePersistFile:Landroid/util/AtomicFile;

.field private mWriteStateScheduled:Z

.field final synthetic this$0:Lcom/android/printspooler/model/PrintSpoolerService;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/model/PrintSpoolerService;)V
    .locals 4

    .prologue
    .line 725
    iput-object p1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 726
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lcom/android/printspooler/model/PrintSpoolerService;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "print_spooler_state.xml"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;

    .line 728
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/model/PrintSpoolerService;Lcom/android/printspooler/model/PrintSpoolerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService;
    .param p2, "x1"    # Lcom/android/printspooler/model/PrintSpoolerService$1;

    .prologue
    .line 661
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;-><init>(Lcom/android/printspooler/model/PrintSpoolerService;)V

    return-void
.end method

.method private accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z
    .locals 2
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "type"    # I
    .param p3, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1202
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-eq v1, p2, :cond_1

    .line 1212
    :cond_0
    :goto_0
    return v0

    .line 1205
    :cond_1
    if-eqz p3, :cond_3

    .line 1206
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1212
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1209
    :cond_3
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_0
.end method

.method static synthetic access$602(Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;
    .param p1, "x1"    # Z

    .prologue
    .line 661
    iput-boolean p1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mWriteStateScheduled:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    .prologue
    .line 661
    invoke-direct {p0}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->doWriteStateLocked()V

    return-void
.end method

.method private doWriteStateLocked()V
    .locals 34

    .prologue
    .line 752
    const-string v29, "PrintSpoolerService"

    const-string v30, "[PERSIST START]"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    const/16 v16, 0x0

    .line 756
    .local v16, "out":Ljava/io/FileOutputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v16

    .line 758
    new-instance v24, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct/range {v24 .. v24}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 759
    .local v24, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    const-string v29, "utf-8"

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    move-object/from16 v2, v29

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 760
    const/16 v29, 0x0

    const/16 v30, 0x1

    invoke-static/range {v30 .. v30}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v30

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 761
    const/16 v29, 0x0

    const-string v30, "spooler"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    move-object/from16 v29, v0

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;
    invoke-static/range {v29 .. v29}, Lcom/android/printspooler/model/PrintSpoolerService;->access$800(Lcom/android/printspooler/model/PrintSpoolerService;)Ljava/util/List;

    move-result-object v20

    .line 765
    .local v20, "printJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v19

    .line 766
    .local v19, "printJobCount":I
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_0
    move/from16 v0, v19

    if-ge v12, v0, :cond_10

    .line 767
    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/print/PrintJobInfo;

    .line 769
    .local v18, "printJob":Landroid/print/PrintJobInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    # invokes: Lcom/android/printspooler/model/PrintSpoolerService;->shouldPersistPrintJob(Landroid/print/PrintJobInfo;)Z
    invoke-static {v0, v1}, Lcom/android/printspooler/model/PrintSpoolerService;->access$900(Lcom/android/printspooler/model/PrintSpoolerService;Landroid/print/PrintJobInfo;)Z

    move-result v29

    if-nez v29, :cond_0

    .line 766
    :goto_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 773
    :cond_0
    const/16 v29, 0x0

    const-string v30, "job"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 775
    const/16 v29, 0x0

    const-string v30, "id"

    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/print/PrintJobId;->flattenToString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 776
    const/16 v29, 0x0

    const-string v30, "label"

    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getLabel()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 777
    const/16 v29, 0x0

    const-string v30, "state"

    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getState()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 778
    const/16 v29, 0x0

    const-string v30, "appId"

    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getAppId()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 779
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getTag()Ljava/lang/String;

    move-result-object v27

    .line 780
    .local v27, "tag":Ljava/lang/String;
    if-eqz v27, :cond_1

    .line 781
    const/16 v29, 0x0

    const-string v30, "tag"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v27

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 783
    :cond_1
    const/16 v29, 0x0

    const-string v30, "creationTime"

    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getCreationTime()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 785
    const/16 v29, 0x0

    const-string v30, "copies"

    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getCopies()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 786
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getPrinterName()Ljava/lang/String;

    move-result-object v22

    .line 787
    .local v22, "printerName":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_2

    .line 788
    const/16 v29, 0x0

    const-string v30, "printerName"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 790
    :cond_2
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getStateReason()Ljava/lang/String;

    move-result-object v25

    .line 791
    .local v25, "stateReason":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_3

    .line 792
    const/16 v29, 0x0

    const-string v30, "stateReason"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v25

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 794
    :cond_3
    const/16 v29, 0x0

    const-string v30, "cancelling"

    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->isCancelling()Z

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 797
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getPrinterId()Landroid/print/PrinterId;

    move-result-object v21

    .line 798
    .local v21, "printerId":Landroid/print/PrinterId;
    if-eqz v21, :cond_4

    .line 799
    const/16 v29, 0x0

    const-string v30, "printerId"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 800
    const/16 v29, 0x0

    const-string v30, "localId"

    invoke-virtual/range {v21 .. v21}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 801
    const/16 v29, 0x0

    const-string v30, "serviceName"

    invoke-virtual/range {v21 .. v21}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 803
    const/16 v29, 0x0

    const-string v30, "printerId"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 806
    :cond_4
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getPages()[Landroid/print/PageRange;

    move-result-object v17

    .line 807
    .local v17, "pages":[Landroid/print/PageRange;
    if-eqz v17, :cond_5

    .line 808
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v29, v0

    move/from16 v0, v29

    if-ge v9, v0, :cond_5

    .line 809
    const/16 v29, 0x0

    const-string v30, "pageRange"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 810
    const/16 v29, 0x0

    const-string v30, "start"

    aget-object v31, v17, v9

    invoke-virtual/range {v31 .. v31}, Landroid/print/PageRange;->getStart()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 812
    const/16 v29, 0x0

    const-string v30, "end"

    aget-object v31, v17, v9

    invoke-virtual/range {v31 .. v31}, Landroid/print/PageRange;->getEnd()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 814
    const/16 v29, 0x0

    const-string v30, "pageRange"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 808
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 818
    .end local v9    # "i":I
    :cond_5
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v5

    .line 819
    .local v5, "attributes":Landroid/print/PrintAttributes;
    if-eqz v5, :cond_9

    .line 820
    const/16 v29, 0x0

    const-string v30, "attributes"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 822
    invoke-virtual {v5}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v6

    .line 823
    .local v6, "colorMode":I
    const/16 v29, 0x0

    const-string v30, "colorMode"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 826
    invoke-virtual {v5}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v15

    .line 827
    .local v15, "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    if-eqz v15, :cond_6

    .line 828
    const/16 v29, 0x0

    const-string v30, "mediaSize"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 829
    const/16 v29, 0x0

    const-string v30, "id"

    invoke-virtual {v15}, Landroid/print/PrintAttributes$MediaSize;->getId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 830
    const/16 v29, 0x0

    const-string v30, "widthMils"

    invoke-virtual {v15}, Landroid/print/PrintAttributes$MediaSize;->getWidthMils()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 832
    const/16 v29, 0x0

    const-string v30, "heightMils"

    invoke-virtual {v15}, Landroid/print/PrintAttributes$MediaSize;->getHeightMils()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 836
    iget-object v0, v15, Landroid/print/PrintAttributes$MediaSize;->mPackageName:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_c

    iget v0, v15, Landroid/print/PrintAttributes$MediaSize;->mLabelResId:I

    move/from16 v29, v0

    if-lez v29, :cond_c

    .line 838
    const/16 v29, 0x0

    const-string v30, "packageName"

    iget-object v0, v15, Landroid/print/PrintAttributes$MediaSize;->mPackageName:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 840
    const/16 v29, 0x0

    const-string v30, "labelResId"

    iget v0, v15, Landroid/print/PrintAttributes$MediaSize;->mLabelResId:I

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 846
    :goto_3
    const/16 v29, 0x0

    const-string v30, "mediaSize"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 849
    :cond_6
    invoke-virtual {v5}, Landroid/print/PrintAttributes;->getResolution()Landroid/print/PrintAttributes$Resolution;

    move-result-object v23

    .line 850
    .local v23, "resolution":Landroid/print/PrintAttributes$Resolution;
    if-eqz v23, :cond_7

    .line 851
    const/16 v29, 0x0

    const-string v30, "resolution"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 852
    const/16 v29, 0x0

    const-string v30, "id"

    invoke-virtual/range {v23 .. v23}, Landroid/print/PrintAttributes$Resolution;->getId()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 853
    const/16 v29, 0x0

    const-string v30, "horizontalDip"

    invoke-virtual/range {v23 .. v23}, Landroid/print/PrintAttributes$Resolution;->getHorizontalDpi()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 855
    const/16 v29, 0x0

    const-string v30, "verticalDpi"

    invoke-virtual/range {v23 .. v23}, Landroid/print/PrintAttributes$Resolution;->getVerticalDpi()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 857
    const/16 v29, 0x0

    const-string v30, "label"

    invoke-virtual/range {v23 .. v23}, Landroid/print/PrintAttributes$Resolution;->getLabel()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 859
    const/16 v29, 0x0

    const-string v30, "resolution"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 862
    :cond_7
    invoke-virtual {v5}, Landroid/print/PrintAttributes;->getMinMargins()Landroid/print/PrintAttributes$Margins;

    move-result-object v14

    .line 863
    .local v14, "margins":Landroid/print/PrintAttributes$Margins;
    if-eqz v14, :cond_8

    .line 864
    const/16 v29, 0x0

    const-string v30, "margins"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 865
    const/16 v29, 0x0

    const-string v30, "leftMils"

    invoke-virtual {v14}, Landroid/print/PrintAttributes$Margins;->getLeftMils()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 867
    const/16 v29, 0x0

    const-string v30, "topMils"

    invoke-virtual {v14}, Landroid/print/PrintAttributes$Margins;->getTopMils()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 869
    const/16 v29, 0x0

    const-string v30, "rightMils"

    invoke-virtual {v14}, Landroid/print/PrintAttributes$Margins;->getRightMils()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 871
    const/16 v29, 0x0

    const-string v30, "bottomMils"

    invoke-virtual {v14}, Landroid/print/PrintAttributes$Margins;->getBottomMils()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 873
    const/16 v29, 0x0

    const-string v30, "margins"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 876
    :cond_8
    const/16 v29, 0x0

    const-string v30, "attributes"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 879
    .end local v6    # "colorMode":I
    .end local v14    # "margins":Landroid/print/PrintAttributes$Margins;
    .end local v15    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v23    # "resolution":Landroid/print/PrintAttributes$Resolution;
    :cond_9
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getDocumentInfo()Landroid/print/PrintDocumentInfo;

    move-result-object v7

    .line 880
    .local v7, "documentInfo":Landroid/print/PrintDocumentInfo;
    if-eqz v7, :cond_a

    .line 881
    const/16 v29, 0x0

    const-string v30, "documentInfo"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 882
    const/16 v29, 0x0

    const-string v30, "name"

    invoke-virtual {v7}, Landroid/print/PrintDocumentInfo;->getName()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 883
    const/16 v29, 0x0

    const-string v30, "contentType"

    invoke-virtual {v7}, Landroid/print/PrintDocumentInfo;->getContentType()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 885
    const/16 v29, 0x0

    const-string v30, "pageCount"

    invoke-virtual {v7}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 887
    const/16 v29, 0x0

    const-string v30, "dataSize"

    invoke-virtual {v7}, Landroid/print/PrintDocumentInfo;->getDataSize()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 889
    const/16 v29, 0x0

    const-string v30, "documentInfo"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 892
    :cond_a
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintJobInfo;->getAdvancedOptions()Landroid/os/Bundle;

    move-result-object v4

    .line 893
    .local v4, "advancedOptions":Landroid/os/Bundle;
    if-eqz v4, :cond_f

    .line 894
    const/16 v29, 0x0

    const-string v30, "advancedOptions"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 895
    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_b
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_e

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 896
    .local v13, "key":Ljava/lang/String;
    invoke-virtual {v4, v13}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v28

    .line 897
    .local v28, "value":Ljava/lang/Object;
    move-object/from16 v0, v28

    instance-of v0, v0, Ljava/lang/String;

    move/from16 v29, v0

    if-eqz v29, :cond_d

    .line 898
    move-object/from16 v0, v28

    check-cast v0, Ljava/lang/String;

    move-object/from16 v26, v0

    .line 899
    .local v26, "stringValue":Ljava/lang/String;
    const/16 v29, 0x0

    const-string v30, "advancedOption"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 900
    const/16 v29, 0x0

    const-string v30, "key"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2, v13}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 901
    const/16 v29, 0x0

    const-string v30, "type"

    const-string v31, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 902
    const/16 v29, 0x0

    const-string v30, "value"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v26

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 903
    const/16 v29, 0x0

    const-string v30, "advancedOption"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 929
    .end local v4    # "advancedOptions":Landroid/os/Bundle;
    .end local v5    # "attributes":Landroid/print/PrintAttributes;
    .end local v7    # "documentInfo":Landroid/print/PrintDocumentInfo;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v12    # "j":I
    .end local v13    # "key":Ljava/lang/String;
    .end local v17    # "pages":[Landroid/print/PageRange;
    .end local v18    # "printJob":Landroid/print/PrintJobInfo;
    .end local v19    # "printJobCount":I
    .end local v20    # "printJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .end local v21    # "printerId":Landroid/print/PrinterId;
    .end local v22    # "printerName":Ljava/lang/String;
    .end local v24    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    .end local v25    # "stateReason":Ljava/lang/String;
    .end local v26    # "stringValue":Ljava/lang/String;
    .end local v27    # "tag":Ljava/lang/String;
    .end local v28    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v8

    .line 930
    .local v8, "e":Ljava/io/IOException;
    :try_start_1
    const-string v29, "PrintSpoolerService"

    const-string v30, "Failed to write state, restoring backup."

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-static {v0, v1, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 931
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 933
    invoke-static/range {v16 .. v16}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 935
    .end local v8    # "e":Ljava/io/IOException;
    :goto_5
    return-void

    .line 843
    .restart local v5    # "attributes":Landroid/print/PrintAttributes;
    .restart local v6    # "colorMode":I
    .restart local v12    # "j":I
    .restart local v15    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    .restart local v17    # "pages":[Landroid/print/PageRange;
    .restart local v18    # "printJob":Landroid/print/PrintJobInfo;
    .restart local v19    # "printJobCount":I
    .restart local v20    # "printJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v21    # "printerId":Landroid/print/PrinterId;
    .restart local v22    # "printerName":Ljava/lang/String;
    .restart local v24    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    .restart local v25    # "stateReason":Ljava/lang/String;
    .restart local v27    # "tag":Ljava/lang/String;
    :cond_c
    const/16 v29, 0x0

    :try_start_2
    const-string v30, "label"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/android/printspooler/model/PrintSpoolerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v15, v0}, Landroid/print/PrintAttributes$MediaSize;->getLabel(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    .line 933
    .end local v5    # "attributes":Landroid/print/PrintAttributes;
    .end local v6    # "colorMode":I
    .end local v12    # "j":I
    .end local v15    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v17    # "pages":[Landroid/print/PageRange;
    .end local v18    # "printJob":Landroid/print/PrintJobInfo;
    .end local v19    # "printJobCount":I
    .end local v20    # "printJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .end local v21    # "printerId":Landroid/print/PrinterId;
    .end local v22    # "printerName":Ljava/lang/String;
    .end local v24    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    .end local v25    # "stateReason":Ljava/lang/String;
    .end local v27    # "tag":Ljava/lang/String;
    :catchall_0
    move-exception v29

    invoke-static/range {v16 .. v16}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v29

    .line 904
    .restart local v4    # "advancedOptions":Landroid/os/Bundle;
    .restart local v5    # "attributes":Landroid/print/PrintAttributes;
    .restart local v7    # "documentInfo":Landroid/print/PrintDocumentInfo;
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v12    # "j":I
    .restart local v13    # "key":Ljava/lang/String;
    .restart local v17    # "pages":[Landroid/print/PageRange;
    .restart local v18    # "printJob":Landroid/print/PrintJobInfo;
    .restart local v19    # "printJobCount":I
    .restart local v20    # "printJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    .restart local v21    # "printerId":Landroid/print/PrinterId;
    .restart local v22    # "printerName":Ljava/lang/String;
    .restart local v24    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    .restart local v25    # "stateReason":Ljava/lang/String;
    .restart local v27    # "tag":Ljava/lang/String;
    .restart local v28    # "value":Ljava/lang/Object;
    :cond_d
    :try_start_3
    move-object/from16 v0, v28

    instance-of v0, v0, Ljava/lang/Integer;

    move/from16 v29, v0

    if-eqz v29, :cond_b

    .line 905
    check-cast v28, Ljava/lang/Integer;

    .end local v28    # "value":Ljava/lang/Object;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    .line 906
    .local v11, "intValue":Ljava/lang/String;
    const/16 v29, 0x0

    const-string v30, "advancedOption"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 907
    const/16 v29, 0x0

    const-string v30, "key"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2, v13}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 908
    const/16 v29, 0x0

    const-string v30, "type"

    const-string v31, "int"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 909
    const/16 v29, 0x0

    const-string v30, "value"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 910
    const/16 v29, 0x0

    const-string v30, "advancedOption"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto/16 :goto_4

    .line 913
    .end local v11    # "intValue":Ljava/lang/String;
    .end local v13    # "key":Ljava/lang/String;
    :cond_e
    const/16 v29, 0x0

    const-string v30, "advancedOptions"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 916
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_f
    const/16 v29, 0x0

    const-string v30, "job"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 919
    const-string v29, "PrintSpoolerService"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "[PERSISTED] "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 923
    .end local v4    # "advancedOptions":Landroid/os/Bundle;
    .end local v5    # "attributes":Landroid/print/PrintAttributes;
    .end local v7    # "documentInfo":Landroid/print/PrintDocumentInfo;
    .end local v17    # "pages":[Landroid/print/PageRange;
    .end local v18    # "printJob":Landroid/print/PrintJobInfo;
    .end local v21    # "printerId":Landroid/print/PrinterId;
    .end local v22    # "printerName":Ljava/lang/String;
    .end local v25    # "stateReason":Ljava/lang/String;
    .end local v27    # "tag":Ljava/lang/String;
    :cond_10
    const/16 v29, 0x0

    const-string v30, "spooler"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 924
    invoke-interface/range {v24 .. v24}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 925
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    .line 927
    const-string v29, "PrintSpoolerService"

    const-string v30, "[PERSIST END]"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 933
    invoke-static/range {v16 .. v16}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto/16 :goto_5
.end method

.method private expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V
    .locals 3
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "type"    # I
    .param p3, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 1185
    invoke-direct {p0, p1, p2, p3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1186
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exepected event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but got event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and tag:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1190
    :cond_0
    return-void
.end method

.method private parsePrintJob(Lorg/xmlpull/v1/XmlPullParser;)Z
    .locals 51
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 986
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 987
    const/16 v48, 0x2

    const-string v49, "job"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-nez v48, :cond_0

    .line 988
    const/16 v48, 0x0

    .line 1180
    :goto_0
    return v48

    .line 991
    :cond_0
    new-instance v34, Landroid/print/PrintJobInfo;

    invoke-direct/range {v34 .. v34}, Landroid/print/PrintJobInfo;-><init>()V

    .line 993
    .local v34, "printJob":Landroid/print/PrintJobInfo;
    const/16 v48, 0x0

    const-string v49, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Landroid/print/PrintJobId;->unflattenFromString(Ljava/lang/String;)Landroid/print/PrintJobId;

    move-result-object v35

    .line 995
    .local v35, "printJobId":Landroid/print/PrintJobId;
    invoke-virtual/range {v34 .. v35}, Landroid/print/PrintJobInfo;->setId(Landroid/print/PrintJobId;)V

    .line 996
    const/16 v48, 0x0

    const-string v49, "label"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 997
    .local v6, "label":Ljava/lang/String;
    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Landroid/print/PrintJobInfo;->setLabel(Ljava/lang/String;)V

    .line 998
    const/16 v48, 0x0

    const-string v49, "state"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v41

    .line 999
    .local v41, "state":I
    move-object/from16 v0, v34

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setState(I)V

    .line 1000
    const/16 v48, 0x0

    const-string v49, "appId"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 1001
    .local v12, "appId":I
    move-object/from16 v0, v34

    invoke-virtual {v0, v12}, Landroid/print/PrintJobInfo;->setAppId(I)V

    .line 1002
    const/16 v48, 0x0

    const-string v49, "tag"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 1003
    .local v43, "tag":Ljava/lang/String;
    move-object/from16 v0, v34

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setTag(Ljava/lang/String;)V

    .line 1004
    const/16 v48, 0x0

    const-string v49, "creationTime"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1005
    .local v19, "creationTime":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v48

    move-object/from16 v0, v34

    move-wide/from16 v1, v48

    invoke-virtual {v0, v1, v2}, Landroid/print/PrintJobInfo;->setCreationTime(J)V

    .line 1006
    const/16 v48, 0x0

    const-string v49, "copies"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1007
    .local v18, "copies":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v48

    move-object/from16 v0, v34

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setCopies(I)V

    .line 1008
    const/16 v48, 0x0

    const-string v49, "printerName"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 1009
    .local v36, "printerName":Ljava/lang/String;
    move-object/from16 v0, v34

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setPrinterName(Ljava/lang/String;)V

    .line 1010
    const/16 v48, 0x0

    const-string v49, "stateReason"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    .line 1011
    .local v42, "stateReason":Ljava/lang/String;
    move-object/from16 v0, v34

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setStateReason(Ljava/lang/String;)V

    .line 1012
    const/16 v48, 0x0

    const-string v49, "cancelling"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1013
    .local v15, "cancelling":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v48

    if-nez v48, :cond_3

    invoke-static {v15}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v48

    :goto_1
    move-object/from16 v0, v34

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setCancelling(Z)V

    .line 1016
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1018
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1019
    const/16 v48, 0x2

    const-string v49, "printerId"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_1

    .line 1020
    const/16 v48, 0x0

    const-string v49, "localId"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 1021
    .local v27, "localId":Ljava/lang/String;
    const/16 v48, 0x0

    const-string v49, "serviceName"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v39

    .line 1023
    .local v39, "service":Landroid/content/ComponentName;
    new-instance v48, Landroid/print/PrinterId;

    move-object/from16 v0, v48

    move-object/from16 v1, v39

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Landroid/print/PrinterId;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    move-object/from16 v0, v34

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setPrinterId(Landroid/print/PrinterId;)V

    .line 1024
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1025
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1026
    const/16 v48, 0x3

    const-string v49, "printerId"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1027
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1030
    .end local v27    # "localId":Ljava/lang/String;
    .end local v39    # "service":Landroid/content/ComponentName;
    :cond_1
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1031
    const/16 v32, 0x0

    .line 1032
    .local v32, "pageRanges":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    :goto_2
    const/16 v48, 0x2

    const-string v49, "pageRange"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 1033
    const/16 v48, 0x0

    const-string v49, "start"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v40

    .line 1034
    .local v40, "start":I
    const/16 v48, 0x0

    const-string v49, "end"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 1035
    .local v21, "end":I
    new-instance v31, Landroid/print/PageRange;

    move-object/from16 v0, v31

    move/from16 v1, v40

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/print/PageRange;-><init>(II)V

    .line 1036
    .local v31, "pageRange":Landroid/print/PageRange;
    if-nez v32, :cond_2

    .line 1037
    new-instance v32, Ljava/util/ArrayList;

    .end local v32    # "pageRanges":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    invoke-direct/range {v32 .. v32}, Ljava/util/ArrayList;-><init>()V

    .line 1039
    .restart local v32    # "pageRanges":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    :cond_2
    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1040
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1041
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1042
    const/16 v48, 0x3

    const-string v49, "pageRange"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1043
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1044
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_2

    .line 1013
    .end local v21    # "end":I
    .end local v31    # "pageRange":Landroid/print/PageRange;
    .end local v32    # "pageRanges":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    .end local v40    # "start":I
    :cond_3
    const/16 v48, 0x0

    goto/16 :goto_1

    .line 1046
    .restart local v32    # "pageRanges":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    :cond_4
    if-eqz v32, :cond_5

    .line 1047
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->size()I

    move-result v48

    move/from16 v0, v48

    new-array v0, v0, [Landroid/print/PageRange;

    move-object/from16 v33, v0

    .line 1048
    .local v33, "pageRangesArray":[Landroid/print/PageRange;
    invoke-interface/range {v32 .. v33}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1049
    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setPages([Landroid/print/PageRange;)V

    .line 1052
    .end local v33    # "pageRangesArray":[Landroid/print/PageRange;
    :cond_5
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1053
    const/16 v48, 0x2

    const-string v49, "attributes"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_9

    .line 1055
    new-instance v14, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v14}, Landroid/print/PrintAttributes$Builder;-><init>()V

    .line 1057
    .local v14, "builder":Landroid/print/PrintAttributes$Builder;
    const/16 v48, 0x0

    const-string v49, "colorMode"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1058
    .local v16, "colorMode":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v48

    move/from16 v0, v48

    invoke-virtual {v14, v0}, Landroid/print/PrintAttributes$Builder;->setColorMode(I)Landroid/print/PrintAttributes$Builder;

    .line 1060
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1062
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1063
    const/16 v48, 0x2

    const-string v49, "mediaSize"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_6

    .line 1064
    const/16 v48, 0x0

    const-string v49, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1065
    .local v5, "id":Ljava/lang/String;
    const/16 v48, 0x0

    const-string v49, "label"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1066
    const/16 v48, 0x0

    const-string v49, "widthMils"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 1068
    .local v8, "widthMils":I
    const/16 v48, 0x0

    const-string v49, "heightMils"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 1070
    .local v9, "heightMils":I
    const/16 v48, 0x0

    const-string v49, "packageName"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1071
    .local v7, "packageName":Ljava/lang/String;
    const/16 v48, 0x0

    const-string v49, "labelResId"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 1072
    .local v25, "labelResIdString":Ljava/lang/String;
    if-eqz v25, :cond_c

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 1074
    .local v10, "labelResId":I
    :goto_3
    const/16 v48, 0x0

    const-string v49, "label"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1075
    new-instance v4, Landroid/print/PrintAttributes$MediaSize;

    invoke-direct/range {v4 .. v10}, Landroid/print/PrintAttributes$MediaSize;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 1077
    .local v4, "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    invoke-virtual {v14, v4}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    .line 1078
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1079
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1080
    const/16 v48, 0x3

    const-string v49, "mediaSize"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1081
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1084
    .end local v4    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v5    # "id":Ljava/lang/String;
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v8    # "widthMils":I
    .end local v9    # "heightMils":I
    .end local v10    # "labelResId":I
    .end local v25    # "labelResIdString":Ljava/lang/String;
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1085
    const/16 v48, 0x2

    const-string v49, "resolution"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_7

    .line 1086
    const/16 v48, 0x0

    const-string v49, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1087
    .restart local v5    # "id":Ljava/lang/String;
    const/16 v48, 0x0

    const-string v49, "label"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1088
    const/16 v48, 0x0

    const-string v49, "horizontalDip"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    .line 1090
    .local v22, "horizontalDpi":I
    const/16 v48, 0x0

    const-string v49, "verticalDpi"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v47

    .line 1092
    .local v47, "verticalDpi":I
    new-instance v37, Landroid/print/PrintAttributes$Resolution;

    move-object/from16 v0, v37

    move/from16 v1, v22

    move/from16 v2, v47

    invoke-direct {v0, v5, v6, v1, v2}, Landroid/print/PrintAttributes$Resolution;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 1093
    .local v37, "resolution":Landroid/print/PrintAttributes$Resolution;
    move-object/from16 v0, v37

    invoke-virtual {v14, v0}, Landroid/print/PrintAttributes$Builder;->setResolution(Landroid/print/PrintAttributes$Resolution;)Landroid/print/PrintAttributes$Builder;

    .line 1094
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1095
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1096
    const/16 v48, 0x3

    const-string v49, "resolution"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1097
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1100
    .end local v5    # "id":Ljava/lang/String;
    .end local v22    # "horizontalDpi":I
    .end local v37    # "resolution":Landroid/print/PrintAttributes$Resolution;
    .end local v47    # "verticalDpi":I
    :cond_7
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1101
    const/16 v48, 0x2

    const-string v49, "margins"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_8

    .line 1102
    const/16 v48, 0x0

    const-string v49, "leftMils"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    .line 1104
    .local v26, "leftMils":I
    const/16 v48, 0x0

    const-string v49, "topMils"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    .line 1106
    .local v44, "topMils":I
    const/16 v48, 0x0

    const-string v49, "rightMils"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v38

    .line 1108
    .local v38, "rightMils":I
    const/16 v48, 0x0

    const-string v49, "bottomMils"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 1110
    .local v13, "bottomMils":I
    new-instance v28, Landroid/print/PrintAttributes$Margins;

    move-object/from16 v0, v28

    move/from16 v1, v26

    move/from16 v2, v44

    move/from16 v3, v38

    invoke-direct {v0, v1, v2, v3, v13}, Landroid/print/PrintAttributes$Margins;-><init>(IIII)V

    .line 1111
    .local v28, "margins":Landroid/print/PrintAttributes$Margins;
    move-object/from16 v0, v28

    invoke-virtual {v14, v0}, Landroid/print/PrintAttributes$Builder;->setMinMargins(Landroid/print/PrintAttributes$Margins;)Landroid/print/PrintAttributes$Builder;

    .line 1112
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1113
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1114
    const/16 v48, 0x3

    const-string v49, "margins"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1115
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1118
    .end local v13    # "bottomMils":I
    .end local v26    # "leftMils":I
    .end local v28    # "margins":Landroid/print/PrintAttributes$Margins;
    .end local v38    # "rightMils":I
    .end local v44    # "topMils":I
    :cond_8
    invoke-virtual {v14}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v48

    move-object/from16 v0, v34

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setAttributes(Landroid/print/PrintAttributes;)V

    .line 1120
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1121
    const/16 v48, 0x3

    const-string v49, "attributes"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1122
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1125
    .end local v14    # "builder":Landroid/print/PrintAttributes$Builder;
    .end local v16    # "colorMode":Ljava/lang/String;
    :cond_9
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1126
    const/16 v48, 0x2

    const-string v49, "documentInfo"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_a

    .line 1127
    const/16 v48, 0x0

    const-string v49, "name"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 1128
    .local v29, "name":Ljava/lang/String;
    const/16 v48, 0x0

    const-string v49, "pageCount"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v30

    .line 1130
    .local v30, "pageCount":I
    const/16 v48, 0x0

    const-string v49, "contentType"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 1132
    .local v17, "contentType":I
    const/16 v48, 0x0

    const-string v49, "dataSize"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    .line 1134
    .local v20, "dataSize":I
    new-instance v48, Landroid/print/PrintDocumentInfo$Builder;

    move-object/from16 v0, v48

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v48

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v48

    move-object/from16 v0, v48

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v23

    .line 1137
    .local v23, "info":Landroid/print/PrintDocumentInfo;
    move-object/from16 v0, v34

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/print/PrintJobInfo;->setDocumentInfo(Landroid/print/PrintDocumentInfo;)V

    .line 1138
    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v48, v0

    move-object/from16 v0, v23

    move-wide/from16 v1, v48

    invoke-virtual {v0, v1, v2}, Landroid/print/PrintDocumentInfo;->setDataSize(J)V

    .line 1139
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1140
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1141
    const/16 v48, 0x3

    const-string v49, "documentInfo"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1142
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1145
    .end local v17    # "contentType":I
    .end local v20    # "dataSize":I
    .end local v23    # "info":Landroid/print/PrintDocumentInfo;
    .end local v29    # "name":Ljava/lang/String;
    .end local v30    # "pageCount":I
    :cond_a
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1146
    const/16 v48, 0x2

    const-string v49, "advancedOptions"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_f

    .line 1147
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1148
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1149
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 1150
    .local v11, "advancedOptions":Landroid/os/Bundle;
    :goto_4
    const/16 v48, 0x2

    const-string v49, "advancedOption"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_e

    .line 1151
    const/16 v48, 0x0

    const-string v49, "key"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1152
    .local v24, "key":Ljava/lang/String;
    const/16 v48, 0x0

    const-string v49, "value"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    .line 1153
    .local v46, "value":Ljava/lang/String;
    const/16 v48, 0x0

    const-string v49, "type"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    .line 1154
    .local v45, "type":Ljava/lang/String;
    const-string v48, "string"

    move-object/from16 v0, v48

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_d

    .line 1155
    move-object/from16 v0, v24

    move-object/from16 v1, v46

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    :cond_b
    :goto_5
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1160
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1161
    const/16 v48, 0x3

    const-string v49, "advancedOption"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1162
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1163
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_4

    .line 1072
    .end local v11    # "advancedOptions":Landroid/os/Bundle;
    .end local v24    # "key":Ljava/lang/String;
    .end local v45    # "type":Ljava/lang/String;
    .end local v46    # "value":Ljava/lang/String;
    .restart local v5    # "id":Ljava/lang/String;
    .restart local v7    # "packageName":Ljava/lang/String;
    .restart local v8    # "widthMils":I
    .restart local v9    # "heightMils":I
    .restart local v14    # "builder":Landroid/print/PrintAttributes$Builder;
    .restart local v16    # "colorMode":Ljava/lang/String;
    .restart local v25    # "labelResIdString":Ljava/lang/String;
    :cond_c
    const/4 v10, 0x0

    goto/16 :goto_3

    .line 1156
    .end local v5    # "id":Ljava/lang/String;
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v8    # "widthMils":I
    .end local v9    # "heightMils":I
    .end local v14    # "builder":Landroid/print/PrintAttributes$Builder;
    .end local v16    # "colorMode":Ljava/lang/String;
    .end local v25    # "labelResIdString":Ljava/lang/String;
    .restart local v11    # "advancedOptions":Landroid/os/Bundle;
    .restart local v24    # "key":Ljava/lang/String;
    .restart local v45    # "type":Ljava/lang/String;
    .restart local v46    # "value":Ljava/lang/String;
    :cond_d
    const-string v48, "int"

    move-object/from16 v0, v48

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_b

    .line 1157
    invoke-static/range {v46 .. v46}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Integer;->intValue()I

    move-result v48

    move-object/from16 v0, v24

    move/from16 v1, v48

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_5

    .line 1165
    .end local v24    # "key":Ljava/lang/String;
    .end local v45    # "type":Ljava/lang/String;
    .end local v46    # "value":Ljava/lang/String;
    :cond_e
    move-object/from16 v0, v34

    invoke-virtual {v0, v11}, Landroid/print/PrintJobInfo;->setAdvancedOptions(Landroid/os/Bundle;)V

    .line 1166
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1167
    const/16 v48, 0x3

    const-string v49, "advancedOptions"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1168
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 1171
    .end local v11    # "advancedOptions":Landroid/os/Bundle;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mPrintJobs:Ljava/util/List;
    invoke-static/range {v48 .. v48}, Lcom/android/printspooler/model/PrintSpoolerService;->access$800(Lcom/android/printspooler/model/PrintSpoolerService;)Ljava/util/List;

    move-result-object v48

    move-object/from16 v0, v48

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1174
    const-string v48, "PrintSpoolerService"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "[RESTORED] "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    invoke-direct/range {p0 .. p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1178
    const/16 v48, 0x3

    const-string v49, "job"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v48

    move-object/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 1180
    const/16 v48, 0x1

    goto/16 :goto_0
.end method

.method private parseState(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 971
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 972
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 973
    const/4 v0, 0x2

    const-string v1, "spooler"

    invoke-direct {p0, p1, v0, v1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 974
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 976
    :goto_0
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->parsePrintJob(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 977
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    goto :goto_0

    .line 980
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 981
    const/4 v0, 0x3

    const-string v1, "spooler"

    invoke-direct {p0, p1, v0, v1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 982
    return-void
.end method

.method private skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 1195
    :goto_0
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\n"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1196
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    goto :goto_0

    .line 1198
    :cond_0
    return-void
.end method


# virtual methods
.method public readStateLocked()V
    .locals 11

    .prologue
    .line 941
    const/4 v1, 0x0

    .line 943
    .local v1, "in":Ljava/io/FileInputStream;
    :try_start_0
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;

    invoke-virtual {v9}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 949
    :try_start_1
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    .line 950
    .local v7, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v9, 0x0

    invoke-interface {v7, v1, v9}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 951
    invoke-direct {p0, v7}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->parseState(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 965
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 967
    .end local v7    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :goto_0
    return-void

    .line 944
    :catch_0
    move-exception v0

    .line 945
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v9, "PrintSpoolerService"

    const-string v10, "No existing print spooler state."

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 952
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v4

    .line 953
    .local v4, "ise":Ljava/lang/IllegalStateException;
    :try_start_2
    const-string v9, "PrintSpoolerService"

    const-string v10, "Failed parsing "

    invoke-static {v9, v10, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 965
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .line 954
    .end local v4    # "ise":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v6

    .line 955
    .local v6, "npe":Ljava/lang/NullPointerException;
    :try_start_3
    const-string v9, "PrintSpoolerService"

    const-string v10, "Failed parsing "

    invoke-static {v9, v10, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 965
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .line 956
    .end local v6    # "npe":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v5

    .line 957
    .local v5, "nfe":Ljava/lang/NumberFormatException;
    :try_start_4
    const-string v9, "PrintSpoolerService"

    const-string v10, "Failed parsing "

    invoke-static {v9, v10, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 965
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .line 958
    .end local v5    # "nfe":Ljava/lang/NumberFormatException;
    :catch_4
    move-exception v8

    .line 959
    .local v8, "xppe":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_5
    const-string v9, "PrintSpoolerService"

    const-string v10, "Failed parsing "

    invoke-static {v9, v10, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 965
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .line 960
    .end local v8    # "xppe":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_5
    move-exception v3

    .line 961
    .local v3, "ioe":Ljava/io/IOException;
    :try_start_6
    const-string v9, "PrintSpoolerService"

    const-string v10, "Failed parsing "

    invoke-static {v9, v10, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 965
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .line 962
    .end local v3    # "ioe":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 963
    .local v2, "iobe":Ljava/lang/IndexOutOfBoundsException;
    :try_start_7
    const-string v9, "PrintSpoolerService"

    const-string v10, "Failed parsing "

    invoke-static {v9, v10, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 965
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .end local v2    # "iobe":Ljava/lang/IndexOutOfBoundsException;
    :catchall_0
    move-exception v9

    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v9
.end method

.method public writeStateLocked()V
    .locals 3

    .prologue
    .line 734
    iget-boolean v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mWriteStateScheduled:Z

    if-eqz v0, :cond_0

    .line 748
    :goto_0
    return-void

    .line 737
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mWriteStateScheduled:Z

    .line 738
    new-instance v1, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;

    invoke-direct {v1, p0}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;-><init>(Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;)V

    sget-object v2, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
