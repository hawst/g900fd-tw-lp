.class final Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;
.super Landroid/os/Handler;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WriteHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;Landroid/os/Looper;)V
    .locals 2
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    .line 1060
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 1061
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 1065
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 1089
    :goto_0
    return-void

    .line 1067
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/ICancellationSignal;

    .line 1068
    .local v0, "cancellation":Landroid/os/ICancellationSignal;
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 1069
    .local v3, "sequence":I
    iget-object v4, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteStarted(Landroid/os/ICancellationSignal;I)V
    invoke-static {v4, v0, v3}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$2100(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;Landroid/os/ICancellationSignal;I)V

    goto :goto_0

    .line 1073
    .end local v0    # "cancellation":Landroid/os/ICancellationSignal;
    .end local v3    # "sequence":I
    :pswitch_1
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [Landroid/print/PageRange;

    move-object v2, v4

    check-cast v2, [Landroid/print/PageRange;

    .line 1074
    .local v2, "pages":[Landroid/print/PageRange;
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 1075
    .restart local v3    # "sequence":I
    iget-object v4, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteFinished([Landroid/print/PageRange;I)V
    invoke-static {v4, v2, v3}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$2200(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;[Landroid/print/PageRange;I)V

    goto :goto_0

    .line 1079
    .end local v2    # "pages":[Landroid/print/PageRange;
    .end local v3    # "sequence":I
    :pswitch_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    .line 1080
    .local v1, "error":Ljava/lang/CharSequence;
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 1081
    .restart local v3    # "sequence":I
    iget-object v4, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteFailed(Ljava/lang/CharSequence;I)V
    invoke-static {v4, v1, v3}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$2300(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 1085
    .end local v1    # "error":Ljava/lang/CharSequence;
    .end local v3    # "sequence":I
    :pswitch_3
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 1086
    .restart local v3    # "sequence":I
    iget-object v4, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteCanceled(I)V
    invoke-static {v4, v3}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$2400(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;I)V

    goto :goto_0

    .line 1065
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
