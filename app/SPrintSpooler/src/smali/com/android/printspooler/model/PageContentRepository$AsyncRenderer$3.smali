.class Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;
.super Landroid/os/AsyncTask;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->destroy(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

.field final synthetic val$callback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 542
    iput-object p1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    iput-object p2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 542
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 545
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 542
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 550
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mBoundToService:Z
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1200(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 551
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    const/4 v2, 0x0

    # setter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mBoundToService:Z
    invoke-static {v1, v2}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1202(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;Z)Z

    .line 553
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$700(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->invalidate()V

    .line 559
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->clear()V

    .line 560
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->val$callback:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 561
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$3;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 564
    :cond_1
    return-void

    .line 554
    :catch_0
    move-exception v0

    .line 555
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "PageContentRepository"

    const-string v2, "AsyncRenderer service is already disconnected"

    invoke-static {v1, v2}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
