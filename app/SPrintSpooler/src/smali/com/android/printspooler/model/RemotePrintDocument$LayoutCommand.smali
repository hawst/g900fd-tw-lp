.class final Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;
.super Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LayoutCommand"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;,
        Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mMetadata:Landroid/os/Bundle;

.field private final mNewAttributes:Landroid/print/PrintAttributes;

.field private final mOldAttributes:Landroid/print/PrintAttributes;

.field private final mRemoteResultCallback:Landroid/print/ILayoutResultCallback;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;ZLcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;)V
    .locals 2
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "adapter"    # Landroid/print/IPrintDocumentAdapter;
    .param p3, "document"    # Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    .param p4, "oldAttributes"    # Landroid/print/PrintAttributes;
    .param p5, "newAttributes"    # Landroid/print/PrintAttributes;
    .param p6, "preview"    # Z
    .param p7, "callback"    # Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    .prologue
    .line 651
    invoke-direct {p0, p2, p3, p7}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;-><init>(Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;)V

    .line 640
    new-instance v0, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v0}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mOldAttributes:Landroid/print/PrintAttributes;

    .line 641
    new-instance v0, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v0}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mNewAttributes:Landroid/print/PrintAttributes;

    .line 642
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mMetadata:Landroid/os/Bundle;

    .line 652
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;

    invoke-direct {v0, p0, p1}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutHandler;-><init>(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mHandler:Landroid/os/Handler;

    .line 653
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mRemoteResultCallback:Landroid/print/ILayoutResultCallback;

    .line 654
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mOldAttributes:Landroid/print/PrintAttributes;

    invoke-virtual {v0, p4}, Landroid/print/PrintAttributes;->copyFrom(Landroid/print/PrintAttributes;)V

    .line 655
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mNewAttributes:Landroid/print/PrintAttributes;

    invoke-virtual {v0, p5}, Landroid/print/PrintAttributes;->copyFrom(Landroid/print/PrintAttributes;)V

    .line 656
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mMetadata:Landroid/os/Bundle;

    const-string v1, "EXTRA_PRINT_PREVIEW"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 657
    return-void
.end method

.method static synthetic access$1400(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;Landroid/os/ICancellationSignal;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;
    .param p1, "x1"    # Landroid/os/ICancellationSignal;
    .param p2, "x2"    # I

    .prologue
    .line 639
    invoke-direct {p0, p1, p2}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutStarted(Landroid/os/ICancellationSignal;I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;Landroid/print/PrintDocumentInfo;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;
    .param p1, "x1"    # Landroid/print/PrintDocumentInfo;
    .param p2, "x2"    # Z
    .param p3, "x3"    # I

    .prologue
    .line 639
    invoke-direct {p0, p1, p2, p3}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutFinished(Landroid/print/PrintDocumentInfo;ZI)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;Ljava/lang/CharSequence;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;
    .param p1, "x1"    # Ljava/lang/CharSequence;
    .param p2, "x2"    # I

    .prologue
    .line 639
    invoke-direct {p0, p1, p2}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutFailed(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;
    .param p1, "x1"    # I

    .prologue
    .line 639
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutCanceled(I)V

    return-void
.end method

.method private equalsIgnoreSize(Landroid/print/PrintDocumentInfo;Landroid/print/PrintDocumentInfo;)Z
    .locals 4
    .param p1, "lhs"    # Landroid/print/PrintDocumentInfo;
    .param p2, "rhs"    # Landroid/print/PrintDocumentInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 775
    if-ne p1, p2, :cond_1

    .line 789
    :cond_0
    :goto_0
    return v0

    .line 778
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 779
    goto :goto_0

    .line 781
    :cond_2
    if-nez p2, :cond_3

    move v0, v1

    .line 782
    goto :goto_0

    .line 784
    :cond_3
    invoke-virtual {p1}, Landroid/print/PrintDocumentInfo;->getContentType()I

    move-result v2

    invoke-virtual {p2}, Landroid/print/PrintDocumentInfo;->getContentType()I

    move-result v3

    if-ne v2, v3, :cond_4

    invoke-virtual {p1}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v2

    invoke-virtual {p2}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    .line 786
    goto :goto_0
.end method

.method private handleOnLayoutCanceled(I)V
    .locals 2
    .param p1, "sequence"    # I

    .prologue
    .line 757
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mSequence:I

    if-eq p1, v0, :cond_0

    .line 772
    :goto_0
    return-void

    .line 762
    :cond_0
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLBACK] onLayoutCanceled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->canceled()V

    .line 768
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mCancellation:Landroid/os/ICancellationSignal;

    .line 771
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    invoke-interface {v0}, Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;->onDone()V

    goto :goto_0
.end method

.method private handleOnLayoutFailed(Ljava/lang/CharSequence;I)V
    .locals 2
    .param p1, "error"    # Ljava/lang/CharSequence;
    .param p2, "sequence"    # I

    .prologue
    .line 737
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mSequence:I

    if-eq p2, v0, :cond_0

    .line 754
    :goto_0
    return-void

    .line 742
    :cond_0
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLBACK] onLayoutFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->laidout:Z

    .line 747
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->failed(Ljava/lang/CharSequence;)V

    .line 750
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mCancellation:Landroid/os/ICancellationSignal;

    .line 753
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    invoke-interface {v0}, Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;->onDone()V

    goto :goto_0
.end method

.method private handleOnLayoutFinished(Landroid/print/PrintDocumentInfo;ZI)V
    .locals 4
    .param p1, "info"    # Landroid/print/PrintDocumentInfo;
    .param p2, "changed"    # Z
    .param p3, "sequence"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 703
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mSequence:I

    if-eq p3, v0, :cond_0

    .line 734
    :goto_0
    return-void

    .line 708
    :cond_0
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLBACK] onLayoutFinished"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->completed()V

    .line 715
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-direct {p0, v0, p1}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->equalsIgnoreSize(Landroid/print/PrintDocumentInfo;Landroid/print/PrintDocumentInfo;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 718
    :cond_1
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-object v2, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->writtenPages:[Landroid/print/PageRange;

    .line 719
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-object v2, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->printedPages:[Landroid/print/PageRange;

    .line 720
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-boolean v3, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->changed:Z

    .line 724
    :cond_2
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mNewAttributes:Landroid/print/PrintAttributes;

    iput-object v1, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->attributes:Landroid/print/PrintAttributes;

    .line 725
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mMetadata:Landroid/os/Bundle;

    iput-object v1, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->metadata:Landroid/os/Bundle;

    .line 726
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-boolean v3, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->laidout:Z

    .line 727
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-object p1, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    .line 730
    iput-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mCancellation:Landroid/os/ICancellationSignal;

    .line 733
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    invoke-interface {v0}, Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;->onDone()V

    goto :goto_0
.end method

.method private handleOnLayoutStarted(Landroid/os/ICancellationSignal;I)V
    .locals 3
    .param p1, "cancellation"    # Landroid/os/ICancellationSignal;
    .param p2, "sequence"    # I

    .prologue
    .line 681
    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mSequence:I

    if-eq p2, v1, :cond_0

    .line 699
    :goto_0
    return-void

    .line 686
    :cond_0
    const-string v1, "RemotePrintDocument"

    const-string v2, "[CALLBACK] onLayoutStarted"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->isCanceling()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 691
    :try_start_0
    invoke-interface {p1}, Landroid/os/ICancellationSignal;->cancel()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 692
    :catch_0
    move-exception v0

    .line 693
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "RemotePrintDocument"

    const-string v2, "Error cancelling"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 694
    const/4 v1, 0x0

    iget v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mSequence:I

    invoke-direct {p0, v1, v2}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutFailed(Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 697
    .end local v0    # "re":Landroid/os/RemoteException;
    :cond_1
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mCancellation:Landroid/os/ICancellationSignal;

    goto :goto_0
.end method


# virtual methods
.method public getCommandType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 660
    const-string v0, "LayoutCommand"

    return-object v0
.end method

.method public run()V
    .locals 7

    .prologue
    .line 665
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->running()V

    .line 669
    :try_start_0
    const-string v0, "RemotePrintDocument"

    const-string v1, "[PERFORMING] layout"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->changed:Z

    .line 672
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mAdapter:Landroid/print/IPrintDocumentAdapter;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mOldAttributes:Landroid/print/PrintAttributes;

    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mNewAttributes:Landroid/print/PrintAttributes;

    iget-object v3, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mRemoteResultCallback:Landroid/print/ILayoutResultCallback;

    iget-object v4, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mMetadata:Landroid/os/Bundle;

    iget v5, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mSequence:I

    invoke-interface/range {v0 .. v5}, Landroid/print/IPrintDocumentAdapter;->layout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/print/ILayoutResultCallback;Landroid/os/Bundle;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    :goto_0
    return-void

    .line 674
    :catch_0
    move-exception v6

    .line 675
    .local v6, "re":Landroid/os/RemoteException;
    const-string v0, "RemotePrintDocument"

    const-string v1, "Error calling layout"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 676
    const/4 v0, 0x0

    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->mSequence:I

    invoke-direct {p0, v0, v1}, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;->handleOnLayoutFailed(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method
