.class public final Lcom/android/printspooler/model/MutexFileProvider;
.super Ljava/lang/Object;
.source "MutexFileProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;
    }
.end annotation


# instance fields
.field private final mFile:Ljava/io/File;

.field private final mLock:Ljava/lang/Object;

.field private mOnReleaseRequestCallback:Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;

.field private mOwnerThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mLock:Ljava/lang/Object;

    .line 53
    iput-object p1, p0, Lcom/android/printspooler/model/MutexFileProvider;->mFile:Ljava/io/File;

    .line 54
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 57
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 58
    return-void
.end method


# virtual methods
.method public acquireFile(Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;)Ljava/io/File;
    .locals 4
    .param p1, "callback"    # Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;

    .prologue
    .line 61
    iget-object v1, p0, Lcom/android/printspooler/model/MutexFileProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOwnerThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 64
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mFile:Ljava/io/File;

    monitor-exit v1

    .line 89
    :goto_0
    return-object v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOwnerThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOnReleaseRequestCallback:Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOnReleaseRequestCallback:Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;

    iget-object v2, p0, Lcom/android/printspooler/model/MutexFileProvider;->mFile:Ljava/io/File;

    invoke-interface {v0, v2}, Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;->onReleaseRequested(Ljava/io/File;)V

    .line 73
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOwnerThread:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 75
    :try_start_1
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 76
    :catch_0
    move-exception v0

    goto :goto_1

    .line 82
    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOwnerThread:Ljava/lang/Thread;

    .line 83
    iput-object p1, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOnReleaseRequestCallback:Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;

    .line 86
    const-string v0, "MutexFileProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Acquired file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/model/MutexFileProvider;->mFile:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " by thread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOwnerThread:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mFile:Ljava/io/File;

    monitor-exit v1

    goto :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public releaseFile()V
    .locals 4

    .prologue
    .line 94
    iget-object v1, p0, Lcom/android/printspooler/model/MutexFileProvider;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOwnerThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v0, v2, :cond_0

    .line 96
    monitor-exit v1

    .line 109
    :goto_0
    return-void

    .line 100
    :cond_0
    const-string v0, "MutexFileProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Released file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/model/MutexFileProvider;->mFile:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from thread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOwnerThread:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOwnerThread:Ljava/lang/Thread;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mOnReleaseRequestCallback:Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;

    .line 107
    iget-object v0, p0, Lcom/android/printspooler/model/MutexFileProvider;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 108
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
