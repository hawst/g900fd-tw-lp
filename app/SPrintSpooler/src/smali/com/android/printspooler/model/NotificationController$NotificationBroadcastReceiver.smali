.class public final Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NotificationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/NotificationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NotificationBroadcastReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private handleCancelPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "printJobId"    # Landroid/print/PrintJobId;

    .prologue
    .line 337
    const-string v2, "NotificationBroadcastReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleCancelPrintJob() printJobId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 345
    .local v0, "powerManager":Landroid/os/PowerManager;
    const/4 v2, 0x1

    const-string v3, "NotificationBroadcastReceiver"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    .line 347
    .local v1, "wakeLock":Landroid/os/PowerManager$WakeLock;
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 349
    new-instance v3, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;

    invoke-direct {v3, p0, p2, v1}, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;-><init>(Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;Landroid/print/PrintJobId;Landroid/os/PowerManager$WakeLock;)V

    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Void;

    invoke-virtual {v3, v4, v2}, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 372
    return-void
.end method

.method private handleRestartPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "printJobId"    # Landroid/print/PrintJobId;

    .prologue
    .line 376
    const-string v2, "NotificationBroadcastReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleRestartPrintJob() printJobId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 384
    .local v0, "powerManager":Landroid/os/PowerManager;
    const/4 v2, 0x1

    const-string v3, "NotificationBroadcastReceiver"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    .line 386
    .local v1, "wakeLock":Landroid/os/PowerManager$WakeLock;
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 388
    new-instance v3, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$2;

    invoke-direct {v3, p0, p2, v1}, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$2;-><init>(Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;Landroid/print/PrintJobId;Landroid/os/PowerManager$WakeLock;)V

    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Void;

    invoke-virtual {v3, v4, v2}, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 409
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 325
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v2, "INTENT_ACTION_CANCEL_PRINTJOB"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 327
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "EXTRA_PRINT_JOB_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/print/PrintJobId;

    .line 328
    .local v1, "printJobId":Landroid/print/PrintJobId;
    invoke-direct {p0, p1, v1}, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;->handleCancelPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)V

    .line 333
    .end local v1    # "printJobId":Landroid/print/PrintJobId;
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    if-eqz v0, :cond_0

    const-string v2, "INTENT_ACTION_RESTART_PRINTJOB"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "EXTRA_PRINT_JOB_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/print/PrintJobId;

    .line 331
    .restart local v1    # "printJobId":Landroid/print/PrintJobId;
    invoke-direct {p0, p1, v1}, Lcom/android/printspooler/model/NotificationController$NotificationBroadcastReceiver;->handleRestartPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)V

    goto :goto_0
.end method
