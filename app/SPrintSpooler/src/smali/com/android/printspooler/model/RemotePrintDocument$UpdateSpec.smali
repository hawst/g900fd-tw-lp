.class final Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;
.super Ljava/lang/Object;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "UpdateSpec"
.end annotation


# instance fields
.field final attributes:Landroid/print/PrintAttributes;

.field pages:[Landroid/print/PageRange;

.field preview:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 477
    new-instance v0, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v0}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->attributes:Landroid/print/PrintAttributes;

    return-void
.end method


# virtual methods
.method public hasSameConstraints(Landroid/print/PrintAttributes;Z)Z
    .locals 1
    .param p1, "attributes"    # Landroid/print/PrintAttributes;
    .param p2, "preview"    # Z

    .prologue
    .line 495
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->attributes:Landroid/print/PrintAttributes;

    invoke-virtual {v0, p1}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->preview:Z

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->attributes:Landroid/print/PrintAttributes;

    invoke-virtual {v0}, Landroid/print/PrintAttributes;->clear()V

    .line 490
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->preview:Z

    .line 491
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->pages:[Landroid/print/PageRange;

    .line 492
    return-void
.end method

.method public update(Landroid/print/PrintAttributes;Z[Landroid/print/PageRange;)V
    .locals 1
    .param p1, "attributes"    # Landroid/print/PrintAttributes;
    .param p2, "preview"    # Z
    .param p3, "pages"    # [Landroid/print/PageRange;

    .prologue
    .line 483
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->attributes:Landroid/print/PrintAttributes;

    invoke-virtual {v0, p1}, Landroid/print/PrintAttributes;->copyFrom(Landroid/print/PrintAttributes;)V

    .line 484
    iput-boolean p2, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->preview:Z

    .line 485
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/print/PageRange;

    :goto_0
    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->pages:[Landroid/print/PageRange;

    .line 486
    return-void

    .line 485
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
