.class Lcom/android/printspooler/model/RemotePrintDocument$1;
.super Ljava/lang/Object;
.source "RemotePrintDocument.java"

# interfaces
.implements Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/RemotePrintDocument;


# direct methods
.method constructor <init>(Lcom/android/printspooler/model/RemotePrintDocument;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x3

    .line 86
    const-string v0, "RemotePrintDocument"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CommandDoneCallback] onDone mCurrentCommand: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v2}, Lcom/android/printspooler/model/RemotePrintDocument;->access$000(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mNextCommand: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v2}, Lcom/android/printspooler/model/RemotePrintDocument;->access$100(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$000(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 88
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$000(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    move-result-object v0

    instance-of v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;

    if-eqz v0, :cond_5

    .line 94
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$100(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    move-result-object v0

    if-nez v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$200(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    move-result-object v0

    iget-object v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->pages:[Landroid/print/PageRange;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->changed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-virtual {v0}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->writtenPages:[Landroid/print/PageRange;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;
    invoke-static {v1}, Lcom/android/printspooler/model/RemotePrintDocument;->access$200(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    move-result-object v1

    iget-object v1, v1, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->pages:[Landroid/print/PageRange;

    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v2}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-virtual {v2}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/printspooler/util/PageRangeUtils;->contains([Landroid/print/PageRange;[Landroid/print/PageRange;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 100
    :cond_0
    iget-object v10, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/printspooler/model/RemotePrintDocument;->access$400(Lcom/android/printspooler/model/RemotePrintDocument;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mLooper:Landroid/os/Looper;
    invoke-static {v2}, Lcom/android/printspooler/model/RemotePrintDocument;->access$500(Lcom/android/printspooler/model/RemotePrintDocument;)Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mPrintDocumentAdapter:Landroid/print/IPrintDocumentAdapter;
    invoke-static {v3}, Lcom/android/printspooler/model/RemotePrintDocument;->access$600(Lcom/android/printspooler/model/RemotePrintDocument;)Landroid/print/IPrintDocumentAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v4}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v5}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v5

    iget-object v5, v5, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-virtual {v5}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v5

    iget-object v6, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;
    invoke-static {v6}, Lcom/android/printspooler/model/RemotePrintDocument;->access$200(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    move-result-object v6

    iget-object v6, v6, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->pages:[Landroid/print/PageRange;

    iget-object v7, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v7}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v7

    iget-object v7, v7, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->fileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    iget-object v8, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mCommandResultCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;
    invoke-static {v8}, Lcom/android/printspooler/model/RemotePrintDocument;->access$700(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;-><init>(Landroid/content/Context;Landroid/os/Looper;Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;I[Landroid/print/PageRange;Lcom/android/printspooler/model/MutexFileProvider;Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;)V

    # setter for: Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v10, v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$102(Lcom/android/printspooler/model/RemotePrintDocument;Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .line 121
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument;->runPendingCommand()V
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$1000(Lcom/android/printspooler/model/RemotePrintDocument;)V

    .line 136
    :cond_2
    :goto_1
    return-void

    .line 105
    :cond_3
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$200(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    move-result-object v0

    iget-object v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->pages:[Landroid/print/PageRange;

    if-eqz v0, :cond_4

    .line 107
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;
    invoke-static {v1}, Lcom/android/printspooler/model/RemotePrintDocument;->access$200(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    move-result-object v1

    iget-object v1, v1, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->pages:[Landroid/print/PageRange;

    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v2}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->writtenPages:[Landroid/print/PageRange;

    iget-object v3, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mDocumentInfo:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    invoke-static {v3}, Lcom/android/printspooler/model/RemotePrintDocument;->access$300(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-virtual {v3}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/android/printspooler/util/PageRangeUtils;->computePrintedPages([Landroid/print/PageRange;[Landroid/print/PageRange;I)[Landroid/print/PageRange;

    move-result-object v1

    iput-object v1, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->printedPages:[Landroid/print/PageRange;

    .line 112
    :cond_4
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # setter for: Lcom/android/printspooler/model/RemotePrintDocument;->mState:I
    invoke-static {v0, v4}, Lcom/android/printspooler/model/RemotePrintDocument;->access$802(Lcom/android/printspooler/model/RemotePrintDocument;I)I

    .line 113
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument;->notifyUpdateCompleted()V
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$900(Lcom/android/printspooler/model/RemotePrintDocument;)V

    goto :goto_0

    .line 118
    :cond_5
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # setter for: Lcom/android/printspooler/model/RemotePrintDocument;->mState:I
    invoke-static {v0, v4}, Lcom/android/printspooler/model/RemotePrintDocument;->access$802(Lcom/android/printspooler/model/RemotePrintDocument;I)I

    .line 119
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument;->notifyUpdateCompleted()V
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$900(Lcom/android/printspooler/model/RemotePrintDocument;)V

    goto :goto_0

    .line 122
    :cond_6
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$000(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isFailed()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 123
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    const/4 v1, 0x4

    # setter for: Lcom/android/printspooler/model/RemotePrintDocument;->mState:I
    invoke-static {v0, v1}, Lcom/android/printspooler/model/RemotePrintDocument;->access$802(Lcom/android/printspooler/model/RemotePrintDocument;I)I

    .line 124
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$000(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->getError()Ljava/lang/CharSequence;

    move-result-object v9

    .line 125
    .local v9, "error":Ljava/lang/CharSequence;
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # setter for: Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v0, v3}, Lcom/android/printspooler/model/RemotePrintDocument;->access$002(Lcom/android/printspooler/model/RemotePrintDocument;Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .line 126
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # setter for: Lcom/android/printspooler/model/RemotePrintDocument;->mNextCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v0, v3}, Lcom/android/printspooler/model/RemotePrintDocument;->access$102(Lcom/android/printspooler/model/RemotePrintDocument;Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    .line 127
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mUpdateSpec:Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$200(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$UpdateSpec;->reset()V

    .line 128
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument;->notifyUpdateFailed(Ljava/lang/CharSequence;)V
    invoke-static {v0, v9}, Lcom/android/printspooler/model/RemotePrintDocument;->access$1100(Lcom/android/printspooler/model/RemotePrintDocument;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 129
    .end local v9    # "error":Ljava/lang/CharSequence;
    :cond_7
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mCurrentCommand:Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$000(Lcom/android/printspooler/model/RemotePrintDocument;)Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument;->mState:I
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$800(Lcom/android/printspooler/model/RemotePrintDocument;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_8

    .line 131
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    const/4 v1, 0x7

    # setter for: Lcom/android/printspooler/model/RemotePrintDocument;->mState:I
    invoke-static {v0, v1}, Lcom/android/printspooler/model/RemotePrintDocument;->access$802(Lcom/android/printspooler/model/RemotePrintDocument;I)I

    .line 132
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument;->notifyUpdateCanceled()V
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$1200(Lcom/android/printspooler/model/RemotePrintDocument;)V

    .line 134
    :cond_8
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument;

    # invokes: Lcom/android/printspooler/model/RemotePrintDocument;->runPendingCommand()V
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$1000(Lcom/android/printspooler/model/RemotePrintDocument;)V

    goto/16 :goto_1
.end method
