.class final Lcom/android/printspooler/model/RemotePrintDocument$PrintDocumentAdapterObserver;
.super Landroid/print/IPrintDocumentAdapterObserver$Stub;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PrintDocumentAdapterObserver"
.end annotation


# instance fields
.field private final mWeakDocument:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/printspooler/model/RemotePrintDocument;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/printspooler/model/RemotePrintDocument;)V
    .locals 1
    .param p1, "document"    # Lcom/android/printspooler/model/RemotePrintDocument;

    .prologue
    .line 1150
    invoke-direct {p0}, Landroid/print/IPrintDocumentAdapterObserver$Stub;-><init>()V

    .line 1151
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$PrintDocumentAdapterObserver;->mWeakDocument:Ljava/lang/ref/WeakReference;

    .line 1152
    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    .prologue
    .line 1156
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$PrintDocumentAdapterObserver;->mWeakDocument:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/model/RemotePrintDocument;

    .line 1157
    .local v0, "document":Lcom/android/printspooler/model/RemotePrintDocument;
    if-eqz v0, :cond_0

    .line 1158
    # invokes: Lcom/android/printspooler/model/RemotePrintDocument;->notifyPrintingAppDied()V
    invoke-static {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->access$1300(Lcom/android/printspooler/model/RemotePrintDocument;)V

    .line 1160
    :cond_0
    return-void
.end method
