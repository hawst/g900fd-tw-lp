.class final Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;
.super Landroid/os/AsyncTask;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RenderPageTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/android/printspooler/model/PageContentRepository$RenderedPage;",
        ">;"
    }
.end annotation


# instance fields
.field mCallback:Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

.field final mPageIndex:I

.field final mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

.field mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

.field final synthetic this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;ILcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V
    .locals 0
    .param p2, "pageIndex"    # I
    .param p3, "renderSpec"    # Lcom/android/printspooler/model/PageContentRepository$RenderSpec;
    .param p4, "callback"    # Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    .prologue
    .line 708
    iput-object p1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 709
    iput p2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    .line 710
    iput-object p3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    .line 711
    iput-object p4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mCallback:Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    .line 712
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 785
    invoke-virtual {p0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 812
    :goto_0
    return-object v0

    .line 789
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    iget-object v0, v0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->content:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 791
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    .line 793
    .local v8, "pipe":[Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v8

    .line 794
    const/4 v0, 0x0

    aget-object v9, v8, v0

    .line 795
    .local v9, "source":Landroid/os/ParcelFileDescriptor;
    const/4 v0, 0x1

    aget-object v5, v8, v0

    .line 797
    .local v5, "destination":Landroid/os/ParcelFileDescriptor;
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mRenderer:Lcom/android/printspooler/renderer/IPdfRenderer;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$900(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/renderer/IPdfRenderer;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    iget-object v4, v4, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    invoke-interface/range {v0 .. v5}, Lcom/android/printspooler/renderer/IPdfRenderer;->renderPage(IIILandroid/print/PrintAttributes;Landroid/os/ParcelFileDescriptor;)V

    .line 802
    invoke-virtual {v5}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 804
    invoke-static {v6, v9}, Lcom/android/printspooler/util/BitmapSerializeUtils;->readBitmapPixels(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 808
    aget-object v0, v8, v10

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 809
    aget-object v0, v8, v11

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 812
    .end local v5    # "destination":Landroid/os/ParcelFileDescriptor;
    .end local v9    # "source":Landroid/os/ParcelFileDescriptor;
    :goto_1
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    goto :goto_0

    .line 805
    :catch_0
    move-exception v0

    move-object v7, v0

    .line 806
    .local v7, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_1
    const-string v0, "PageContentRepository"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error rendering page:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 808
    aget-object v0, v8, v10

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 809
    aget-object v0, v8, v11

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_1

    .line 808
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    aget-object v1, v8, v10

    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 809
    aget-object v1, v8, v11

    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v0

    .line 805
    :catch_1
    move-exception v0

    move-object v7, v0

    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 701
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->doInBackground([Ljava/lang/Void;)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    move-result-object v0

    return-object v0
.end method

.method public isPreload()Z
    .locals 1

    .prologue
    .line 852
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mCallback:Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCancelled(Lcom/android/printspooler/model/PageContentRepository$RenderedPage;)V
    .locals 3
    .param p1, "renderedPage"    # Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .prologue
    .line 836
    const-string v0, "PageContentRepository"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cancelled rendering page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1400(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Landroid/util/ArrayMap;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    if-nez p1, :cond_0

    .line 849
    :goto_0
    return-void

    .line 848
    :cond_0
    const/4 v0, 0x2

    iput v0, p1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    goto :goto_0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 701
    check-cast p1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->onCancelled(Lcom/android/printspooler/model/PageContentRepository$RenderedPage;)V

    return-void
.end method

.method public onPostExecute(Lcom/android/printspooler/model/PageContentRepository$RenderedPage;)V
    .locals 3
    .param p1, "renderedPage"    # Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .prologue
    .line 818
    const-string v0, "PageContentRepository"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Completed rendering page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageToRenderTaskMap:Landroid/util/ArrayMap;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1400(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Landroid/util/ArrayMap;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825
    const/4 v0, 0x0

    iput v0, p1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    .line 828
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mCallback:Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mCallback:Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;

    iget-object v1, p1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->content:Landroid/graphics/drawable/BitmapDrawable;

    invoke-interface {v0, v1}, Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;->onPageContentAvailable(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 831
    :cond_0
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 701
    check-cast p1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->onPostExecute(Lcom/android/printspooler/model/PageContentRepository$RenderedPage;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 7

    .prologue
    .line 716
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v4}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v4

    iget v5, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v4, v5}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getRenderedPage(I)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    move-result-object v4

    iput-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 717
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    iget v4, v4, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    if-nez v4, :cond_0

    .line 718
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Trying to render a rendered page"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 722
    :cond_0
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    iget-object v5, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    invoke-virtual {v4, v5}, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->hasSameSize(Lcom/android/printspooler/model/PageContentRepository$RenderedPage;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 724
    const-string v4, "PageContentRepository"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Recycling bitmap for page: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with different size."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v4}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v4

    iget v5, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v4, v5}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->removeRenderedPage(I)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 728
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 731
    :cond_1
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    iget v4, v4, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapWidth:I

    iget-object v5, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    iget v5, v5, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapHeight:I

    mul-int/2addr v4, v5

    mul-int/lit8 v1, v4, 0x4

    .line 735
    .local v1, "bitmapSizeInBytes":I
    :goto_0
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    if-nez v4, :cond_2

    .line 738
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v4}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getSizeInBytes()I

    move-result v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v4}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getSizeInBytes()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v5}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getMaxSizeInBytes()I

    move-result v5

    if-gt v4, v5, :cond_4

    .line 765
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    if-nez v4, :cond_3

    .line 767
    const-string v4, "PageContentRepository"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Created bitmap for page: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " cache size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v6}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getSizeInBytes()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    iget v4, v4, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapWidth:I

    iget-object v5, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    iget v5, v5, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 772
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v4, -0x1

    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 773
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$700(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v2, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 774
    .local v2, "content":Landroid/graphics/drawable/BitmapDrawable;
    new-instance v4, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    invoke-direct {v4, v2}, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V

    iput-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 777
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "content":Landroid/graphics/drawable/BitmapDrawable;
    :cond_3
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    iget-object v5, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    iput-object v5, v4, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->renderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    .line 778
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    const/4 v5, 0x1

    iput v5, v4, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    .line 780
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v4}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v4

    iget v5, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    iget-object v6, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    invoke-virtual {v4, v5, v6}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->putRenderedPage(ILcom/android/printspooler/model/PageContentRepository$RenderedPage;)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 781
    return-void

    .line 744
    :cond_4
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v4}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->removeLeastNeeded()Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    move-result-object v3

    .line 746
    .local v3, "renderedPage":Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    invoke-virtual {v4, v3}, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->hasSameSize(Lcom/android/printspooler/model/PageContentRepository$RenderedPage;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 748
    const-string v4, "PageContentRepository"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Recycling bitmap for page: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with different size."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 754
    :cond_5
    iput-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mRenderedPage:Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 755
    invoke-virtual {v3}, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->erase()V

    .line 758
    const-string v4, "PageContentRepository"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Reused bitmap for page: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->mPageIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " cache size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$RenderPageTask;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageContentCache:Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
    invoke-static {v6}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1300(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->getSizeInBytes()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
