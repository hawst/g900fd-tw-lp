.class public interface abstract Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;
.super Ljava/lang/Object;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UpdateResultCallbacks"
.end annotation


# virtual methods
.method public abstract onUpdateCanceled()V
.end method

.method public abstract onUpdateCompleted(Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;)V
.end method

.method public abstract onUpdateFailed(Ljava/lang/CharSequence;)V
.end method
