.class Lcom/android/printspooler/model/PrintSpoolerService$1;
.super Landroid/os/AsyncTask;
.source "PrintSpoolerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/model/PrintSpoolerService;->writePrintJobData(Landroid/os/ParcelFileDescriptor;Landroid/print/PrintJobId;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/PrintSpoolerService;

.field final synthetic val$fd:Landroid/os/ParcelFileDescriptor;

.field final synthetic val$printJob:Landroid/print/PrintJobInfo;

.field final synthetic val$printJobId:Landroid/print/PrintJobId;


# direct methods
.method constructor <init>(Lcom/android/printspooler/model/PrintSpoolerService;Landroid/print/PrintJobInfo;Landroid/print/PrintJobId;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    iput-object p2, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$printJob:Landroid/print/PrintJobInfo;

    iput-object p3, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$printJobId:Landroid/print/PrintJobId;

    iput-object p4, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$fd:Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 397
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v11, 0x0

    .line 400
    const/4 v3, 0x0

    .line 401
    .local v3, "in":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 403
    .local v6, "out":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$printJob:Landroid/print/PrintJobInfo;

    if-eqz v9, :cond_0

    .line 404
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    iget-object v10, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$printJobId:Landroid/print/PrintJobId;

    invoke-static {v9, v10}, Lcom/android/printspooler/model/PrintSpoolerService;->generateFileForPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)Ljava/io/File;

    move-result-object v1

    .line 405
    .local v1, "file":Ljava/io/File;
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406
    .end local v3    # "in":Ljava/io/FileInputStream;
    .local v4, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$fd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v6    # "out":Ljava/io/FileOutputStream;
    .local v7, "out":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 408
    .end local v1    # "file":Ljava/io/File;
    .end local v4    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    :cond_0
    const/16 v9, 0x2000

    :try_start_2
    new-array v0, v9, [B

    .line 410
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v8

    .line 411
    .local v8, "readByteCount":I
    if-gez v8, :cond_1

    .line 421
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 422
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 423
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$fd:Landroid/os/ParcelFileDescriptor;

    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 426
    .end local v0    # "buffer":[B
    .end local v8    # "readByteCount":I
    :goto_1
    return-object v11

    .line 414
    .restart local v0    # "buffer":[B
    .restart local v8    # "readByteCount":I
    :cond_1
    const/4 v9, 0x0

    :try_start_3
    invoke-virtual {v6, v0, v9, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 416
    .end local v0    # "buffer":[B
    .end local v8    # "readByteCount":I
    :catch_0
    move-exception v2

    .line 417
    .local v2, "fnfe":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v9, "PrintSpoolerService"

    const-string v10, "Error writing print job data!"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 421
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 422
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 423
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$fd:Landroid/os/ParcelFileDescriptor;

    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 425
    .end local v2    # "fnfe":Ljava/io/FileNotFoundException;
    :goto_3
    const-string v9, "PrintSpoolerService"

    const-string v10, "[END WRITE]"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 418
    :catch_1
    move-exception v5

    .line 419
    .local v5, "ioe":Ljava/io/IOException;
    :goto_4
    :try_start_5
    const-string v9, "PrintSpoolerService"

    const-string v10, "Error writing print job data!"

    invoke-static {v9, v10, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 421
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 422
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 423
    iget-object v9, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$fd:Landroid/os/ParcelFileDescriptor;

    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_3

    .line 421
    .end local v5    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_5
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 422
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 423
    iget-object v10, p0, Lcom/android/printspooler/model/PrintSpoolerService$1;->val$fd:Landroid/os/ParcelFileDescriptor;

    invoke-static {v10}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v9

    .line 421
    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v9

    move-object v3, v4

    .end local v4    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    goto :goto_5

    .line 418
    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    :catch_2
    move-exception v5

    move-object v3, v4

    .end local v4    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    goto :goto_4

    .line 416
    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    :catch_3
    move-exception v2

    move-object v3, v4

    .end local v4    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    goto :goto_2
.end method
