.class Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;
.super Landroid/os/AsyncTask;
.source "PrintSpoolerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->writeStateLocked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;


# direct methods
.method constructor <init>(Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;)V
    .locals 0

    .prologue
    .line 738
    iput-object p1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;->this$1:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 738
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 741
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;->this$1:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    iget-object v0, v0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/printspooler/model/PrintSpoolerService;->access$200(Lcom/android/printspooler/model/PrintSpoolerService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 742
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;->this$1:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    const/4 v2, 0x0

    # setter for: Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->mWriteStateScheduled:Z
    invoke-static {v0, v2}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->access$602(Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;Z)Z

    .line 743
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager$1;->this$1:Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;

    # invokes: Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->doWriteStateLocked()V
    invoke-static {v0}, Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;->access$700(Lcom/android/printspooler/model/PrintSpoolerService$PersistenceManager;)V

    .line 744
    monitor-exit v1

    .line 745
    const/4 v0, 0x0

    return-object v0

    .line 744
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
