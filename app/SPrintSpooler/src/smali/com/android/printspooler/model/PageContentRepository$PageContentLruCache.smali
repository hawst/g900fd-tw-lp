.class final Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;
.super Ljava/lang/Object;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PageContentRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PageContentLruCache"
.end annotation


# instance fields
.field private final mMaxSizeInBytes:I

.field private final mRenderedPages:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/printspooler/model/PageContentRepository$RenderedPage;",
            ">;"
        }
    .end annotation
.end field

.field private mSizeInBytes:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxSizeInBytes"    # I

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    .line 256
    iput p1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mMaxSizeInBytes:I

    .line 257
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 327
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 329
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/printspooler/model/PageContentRepository$RenderedPage;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 331
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 333
    :cond_0
    return-void
.end method

.method public getMaxSizeInBytes()I
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mMaxSizeInBytes:I

    return v0
.end method

.method public getRenderedPage(I)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    .locals 2
    .param p1, "pageIndex"    # I

    .prologue
    .line 260
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    return-object v0
.end method

.method public getSizeInBytes()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    return v0
.end method

.method public invalidate()V
    .locals 4

    .prologue
    .line 289
    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 290
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/printspooler/model/PageContentRepository$RenderedPage;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    const/4 v3, 0x2

    iput v3, v2, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    goto :goto_0

    .line 292
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/printspooler/model/PageContentRepository$RenderedPage;>;"
    :cond_0
    return-void
.end method

.method public putRenderedPage(ILcom/android/printspooler/model/PageContentRepository$RenderedPage;)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    .locals 4
    .param p1, "pageIndex"    # I
    .param p2, "renderedPage"    # Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .prologue
    .line 272
    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 273
    .local v1, "oldRenderedPage":Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    if-eqz v1, :cond_0

    .line 274
    iget-object v2, v1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->renderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    iget-object v3, p2, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->renderSpec:Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    invoke-virtual {v2, v3}, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 275
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Wrong page size"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 278
    :cond_0
    invoke-virtual {p2}, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->getSizeInBytes()I

    move-result v0

    .line 279
    .local v0, "contentSizeInBytes":I
    iget v2, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    add-int/2addr v2, v0

    iget v3, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mMaxSizeInBytes:I

    if-le v2, v3, :cond_1

    .line 280
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Client didn\'t free space"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 283
    :cond_1
    iget v2, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    .line 285
    .end local v0    # "contentSizeInBytes":I
    :cond_2
    iget-object v2, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    return-object v2
.end method

.method public removeLeastNeeded()Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    .locals 6

    .prologue
    .line 295
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 296
    const/4 v3, 0x0

    .line 315
    :goto_0
    return-object v3

    .line 301
    :cond_0
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 302
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/printspooler/model/PageContentRepository$RenderedPage;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 303
    .local v3, "renderedPage":Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    iget v4, v3, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->state:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 304
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 305
    .local v2, "pageIndex":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    invoke-virtual {v3}, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->getSizeInBytes()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    goto :goto_0

    .line 312
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/printspooler/model/PageContentRepository$RenderedPage;>;"
    .end local v2    # "pageIndex":Ljava/lang/Integer;
    .end local v3    # "renderedPage":Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    :cond_2
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 313
    .local v2, "pageIndex":I
    iget-object v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 314
    .restart local v3    # "renderedPage":Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    iget v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    invoke-virtual {v3}, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->getSizeInBytes()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    goto :goto_0
.end method

.method public removeRenderedPage(I)Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    .line 264
    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mRenderedPages:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .line 265
    .local v0, "page":Lcom/android/printspooler/model/PageContentRepository$RenderedPage;
    if-eqz v0, :cond_0

    .line 266
    iget v1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    invoke-virtual {v0}, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->getSizeInBytes()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/printspooler/model/PageContentRepository$PageContentLruCache;->mSizeInBytes:I

    .line 268
    :cond_0
    return-object v0
.end method
