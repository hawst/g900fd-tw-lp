.class public final Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
.super Ljava/lang/Object;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RemotePrintDocumentInfo"
.end annotation


# instance fields
.field public attributes:Landroid/print/PrintAttributes;

.field public changed:Z

.field public fileProvider:Lcom/android/printspooler/model/MutexFileProvider;

.field public info:Landroid/print/PrintDocumentInfo;

.field public laidout:Z

.field public metadata:Landroid/os/Bundle;

.field public printedPages:[Landroid/print/PageRange;

.field public writtenPages:[Landroid/print/PageRange;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
