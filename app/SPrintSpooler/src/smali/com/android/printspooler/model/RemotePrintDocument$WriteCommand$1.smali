.class Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;
.super Landroid/os/AsyncTask;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;


# direct methods
.method constructor <init>(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)V
    .locals 0

    .prologue
    .line 913
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 913
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 15
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 916
    const/4 v2, 0x0

    .line 917
    .local v2, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 918
    .local v3, "in":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 919
    .local v5, "out":Ljava/io/OutputStream;
    const/4 v10, 0x0

    .line 920
    .local v10, "source":Landroid/os/ParcelFileDescriptor;
    const/4 v9, 0x0

    .line 922
    .local v9, "sink":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    iget-object v11, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;
    invoke-static {v11}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$1800(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)Lcom/android/printspooler/model/MutexFileProvider;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/android/printspooler/model/MutexFileProvider;->acquireFile(Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;)Ljava/io/File;

    move-result-object v2

    .line 923
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v7

    .line 924
    .local v7, "pipe":[Landroid/os/ParcelFileDescriptor;
    const/4 v11, 0x0

    aget-object v10, v7, v11

    .line 925
    const/4 v11, 0x1

    aget-object v9, v7, v11

    .line 927
    new-instance v4, Ljava/io/FileInputStream;

    invoke-virtual {v10}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    invoke-direct {v4, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 928
    .end local v3    # "in":Ljava/io/InputStream;
    .local v4, "in":Ljava/io/InputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 932
    .end local v5    # "out":Ljava/io/OutputStream;
    .local v6, "out":Ljava/io/OutputStream;
    :try_start_2
    const-string v11, "RemotePrintDocument"

    const-string v12, "[PERFORMING] write"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    iget-object v11, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    iget-object v11, v11, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mAdapter:Landroid/print/IPrintDocumentAdapter;

    iget-object v12, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mPages:[Landroid/print/PageRange;
    invoke-static {v12}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$1900(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)[Landroid/print/PageRange;

    move-result-object v12

    iget-object v13, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mRemoteResultCallback:Landroid/print/IWriteResultCallback;
    invoke-static {v13}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$2000(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)Landroid/print/IWriteResultCallback;

    move-result-object v13

    iget-object v14, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    iget v14, v14, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mSequence:I

    invoke-interface {v11, v12, v9, v13, v14}, Landroid/print/IPrintDocumentAdapter;->write([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/print/IWriteResultCallback;I)V

    .line 937
    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 938
    const/4 v9, 0x0

    .line 941
    const/16 v11, 0x2000

    new-array v0, v11, [B

    .line 943
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v8

    .line 944
    .local v8, "readByteCount":I
    if-gez v8, :cond_1

    .line 952
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 953
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 954
    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 955
    invoke-static {v10}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 956
    if-eqz v2, :cond_3

    .line 957
    iget-object v11, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;
    invoke-static {v11}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$1800(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)Lcom/android/printspooler/model/MutexFileProvider;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    move-object v5, v6

    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .line 960
    .end local v0    # "buffer":[B
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v7    # "pipe":[Landroid/os/ParcelFileDescriptor;
    .end local v8    # "readByteCount":I
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_1
    const/4 v11, 0x0

    return-object v11

    .line 947
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    .restart local v7    # "pipe":[Landroid/os/ParcelFileDescriptor;
    .restart local v8    # "readByteCount":I
    :cond_1
    const/4 v11, 0x0

    :try_start_3
    invoke-virtual {v6, v0, v11, v8}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_0

    .line 949
    .end local v0    # "buffer":[B
    .end local v8    # "readByteCount":I
    :catch_0
    move-exception v11

    move-object v5, v6

    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .end local v7    # "pipe":[Landroid/os/ParcelFileDescriptor;
    .restart local v3    # "in":Ljava/io/InputStream;
    :goto_2
    move-object v1, v11

    .line 950
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_4
    const-string v11, "RemotePrintDocument"

    const-string v12, "Error calling write()"

    invoke-static {v11, v12, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 952
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 953
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 954
    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 955
    invoke-static {v10}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 956
    if-eqz v2, :cond_0

    .line 957
    iget-object v11, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;
    invoke-static {v11}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$1800(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)Lcom/android/printspooler/model/MutexFileProvider;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    goto :goto_1

    .line 952
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    :goto_4
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 953
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 954
    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 955
    invoke-static {v10}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 956
    if-eqz v2, :cond_2

    .line 957
    iget-object v12, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->this$0:Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    # getter for: Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;
    invoke-static {v12}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->access$1800(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)Lcom/android/printspooler/model/MutexFileProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    :cond_2
    throw v11

    .line 949
    :catch_1
    move-exception v11

    :goto_5
    move-object v1, v11

    goto :goto_3

    .line 952
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v7    # "pipe":[Landroid/os/ParcelFileDescriptor;
    :catchall_1
    move-exception v11

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_4

    .end local v3    # "in":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    :catchall_2
    move-exception v11

    move-object v5, v6

    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_4

    .line 949
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catch_2
    move-exception v11

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_5

    .end local v3    # "in":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    :catch_3
    move-exception v11

    move-object v5, v6

    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_5

    .end local v7    # "pipe":[Landroid/os/ParcelFileDescriptor;
    :catch_4
    move-exception v11

    goto :goto_2

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v7    # "pipe":[Landroid/os/ParcelFileDescriptor;
    :catch_5
    move-exception v11

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v3    # "in":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    .restart local v8    # "readByteCount":I
    :cond_3
    move-object v5, v6

    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method
