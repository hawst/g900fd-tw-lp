.class public final Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;
.super Landroid/print/IPrintSpooler$Stub;
.source "PrintSpoolerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PrintSpoolerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PrintSpooler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/PrintSpoolerService;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/model/PrintSpoolerService;)V
    .locals 0

    .prologue
    .line 1216
    iput-object p1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-direct {p0}, Landroid/print/IPrintSpooler$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public createPrintJob(Landroid/print/PrintJobInfo;)V
    .locals 1
    .param p1, "printJob"    # Landroid/print/PrintJobInfo;

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v0, p1}, Lcom/android/printspooler/model/PrintSpoolerService;->createPrintJob(Landroid/print/PrintJobInfo;)V

    .line 1244
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/printspooler/model/PrintSpoolerService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1289
    return-void
.end method

.method public getPrintJobInfo(Landroid/print/PrintJobId;Landroid/print/IPrintSpoolerCallbacks;II)V
    .locals 2
    .param p1, "printJobId"    # Landroid/print/PrintJobId;
    .param p2, "callback"    # Landroid/print/IPrintSpoolerCallbacks;
    .param p3, "appId"    # I
    .param p4, "sequence"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1233
    const/4 v0, 0x0

    .line 1235
    .local v0, "printJob":Landroid/print/PrintJobInfo;
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v1, p1, p3}, Lcom/android/printspooler/model/PrintSpoolerService;->getPrintJobInfo(Landroid/print/PrintJobId;I)Landroid/print/PrintJobInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1237
    invoke-interface {p2, v0, p4}, Landroid/print/IPrintSpoolerCallbacks;->onGetPrintJobInfoResult(Landroid/print/PrintJobInfo;I)V

    .line 1239
    return-void

    .line 1237
    :catchall_0
    move-exception v1

    invoke-interface {p2, v0, p4}, Landroid/print/IPrintSpoolerCallbacks;->onGetPrintJobInfoResult(Landroid/print/PrintJobInfo;I)V

    throw v1
.end method

.method public getPrintJobInfos(Landroid/print/IPrintSpoolerCallbacks;Landroid/content/ComponentName;III)V
    .locals 2
    .param p1, "callback"    # Landroid/print/IPrintSpoolerCallbacks;
    .param p2, "componentName"    # Landroid/content/ComponentName;
    .param p3, "state"    # I
    .param p4, "appId"    # I
    .param p5, "sequence"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1221
    const/4 v0, 0x0

    .line 1223
    .local v0, "printJobs":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintJobInfo;>;"
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v1, p2, p3, p4}, Lcom/android/printspooler/model/PrintSpoolerService;->getPrintJobInfos(Landroid/content/ComponentName;II)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1226
    invoke-interface {p1, v0, p5}, Landroid/print/IPrintSpoolerCallbacks;->onGetPrintJobInfosResult(Ljava/util/List;I)V

    .line 1228
    return-void

    .line 1226
    :catchall_0
    move-exception v1

    invoke-interface {p1, v0, p5}, Landroid/print/IPrintSpoolerCallbacks;->onGetPrintJobInfosResult(Ljava/util/List;I)V

    throw v1
.end method

.method public getService()Lcom/android/printspooler/model/PrintSpoolerService;
    .locals 1

    .prologue
    .line 1297
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    return-object v0
.end method

.method public removeObsoletePrintJobs()V
    .locals 1

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # invokes: Lcom/android/printspooler/model/PrintSpoolerService;->removeObsoletePrintJobs()V
    invoke-static {v0}, Lcom/android/printspooler/model/PrintSpoolerService;->access$1000(Lcom/android/printspooler/model/PrintSpoolerService;)V

    .line 1284
    return-void
.end method

.method public setClient(Landroid/print/IPrintSpoolerClient;)V
    .locals 3
    .param p1, "client"    # Landroid/print/IPrintSpoolerClient;

    .prologue
    .line 1276
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;
    invoke-static {v1}, Lcom/android/printspooler/model/PrintSpoolerService;->access$400(Lcom/android/printspooler/model/PrintSpoolerService;)Lcom/android/internal/os/HandlerCaller;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1278
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    # getter for: Lcom/android/printspooler/model/PrintSpoolerService;->mHandlerCaller:Lcom/android/internal/os/HandlerCaller;
    invoke-static {v1}, Lcom/android/printspooler/model/PrintSpoolerService;->access$400(Lcom/android/printspooler/model/PrintSpoolerService;)Lcom/android/internal/os/HandlerCaller;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    .line 1279
    return-void
.end method

.method public setPrintJobCancelling(Landroid/print/PrintJobId;Z)V
    .locals 1
    .param p1, "printJobId"    # Landroid/print/PrintJobId;
    .param p2, "cancelling"    # Z

    .prologue
    .line 1293
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v0, p1, p2}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobCancelling(Landroid/print/PrintJobId;Z)V

    .line 1294
    return-void
.end method

.method public setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;Landroid/print/IPrintSpoolerCallbacks;I)V
    .locals 2
    .param p1, "printJobId"    # Landroid/print/PrintJobId;
    .param p2, "state"    # I
    .param p3, "error"    # Ljava/lang/String;
    .param p4, "callback"    # Landroid/print/IPrintSpoolerCallbacks;
    .param p5, "sequece"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1249
    const/4 v0, 0x0

    .line 1251
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v1, p1, p2, p3}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1254
    invoke-interface {p4, v0, p5}, Landroid/print/IPrintSpoolerCallbacks;->onSetPrintJobStateResult(ZI)V

    .line 1256
    return-void

    .line 1254
    :catchall_0
    move-exception v1

    invoke-interface {p4, v0, p5}, Landroid/print/IPrintSpoolerCallbacks;->onSetPrintJobStateResult(ZI)V

    throw v1
.end method

.method public setPrintJobTag(Landroid/print/PrintJobId;Ljava/lang/String;Landroid/print/IPrintSpoolerCallbacks;I)V
    .locals 2
    .param p1, "printJobId"    # Landroid/print/PrintJobId;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "callback"    # Landroid/print/IPrintSpoolerCallbacks;
    .param p4, "sequece"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1261
    const/4 v0, 0x0

    .line 1263
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v1, p1, p2}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobTag(Landroid/print/PrintJobId;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1265
    invoke-interface {p3, v0, p4}, Landroid/print/IPrintSpoolerCallbacks;->onSetPrintJobTagResult(ZI)V

    .line 1267
    return-void

    .line 1265
    :catchall_0
    move-exception v1

    invoke-interface {p3, v0, p4}, Landroid/print/IPrintSpoolerCallbacks;->onSetPrintJobTagResult(ZI)V

    throw v1
.end method

.method public writePrintJobData(Landroid/os/ParcelFileDescriptor;Landroid/print/PrintJobId;)V
    .locals 1
    .param p1, "fd"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "printJobId"    # Landroid/print/PrintJobId;

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/android/printspooler/model/PrintSpoolerService$PrintSpooler;->this$0:Lcom/android/printspooler/model/PrintSpoolerService;

    invoke-virtual {v0, p1, p2}, Lcom/android/printspooler/model/PrintSpoolerService;->writePrintJobData(Landroid/os/ParcelFileDescriptor;Landroid/print/PrintJobId;)V

    .line 1272
    return-void
.end method
