.class final Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;
.super Landroid/print/ILayoutResultCallback$Stub;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LayoutResultCallback"
.end annotation


# instance fields
.field private final mWeakHandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 835
    invoke-direct {p0}, Landroid/print/ILayoutResultCallback$Stub;-><init>()V

    .line 836
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    .line 837
    return-void
.end method


# virtual methods
.method public onLayoutCanceled(I)V
    .locals 3
    .param p1, "sequence"    # I

    .prologue
    .line 868
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 869
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 870
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 873
    :cond_0
    return-void
.end method

.method public onLayoutFailed(Ljava/lang/CharSequence;I)V
    .locals 3
    .param p1, "error"    # Ljava/lang/CharSequence;
    .param p2, "sequence"    # I

    .prologue
    .line 859
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 860
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 861
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 864
    :cond_0
    return-void
.end method

.method public onLayoutFinished(Landroid/print/PrintDocumentInfo;ZI)V
    .locals 3
    .param p1, "info"    # Landroid/print/PrintDocumentInfo;
    .param p2, "changed"    # Z
    .param p3, "sequence"    # I

    .prologue
    .line 850
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 851
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 852
    const/4 v2, 0x2

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v2, v1, p3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 855
    :cond_0
    return-void

    .line 852
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onLayoutStarted(Landroid/os/ICancellationSignal;I)V
    .locals 3
    .param p1, "cancellation"    # Landroid/os/ICancellationSignal;
    .param p2, "sequence"    # I

    .prologue
    .line 841
    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$LayoutCommand$LayoutResultCallback;->mWeakHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 842
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 843
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 846
    :cond_0
    return-void
.end method
