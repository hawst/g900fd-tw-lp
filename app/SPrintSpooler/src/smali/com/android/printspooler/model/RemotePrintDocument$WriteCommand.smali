.class final Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;
.super Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;
.source "RemotePrintDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/RemotePrintDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WriteCommand"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;,
        Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

.field private final mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

.field private final mHandler:Landroid/os/Handler;

.field private final mPageCount:I

.field private final mPages:[Landroid/print/PageRange;

.field private final mRemoteResultCallback:Landroid/print/IWriteResultCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;I[Landroid/print/PageRange;Lcom/android/printspooler/model/MutexFileProvider;Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "adapter"    # Landroid/print/IPrintDocumentAdapter;
    .param p4, "document"    # Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    .param p5, "pageCount"    # I
    .param p6, "pages"    # [Landroid/print/PageRange;
    .param p7, "fileProvider"    # Lcom/android/printspooler/model/MutexFileProvider;
    .param p8, "callback"    # Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    .prologue
    .line 891
    invoke-direct {p0, p3, p4, p8}, Lcom/android/printspooler/model/RemotePrintDocument$AsyncCommand;-><init>(Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;)V

    .line 892
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mContext:Landroid/content/Context;

    .line 893
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;

    invoke-direct {v0, p0, p2}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteHandler;-><init>(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mHandler:Landroid/os/Handler;

    .line 894
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;

    iget-object v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$WriteResultCallback;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mRemoteResultCallback:Landroid/print/IWriteResultCallback;

    .line 895
    iput p5, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mPageCount:I

    .line 896
    array-length v0, p6

    invoke-static {p6, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/print/PageRange;

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mPages:[Landroid/print/PageRange;

    .line 897
    iput-object p7, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    .line 898
    iput-object p8, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    .line 899
    return-void
.end method

.method static synthetic access$1800(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)Lcom/android/printspooler/model/MutexFileProvider;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    .prologue
    .line 877
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)[Landroid/print/PageRange;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    .prologue
    .line 877
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mPages:[Landroid/print/PageRange;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)Landroid/print/IWriteResultCallback;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;

    .prologue
    .line 877
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mRemoteResultCallback:Landroid/print/IWriteResultCallback;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;Landroid/os/ICancellationSignal;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;
    .param p1, "x1"    # Landroid/os/ICancellationSignal;
    .param p2, "x2"    # I

    .prologue
    .line 877
    invoke-direct {p0, p1, p2}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteStarted(Landroid/os/ICancellationSignal;I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;[Landroid/print/PageRange;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;
    .param p1, "x1"    # [Landroid/print/PageRange;
    .param p2, "x2"    # I

    .prologue
    .line 877
    invoke-direct {p0, p1, p2}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteFinished([Landroid/print/PageRange;I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;Ljava/lang/CharSequence;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;
    .param p1, "x1"    # Ljava/lang/CharSequence;
    .param p2, "x2"    # I

    .prologue
    .line 877
    invoke-direct {p0, p1, p2}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteFailed(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;
    .param p1, "x1"    # I

    .prologue
    .line 877
    invoke-direct {p0, p1}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteCanceled(I)V

    return-void
.end method

.method private handleOnWriteCanceled(I)V
    .locals 2
    .param p1, "sequence"    # I

    .prologue
    .line 1036
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mSequence:I

    if-eq p1, v0, :cond_0

    .line 1051
    :goto_0
    return-void

    .line 1041
    :cond_0
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLBACK] onWriteCanceled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->canceled()V

    .line 1047
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mCancellation:Landroid/os/ICancellationSignal;

    .line 1050
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    invoke-interface {v0}, Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;->onDone()V

    goto :goto_0
.end method

.method private handleOnWriteFailed(Ljava/lang/CharSequence;I)V
    .locals 2
    .param p1, "error"    # Ljava/lang/CharSequence;
    .param p2, "sequence"    # I

    .prologue
    .line 1018
    iget v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mSequence:I

    if-eq p2, v0, :cond_0

    .line 1033
    :goto_0
    return-void

    .line 1023
    :cond_0
    const-string v0, "RemotePrintDocument"

    const-string v1, "[CALLBACK] onWriteFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->failed(Ljava/lang/CharSequence;)V

    .line 1029
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mCancellation:Landroid/os/ICancellationSignal;

    .line 1032
    iget-object v0, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    invoke-interface {v0}, Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;->onDone()V

    goto :goto_0
.end method

.method private handleOnWriteFinished([Landroid/print/PageRange;I)V
    .locals 5
    .param p1, "pages"    # [Landroid/print/PageRange;
    .param p2, "sequence"    # I

    .prologue
    const/4 v4, 0x0

    .line 987
    iget v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mSequence:I

    if-eq p2, v2, :cond_0

    .line 1015
    :goto_0
    return-void

    .line 992
    :cond_0
    const-string v2, "RemotePrintDocument"

    const-string v3, "[CALLBACK] onWriteFinished"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    invoke-static {p1}, Lcom/android/printspooler/util/PageRangeUtils;->normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;

    move-result-object v1

    .line 996
    .local v1, "writtenPages":[Landroid/print/PageRange;
    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mPages:[Landroid/print/PageRange;

    iget v3, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mPageCount:I

    invoke-static {v2, v1, v3}, Lcom/android/printspooler/util/PageRangeUtils;->computePrintedPages([Landroid/print/PageRange;[Landroid/print/PageRange;I)[Landroid/print/PageRange;

    move-result-object v0

    .line 1000
    .local v0, "printedPages":[Landroid/print/PageRange;
    if-eqz v0, :cond_1

    .line 1001
    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-object v1, v2, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->writtenPages:[Landroid/print/PageRange;

    .line 1002
    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-object v0, v2, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->printedPages:[Landroid/print/PageRange;

    .line 1003
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->completed()V

    .line 1011
    :goto_1
    iput-object v4, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mCancellation:Landroid/os/ICancellationSignal;

    .line 1014
    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mDoneCallback:Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;

    invoke-interface {v2}, Lcom/android/printspooler/model/RemotePrintDocument$CommandDoneCallback;->onDone()V

    goto :goto_0

    .line 1005
    :cond_1
    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-object v4, v2, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->writtenPages:[Landroid/print/PageRange;

    .line 1006
    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mDocument:Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    iput-object v4, v2, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->printedPages:[Landroid/print/PageRange;

    .line 1007
    iget-object v2, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mContext:Landroid/content/Context;

    const v3, 0x7f09003c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->failed(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private handleOnWriteStarted(Landroid/os/ICancellationSignal;I)V
    .locals 3
    .param p1, "cancellation"    # Landroid/os/ICancellationSignal;
    .param p2, "sequence"    # I

    .prologue
    .line 966
    iget v1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mSequence:I

    if-eq p2, v1, :cond_0

    .line 984
    :goto_0
    return-void

    .line 971
    :cond_0
    const-string v1, "RemotePrintDocument"

    const-string v2, "[CALLBACK] onWriteStarted"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->isCanceling()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 976
    :try_start_0
    invoke-interface {p1}, Landroid/os/ICancellationSignal;->cancel()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 977
    :catch_0
    move-exception v0

    .line 978
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "RemotePrintDocument"

    const-string v2, "Error cancelling"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 979
    const/4 v1, 0x0

    invoke-direct {p0, v1, p2}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->handleOnWriteFailed(Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 982
    .end local v0    # "re":Landroid/os/RemoteException;
    :cond_1
    iput-object p1, p0, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->mCancellation:Landroid/os/ICancellationSignal;

    goto :goto_0
.end method


# virtual methods
.method public getCommandType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 902
    const-string v0, "WriteCommand"

    return-object v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 907
    invoke-virtual {p0}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;->running()V

    .line 913
    new-instance v1, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;

    invoke-direct {v1, p0}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;-><init>(Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, Lcom/android/printspooler/model/RemotePrintDocument$WriteCommand$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 963
    return-void
.end method
