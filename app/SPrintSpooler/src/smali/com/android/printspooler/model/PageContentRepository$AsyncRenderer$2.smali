.class Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;
.super Landroid/os/AsyncTask;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->close(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

.field final synthetic val$callback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    iput-object p2, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 518
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 521
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$800(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 523
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    # getter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mRenderer:Lcom/android/printspooler/renderer/IPdfRenderer;
    invoke-static {v0}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$900(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;)Lcom/android/printspooler/renderer/IPdfRenderer;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/printspooler/renderer/IPdfRenderer;->closeDocument()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 528
    const/4 v0, 0x0

    return-object v0

    .line 527
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 524
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 518
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 533
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->this$0:Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;

    const/4 v1, -0x1

    # setter for: Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->mPageCount:I
    invoke-static {v0, v1}, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;->access$1102(Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer;I)I

    .line 534
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->val$callback:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$AsyncRenderer$2;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 537
    :cond_0
    return-void
.end method
