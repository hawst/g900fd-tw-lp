.class public final Lcom/android/printspooler/model/PageContentRepository$RenderSpec;
.super Ljava/lang/Object;
.source "PageContentRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/model/PageContentRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RenderSpec"
.end annotation


# instance fields
.field final bitmapHeight:I

.field final bitmapWidth:I

.field final printAttributes:Landroid/print/PrintAttributes;


# direct methods
.method public constructor <init>(IILandroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V
    .locals 1
    .param p1, "bitmapWidth"    # I
    .param p2, "bitmapHeight"    # I
    .param p3, "mediaSize"    # Landroid/print/PrintAttributes$MediaSize;
    .param p4, "minMargins"    # Landroid/print/PrintAttributes$Margins;

    .prologue
    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    new-instance v0, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v0}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    .line 343
    iput p1, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapWidth:I

    .line 344
    iput p2, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapHeight:I

    .line 345
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    invoke-virtual {v0, p3}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    .line 346
    iget-object v0, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    invoke-virtual {v0, p4}, Landroid/print/PrintAttributes;->setMinMargins(Landroid/print/PrintAttributes$Margins;)V

    .line 347
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 351
    if-ne p0, p1, :cond_1

    .line 374
    :cond_0
    :goto_0
    return v1

    .line 354
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 355
    goto :goto_0

    .line 357
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 358
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 360
    check-cast v0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    .line 361
    .local v0, "other":Lcom/android/printspooler/model/PageContentRepository$RenderSpec;
    iget v3, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapHeight:I

    iget v4, v0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapHeight:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 362
    goto :goto_0

    .line 364
    :cond_4
    iget v3, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapWidth:I

    iget v4, v0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapWidth:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 365
    goto :goto_0

    .line 367
    :cond_5
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    if-eqz v3, :cond_6

    .line 368
    iget-object v3, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    iget-object v4, v0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    invoke-virtual {v3, v4}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 369
    goto :goto_0

    .line 371
    :cond_6
    iget-object v3, v0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    if-eqz v3, :cond_0

    move v1, v2

    .line 372
    goto :goto_0
.end method

.method public hasSameSize(Lcom/android/printspooler/model/PageContentRepository$RenderedPage;)Z
    .locals 3
    .param p1, "page"    # Lcom/android/printspooler/model/PageContentRepository$RenderedPage;

    .prologue
    .line 378
    iget-object v1, p1, Lcom/android/printspooler/model/PageContentRepository$RenderedPage;->content:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 379
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapWidth:I

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapHeight:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 385
    iget v0, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapWidth:I

    .line 386
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->bitmapHeight:I

    add-int v0, v1, v2

    .line 387
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;->printAttributes:Landroid/print/PrintAttributes;

    invoke-virtual {v1}, Landroid/print/PrintAttributes;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v2, v1

    .line 388
    return v0

    .line 387
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
