.class Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;
.super Ljava/lang/Object;
.source "SelectPrinterActivity.java"

# interfaces
.implements Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

.field final synthetic val$this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;Lcom/android/printspooler/ui/SelectPrinterActivity;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    iput-object p2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->val$this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrintersChanged(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 451
    .local p1, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$400(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 452
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mPrinters:Ljava/util/List;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$500(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 453
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mPrinters:Ljava/util/List;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$500(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 454
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mFilteredPrinters:Ljava/util/List;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$600(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 455
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mFilteredPrinters:Ljava/util/List;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$600(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 456
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLastSearchString:Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$700(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iget-object v2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLastSearchString:Ljava/lang/CharSequence;
    invoke-static {v2}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$700(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 459
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->notifyDataSetChanged()V

    .line 461
    return-void

    .line 459
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onPrintersInvalid()V
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$400(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 466
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mPrinters:Ljava/util/List;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$500(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 467
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mFilteredPrinters:Ljava/util/List;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->access$600(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 468
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;->this$1:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->notifyDataSetInvalidated()V

    .line 470
    return-void

    .line 468
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
