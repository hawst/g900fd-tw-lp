.class final Lcom/android/printspooler/ui/PageAdapter$PageClickListener;
.super Ljava/lang/Object;
.source "PageAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PageClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PageAdapter;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/PageAdapter;)V
    .locals 0

    .prologue
    .line 788
    iput-object p1, p0, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;->this$0:Lcom/android/printspooler/ui/PageAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/PageAdapter;Lcom/android/printspooler/ui/PageAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/PageAdapter;
    .param p2, "x1"    # Lcom/android/printspooler/ui/PageAdapter$1;

    .prologue
    .line 788
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;-><init>(Lcom/android/printspooler/ui/PageAdapter;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 791
    move-object v1, p1

    check-cast v1, Lcom/android/printspooler/widget/PreviewPageFrame;

    .line 792
    .local v1, "page":Lcom/android/printspooler/widget/PreviewPageFrame;
    invoke-virtual {v1}, Lcom/android/printspooler/widget/PreviewPageFrame;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;

    .line 793
    .local v0, "holder":Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;
    iget v2, v0, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;->mPageInAdapter:I

    .line 794
    .local v2, "pageInAdapter":I
    iget-object v4, p0, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;->this$0:Lcom/android/printspooler/ui/PageAdapter;

    # invokes: Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInDocument(I)I
    invoke-static {v4, v2}, Lcom/android/printspooler/ui/PageAdapter;->access$200(Lcom/android/printspooler/ui/PageAdapter;I)I

    move-result v3

    .line 795
    .local v3, "pageInDocument":I
    iget-object v4, p0, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;->this$0:Lcom/android/printspooler/ui/PageAdapter;

    # getter for: Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;
    invoke-static {v4}, Lcom/android/printspooler/ui/PageAdapter;->access$300(Lcom/android/printspooler/ui/PageAdapter;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_1

    .line 796
    iget-object v4, p0, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;->this$0:Lcom/android/printspooler/ui/PageAdapter;

    # getter for: Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;
    invoke-static {v4}, Lcom/android/printspooler/ui/PageAdapter;->access$300(Lcom/android/printspooler/ui/PageAdapter;)Landroid/util/SparseArray;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 797
    invoke-virtual {v1, v6, v6}, Lcom/android/printspooler/widget/PreviewPageFrame;->setSelected(ZZ)V

    .line 805
    :cond_0
    :goto_0
    return-void

    .line 799
    :cond_1
    iget-object v4, p0, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;->this$0:Lcom/android/printspooler/ui/PageAdapter;

    # getter for: Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;
    invoke-static {v4}, Lcom/android/printspooler/ui/PageAdapter;->access$300(Lcom/android/printspooler/ui/PageAdapter;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-le v4, v6, :cond_0

    .line 802
    iget-object v4, p0, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;->this$0:Lcom/android/printspooler/ui/PageAdapter;

    # getter for: Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;
    invoke-static {v4}, Lcom/android/printspooler/ui/PageAdapter;->access$300(Lcom/android/printspooler/ui/PageAdapter;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 803
    const/4 v4, 0x0

    invoke-virtual {v1, v4, v6}, Lcom/android/printspooler/widget/PreviewPageFrame;->setSelected(ZZ)V

    goto :goto_0
.end method
