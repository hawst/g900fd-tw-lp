.class Lcom/android/printspooler/ui/PrintActivity$7;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/PrintActivity;->shredPagesAndFinish(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;

.field final synthetic val$writeToUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1647
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$7;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    iput-object p2, p0, Lcom/android/printspooler/ui/PrintActivity$7;->val$writeToUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1650
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$7;->val$writeToUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1651
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$7;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$600(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/model/RemotePrintDocument;

    move-result-object v0

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$7;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$7;->val$writeToUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/android/printspooler/model/RemotePrintDocument;->writeContent(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1653
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$7;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->doFinish()V
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$900(Lcom/android/printspooler/ui/PrintActivity;)V

    .line 1654
    return-void
.end method
