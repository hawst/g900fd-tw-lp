.class final Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;
.super Landroid/widget/BaseAdapter;
.source "PrintActivity.java"

# interfaces
.implements Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DestinationAdapter"
.end annotation


# instance fields
.field private final mFakePdfPrinterHolder:Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

.field private mHistoricalPrintersLoaded:Z

.field private final mPrinterHolders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 6

    .prologue
    .line 1783
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1777
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    .line 1784
    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;
    invoke-static {p1}, Lcom/android/printspooler/ui/PrintActivity;->access$2400(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrinterRegistry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/printspooler/ui/PrinterRegistry;->areHistoricalPrintersLoaded()Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mHistoricalPrintersLoaded:Z

    .line 1785
    # getter for: Lcom/android/printspooler/ui/PrintActivity;->bSConnectPrintMode:Z
    invoke-static {p1}, Lcom/android/printspooler/ui/PrintActivity;->access$2500(Lcom/android/printspooler/ui/PrintActivity;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1786
    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;
    invoke-static {p1}, Lcom/android/printspooler/ui/PrintActivity;->access$2400(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrinterRegistry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinters()Ljava/util/List;

    move-result-object v2

    .line 1787
    .local v2, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v3, 0x0

    .line 1788
    .local v3, "updatedPrinter":Landroid/print/PrinterInfo;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrinterInfo;

    .line 1789
    .local v1, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {v1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mSConnectPrinter:Landroid/print/PrinterInfo;
    invoke-static {p1}, Lcom/android/printspooler/ui/PrintActivity;->access$2600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1790
    move-object v3, v1

    goto :goto_0

    .line 1793
    .end local v1    # "printer":Landroid/print/PrinterInfo;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1794
    if-eqz v3, :cond_3

    .line 1795
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1799
    :goto_1
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-direct {p0, v4, v2}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->addPrinters(Ljava/util/List;Ljava/util/Collection;)V

    .line 1803
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    .end local v3    # "updatedPrinter":Landroid/print/PrinterInfo;
    :cond_2
    :goto_2
    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;
    invoke-static {p1}, Lcom/android/printspooler/ui/PrintActivity;->access$2400(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrinterRegistry;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/android/printspooler/ui/PrinterRegistry;->setOnPrintersChangeListener(Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;)V

    .line 1804
    new-instance v4, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->createFakePdfPrinter()Landroid/print/PrinterInfo;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;-><init>(Landroid/print/PrinterInfo;)V

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mFakePdfPrinterHolder:Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 1805
    return-void

    .line 1797
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    .restart local v3    # "updatedPrinter":Landroid/print/PrinterInfo;
    :cond_3
    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mSConnectPrinter:Landroid/print/PrinterInfo;
    invoke-static {p1}, Lcom/android/printspooler/ui/PrintActivity;->access$2600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1800
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    .end local v3    # "updatedPrinter":Landroid/print/PrinterInfo;
    :cond_4
    iget-boolean v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mHistoricalPrintersLoaded:Z

    if-eqz v4, :cond_2

    .line 1801
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;
    invoke-static {p1}, Lcom/android/printspooler/ui/PrintActivity;->access$2400(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrinterRegistry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinters()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->addPrinters(Ljava/util/List;Ljava/util/Collection;)V

    goto :goto_2
.end method

.method private addPrinters(Ljava/util/List;Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2059
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;>;"
    .local p2, "printers":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/print/PrinterInfo;>;"
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrinterInfo;

    .line 2060
    .local v1, "printer":Landroid/print/PrinterInfo;
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->bSConnectPrintMode:Z
    invoke-static {v3}, Lcom/android/printspooler/ui/PrintActivity;->access$2500(Lcom/android/printspooler/ui/PrintActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v3

    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mSConnectPrinter:Landroid/print/PrinterInfo;
    invoke-static {v4}, Lcom/android/printspooler/ui/PrintActivity;->access$2600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2061
    :cond_1
    new-instance v2, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    invoke-direct {v2, v1}, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;-><init>(Landroid/print/PrinterInfo;)V

    .line 2062
    .local v2, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2065
    .end local v1    # "printer":Landroid/print/PrinterInfo;
    .end local v2    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_2
    return-void
.end method

.method private createFakePdfPrinter()Landroid/print/PrinterInfo;
    .locals 13

    .prologue
    const/16 v12, 0x12c

    const/4 v11, 0x1

    .line 2068
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-static {v8}, Lcom/android/printspooler/util/MediaSizeUtils;->getDefault(Landroid/content/Context;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v1

    .line 2070
    .local v1, "defaultMediaSize":Landroid/print/PrintAttributes$MediaSize;
    new-instance v7, Landroid/print/PrinterId;

    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v8}, Lcom/android/printspooler/ui/PrintActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v8

    const-string v9, "PDF printer"

    invoke-direct {v7, v8, v9}, Landroid/print/PrinterId;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 2072
    .local v7, "printerId":Landroid/print/PrinterId;
    new-instance v0, Landroid/print/PrinterCapabilitiesInfo$Builder;

    invoke-direct {v0, v7}, Landroid/print/PrinterCapabilitiesInfo$Builder;-><init>(Landroid/print/PrinterId;)V

    .line 2075
    .local v0, "builder":Landroid/print/PrinterCapabilitiesInfo$Builder;
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v8}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f040001

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 2076
    .local v6, "mediaSizeIds":[Ljava/lang/String;
    array-length v5, v6

    .line 2077
    .local v5, "mediaSizeIdCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_0

    .line 2078
    aget-object v3, v6, v2

    .line 2079
    .local v3, "id":Ljava/lang/String;
    invoke-static {v3}, Landroid/print/PrintAttributes$MediaSize;->getStandardMediaSizeById(Ljava/lang/String;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v4

    .line 2080
    .local v4, "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    invoke-virtual {v4, v1}, Landroid/print/PrintAttributes$MediaSize;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-virtual {v0, v4, v8}, Landroid/print/PrinterCapabilitiesInfo$Builder;->addMediaSize(Landroid/print/PrintAttributes$MediaSize;Z)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 2077
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2083
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    :cond_0
    new-instance v8, Landroid/print/PrintAttributes$Resolution;

    const-string v9, "PDF resolution"

    const-string v10, "PDF resolution"

    invoke-direct {v8, v9, v10, v12, v12}, Landroid/print/PrintAttributes$Resolution;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v0, v8, v11}, Landroid/print/PrinterCapabilitiesInfo$Builder;->addResolution(Landroid/print/PrintAttributes$Resolution;Z)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 2085
    const/4 v8, 0x3

    const/4 v9, 0x2

    invoke-virtual {v0, v8, v9}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setColorModes(II)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 2088
    new-instance v8, Landroid/print/PrinterInfo$Builder;

    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    const v10, 0x7f090017

    invoke-virtual {v9, v10}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v7, v9, v11}, Landroid/print/PrinterInfo$Builder;-><init>(Landroid/print/PrinterId;Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/print/PrinterCapabilitiesInfo$Builder;->build()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/print/PrinterInfo$Builder;->setCapabilities(Landroid/print/PrinterCapabilitiesInfo;)Landroid/print/PrinterInfo$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/print/PrinterInfo$Builder;->build()Landroid/print/PrinterInfo;

    move-result-object v8

    return-object v8
.end method


# virtual methods
.method public ensurePrinterInVisibleAdapterPosition(Landroid/print/PrinterId;)V
    .locals 6
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 1823
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 1824
    .local v2, "printerCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1825
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 1826
    .local v3, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-object v4, v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1828
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    if-ge v0, v4, :cond_1

    .line 1839
    .end local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_0
    :goto_1
    return-void

    .line 1832
    .restart local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_1
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getCount()I

    move-result v4

    add-int/lit8 v1, v4, -0x3

    .line 1833
    .local v1, "lastPrinterIndex":I
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1834
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v4, v1, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1835
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 1824
    .end local v1    # "lastPrinterIndex":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 1843
    iget-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mHistoricalPrintersLoaded:Z

    if-eqz v0, :cond_0

    .line 1844
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    const/16 v1, 0x9

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1846
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1901
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1902
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->isEnabled(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1903
    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 1862
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1863
    if-nez p1, :cond_3

    .line 1864
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mFakePdfPrinterHolder:Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 1877
    :goto_0
    return-object v0

    .line 1867
    :cond_0
    if-ge p1, v1, :cond_1

    .line 1868
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1870
    :cond_1
    if-ne p1, v1, :cond_2

    .line 1871
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mFakePdfPrinterHolder:Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    goto :goto_0

    .line 1873
    :cond_2
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_3

    .line 1874
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1877
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 6
    .param p1, "position"    # I

    .prologue
    const-wide/32 v0, 0x7fffffff

    const-wide/32 v2, 0x7ffffffe

    const/4 v5, 0x1

    .line 1882
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1883
    if-nez p1, :cond_1

    .line 1896
    :cond_0
    :goto_0
    return-wide v0

    .line 1885
    :cond_1
    if-ne p1, v5, :cond_3

    move-wide v0, v2

    .line 1886
    goto :goto_0

    .line 1889
    :cond_2
    if-eq p1, v5, :cond_0

    .line 1892
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    move-wide v0, v2

    .line 1893
    goto :goto_0

    .line 1896
    :cond_3
    int-to-long v0, p1

    goto :goto_0
.end method

.method public getPdfPrinter()Landroid/print/PrinterInfo;
    .locals 1

    .prologue
    .line 1808
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mFakePdfPrinterHolder:Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    return-object v0
.end method

.method public getPrinterHolder(Landroid/print/PrinterId;)Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    .locals 5
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 2035
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getCount()I

    move-result v2

    .line 2036
    .local v2, "itemCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 2037
    invoke-virtual {p0, v0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 2038
    .local v1, "item":Ljava/lang/Object;
    instance-of v4, v1, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    if-eqz v4, :cond_0

    move-object v3, v1

    .line 2039
    check-cast v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 2040
    .local v3, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-object v4, v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2045
    .end local v1    # "item":Ljava/lang/Object;
    .end local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :goto_1
    return-object v3

    .line 2036
    .restart local v1    # "item":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2045
    .end local v1    # "item":Ljava/lang/Object;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getPrinterIndex(Landroid/print/PrinterId;)I
    .locals 3
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 1812
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1813
    invoke-virtual {p0, v0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 1814
    .local v1, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->removed:Z

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v2}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1819
    .end local v0    # "i":I
    .end local v1    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :goto_1
    return v0

    .line 1812
    .restart local v0    # "i":I
    .restart local v1    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1819
    .end local v1    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1908
    if-nez p2, :cond_0

    .line 1909
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v8}, Lcom/android/printspooler/ui/PrintActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f030006

    const/4 v10, 0x0

    invoke-virtual {v8, v9, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1913
    :cond_0
    const/4 v6, 0x0

    .line 1914
    .local v6, "title":Ljava/lang/CharSequence;
    const/4 v4, 0x0

    .line 1915
    .local v4, "subtitle":Ljava/lang/CharSequence;
    const/4 v0, 0x0

    .line 1917
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1918
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPdfPrinter()Landroid/print/PrinterInfo;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 1919
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 1920
    .local v3, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-object v8, v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v8}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1921
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v8}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020017

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1946
    .end local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_1
    :goto_0
    const v8, 0x7f0d0021

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1947
    .local v7, "titleView":Landroid/widget/TextView;
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1949
    const v8, 0x7f0d0022

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1950
    .local v5, "subtitleView":Landroid/widget/TextView;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 1951
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1952
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1958
    :goto_1
    const v8, 0x7f0d0020

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1959
    .local v1, "iconView":Landroid/widget/ImageView;
    if-eqz v0, :cond_7

    .line 1960
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1961
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1966
    :goto_2
    return-object p2

    .line 1922
    .end local v1    # "iconView":Landroid/widget/ImageView;
    .end local v5    # "subtitleView":Landroid/widget/TextView;
    .end local v7    # "titleView":Landroid/widget/TextView;
    :cond_2
    const/4 v8, 0x1

    if-ne p1, v8, :cond_1

    .line 1923
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    const v9, 0x7f090018

    invoke-virtual {v8, v9}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1926
    :cond_3
    const/4 v8, 0x1

    if-ne p1, v8, :cond_4

    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPdfPrinter()Landroid/print/PrinterInfo;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 1927
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 1928
    .restart local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-object v8, v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v8}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1929
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v8}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020017

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1930
    goto :goto_0

    .end local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_4
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne p1, v8, :cond_5

    .line 1931
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    const v9, 0x7f090018

    invoke-virtual {v8, v9}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1933
    :cond_5
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 1934
    .restart local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-object v8, v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v8}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1936
    :try_start_0
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v8}, Lcom/android/printspooler/ui/PrintActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    iget-object v9, v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v9}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {v9}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 1938
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v8, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 1939
    iget-object v8, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 1954
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    .restart local v5    # "subtitleView":Landroid/widget/TextView;
    .restart local v7    # "titleView":Landroid/widget/TextView;
    :cond_6
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1955
    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1963
    .restart local v1    # "iconView":Landroid/widget/ImageView;
    :cond_7
    const/4 v8, 0x4

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1940
    .end local v1    # "iconView":Landroid/widget/ImageView;
    .end local v5    # "subtitleView":Landroid/widget/TextView;
    .end local v7    # "titleView":Landroid/widget/TextView;
    .restart local v3    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :catch_0
    move-exception v8

    goto/16 :goto_0
.end method

.method public isEnabled(I)Z
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    .line 1851
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1852
    .local v0, "item":Ljava/lang/Object;
    instance-of v3, v0, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    if-eqz v3, :cond_0

    move-object v1, v0

    .line 1853
    check-cast v1, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 1854
    .local v1, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-boolean v3, v1, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->removed:Z

    if-nez v3, :cond_1

    iget-object v3, v1, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v3}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    .line 1857
    .end local v1    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_0
    :goto_0
    return v2

    .line 1854
    .restart local v1    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onPrintersChanged(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1979
    .local p1, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;
    invoke-static {v11}, Lcom/android/printspooler/ui/PrintActivity;->access$2400(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrinterRegistry;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/printspooler/ui/PrinterRegistry;->areHistoricalPrintersLoaded()Z

    move-result v11

    iput-boolean v11, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mHistoricalPrintersLoaded:Z

    .line 1982
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1983
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-direct {p0, v11, p1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->addPrinters(Ljava/util/List;Ljava/util/Collection;)V

    .line 1984
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->notifyDataSetChanged()V

    .line 2026
    :goto_0
    return-void

    .line 1989
    :cond_0
    new-instance v4, Landroid/util/ArrayMap;

    invoke-direct {v4}, Landroid/util/ArrayMap;-><init>()V

    .line 1990
    .local v4, "newPrintersMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Landroid/print/PrinterId;Landroid/print/PrinterInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    .line 1991
    .local v8, "printerCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v8, :cond_1

    .line 1992
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/print/PrinterInfo;

    .line 1993
    .local v7, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {v7}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v11

    invoke-virtual {v4, v11, v7}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1991
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1996
    .end local v7    # "printer":Landroid/print/PrinterInfo;
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2001
    .local v3, "newPrinterHolders":Ljava/util/List;, "Ljava/util/List<Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;>;"
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v5

    .line 2002
    .local v5, "oldPrinterCount":I
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v5, :cond_3

    .line 2003
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 2004
    .local v9, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-object v11, v9, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v11}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v6

    .line 2005
    .local v6, "oldPrinterId":Landroid/print/PrinterId;
    invoke-virtual {v4, v6}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/print/PrinterInfo;

    .line 2006
    .local v10, "updatedPrinter":Landroid/print/PrinterInfo;
    if-eqz v10, :cond_2

    .line 2007
    iput-object v10, v9, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    .line 2011
    :goto_3
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2002
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2009
    :cond_2
    const/4 v11, 0x1

    iput-boolean v11, v9, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->removed:Z

    goto :goto_3

    .line 2014
    .end local v6    # "oldPrinterId":Landroid/print/PrinterId;
    .end local v9    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    .end local v10    # "updatedPrinter":Landroid/print/PrinterInfo;
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 2015
    .local v0, "holder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-object v11, v0, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    invoke-virtual {v11}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v11

    if-eqz v11, :cond_4

    .line 2016
    const/4 v11, 0x0

    iput-boolean v11, v0, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->removed:Z

    goto :goto_4

    .line 2020
    .end local v0    # "holder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_5
    invoke-virtual {v4}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v11

    invoke-direct {p0, v3, v11}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->addPrinters(Ljava/util/List;Ljava/util/Collection;)V

    .line 2022
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 2023
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v11, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2025
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onPrintersInvalid()V
    .locals 1

    .prologue
    .line 2030
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2031
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->notifyDataSetInvalidated()V

    .line 2032
    return-void
.end method

.method public pruneRemovedPrinters()V
    .locals 4

    .prologue
    .line 2049
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 2050
    .local v0, "holderCounts":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 2051
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 2052
    .local v2, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-boolean v3, v2, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->removed:Z

    if-eqz v3, :cond_0

    .line 2053
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->mPrinterHolders:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2050
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2056
    .end local v2    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_1
    return-void
.end method
