.class final Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EditTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0

    .prologue
    .line 2344
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p2, "x1"    # Lcom/android/printspooler/ui/PrintActivity$1;

    .prologue
    .line 2344
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 2357
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z
    invoke-static {v2}, Lcom/android/printspooler/ui/PrintActivity;->access$4400(Lcom/android/printspooler/ui/PrintActivity;)Z

    move-result v1

    .line 2359
    .local v1, "hadErrors":Z
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 2360
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/android/printspooler/ui/PrintActivity;->access$5000(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2361
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v2}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 2387
    :cond_0
    :goto_0
    return-void

    .line 2365
    :cond_1
    const/4 v0, 0x0

    .line 2367
    .local v0, "copies":I
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2372
    :goto_1
    const/4 v2, 0x1

    if-ge v0, v2, :cond_2

    .line 2373
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/android/printspooler/ui/PrintActivity;->access$5000(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2374
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v2}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    goto :goto_0

    .line 2378
    :cond_2
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;
    invoke-static {v2}, Lcom/android/printspooler/ui/PrintActivity;->access$3400(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrintJobInfo;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/print/PrintJobInfo;->setCopies(I)V

    .line 2380
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/android/printspooler/ui/PrintActivity;->access$5000(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2382
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v2}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 2384
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z
    invoke-static {v2}, Lcom/android/printspooler/ui/PrintActivity;->access$3000(Lcom/android/printspooler/ui/PrintActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2385
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    const/4 v3, 0x0

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z
    invoke-static {v2, v3}, Lcom/android/printspooler/ui/PrintActivity;->access$3100(Lcom/android/printspooler/ui/PrintActivity;Z)Z

    goto :goto_0

    .line 2368
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 2353
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 2348
    return-void
.end method
