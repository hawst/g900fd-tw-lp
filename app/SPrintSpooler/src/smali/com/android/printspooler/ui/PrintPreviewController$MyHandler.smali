.class final Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;
.super Landroid/os/Handler;
.source "PrintPreviewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintPreviewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyHandler"
.end annotation


# instance fields
.field private mAsyncOperationInProgress:Z

.field private final mOnAsyncOperationDoneCallback:Ljava/lang/Runnable;

.field private final mPendingOperations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/printspooler/ui/PrintPreviewController;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/ui/PrintPreviewController;Landroid/os/Looper;)V
    .locals 2
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    .line 251
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 240
    new-instance v0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$1;

    invoke-direct {v0, p0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$1;-><init>(Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mOnAsyncOperationDoneCallback:Ljava/lang/Runnable;

    .line 248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mPendingOperations:Ljava/util/List;

    .line 252
    return-void
.end method

.method static synthetic access$202(Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 231
    iput-boolean p1, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mAsyncOperationInProgress:Z

    return p1
.end method

.method static synthetic access$600(Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mOnAsyncOperationDoneCallback:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public enqueueOperation(Landroid/os/Message;)V
    .locals 1
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mPendingOperations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->handleNextOperation()V

    .line 257
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x1

    .line 268
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 327
    :goto_0
    return-void

    .line 271
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$300(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/model/MutexFileProvider;

    move-result-object v0

    iget-object v10, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v0, v10}, Lcom/android/printspooler/model/MutexFileProvider;->acquireFile(Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;)Ljava/io/File;

    move-result-object v8

    .line 272
    .local v8, "file":Ljava/io/File;
    const/high16 v0, 0x10000000

    invoke-static {v8, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v9

    .line 275
    .local v9, "pfd":Landroid/os/ParcelFileDescriptor;
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mAsyncOperationInProgress:Z

    .line 276
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$000(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PageAdapter;

    move-result-object v0

    new-instance v10, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;

    invoke-direct {v10, p0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;-><init>(Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;)V

    invoke-virtual {v0, v9, v10}, Lcom/android/printspooler/ui/PageAdapter;->open(Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 286
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "pfd":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v0

    goto :goto_0

    .line 292
    :pswitch_1
    iput-boolean v10, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mAsyncOperationInProgress:Z

    .line 293
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$000(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PageAdapter;

    move-result-object v0

    new-instance v10, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$3;

    invoke-direct {v10, p0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$3;-><init>(Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;)V

    invoke-virtual {v0, v10}, Lcom/android/printspooler/ui/PageAdapter;->close(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 303
    :pswitch_2
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Runnable;

    .line 304
    .local v7, "callback":Ljava/lang/Runnable;
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$700(Lcom/android/printspooler/ui/PrintPreviewController;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 305
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$000(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PageAdapter;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/android/printspooler/ui/PageAdapter;->destroy(Ljava/lang/Runnable;)V

    .line 306
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->handleNextOperation()V

    goto :goto_0

    .line 310
    .end local v7    # "callback":Ljava/lang/Runnable;
    :pswitch_3
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lcom/android/internal/os/SomeArgs;

    .line 311
    .local v6, "args":Lcom/android/internal/os/SomeArgs;
    iget-object v0, v6, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    check-cast v0, [Landroid/print/PageRange;

    move-object v1, v0

    check-cast v1, [Landroid/print/PageRange;

    .line 312
    .local v1, "writtenPages":[Landroid/print/PageRange;
    iget-object v0, v6, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    check-cast v0, [Landroid/print/PageRange;

    move-object v2, v0

    check-cast v2, [Landroid/print/PageRange;

    .line 313
    .local v2, "selectedPages":[Landroid/print/PageRange;
    iget-object v4, v6, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    check-cast v4, Landroid/print/PrintAttributes$MediaSize;

    .line 314
    .local v4, "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    iget-object v5, v6, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    check-cast v5, Landroid/print/PrintAttributes$Margins;

    .line 315
    .local v5, "margins":Landroid/print/PrintAttributes$Margins;
    iget v3, v6, Lcom/android/internal/os/SomeArgs;->argi1:I

    .line 316
    .local v3, "pageCount":I
    invoke-virtual {v6}, Lcom/android/internal/os/SomeArgs;->recycle()V

    .line 318
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$000(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PageAdapter;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Lcom/android/printspooler/ui/PageAdapter;->update([Landroid/print/PageRange;[Landroid/print/PageRange;ILandroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V

    goto :goto_0

    .line 324
    .end local v1    # "writtenPages":[Landroid/print/PageRange;
    .end local v2    # "selectedPages":[Landroid/print/PageRange;
    .end local v3    # "pageCount":I
    .end local v4    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v5    # "margins":Landroid/print/PrintAttributes$Margins;
    .end local v6    # "args":Lcom/android/internal/os/SomeArgs;
    :pswitch_4
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mPreloadController:Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$800(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->startPreloadContent()V

    goto/16 :goto_0

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public handleNextOperation()V
    .locals 3

    .prologue
    .line 260
    :goto_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mPendingOperations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mAsyncOperationInProgress:Z

    if-nez v1, :cond_0

    .line 261
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mPendingOperations:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 262
    .local v0, "operation":Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 264
    .end local v0    # "operation":Landroid/os/Message;
    :cond_0
    return-void
.end method
