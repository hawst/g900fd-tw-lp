.class public final Lcom/android/printspooler/ui/PageAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "PageAdapter.java"

# interfaces
.implements Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/ui/PageAdapter$PageClickListener;,
        Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;,
        Lcom/android/printspooler/ui/PageAdapter$PreviewArea;,
        Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;
    }
.end annotation


# static fields
.field private static final ALL_PAGES_ARRAY:[Landroid/print/PageRange;


# instance fields
.field private final mBoundPagesInAdapter:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbacks:Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;

.field private final mCloseGuard:Ldalvik/system/CloseGuard;

.field private mColorMode:I

.field private mColumnCount:I

.field private final mConfirmedPagesInDocument:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mDocumentPageCount:I

.field private mEmptyState:Landroid/graphics/drawable/BitmapDrawable;

.field private mFooterHeight:I

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private mMediaSize:Landroid/print/PrintAttributes$MediaSize;

.field private mMinMargins:Landroid/print/PrintAttributes$Margins;

.field private final mPageClickListener:Lcom/android/printspooler/ui/PageAdapter$PageClickListener;

.field private mPageContentHeight:I

.field private final mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

.field private mPageContentWidth:I

.field private final mPreviewArea:Lcom/android/printspooler/ui/PageAdapter$PreviewArea;

.field private mPreviewListPadding:I

.field private mPreviewPageMargin:I

.field private mPreviewPageMinWidth:I

.field private mRequestedPages:[Landroid/print/PageRange;

.field private mSelectedPageCount:I

.field private mSelectedPages:[Landroid/print/PageRange;

.field private mState:I

.field private mWrittenPages:[Landroid/print/PageRange;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/print/PageRange;

    const/4 v1, 0x0

    sget-object v2, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/printspooler/ui/PageAdapter;->ALL_PAGES_ARRAY:[Landroid/print/PageRange;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;Lcom/android/printspooler/ui/PageAdapter$PreviewArea;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;
    .param p3, "previewArea"    # Lcom/android/printspooler/ui/PageAdapter$PreviewArea;

    .prologue
    .line 129
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 73
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mCloseGuard:Ldalvik/system/CloseGuard;

    .line 75
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mBoundPagesInAdapter:Landroid/util/SparseArray;

    .line 76
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;

    .line 78
    new-instance v0, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/printspooler/ui/PageAdapter$PageClickListener;-><init>(Lcom/android/printspooler/ui/PageAdapter;Lcom/android/printspooler/ui/PageAdapter$1;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageClickListener:Lcom/android/printspooler/ui/PageAdapter$PageClickListener;

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    .line 114
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mColorMode:I

    .line 130
    iput-object p1, p0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    .line 131
    iput-object p2, p0, Lcom/android/printspooler/ui/PageAdapter;->mCallbacks:Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;

    .line 132
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 134
    new-instance v0, Lcom/android/printspooler/model/PageContentRepository;

    invoke-direct {v0, p1, p0}, Lcom/android/printspooler/model/PageContentRepository;-><init>(Landroid/content/Context;Lcom/android/printspooler/model/PageContentRepository$OnMalformedPdfFileListener;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    .line 136
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewPageMargin:I

    .line 139
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewPageMinWidth:I

    .line 142
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewListPadding:I

    .line 145
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mColumnCount:I

    .line 148
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mFooterHeight:I

    .line 151
    iput-object p3, p0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewArea:Lcom/android/printspooler/ui/PageAdapter$PreviewArea;

    .line 153
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mCloseGuard:Ldalvik/system/CloseGuard;

    const-string v1, "destroy"

    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/printspooler/ui/PageAdapter;->setHasStableIds(Z)V

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    .line 159
    const-string v0, "PageAdapter"

    const-string v1, "STATE_CLOSED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    return-void
.end method

.method static synthetic access$200(Lcom/android/printspooler/ui/PageAdapter;I)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PageAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInDocument(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/android/printspooler/ui/PageAdapter;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PageAdapter;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;

    return-object v0
.end method

.method private computeBoundPagesInDocument()[Landroid/print/PageRange;
    .locals 12

    .prologue
    const/4 v10, -0x1

    .line 692
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 694
    .local v7, "pagesInDocumentList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    const/4 v3, -0x1

    .line 695
    .local v3, "fromPage":I
    const/4 v8, -0x1

    .line 697
    .local v8, "toPage":I
    iget-object v9, p0, Lcom/android/printspooler/ui/PageAdapter;->mBoundPagesInAdapter:Landroid/util/SparseArray;

    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 698
    .local v0, "boundPageCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v0, :cond_3

    .line 700
    iget-object v9, p0, Lcom/android/printspooler/ui/PageAdapter;->mBoundPagesInAdapter:Landroid/util/SparseArray;

    invoke-virtual {v9, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 701
    .local v1, "boundPageInAdapter":I
    invoke-direct {p0, v1}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInDocument(I)I

    move-result v2

    .line 703
    .local v2, "boundPageInDocument":I
    if-ne v3, v10, :cond_0

    .line 704
    move v3, v2

    .line 707
    :cond_0
    if-ne v8, v10, :cond_1

    .line 708
    move v8, v2

    .line 711
    :cond_1
    add-int/lit8 v9, v8, 0x1

    if-le v2, v9, :cond_2

    .line 712
    new-instance v6, Landroid/print/PageRange;

    invoke-direct {v6, v3, v8}, Landroid/print/PageRange;-><init>(II)V

    .line 713
    .local v6, "pageRange":Landroid/print/PageRange;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 714
    move v8, v2

    move v3, v2

    .line 698
    .end local v6    # "pageRange":Landroid/print/PageRange;
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 716
    :cond_2
    move v8, v2

    goto :goto_1

    .line 720
    .end local v1    # "boundPageInAdapter":I
    .end local v2    # "boundPageInDocument":I
    :cond_3
    if-eq v3, v10, :cond_4

    if-eq v8, v10, :cond_4

    .line 721
    new-instance v6, Landroid/print/PageRange;

    invoke-direct {v6, v3, v8}, Landroid/print/PageRange;-><init>(II)V

    .line 722
    .restart local v6    # "pageRange":Landroid/print/PageRange;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 725
    .end local v6    # "pageRange":Landroid/print/PageRange;
    :cond_4
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    new-array v5, v9, [Landroid/print/PageRange;

    .line 726
    .local v5, "pageInDocument":[Landroid/print/PageRange;
    invoke-interface {v7, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 729
    const-string v9, "PageAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Bound pages: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    return-object v5
.end method

.method private computePageIndexInDocument(I)I
    .locals 7
    .param p1, "indexInAdapter"    # I

    .prologue
    .line 520
    const/4 v4, 0x0

    .line 521
    .local v4, "skippedAdapterPages":I
    iget-object v5, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    array-length v3, v5

    .line 522
    .local v3, "selectedPagesCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 523
    iget-object v5, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    aget-object v5, v5, v0

    iget v6, p0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    invoke-static {v5, v6}, Lcom/android/printspooler/util/PageRangeUtils;->asAbsoluteRange(Landroid/print/PageRange;I)Landroid/print/PageRange;

    move-result-object v2

    .line 525
    .local v2, "pageRange":Landroid/print/PageRange;
    invoke-virtual {v2}, Landroid/print/PageRange;->getSize()I

    move-result v5

    add-int/2addr v4, v5

    .line 526
    if-le v4, p1, :cond_0

    .line 527
    sub-int v5, v4, p1

    add-int/lit8 v1, v5, -0x1

    .line 528
    .local v1, "overshoot":I
    invoke-virtual {v2}, Landroid/print/PageRange;->getEnd()I

    move-result v5

    sub-int/2addr v5, v1

    .line 531
    .end local v1    # "overshoot":I
    .end local v2    # "pageRange":Landroid/print/PageRange;
    :goto_1
    return v5

    .line 522
    .restart local v2    # "pageRange":Landroid/print/PageRange;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 531
    .end local v2    # "pageRange":Landroid/print/PageRange;
    :cond_1
    const/4 v5, -0x1

    goto :goto_1
.end method

.method private computePageIndexInFile(I)I
    .locals 6
    .param p1, "pageIndexInDocument"    # I

    .prologue
    const/4 v4, -0x1

    .line 535
    iget-object v5, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    invoke-static {v5, p1}, Lcom/android/printspooler/util/PageRangeUtils;->contains([Landroid/print/PageRange;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v1, v4

    .line 553
    :goto_0
    return v1

    .line 538
    :cond_0
    iget-object v5, p0, Lcom/android/printspooler/ui/PageAdapter;->mWrittenPages:[Landroid/print/PageRange;

    if-nez v5, :cond_1

    move v1, v4

    .line 539
    goto :goto_0

    .line 542
    :cond_1
    const/4 v1, -0x1

    .line 543
    .local v1, "indexInFile":I
    iget-object v5, p0, Lcom/android/printspooler/ui/PageAdapter;->mWrittenPages:[Landroid/print/PageRange;

    array-length v3, v5

    .line 544
    .local v3, "rangeCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_3

    .line 545
    iget-object v5, p0, Lcom/android/printspooler/ui/PageAdapter;->mWrittenPages:[Landroid/print/PageRange;

    aget-object v2, v5, v0

    .line 546
    .local v2, "pageRange":Landroid/print/PageRange;
    invoke-virtual {v2, p1}, Landroid/print/PageRange;->contains(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 547
    invoke-virtual {v2}, Landroid/print/PageRange;->getSize()I

    move-result v5

    add-int/2addr v1, v5

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 549
    :cond_2
    invoke-virtual {v2}, Landroid/print/PageRange;->getStart()I

    move-result v4

    sub-int v4, p1, v4

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v1, v4

    .line 550
    goto :goto_0

    .end local v2    # "pageRange":Landroid/print/PageRange;
    :cond_3
    move v1, v4

    .line 553
    goto :goto_0
.end method

.method private computeRequestedPages(I)[Landroid/print/PageRange;
    .locals 18
    .param p1, "pageInDocument"    # I

    .prologue
    .line 580
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    if-eqz v15, :cond_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    move/from16 v0, p1

    invoke-static {v15, v0}, Lcom/android/printspooler/util/PageRangeUtils;->contains([Landroid/print/PageRange;I)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 582
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    .line 688
    :goto_0
    return-object v15

    .line 585
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 587
    .local v7, "pageRangesList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    const/16 v10, 0x32

    .line 588
    .local v10, "remainingPagesToRequest":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    array-length v12, v15

    .line 591
    .local v12, "selectedPagesCount":I
    invoke-direct/range {p0 .. p0}, Lcom/android/printspooler/ui/PageAdapter;->computeBoundPagesInDocument()[Landroid/print/PageRange;

    move-result-object v1

    .line 593
    .local v1, "boundPagesInDocument":[Landroid/print/PageRange;
    array-length v3, v1

    .line 594
    .local v3, "boundRangeCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v3, :cond_1

    .line 595
    aget-object v2, v1, v5

    .line 596
    .local v2, "boundRange":Landroid/print/PageRange;
    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 594
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 598
    .end local v2    # "boundRange":Landroid/print/PageRange;
    :cond_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    invoke-static {v1, v15}, Lcom/android/printspooler/util/PageRangeUtils;->getNormalizedPageCount([Landroid/print/PageRange;I)I

    move-result v15

    sub-int/2addr v10, v15

    .line 601
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    aget-object v15, v15, v16

    invoke-virtual {v15}, Landroid/print/PageRange;->getEnd()I

    move-result v15

    move/from16 v0, p1

    if-le v0, v15, :cond_4

    :cond_2
    const/4 v11, 0x1

    .line 604
    .local v11, "requestFromStart":Z
    :goto_2
    if-nez v11, :cond_8

    .line 606
    const-string v15, "PageAdapter"

    const-string v16, "Requesting from end"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    add-int/lit8 v5, v12, -0x1

    :goto_3
    if-ltz v5, :cond_3

    .line 611
    if-gtz v10, :cond_5

    .line 685
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v15

    new-array v6, v15, [Landroid/print/PageRange;

    .line 686
    .local v6, "pageRanges":[Landroid/print/PageRange;
    invoke-interface {v7, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 688
    invoke-static {v6}, Lcom/android/printspooler/util/PageRangeUtils;->normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;

    move-result-object v15

    goto :goto_0

    .line 601
    .end local v6    # "pageRanges":[Landroid/print/PageRange;
    .end local v11    # "requestFromStart":Z
    :cond_4
    const/4 v11, 0x0

    goto :goto_2

    .line 615
    .restart local v11    # "requestFromStart":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    aget-object v15, v15, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    move/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/android/printspooler/util/PageRangeUtils;->asAbsoluteRange(Landroid/print/PageRange;I)Landroid/print/PageRange;

    move-result-object v13

    .line 617
    .local v13, "selectedRange":Landroid/print/PageRange;
    invoke-virtual {v13}, Landroid/print/PageRange;->getStart()I

    move-result v15

    move/from16 v0, p1

    if-ge v0, v15, :cond_6

    .line 610
    :goto_4
    add-int/lit8 v5, v5, -0x1

    goto :goto_3

    .line 624
    :cond_6
    move/from16 v0, p1

    invoke-virtual {v13, v0}, Landroid/print/PageRange;->contains(I)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 625
    invoke-virtual {v13}, Landroid/print/PageRange;->getStart()I

    move-result v15

    sub-int v15, p1, v15

    add-int/lit8 v9, v15, 0x1

    .line 626
    .local v9, "rangeSpan":I
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 627
    sub-int v15, p1, v9

    add-int/lit8 v15, v15, -0x1

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 628
    .local v4, "fromPage":I
    const/4 v15, 0x0

    invoke-static {v9, v15}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 629
    new-instance v8, Landroid/print/PageRange;

    move/from16 v0, p1

    invoke-direct {v8, v4, v0}, Landroid/print/PageRange;-><init>(II)V

    .line 639
    .local v8, "pagesInRange":Landroid/print/PageRange;
    :goto_5
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 640
    sub-int/2addr v10, v9

    goto :goto_4

    .line 631
    .end local v4    # "fromPage":I
    .end local v8    # "pagesInRange":Landroid/print/PageRange;
    .end local v9    # "rangeSpan":I
    :cond_7
    invoke-virtual {v13}, Landroid/print/PageRange;->getSize()I

    move-result v9

    .line 632
    .restart local v9    # "rangeSpan":I
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 633
    const/4 v15, 0x0

    invoke-static {v9, v15}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 634
    invoke-virtual {v13}, Landroid/print/PageRange;->getEnd()I

    move-result v15

    sub-int/2addr v15, v9

    add-int/lit8 v15, v15, -0x1

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 635
    .restart local v4    # "fromPage":I
    invoke-virtual {v13}, Landroid/print/PageRange;->getEnd()I

    move-result v14

    .line 636
    .local v14, "toPage":I
    new-instance v8, Landroid/print/PageRange;

    invoke-direct {v8, v4, v14}, Landroid/print/PageRange;-><init>(II)V

    .restart local v8    # "pagesInRange":Landroid/print/PageRange;
    goto :goto_5

    .line 644
    .end local v4    # "fromPage":I
    .end local v8    # "pagesInRange":Landroid/print/PageRange;
    .end local v9    # "rangeSpan":I
    .end local v13    # "selectedRange":Landroid/print/PageRange;
    .end local v14    # "toPage":I
    :cond_8
    const-string v15, "PageAdapter"

    const-string v16, "Requesting from start"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    const/4 v5, 0x0

    :goto_6
    if-ge v5, v12, :cond_3

    .line 649
    if-lez v10, :cond_3

    .line 653
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    aget-object v15, v15, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    move/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/android/printspooler/util/PageRangeUtils;->asAbsoluteRange(Landroid/print/PageRange;I)Landroid/print/PageRange;

    move-result-object v13

    .line 655
    .restart local v13    # "selectedRange":Landroid/print/PageRange;
    invoke-virtual {v13}, Landroid/print/PageRange;->getEnd()I

    move-result v15

    move/from16 v0, p1

    if-le v0, v15, :cond_9

    .line 648
    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 662
    :cond_9
    move/from16 v0, p1

    invoke-virtual {v13, v0}, Landroid/print/PageRange;->contains(I)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 663
    invoke-virtual {v13}, Landroid/print/PageRange;->getEnd()I

    move-result v15

    sub-int v15, v15, p1

    add-int/lit8 v9, v15, 0x1

    .line 664
    .restart local v9    # "rangeSpan":I
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 665
    add-int v15, p1, v9

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 667
    .restart local v14    # "toPage":I
    new-instance v8, Landroid/print/PageRange;

    move/from16 v0, p1

    invoke-direct {v8, v0, v14}, Landroid/print/PageRange;-><init>(II)V

    .line 678
    .restart local v8    # "pagesInRange":Landroid/print/PageRange;
    :goto_8
    const-string v15, "PageAdapter"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "computeRequestedPages() Adding range:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 681
    sub-int/2addr v10, v9

    goto :goto_7

    .line 669
    .end local v8    # "pagesInRange":Landroid/print/PageRange;
    .end local v9    # "rangeSpan":I
    .end local v14    # "toPage":I
    :cond_a
    invoke-virtual {v13}, Landroid/print/PageRange;->getSize()I

    move-result v9

    .line 670
    .restart local v9    # "rangeSpan":I
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 671
    invoke-virtual {v13}, Landroid/print/PageRange;->getStart()I

    move-result v4

    .line 672
    .restart local v4    # "fromPage":I
    invoke-virtual {v13}, Landroid/print/PageRange;->getStart()I

    move-result v15

    add-int/2addr v15, v9

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 674
    .restart local v14    # "toPage":I
    new-instance v8, Landroid/print/PageRange;

    invoke-direct {v8, v4, v14}, Landroid/print/PageRange;-><init>(II)V

    .restart local v8    # "pagesInRange":Landroid/print/PageRange;
    goto :goto_8
.end method

.method private computeSelectedPages()[Landroid/print/PageRange;
    .locals 10

    .prologue
    const/4 v9, -0x1

    .line 471
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 473
    .local v6, "selectedPagesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/print/PageRange;>;"
    const/4 v7, -0x1

    .line 474
    .local v7, "startPageIndex":I
    const/4 v0, -0x1

    .line 476
    .local v0, "endPageIndex":I
    iget-object v8, p0, Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 477
    .local v2, "pageCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 478
    iget-object v8, p0, Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;

    invoke-virtual {v8, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 479
    .local v3, "pageIndex":I
    if-ne v7, v9, :cond_0

    .line 480
    move v0, v3

    move v7, v3

    .line 482
    :cond_0
    add-int/lit8 v8, v0, 0x1

    if-ge v8, v3, :cond_1

    .line 483
    new-instance v4, Landroid/print/PageRange;

    invoke-direct {v4, v7, v0}, Landroid/print/PageRange;-><init>(II)V

    .line 484
    .local v4, "pageRange":Landroid/print/PageRange;
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    move v7, v3

    .line 487
    .end local v4    # "pageRange":Landroid/print/PageRange;
    :cond_1
    move v0, v3

    .line 477
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 490
    .end local v3    # "pageIndex":I
    :cond_2
    if-eq v7, v9, :cond_3

    if-eq v0, v9, :cond_3

    .line 492
    new-instance v4, Landroid/print/PageRange;

    invoke-direct {v4, v7, v0}, Landroid/print/PageRange;-><init>(II)V

    .line 493
    .restart local v4    # "pageRange":Landroid/print/PageRange;
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    .end local v4    # "pageRange":Landroid/print/PageRange;
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v5, v8, [Landroid/print/PageRange;

    .line 497
    .local v5, "selectedPages":[Landroid/print/PageRange;
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 499
    return-object v5
.end method

.method private doDestroy(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 760
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    invoke-virtual {v0, p1}, Lcom/android/printspooler/model/PageContentRepository;->destroy(Ljava/lang/Runnable;)V

    .line 761
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mCloseGuard:Ldalvik/system/CloseGuard;

    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    .line 762
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    .line 764
    const-string v0, "PageAdapter"

    const-string v1, "STATE_DESTROYED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    return-void
.end method

.method private onSelectedPageNotInFile(I)V
    .locals 4
    .param p1, "pageInDocument"    # I

    .prologue
    .line 569
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PageAdapter;->computeRequestedPages(I)[Landroid/print/PageRange;

    move-result-object v0

    .line 570
    .local v0, "requestedPages":[Landroid/print/PageRange;
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 571
    iput-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    .line 573
    const-string v1, "PageAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requesting pages: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mCallbacks:Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;

    invoke-interface {v1}, Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;->onRequestContentUpdate()V

    .line 577
    :cond_0
    return-void
.end method

.method private recyclePageView(Lcom/android/printspooler/widget/PageContentView;I)V
    .locals 5
    .param p1, "page"    # Lcom/android/printspooler/widget/PageContentView;
    .param p2, "pageIndexInAdapter"    # I

    .prologue
    const/4 v4, 0x0

    .line 736
    invoke-virtual {p1}, Lcom/android/printspooler/widget/PageContentView;->getPageContentProvider()Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    move-result-object v0

    .line 737
    .local v0, "provider":Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;
    if-eqz v0, :cond_0

    .line 738
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mEmptyState:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    iget-object v3, p0, Lcom/android/printspooler/ui/PageAdapter;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    invoke-virtual {p1, v4, v1, v2, v3}, Lcom/android/printspooler/widget/PageContentView;->init(Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;Landroid/graphics/drawable/Drawable;Landroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V

    .line 739
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    invoke-virtual {v1, v0}, Lcom/android/printspooler/model/PageContentRepository;->releasePageContentProvider(Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;)V

    .line 741
    :cond_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mBoundPagesInAdapter:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 742
    invoke-virtual {p1, v4}, Lcom/android/printspooler/widget/PageContentView;->setTag(Ljava/lang/Object;)V

    .line 743
    return-void
.end method

.method private setConfirmedPages([Landroid/print/PageRange;I)V
    .locals 6
    .param p1, "pagesInDocument"    # [Landroid/print/PageRange;
    .param p2, "documentPageCount"    # I

    .prologue
    .line 557
    iget-object v4, p0, Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->clear()V

    .line 558
    array-length v3, p1

    .line 559
    .local v3, "rangeCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 560
    aget-object v4, p1, v0

    invoke-static {v4, p2}, Lcom/android/printspooler/util/PageRangeUtils;->asAbsoluteRange(Landroid/print/PageRange;I)Landroid/print/PageRange;

    move-result-object v2

    .line 562
    .local v2, "pageRange":Landroid/print/PageRange;
    invoke-virtual {v2}, Landroid/print/PageRange;->getStart()I

    move-result v1

    .local v1, "j":I
    :goto_1
    invoke-virtual {v2}, Landroid/print/PageRange;->getEnd()I

    move-result v4

    if-gt v1, v4, :cond_0

    .line 563
    iget-object v4, p0, Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 562
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 559
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 566
    .end local v1    # "j":I
    .end local v2    # "pageRange":Landroid/print/PageRange;
    :cond_1
    return-void
.end method

.method private throwIfNotClosed()V
    .locals 2

    .prologue
    .line 775
    iget v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    if-eqz v0, :cond_0

    .line 776
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 778
    :cond_0
    return-void
.end method

.method private throwIfNotOpened()V
    .locals 2

    .prologue
    .line 769
    iget v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 770
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not opened"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 772
    :cond_0
    return-void
.end method

.method private updatePreviewAreaPageSizeAndEmptyState()V
    .locals 27

    .prologue
    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    move-object/from16 v23, v0

    if-nez v23, :cond_0

    .line 468
    :goto_0
    return-void

    .line 400
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewArea:Lcom/android/printspooler/ui/PageAdapter$PreviewArea;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/android/printspooler/ui/PageAdapter$PreviewArea;->getWidth()I

    move-result v5

    .line 401
    .local v5, "availableWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewArea:Lcom/android/printspooler/ui/PageAdapter$PreviewArea;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/android/printspooler/ui/PageAdapter$PreviewArea;->getHeight()I

    move-result v4

    .line 404
    .local v4, "availableHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/print/PrintAttributes$MediaSize;->getWidthMils()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/print/PrintAttributes$MediaSize;->getHeightMils()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v14, v23, v24

    .line 408
    .local v14, "pageAspectRatio":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPageCount:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mColumnCount:I

    move/from16 v24, v0

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 409
    .local v8, "columnCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewArea:Lcom/android/printspooler/ui/PageAdapter$PreviewArea;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-interface {v0, v8}, Lcom/android/printspooler/ui/PageAdapter$PreviewArea;->setColumnCount(I)V

    .line 412
    mul-int/lit8 v23, v8, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewPageMargin:I

    move/from16 v24, v0

    mul-int v10, v23, v24

    .line 413
    .local v10, "horizontalMargins":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewListPadding:I

    move/from16 v23, v0

    mul-int/lit8 v23, v23, 0x2

    add-int v12, v10, v23

    .line 414
    .local v12, "horizontalPaddingAndMargins":I
    int-to-float v0, v5

    move/from16 v23, v0

    int-to-float v0, v12

    move/from16 v24, v0

    sub-float v23, v23, v24

    int-to-float v0, v8

    move/from16 v24, v0

    div-float v23, v23, v24

    const/high16 v24, 0x3f000000    # 0.5f

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v16, v0

    .line 418
    .local v16, "pageContentDesiredWidth":I
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v23, v23, v14

    const/high16 v24, 0x3f000000    # 0.5f

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v15, v0

    .line 423
    .local v15, "pageContentDesiredHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewPageMinWidth:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v23, v23, v14

    const/high16 v24, 0x3f000000    # 0.5f

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v18, v0

    .line 424
    .local v18, "pageContentMinHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewListPadding:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewPageMargin:I

    move/from16 v24, v0

    add-int v23, v23, v24

    mul-int/lit8 v23, v23, 0x2

    sub-int v23, v4, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mFooterHeight:I

    move/from16 v24, v0

    sub-int v23, v23, v24

    move/from16 v0, v18

    move/from16 v1, v23

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 427
    .local v17, "pageContentMaxHeight":I
    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->min(II)I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/printspooler/ui/PageAdapter;->mPageContentHeight:I

    .line 428
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v14

    const/high16 v24, 0x3f000000    # 0.5f

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/printspooler/ui/PageAdapter;->mPageContentWidth:I

    .line 430
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentWidth:I

    move/from16 v23, v0

    mul-int v23, v23, v8

    add-int v21, v23, v10

    .line 431
    .local v21, "totalContentWidth":I
    sub-int v23, v5, v21

    div-int/lit8 v11, v23, 0x2

    .line 433
    .local v11, "horizontalPadding":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPageCount:I

    move/from16 v23, v0

    div-int v24, v23, v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPageCount:I

    move/from16 v23, v0

    rem-int v23, v23, v8

    if-lez v23, :cond_1

    const/16 v23, 0x1

    :goto_1
    add-int v19, v24, v23

    .line 435
    .local v19, "rowCount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentHeight:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mFooterHeight:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewPageMargin:I

    move/from16 v24, v0

    mul-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    mul-int v20, v19, v23

    .line 439
    .local v20, "totalContentHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentHeight:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mFooterHeight:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewListPadding:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewPageMargin:I

    move/from16 v24, v0

    mul-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    move/from16 v0, v23

    if-le v0, v4, :cond_2

    .line 441
    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentHeight:I

    move/from16 v24, v0

    sub-int v24, v4, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mFooterHeight:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    div-int/lit8 v24, v24, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewPageMargin:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 449
    .local v22, "verticalPadding":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewArea:Lcom/android/printspooler/ui/PageAdapter$PreviewArea;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v22

    move/from16 v2, v22

    invoke-interface {v0, v11, v1, v11, v2}, Lcom/android/printspooler/ui/PageAdapter$PreviewArea;->setPadding(IIII)V

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    .line 455
    .local v13, "inflater":Landroid/view/LayoutInflater;
    const v23, 0x7f030001

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v13, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 456
    .local v9, "content":Landroid/view/View;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentWidth:I

    move/from16 v23, v0

    const/high16 v24, 0x40000000    # 2.0f

    invoke-static/range {v23 .. v24}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentHeight:I

    move/from16 v24, v0

    const/high16 v25, 0x40000000    # 2.0f

    invoke-static/range {v24 .. v25}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/view/View;->measure(II)V

    .line 458
    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v25

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v26

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 460
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentWidth:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentHeight:I

    move/from16 v24, v0

    sget-object v25, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v23 .. v25}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 462
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 463
    .local v7, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v9, v7}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 467
    new-instance v23, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/printspooler/ui/PageAdapter;->mEmptyState:Landroid/graphics/drawable/BitmapDrawable;

    goto/16 :goto_0

    .line 433
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "canvas":Landroid/graphics/Canvas;
    .end local v9    # "content":Landroid/view/View;
    .end local v13    # "inflater":Landroid/view/LayoutInflater;
    .end local v19    # "rowCount":I
    .end local v20    # "totalContentHeight":I
    .end local v22    # "verticalPadding":I
    :cond_1
    const/16 v23, 0x0

    goto/16 :goto_1

    .line 445
    .restart local v19    # "rowCount":I
    .restart local v20    # "totalContentHeight":I
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PageAdapter;->mPreviewListPadding:I

    move/from16 v23, v0

    sub-int v24, v4, v20

    div-int/lit8 v24, v24, 0x2

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(II)I

    move-result v22

    .restart local v22    # "verticalPadding":I
    goto/16 :goto_2
.end method


# virtual methods
.method public close(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/android/printspooler/ui/PageAdapter;->throwIfNotOpened()V

    .line 281
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    .line 283
    const-string v0, "PageAdapter"

    const-string v1, "STATE_CLOSED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    invoke-virtual {v0, p1}, Lcom/android/printspooler/model/PageContentRepository;->close(Ljava/lang/Runnable;)V

    .line 286
    return-void
.end method

.method public destroy(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/android/printspooler/ui/PageAdapter;->throwIfNotClosed()V

    .line 504
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PageAdapter;->doDestroy(Ljava/lang/Runnable;)V

    .line 505
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 510
    :try_start_0
    iget v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 511
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mCloseGuard:Ldalvik/system/CloseGuard;

    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    .line 512
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PageAdapter;->doDestroy(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 515
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 517
    return-void

    .line 515
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getFilePageCount()I
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    invoke-virtual {v0}, Lcom/android/printspooler/model/PageContentRepository;->getFilePageCount()I

    move-result v0

    return v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 355
    iget v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPageCount:I

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInDocument(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getRequestedPages()[Landroid/print/PageRange;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    return-object v0
.end method

.method public getSelectedPages()[Landroid/print/PageRange;
    .locals 3

    .prologue
    .line 377
    invoke-direct {p0}, Lcom/android/printspooler/ui/PageAdapter;->computeSelectedPages()[Landroid/print/PageRange;

    move-result-object v0

    .line 378
    .local v0, "selectedPages":[Landroid/print/PageRange;
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 379
    iput-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    .line 380
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    iget v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    invoke-static {v1, v2}, Lcom/android/printspooler/util/PageRangeUtils;->getNormalizedPageCount([Landroid/print/PageRange;I)I

    move-result v1

    iput v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPageCount:I

    .line 382
    invoke-direct {p0}, Lcom/android/printspooler/ui/PageAdapter;->updatePreviewAreaPageSizeAndEmptyState()V

    .line 383
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PageAdapter;->notifyDataSetChanged()V

    .line 385
    :cond_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    return-object v1
.end method

.method public isOpened()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 181
    iget v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 16
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 297
    const-string v11, "PageAdapter"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Binding holder: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " with id: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PageAdapter;->getItemId(I)J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " for position: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v3, p1

    .line 301
    check-cast v3, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;

    .line 303
    .local v3, "myHolder":Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v4, Lcom/android/printspooler/widget/PreviewPageFrame;

    .line 304
    .local v4, "page":Lcom/android/printspooler/widget/PreviewPageFrame;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageClickListener:Lcom/android/printspooler/ui/PageAdapter$PageClickListener;

    invoke-virtual {v4, v11}, Lcom/android/printspooler/widget/PreviewPageFrame;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 306
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/printspooler/widget/PreviewPageFrame;->setTag(Ljava/lang/Object;)V

    .line 308
    move/from16 v0, p2

    iput v0, v3, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;->mPageInAdapter:I

    .line 310
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInDocument(I)I

    move-result v5

    .line 311
    .local v5, "pageInDocument":I
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInFile(I)I

    move-result v6

    .line 313
    .local v6, "pageIndexInFile":I
    const v11, 0x7f0d0001

    invoke-virtual {v4, v11}, Lcom/android/printspooler/widget/PreviewPageFrame;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/printspooler/widget/PageContentView;

    .line 314
    .local v2, "content":Lcom/android/printspooler/widget/PageContentView;
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mColorMode:I

    invoke-virtual {v2, v11}, Lcom/android/printspooler/widget/PageContentView;->setColorMode(I)V

    .line 316
    invoke-virtual {v2}, Lcom/android/printspooler/widget/PageContentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 317
    .local v8, "params":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentWidth:I

    iput v11, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 318
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentHeight:I

    iput v11, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 320
    invoke-virtual {v2}, Lcom/android/printspooler/widget/PageContentView;->getPageContentProvider()Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    move-result-object v9

    .line 322
    .local v9, "provider":Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;
    const/4 v11, -0x1

    if-eq v6, v11, :cond_0

    .line 324
    const-string v11, "PageAdapter"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Binding provider: pageIndexInAdapter: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", pageInDocument: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", pageIndexInFile: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    invoke-virtual {v11, v6, v2}, Lcom/android/printspooler/model/PageContentRepository;->acquirePageContentProvider(ILandroid/view/View;)Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    move-result-object v9

    .line 332
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mBoundPagesInAdapter:Landroid/util/SparseArray;

    const/4 v12, 0x0

    move/from16 v0, p2

    invoke-virtual {v11, v0, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 336
    :goto_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mEmptyState:Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/printspooler/ui/PageAdapter;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    invoke-virtual {v2, v9, v11, v12, v13}, Lcom/android/printspooler/widget/PageContentView;->init(Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;Landroid/graphics/drawable/Drawable;Landroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V

    .line 338
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mConfirmedPagesInDocument:Landroid/util/SparseArray;

    invoke-virtual {v11, v5}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v11

    if-ltz v11, :cond_1

    .line 339
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-virtual {v4, v11, v12}, Lcom/android/printspooler/widget/PreviewPageFrame;->setSelected(ZZ)V

    .line 344
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    const v12, 0x7f09001b

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    add-int/lit8 v15, v5, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Lcom/android/printspooler/widget/PreviewPageFrame;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 347
    const v11, 0x7f0d0003

    invoke-virtual {v4, v11}, Lcom/android/printspooler/widget/PreviewPageFrame;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 348
    .local v7, "pageNumberView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    const v12, 0x7f09001a

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    add-int/lit8 v15, v5, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 350
    .local v10, "text":Ljava/lang/String;
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    return-void

    .line 334
    .end local v7    # "pageNumberView":Landroid/widget/TextView;
    .end local v10    # "text":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/printspooler/ui/PageAdapter;->onSelectedPageNotInFile(I)V

    goto :goto_0

    .line 341
    :cond_1
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v4, v11, v12}, Lcom/android/printspooler/widget/PreviewPageFrame;->setSelected(ZZ)V

    goto :goto_1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 290
    iget-object v1, p0, Lcom/android/printspooler/ui/PageAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/high16 v2, 0x7f030000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 291
    .local v0, "page":Landroid/view/View;
    new-instance v1, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;-><init>(Lcom/android/printspooler/ui/PageAdapter;Landroid/view/View;Lcom/android/printspooler/ui/PageAdapter$1;)V

    return-object v1
.end method

.method public onMalformedPdfFile()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mCallbacks:Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;

    invoke-interface {v0}, Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;->onMalformedPdfFile()V

    .line 166
    return-void
.end method

.method public onOrientationChanged()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mColumnCount:I

    .line 177
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PageAdapter;->notifyDataSetChanged()V

    .line 178
    return-void
.end method

.method public onPreviewAreaSizeChanged()V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    if-eqz v0, :cond_0

    .line 390
    invoke-direct {p0}, Lcom/android/printspooler/ui/PageAdapter;->updatePreviewAreaPageSizeAndEmptyState()V

    .line 391
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PageAdapter;->notifyDataSetChanged()V

    .line 393
    :cond_0
    return-void
.end method

.method public onRendererBusy()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mCallbacks:Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;

    invoke-interface {v0}, Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;->onRendererBusy()V

    .line 172
    return-void
.end method

.method public onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 365
    move-object v1, p1

    check-cast v1, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;

    .line 366
    .local v1, "myHolder":Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;
    iget-object v2, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0d0001

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/widget/PageContentView;

    .line 368
    .local v0, "content":Lcom/android/printspooler/widget/PageContentView;
    iget v2, v1, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;->mPageInAdapter:I

    invoke-direct {p0, v0, v2}, Lcom/android/printspooler/ui/PageAdapter;->recyclePageView(Lcom/android/printspooler/widget/PageContentView;I)V

    .line 369
    const/4 v2, -0x1

    iput v2, v1, Lcom/android/printspooler/ui/PageAdapter$MyViewHolder;->mPageInAdapter:I

    .line 370
    return-void
.end method

.method public open(Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "source"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 189
    iget v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 190
    const-string v0, "PageAdapter"

    const-string v1, "[open] Already destroyed. skipping..."

    invoke-static {v0, v1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-direct {p0}, Lcom/android/printspooler/ui/PageAdapter;->throwIfNotClosed()V

    .line 194
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mState:I

    .line 196
    const-string v0, "PageAdapter"

    const-string v1, "STATE_OPENED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    new-instance v1, Lcom/android/printspooler/ui/PageAdapter$1;

    invoke-direct {v1, p0, p2}, Lcom/android/printspooler/ui/PageAdapter$1;-><init>(Lcom/android/printspooler/ui/PageAdapter;Ljava/lang/Runnable;)V

    invoke-virtual {v0, p1, v1}, Lcom/android/printspooler/model/PageContentRepository;->open(Landroid/os/ParcelFileDescriptor;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setColorMode(I)V
    .locals 0
    .param p1, "colorMode"    # I

    .prologue
    .line 809
    iput p1, p0, Lcom/android/printspooler/ui/PageAdapter;->mColorMode:I

    .line 811
    return-void
.end method

.method public startPreloadContent(Landroid/print/PageRange;)V
    .locals 6
    .param p1, "pageRangeInAdapter"    # Landroid/print/PageRange;

    .prologue
    const/4 v5, -0x1

    .line 746
    invoke-virtual {p1}, Landroid/print/PageRange;->getStart()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInDocument(I)I

    move-result v2

    .line 747
    .local v2, "startPageInDocument":I
    invoke-direct {p0, v2}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInFile(I)I

    move-result v3

    .line 748
    .local v3, "startPageInFile":I
    invoke-virtual {p1}, Landroid/print/PageRange;->getEnd()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInDocument(I)I

    move-result v0

    .line 749
    .local v0, "endPageInDocument":I
    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PageAdapter;->computePageIndexInFile(I)I

    move-result v1

    .line 750
    .local v1, "endPageInFile":I
    if-eq v2, v5, :cond_0

    if-eq v0, v5, :cond_0

    .line 751
    iget-object v4, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    invoke-virtual {v4, v3, v1}, Lcom/android/printspooler/model/PageContentRepository;->startPreload(II)V

    .line 753
    :cond_0
    return-void
.end method

.method public stopPreloadContent()V
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    invoke-virtual {v0}, Lcom/android/printspooler/model/PageContentRepository;->stopPreload()V

    .line 757
    return-void
.end method

.method public update([Landroid/print/PageRange;[Landroid/print/PageRange;ILandroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V
    .locals 4
    .param p1, "writtenPages"    # [Landroid/print/PageRange;
    .param p2, "selectedPages"    # [Landroid/print/PageRange;
    .param p3, "documentPageCount"    # I
    .param p4, "mediaSize"    # Landroid/print/PrintAttributes$MediaSize;
    .param p5, "minMargins"    # Landroid/print/PrintAttributes$Margins;

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 210
    .local v0, "documentChanged":Z
    const/4 v1, 0x0

    .line 214
    .local v1, "updatePreviewAreaAndPageSize":Z
    const/4 v2, -0x1

    if-ne p3, v2, :cond_2

    .line 215
    if-nez p1, :cond_1

    .line 217
    sget-object v2, Lcom/android/printspooler/ui/PageAdapter;->ALL_PAGES_ARRAY:[Landroid/print/PageRange;

    iget-object v3, p0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 218
    sget-object v2, Lcom/android/printspooler/ui/PageAdapter;->ALL_PAGES_ARRAY:[Landroid/print/PageRange;

    iput-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    .line 219
    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mCallbacks:Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;

    invoke-interface {v2}, Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;->onRequestContentUpdate()V

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mPageContentRepository:Lcom/android/printspooler/model/PageContentRepository;

    invoke-virtual {v2}, Lcom/android/printspooler/model/PageContentRepository;->getFilePageCount()I

    move-result p3

    .line 224
    if-lez p3, :cond_0

    .line 230
    :cond_2
    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    invoke-static {v2, p2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 231
    iput-object p2, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    .line 232
    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    invoke-static {v2, p3}, Lcom/android/printspooler/util/PageRangeUtils;->getNormalizedPageCount([Landroid/print/PageRange;I)I

    move-result v2

    iput v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPageCount:I

    .line 234
    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mSelectedPages:[Landroid/print/PageRange;

    invoke-direct {p0, v2, p3}, Lcom/android/printspooler/ui/PageAdapter;->setConfirmedPages([Landroid/print/PageRange;I)V

    .line 235
    const/4 v1, 0x1

    .line 236
    const/4 v0, 0x1

    .line 239
    :cond_3
    iget v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    if-eq v2, p3, :cond_4

    .line 240
    iput p3, p0, Lcom/android/printspooler/ui/PageAdapter;->mDocumentPageCount:I

    .line 241
    const/4 v0, 0x1

    .line 244
    :cond_4
    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v2, p4}, Landroid/print/PrintAttributes$MediaSize;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 245
    :cond_5
    iput-object p4, p0, Lcom/android/printspooler/ui/PageAdapter;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    .line 246
    const/4 v1, 0x1

    .line 247
    const/4 v0, 0x1

    .line 250
    :cond_6
    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    invoke-virtual {v2, p5}, Landroid/print/PrintAttributes$Margins;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 251
    :cond_7
    iput-object p5, p0, Lcom/android/printspooler/ui/PageAdapter;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    .line 252
    const/4 v1, 0x1

    .line 253
    const/4 v0, 0x1

    .line 258
    :cond_8
    if-eqz p1, :cond_a

    .line 260
    invoke-static {p1}, Lcom/android/printspooler/util/PageRangeUtils;->isAllPages([Landroid/print/PageRange;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 261
    iget-object p1, p0, Lcom/android/printspooler/ui/PageAdapter;->mRequestedPages:[Landroid/print/PageRange;

    .line 263
    :cond_9
    iget-object v2, p0, Lcom/android/printspooler/ui/PageAdapter;->mWrittenPages:[Landroid/print/PageRange;

    invoke-static {v2, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 265
    iput-object p1, p0, Lcom/android/printspooler/ui/PageAdapter;->mWrittenPages:[Landroid/print/PageRange;

    .line 266
    const/4 v0, 0x1

    .line 270
    :cond_a
    if-eqz v1, :cond_b

    .line 271
    invoke-direct {p0}, Lcom/android/printspooler/ui/PageAdapter;->updatePreviewAreaPageSizeAndEmptyState()V

    .line 274
    :cond_b
    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PageAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
