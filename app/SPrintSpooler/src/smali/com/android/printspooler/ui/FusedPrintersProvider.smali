.class public final Lcom/android/printspooler/ui/FusedPrintersProvider;
.super Landroid/content/Loader;
.source "FusedPrintersProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/Loader",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/print/PrinterInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mDiscoverySession:Landroid/print/PrinterDiscoverySession;

.field private final mFavoritePrinters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

.field private final mPrinters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPrintersUpdatedBefore:Z

.field private mTrackedPrinter:Landroid/print/PrinterId;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Landroid/content/Loader;-><init>(Landroid/content/Context;)V

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    .line 88
    new-instance v0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;-><init>(Lcom/android/printspooler/ui/FusedPrintersProvider;Landroid/content/Context;Lcom/android/printspooler/ui/FusedPrintersProvider$1;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    .line 89
    return-void
.end method

.method static synthetic access$100(Lcom/android/printspooler/ui/FusedPrintersProvider;)Landroid/print/PrinterDiscoverySession;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/printspooler/ui/FusedPrintersProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->loadInternal()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/printspooler/ui/FusedPrintersProvider;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/printspooler/ui/FusedPrintersProvider;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/android/printspooler/ui/FusedPrintersProvider;->updatePrinters(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private cancelInternal()Z
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->isPrinterDiscoveryStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mTrackedPrinter:Landroid/print/PrinterId;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    iget-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mTrackedPrinter:Landroid/print/PrinterId;

    invoke-virtual {v0, v1}, Landroid/print/PrinterDiscoverySession;->stopPrinterStateTracking(Landroid/print/PrinterId;)V

    .line 252
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mTrackedPrinter:Landroid/print/PrinterId;

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->stopPrinterDiscovery()V

    .line 255
    const/4 v0, 0x1

    .line 259
    :goto_0
    return v0

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->isReadHistoryInProgress()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->stopReadPrinterHistory()Z

    move-result v0

    goto :goto_0

    .line 259
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private computeAndDeliverResult(Landroid/util/ArrayMap;Ljava/util/LinkedHashMap;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap",
            "<",
            "Landroid/print/PrinterId;",
            "Landroid/print/PrinterInfo;",
            ">;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Landroid/print/PrinterId;",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "discoveredPrinters":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Landroid/print/PrinterId;Landroid/print/PrinterInfo;>;"
    .local p2, "favoritePrinters":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Landroid/print/PrinterId;Landroid/print/PrinterInfo;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v7, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    invoke-virtual {p2}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    .line 101
    .local v1, "favoritePrinterCount":I
    invoke-virtual {p2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/print/PrinterId;

    .line 102
    .local v4, "id":Landroid/print/PrinterId;
    invoke-virtual {p2, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    .line 103
    .local v0, "favoritePrinter":Landroid/print/PrinterInfo;
    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/print/PrinterInfo;

    .line 105
    .local v8, "updatedPrinter":Landroid/print/PrinterInfo;
    if-eqz v8, :cond_0

    .line 106
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_0
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    .end local v0    # "favoritePrinter":Landroid/print/PrinterInfo;
    .end local v4    # "id":Landroid/print/PrinterId;
    .end local v8    # "updatedPrinter":Landroid/print/PrinterInfo;
    :cond_1
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v6

    .line 114
    .local v6, "printerCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_3

    .line 115
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/print/PrinterInfo;

    .line 116
    .local v5, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {v5}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/print/PrinterInfo;

    .line 118
    .restart local v8    # "updatedPrinter":Landroid/print/PrinterInfo;
    if-eqz v8, :cond_2

    .line 119
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 124
    .end local v5    # "printer":Landroid/print/PrinterInfo;
    .end local v8    # "updatedPrinter":Landroid/print/PrinterInfo;
    :cond_3
    invoke-virtual {p1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 127
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 128
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 130
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->isStarted()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 132
    invoke-virtual {p0, v7}, Lcom/android/printspooler/ui/FusedPrintersProvider;->deliverResult(Ljava/lang/Object;)V

    .line 137
    :goto_2
    return-void

    .line 135
    :cond_4
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->onContentChanged()V

    goto :goto_2
.end method

.method private loadInternal()V
    .locals 7

    .prologue
    .line 171
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    if-nez v5, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "print"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/print/PrintManager;

    .line 174
    .local v2, "printManager":Landroid/print/PrintManager;
    invoke-virtual {v2}, Landroid/print/PrintManager;->createPrinterDiscoverySession()Landroid/print/PrinterDiscoverySession;

    move-result-object v5

    iput-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    .line 175
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v5}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->readPrinterHistory()V

    .line 179
    .end local v2    # "printManager":Landroid/print/PrintManager;
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v5}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->isReadHistoryCompleted()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v5}, Landroid/print/PrinterDiscoverySession;->isPrinterDiscoveryStarted()Z

    move-result v5

    if-nez v5, :cond_3

    .line 181
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    new-instance v6, Lcom/android/printspooler/ui/FusedPrintersProvider$1;

    invoke-direct {v6, p0}, Lcom/android/printspooler/ui/FusedPrintersProvider$1;-><init>(Lcom/android/printspooler/ui/FusedPrintersProvider;)V

    invoke-virtual {v5, v6}, Landroid/print/PrinterDiscoverySession;->setOnPrintersChangeListener(Landroid/print/PrinterDiscoverySession$OnPrintersChangeListener;)V

    .line 196
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 197
    .local v0, "favoriteCount":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 198
    .local v3, "printerIds":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterId;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 199
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/print/PrinterInfo;

    invoke-virtual {v5}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 176
    .end local v0    # "favoriteCount":I
    .end local v1    # "i":I
    .end local v3    # "printerIds":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterId;>;"
    :cond_1
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v5}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->isHistoryChanged()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 177
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v5}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->readPrinterHistory()V

    goto :goto_0

    .line 201
    .restart local v0    # "favoriteCount":I
    .restart local v1    # "i":I
    .restart local v3    # "printerIds":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterId;>;"
    :cond_2
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v5, v3}, Landroid/print/PrinterDiscoverySession;->startPrinterDiscovery(Ljava/util/List;)V

    .line 202
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v5}, Landroid/print/PrinterDiscoverySession;->getPrinters()Ljava/util/List;

    move-result-object v4

    .line 203
    .local v4, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 204
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    invoke-direct {p0, v4, v5}, Lcom/android/printspooler/ui/FusedPrintersProvider;->updatePrinters(Ljava/util/List;Ljava/util/List;)V

    .line 207
    .end local v0    # "favoriteCount":I
    .end local v1    # "i":I
    .end local v3    # "printerIds":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterId;>;"
    .end local v4    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    :cond_3
    return-void
.end method

.method private updatePrinters(Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 210
    .local p1, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    .local p2, "favoritePrinters":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    iget-boolean v7, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrintersUpdatedBefore:Z

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    invoke-interface {v7, p2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 237
    :goto_0
    return-void

    .line 215
    :cond_0
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrintersUpdatedBefore:Z

    .line 220
    iget-object v7, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v7, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->updatePrintersHistoricalNamesIfNeeded(Ljava/util/List;)V

    .line 222
    new-instance v6, Landroid/util/ArrayMap;

    invoke-direct {v6}, Landroid/util/ArrayMap;-><init>()V

    .line 223
    .local v6, "printersMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Landroid/print/PrinterId;Landroid/print/PrinterInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 224
    .local v5, "printerCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_1

    .line 225
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/print/PrinterInfo;

    .line 226
    .local v4, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v7

    invoke-virtual {v6, v7, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 229
    .end local v4    # "printer":Landroid/print/PrinterInfo;
    :cond_1
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 230
    .local v2, "favoritePrintersMap":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Landroid/print/PrinterId;Landroid/print/PrinterInfo;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 231
    .local v1, "favoritePrinterCount":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_2

    .line 232
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    .line 233
    .local v0, "favoritePrinter":Landroid/print/PrinterInfo;
    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v7

    invoke-virtual {v2, v7, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 236
    .end local v0    # "favoritePrinter":Landroid/print/PrinterInfo;
    :cond_2
    invoke-direct {p0, v6, v2}, Lcom/android/printspooler/ui/FusedPrintersProvider;->computeAndDeliverResult(Landroid/util/ArrayMap;Ljava/util/LinkedHashMap;)V

    goto :goto_0
.end method


# virtual methods
.method public addHistoricalPrinter(Landroid/print/PrinterInfo;)V
    .locals 1
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->addPrinterAndWritePrinterHistory(Landroid/print/PrinterInfo;)V

    .line 93
    return-void
.end method

.method public areHistoricalPrintersLoaded()Z
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryCompleted:Z
    invoke-static {v0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$400(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Z

    move-result v0

    return v0
.end method

.method public forgetFavoritePrinter(Landroid/print/PrinterId;)V
    .locals 5
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 315
    const/4 v3, 0x0

    .line 318
    .local v3, "newFavoritePrinters":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    .line 319
    .local v1, "favoritePrinterCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 320
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    .line 321
    .local v0, "favoritePrinter":Landroid/print/PrinterInfo;
    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 322
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "newFavoritePrinters":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 323
    .restart local v3    # "newFavoritePrinters":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 324
    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 330
    .end local v0    # "favoritePrinter":Landroid/print/PrinterInfo;
    :cond_0
    if-eqz v3, :cond_1

    .line 332
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPersistenceManager:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-virtual {v4, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->removeHistoricalPrinterAndWritePrinterHistory(Landroid/print/PrinterId;)V

    .line 335
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v4}, Landroid/print/PrinterDiscoverySession;->getPrinters()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v4, v3}, Lcom/android/printspooler/ui/FusedPrintersProvider;->updatePrinters(Ljava/util/List;Ljava/util/List;)V

    .line 337
    :cond_1
    return-void

    .line 319
    .restart local v0    # "favoritePrinter":Landroid/print/PrinterInfo;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public isFavoritePrinter(Landroid/print/PrinterId;)Z
    .locals 4
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 304
    iget-object v3, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 305
    .local v2, "printerCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 306
    iget-object v3, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    .line 307
    .local v0, "favoritePritner":Landroid/print/PrinterInfo;
    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 308
    const/4 v3, 0x1

    .line 311
    .end local v0    # "favoritePritner":Landroid/print/PrinterInfo;
    :goto_1
    return v3

    .line 305
    .restart local v0    # "favoritePritner":Landroid/print/PrinterInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 311
    .end local v0    # "favoritePritner":Landroid/print/PrinterInfo;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method protected onAbandon()V
    .locals 3

    .prologue
    .line 278
    const-string v0, "FusedPrintersProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAbandon() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->onStopLoading()V

    .line 281
    return-void
.end method

.method protected onCancelLoad()Z
    .locals 3

    .prologue
    .line 242
    const-string v0, "FusedPrintersProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCancelLoad() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-direct {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->cancelInternal()Z

    move-result v0

    return v0
.end method

.method protected onForceLoad()V
    .locals 3

    .prologue
    .line 165
    const-string v0, "FusedPrintersProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onForceLoad() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-direct {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->loadInternal()V

    .line 168
    return-void
.end method

.method protected onReset()V
    .locals 3

    .prologue
    .line 265
    const-string v0, "FusedPrintersProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReset() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->onStopLoading()V

    .line 268
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 269
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->destroy()V

    .line 271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    .line 273
    :cond_0
    return-void
.end method

.method protected onStartLoading()V
    .locals 3

    .prologue
    .line 142
    const-string v0, "FusedPrintersProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartLoading() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mPrinters:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->deliverResult(Ljava/lang/Object;)V

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->onForceLoad()V

    .line 152
    return-void
.end method

.method protected onStopLoading()V
    .locals 3

    .prologue
    .line 157
    const-string v0, "FusedPrintersProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStopLoading() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->onCancelLoad()Z

    .line 160
    return-void
.end method

.method public setTrackedPrinter(Landroid/print/PrinterId;)V
    .locals 2
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->isPrinterDiscoveryStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mTrackedPrinter:Landroid/print/PrinterId;

    if-eqz v0, :cond_2

    .line 291
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mTrackedPrinter:Landroid/print/PrinterId;

    invoke-virtual {v0, p1}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    iget-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mTrackedPrinter:Landroid/print/PrinterId;

    invoke-virtual {v0, v1}, Landroid/print/PrinterDiscoverySession;->stopPrinterStateTracking(Landroid/print/PrinterId;)V

    .line 296
    :cond_2
    iput-object p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mTrackedPrinter:Landroid/print/PrinterId;

    .line 297
    if-eqz p1, :cond_0

    .line 298
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0, p1}, Landroid/print/PrinterDiscoverySession;->startPrinterStateTracking(Landroid/print/PrinterId;)V

    goto :goto_0
.end method
