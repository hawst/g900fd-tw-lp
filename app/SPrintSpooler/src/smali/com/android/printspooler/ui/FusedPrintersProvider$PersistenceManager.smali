.class final Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
.super Ljava/lang/Object;
.source "FusedPrintersProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/FusedPrintersProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PersistenceManager"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;,
        Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;,
        Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;
    }
.end annotation


# instance fields
.field private mHistoricalPrinters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mLastReadHistoryTimestamp:J

.field private mReadHistoryCompleted:Z

.field private mReadHistoryInProgress:Z

.field private mReadTask:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

.field private final mStatePersistFile:Landroid/util/AtomicFile;

.field final synthetic this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/FusedPrintersProvider;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 365
    iput-object p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    .line 366
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "printer_history.xml"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;

    .line 368
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/FusedPrintersProvider;Landroid/content/Context;Lcom/android/printspooler/ui/FusedPrintersProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider;
    .param p2, "x1"    # Landroid/content/Context;
    .param p3, "x2"    # Lcom/android/printspooler/ui/FusedPrintersProvider$1;

    .prologue
    .line 339
    invoke-direct {p0, p1, p2}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;-><init>(Lcom/android/printspooler/ui/FusedPrintersProvider;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$1102(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;)Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    .param p1, "x1"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    .prologue
    .line 339
    iput-object p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadTask:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Landroid/util/AtomicFile;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    .param p1, "x1"    # J

    .prologue
    .line 339
    iput-wide p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mLastReadHistoryTimestamp:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    .prologue
    .line 339
    iget-boolean v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryCompleted:Z

    return v0
.end method

.method static synthetic access$402(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    .param p1, "x1"    # Z

    .prologue
    .line 339
    iput-boolean p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryCompleted:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 339
    iput-object p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->computeFavoritePrinters(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$902(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    .param p1, "x1"    # Z

    .prologue
    .line 339
    iput-boolean p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryInProgress:Z

    return p1
.end method

.method private computeFavoritePrinters(Ljava/util/List;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 459
    .local p1, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    new-instance v8, Landroid/util/ArrayMap;

    invoke-direct {v8}, Landroid/util/ArrayMap;-><init>()V

    .line 462
    .local v8, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Landroid/print/PrinterId;Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;>;"
    const/high16 v0, 0x3f800000    # 1.0f

    .line 463
    .local v0, "currentWeight":F
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    .line 464
    .local v6, "printerCount":I
    add-int/lit8 v4, v6, -0x1

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_1

    .line 465
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/print/PrinterInfo;

    .line 467
    .local v5, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {v5}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;

    .line 468
    .local v7, "record":Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;
    if-nez v7, :cond_0

    .line 469
    new-instance v7, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;

    .end local v7    # "record":Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;
    invoke-direct {v7, p0, v5}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;-><init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Landroid/print/PrinterInfo;)V

    .line 470
    .restart local v7    # "record":Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;
    invoke-virtual {v5}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-interface {v8, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    :cond_0
    iget v9, v7, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;->weight:F

    add-float/2addr v9, v0

    iput v9, v7, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;->weight:F

    .line 473
    float-to-double v10, v0

    const-wide v12, 0x3fee666660000000L    # 0.949999988079071

    mul-double/2addr v10, v12

    double-to-float v0, v10

    .line 464
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 477
    .end local v5    # "printer":Landroid/print/PrinterInfo;
    .end local v7    # "record":Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 479
    .local v3, "favoriteRecords":Ljava/util/List;, "Ljava/util/List<Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;>;"
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 482
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x4

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 484
    .local v1, "favoriteCount":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 485
    .local v2, "favoritePrinters":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_2

    .line 486
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;

    iget-object v5, v9, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;->printer:Landroid/print/PrinterInfo;

    .line 487
    .restart local v5    # "printer":Landroid/print/PrinterInfo;
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 485
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 490
    .end local v5    # "printer":Landroid/print/PrinterInfo;
    :cond_2
    return-object v2
.end method

.method private writePrinterHistory()V
    .locals 6

    .prologue
    .line 450
    new-instance v0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;-><init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Lcom/android/printspooler/ui/FusedPrintersProvider$1;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/util/List;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 452
    return-void
.end method


# virtual methods
.method public addPrinterAndWritePrinterHistory(Landroid/print/PrinterInfo;)V
    .locals 2
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 426
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 427
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    invoke-direct {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->writePrinterHistory()V

    .line 431
    return-void
.end method

.method public isHistoryChanged()Z
    .locals 4

    .prologue
    .line 455
    iget-wide v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mLastReadHistoryTimestamp:J

    iget-object v2, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;

    invoke-virtual {v2}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReadHistoryCompleted()Z
    .locals 1

    .prologue
    .line 375
    iget-boolean v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryCompleted:Z

    return v0
.end method

.method public isReadHistoryInProgress()Z
    .locals 1

    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryInProgress:Z

    return v0
.end method

.method public readPrinterHistory()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 389
    const-string v1, "FusedPrintersProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read history started "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryInProgress:Z

    .line 393
    new-instance v1, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    invoke-direct {v1, p0, v0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;-><init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Lcom/android/printspooler/ui/FusedPrintersProvider$1;)V

    iput-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadTask:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    .line 394
    iget-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadTask:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    sget-object v2, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 395
    return-void
.end method

.method public removeHistoricalPrinterAndWritePrinterHistory(Landroid/print/PrinterId;)V
    .locals 5
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 434
    const/4 v3, 0x0

    .line 435
    .local v3, "writeHistory":Z
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 436
    .local v2, "printerCount":I
    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 437
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    .line 438
    .local v0, "historicalPrinter":Landroid/print/PrinterInfo;
    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 439
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 440
    const/4 v3, 0x1

    .line 436
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 443
    .end local v0    # "historicalPrinter":Landroid/print/PrinterInfo;
    :cond_1
    if-eqz v3, :cond_2

    .line 444
    invoke-direct {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->writePrinterHistory()V

    .line 446
    :cond_2
    return-void
.end method

.method public renamePrinterIfNeeded(Landroid/print/PrinterInfo;)Z
    .locals 6
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 412
    const/4 v3, 0x0

    .line 413
    .local v3, "renamed":Z
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 414
    .local v2, "printerCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 415
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    .line 416
    .local v0, "historicalPrinter":Landroid/print/PrinterInfo;
    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 418
    iget-object v4, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;

    invoke-interface {v4, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 419
    const/4 v3, 0x1

    .line 414
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 422
    .end local v0    # "historicalPrinter":Landroid/print/PrinterInfo;
    :cond_1
    return v3
.end method

.method public stopReadPrinterHistory()Z
    .locals 3

    .prologue
    .line 379
    const/4 v0, 0x1

    .line 380
    .local v0, "cancelled":Z
    iget-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadTask:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    if-eqz v1, :cond_0

    .line 381
    iget-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadTask:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->cancel(Z)Z

    move-result v0

    .line 383
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadTask:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    .line 384
    return v0
.end method

.method public updatePrintersHistoricalNamesIfNeeded(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 398
    .local p1, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v3, 0x0

    .line 400
    .local v3, "writeHistory":Z
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 401
    .local v2, "printerCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 402
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrinterInfo;

    .line 403
    .local v1, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {p0, v1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->renamePrinterIfNeeded(Landroid/print/PrinterInfo;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 401
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 406
    .end local v1    # "printer":Landroid/print/PrinterInfo;
    :cond_0
    if-eqz v3, :cond_1

    .line 407
    invoke-direct {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->writePrinterHistory()V

    .line 409
    :cond_1
    return-void
.end method
