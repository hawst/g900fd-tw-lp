.class Lcom/android/printspooler/ui/SelectPrinterActivity$4;
.super Ljava/lang/Object;
.source "SelectPrinterActivity.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$4;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$4;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$4;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    const v1, 0x7f090026

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 179
    :cond_0
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$4;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$4;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$4;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    const v1, 0x7f090027

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 187
    :cond_0
    return-void
.end method
