.class final Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ProgressMessageController"
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mPosted:Z

.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2397
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2398
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->mHandler:Landroid/os/Handler;

    .line 2399
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 2410
    iget-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->mPosted:Z

    if-nez v0, :cond_0

    .line 2415
    :goto_0
    return-void

    .line 2413
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->mPosted:Z

    .line 2414
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public post()V
    .locals 4

    .prologue
    .line 2402
    iget-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->mPosted:Z

    if-eqz v0, :cond_0

    .line 2407
    :goto_0
    return-void

    .line 2405
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->mPosted:Z

    .line 2406
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 2419
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->mPosted:Z

    .line 2420
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    const/4 v1, 0x7

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->setState(I)V
    invoke-static {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->access$800(Lcom/android/printspooler/ui/PrintActivity;I)V

    .line 2421
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->ensureProgressUiShown()V
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$5100(Lcom/android/printspooler/ui/PrintActivity;)V

    .line 2422
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 2423
    return-void
.end method
