.class final Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;
.super Landroid/os/AsyncTask;
.source "FusedPrintersProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ReadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/print/PrinterInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Lcom/android/printspooler/ui/FusedPrintersProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    .param p2, "x1"    # Lcom/android/printspooler/ui/FusedPrintersProvider$1;

    .prologue
    .line 507
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;-><init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)V

    return-void
.end method

.method private accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z
    .locals 2
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "type"    # I
    .param p3, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 674
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-eq v1, p2, :cond_1

    .line 684
    :cond_0
    :goto_0
    return v0

    .line 677
    :cond_1
    if-eqz p3, :cond_3

    .line 678
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 684
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 681
    :cond_3
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_0
.end method

.method private doReadPrinterHistory()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566
    :try_start_0
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;
    invoke-static {v5}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$1200(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Landroid/util/AtomicFile;

    move-result-object v5

    invoke-virtual {v5}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 575
    .local v2, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 576
    .local v4, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 577
    .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v5, 0x0

    invoke-interface {v3, v2, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 578
    invoke-direct {p0, v3, v4}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->parseState(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/List;)V

    .line 580
    iget-object v5, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v6, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;
    invoke-static {v6}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$1200(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Landroid/util/AtomicFile;

    move-result-object v6

    invoke-virtual {v6}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    # setter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mLastReadHistoryTimestamp:J
    invoke-static {v5, v6, v7}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$1302(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;J)J
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 593
    .end local v2    # "in":Ljava/io/FileInputStream;
    .end local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    :goto_0
    return-object v4

    .line 567
    :catch_0
    move-exception v1

    .line 569
    .local v1, "fnfe":Ljava/io/FileNotFoundException;
    const-string v5, "FusedPrintersProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No existing printer history "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v7, v7, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    invoke-virtual {v7}, Ljava/lang/Object;->hashCode()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 582
    .end local v1    # "fnfe":Ljava/io/FileNotFoundException;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_1
    move-exception v5

    move-object v0, v5

    .line 588
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    const-string v5, "FusedPrintersProvider"

    const-string v6, "Failed parsing "

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 590
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 593
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    goto :goto_0

    .line 590
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v5

    .line 582
    :catch_2
    move-exception v5

    move-object v0, v5

    goto :goto_1

    :catch_3
    move-exception v5

    move-object v0, v5

    goto :goto_1

    :catch_4
    move-exception v5

    move-object v0, v5

    goto :goto_1

    :catch_5
    move-exception v5

    move-object v0, v5

    goto :goto_1

    :catch_6
    move-exception v5

    move-object v0, v5

    goto :goto_1
.end method

.method private expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V
    .locals 3
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "type"    # I
    .param p3, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 657
    invoke-direct {p0, p1, p2, p3}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exepected event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but got event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and tag:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662
    :cond_0
    return-void
.end method

.method private parsePrinter(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/List;)Z
    .locals 12
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .local p2, "outPrinters":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 617
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 618
    const-string v8, "printer"

    invoke-direct {p0, p1, v10, v8}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 619
    const/4 v8, 0x0

    .line 652
    :goto_0
    return v8

    .line 622
    :cond_0
    const-string v8, "name"

    invoke-interface {p1, v9, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 623
    .local v3, "name":Ljava/lang/String;
    const-string v8, "description"

    invoke-interface {p1, v9, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 624
    .local v1, "description":Ljava/lang/String;
    const-string v8, "status"

    invoke-interface {p1, v9, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 626
    .local v7, "status":I
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 628
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 629
    const-string v8, "printerId"

    invoke-direct {p0, p1, v10, v8}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 630
    const-string v8, "localId"

    invoke-interface {p1, v9, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 631
    .local v2, "localId":Ljava/lang/String;
    const-string v8, "serviceName"

    invoke-interface {p1, v9, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v6

    .line 633
    .local v6, "service":Landroid/content/ComponentName;
    new-instance v5, Landroid/print/PrinterId;

    invoke-direct {v5, v6, v2}, Landroid/print/PrinterId;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 634
    .local v5, "printerId":Landroid/print/PrinterId;
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 635
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 636
    const-string v8, "printerId"

    invoke-direct {p0, p1, v11, v8}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 637
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 639
    new-instance v0, Landroid/print/PrinterInfo$Builder;

    invoke-direct {v0, v5, v3, v7}, Landroid/print/PrinterInfo$Builder;-><init>(Landroid/print/PrinterId;Ljava/lang/String;I)V

    .line 640
    .local v0, "builder":Landroid/print/PrinterInfo$Builder;
    invoke-virtual {v0, v1}, Landroid/print/PrinterInfo$Builder;->setDescription(Ljava/lang/String;)Landroid/print/PrinterInfo$Builder;

    .line 641
    invoke-virtual {v0}, Landroid/print/PrinterInfo$Builder;->build()Landroid/print/PrinterInfo;

    move-result-object v4

    .line 643
    .local v4, "printer":Landroid/print/PrinterInfo;
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    const-string v8, "FusedPrintersProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[RESTORED] "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 650
    const-string v8, "printer"

    invoke-direct {p0, p1, v11, v8}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 652
    const/4 v8, 0x1

    goto :goto_0
.end method

.method private parseState(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/List;)V
    .locals 2
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 598
    .local p2, "outPrinters":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 599
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 600
    const/4 v0, 0x2

    const-string v1, "printers"

    invoke-direct {p0, p1, v0, v1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    .line 601
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 603
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->parsePrinter(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 605
    invoke-virtual {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    :goto_1
    return-void

    .line 608
    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    goto :goto_0

    .line 611
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 612
    const/4 v0, 0x3

    const-string v1, "printers"

    invoke-direct {p0, p1, v0, v1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->expect(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)V

    goto :goto_1
.end method

.method private skipEmptyTextTags(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 667
    :goto_0
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->accept(Lorg/xmlpull/v1/XmlPullParser;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\n"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    goto :goto_0

    .line 670
    :cond_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 507
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 1
    .param p1, "args"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 510
    invoke-direct {p0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->doReadPrinterHistory()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 507
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 516
    .local p1, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const-string v9, "FusedPrintersProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "read history completed "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v11, v11, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    invoke-virtual {v11}, Ljava/lang/Object;->hashCode()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v9, v9, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/FusedPrintersProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "print"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/print/PrintManager;

    .line 523
    .local v4, "printManager":Landroid/print/PrintManager;
    invoke-virtual {v4}, Landroid/print/PrintManager;->getEnabledPrintServices()Ljava/util/List;

    move-result-object v8

    .line 526
    .local v8, "services":Ljava/util/List;, "Ljava/util/List<Landroid/printservice/PrintServiceInfo;>;"
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    .line 527
    .local v1, "enabledComponents":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    .line 528
    .local v3, "installedServiceCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 529
    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/printservice/PrintServiceInfo;

    invoke-virtual {v9}, Landroid/printservice/PrintServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v9

    iget-object v7, v9, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 530
    .local v7, "serviceInfo":Landroid/content/pm/ServiceInfo;
    new-instance v0, Landroid/content/ComponentName;

    iget-object v9, v7, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v10, v7, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    .local v0, "componentName":Landroid/content/ComponentName;
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 528
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 535
    .end local v0    # "componentName":Landroid/content/ComponentName;
    .end local v7    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 536
    .local v5, "printerCount":I
    add-int/lit8 v2, v5, -0x1

    :goto_1
    if-ltz v2, :cond_2

    .line 537
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/print/PrinterInfo;

    invoke-virtual {v9}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {v9}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v6

    .line 538
    .local v6, "printerServiceName":Landroid/content/ComponentName;
    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 539
    invoke-interface {p1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 536
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 544
    .end local v6    # "printerServiceName":Landroid/content/ComponentName;
    :cond_2
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    # setter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;
    invoke-static {v9, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$702(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Ljava/util/List;)Ljava/util/List;

    .line 547
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v9, v9, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;
    invoke-static {v9}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$200(Lcom/android/printspooler/ui/FusedPrintersProvider;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 548
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v9, v9, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;
    invoke-static {v9}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$200(Lcom/android/printspooler/ui/FusedPrintersProvider;)Ljava/util/List;

    move-result-object v9

    iget-object v10, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v11, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mHistoricalPrinters:Ljava/util/List;
    invoke-static {v11}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$700(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Ljava/util/List;

    move-result-object v11

    # invokes: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->computeFavoritePrinters(Ljava/util/List;)Ljava/util/List;
    invoke-static {v10, v11}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$800(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 550
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    const/4 v10, 0x0

    # setter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryInProgress:Z
    invoke-static {v9, v10}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$902(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Z)Z

    .line 551
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    const/4 v10, 0x1

    # setter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadHistoryCompleted:Z
    invoke-static {v9, v10}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$402(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Z)Z

    .line 554
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v9, v9, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    iget-object v10, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v10, v10, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;
    invoke-static {v10}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$100(Lcom/android/printspooler/ui/FusedPrintersProvider;)Landroid/print/PrinterDiscoverySession;

    move-result-object v10

    invoke-virtual {v10}, Landroid/print/PrinterDiscoverySession;->getPrinters()Ljava/util/List;

    move-result-object v10

    iget-object v11, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v11, v11, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;
    invoke-static {v11}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$200(Lcom/android/printspooler/ui/FusedPrintersProvider;)Ljava/util/List;

    move-result-object v11

    # invokes: Lcom/android/printspooler/ui/FusedPrintersProvider;->updatePrinters(Ljava/util/List;Ljava/util/List;)V
    invoke-static {v9, v10, v11}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$300(Lcom/android/printspooler/ui/FusedPrintersProvider;Ljava/util/List;Ljava/util/List;)V

    .line 557
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    iget-object v9, v9, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    # invokes: Lcom/android/printspooler/ui/FusedPrintersProvider;->loadInternal()V
    invoke-static {v9}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$1000(Lcom/android/printspooler/ui/FusedPrintersProvider;)V

    .line 560
    iget-object v9, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    const/4 v10, 0x0

    # setter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mReadTask:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;
    invoke-static {v9, v10}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$1102(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;)Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$ReadTask;

    .line 561
    return-void
.end method
