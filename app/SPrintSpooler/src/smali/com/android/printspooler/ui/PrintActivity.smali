.class public Lcom/android/printspooler/ui/PrintActivity;
.super Landroid/app/Activity;
.source "PrintActivity.java"

# interfaces
.implements Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;
.implements Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;
.implements Lcom/android/printspooler/ui/PrintErrorFragment$OnActionListener;
.implements Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;
.implements Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/ui/PrintActivity$PageShredder;,
        Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;,
        Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;,
        Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;,
        Lcom/android/printspooler/ui/PrintActivity$SelectAllOnFocusListener;,
        Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;,
        Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;,
        Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;,
        Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;,
        Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;,
        Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;,
        Lcom/android/printspooler/ui/PrintActivity$MyClickListener;
    }
.end annotation


# static fields
.field public static final ALL_PAGES_ARRAY:[Landroid/print/PageRange;

.field private static final MIN_COPIES_STRING:Ljava/lang/String;

.field private static final PATTERN_DIGITS:Ljava/util/regex/Pattern;

.field private static final PATTERN_ESCAPE_SPECIAL_CHARS:Ljava/util/regex/Pattern;

.field private static final PATTERN_PAGE_RANGE:Ljava/util/regex/Pattern;


# instance fields
.field private bSConnectPrintMode:Z

.field private locale:Ljava/util/Locale;

.field private mCallingPackageName:Ljava/lang/String;

.field private mColorModeSpinner:Landroid/widget/Spinner;

.field private mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCopiesEditText:Landroid/widget/EditText;

.field private mCurrentPageCount:I

.field private mCurrentPrinter:Landroid/print/PrinterInfo;

.field private mDestinationSpinner:Landroid/widget/Spinner;

.field private mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

.field private mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

.field private mMediaSizeComparator:Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;

.field private mMediaSizeSpinner:Landroid/widget/Spinner;

.field private mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem",
            "<",
            "Landroid/print/PrintAttributes$MediaSize;",
            ">;>;"
        }
    .end annotation
.end field

.field private mMoreOptionsButton:Landroid/widget/Button;

.field private mOptionsContent:Lcom/android/printspooler/widget/PrintContentView;

.field private mOrientationSpinner:Landroid/widget/Spinner;

.field private mOrientationSpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPageRangeEditText:Landroid/widget/EditText;

.field private mPageRangeTitle:Landroid/widget/TextView;

.field private mPrintButton:Landroid/widget/ImageView;

.field private mPrintJob:Landroid/print/PrintJobInfo;

.field private mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

.field private mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

.field private final mPrinterAvailabilityDetector:Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;

.field private mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

.field private mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

.field private mRangeOptionsSpinner:Landroid/widget/Spinner;

.field private mSConnectPrinter:Landroid/print/PrinterInfo;

.field private final mSelectAllOnFocusListener:Landroid/view/View$OnFocusChangeListener;

.field private mSelectedPages:[Landroid/print/PageRange;

.field private mSpoolerProvider:Lcom/android/printspooler/model/PrintSpoolerProvider;

.field private mState:I

.field private final mStringCommaSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

.field private mSummaryContainer:Landroid/view/View;

.field private mSummaryCopies:Landroid/widget/TextView;

.field private mSummaryPaperSize:Landroid/widget/TextView;

.field private mUiState:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 149
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/printspooler/ui/PrintActivity;->MIN_COPIES_STRING:Ljava/lang/String;

    .line 151
    const-string v0, "[\\d]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/printspooler/ui/PrintActivity;->PATTERN_DIGITS:Ljava/util/regex/Pattern;

    .line 153
    const-string v0, "(?=[]\\[+&|!(){}^\"~*?:\\\\])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/printspooler/ui/PrintActivity;->PATTERN_ESCAPE_SPECIAL_CHARS:Ljava/util/regex/Pattern;

    .line 156
    const-string v0, "[\\s]*[0-9]+[\\-]?[\\s]*[0-9]*[\\s]*?(([,])[\\s]*[0-9]+[\\s]*[\\-]?[\\s]*[0-9]*[\\s]*|[\\s]*)+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/printspooler/ui/PrintActivity;->PATTERN_PAGE_RANGE:Ljava/util/regex/Pattern;

    .line 160
    new-array v0, v1, [Landroid/print/PageRange;

    const/4 v1, 0x0

    sget-object v2, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/printspooler/ui/PrintActivity;->ALL_PAGES_ARRAY:[Landroid/print/PageRange;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 111
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 162
    new-instance v0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;

    invoke-direct {v0, p0, v3}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;-><init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterAvailabilityDetector:Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;

    .line 165
    new-instance v0, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v1, 0x2c

    invoke-direct {v0, v1}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mStringCommaSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

    .line 167
    new-instance v0, Lcom/android/printspooler/ui/PrintActivity$SelectAllOnFocusListener;

    invoke-direct {v0, p0, v3}, Lcom/android/printspooler/ui/PrintActivity$SelectAllOnFocusListener;-><init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectAllOnFocusListener:Landroid/view/View$OnFocusChangeListener;

    .line 219
    iput v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    .line 221
    iput v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mUiState:I

    .line 225
    iput-boolean v2, p0, Lcom/android/printspooler/ui/PrintActivity;->bSConnectPrintMode:Z

    .line 2426
    return-void
.end method

.method static synthetic access$1000(Lcom/android/printspooler/ui/PrintActivity;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->shredPagesAndFinish(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrinterInfo;)Landroid/print/PrinterInfo;
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Landroid/print/PrinterInfo;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->confirmPrint()V

    return-void
.end method

.method static synthetic access$1900(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->cancelPrint()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/printspooler/ui/PrintActivity;Landroid/os/IBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->onConnectedToPrintSpooler(Landroid/os/IBinder;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrinterInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Landroid/print/PrinterInfo;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->startAdvancedPrintOptionsActivity(Landroid/print/PrinterInfo;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrinterRegistry;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/printspooler/ui/PrintActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity;->bSConnectPrintMode:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mSConnectPrinter:Landroid/print/PrinterInfo;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterAvailabilityDetector:Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrinterCapabilitiesInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Landroid/print/PrinterCapabilitiesInfo;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->updatePrintAttributesFromCapabilities(Landroid/print/PrinterCapabilitiesInfo;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/android/printspooler/ui/PrintActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->updatePrintPreviewController(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/printspooler/ui/PrintActivity;Landroid/os/IBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->onPrinterRegistryReady(Landroid/os/IBinder;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/android/printspooler/ui/PrintActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/android/printspooler/ui/PrintActivity;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->startSelectPrinterActivity()V

    return-void
.end method

.method static synthetic access$3300(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->ensurePreviewUiShown()V

    return-void
.end method

.method static synthetic access$3400(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrintJobInfo;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/printspooler/ui/PrintActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    return v0
.end method

.method static synthetic access$4000(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintPreviewController;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinnerAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/android/printspooler/ui/PrintActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4500()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/android/printspooler/ui/PrintActivity;->PATTERN_ESCAPE_SPECIAL_CHARS:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$4600()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/android/printspooler/ui/PrintActivity;->PATTERN_PAGE_RANGE:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrintDocumentInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # Landroid/print/PrintDocumentInfo;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I

    move-result v0

    return v0
.end method

.method static synthetic access$4800()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/android/printspooler/ui/PrintActivity;->PATTERN_DIGITS:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/android/printspooler/ui/PrintActivity;)[Landroid/print/PageRange;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->computeSelectedPages()[Landroid/print/PageRange;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(I)Z
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 111
    invoke-static {p0}, Lcom/android/printspooler/ui/PrintActivity;->isFinalState(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5000(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->ensureProgressUiShown()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/model/RemotePrintDocument;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/printspooler/ui/PrintActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mCallingPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/printspooler/ui/PrintActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p1, "x1"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->doFinish()V

    return-void
.end method

.method private addCurrentPrinterToHistory()V
    .locals 3

    .prologue
    .line 1044
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    if-eqz v1, :cond_0

    .line 1045
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPdfPrinter()Landroid/print/PrinterInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v0

    .line 1046
    .local v0, "fakePdfPrinterId":Landroid/print/PrinterId;
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1047
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v1, v2}, Lcom/android/printspooler/ui/PrinterRegistry;->addHistoricalPrinter(Landroid/print/PrinterInfo;)V

    .line 1050
    .end local v0    # "fakePdfPrinterId":Landroid/print/PrinterId;
    :cond_0
    return-void
.end method

.method private bindUi()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const v11, 0x7f03000a

    const v10, 0x1020014

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 1088
    const v4, 0x7f0d0008

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mSummaryContainer:Landroid/view/View;

    .line 1089
    const v4, 0x7f0d0009

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mSummaryCopies:Landroid/widget/TextView;

    .line 1090
    const v4, 0x7f0d000a

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mSummaryPaperSize:Landroid/widget/TextView;

    .line 1091
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mSummaryPaperSize:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1092
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mSummaryPaperSize:Landroid/widget/TextView;

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1095
    const v4, 0x7f0d0005

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/printspooler/widget/PrintContentView;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOptionsContent:Lcom/android/printspooler/widget/PrintContentView;

    .line 1096
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOptionsContent:Lcom/android/printspooler/widget/PrintContentView;

    invoke-virtual {v4, p0}, Lcom/android/printspooler/widget/PrintContentView;->setOptionsStateChangeListener(Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;)V

    .line 1097
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOptionsContent:Lcom/android/printspooler/widget/PrintContentView;

    invoke-virtual {v4, p0}, Lcom/android/printspooler/widget/PrintContentView;->setOpenOptionsController(Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;)V

    .line 1099
    new-instance v1, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;

    invoke-direct {v1, p0, v9}, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;-><init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V

    .line 1100
    .local v1, "itemSelectedListener":Landroid/widget/AdapterView$OnItemSelectedListener;
    new-instance v0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;

    invoke-direct {v0, p0, v9}, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;-><init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V

    .line 1103
    .local v0, "clickListener":Landroid/view/View$OnClickListener;
    const v4, 0x7f0d0012

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    .line 1104
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectAllOnFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v4, v7}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1105
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    sget-object v7, Lcom/android/printspooler/ui/PrintActivity;->MIN_COPIES_STRING:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1106
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/EditText;->setSelection(I)V

    .line 1107
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    new-instance v7, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;

    invoke-direct {v7, p0, v9}, Lcom/android/printspooler/ui/PrintActivity$EditTextWatcher;-><init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V

    invoke-virtual {v4, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1110
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    new-instance v7, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;

    invoke-direct {v7, p0, v9}, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;-><init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V

    invoke-virtual {v4, v7}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1111
    const v4, 0x7f0d0007

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    .line 1112
    iget-object v7, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    iget-boolean v4, p0, Lcom/android/printspooler/ui/PrintActivity;->bSConnectPrintMode:Z

    if-nez v4, :cond_0

    move v4, v5

    :goto_0
    invoke-virtual {v7, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1113
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    iget-object v7, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1114
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1117
    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-direct {v4, p0, v11, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 1119
    const v4, 0x7f0d0013

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    .line 1120
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    iget-object v7, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1121
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1124
    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-direct {v4, p0, v11, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 1126
    const v4, 0x7f0d0014

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    .line 1127
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    iget-object v7, p0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1128
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1131
    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-direct {v4, p0, v11, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 1133
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f040004

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 1135
    .local v2, "orientationLabels":[Ljava/lang/String;
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v7, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aget-object v6, v2, v6

    invoke-direct {v7, p0, v8, v6}, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;-><init>(Lcom/android/printspooler/ui/PrintActivity;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v7}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1137
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v6, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aget-object v5, v2, v5

    invoke-direct {v6, p0, v7, v5}, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;-><init>(Lcom/android/printspooler/ui/PrintActivity;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1139
    const v4, 0x7f0d0015

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    .line 1140
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1141
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1144
    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-direct {v3, p0, v11, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 1146
    .local v3, "rangeOptionsSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;>;"
    const v4, 0x7f0d0016

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    .line 1147
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1148
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1149
    const/4 v4, -0x1

    invoke-direct {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->updatePageRangeOptions(I)V

    .line 1152
    const v4, 0x7f0d0017

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeTitle:Landroid/widget/TextView;

    .line 1153
    const v4, 0x7f0d0018

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    .line 1154
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectAllOnFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1155
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    new-instance v5, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;

    invoke-direct {v5, p0, v9}, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;-><init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1158
    const v4, 0x7f0d0019

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    .line 1159
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1162
    const v4, 0x7f0d000b

    invoke-virtual {p0, v4}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    .line 1163
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1164
    return-void

    .end local v2    # "orientationLabels":[Ljava/lang/String;
    .end local v3    # "rangeOptionsSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;>;"
    :cond_0
    move v4, v6

    .line 1112
    goto/16 :goto_0
.end method

.method private static canPrint(Landroid/print/PrinterInfo;)Z
    .locals 2
    .param p0, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 1184
    invoke-virtual {p0}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canUpdateDocument()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1607
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v5}, Lcom/android/printspooler/model/RemotePrintDocument;->isDestroyed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1643
    :cond_0
    :goto_0
    return v3

    .line 1611
    :cond_1
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1615
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v5}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v0

    .line 1617
    .local v0, "attributes":Landroid/print/PrintAttributes;
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v2

    .line 1618
    .local v2, "colorMode":I
    const/4 v5, 0x2

    if-eq v2, v5, :cond_2

    if-ne v2, v4, :cond_0

    .line 1622
    :cond_2
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1625
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->getMinMargins()Landroid/print/PrintAttributes$Margins;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1628
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->getResolution()Landroid/print/PrintAttributes$Resolution;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1632
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    if-eqz v5, :cond_0

    .line 1635
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v5}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v1

    .line 1636
    .local v1, "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    if-eqz v1, :cond_0

    .line 1639
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v5}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_0

    move v3, v4

    .line 1643
    goto :goto_0
.end method

.method private cancelPrint()V
    .locals 1

    .prologue
    .line 1053
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 1054
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 1055
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->isUpdating()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1056
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->cancel()V

    .line 1058
    :cond_0
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->doFinish()V

    .line 1059
    return-void
.end method

.method private checkPluginsState()V
    .locals 6

    .prologue
    .line 268
    const-string v3, "print"

    invoke-virtual {p0, v3}, Lcom/android/printspooler/ui/PrintActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrintManager;

    .line 269
    .local v1, "printManager":Landroid/print/PrintManager;
    const-string v3, "PrintActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enabled Plugin services: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/print/PrintManager;->getEnabledPrintServices()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    invoke-virtual {v1}, Landroid/print/PrintManager;->getEnabledPrintServices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/printservice/PrintServiceInfo;

    .line 271
    .local v2, "service":Landroid/printservice/PrintServiceInfo;
    const-string v3, "PrintActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    .end local v2    # "service":Landroid/printservice/PrintServiceInfo;
    :cond_0
    return-void
.end method

.method private computeSelectedPages()[Landroid/print/PageRange;
    .locals 10

    .prologue
    .line 1519
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1520
    const/4 v8, 0x0

    .line 1562
    :goto_0
    return-object v8

    .line 1523
    :cond_0
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v8

    if-lez v8, :cond_5

    .line 1524
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1525
    .local v4, "pageRanges":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity;->mStringCommaSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 1527
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity;->mStringCommaSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

    invoke-virtual {v8}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1528
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity;->mStringCommaSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

    invoke-virtual {v8}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1529
    .local v6, "range":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1532
    const/16 v8, 0x2d

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1536
    .local v0, "dashIndex":I
    if-lez v0, :cond_3

    .line 1537
    const/4 v8, 0x0

    invoke-virtual {v6, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v1, v8, -0x1

    .line 1541
    .local v1, "fromIndex":I
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v0, v8, :cond_2

    .line 1542
    add-int/lit8 v8, v0, 0x1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1543
    .local v2, "fromString":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v7, v8, -0x1

    .line 1551
    .end local v2    # "fromString":Ljava/lang/String;
    .local v7, "toIndex":I
    :goto_2
    new-instance v3, Landroid/print/PageRange;

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-direct {v3, v8, v9}, Landroid/print/PageRange;-><init>(II)V

    .line 1553
    .local v3, "pageRange":Landroid/print/PageRange;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1545
    .end local v3    # "pageRange":Landroid/print/PageRange;
    .end local v7    # "toIndex":I
    :cond_2
    move v7, v1

    .restart local v7    # "toIndex":I
    goto :goto_2

    .line 1548
    .end local v1    # "fromIndex":I
    .end local v7    # "toIndex":I
    :cond_3
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v7, v8, -0x1

    .restart local v7    # "toIndex":I
    move v1, v7

    .restart local v1    # "fromIndex":I
    goto :goto_2

    .line 1556
    .end local v0    # "dashIndex":I
    .end local v1    # "fromIndex":I
    .end local v6    # "range":Ljava/lang/String;
    .end local v7    # "toIndex":I
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    new-array v5, v8, [Landroid/print/PageRange;

    .line 1557
    .local v5, "pageRangesArray":[Landroid/print/PageRange;
    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1559
    invoke-static {v5}, Lcom/android/printspooler/util/PageRangeUtils;->normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;

    move-result-object v8

    goto/16 :goto_0

    .line 1562
    .end local v4    # "pageRanges":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    .end local v5    # "pageRangesArray":[Landroid/print/PageRange;
    :cond_5
    sget-object v8, Lcom/android/printspooler/ui/PrintActivity;->ALL_PAGES_ARRAY:[Landroid/print/PageRange;

    goto/16 :goto_0
.end method

.method private confirmPrint()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1062
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 1064
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 1065
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->addCurrentPrinterToHistory()V

    .line 1067
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->computeSelectedPages()[Landroid/print/PageRange;

    move-result-object v0

    .line 1068
    .local v0, "selectedPages":[Landroid/print/PageRange;
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1069
    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    .line 1071
    invoke-direct {p0, v2}, Lcom/android/printspooler/ui/PrintActivity;->updatePrintPreviewController(Z)V

    .line 1074
    :cond_0
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateSelectedPagesFromPreview()V

    .line 1075
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintPreviewController;->closeOptions()V

    .line 1077
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1078
    invoke-direct {p0, v2}, Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z

    .line 1081
    :cond_1
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v1}, Lcom/android/printspooler/model/RemotePrintDocument;->isUpdating()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1082
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->requestCreatePdfFileOrFinish()V

    .line 1084
    :cond_2
    return-void
.end method

.method private doFinish()V
    .locals 2

    .prologue
    .line 1661
    iget v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    if-eqz v0, :cond_1

    .line 1662
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->cancel()V

    .line 1663
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrinterRegistry;->setTrackedPrinter(Landroid/print/PrinterId;)V

    .line 1664
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mSpoolerProvider:Lcom/android/printspooler/model/PrintSpoolerProvider;

    invoke-virtual {v0}, Lcom/android/printspooler/model/PrintSpoolerProvider;->destroy()V

    .line 1665
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->isUpdating()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1666
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->cancel()V

    .line 1667
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->finish()V

    .line 1668
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->destroy()V

    .line 1669
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    new-instance v1, Lcom/android/printspooler/ui/PrintActivity$8;

    invoke-direct {v1, p0}, Lcom/android/printspooler/ui/PrintActivity$8;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrintPreviewController;->destroy(Ljava/lang/Runnable;)V

    .line 1678
    :goto_0
    return-void

    .line 1676
    :cond_1
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->finish()V

    goto :goto_0
.end method

.method private ensureErrorUiShown(Ljava/lang/CharSequence;I)V
    .locals 3
    .param p1, "message"    # Ljava/lang/CharSequence;
    .param p2, "action"    # I

    .prologue
    const/4 v2, 0x1

    .line 928
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 937
    :cond_0
    :goto_0
    return-void

    .line 931
    :cond_1
    iget v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mUiState:I

    if-eq v1, v2, :cond_0

    .line 932
    iput v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mUiState:I

    .line 933
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v1, v2}, Lcom/android/printspooler/ui/PrintPreviewController;->setUiShown(Z)V

    .line 934
    invoke-static {p1, p2}, Lcom/android/printspooler/ui/PrintErrorFragment;->newInstance(Ljava/lang/CharSequence;I)Lcom/android/printspooler/ui/PrintErrorFragment;

    move-result-object v0

    .line 935
    .local v0, "fragment":Landroid/app/Fragment;
    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->showFragment(Landroid/app/Fragment;)V

    goto :goto_0
.end method

.method private ensurePreviewUiShown()V
    .locals 2

    .prologue
    .line 917
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 925
    :cond_0
    :goto_0
    return-void

    .line 920
    :cond_1
    iget v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mUiState:I

    if-eqz v0, :cond_0

    .line 921
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mUiState:I

    .line 922
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrintPreviewController;->setUiShown(Z)V

    .line 923
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->showFragment(Landroid/app/Fragment;)V

    goto :goto_0
.end method

.method private ensureProgressUiShown()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 905
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 914
    :cond_0
    :goto_0
    return-void

    .line 908
    :cond_1
    iget v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mUiState:I

    if-eq v1, v2, :cond_0

    .line 909
    iput v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mUiState:I

    .line 910
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/printspooler/ui/PrintPreviewController;->setUiShown(Z)V

    .line 911
    invoke-static {}, Lcom/android/printspooler/ui/PrintProgressFragment;->newInstance()Lcom/android/printspooler/ui/PrintProgressFragment;

    move-result-object v0

    .line 912
    .local v0, "fragment":Landroid/app/Fragment;
    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->showFragment(Landroid/app/Fragment;)V

    goto :goto_0
.end method

.method private getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I
    .locals 2
    .param p1, "info"    # Landroid/print/PrintDocumentInfo;

    .prologue
    .line 1566
    if-eqz p1, :cond_0

    .line 1567
    invoke-virtual {p1}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v0

    .line 1568
    .local v0, "pageCount":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1574
    .end local v0    # "pageCount":I
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintPreviewController;->getFilePageCount()I

    move-result v0

    goto :goto_0
.end method

.method private hasErrors()Z
    .locals 1

    .prologue
    .line 1578
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isFinalState(I)Z
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 830
    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAdvancedPrintOptionsActivityResult(ILandroid/content/Intent;)V
    .locals 26
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 721
    const/16 v24, -0x1

    move/from16 v0, p1

    move/from16 v1, v24

    if-ne v0, v1, :cond_0

    if-nez p2, :cond_1

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 725
    :cond_1
    const-string v24, "android.intent.extra.print.PRINT_JOB_INFO"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Landroid/print/PrintJobInfo;

    .line 727
    .local v16, "printJobInfo":Landroid/print/PrintJobInfo;
    if-eqz v16, :cond_0

    .line 732
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    move-object/from16 v24, v0

    invoke-virtual/range {v16 .. v16}, Landroid/print/PrintJobInfo;->getAdvancedOptions()Landroid/os/Bundle;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Landroid/print/PrintJobInfo;->setAdvancedOptions(Landroid/os/Bundle;)V

    .line 736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v24, v0

    invoke-virtual/range {v16 .. v16}, Landroid/print/PrintJobInfo;->getCopies()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 737
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    move-object/from16 v24, v0

    invoke-virtual/range {v16 .. v16}, Landroid/print/PrintJobInfo;->getCopies()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Landroid/print/PrintJobInfo;->setCopies(I)V

    .line 739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v3

    .line 740
    .local v3, "currAttributes":Landroid/print/PrintAttributes;
    invoke-virtual/range {v16 .. v16}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v8

    .line 743
    .local v8, "newAttributes":Landroid/print/PrintAttributes;
    invoke-virtual {v3}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v12

    .line 744
    .local v12, "oldMediaSize":Landroid/print/PrintAttributes$MediaSize;
    invoke-virtual {v8}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v10

    .line 745
    .local v10, "newMediaSize":Landroid/print/PrintAttributes$MediaSize;
    invoke-virtual {v12, v10}, Landroid/print/PrintAttributes$MediaSize;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    .line 746
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v7

    .line 747
    .local v7, "mediaSizeCount":I
    invoke-virtual {v8}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/print/PrintAttributes$MediaSize;->asPortrait()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v11

    .line 748
    .local v11, "newMediaSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v7, :cond_2

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    move-object/from16 v24, v0

    check-cast v24, Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual/range {v24 .. v24}, Landroid/print/PrintAttributes$MediaSize;->asPortrait()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v21

    .line 751
    .local v21, "supportedSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Landroid/print/PrintAttributes$MediaSize;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 752
    invoke-virtual {v3, v10}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    .line 753
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 754
    invoke-virtual {v3}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/print/PrintAttributes$MediaSize;->isPortrait()Z

    move-result v24

    if-eqz v24, :cond_6

    .line 755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v24

    if-eqz v24, :cond_2

    .line 756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/Spinner;->setSelection(I)V

    .line 769
    .end local v5    # "i":I
    .end local v7    # "mediaSizeCount":I
    .end local v11    # "newMediaSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    .end local v21    # "supportedSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    :cond_2
    :goto_2
    invoke-virtual {v3}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v4

    .line 770
    .local v4, "currColorMode":I
    invoke-virtual {v8}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v9

    .line 771
    .local v9, "newColorMode":I
    if-eq v4, v9, :cond_3

    .line 772
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/Spinner;->getCount()I

    move-result v2

    .line 773
    .local v2, "colorModeCount":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    if-ge v5, v2, :cond_3

    .line 774
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    move-object/from16 v24, v0

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v20

    .line 775
    .local v20, "supportedColorMode":I
    move/from16 v0, v20

    if-ne v0, v9, :cond_8

    .line 776
    invoke-virtual {v3, v9}, Landroid/print/PrintAttributes;->setColorMode(I)V

    .line 777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 784
    .end local v2    # "colorModeCount":I
    .end local v5    # "i":I
    .end local v20    # "supportedColorMode":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v6, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    .line 785
    .local v6, "info":Landroid/print/PrintDocumentInfo;
    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/printspooler/ui/PrintActivity;->getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I

    move-result v13

    .line 786
    .local v13, "pageCount":I
    :goto_4
    invoke-virtual/range {v16 .. v16}, Landroid/print/PrintJobInfo;->getPages()[Landroid/print/PageRange;

    move-result-object v15

    .line 787
    .local v15, "pageRanges":[Landroid/print/PageRange;
    if-eqz v15, :cond_5

    if-lez v13, :cond_5

    .line 788
    invoke-static {v15}, Lcom/android/printspooler/util/PageRangeUtils;->normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;

    move-result-object v15

    .line 790
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 791
    .local v23, "validatedList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    array-length v0, v15

    move/from16 v17, v0

    .line 792
    .local v17, "rangeCount":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_5
    move/from16 v0, v17

    if-ge v5, v0, :cond_4

    .line 793
    aget-object v14, v15, v5

    .line 794
    .local v14, "pageRange":Landroid/print/PageRange;
    invoke-virtual {v14}, Landroid/print/PageRange;->getEnd()I

    move-result v24

    move/from16 v0, v24

    if-lt v0, v13, :cond_a

    .line 795
    invoke-virtual {v14}, Landroid/print/PageRange;->getStart()I

    move-result v19

    .line 796
    .local v19, "rangeStart":I
    add-int/lit8 v18, v13, -0x1

    .line 797
    .local v18, "rangeEnd":I
    move/from16 v0, v19

    move/from16 v1, v18

    if-gt v0, v1, :cond_4

    .line 798
    new-instance v14, Landroid/print/PageRange;

    .end local v14    # "pageRange":Landroid/print/PageRange;
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-direct {v14, v0, v1}, Landroid/print/PageRange;-><init>(II)V

    .line 799
    .restart local v14    # "pageRange":Landroid/print/PageRange;
    move-object/from16 v0, v23

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 806
    .end local v14    # "pageRange":Landroid/print/PageRange;
    .end local v18    # "rangeEnd":I
    .end local v19    # "rangeStart":I
    :cond_4
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_5

    .line 807
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v24

    move/from16 v0, v24

    new-array v0, v0, [Landroid/print/PageRange;

    move-object/from16 v22, v0

    .line 808
    .local v22, "validatedArray":[Landroid/print/PageRange;
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 809
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v13}, Lcom/android/printspooler/ui/PrintActivity;->updateSelectedPages([Landroid/print/PageRange;I)V

    .line 814
    .end local v5    # "i":I
    .end local v17    # "rangeCount":I
    .end local v22    # "validatedArray":[Landroid/print/PageRange;
    .end local v23    # "validatedList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z

    move-result v24

    if-eqz v24, :cond_0

    .line 815
    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z

    goto/16 :goto_0

    .line 759
    .end local v4    # "currColorMode":I
    .end local v6    # "info":Landroid/print/PrintDocumentInfo;
    .end local v9    # "newColorMode":I
    .end local v13    # "pageCount":I
    .end local v15    # "pageRanges":[Landroid/print/PageRange;
    .restart local v5    # "i":I
    .restart local v7    # "mediaSizeCount":I
    .restart local v11    # "newMediaSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    .restart local v21    # "supportedSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_2

    .line 760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_2

    .line 748
    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 773
    .end local v7    # "mediaSizeCount":I
    .end local v11    # "newMediaSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    .end local v21    # "supportedSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    .restart local v2    # "colorModeCount":I
    .restart local v4    # "currColorMode":I
    .restart local v9    # "newColorMode":I
    .restart local v20    # "supportedColorMode":I
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 785
    .end local v2    # "colorModeCount":I
    .end local v5    # "i":I
    .end local v20    # "supportedColorMode":I
    .restart local v6    # "info":Landroid/print/PrintDocumentInfo;
    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_4

    .line 803
    .restart local v5    # "i":I
    .restart local v13    # "pageCount":I
    .restart local v14    # "pageRange":Landroid/print/PageRange;
    .restart local v15    # "pageRanges":[Landroid/print/PageRange;
    .restart local v17    # "rangeCount":I
    .restart local v23    # "validatedList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    :cond_a
    move-object/from16 v0, v23

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 792
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_5
.end method

.method private onConnectedToPrintSpooler(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "documentAdapter"    # Landroid/os/IBinder;

    .prologue
    .line 281
    new-instance v0, Lcom/android/printspooler/ui/PrinterRegistry;

    new-instance v1, Lcom/android/printspooler/ui/PrintActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/android/printspooler/ui/PrintActivity$2;-><init>(Lcom/android/printspooler/ui/PrintActivity;Landroid/os/IBinder;)V

    invoke-direct {v0, p0, v1}, Lcom/android/printspooler/ui/PrinterRegistry;-><init>(Landroid/app/Activity;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    .line 288
    return-void
.end method

.method private onPrinterRegistryReady(Landroid/os/IBinder;)V
    .locals 7
    .param p1, "documentAdapter"    # Landroid/os/IBinder;

    .prologue
    .line 294
    const v0, 0x7f090019

    invoke-virtual {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setTitle(I)V

    .line 295
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setContentView(I)V

    .line 298
    :try_start_0
    new-instance v0, Lcom/android/printspooler/model/MutexFileProvider;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/printspooler/model/PrintSpoolerService;->generateFileForPrintJob(Landroid/content/Context;Landroid/print/PrintJobId;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/printspooler/model/MutexFileProvider;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    new-instance v0, Lcom/android/printspooler/ui/PrintPreviewController;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    invoke-direct {v0, p0, v1}, Lcom/android/printspooler/ui/PrintPreviewController;-><init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/model/MutexFileProvider;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    .line 308
    new-instance v0, Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-static {p1}, Landroid/print/IPrintDocumentAdapter$Stub;->asInterface(Landroid/os/IBinder;)Landroid/print/IPrintDocumentAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    new-instance v4, Lcom/android/printspooler/ui/PrintActivity$3;

    invoke-direct {v4, p0}, Lcom/android/printspooler/ui/PrintActivity$3;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/printspooler/model/RemotePrintDocument;-><init>(Landroid/content/Context;Landroid/print/IPrintDocumentAdapter;Lcom/android/printspooler/model/MutexFileProvider;Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;Lcom/android/printspooler/model/RemotePrintDocument$UpdateResultCallbacks;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    .line 323
    new-instance v0, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    invoke-direct {v0, p0, p0}, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;-><init>(Lcom/android/printspooler/ui/PrintActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    .line 325
    new-instance v0, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;

    invoke-direct {v0, p0}, Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeComparator:Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;

    .line 326
    new-instance v0, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    invoke-direct {v0, p0}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    .line 328
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->bindUi()V

    .line 329
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 332
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mOptionsContent:Lcom/android/printspooler/widget/PrintContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/printspooler/widget/PrintContentView;->setVisibility(I)V

    .line 333
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->computeSelectedPages()[Landroid/print/PageRange;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    .line 334
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->start()V

    .line 336
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->ensurePreviewUiShown()V

    .line 338
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 339
    return-void

    .line 301
    :catch_0
    move-exception v6

    .line 303
    .local v6, "ioe":Ljava/io/IOException;
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot create print job file"

    invoke-direct {v0, v1, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private onSelectPrinterActivityResult(ILandroid/content/Intent;)V
    .locals 4
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 671
    if-ne p1, v3, :cond_1

    if-eqz p2, :cond_1

    .line 672
    const-string v2, "INTENT_EXTRA_PRINTER_ID"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/print/PrinterId;

    .line 673
    .local v1, "printerId":Landroid/print/PrinterId;
    if-eqz v1, :cond_1

    .line 674
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    invoke-virtual {v2, v1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->ensurePrinterInVisibleAdapterPosition(Landroid/print/PrinterId;)V

    .line 675
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    invoke-virtual {v2, v1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPrinterIndex(Landroid/print/PrinterId;)I

    move-result v0

    .line 676
    .local v0, "index":I
    if-eq v0, v3, :cond_1

    .line 677
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 687
    .end local v0    # "index":I
    .end local v1    # "printerId":Landroid/print/PrinterId;
    :cond_0
    :goto_0
    return-void

    .line 682
    :cond_1
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    if-eqz v2, :cond_0

    .line 683
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v2}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v1

    .line 684
    .restart local v1    # "printerId":Landroid/print/PrinterId;
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    invoke-virtual {v2, v1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPrinterIndex(Landroid/print/PrinterId;)I

    move-result v0

    .line 685
    .restart local v0    # "index":I
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method

.method private onStartCreateDocumentActivityResult(ILandroid/content/Intent;)V
    .locals 6
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 630
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    if-eqz p2, :cond_0

    .line 631
    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 632
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 633
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 636
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    new-instance v3, Lcom/android/printspooler/ui/PrintActivity$4;

    invoke-direct {v3, p0, v1}, Lcom/android/printspooler/ui/PrintActivity$4;-><init>(Lcom/android/printspooler/ui/PrintActivity;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->post(Ljava/lang/Runnable;)Z

    .line 663
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 642
    :cond_0
    if-nez p1, :cond_1

    .line 643
    const/4 v2, 0x1

    iput v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    .line 644
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 645
    .local v0, "languageHandler":Landroid/os/Handler;
    new-instance v2, Lcom/android/printspooler/ui/PrintActivity$5;

    invoke-direct {v2, p0}, Lcom/android/printspooler/ui/PrintActivity$5;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 652
    .end local v0    # "languageHandler":Landroid/os/Handler;
    :cond_1
    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 653
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 656
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    new-instance v3, Lcom/android/printspooler/ui/PrintActivity$6;

    invoke-direct {v3, p0}, Lcom/android/printspooler/ui/PrintActivity$6;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private requestCreatePdfFileOrFinish()V
    .locals 2

    .prologue
    .line 955
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPdfPrinter()Landroid/print/PrinterInfo;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 956
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->startCreateDocumentActivity()V

    .line 960
    :goto_0
    return-void

    .line 958
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->shredPagesAndFinish(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private setState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 820
    iget v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->isFinalState(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 821
    invoke-static {p1}, Lcom/android/printspooler/ui/PrintActivity;->isFinalState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 822
    iput p1, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    .line 827
    :cond_0
    :goto_0
    return-void

    .line 825
    :cond_1
    iput p1, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    goto :goto_0
.end method

.method private showFragment(Landroid/app/Fragment;)V
    .locals 4
    .param p1, "newFragment"    # Landroid/app/Fragment;

    .prologue
    .line 940
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 941
    .local v1, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_TAG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 942
    .local v0, "oldFragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 943
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 945
    :cond_0
    if-eqz p1, :cond_1

    .line 946
    const v2, 0x7f0d000c

    const-string v3, "FRAGMENT_TAG"

    invoke-virtual {v1, v2, p1, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 948
    :cond_1
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_2

    .line 949
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 950
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 952
    :cond_2
    return-void
.end method

.method private shredPagesAndFinish(Landroid/net/Uri;)V
    .locals 4
    .param p1, "writeToUri"    # Landroid/net/Uri;

    .prologue
    .line 1647
    new-instance v0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    new-instance v3, Lcom/android/printspooler/ui/PrintActivity$7;

    invoke-direct {v3, p0, p1}, Lcom/android/printspooler/ui/PrintActivity$7;-><init>(Lcom/android/printspooler/ui/PrintActivity;Landroid/net/Uri;)V

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;-><init>(Landroid/content/Context;Landroid/print/PrintJobInfo;Lcom/android/printspooler/model/MutexFileProvider;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->shred()V

    .line 1656
    return-void
.end method

.method private startAdvancedPrintOptionsActivity(Landroid/print/PrinterInfo;)V
    .locals 8
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    const/4 v7, 0x0

    .line 690
    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v5

    invoke-virtual {v5}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v4

    .line 692
    .local v4, "serviceName":Landroid/content/ComponentName;
    invoke-static {p0, v4}, Lcom/android/printspooler/util/PrintOptionUtils;->getAdvancedOptionsActivityName(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 693
    .local v0, "activityName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 718
    :cond_0
    :goto_0
    return-void

    .line 697
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 698
    .local v2, "intent":Landroid/content/Intent;
    new-instance v5, Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 700
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v2, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 702
    .local v3, "resolvedActivities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 707
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v5, v5, Landroid/content/pm/ActivityInfo;->exported:Z

    if-eqz v5, :cond_0

    .line 708
    const-string v5, "android.intent.extra.print.PRINT_JOB_INFO"

    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 709
    const-string v5, "android.intent.extra.print.EXTRA_PRINTER_INFO"

    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 713
    const/4 v5, 0x3

    :try_start_0
    invoke-virtual {p0, v2, v5}, Lcom/android/printspooler/ui/PrintActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 714
    :catch_0
    move-exception v1

    .line 715
    .local v1, "anfe":Landroid/content/ActivityNotFoundException;
    const-string v5, "PrintActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error starting activity for intent: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private startCreateDocumentActivity()V
    .locals 5

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->isResumed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 615
    :cond_1
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v2}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v2

    iget-object v0, v2, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    .line 616
    .local v0, "info":Landroid/print/PrintDocumentInfo;
    if-eqz v0, :cond_0

    .line 619
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CREATE_DOCUMENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 620
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "application/pdf"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 621
    invoke-virtual {v0}, Landroid/print/PrintDocumentInfo;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".pdf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 622
    const-string v2, "android.intent.extra.TITLE"

    invoke-virtual {v0}, Landroid/print/PrintDocumentInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 625
    :goto_1
    const-string v2, "android.content.extra.PACKAGE_NAME"

    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity;->mCallingPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 626
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/android/printspooler/ui/PrintActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 624
    :cond_2
    const-string v2, "android.intent.extra.TITLE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/print/PrintDocumentInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".pdf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private startSelectPrinterActivity()V
    .locals 2

    .prologue
    .line 666
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 667
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 668
    return-void
.end method

.method private updateDocument(Z)Z
    .locals 7
    .param p1, "clearLastError"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1012
    if-nez p1, :cond_1

    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v5}, Lcom/android/printspooler/model/RemotePrintDocument;->hasUpdateError()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1040
    :cond_0
    :goto_0
    return v3

    .line 1016
    :cond_1
    if-eqz p1, :cond_2

    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v5}, Lcom/android/printspooler/model/RemotePrintDocument;->hasUpdateError()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1017
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v5}, Lcom/android/printspooler/model/RemotePrintDocument;->clearUpdateError()V

    .line 1020
    :cond_2
    iget v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_3

    move v1, v4

    .line 1022
    .local v1, "preview":Z
    :goto_1
    if-eqz v1, :cond_4

    .line 1023
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v5}, Lcom/android/printspooler/ui/PrintPreviewController;->getRequestedPages()[Landroid/print/PageRange;

    move-result-object v0

    .line 1028
    .local v0, "pages":[Landroid/print/PageRange;
    :goto_2
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v6

    invoke-virtual {v5, v6, v0, v1}, Lcom/android/printspooler/model/RemotePrintDocument;->update(Landroid/print/PrintAttributes;[Landroid/print/PageRange;Z)Z

    move-result v2

    .line 1031
    .local v2, "willUpdate":Z
    if-eqz v2, :cond_5

    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v5}, Lcom/android/printspooler/model/RemotePrintDocument;->hasLaidOutPages()Z

    move-result v5

    if-nez v5, :cond_5

    .line 1033
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity;->mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    invoke-virtual {v3}, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->post()V

    move v3, v4

    .line 1034
    goto :goto_0

    .end local v0    # "pages":[Landroid/print/PageRange;
    .end local v1    # "preview":Z
    .end local v2    # "willUpdate":Z
    :cond_3
    move v1, v3

    .line 1020
    goto :goto_1

    .line 1025
    .restart local v1    # "preview":Z
    :cond_4
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v5}, Lcom/android/printspooler/ui/PrintPreviewController;->getSelectedPages()[Landroid/print/PageRange;

    move-result-object v0

    .restart local v0    # "pages":[Landroid/print/PageRange;
    goto :goto_2

    .line 1035
    .restart local v2    # "willUpdate":Z
    :cond_5
    if-nez v2, :cond_0

    .line 1037
    invoke-direct {p0, v3}, Lcom/android/printspooler/ui/PrintActivity;->updatePrintPreviewController(Z)V

    goto :goto_0
.end method

.method private updatePageRangeOptions(I)V
    .locals 11
    .param p1, "pageCount"    # I

    .prologue
    const v10, 0x7f090010

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1492
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v4

    check-cast v4, Landroid/widget/ArrayAdapter;

    .line 1494
    .local v4, "rangeOptionsSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;>;"
    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->clear()V

    .line 1496
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f040002

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v5

    .line 1499
    .local v5, "rangeOptionsValues":[I
    if-lez p1, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 1500
    .local v1, "pageCountLabel":Ljava/lang/String;
    :goto_0
    const/4 v6, 0x2

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v8

    const v6, 0x7f090011

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/android/printspooler/ui/PrintActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v9

    .line 1504
    .local v3, "rangeOptionsLabels":[Ljava/lang/String;
    aget-object v6, v3, v8

    const-string v7, "%d"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1505
    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v10, v6}, Lcom/android/printspooler/ui/PrintActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v8

    .line 1511
    :goto_1
    array-length v2, v3

    .line 1512
    .local v2, "rangeOptionsCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, v2, :cond_2

    .line 1513
    new-instance v6, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    aget v7, v5, v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aget-object v8, v3, v0

    invoke-direct {v6, p0, v7, v8}, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;-><init>(Lcom/android/printspooler/ui/PrintActivity;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1512
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1499
    .end local v0    # "i":I
    .end local v1    # "pageCountLabel":Ljava/lang/String;
    .end local v2    # "rangeOptionsCount":I
    .end local v3    # "rangeOptionsLabels":[Ljava/lang/String;
    :cond_0
    const-string v1, ""

    goto :goto_0

    .line 1508
    .restart local v1    # "pageCountLabel":Ljava/lang/String;
    .restart local v3    # "rangeOptionsLabels":[Ljava/lang/String;
    :cond_1
    new-array v6, v9, [Ljava/lang/Object;

    aput-object v1, v6, v8

    invoke-virtual {p0, v10, v6}, Lcom/android/printspooler/ui/PrintActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v8

    goto :goto_1

    .line 1516
    .restart local v0    # "i":I
    .restart local v2    # "rangeOptionsCount":I
    :cond_2
    return-void
.end method

.method private updatePrintAttributesFromCapabilities(Landroid/print/PrinterCapabilitiesInfo;)V
    .locals 12
    .param p1, "capabilities"    # Landroid/print/PrinterCapabilitiesInfo;

    .prologue
    .line 963
    invoke-virtual {p1}, Landroid/print/PrinterCapabilitiesInfo;->getDefaults()Landroid/print/PrintAttributes;

    move-result-object v4

    .line 966
    .local v4, "defaults":Landroid/print/PrintAttributes;
    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/print/PrinterCapabilitiesInfo;->getMediaSizes()Ljava/util/List;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 967
    .local v10, "sortedMediaSizes":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintAttributes$MediaSize;>;"
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeComparator:Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;

    invoke-static {v10, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 969
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v11}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v0

    .line 972
    .local v0, "attributes":Landroid/print/PrintAttributes;
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v2

    .line 973
    .local v2, "currMediaSize":Landroid/print/PrintAttributes$MediaSize;
    if-nez v2, :cond_4

    .line 974
    invoke-virtual {v4}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v11

    invoke-virtual {v0, v11}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    .line 996
    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v1

    .line 997
    .local v1, "colorMode":I
    invoke-virtual {p1}, Landroid/print/PrinterCapabilitiesInfo;->getColorModes()I

    move-result v11

    and-int/2addr v11, v1

    if-nez v11, :cond_1

    .line 998
    invoke-virtual {v4}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v11

    invoke-virtual {v0, v11}, Landroid/print/PrintAttributes;->setColorMode(I)V

    .line 1002
    :cond_1
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->getResolution()Landroid/print/PrintAttributes$Resolution;

    move-result-object v9

    .line 1003
    .local v9, "resolution":Landroid/print/PrintAttributes$Resolution;
    if-eqz v9, :cond_2

    invoke-virtual {p1}, Landroid/print/PrinterCapabilitiesInfo;->getResolutions()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 1004
    :cond_2
    invoke-virtual {v4}, Landroid/print/PrintAttributes;->getResolution()Landroid/print/PrintAttributes$Resolution;

    move-result-object v11

    invoke-virtual {v0, v11}, Landroid/print/PrintAttributes;->setResolution(Landroid/print/PrintAttributes$Resolution;)V

    .line 1008
    :cond_3
    invoke-virtual {v4}, Landroid/print/PrintAttributes;->getMinMargins()Landroid/print/PrintAttributes$Margins;

    move-result-object v11

    invoke-virtual {v0, v11}, Landroid/print/PrintAttributes;->setMinMargins(Landroid/print/PrintAttributes$Margins;)V

    .line 1009
    return-void

    .line 976
    .end local v1    # "colorMode":I
    .end local v9    # "resolution":Landroid/print/PrintAttributes$Resolution;
    :cond_4
    const/4 v5, 0x0

    .line 979
    .local v5, "foundCurrentMediaSize":Z
    invoke-virtual {v2}, Landroid/print/PrintAttributes$MediaSize;->asPortrait()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v3

    .line 980
    .local v3, "currMediaSizePortrait":Landroid/print/PrintAttributes$MediaSize;
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v8

    .line 981
    .local v8, "mediaSizeCount":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v8, :cond_5

    .line 982
    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/print/PrintAttributes$MediaSize;

    .line 983
    .local v7, "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    invoke-virtual {v7}, Landroid/print/PrintAttributes$MediaSize;->asPortrait()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v11

    invoke-virtual {v3, v11}, Landroid/print/PrintAttributes$MediaSize;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 984
    invoke-virtual {v0, v2}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    .line 985
    const/4 v5, 0x1

    .line 990
    .end local v7    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    :cond_5
    if-nez v5, :cond_0

    .line 991
    invoke-virtual {v4}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v11

    invoke-virtual {v0, v11}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    goto :goto_0

    .line 981
    .restart local v7    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method private updatePrintPreviewController(Z)V
    .locals 8
    .param p1, "contentUpdated"    # Z

    .prologue
    .line 559
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v7

    .line 560
    .local v7, "documentInfo":Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;
    iget-boolean v0, v7, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->laidout:Z

    if-nez v0, :cond_0

    .line 570
    :goto_0
    return-void

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    iget-object v1, v7, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-direct {p0, v1}, Lcom/android/printspooler/ui/PrintActivity;->getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I

    move-result v2

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v1}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v1

    iget-object v3, v1, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->writtenPages:[Landroid/print/PageRange;

    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v5

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintAttributes;->getMinMargins()Landroid/print/PrintAttributes$Margins;

    move-result-object v6

    move v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/android/printspooler/ui/PrintPreviewController;->onContentUpdated(ZI[Landroid/print/PageRange;[Landroid/print/PageRange;Landroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V

    goto :goto_0
.end method

.method private updateSelectedPages([Landroid/print/PageRange;I)V
    .locals 9
    .param p1, "selectedPages"    # [Landroid/print/PageRange;
    .param p2, "pageInDocumentCount"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 844
    if-eqz p1, :cond_0

    array-length v6, p1

    if-gtz v6, :cond_1

    .line 902
    :cond_0
    :goto_0
    return-void

    .line 848
    :cond_1
    invoke-static {p1}, Lcom/android/printspooler/util/PageRangeUtils;->normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;

    move-result-object p1

    .line 852
    invoke-static {p1, p2}, Lcom/android/printspooler/util/PageRangeUtils;->isAllPages([Landroid/print/PageRange;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 853
    new-array p1, v8, [Landroid/print/PageRange;

    .end local p1    # "selectedPages":[Landroid/print/PageRange;
    sget-object v6, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v6, p1, v7

    .line 856
    .restart local p1    # "selectedPages":[Landroid/print/PageRange;
    :cond_2
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    invoke-static {v6, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 860
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    .line 861
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v6, p1}, Landroid/print/PrintJobInfo;->setPages([Landroid/print/PageRange;)V

    .line 863
    sget-object v6, Lcom/android/printspooler/ui/PrintActivity;->ALL_PAGES_ARRAY:[Landroid/print/PageRange;

    invoke-static {p1, v6}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 864
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v6

    if-eqz v6, :cond_0

    .line 865
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v7}, Landroid/widget/Spinner;->setSelection(I)V

    .line 866
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 868
    :cond_3
    aget-object v6, p1, v7

    invoke-virtual {v6}, Landroid/print/PageRange;->getStart()I

    move-result v6

    if-ltz v6, :cond_0

    array-length v6, p1

    add-int/lit8 v6, v6, -0x1

    aget-object v6, p1, v6

    invoke-virtual {v6}, Landroid/print/PageRange;->getEnd()I

    move-result v6

    if-ge v6, p2, :cond_0

    .line 870
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v6

    if-eq v6, v8, :cond_4

    .line 871
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v8}, Landroid/widget/Spinner;->setSelection(I)V

    .line 874
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 875
    .local v0, "builder":Ljava/lang/StringBuilder;
    array-length v3, p1

    .line 876
    .local v3, "pageRangeCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_8

    .line 877
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_5

    .line 878
    const/16 v6, 0x2c

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 883
    :cond_5
    aget-object v2, p1, v1

    .line 884
    .local v2, "pageRange":Landroid/print/PageRange;
    sget-object v6, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    invoke-virtual {v2, v6}, Landroid/print/PageRange;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 885
    const/4 v5, 0x1

    .line 886
    .local v5, "shownStartPage":I
    move v4, p2

    .line 892
    .local v4, "shownEndPage":I
    :goto_2
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 894
    if-eq v5, v4, :cond_6

    .line 895
    const/16 v6, 0x2d

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 896
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 876
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 888
    .end local v4    # "shownEndPage":I
    .end local v5    # "shownStartPage":I
    :cond_7
    invoke-virtual {v2}, Landroid/print/PageRange;->getStart()I

    move-result v6

    add-int/lit8 v5, v6, 0x1

    .line 889
    .restart local v5    # "shownStartPage":I
    invoke-virtual {v2}, Landroid/print/PageRange;->getEnd()I

    move-result v6

    add-int/lit8 v4, v6, 0x1

    .restart local v4    # "shownEndPage":I
    goto :goto_2

    .line 900
    .end local v2    # "pageRange":Landroid/print/PageRange;
    .end local v4    # "shownEndPage":I
    .end local v5    # "shownStartPage":I
    :cond_8
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private updateSelectedPagesFromPreview()V
    .locals 2

    .prologue
    .line 836
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintPreviewController;->getSelectedPages()[Landroid/print/PageRange;

    move-result-object v0

    .line 837
    .local v0, "selectedPages":[Landroid/print/PageRange;
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 838
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v1}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    invoke-direct {p0, v1}, Lcom/android/printspooler/ui/PrintActivity;->getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->updateSelectedPages([Landroid/print/PageRange;I)V

    .line 841
    :cond_0
    return-void
.end method

.method private updateSummary()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const v7, 0x7f09001c

    .line 1465
    const/4 v0, 0x0

    .line 1466
    .local v0, "copies":I
    const/4 v2, 0x0

    .line 1468
    .local v2, "mediaSizeText":Ljava/lang/CharSequence;
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1469
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mSummaryCopies:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1470
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1473
    :cond_0
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    .line 1474
    .local v3, "selectedMediaIndex":I
    if-ltz v3, :cond_1

    .line 1475
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    .line 1476
    .local v1, "mediaItem":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Landroid/print/PrintAttributes$MediaSize;>;"
    iget-object v2, v1, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->label:Ljava/lang/CharSequence;

    .line 1477
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mSummaryPaperSize:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1480
    .end local v1    # "mediaItem":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Landroid/print/PrintAttributes$MediaSize;>;"
    :cond_1
    if-eqz v0, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1481
    invoke-virtual {p0, v7}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1482
    .local v4, "summaryText":Ljava/lang/String;
    const-string v5, "%1$d"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1483
    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    aput-object v2, v5, v9

    invoke-virtual {p0, v7, v5}, Lcom/android/printspooler/ui/PrintActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1487
    :goto_0
    iget-object v5, p0, Lcom/android/printspooler/ui/PrintActivity;->mSummaryContainer:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1489
    .end local v4    # "summaryText":Ljava/lang/String;
    :cond_2
    return-void

    .line 1485
    .restart local v4    # "summaryText":Ljava/lang/String;
    :cond_3
    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    aput-object v2, v5, v9

    invoke-virtual {p0, v7, v5}, Lcom/android/printspooler/ui/PrintActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public canCloseOptions()Z
    .locals 1

    .prologue
    .line 580
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canOpenOptions()Z
    .locals 1

    .prologue
    .line 575
    const/4 v0, 0x1

    return v0
.end method

.method public onActionPerformed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 454
    iget v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->ensurePreviewUiShown()V

    .line 457
    invoke-direct {p0, v2}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 458
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 460
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 596
    packed-switch p1, :pswitch_data_0

    .line 609
    :goto_0
    return-void

    .line 598
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/android/printspooler/ui/PrintActivity;->onStartCreateDocumentActivityResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 602
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/android/printspooler/ui/PrintActivity;->onSelectPrinterActivityResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 606
    :pswitch_2
    invoke-direct {p0, p2, p3}, Lcom/android/printspooler/ui/PrintActivity;->onAdvancedPrintOptionsActivityResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 596
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 585
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 586
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->onOrientationChanged()V

    .line 588
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->locale:Ljava/util/Locale;

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 589
    const-string v0, "PrintActivity"

    const-string v1, "Finishing Activity on Language change"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->finish()V

    .line 592
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 231
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 233
    iput-object p0, p0, Lcom/android/printspooler/ui/PrintActivity;->mContext:Landroid/content/Context;

    .line 234
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 235
    .local v1, "extras":Landroid/os/Bundle;
    invoke-static {p0}, Lcom/android/printspooler/util/SPrintSpoolerCommon;->readSelectedPrinter(Landroid/content/Context;)Landroid/print/PrinterInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mSConnectPrinter:Landroid/print/PrinterInfo;

    .line 236
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mSConnectPrinter:Landroid/print/PrinterInfo;

    if-eqz v2, :cond_0

    .line 237
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/printspooler/ui/PrintActivity;->bSConnectPrintMode:Z

    .line 239
    :cond_0
    const-string v2, "android.print.intent.extra.EXTRA_PRINT_JOB"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/print/PrintJobInfo;

    iput-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    .line 240
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    if-nez v2, :cond_1

    .line 241
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "android.print.intent.extra.EXTRA_PRINT_JOB cannot be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 244
    :cond_1
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    new-instance v3, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v3}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v3}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/print/PrintJobInfo;->setAttributes(Landroid/print/PrintAttributes;)V

    .line 246
    const-string v2, "android.print.intent.extra.EXTRA_PRINT_DOCUMENT_ADAPTER"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBinder(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 247
    .local v0, "adapter":Landroid/os/IBinder;
    if-nez v0, :cond_2

    .line 248
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "android.print.intent.extra.EXTRA_PRINT_DOCUMENT_ADAPTER cannot be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 252
    :cond_2
    const-string v2, "android.content.extra.PACKAGE_NAME"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mCallingPackageName:Ljava/lang/String;

    .line 256
    new-instance v2, Lcom/android/printspooler/model/PrintSpoolerProvider;

    new-instance v3, Lcom/android/printspooler/ui/PrintActivity$1;

    invoke-direct {v3, p0, v0}, Lcom/android/printspooler/ui/PrintActivity$1;-><init>(Lcom/android/printspooler/ui/PrintActivity;Landroid/os/IBinder;)V

    invoke-direct {v2, p0, v3}, Lcom/android/printspooler/model/PrintSpoolerProvider;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mSpoolerProvider:Lcom/android/printspooler/model/PrintSpoolerProvider;

    .line 263
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->checkPluginsState()V

    .line 264
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->locale:Ljava/util/Locale;

    .line 265
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 392
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 393
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    .line 394
    const/4 v0, 0x1

    .line 396
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 401
    iget v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    if-nez v1, :cond_1

    .line 402
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->doFinish()V

    .line 421
    :cond_0
    :goto_0
    return v0

    .line 406
    :cond_1
    iget v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 411
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 413
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintPreviewController;->isOptionsOpened()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z

    move-result v1

    if-nez v1, :cond_2

    .line 415
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintPreviewController;->closeOptions()V

    goto :goto_0

    .line 417
    :cond_2
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->cancelPrint()V

    goto :goto_0

    .line 421
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMalformedPdfFile()V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->cancel()V

    .line 434
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->ensureErrorUiShown(Ljava/lang/CharSequence;I)V

    .line 436
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 438
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 439
    return-void
.end method

.method public onOptionsClosed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 542
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->computeSelectedPages()[Landroid/print/PageRange;

    move-result-object v1

    .line 543
    .local v1, "selectedPages":[Landroid/print/PageRange;
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 544
    iput-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mSelectedPages:[Landroid/print/PageRange;

    .line 547
    invoke-direct {p0, v3}, Lcom/android/printspooler/ui/PrintActivity;->updatePrintPreviewController(Z)V

    .line 552
    :cond_0
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/android/printspooler/ui/PrintActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 554
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 555
    return-void
.end method

.method public onOptionsOpened()V
    .locals 0

    .prologue
    .line 537
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateSelectedPagesFromPreview()V

    .line 538
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v2, 0x7

    const/4 v4, 0x0

    .line 351
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mSpoolerProvider:Lcom/android/printspooler/model/PrintSpoolerProvider;

    invoke-virtual {v1}, Lcom/android/printspooler/model/PrintSpoolerProvider;->getSpooler()Lcom/android/printspooler/model/PrintSpoolerService;

    move-result-object v0

    .line 353
    .local v0, "spooler":Lcom/android/printspooler/model/PrintSpoolerService;
    iget v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    if-nez v1, :cond_1

    .line 354
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 355
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z

    .line 357
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 388
    :goto_0
    return-void

    .line 361
    :cond_1
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 362
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v0, v1}, Lcom/android/printspooler/model/PrintSpoolerService;->updatePrintJobUserConfigurableOptionsNoPersistence(Landroid/print/PrintJobInfo;)V

    .line 364
    iget v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    sparse-switch v1, :sswitch_data_0

    .line 379
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z

    .line 384
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterAvailabilityDetector:Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->cancel()V

    .line 385
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    invoke-virtual {v1, v4}, Lcom/android/printspooler/ui/PrinterRegistry;->setTrackedPrinter(Landroid/print/PrinterId;)V

    .line 387
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    goto :goto_0

    .line 366
    :sswitch_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z

    goto :goto_1

    .line 370
    :sswitch_1
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z

    goto :goto_1

    .line 374
    :sswitch_2
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v1}, Landroid/print/PrintJobInfo;->getId()Landroid/print/PrintJobId;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f09003b

    invoke-virtual {p0, v3}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/printspooler/model/PrintSpoolerService;->setPrintJobState(Landroid/print/PrintJobId;ILjava/lang/String;)Z

    goto :goto_1

    .line 364
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_2
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrinterAvailable(Landroid/print/PrinterInfo;)V
    .locals 1
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v0, p1}, Landroid/print/PrinterInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1585
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 1586
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1587
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z

    .line 1589
    :cond_0
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->ensurePreviewUiShown()V

    .line 1590
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 1592
    :cond_1
    return-void
.end method

.method public onPrinterUnavailable(Landroid/print/PrinterInfo;)V
    .locals 2
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 1595
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v0

    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1596
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 1597
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->isUpdating()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1598
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->cancel()V

    .line 1600
    :cond_0
    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->ensureErrorUiShown(Ljava/lang/CharSequence;I)V

    .line 1602
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 1604
    :cond_1
    return-void
.end method

.method public onRendererBusy()V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->cancel()V

    .line 444
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mContext:Landroid/content/Context;

    const v1, 0x7f090040

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->ensureErrorUiShown(Ljava/lang/CharSequence;I)V

    .line 446
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 448
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 450
    return-void
.end method

.method public onRequestContentUpdate()V
    .locals 1

    .prologue
    .line 426
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z

    .line 429
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 343
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 344
    iget v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrinterRegistry;->setTrackedPrinter(Landroid/print/PrinterId;)V

    .line 347
    :cond_0
    return-void
.end method

.method public onUpdateCanceled()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->cancel()V

    .line 468
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->ensurePreviewUiShown()V

    .line 470
    iget v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 479
    :goto_0
    return-void

    .line 472
    :pswitch_0
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->requestCreatePdfFileOrFinish()V

    goto :goto_0

    .line 476
    :pswitch_1
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->doFinish()V

    goto :goto_0

    .line 470
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUpdateCompleted(Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;)V
    .locals 5
    .param p1, "document"    # Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    .prologue
    .line 487
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity;->mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    invoke-virtual {v3}, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->cancel()V

    .line 488
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->ensurePreviewUiShown()V

    .line 495
    iget-object v1, p1, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    .line 496
    .local v1, "info":Landroid/print/PrintDocumentInfo;
    if-eqz v1, :cond_0

    .line 497
    iget-object v3, p1, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->writtenPages:[Landroid/print/PageRange;

    invoke-direct {p0, v1}, Lcom/android/printspooler/ui/PrintActivity;->getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/printspooler/util/PageRangeUtils;->getNormalizedPageCount([Landroid/print/PageRange;I)I

    move-result v2

    .line 499
    .local v2, "pageCount":I
    new-instance v3, Landroid/print/PrintDocumentInfo$Builder;

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo;->getContentType()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v0

    .line 503
    .local v0, "adjustedInfo":Landroid/print/PrintDocumentInfo;
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v3, v0}, Landroid/print/PrintJobInfo;->setDocumentInfo(Landroid/print/PrintDocumentInfo;)V

    .line 504
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    iget-object v4, p1, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->printedPages:[Landroid/print/PageRange;

    invoke-virtual {v3, v4}, Landroid/print/PrintJobInfo;->setPages([Landroid/print/PageRange;)V

    .line 507
    .end local v0    # "adjustedInfo":Landroid/print/PrintDocumentInfo;
    .end local v2    # "pageCount":I
    :cond_0
    iget v3, p0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    packed-switch v3, :pswitch_data_0

    .line 513
    iget-boolean v3, p1, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->changed:Z

    invoke-direct {p0, v3}, Lcom/android/printspooler/ui/PrintActivity;->updatePrintPreviewController(Z)V

    .line 515
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 516
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 519
    :goto_0
    return-void

    .line 509
    :pswitch_0
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity;->requestCreatePdfFileOrFinish()V

    goto :goto_0

    .line 507
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onUpdateFailed(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/CharSequence;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity;->mProgressMessageController:Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity$ProgressMessageController;->cancel()V

    .line 528
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/printspooler/ui/PrintActivity;->ensureErrorUiShown(Ljava/lang/CharSequence;I)V

    .line 530
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/PrintActivity;->setState(I)V

    .line 532
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 533
    return-void
.end method

.method updateOptionsUi()V
    .locals 36

    .prologue
    .line 1190
    invoke-direct/range {p0 .. p0}, Lcom/android/printspooler/ui/PrintActivity;->updateSummary()V

    .line 1192
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    move/from16 v32, v0

    const/16 v33, 0x2

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    move/from16 v32, v0

    const/16 v33, 0x8

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    move/from16 v32, v0

    const/16 v33, 0x3

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    move/from16 v32, v0

    const/16 v33, 0x4

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    move/from16 v32, v0

    const/16 v33, 0x5

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    move/from16 v32, v0

    const/16 v33, 0x6

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    move/from16 v32, v0

    const/16 v33, 0x7

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_3

    .line 1199
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mState:I

    move/from16 v32, v0

    const/16 v33, 0x6

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_1

    .line 1200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1202
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const/16 v33, 0x8

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1462
    :cond_2
    :goto_0
    return-void

    .line 1216
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    move-object/from16 v32, v0

    if-eqz v32, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    move-object/from16 v32, v0

    invoke-static/range {v32 .. v32}, Lcom/android/printspooler/ui/PrintActivity;->canPrint(Landroid/print/PrinterInfo;)Z

    move-result v32

    if-nez v32, :cond_5

    .line 1217
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const/16 v33, 0x8

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 1229
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v6

    .line 1230
    .local v6, "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    invoke-virtual {v6}, Landroid/print/PrinterCapabilitiesInfo;->getDefaults()Landroid/print/PrintAttributes;

    move-result-object v12

    .line 1233
    .local v12, "defaultAttributes":Landroid/print/PrintAttributes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/printspooler/ui/PrintActivity;->bSConnectPrintMode:Z

    move/from16 v32, v0

    if-nez v32, :cond_8

    const/16 v32, 0x1

    :goto_1
    move-object/from16 v0, v33

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1238
    new-instance v21, Ljava/util/ArrayList;

    invoke-virtual {v6}, Landroid/print/PrinterCapabilitiesInfo;->getMediaSizes()Ljava/util/List;

    move-result-object v32

    move-object/from16 v0, v21

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1240
    .local v21, "mediaSizes":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintAttributes$MediaSize;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeComparator:Lcom/android/printspooler/util/MediaSizeUtils$MediaSizeComparator;

    move-object/from16 v32, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v5

    .line 1245
    .local v5, "attributes":Landroid/print/PrintAttributes;
    const/16 v22, 0x0

    .line 1246
    .local v22, "mediaSizesChanged":Z
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v19

    .line 1247
    .local v19, "mediaSizeCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v32

    move/from16 v0, v19

    move/from16 v1, v32

    if-eq v0, v1, :cond_9

    .line 1248
    const/16 v22, 0x1

    .line 1257
    :cond_6
    :goto_2
    if-eqz v22, :cond_c

    .line 1259
    const/16 v27, -0x1

    .line 1260
    .local v27, "oldMediaSizeNewIndex":I
    invoke-virtual {v5}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v26

    .line 1263
    .local v26, "oldMediaSize":Landroid/print/PrintAttributes$MediaSize;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ArrayAdapter;->clear()V

    .line 1264
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    move/from16 v0, v19

    if-ge v13, v0, :cond_b

    .line 1265
    move-object/from16 v0, v21

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/print/PrintAttributes$MediaSize;

    .line 1266
    .local v18, "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    if-eqz v26, :cond_7

    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintAttributes$MediaSize;->asPortrait()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v32

    invoke-virtual/range {v26 .. v26}, Landroid/print/PrintAttributes$MediaSize;->asPortrait()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Landroid/print/PrintAttributes$MediaSize;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_7

    .line 1269
    move/from16 v27, v13

    .line 1271
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    new-instance v33, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/ui/PrintActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v34

    move-object/from16 v0, v18

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/print/PrintAttributes$MediaSize;->getLabel(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    move-object/from16 v3, v34

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;-><init>(Lcom/android/printspooler/ui/PrintActivity;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1264
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 1233
    .end local v5    # "attributes":Landroid/print/PrintAttributes;
    .end local v13    # "i":I
    .end local v18    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v19    # "mediaSizeCount":I
    .end local v21    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintAttributes$MediaSize;>;"
    .end local v22    # "mediaSizesChanged":Z
    .end local v26    # "oldMediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v27    # "oldMediaSizeNewIndex":I
    :cond_8
    const/16 v32, 0x0

    goto/16 :goto_1

    .line 1250
    .restart local v5    # "attributes":Landroid/print/PrintAttributes;
    .restart local v19    # "mediaSizeCount":I
    .restart local v21    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrintAttributes$MediaSize;>;"
    .restart local v22    # "mediaSizesChanged":Z
    :cond_9
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_4
    move/from16 v0, v19

    if-ge v13, v0, :cond_6

    .line 1251
    move-object/from16 v0, v21

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Landroid/print/PrintAttributes$MediaSize;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Landroid/print/PrintAttributes$MediaSize;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-nez v32, :cond_a

    .line 1252
    const/16 v22, 0x1

    .line 1253
    goto/16 :goto_2

    .line 1250
    :cond_a
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 1275
    .restart local v26    # "oldMediaSize":Landroid/print/PrintAttributes$MediaSize;
    .restart local v27    # "oldMediaSizeNewIndex":I
    :cond_b
    const/16 v32, -0x1

    move/from16 v0, v27

    move/from16 v1, v32

    if-eq v0, v1, :cond_f

    .line 1277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    move/from16 v0, v32

    move/from16 v1, v27

    if-eq v0, v1, :cond_c

    .line 1278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1301
    .end local v13    # "i":I
    .end local v26    # "oldMediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v27    # "oldMediaSizeNewIndex":I
    :cond_c
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1302
    invoke-virtual {v6}, Landroid/print/PrinterCapabilitiesInfo;->getColorModes()I

    move-result v10

    .line 1305
    .local v10, "colorModes":I
    const/4 v11, 0x0

    .line 1306
    .local v11, "colorModesChanged":Z
    invoke-static {v10}, Ljava/lang/Integer;->bitCount(I)I

    move-result v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v33

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_12

    .line 1307
    const/4 v11, 0x1

    .line 1322
    :cond_d
    :goto_6
    if-eqz v11, :cond_15

    .line 1324
    const/16 v25, -0x1

    .line 1325
    .local v25, "oldColorModeNewIndex":I
    invoke-virtual {v5}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v24

    .line 1328
    .local v24, "oldColorMode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ArrayAdapter;->clear()V

    .line 1329
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v32

    const v33, 0x7f040003

    invoke-virtual/range {v32 .. v33}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    .line 1330
    .local v9, "colorModeLabels":[Ljava/lang/String;
    move/from16 v29, v10

    .line 1331
    .local v29, "remainingColorModes":I
    :goto_7
    if-eqz v29, :cond_14

    .line 1332
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v7

    .line 1333
    .local v7, "colorBitOffset":I
    const/16 v32, 0x1

    shl-int v8, v32, v7

    .line 1334
    .local v8, "colorMode":I
    move/from16 v0, v24

    if-ne v8, v0, :cond_e

    .line 1336
    move/from16 v25, v7

    .line 1338
    :cond_e
    xor-int/lit8 v32, v8, -0x1

    and-int v29, v29, v32

    .line 1339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    new-instance v33, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    aget-object v35, v9, v7

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    invoke-direct {v0, v1, v2, v3}, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;-><init>(Lcom/android/printspooler/ui/PrintActivity;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_7

    .line 1282
    .end local v7    # "colorBitOffset":I
    .end local v8    # "colorMode":I
    .end local v9    # "colorModeLabels":[Ljava/lang/String;
    .end local v10    # "colorModes":I
    .end local v11    # "colorModesChanged":Z
    .end local v24    # "oldColorMode":I
    .end local v25    # "oldColorModeNewIndex":I
    .end local v29    # "remainingColorModes":I
    .restart local v13    # "i":I
    .restart local v26    # "oldMediaSize":Landroid/print/PrintAttributes$MediaSize;
    .restart local v27    # "oldMediaSizeNewIndex":I
    :cond_f
    invoke-virtual {v12}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v32

    move-object/from16 v0, v21

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v32

    const/16 v33, 0x0

    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 1284
    .local v20, "mediaSizeIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    move/from16 v0, v32

    move/from16 v1, v20

    if-eq v0, v1, :cond_10

    .line 1285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1288
    :cond_10
    if-eqz v26, :cond_c

    .line 1289
    invoke-virtual/range {v26 .. v26}, Landroid/print/PrintAttributes$MediaSize;->isPortrait()Z

    move-result v32

    if-eqz v32, :cond_11

    .line 1290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    move-object/from16 v32, v0

    check-cast v32, Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual/range {v32 .. v32}, Landroid/print/PrintAttributes$MediaSize;->asPortrait()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    goto/16 :goto_5

    .line 1293
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    move-object/from16 v32, v0

    check-cast v32, Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual/range {v32 .. v32}, Landroid/print/PrintAttributes$MediaSize;->asLandscape()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    goto/16 :goto_5

    .line 1309
    .end local v13    # "i":I
    .end local v20    # "mediaSizeIndex":I
    .end local v26    # "oldMediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v27    # "oldMediaSizeNewIndex":I
    .restart local v10    # "colorModes":I
    .restart local v11    # "colorModesChanged":Z
    :cond_12
    move/from16 v29, v10

    .line 1310
    .restart local v29    # "remainingColorModes":I
    const/4 v4, 0x0

    .line 1311
    .local v4, "adapterIndex":I
    :goto_8
    if-eqz v29, :cond_d

    .line 1312
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v7

    .line 1313
    .restart local v7    # "colorBitOffset":I
    const/16 v32, 0x1

    shl-int v8, v32, v7

    .line 1314
    .restart local v8    # "colorMode":I
    xor-int/lit8 v32, v8, -0x1

    and-int v29, v29, v32

    .line 1315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    move-object/from16 v32, v0

    check-cast v32, Ljava/lang/Integer;

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v32

    move/from16 v0, v32

    if-eq v8, v0, :cond_13

    .line 1316
    const/4 v11, 0x1

    .line 1317
    goto/16 :goto_6

    .line 1319
    :cond_13
    add-int/lit8 v4, v4, 0x1

    .line 1320
    goto :goto_8

    .line 1342
    .end local v4    # "adapterIndex":I
    .end local v7    # "colorBitOffset":I
    .end local v8    # "colorMode":I
    .restart local v9    # "colorModeLabels":[Ljava/lang/String;
    .restart local v24    # "oldColorMode":I
    .restart local v25    # "oldColorModeNewIndex":I
    :cond_14
    const/16 v32, -0x1

    move/from16 v0, v25

    move/from16 v1, v32

    if-eq v0, v1, :cond_1b

    .line 1344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    move/from16 v0, v32

    move/from16 v1, v25

    if-eq v0, v1, :cond_15

    .line 1345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1364
    .end local v9    # "colorModeLabels":[Ljava/lang/String;
    .end local v24    # "oldColorMode":I
    .end local v25    # "oldColorModeNewIndex":I
    .end local v29    # "remainingColorModes":I
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1365
    invoke-virtual {v5}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v18

    .line 1366
    .restart local v18    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    if-eqz v18, :cond_16

    .line 1367
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintAttributes$MediaSize;->isPortrait()Z

    move-result v32

    if-eqz v32, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    if-eqz v32, :cond_1e

    .line 1369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1377
    :cond_16
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v32

    move-object/from16 v0, v32

    iget-object v15, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    .line 1378
    .local v15, "info":Landroid/print/PrintDocumentInfo;
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/printspooler/ui/PrintActivity;->getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I

    move-result v28

    .line 1379
    .local v28, "pageCount":I
    if-eqz v15, :cond_21

    if-lez v28, :cond_21

    .line 1380
    const/16 v32, 0x1

    move/from16 v0, v28

    move/from16 v1, v32

    if-ne v0, v1, :cond_1f

    .line 1381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1411
    :cond_17
    :goto_a
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/printspooler/ui/PrintActivity;->getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I

    move-result v23

    .line 1412
    .local v23, "newPageCount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPageCount:I

    move/from16 v32, v0

    move/from16 v0, v23

    move/from16 v1, v32

    if-eq v0, v1, :cond_18

    .line 1413
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPageCount:I

    .line 1414
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->updatePageRangeOptions(I)V

    .line 1418
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v31

    .line 1419
    .local v31, "serviceName":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/android/printspooler/util/PrintOptionUtils;->getAdvancedOptionsActivityName(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_23

    .line 1421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Button;->setVisibility(I)V

    .line 1422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1429
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPdfPrinter()Landroid/print/PrinterInfo;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_24

    .line 1430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const v33, 0x7f020015

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1431
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const v33, 0x7f09001f

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1436
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v32

    move-object/from16 v0, v32

    iget-boolean v0, v0, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->laidout:Z

    move/from16 v32, v0

    if-eqz v32, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_1a

    invoke-direct/range {p0 .. p0}, Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z

    move-result v32

    if-nez v32, :cond_1a

    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    if-nez v32, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v32

    if-eqz v32, :cond_1a

    invoke-direct/range {p0 .. p0}, Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z

    move-result v32

    if-eqz v32, :cond_25

    .line 1441
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const/16 v33, 0x8

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1447
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPdfPrinter()Landroid/print/PrinterInfo;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_26

    .line 1448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 1457
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v32

    if-nez v32, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-eqz v32, :cond_2

    .line 1459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    .line 1349
    .end local v15    # "info":Landroid/print/PrintDocumentInfo;
    .end local v18    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    .end local v23    # "newPageCount":I
    .end local v28    # "pageCount":I
    .end local v31    # "serviceName":Landroid/content/ComponentName;
    .restart local v9    # "colorModeLabels":[Ljava/lang/String;
    .restart local v24    # "oldColorMode":I
    .restart local v25    # "oldColorModeNewIndex":I
    .restart local v29    # "remainingColorModes":I
    :cond_1b
    invoke-virtual {v12}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v32

    and-int v30, v10, v32

    .line 1350
    .local v30, "selectedColorMode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v17

    .line 1351
    .local v17, "itemCount":I
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_f
    move/from16 v0, v17

    if-ge v13, v0, :cond_15

    .line 1352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v13}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    .line 1353
    .local v16, "item":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;"
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    move-object/from16 v32, v0

    check-cast v32, Ljava/lang/Integer;

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v32

    move/from16 v0, v30

    move/from16 v1, v32

    if-ne v0, v1, :cond_1d

    .line 1354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    move/from16 v0, v32

    if-eq v0, v13, :cond_1c

    .line 1355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v13}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1357
    :cond_1c
    move/from16 v0, v30

    invoke-virtual {v5, v0}, Landroid/print/PrintAttributes;->setColorMode(I)V

    .line 1351
    :cond_1d
    add-int/lit8 v13, v13, 0x1

    goto :goto_f

    .line 1370
    .end local v9    # "colorModeLabels":[Ljava/lang/String;
    .end local v13    # "i":I
    .end local v16    # "item":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;"
    .end local v17    # "itemCount":I
    .end local v24    # "oldColorMode":I
    .end local v25    # "oldColorModeNewIndex":I
    .end local v29    # "remainingColorModes":I
    .end local v30    # "selectedColorMode":I
    .restart local v18    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    :cond_1e
    invoke-virtual/range {v18 .. v18}, Landroid/print/PrintAttributes$MediaSize;->isPortrait()Z

    move-result v32

    if-nez v32, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_16

    .line 1372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_9

    .line 1383
    .restart local v15    # "info":Landroid/print/PrintDocumentInfo;
    .restart local v28    # "pageCount":I
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    if-lez v32, :cond_20

    .line 1385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/EditText;->isEnabled()Z

    move-result v32

    if-nez v32, :cond_17

    .line 1386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeTitle:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/EditText;->requestFocus()Z

    .line 1390
    const-string v32, "input_method"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/inputmethod/InputMethodManager;

    .line 1392
    .local v14, "imm":Landroid/view/inputmethod/InputMethodManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-virtual {v14, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto/16 :goto_a

    .line 1395
    .end local v14    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x4

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeTitle:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x4

    invoke-virtual/range {v32 .. v33}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    .line 1401
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v32

    if-eqz v32, :cond_22

    .line 1402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const-string v33, ""

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1405
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x4

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPageRangeTitle:Landroid/widget/TextView;

    move-object/from16 v32, v0

    const/16 v33, 0x4

    invoke-virtual/range {v32 .. v33}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    .line 1424
    .restart local v23    # "newPageCount":I
    .restart local v31    # "serviceName":Landroid/content/ComponentName;
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    move-object/from16 v32, v0

    const/16 v33, 0x8

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Button;->setVisibility(I)V

    .line 1425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_b

    .line 1433
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const v33, 0x7f020016

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const v33, 0x7f090020

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_c

    .line 1443
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_d

    .line 1451
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 1452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mCopiesEditText:Landroid/widget/EditText;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 1455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Landroid/widget/Spinner;->setEnabled(Z)V

    goto/16 :goto_e
.end method
