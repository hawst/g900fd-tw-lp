.class public interface abstract Lcom/android/printspooler/ui/PageAdapter$PreviewArea;
.super Ljava/lang/Object;
.source "PageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PreviewArea"
.end annotation


# virtual methods
.method public abstract getHeight()I
.end method

.method public abstract getWidth()I
.end method

.method public abstract setColumnCount(I)V
.end method

.method public abstract setPadding(IIII)V
.end method
