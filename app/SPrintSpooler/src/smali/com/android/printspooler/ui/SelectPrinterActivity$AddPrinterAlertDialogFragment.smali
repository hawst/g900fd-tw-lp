.class public Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "SelectPrinterActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddPrinterAlertDialogFragment"
.end annotation


# instance fields
.field private mAddPrintServiceItem:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 369
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;

    .prologue
    .line 369
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->mAddPrintServiceItem:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 376
    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v10, 0x7f09002b

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 379
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "FRAGMENT_ARGUMENT_PRINT_SERVICE_INFOS"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 382
    .local v6, "printServices":Ljava/util/List;, "Ljava/util/List<Landroid/printservice/PrintServiceInfo;>;"
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v10, 0x7f030009

    invoke-direct {v0, v9, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 384
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    .line 385
    .local v5, "printServiceCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_0

    .line 386
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/printservice/PrintServiceInfo;

    .line 387
    .local v4, "printService":Landroid/printservice/PrintServiceInfo;
    invoke-virtual {v4}, Landroid/printservice/PrintServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v9

    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 385
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 391
    .end local v4    # "printService":Landroid/printservice/PrintServiceInfo;
    :cond_0
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "print_service_search_uri"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 394
    .local v7, "searchUri":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 395
    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v3, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 396
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v3, v10}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 397
    move-object v8, v3

    .line 398
    .local v8, "viewIntent":Landroid/content/Intent;
    const v9, 0x7f090025

    invoke-virtual {p0, v9}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->mAddPrintServiceItem:Ljava/lang/String;

    .line 399
    iget-object v9, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->mAddPrintServiceItem:Ljava/lang/String;

    invoke-virtual {v0, v9}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 407
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_1
    new-instance v9, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;

    invoke-direct {v9, p0, v0, v8, v6}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;Landroid/widget/ArrayAdapter;Landroid/content/Intent;Ljava/util/List;)V

    invoke-virtual {v1, v0, v9}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 433
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    return-object v9

    .line 401
    .end local v8    # "viewIntent":Landroid/content/Intent;
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v8, 0x0

    .restart local v8    # "viewIntent":Landroid/content/Intent;
    goto :goto_1

    .line 404
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v8    # "viewIntent":Landroid/content/Intent;
    :cond_2
    const/4 v8, 0x0

    .restart local v8    # "viewIntent":Landroid/content/Intent;
    goto :goto_1
.end method
