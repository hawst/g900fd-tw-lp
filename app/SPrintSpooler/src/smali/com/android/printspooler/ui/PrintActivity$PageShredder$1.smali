.class Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;
.super Landroid/os/AsyncTask;
.source "PrintActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/PrintActivity$PageShredder;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity$PageShredder;

.field final synthetic val$editor:Lcom/android/printspooler/renderer/IPdfEditor;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/PrintActivity$PageShredder;Lcom/android/printspooler/renderer/IPdfEditor;)V
    .locals 0

    .prologue
    .line 2466
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->this$0:Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    iput-object p2, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->val$editor:Lcom/android/printspooler/renderer/IPdfEditor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2466
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 2473
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->this$0:Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->val$editor:Lcom/android/printspooler/renderer/IPdfEditor;

    # invokes: Lcom/android/printspooler/ui/PrintActivity$PageShredder;->shredPages(Lcom/android/printspooler/renderer/IPdfEditor;)V
    invoke-static {v0, v1}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->access$5200(Lcom/android/printspooler/ui/PrintActivity$PageShredder;Lcom/android/printspooler/renderer/IPdfEditor;)V

    .line 2474
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->this$0:Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    # invokes: Lcom/android/printspooler/ui/PrintActivity$PageShredder;->updatePrintJob()V
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->access$5300(Lcom/android/printspooler/ui/PrintActivity$PageShredder;)V

    .line 2475
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2466
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 2481
    :try_start_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->this$0:Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    # getter for: Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->access$5400(Lcom/android/printspooler/ui/PrintActivity$PageShredder;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->this$0:Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2485
    :goto_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->this$0:Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    # getter for: Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mCallback:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->access$5500(Lcom/android/printspooler/ui/PrintActivity$PageShredder;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 2486
    return-void

    .line 2482
    :catch_0
    move-exception v0

    .line 2483
    .local v0, "ie":Ljava/lang/IllegalArgumentException;
    const-string v1, "PrintActivity"

    const-string v2, "The PageShredder service is already disconnected"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
