.class final Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;
.super Landroid/os/AsyncTask;
.source "FusedPrintersProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WriteTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/print/PrinterInfo;",
        ">;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)V
    .locals 0

    .prologue
    .line 688
    iput-object p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Lcom/android/printspooler/ui/FusedPrintersProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
    .param p2, "x1"    # Lcom/android/printspooler/ui/FusedPrintersProvider$1;

    .prologue
    .line 688
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;-><init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)V

    return-void
.end method

.method private doWritePrinterHistory(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 696
    .local p1, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v3, 0x0

    .line 698
    .local v3, "out":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v8, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;
    invoke-static {v8}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$1200(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Landroid/util/AtomicFile;

    move-result-object v8

    invoke-virtual {v8}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v3

    .line 700
    new-instance v7, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v7}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 701
    .local v7, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    const-string v8, "utf-8"

    invoke-interface {v7, v3, v8}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 702
    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 703
    const/4 v8, 0x0

    const-string v9, "printers"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 705
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 706
    .local v5, "printerCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_1

    .line 707
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/print/PrinterInfo;

    .line 709
    .local v4, "printer":Landroid/print/PrinterInfo;
    const/4 v8, 0x0

    const-string v9, "printer"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 711
    const/4 v8, 0x0

    const-string v9, "name"

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v8, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 713
    const/4 v8, 0x0

    const-string v9, "status"

    const/4 v10, 0x3

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v8, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 715
    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 716
    .local v0, "description":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 717
    const/4 v8, 0x0

    const-string v9, "description"

    invoke-interface {v7, v8, v9, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 720
    :cond_0
    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v6

    .line 721
    .local v6, "printerId":Landroid/print/PrinterId;
    const/4 v8, 0x0

    const-string v9, "printerId"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 722
    const/4 v8, 0x0

    const-string v9, "localId"

    invoke-virtual {v6}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v8, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 723
    const/4 v8, 0x0

    const-string v9, "serviceName"

    invoke-virtual {v6}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v8, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 725
    const/4 v8, 0x0

    const-string v9, "printerId"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 727
    const/4 v8, 0x0

    const-string v9, "printer"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 730
    const-string v8, "FusedPrintersProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[PERSISTED] "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 734
    .end local v0    # "description":Ljava/lang/String;
    .end local v4    # "printer":Landroid/print/PrinterInfo;
    .end local v6    # "printerId":Landroid/print/PrinterId;
    :cond_1
    const/4 v8, 0x0

    const-string v9, "printers"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 735
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 736
    iget-object v8, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;
    invoke-static {v8}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$1200(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Landroid/util/AtomicFile;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    .line 739
    const-string v8, "FusedPrintersProvider"

    const-string v9, "[PERSIST END]"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 745
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 747
    .end local v1    # "i":I
    .end local v5    # "printerCount":I
    .end local v7    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :goto_1
    return-void

    .line 741
    :catch_0
    move-exception v2

    .line 742
    .local v2, "ioe":Ljava/io/IOException;
    :try_start_1
    const-string v8, "FusedPrintersProvider"

    const-string v9, "Failed to write printer history, restoring backup."

    invoke-static {v8, v9, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 743
    iget-object v8, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->mStatePersistFile:Landroid/util/AtomicFile;
    invoke-static {v8}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;->access$1200(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;)Landroid/util/AtomicFile;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 745
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_1

    .end local v2    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v8
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 688
    check-cast p1, [Ljava/util/List;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;->doInBackground([Ljava/util/List;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/util/List;)Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 691
    .local p1, "printers":[Ljava/util/List;, "[Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$WriteTask;->doWritePrinterHistory(Ljava/util/List;)V

    .line 692
    const/4 v0, 0x0

    return-object v0
.end method
