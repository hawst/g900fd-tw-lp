.class final Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;
.super Landroid/database/DataSetObserver;
.source "PrintActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PrintersObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0

    .prologue
    .line 2093
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p2, "x1"    # Lcom/android/printspooler/ui/PrintActivity$1;

    .prologue
    .line 2093
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    return-void
.end method

.method private capabilitiesChanged(Landroid/print/PrinterCapabilitiesInfo;Landroid/print/PrinterCapabilitiesInfo;)Z
    .locals 2
    .param p1, "oldCapabilities"    # Landroid/print/PrinterCapabilitiesInfo;
    .param p2, "newCapabilities"    # Landroid/print/PrinterCapabilitiesInfo;

    .prologue
    const/4 v0, 0x1

    .line 2161
    if-nez p1, :cond_1

    .line 2162
    if-eqz p2, :cond_2

    .line 2168
    :cond_0
    :goto_0
    return v0

    .line 2165
    :cond_1
    invoke-virtual {p1, p2}, Landroid/print/PrinterCapabilitiesInfo;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2168
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onChanged()V
    .locals 18

    .prologue
    .line 2096
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;
    invoke-static/range {v16 .. v16}, Lcom/android/printspooler/ui/PrintActivity;->access$1700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;

    move-result-object v12

    .line 2097
    .local v12, "oldPrinterState":Landroid/print/PrinterInfo;
    if-nez v12, :cond_1

    .line 2157
    :cond_0
    :goto_0
    return-void

    .line 2101
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;
    invoke-static/range {v16 .. v16}, Lcom/android/printspooler/ui/PrintActivity;->access$2200(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    move-result-object v16

    invoke-virtual {v12}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPrinterHolder(Landroid/print/PrinterId;)Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    move-result-object v14

    .line 2103
    .local v14, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    if-eqz v14, :cond_0

    .line 2106
    iget-object v9, v14, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    .line 2108
    .local v9, "newPrinterState":Landroid/print/PrinterInfo;
    iget-boolean v0, v14, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->removed:Z

    move/from16 v16, v0

    if-nez v16, :cond_a

    .line 2109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;
    invoke-static/range {v16 .. v16}, Lcom/android/printspooler/ui/PrintActivity;->access$2200(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->pruneRemovedPrinters()V

    .line 2114
    :goto_1
    invoke-virtual {v12, v9}, Landroid/print/PrinterInfo;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_0

    .line 2118
    invoke-virtual {v12}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v11

    .line 2119
    .local v11, "oldCapab":Landroid/print/PrinterCapabilitiesInfo;
    invoke-virtual {v9}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v8

    .line 2121
    .local v8, "newCapab":Landroid/print/PrinterCapabilitiesInfo;
    if-eqz v8, :cond_b

    const/4 v5, 0x1

    .line 2122
    .local v5, "hasCapab":Z
    :goto_2
    if-nez v11, :cond_c

    if-eqz v8, :cond_c

    const/4 v4, 0x1

    .line 2123
    .local v4, "gotCapab":Z
    :goto_3
    if-eqz v11, :cond_d

    if-nez v8, :cond_d

    const/4 v7, 0x1

    .line 2124
    .local v7, "lostCapab":Z
    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v8}, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->capabilitiesChanged(Landroid/print/PrinterCapabilitiesInfo;Landroid/print/PrinterCapabilitiesInfo;)Z

    move-result v3

    .line 2126
    .local v3, "capabChanged":Z
    invoke-virtual {v12}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v13

    .line 2127
    .local v13, "oldStatus":I
    invoke-virtual {v9}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v10

    .line 2129
    .local v10, "newStatus":I
    const/16 v16, 0x3

    move/from16 v0, v16

    if-eq v10, v0, :cond_e

    const/4 v6, 0x1

    .line 2130
    .local v6, "isActive":Z
    :goto_5
    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v13, v0, :cond_f

    if-eq v13, v10, :cond_f

    const/4 v1, 0x1

    .line 2132
    .local v1, "becameActive":Z
    :goto_6
    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v10, v0, :cond_10

    if-eq v13, v10, :cond_10

    const/4 v2, 0x1

    .line 2135
    .local v2, "becameInactive":Z
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrinterAvailabilityDetector:Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;
    invoke-static/range {v16 .. v16}, Lcom/android/printspooler/ui/PrintActivity;->access$2700(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->updatePrinter(Landroid/print/PrinterInfo;)V

    .line 2137
    invoke-virtual {v12, v9}, Landroid/print/PrinterInfo;->copyFrom(Landroid/print/PrinterInfo;)V

    .line 2139
    if-eqz v6, :cond_2

    if-nez v4, :cond_3

    :cond_2
    if-eqz v1, :cond_11

    if-eqz v5, :cond_11

    .line 2140
    :cond_3
    if-eqz v5, :cond_4

    if-eqz v3, :cond_4

    .line 2141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->updatePrintAttributesFromCapabilities(Landroid/print/PrinterCapabilitiesInfo;)V
    invoke-static {v0, v8}, Lcom/android/printspooler/ui/PrintActivity;->access$2800(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrinterCapabilitiesInfo;)V

    .line 2142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->updatePrintPreviewController(Z)V
    invoke-static/range {v16 .. v17}, Lcom/android/printspooler/ui/PrintActivity;->access$2900(Lcom/android/printspooler/ui/PrintActivity;Z)V

    .line 2144
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lcom/android/printspooler/ui/PrintActivity;->onPrinterAvailable(Landroid/print/PrinterInfo;)V

    .line 2149
    :cond_5
    :goto_8
    if-eqz v3, :cond_6

    if-eqz v5, :cond_6

    if-nez v6, :cond_8

    :cond_6
    if-eqz v1, :cond_7

    if-nez v5, :cond_8

    :cond_7
    if-eqz v6, :cond_14

    if-eqz v4, :cond_14

    :cond_8
    const/4 v15, 0x1

    .line 2152
    .local v15, "updateNeeded":Z
    :goto_9
    if-eqz v15, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z
    invoke-static/range {v16 .. v16}, Lcom/android/printspooler/ui/PrintActivity;->access$3000(Lcom/android/printspooler/ui/PrintActivity;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 2153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z
    invoke-static/range {v16 .. v17}, Lcom/android/printspooler/ui/PrintActivity;->access$3100(Lcom/android/printspooler/ui/PrintActivity;Z)Z

    .line 2156
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    goto/16 :goto_0

    .line 2111
    .end local v1    # "becameActive":Z
    .end local v2    # "becameInactive":Z
    .end local v3    # "capabChanged":Z
    .end local v4    # "gotCapab":Z
    .end local v5    # "hasCapab":Z
    .end local v6    # "isActive":Z
    .end local v7    # "lostCapab":Z
    .end local v8    # "newCapab":Landroid/print/PrinterCapabilitiesInfo;
    .end local v10    # "newStatus":I
    .end local v11    # "oldCapab":Landroid/print/PrinterCapabilitiesInfo;
    .end local v13    # "oldStatus":I
    .end local v15    # "updateNeeded":Z
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lcom/android/printspooler/ui/PrintActivity;->onPrinterUnavailable(Landroid/print/PrinterInfo;)V

    goto/16 :goto_1

    .line 2121
    .restart local v8    # "newCapab":Landroid/print/PrinterCapabilitiesInfo;
    .restart local v11    # "oldCapab":Landroid/print/PrinterCapabilitiesInfo;
    :cond_b
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2122
    .restart local v5    # "hasCapab":Z
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 2123
    .restart local v4    # "gotCapab":Z
    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 2129
    .restart local v3    # "capabChanged":Z
    .restart local v7    # "lostCapab":Z
    .restart local v10    # "newStatus":I
    .restart local v13    # "oldStatus":I
    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 2130
    .restart local v6    # "isActive":Z
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_6

    .line 2132
    .restart local v1    # "becameActive":Z
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 2145
    .restart local v2    # "becameInactive":Z
    :cond_11
    if-eqz v2, :cond_12

    if-nez v5, :cond_13

    :cond_12
    if-eqz v6, :cond_5

    if-eqz v7, :cond_5

    .line 2146
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintActivity$PrintersObserver;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lcom/android/printspooler/ui/PrintActivity;->onPrinterUnavailable(Landroid/print/PrinterInfo;)V

    goto :goto_8

    .line 2149
    :cond_14
    const/4 v15, 0x0

    goto :goto_9
.end method
