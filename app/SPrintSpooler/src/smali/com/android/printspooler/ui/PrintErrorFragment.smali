.class public final Lcom/android/printspooler/ui/PrintErrorFragment;
.super Landroid/app/Fragment;
.source "PrintErrorFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/ui/PrintErrorFragment$OnActionListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 42
    return-void
.end method

.method public static newInstance(Ljava/lang/CharSequence;I)Lcom/android/printspooler/ui/PrintErrorFragment;
    .locals 3
    .param p0, "message"    # Ljava/lang/CharSequence;
    .param p1, "action"    # I

    .prologue
    .line 47
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 48
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "EXTRA_MESSAGE"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 49
    const-string v2, "EXTRA_ACTION"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    new-instance v1, Lcom/android/printspooler/ui/PrintErrorFragment;

    invoke-direct {v1}, Lcom/android/printspooler/ui/PrintErrorFragment;-><init>()V

    .line 51
    .local v1, "fragment":Lcom/android/printspooler/ui/PrintErrorFragment;
    invoke-virtual {v1, v0}, Lcom/android/printspooler/ui/PrintErrorFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    const v0, 0x7f030004

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintErrorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "EXTRA_MESSAGE"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 67
    .local v2, "message":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 68
    const v4, 0x7f0d001d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 69
    .local v3, "messageView":Landroid/widget/TextView;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    .end local v3    # "messageView":Landroid/widget/TextView;
    :cond_0
    const v4, 0x7f0d001e

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 74
    .local v1, "actionButton":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintErrorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "EXTRA_ACTION"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 75
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_0
    new-instance v4, Lcom/android/printspooler/ui/PrintErrorFragment$1;

    invoke-direct {v4, p0}, Lcom/android/printspooler/ui/PrintErrorFragment$1;-><init>(Lcom/android/printspooler/ui/PrintErrorFragment;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    return-void

    .line 77
    :pswitch_0
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 78
    const v4, 0x7f09003d

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 82
    :pswitch_1
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
