.class final Lcom/android/printspooler/ui/PrinterRegistry$MyHandler;
.super Landroid/os/Handler;
.source "PrinterRegistry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrinterRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MyHandler"
.end annotation


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 2
    .param p1, "looper"    # Landroid/os/Looper;

    .prologue
    .line 167
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 168
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 173
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 187
    :goto_0
    return-void

    .line 175
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/internal/os/SomeArgs;

    .line 176
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    iget-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    check-cast v1, Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    .line 177
    .local v1, "callback":Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    iget-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 178
    .local v2, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    .line 179
    invoke-interface {v1, v2}, Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;->onPrintersChanged(Ljava/util/List;)V

    goto :goto_0

    .line 183
    .end local v0    # "args":Lcom/android/internal/os/SomeArgs;
    .end local v1    # "callback":Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    .end local v2    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    .line 184
    .restart local v1    # "callback":Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    invoke-interface {v1}, Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;->onPrintersInvalid()V

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
