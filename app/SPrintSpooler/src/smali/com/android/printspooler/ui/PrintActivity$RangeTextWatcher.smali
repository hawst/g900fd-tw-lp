.class final Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RangeTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0

    .prologue
    .line 2267
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p2, "x1"    # Lcom/android/printspooler/ui/PrintActivity$1;

    .prologue
    .line 2267
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 13
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 2280
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4400(Lcom/android/printspooler/ui/PrintActivity;)Z

    move-result v2

    .line 2281
    .local v2, "hadErrors":Z
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2283
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2285
    .local v8, "text":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2286
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2287
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 2341
    :cond_0
    :goto_0
    return-void

    .line 2291
    :cond_1
    # getter for: Lcom/android/printspooler/ui/PrintActivity;->PATTERN_ESCAPE_SPECIAL_CHARS:Ljava/util/regex/Pattern;
    invoke-static {}, Lcom/android/printspooler/ui/PrintActivity;->access$4500()Ljava/util/regex/Pattern;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    const-string v10, "////"

    invoke-virtual {v9, v10}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2292
    .local v1, "escapedText":Ljava/lang/String;
    # getter for: Lcom/android/printspooler/ui/PrintActivity;->PATTERN_PAGE_RANGE:Ljava/util/regex/Pattern;
    invoke-static {}, Lcom/android/printspooler/ui/PrintActivity;->access$4600()Ljava/util/regex/Pattern;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-nez v9, :cond_2

    .line 2293
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2294
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    goto :goto_0

    .line 2298
    :cond_2
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$600(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/model/RemotePrintDocument;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/printspooler/model/RemotePrintDocument;->getDocumentInfo()Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;

    move-result-object v9

    iget-object v3, v9, Lcom/android/printspooler/model/RemotePrintDocument$RemotePrintDocumentInfo;->info:Landroid/print/PrintDocumentInfo;

    .line 2299
    .local v3, "info":Landroid/print/PrintDocumentInfo;
    if-eqz v3, :cond_5

    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->getAdjustedPageCount(Landroid/print/PrintDocumentInfo;)I
    invoke-static {v9, v3}, Lcom/android/printspooler/ui/PrintActivity;->access$4700(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrintDocumentInfo;)I

    move-result v6

    .line 2302
    .local v6, "pageCount":I
    :goto_1
    # getter for: Lcom/android/printspooler/ui/PrintActivity;->PATTERN_DIGITS:Ljava/util/regex/Pattern;
    invoke-static {}, Lcom/android/printspooler/ui/PrintActivity;->access$4800()Ljava/util/regex/Pattern;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 2303
    .local v4, "matcher":Ljava/util/regex/Matcher;
    :cond_3
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2304
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->start()I

    move-result v9

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->end()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 2305
    .local v5, "numericString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 2309
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 2310
    .local v7, "pageIndex":I
    if-lt v7, v11, :cond_4

    if-le v7, v6, :cond_3

    .line 2311
    :cond_4
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2312
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2315
    .end local v7    # "pageIndex":I
    :catch_0
    move-exception v0

    .line 2316
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2317
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    goto/16 :goto_0

    .line 2299
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v4    # "matcher":Ljava/util/regex/Matcher;
    .end local v5    # "numericString":Ljava/lang/String;
    .end local v6    # "pageCount":I
    :cond_5
    const/4 v6, 0x0

    goto :goto_1

    .line 2323
    .restart local v4    # "matcher":Ljava/util/regex/Matcher;
    .restart local v6    # "pageCount":I
    :cond_6
    :try_start_1
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->computeSelectedPages()[Landroid/print/PageRange;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4900(Lcom/android/printspooler/ui/PrintActivity;)[Landroid/print/PageRange;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2334
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2335
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$1600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ImageView;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 2336
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 2338
    if-eqz v2, :cond_0

    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->hasErrors()Z
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4400(Lcom/android/printspooler/ui/PrintActivity;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2339
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    goto/16 :goto_0

    .line 2324
    :catch_1
    move-exception v0

    .line 2325
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v9}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 2326
    iget-object v9, p0, Lcom/android/printspooler/ui/PrintActivity$RangeTextWatcher;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    goto/16 :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 2276
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 2271
    return-void
.end method
