.class Lcom/android/printspooler/ui/SelectPrinterActivity$1;
.super Landroid/database/DataSetObserver;
.source "SelectPrinterActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

.field final synthetic val$adapter:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity;Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    iput-object p2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->val$adapter:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->val$adapter:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->val$adapter:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->updateEmptyView(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)V

    .line 124
    :cond_0
    return-void
.end method

.method public onInvalidated()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$1;->val$adapter:Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->updateEmptyView(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)V

    .line 131
    :cond_0
    return-void
.end method
