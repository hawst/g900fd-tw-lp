.class public interface abstract Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
.super Ljava/lang/Object;
.source "PrinterRegistry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrinterRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnPrintersChangeListener"
.end annotation


# virtual methods
.method public abstract onPrintersChanged(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onPrintersInvalid()V
.end method
