.class Lcom/android/printspooler/ui/PrintActivity$3;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Lcom/android/printspooler/model/RemotePrintDocument$RemoteAdapterDeathObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/PrintActivity;->onPrinterRegistryReady(Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$3;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDied()V
    .locals 3

    .prologue
    .line 315
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$3;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$3;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mState:I
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$400(Lcom/android/printspooler/ui/PrintActivity;)I

    move-result v0

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->isFinalState(I)Z
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$500(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$3;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintedDocument:Lcom/android/printspooler/model/RemotePrintDocument;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$600(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/model/RemotePrintDocument;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/printspooler/model/RemotePrintDocument;->isUpdating()Z

    move-result v0

    if-nez v0, :cond_1

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    const-string v0, "PrintActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDied client application died: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$3;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCallingPackageName:Ljava/lang/String;
    invoke-static {v2}, Lcom/android/printspooler/ui/PrintActivity;->access$700(Lcom/android/printspooler/ui/PrintActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$3;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    const/4 v1, 0x3

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->setState(I)V
    invoke-static {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->access$800(Lcom/android/printspooler/ui/PrintActivity;I)V

    .line 320
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$3;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->doFinish()V
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$900(Lcom/android/printspooler/ui/PrintActivity;)V

    goto :goto_0
.end method
