.class final Lcom/android/printspooler/ui/PrintActivity$MyClickListener;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0

    .prologue
    .line 1166
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p2, "x1"    # Lcom/android/printspooler/ui/PrintActivity$1;

    .prologue
    .line 1166
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1169
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$1600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 1170
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$1700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1171
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->confirmPrint()V
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$1800(Lcom/android/printspooler/ui/PrintActivity;)V

    .line 1180
    :cond_0
    :goto_0
    return-void

    .line 1173
    :cond_1
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->cancelPrint()V
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$1900(Lcom/android/printspooler/ui/PrintActivity;)V

    goto :goto_0

    .line 1175
    :cond_2
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mMoreOptionsButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$2000(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1176
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$1700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1177
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$MyClickListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrintActivity;->access$1700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;

    move-result-object v1

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->startAdvancedPrintOptionsActivity(Landroid/print/PrinterInfo;)V
    invoke-static {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->access$2100(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrinterInfo;)V

    goto :goto_0
.end method
