.class final Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyOnItemSelectedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0

    .prologue
    .line 2172
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p2, "x1"    # Lcom/android/printspooler/ui/PrintActivity$1;

    .prologue
    .line 2172
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2175
    .local p1, "spinner":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$2300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    if-ne p1, v8, :cond_8

    .line 2176
    const/4 v8, -0x1

    if-ne p3, v8, :cond_1

    .line 2249
    :cond_0
    :goto_0
    return-void

    .line 2180
    :cond_1
    const-wide/32 v8, 0x7ffffffe

    cmp-long v8, p4, v8

    if-nez v8, :cond_2

    .line 2181
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->startSelectPrinterActivity()V
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3200(Lcom/android/printspooler/ui/PrintActivity;)V

    goto :goto_0

    .line 2185
    :cond_2
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$2300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    .line 2186
    .local v3, "currentItem":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    if-eqz v3, :cond_7

    iget-object v4, v3, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->printer:Landroid/print/PrinterInfo;

    .line 2189
    .local v4, "currentPrinter":Landroid/print/PrinterInfo;
    :goto_1
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$1700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrinterInfo;

    move-result-object v8

    if-eq v8, v4, :cond_0

    .line 2193
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # setter for: Lcom/android/printspooler/ui/PrintActivity;->mCurrentPrinter:Landroid/print/PrinterInfo;
    invoke-static {v8, v4}, Lcom/android/printspooler/ui/PrintActivity;->access$1702(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrinterInfo;)Landroid/print/PrinterInfo;

    .line 2195
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$2200(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    move-result-object v8

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPrinterHolder(Landroid/print/PrinterId;)Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;

    move-result-object v7

    .line 2197
    .local v7, "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    iget-boolean v8, v7, Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;->removed:Z

    if-nez v8, :cond_3

    .line 2198
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    const/4 v9, 0x1

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->setState(I)V
    invoke-static {v8, v9}, Lcom/android/printspooler/ui/PrintActivity;->access$800(Lcom/android/printspooler/ui/PrintActivity;I)V

    .line 2199
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$2200(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->pruneRemovedPrinters()V

    .line 2200
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->ensurePreviewUiShown()V
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3300(Lcom/android/printspooler/ui/PrintActivity;)V

    .line 2203
    :cond_3
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3400(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrintJobInfo;

    move-result-object v8

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/print/PrintJobInfo;->setPrinterId(Landroid/print/PrinterId;)V

    .line 2204
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3400(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrintJobInfo;

    move-result-object v8

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/print/PrintJobInfo;->setPrinterName(Ljava/lang/String;)V

    .line 2206
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$2400(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrinterRegistry;

    move-result-object v8

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/printspooler/ui/PrinterRegistry;->setTrackedPrinter(Landroid/print/PrinterId;)V

    .line 2208
    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v1

    .line 2209
    .local v1, "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    if-eqz v1, :cond_4

    .line 2210
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->updatePrintAttributesFromCapabilities(Landroid/print/PrinterCapabilitiesInfo;)V
    invoke-static {v8, v1}, Lcom/android/printspooler/ui/PrintActivity;->access$2800(Lcom/android/printspooler/ui/PrintActivity;Landroid/print/PrinterCapabilitiesInfo;)V

    .line 2213
    :cond_4
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrinterAvailabilityDetector:Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$2700(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->updatePrinter(Landroid/print/PrinterInfo;)V

    .line 2244
    .end local v1    # "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    .end local v3    # "currentItem":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    .end local v4    # "currentPrinter":Landroid/print/PrinterInfo;
    .end local v7    # "printerHolder":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_5
    :goto_2
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->canUpdateDocument()Z
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3000(Lcom/android/printspooler/ui/PrintActivity;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2245
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    const/4 v9, 0x0

    # invokes: Lcom/android/printspooler/ui/PrintActivity;->updateDocument(Z)Z
    invoke-static {v8, v9}, Lcom/android/printspooler/ui/PrintActivity;->access$3100(Lcom/android/printspooler/ui/PrintActivity;Z)Z

    .line 2248
    :cond_6
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v8}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    goto/16 :goto_0

    .line 2186
    .restart local v3    # "currentItem":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 2214
    .end local v3    # "currentItem":Lcom/android/printspooler/ui/PrintActivity$PrinterHolder;
    :cond_8
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3500(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    if-ne p1, v8, :cond_a

    .line 2215
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinnerAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3600(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v8

    invoke-virtual {v8, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    .line 2216
    .local v5, "mediaItem":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Landroid/print/PrintAttributes$MediaSize;>;"
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3400(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrintJobInfo;

    move-result-object v8

    invoke-virtual {v8}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v0

    .line 2217
    .local v0, "attributes":Landroid/print/PrintAttributes;
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v8

    if-nez v8, :cond_9

    .line 2218
    iget-object v8, v5, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    check-cast v8, Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v8}, Landroid/print/PrintAttributes$MediaSize;->asPortrait()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    goto :goto_2

    .line 2220
    :cond_9
    iget-object v8, v5, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    check-cast v8, Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v8}, Landroid/print/PrintAttributes$MediaSize;->asLandscape()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/print/PrintAttributes;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)V

    goto :goto_2

    .line 2222
    .end local v0    # "attributes":Landroid/print/PrintAttributes;
    .end local v5    # "mediaItem":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Landroid/print/PrintAttributes$MediaSize;>;"
    :cond_a
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3800(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    if-ne p1, v8, :cond_b

    .line 2223
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mColorModeSpinnerAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3900(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v8

    invoke-virtual {v8, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    .line 2224
    .local v2, "colorModeItem":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;"
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3400(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrintJobInfo;

    move-result-object v8

    invoke-virtual {v8}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v9

    iget-object v8, v2, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v9, v8}, Landroid/print/PrintAttributes;->setColorMode(I)V

    .line 2225
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintPreviewController:Lcom/android/printspooler/ui/PrintPreviewController;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$4000(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintPreviewController;

    move-result-object v9

    iget-object v8, v2, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/android/printspooler/ui/PrintPreviewController;->onColorModeChanged(I)V

    goto/16 :goto_2

    .line 2226
    .end local v2    # "colorModeItem":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;"
    :cond_b
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3700(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    if-ne p1, v8, :cond_d

    .line 2227
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mOrientationSpinnerAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$4100(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v8

    invoke-virtual {v8, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;

    .line 2228
    .local v6, "orientationItem":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;"
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPrintJob:Landroid/print/PrintJobInfo;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3400(Lcom/android/printspooler/ui/PrintActivity;)Landroid/print/PrintJobInfo;

    move-result-object v8

    invoke-virtual {v8}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v0

    .line 2229
    .restart local v0    # "attributes":Landroid/print/PrintAttributes;
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mMediaSizeSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$3500(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 2230
    iget-object v8, v6, Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;->value:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-nez v8, :cond_c

    .line 2231
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->asPortrait()Landroid/print/PrintAttributes;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/print/PrintAttributes;->copyFrom(Landroid/print/PrintAttributes;)V

    goto/16 :goto_2

    .line 2233
    :cond_c
    invoke-virtual {v0}, Landroid/print/PrintAttributes;->asLandscape()Landroid/print/PrintAttributes;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/print/PrintAttributes;->copyFrom(Landroid/print/PrintAttributes;)V

    goto/16 :goto_2

    .line 2236
    .end local v0    # "attributes":Landroid/print/PrintAttributes;
    .end local v6    # "orientationItem":Lcom/android/printspooler/ui/PrintActivity$SpinnerItem;, "Lcom/android/printspooler/ui/PrintActivity$SpinnerItem<Ljava/lang/Integer;>;"
    :cond_d
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$4200(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    if-ne p1, v8, :cond_5

    .line 2237
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mRangeOptionsSpinner:Landroid/widget/Spinner;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$4200(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v8

    if-nez v8, :cond_e

    .line 2238
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2239
    :cond_e
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2240
    iget-object v8, p0, Lcom/android/printspooler/ui/PrintActivity$MyOnItemSelectedListener;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mPageRangeEditText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/android/printspooler/ui/PrintActivity;->access$4300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/EditText;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2254
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
