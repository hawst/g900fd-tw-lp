.class Lcom/android/printspooler/ui/PrinterRegistry$1;
.super Ljava/lang/Object;
.source "PrinterRegistry.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrinterRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/print/PrinterInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/PrinterRegistry;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/PrinterRegistry;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 2
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 155
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 156
    new-instance v0, Lcom/android/printspooler/ui/FusedPrintersProvider;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$500(Lcom/android/printspooler/ui/PrinterRegistry;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/printspooler/ui/FusedPrintersProvider;-><init>(Landroid/content/Context;)V

    .line 158
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 113
    check-cast p2, Ljava/util/List;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/android/printspooler/ui/PrinterRegistry$1;->onLoadFinished(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Ljava/util/List<Landroid/print/PrinterInfo;>;>;"
    .local p2, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v3, 0x1

    .line 131
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 132
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mPrinters:Ljava/util/List;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$000(Lcom/android/printspooler/ui/PrinterRegistry;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 133
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mPrinters:Ljava/util/List;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$000(Lcom/android/printspooler/ui/PrinterRegistry;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 134
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mOnPrintersChangeListener:Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$100(Lcom/android/printspooler/ui/PrinterRegistry;)Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 138
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    move-result-object v0

    .line 139
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mOnPrintersChangeListener:Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$100(Lcom/android/printspooler/ui/PrinterRegistry;)Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    .line 140
    iput-object p2, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    .line 141
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$200(Lcom/android/printspooler/ui/PrinterRegistry;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 143
    .end local v0    # "args":Lcom/android/internal/os/SomeArgs;
    :cond_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mReady:Z
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$300(Lcom/android/printspooler/ui/PrinterRegistry;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 144
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # setter for: Lcom/android/printspooler/ui/PrinterRegistry;->mReady:Z
    invoke-static {v1, v3}, Lcom/android/printspooler/ui/PrinterRegistry;->access$302(Lcom/android/printspooler/ui/PrinterRegistry;Z)Z

    .line 145
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mReadyCallback:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$400(Lcom/android/printspooler/ui/PrinterRegistry;)Ljava/lang/Runnable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mReadyCallback:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$400(Lcom/android/printspooler/ui/PrinterRegistry;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 150
    :cond_1
    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Ljava/util/List<Landroid/print/PrinterInfo;>;>;"
    const/4 v2, 0x1

    .line 116
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 117
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mPrinters:Ljava/util/List;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrinterRegistry;->access$000(Lcom/android/printspooler/ui/PrinterRegistry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 118
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mOnPrintersChangeListener:Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrinterRegistry;->access$100(Lcom/android/printspooler/ui/PrinterRegistry;)Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrinterRegistry;->access$200(Lcom/android/printspooler/ui/PrinterRegistry;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry$1;->this$0:Lcom/android/printspooler/ui/PrinterRegistry;

    # getter for: Lcom/android/printspooler/ui/PrinterRegistry;->mOnPrintersChangeListener:Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrinterRegistry;->access$100(Lcom/android/printspooler/ui/PrinterRegistry;)Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 126
    :cond_0
    return-void
.end method
