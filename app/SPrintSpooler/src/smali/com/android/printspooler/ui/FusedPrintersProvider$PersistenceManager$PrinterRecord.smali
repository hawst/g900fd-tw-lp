.class final Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;
.super Ljava/lang/Object;
.source "FusedPrintersProvider.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PrinterRecord"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;",
        ">;"
    }
.end annotation


# instance fields
.field public final printer:Landroid/print/PrinterInfo;

.field final synthetic this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

.field public weight:F


# direct methods
.method public constructor <init>(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;Landroid/print/PrinterInfo;)V
    .locals 0
    .param p2, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 497
    iput-object p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;->this$1:Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 498
    iput-object p2, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;->printer:Landroid/print/PrinterInfo;

    .line 499
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;)I
    .locals 2
    .param p1, "another"    # Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;

    .prologue
    .line 503
    iget v0, p1, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;->weight:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;->weight:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 493
    check-cast p1, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;->compareTo(Lcom/android/printspooler/ui/FusedPrintersProvider$PersistenceManager$PrinterRecord;)I

    move-result v0

    return v0
.end method
