.class final Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;
.super Ljava/lang/Object;
.source "SelectPrinterActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AnnounceFilterResult"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V
    .locals 0

    .prologue
    .line 608
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity;Lcom/android/printspooler/ui/SelectPrinterActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity;
    .param p2, "x1"    # Lcom/android/printspooler/ui/SelectPrinterActivity$1;

    .prologue
    .line 608
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V

    return-void
.end method


# virtual methods
.method public post()V
    .locals 4

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->remove()V

    .line 613
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$000(Lcom/android/printspooler/ui/SelectPrinterActivity;)Landroid/widget/ListView;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Landroid/widget/ListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 614
    return-void
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$000(Lcom/android/printspooler/ui/SelectPrinterActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 618
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 622
    iget-object v2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$000(Lcom/android/printspooler/ui/SelectPrinterActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 624
    .local v0, "count":I
    if-gtz v0, :cond_0

    .line 625
    iget-object v2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    const v3, 0x7f09002d

    invoke-virtual {v2, v3}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 630
    .local v1, "text":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$000(Lcom/android/printspooler/ui/SelectPrinterActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 631
    return-void

    .line 627
    .end local v1    # "text":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-virtual {v2}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0a0000

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0
.end method
