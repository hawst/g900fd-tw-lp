.class Lcom/android/printspooler/ui/SelectPrinterActivity$2;
.super Ljava/lang/Object;
.source "SelectPrinterActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$2;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$2;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$000(Lcom/android/printspooler/ui/SelectPrinterActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-virtual {v1, p3}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->isActionable(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$2;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$000(Lcom/android/printspooler/ui/SelectPrinterActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    .line 143
    .local v0, "printer":Landroid/print/PrinterInfo;
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$2;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v2

    # invokes: Lcom/android/printspooler/ui/SelectPrinterActivity;->onPrinterSelected(Landroid/print/PrinterId;)V
    invoke-static {v1, v2}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$100(Lcom/android/printspooler/ui/SelectPrinterActivity;Landroid/print/PrinterId;)V

    goto :goto_0
.end method
