.class public final Lcom/android/printspooler/ui/SelectPrinterActivity;
.super Landroid/app/Activity;
.source "SelectPrinterActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;,
        Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;,
        Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;
    }
.end annotation


# instance fields
.field private locale:Ljava/util/Locale;

.field private final mAddPrinterServices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/printservice/PrintServiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAnnounceFilterResult:Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;

.field private mListView:Landroid/widget/ListView;

.field private mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAddPrinterServices:Ljava/util/ArrayList;

    .line 608
    return-void
.end method

.method static synthetic access$000(Lcom/android/printspooler/ui/SelectPrinterActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/printspooler/ui/SelectPrinterActivity;Landroid/print/PrinterId;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity;
    .param p1, "x1"    # Landroid/print/PrinterId;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->onPrinterSelected(Landroid/print/PrinterId;)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/printspooler/ui/SelectPrinterActivity;)Lcom/android/printspooler/ui/PrinterRegistry;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/printspooler/ui/SelectPrinterActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->announceSearchResultIfNeeded()V

    return-void
.end method

.method private announceSearchResultIfNeeded()V
    .locals 2

    .prologue
    .line 361
    invoke-static {p0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAnnounceFilterResult:Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;

    if-nez v0, :cond_0

    .line 363
    new-instance v0, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity;Lcom/android/printspooler/ui/SelectPrinterActivity$1;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAnnounceFilterResult:Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAnnounceFilterResult:Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->post()V

    .line 367
    :cond_1
    return-void
.end method

.method private onPrinterSelected(Landroid/print/PrinterId;)V
    .locals 2
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 266
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 267
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "INTENT_EXTRA_PRINTER_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 268
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->setResult(ILandroid/content/Intent;)V

    .line 269
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->finish()V

    .line 270
    return-void
.end method

.method private showAddPrinterSelectionDialog()V
    .locals 6

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 328
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_TAG_ADD_PRINTER_DIALOG"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 330
    .local v2, "oldFragment":Landroid/app/Fragment;
    if-eqz v2, :cond_0

    .line 331
    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 333
    :cond_0
    new-instance v1, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;

    invoke-direct {v1}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;-><init>()V

    .line 334
    .local v1, "newFragment":Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 335
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v4, "FRAGMENT_ARGUMENT_PRINT_SERVICE_INFOS"

    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAddPrinterServices:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 337
    invoke-virtual {v1, v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 338
    const-string v4, "FRAGMENT_TAG_ADD_PRINTER_DIALOG"

    invoke-virtual {v3, v1, v4}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 339
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 340
    iget-object v4, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v4, :cond_1

    .line 341
    iget-object v4, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->clearFocus()V

    .line 342
    :cond_1
    return-void
.end method

.method private updateServicesWithAddPrinterActivity()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 283
    iget-object v11, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAddPrinterServices:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 286
    const-string v11, "print"

    invoke-virtual {p0, v11}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/print/PrintManager;

    .line 287
    .local v8, "printManager":Landroid/print/PrintManager;
    invoke-virtual {v8}, Landroid/print/PrintManager;->getEnabledPrintServices()Ljava/util/List;

    move-result-object v5

    .line 290
    .local v5, "enabledServices":Ljava/util/List;, "Ljava/util/List<Landroid/printservice/PrintServiceInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 324
    :cond_0
    return-void

    .line 295
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 296
    .local v4, "enabledServiceCount":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v4, :cond_0

    .line 297
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/printservice/PrintServiceInfo;

    .line 300
    .local v3, "enabledService":Landroid/printservice/PrintServiceInfo;
    invoke-virtual {v3}, Landroid/printservice/PrintServiceInfo;->getAddPrintersActivityName()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 296
    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 304
    :cond_3
    invoke-virtual {v3}, Landroid/printservice/PrintServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v11

    iget-object v10, v11, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 305
    .local v10, "serviceInfo":Landroid/content/pm/ServiceInfo;
    new-instance v1, Landroid/content/ComponentName;

    iget-object v11, v10, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/printservice/PrintServiceInfo;->getAddPrintersActivityName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v11, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    .local v1, "addPrintersComponentName":Landroid/content/ComponentName;
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v11, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 311
    .local v2, "addPritnersIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 312
    .local v7, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v7, v2, v13}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v9

    .line 313
    .local v9, "resolvedActivities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 315
    invoke-interface {v9, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/pm/ResolveInfo;

    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 316
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    iget-boolean v11, v0, Landroid/content/pm/ActivityInfo;->exported:Z

    if-eqz v11, :cond_2

    iget-object v11, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    if-eqz v11, :cond_4

    iget-object v11, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    if-nez v11, :cond_2

    .line 320
    :cond_4
    iget-object v11, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAddPrinterServices:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 275
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 276
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->locale:Ljava/util/Locale;

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    const-string v0, "SelectPrinterFragment"

    const-string v1, "Finishing Activity on Language change"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->finish()V

    .line 280
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 227
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 238
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 229
    :pswitch_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EXTRA_PRINTER_ID"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterId;

    .line 230
    .local v0, "printerId":Landroid/print/PrinterId;
    invoke-direct {p0, v0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->onPrinterSelected(Landroid/print/PrinterId;)V

    goto :goto_0

    .line 234
    .end local v0    # "printerId":Landroid/print/PrinterId;
    :pswitch_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EXTRA_PRINTER_ID"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterId;

    .line 235
    .restart local v0    # "printerId":Landroid/print/PrinterId;
    iget-object v2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    invoke-virtual {v2, v0}, Lcom/android/printspooler/ui/PrinterRegistry;->forgetFavoritePrinter(Landroid/print/PrinterId;)V

    goto :goto_0

    .line 227
    :pswitch_data_0
    .packed-switch 0x7f090029
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const v2, 0x7f020007

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setIcon(I)V

    .line 108
    const v1, 0x7f030008

    invoke-virtual {p0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->setContentView(I)V

    .line 110
    new-instance v1, Lcom/android/printspooler/ui/PrinterRegistry;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/printspooler/ui/PrinterRegistry;-><init>(Landroid/app/Activity;Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    .line 113
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->locale:Ljava/util/Locale;

    .line 116
    const v1, 0x102000a

    invoke-virtual {p0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    .line 117
    new-instance v0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-direct {v0, p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V

    .line 118
    .local v0, "adapter":Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;
    new-instance v1, Lcom/android/printspooler/ui/SelectPrinterActivity$1;

    invoke-direct {v1, p0, v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$1;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity;Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)V

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 133
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 135
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/android/printspooler/ui/SelectPrinterActivity$2;

    invoke-direct {v2, p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$2;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 147
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0, v1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 148
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    const v9, 0x7f09002a

    const v8, 0x7f090029

    const/4 v7, 0x0

    .line 199
    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    if-ne p2, v5, :cond_1

    .line 200
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .end local p3    # "menuInfo":Landroid/view/ContextMenu$ContextMenuInfo;
    iget v2, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    .line 201
    .local v2, "position":I
    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-interface {v5, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/print/PrinterInfo;

    .line 203
    .local v3, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {v3}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 206
    invoke-virtual {v3}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_0

    .line 207
    invoke-interface {p1, v7, v8, v7, v8}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v4

    .line 209
    .local v4, "selectItem":Landroid/view/MenuItem;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 210
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "EXTRA_PRINTER_ID"

    invoke-virtual {v3}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 211
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    .line 215
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v4    # "selectItem":Landroid/view/MenuItem;
    :cond_0
    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;

    invoke-virtual {v3}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/printspooler/ui/PrinterRegistry;->isFavoritePrinter(Landroid/print/PrinterId;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 216
    invoke-interface {p1, v7, v9, v7, v9}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 218
    .local v0, "forgetItem":Landroid/view/MenuItem;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 219
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v5, "EXTRA_PRINTER_ID"

    invoke-virtual {v3}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 220
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    .line 223
    .end local v0    # "forgetItem":Landroid/view/MenuItem;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "position":I
    .end local v3    # "printer":Landroid/print/PrinterInfo;
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 152
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 154
    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f0c0000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 156
    const v1, 0x7f0d0025

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 157
    .local v0, "searchItem":Landroid/view/MenuItem;
    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mSearchView:Landroid/widget/SearchView;

    .line 158
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 159
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x104000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Lcom/android/printspooler/ui/SelectPrinterActivity$3;

    invoke-direct {v2, p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$3;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 172
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Lcom/android/printspooler/ui/SelectPrinterActivity$4;

    invoke-direct {v2, p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$4;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 190
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAddPrinterServices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    const v1, 0x7f0d0026

    invoke-interface {p1, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 194
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 258
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d0026

    if-ne v0, v1, :cond_0

    .line 259
    invoke-direct {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->showAddPrinterSelectionDialog()V

    .line 260
    const/4 v0, 0x1

    .line 262
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAnnounceFilterResult:Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mAnnounceFilterResult:Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$AnnounceFilterResult;->remove()V

    .line 253
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 254
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 243
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 244
    invoke-direct {p0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->updateServicesWithAddPrinterActivity()V

    .line 246
    return-void
.end method

.method public updateEmptyView(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)V
    .locals 4
    .param p1, "adapter"    # Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    .prologue
    .line 345
    iget-object v3, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_0

    .line 346
    const v3, 0x7f0d0023

    invoke-virtual {p0, v3}, Lcom/android/printspooler/ui/SelectPrinterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 347
    .local v0, "emptyView":Landroid/view/View;
    iget-object v3, p0, Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 349
    .end local v0    # "emptyView":Landroid/view/View;
    :cond_0
    const v3, 0x7f0d0021

    invoke-virtual {p0, v3}, Lcom/android/printspooler/ui/SelectPrinterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 350
    .local v2, "titleView":Landroid/widget/TextView;
    const v3, 0x7f0d0024

    invoke-virtual {p0, v3}, Lcom/android/printspooler/ui/SelectPrinterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 351
    .local v1, "progressBar":Landroid/view/View;
    invoke-virtual {p1}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->getUnfilteredCount()I

    move-result v3

    if-gtz v3, :cond_1

    .line 352
    const v3, 0x7f09002c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 353
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 358
    :goto_0
    return-void

    .line 355
    :cond_1
    const v3, 0x7f09002d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 356
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
