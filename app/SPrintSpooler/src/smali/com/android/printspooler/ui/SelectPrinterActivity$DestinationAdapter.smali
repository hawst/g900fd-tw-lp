.class final Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;
.super Landroid/widget/BaseAdapter;
.source "SelectPrinterActivity.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DestinationAdapter"
.end annotation


# instance fields
.field private final mFilteredPrinters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLastSearchString:Ljava/lang/CharSequence;

.field private final mLock:Ljava/lang/Object;

.field private final mPrinters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V
    .locals 2

    .prologue
    .line 447
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 439
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLock:Ljava/lang/Object;

    .line 441
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mPrinters:Ljava/util/List;

    .line 443
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mFilteredPrinters:Ljava/util/List;

    .line 448
    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity;->mPrinterRegistry:Lcom/android/printspooler/ui/PrinterRegistry;
    invoke-static {p1}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$800(Lcom/android/printspooler/ui/SelectPrinterActivity;)Lcom/android/printspooler/ui/PrinterRegistry;

    move-result-object v0

    new-instance v1, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$1;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;Lcom/android/printspooler/ui/SelectPrinterActivity;)V

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrinterRegistry;->setOnPrintersChangeListener(Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;)V

    .line 472
    return-void
.end method

.method static synthetic access$400(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mPrinters:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mFilteredPrinters:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLastSearchString:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$702(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 437
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLastSearchString:Ljava/lang/CharSequence;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 531
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 532
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mFilteredPrinters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 533
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 550
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 476
    new-instance v0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$2;

    invoke-direct {v0, p0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter$2;-><init>(Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 538
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 539
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mFilteredPrinters:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 540
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 545
    int-to-long v0, p1

    return-wide v0
.end method

.method public getUnfilteredCount()I
    .locals 2

    .prologue
    .line 524
    iget-object v1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 525
    :try_start_0
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->mPrinters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 526
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 555
    if-nez p2, :cond_0

    .line 556
    iget-object v9, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v9

    const v10, 0x7f030007

    const/4 v11, 0x0

    invoke-virtual {v9, v10, p3, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 560
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->isActionable(I)Z

    move-result v9

    invoke-virtual {p2, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 562
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/print/PrinterInfo;

    .line 564
    .local v4, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v7

    .line 565
    .local v7, "title":Ljava/lang/CharSequence;
    const/4 v5, 0x0

    .line 566
    .local v5, "subtitle":Ljava/lang/CharSequence;
    const/4 v0, 0x0

    .line 569
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    iget-object v9, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-virtual {v9}, Lcom/android/printspooler/ui/SelectPrinterActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 570
    .local v3, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {v9}, Landroid/print/PrinterId;->getServiceName()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 572
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v9, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v9, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 573
    iget-object v9, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v9, v3}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 578
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    const v9, 0x7f0d0021

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 579
    .local v8, "titleView":Landroid/widget/TextView;
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 581
    const v9, 0x7f0d0022

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 582
    .local v6, "subtitleView":Landroid/widget/TextView;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 583
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 591
    :goto_1
    const v9, 0x7f0d0020

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 592
    .local v1, "iconView":Landroid/widget/ImageView;
    if-eqz v0, :cond_2

    .line 593
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 594
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 599
    :goto_2
    return-object p2

    .line 586
    .end local v1    # "iconView":Landroid/widget/ImageView;
    :cond_1
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    const/16 v9, 0x8

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 596
    .restart local v1    # "iconView":Landroid/widget/ImageView;
    :cond_2
    const/16 v9, 0x8

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 574
    .end local v1    # "iconView":Landroid/widget/ImageView;
    .end local v6    # "subtitleView":Landroid/widget/TextView;
    .end local v8    # "titleView":Landroid/widget/TextView;
    :catch_0
    move-exception v9

    goto :goto_0
.end method

.method public isActionable(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 603
    invoke-virtual {p0, p1}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrinterInfo;

    .line 604
    .local v0, "printer":Landroid/print/PrinterInfo;
    invoke-virtual {v0}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
