.class final Lcom/android/printspooler/ui/PrintActivity$PageShredder;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PageShredder"
.end annotation


# instance fields
.field private final mCallback:Ljava/lang/Runnable;

.field private final mContext:Landroid/content/Context;

.field private final mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

.field private final mPagesToShred:[Landroid/print/PageRange;

.field private final mPrintJob:Landroid/print/PrintJobInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/print/PrintJobInfo;Lcom/android/printspooler/model/MutexFileProvider;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "printJob"    # Landroid/print/PrintJobInfo;
    .param p3, "fileProvider"    # Lcom/android/printspooler/model/MutexFileProvider;
    .param p4, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 2441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2442
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mContext:Landroid/content/Context;

    .line 2443
    iput-object p2, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPrintJob:Landroid/print/PrintJobInfo;

    .line 2444
    iput-object p3, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    .line 2445
    iput-object p4, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mCallback:Ljava/lang/Runnable;

    .line 2446
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->computePagesToShred(Landroid/print/PrintJobInfo;)[Landroid/print/PageRange;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPagesToShred:[Landroid/print/PageRange;

    .line 2447
    return-void
.end method

.method static synthetic access$5200(Lcom/android/printspooler/ui/PrintActivity$PageShredder;Lcom/android/printspooler/renderer/IPdfEditor;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity$PageShredder;
    .param p1, "x1"    # Lcom/android/printspooler/renderer/IPdfEditor;

    .prologue
    .line 2426
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->shredPages(Lcom/android/printspooler/renderer/IPdfEditor;)V

    return-void
.end method

.method static synthetic access$5300(Lcom/android/printspooler/ui/PrintActivity$PageShredder;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    .prologue
    .line 2426
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->updatePrintJob()V

    return-void
.end method

.method static synthetic access$5400(Lcom/android/printspooler/ui/PrintActivity$PageShredder;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    .prologue
    .line 2426
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/android/printspooler/ui/PrintActivity$PageShredder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintActivity$PageShredder;

    .prologue
    .line 2426
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method private static computePagesToShred(Landroid/print/PrintJobInfo;)[Landroid/print/PageRange;
    .locals 12
    .param p0, "printJob"    # Landroid/print/PrintJobInfo;

    .prologue
    .line 2563
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2564
    .local v7, "rangesToShred":Ljava/util/List;, "Ljava/util/List<Landroid/print/PageRange;>;"
    const/4 v3, 0x0

    .line 2566
    .local v3, "previousRange":Landroid/print/PageRange;
    invoke-virtual {p0}, Landroid/print/PrintJobInfo;->getDocumentInfo()Landroid/print/PrintDocumentInfo;

    move-result-object v11

    invoke-virtual {v11}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v2

    .line 2568
    .local v2, "pageCount":I
    invoke-virtual {p0}, Landroid/print/PrintJobInfo;->getPages()[Landroid/print/PageRange;

    move-result-object v4

    .line 2569
    .local v4, "printedPages":[Landroid/print/PageRange;
    array-length v6, v4

    .line 2570
    .local v6, "rangeCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_3

    .line 2571
    aget-object v11, v4, v1

    invoke-static {v11, v2}, Lcom/android/printspooler/util/PageRangeUtils;->asAbsoluteRange(Landroid/print/PageRange;I)Landroid/print/PageRange;

    move-result-object v5

    .line 2573
    .local v5, "range":Landroid/print/PageRange;
    if-nez v3, :cond_2

    .line 2574
    const/4 v10, 0x0

    .line 2575
    .local v10, "startPageIdx":I
    invoke-virtual {v5}, Landroid/print/PageRange;->getStart()I

    move-result v11

    add-int/lit8 v0, v11, -0x1

    .line 2576
    .local v0, "endPageIdx":I
    if-ltz v0, :cond_0

    .line 2577
    new-instance v8, Landroid/print/PageRange;

    const/4 v11, 0x0

    invoke-direct {v8, v11, v0}, Landroid/print/PageRange;-><init>(II)V

    .line 2578
    .local v8, "removedRange":Landroid/print/PageRange;
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2589
    .end local v8    # "removedRange":Landroid/print/PageRange;
    :cond_0
    :goto_1
    add-int/lit8 v11, v6, -0x1

    if-ne v1, v11, :cond_1

    .line 2590
    invoke-virtual {v5}, Landroid/print/PageRange;->getEnd()I

    move-result v11

    add-int/lit8 v10, v11, 0x1

    .line 2591
    invoke-virtual {p0}, Landroid/print/PrintJobInfo;->getDocumentInfo()Landroid/print/PrintDocumentInfo;

    move-result-object v11

    invoke-virtual {v11}, Landroid/print/PrintDocumentInfo;->getPageCount()I

    move-result v11

    add-int/lit8 v0, v11, -0x1

    .line 2592
    if-gt v10, v0, :cond_1

    .line 2593
    new-instance v8, Landroid/print/PageRange;

    invoke-direct {v8, v10, v0}, Landroid/print/PageRange;-><init>(II)V

    .line 2594
    .restart local v8    # "removedRange":Landroid/print/PageRange;
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2598
    .end local v8    # "removedRange":Landroid/print/PageRange;
    :cond_1
    move-object v3, v5

    .line 2570
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2581
    .end local v0    # "endPageIdx":I
    .end local v10    # "startPageIdx":I
    :cond_2
    invoke-virtual {v3}, Landroid/print/PageRange;->getEnd()I

    move-result v11

    add-int/lit8 v10, v11, 0x1

    .line 2582
    .restart local v10    # "startPageIdx":I
    invoke-virtual {v5}, Landroid/print/PageRange;->getStart()I

    move-result v11

    add-int/lit8 v0, v11, -0x1

    .line 2583
    .restart local v0    # "endPageIdx":I
    if-gt v10, v0, :cond_0

    .line 2584
    new-instance v8, Landroid/print/PageRange;

    invoke-direct {v8, v10, v0}, Landroid/print/PageRange;-><init>(II)V

    .line 2585
    .restart local v8    # "removedRange":Landroid/print/PageRange;
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2601
    .end local v0    # "endPageIdx":I
    .end local v5    # "range":Landroid/print/PageRange;
    .end local v8    # "removedRange":Landroid/print/PageRange;
    .end local v10    # "startPageIdx":I
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    new-array v9, v11, [Landroid/print/PageRange;

    .line 2602
    .local v9, "result":[Landroid/print/PageRange;
    invoke-interface {v7, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 2603
    return-object v9
.end method

.method private shredPages(Lcom/android/printspooler/renderer/IPdfEditor;)V
    .locals 13
    .param p1, "editor"    # Lcom/android/printspooler/renderer/IPdfEditor;

    .prologue
    .line 2496
    const/4 v9, 0x0

    .line 2497
    .local v9, "tempFile":Ljava/io/File;
    const/4 v8, 0x0

    .line 2498
    .local v8, "src":Landroid/os/ParcelFileDescriptor;
    const/4 v0, 0x0

    .line 2499
    .local v0, "dst":Landroid/os/ParcelFileDescriptor;
    const/4 v3, 0x0

    .line 2500
    .local v3, "in":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 2502
    .local v6, "out":Ljava/io/OutputStream;
    :try_start_0
    iget-object v10, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/android/printspooler/model/MutexFileProvider;->acquireFile(Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;)Ljava/io/File;

    move-result-object v5

    .line 2503
    .local v5, "jobFile":Ljava/io/File;
    const/high16 v10, 0x30000000

    invoke-static {v5, v10}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v8

    .line 2506
    invoke-interface {p1, v8}, Lcom/android/printspooler/renderer/IPdfEditor;->openDocument(Landroid/os/ParcelFileDescriptor;)I

    .line 2509
    invoke-virtual {v8}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2512
    :try_start_1
    iget-object v10, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPagesToShred:[Landroid/print/PageRange;

    invoke-interface {p1, v10}, Lcom/android/printspooler/renderer/IPdfEditor;->removePages([Landroid/print/PageRange;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2518
    :goto_0
    :try_start_2
    const-string v10, "print_job"

    const-string v11, ".pdf"

    iget-object v12, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v12

    invoke-static {v10, v11, v12}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    .line 2520
    const/high16 v10, 0x30000000

    invoke-static {v9, v10}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 2521
    invoke-interface {p1, v0}, Lcom/android/printspooler/renderer/IPdfEditor;->write(Landroid/os/ParcelFileDescriptor;)V

    .line 2522
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 2525
    invoke-interface {p1}, Lcom/android/printspooler/renderer/IPdfEditor;->closeDocument()V

    .line 2528
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 2529
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2530
    .end local v3    # "in":Ljava/io/InputStream;
    .local v4, "in":Ljava/io/InputStream;
    :try_start_3
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2531
    .end local v6    # "out":Ljava/io/OutputStream;
    .local v7, "out":Ljava/io/OutputStream;
    :try_start_4
    invoke-static {v4, v7}, Llibcore/io/Streams;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2535
    invoke-static {v8}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2536
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2537
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2538
    invoke-static {v7}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2539
    if-eqz v9, :cond_0

    .line 2540
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2542
    :cond_0
    iget-object v10, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    invoke-virtual {v10}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    move-object v6, v7

    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .line 2544
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "jobFile":Ljava/io/File;
    .restart local v3    # "in":Ljava/io/InputStream;
    :goto_1
    return-void

    .line 2513
    .restart local v5    # "jobFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 2514
    .local v2, "ex":Ljava/lang/IllegalArgumentException;
    :try_start_5
    const-string v10, "PrintActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error removing pages "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPagesToShred:[Landroid/print/PageRange;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2515
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 2532
    .end local v2    # "ex":Ljava/lang/IllegalArgumentException;
    .end local v5    # "jobFile":Ljava/io/File;
    :catch_1
    move-exception v10

    :goto_2
    move-object v1, v10

    .line 2533
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_6
    const-string v10, "PrintActivity"

    const-string v11, "Error dropping pages"

    invoke-static {v10, v11, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2535
    invoke-static {v8}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2536
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2537
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2538
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2539
    if-eqz v9, :cond_1

    .line 2540
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2542
    :cond_1
    iget-object v10, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    invoke-virtual {v10}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    goto :goto_1

    .line 2535
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    :goto_4
    invoke-static {v8}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2536
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2537
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2538
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 2539
    if-eqz v9, :cond_2

    .line 2540
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2542
    :cond_2
    iget-object v11, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    invoke-virtual {v11}, Lcom/android/printspooler/model/MutexFileProvider;->releaseFile()V

    throw v10

    .line 2532
    :catch_2
    move-exception v10

    :goto_5
    move-object v1, v10

    goto :goto_3

    .line 2535
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "jobFile":Ljava/io/File;
    :catchall_1
    move-exception v10

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_4

    .end local v3    # "in":Ljava/io/InputStream;
    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v7    # "out":Ljava/io/OutputStream;
    :catchall_2
    move-exception v10

    move-object v6, v7

    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_4

    .line 2532
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catch_3
    move-exception v10

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v3    # "in":Ljava/io/InputStream;
    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v7    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v10

    move-object v6, v7

    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catch_5
    move-exception v10

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_5

    .end local v3    # "in":Ljava/io/InputStream;
    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v7    # "out":Ljava/io/OutputStream;
    :catch_6
    move-exception v10

    move-object v6, v7

    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_5
.end method

.method private updatePrintJob()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2548
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v3}, Landroid/print/PrintJobInfo;->getPages()[Landroid/print/PageRange;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/android/printspooler/util/PageRangeUtils;->getNormalizedPageCount([Landroid/print/PageRange;I)I

    move-result v1

    .line 2550
    .local v1, "newPageCount":I
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPrintJob:Landroid/print/PrintJobInfo;

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/print/PageRange;

    sget-object v5, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Landroid/print/PrintJobInfo;->setPages([Landroid/print/PageRange;)V

    .line 2553
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v3}, Landroid/print/PrintJobInfo;->getDocumentInfo()Landroid/print/PrintDocumentInfo;

    move-result-object v2

    .line 2554
    .local v2, "oldDocInfo":Landroid/print/PrintDocumentInfo;
    new-instance v3, Landroid/print/PrintDocumentInfo$Builder;

    invoke-virtual {v2}, Landroid/print/PrintDocumentInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/print/PrintDocumentInfo;->getContentType()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v0

    .line 2559
    .local v0, "newDocInfo":Landroid/print/PrintDocumentInfo;
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPrintJob:Landroid/print/PrintJobInfo;

    invoke-virtual {v3, v0}, Landroid/print/PrintJobInfo;->setDocumentInfo(Landroid/print/PrintDocumentInfo;)V

    .line 2560
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 2465
    invoke-static {p2}, Lcom/android/printspooler/renderer/IPdfEditor$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/printspooler/renderer/IPdfEditor;

    move-result-object v0

    .line 2466
    .local v0, "editor":Lcom/android/printspooler/renderer/IPdfEditor;
    new-instance v1, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;

    invoke-direct {v1, p0, v0}, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;-><init>(Lcom/android/printspooler/ui/PrintActivity$PageShredder;Lcom/android/printspooler/renderer/IPdfEditor;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lcom/android/printspooler/ui/PrintActivity$PageShredder$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2488
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 2493
    return-void
.end method

.method public shred()V
    .locals 3

    .prologue
    .line 2451
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mPagesToShred:[Landroid/print/PageRange;

    array-length v1, v1

    if-gtz v1, :cond_0

    .line 2452
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mCallback:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 2461
    :goto_0
    return-void

    .line 2458
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.printspooler.renderer.ACTION_GET_EDITOR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2459
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/printspooler/renderer/PdfManipulationService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2460
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$PageShredder;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method
