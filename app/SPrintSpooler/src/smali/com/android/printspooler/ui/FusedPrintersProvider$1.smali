.class Lcom/android/printspooler/ui/FusedPrintersProvider$1;
.super Ljava/lang/Object;
.source "FusedPrintersProvider.java"

# interfaces
.implements Landroid/print/PrinterDiscoverySession$OnPrintersChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/FusedPrintersProvider;->loadInternal()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/FusedPrintersProvider;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$1;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrintersChanged()V
    .locals 4

    .prologue
    .line 184
    iget-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$1;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;
    invoke-static {v1}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$100(Lcom/android/printspooler/ui/FusedPrintersProvider;)Landroid/print/PrinterDiscoverySession;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrinterDiscoverySession;->getPrinters()Ljava/util/List;

    move-result-object v0

    .line 186
    .local v0, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const-string v1, "FusedPrintersProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPrintersChanged() count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$1;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const-string v1, "FusedPrintersProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPrintersChanged() printers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    iget-object v1, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$1;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    iget-object v2, p0, Lcom/android/printspooler/ui/FusedPrintersProvider$1;->this$0:Lcom/android/printspooler/ui/FusedPrintersProvider;

    # getter for: Lcom/android/printspooler/ui/FusedPrintersProvider;->mFavoritePrinters:Ljava/util/List;
    invoke-static {v2}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$200(Lcom/android/printspooler/ui/FusedPrintersProvider;)Ljava/util/List;

    move-result-object v2

    # invokes: Lcom/android/printspooler/ui/FusedPrintersProvider;->updatePrinters(Ljava/util/List;Ljava/util/List;)V
    invoke-static {v1, v0, v2}, Lcom/android/printspooler/ui/FusedPrintersProvider;->access$300(Lcom/android/printspooler/ui/FusedPrintersProvider;Ljava/util/List;Ljava/util/List;)V

    .line 194
    return-void
.end method
