.class Lcom/android/printspooler/ui/SelectPrinterActivity$3;
.super Ljava/lang/Object;
.source "SelectPrinterActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$3;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$3;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity;->access$000(Lcom/android/printspooler/ui/SelectPrinterActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/SelectPrinterActivity$DestinationAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 169
    const/4 v0, 0x1

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 163
    const/4 v0, 0x1

    return v0
.end method
