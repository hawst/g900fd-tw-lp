.class Lcom/android/printspooler/ui/PrintPreviewController;
.super Ljava/lang/Object;
.source "PrintPreviewController.java"

# interfaces
.implements Lcom/android/printspooler/model/MutexFileProvider$OnReleaseRequestCallback;
.implements Lcom/android/printspooler/ui/PageAdapter$PreviewArea;
.implements Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;,
        Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;
    }
.end annotation


# instance fields
.field private final mActivity:Lcom/android/printspooler/ui/PrintActivity;

.field private final mContentView:Lcom/android/printspooler/widget/PrintContentView;

.field private mDocumentPageCount:I

.field private final mEmbeddedContentContainer:Lcom/android/printspooler/widget/EmbeddedContentContainer;

.field private final mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

.field private final mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

.field private final mLayoutManger:Landroid/support/v7/widget/GridLayoutManager;

.field private final mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

.field private final mPreloadController:Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;

.field private final mPrintOptionsLayout:Lcom/android/printspooler/widget/PrintOptionsLayout;

.field private final mRecyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/model/MutexFileProvider;)V
    .locals 3
    .param p1, "activity"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p2, "fileProvider"    # Lcom/android/printspooler/model/MutexFileProvider;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mActivity:Lcom/android/printspooler/ui/PrintActivity;

    .line 66
    new-instance v1, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    invoke-virtual {p1}, Lcom/android/printspooler/ui/PrintActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;-><init>(Lcom/android/printspooler/ui/PrintPreviewController;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    .line 67
    iput-object p2, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    .line 69
    const v1, 0x7f0d0011

    invoke-virtual {p1, v1}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/widget/PrintOptionsLayout;

    iput-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPrintOptionsLayout:Lcom/android/printspooler/widget/PrintOptionsLayout;

    .line 70
    new-instance v1, Lcom/android/printspooler/ui/PageAdapter;

    invoke-direct {v1, p1, p1, p0}, Lcom/android/printspooler/ui/PageAdapter;-><init>(Landroid/content/Context;Lcom/android/printspooler/ui/PageAdapter$ContentCallbacks;Lcom/android/printspooler/ui/PageAdapter$PreviewArea;)V

    iput-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    .line 72
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mActivity:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 75
    .local v0, "columnCount":I
    new-instance v1, Landroid/support/v7/widget/GridLayoutManager;

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mActivity:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {v1, v2, v0}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mLayoutManger:Landroid/support/v7/widget/GridLayoutManager;

    .line 77
    const v1, 0x7f0d000d

    invoke-virtual {p1, v1}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 78
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mLayoutManger:Landroid/support/v7/widget/GridLayoutManager;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 79
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 80
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setItemViewCacheSize(I)V

    .line 81
    new-instance v1, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {v1, p0, v2}, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;-><init>(Lcom/android/printspooler/ui/PrintPreviewController;Landroid/support/v7/widget/RecyclerView;)V

    iput-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPreloadController:Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;

    .line 82
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPreloadController:Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 84
    const v1, 0x7f0d0005

    invoke-virtual {p1, v1}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/widget/PrintContentView;

    iput-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mContentView:Lcom/android/printspooler/widget/PrintContentView;

    .line 85
    const v1, 0x7f0d000c

    invoke-virtual {p1, v1}, Lcom/android/printspooler/ui/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/printspooler/widget/EmbeddedContentContainer;

    iput-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mEmbeddedContentContainer:Lcom/android/printspooler/widget/EmbeddedContentContainer;

    .line 87
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mEmbeddedContentContainer:Lcom/android/printspooler/widget/EmbeddedContentContainer;

    invoke-virtual {v1, p0}, Lcom/android/printspooler/widget/EmbeddedContentContainer;->setOnSizeChangeListener(Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;)V

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PageAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/model/MutexFileProvider;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mFileProvider:Lcom/android/printspooler/model/MutexFileProvider;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/printspooler/ui/PrintPreviewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController;

    .prologue
    .line 44
    iget v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mDocumentPageCount:I

    return v0
.end method

.method static synthetic access$402(Lcom/android/printspooler/ui/PrintPreviewController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mDocumentPageCount:I

    return p1
.end method

.method static synthetic access$500(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PrintActivity;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mActivity:Lcom/android/printspooler/ui/PrintActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/printspooler/ui/PrintPreviewController;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrintPreviewController;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPreloadController:Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;

    return-object v0
.end method


# virtual methods
.method public closeOptions()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mContentView:Lcom/android/printspooler/widget/PrintContentView;

    invoke-virtual {v0}, Lcom/android/printspooler/widget/PrintContentView;->closeOptions()V

    .line 101
    return-void
.end method

.method public destroy(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 201
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PageAdapter;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 203
    .local v0, "operation":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->enqueueOperation(Landroid/os/Message;)V

    .line 206
    .end local v0    # "operation":Landroid/os/Message;
    :cond_0
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 207
    .restart local v0    # "operation":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 208
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->enqueueOperation(Landroid/os/Message;)V

    .line 209
    return-void
.end method

.method public getFilePageCount()I
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PageAdapter;->getFilePageCount()I

    move-result v0

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mEmbeddedContentContainer:Lcom/android/printspooler/widget/EmbeddedContentContainer;

    invoke-virtual {v0}, Lcom/android/printspooler/widget/EmbeddedContentContainer;->getHeight()I

    move-result v0

    return v0
.end method

.method public getRequestedPages()[Landroid/print/PageRange;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PageAdapter;->getRequestedPages()[Landroid/print/PageRange;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedPages()[Landroid/print/PageRange;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PageAdapter;->getSelectedPages()[Landroid/print/PageRange;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mEmbeddedContentContainer:Lcom/android/printspooler/widget/EmbeddedContentContainer;

    invoke-virtual {v0}, Lcom/android/printspooler/widget/EmbeddedContentContainer;->getWidth()I

    move-result v0

    return v0
.end method

.method public isOptionsOpened()Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mContentView:Lcom/android/printspooler/widget/PrintContentView;

    invoke-virtual {v0}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsOpened()Z

    move-result v0

    return v0
.end method

.method public onColorModeChanged(I)V
    .locals 1
    .param p1, "colorMode"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v0, p1}, Lcom/android/printspooler/ui/PageAdapter;->setColorMode(I)V

    .line 121
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PageAdapter;->notifyDataSetChanged()V

    .line 122
    return-void
.end method

.method public onContentUpdated(ZI[Landroid/print/PageRange;[Landroid/print/PageRange;Landroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V
    .locals 5
    .param p1, "documentChanged"    # Z
    .param p2, "documentPageCount"    # I
    .param p3, "writtenPages"    # [Landroid/print/PageRange;
    .param p4, "selectedPages"    # [Landroid/print/PageRange;
    .param p5, "mediaSize"    # Landroid/print/PrintAttributes$MediaSize;
    .param p6, "minMargins"    # Landroid/print/PrintAttributes$Margins;

    .prologue
    .line 139
    const/4 v1, 0x0

    .line 141
    .local v1, "contentChanged":Z
    if-eqz p1, :cond_0

    .line 142
    const/4 v1, 0x1

    .line 145
    :cond_0
    iget v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mDocumentPageCount:I

    if-eq p2, v3, :cond_1

    .line 146
    iput p2, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mDocumentPageCount:I

    .line 147
    const/4 v1, 0x1

    .line 150
    :cond_1
    if-eqz v1, :cond_2

    .line 152
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v3}, Lcom/android/printspooler/ui/PageAdapter;->isOpened()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 153
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 154
    .local v2, "operation":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->enqueueOperation(Landroid/os/Message;)V

    .line 160
    .end local v2    # "operation":Landroid/os/Message;
    :cond_2
    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v3}, Lcom/android/printspooler/ui/PageAdapter;->isOpened()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    if-eqz p3, :cond_4

    .line 161
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 162
    .restart local v2    # "operation":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->enqueueOperation(Landroid/os/Message;)V

    .line 166
    .end local v2    # "operation":Landroid/os/Message;
    :cond_4
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    move-result-object v0

    .line 167
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    iput-object p3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    .line 168
    iput-object p4, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    .line 169
    iput-object p5, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    .line 170
    iput-object p6, v0, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    .line 171
    iput p2, v0, Lcom/android/internal/os/SomeArgs;->argi1:I

    .line 173
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4, v0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 174
    .restart local v2    # "operation":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->enqueueOperation(Landroid/os/Message;)V

    .line 177
    if-eqz v1, :cond_5

    if-eqz p3, :cond_5

    .line 178
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 179
    iget-object v3, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->enqueueOperation(Landroid/os/Message;)V

    .line 181
    :cond_5
    return-void
.end method

.method public onOrientationChanged()V
    .locals 3

    .prologue
    .line 113
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mActivity:Lcom/android/printspooler/ui/PrintActivity;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 115
    .local v0, "optionColumnCount":I
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPrintOptionsLayout:Lcom/android/printspooler/widget/PrintOptionsLayout;

    invoke-virtual {v1, v0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->setColumnCount(I)V

    .line 116
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PageAdapter;->onOrientationChanged()V

    .line 117
    return-void
.end method

.method public onReleaseRequested(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mHandler:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    new-instance v1, Lcom/android/printspooler/ui/PrintPreviewController$1;

    invoke-direct {v1, p0}, Lcom/android/printspooler/ui/PrintPreviewController$1;-><init>(Lcom/android/printspooler/ui/PrintPreviewController;)V

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 198
    return-void
.end method

.method public onSizeChanged(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PageAdapter;->onPreviewAreaSizeChanged()V

    .line 93
    return-void
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1, "columnCount"    # I

    .prologue
    .line 223
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mLayoutManger:Landroid/support/v7/widget/GridLayoutManager;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/GridLayoutManager;->setSpanCount(I)V

    .line 224
    return-void
.end method

.method public setPadding(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 228
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 229
    return-void
.end method

.method public setUiShown(Z)V
    .locals 2
    .param p1, "shown"    # Z

    .prologue
    .line 104
    if-eqz p1, :cond_0

    .line 105
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0
.end method
