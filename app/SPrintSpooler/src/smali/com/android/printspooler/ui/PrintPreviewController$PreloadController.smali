.class final Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "PrintPreviewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintPreviewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PreloadController"
.end annotation


# instance fields
.field private mOldScrollState:I

.field private final mRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field final synthetic this$0:Lcom/android/printspooler/ui/PrintPreviewController;


# direct methods
.method public constructor <init>(Lcom/android/printspooler/ui/PrintPreviewController;Landroid/support/v7/widget/RecyclerView;)V
    .locals 1
    .param p2, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 336
    iput-object p2, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 337
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getScrollState()I

    move-result v0

    iput v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mOldScrollState:I

    .line 338
    return-void
.end method

.method private computeShownPages()Landroid/print/PageRange;
    .locals 9

    .prologue
    .line 378
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    .line 379
    .local v0, "childCount":I
    if-lez v0, :cond_0

    .line 380
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v5

    .line 382
    .local v5, "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 383
    .local v1, "firstChild":Landroid/view/View;
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v1}, Landroid/support/v7/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v2

    .line 385
    .local v2, "firstHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 386
    .local v3, "lastChild":Landroid/view/View;
    iget-object v6, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v3}, Landroid/support/v7/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v4

    .line 388
    .local v4, "lastHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    new-instance v6, Landroid/print/PageRange;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getPosition()I

    move-result v7

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getPosition()I

    move-result v8

    invoke-direct {v6, v7, v8}, Landroid/print/PageRange;-><init>(II)V

    .line 390
    .end local v1    # "firstChild":Landroid/view/View;
    .end local v2    # "firstHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .end local v3    # "lastChild":Landroid/view/View;
    .end local v4    # "lastHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .end local v5    # "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    :goto_0
    return-object v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "state"    # I

    .prologue
    .line 342
    iget v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mOldScrollState:I

    packed-switch v0, :pswitch_data_0

    .line 357
    :cond_0
    :goto_0
    iput p2, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mOldScrollState:I

    .line 358
    return-void

    .line 344
    :pswitch_0
    if-eqz p2, :cond_1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 346
    :cond_1
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->startPreloadContent()V

    goto :goto_0

    .line 352
    :pswitch_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 353
    invoke-virtual {p0}, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->stopPreloadContent()V

    goto :goto_0

    .line 342
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public startPreloadContent()V
    .locals 3

    .prologue
    .line 361
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/ui/PageAdapter;

    .line 362
    .local v0, "pageAdapter":Lcom/android/printspooler/ui/PageAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PageAdapter;->isOpened()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 363
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->computeShownPages()Landroid/print/PageRange;

    move-result-object v1

    .line 364
    .local v1, "shownPages":Landroid/print/PageRange;
    if-eqz v1, :cond_0

    .line 365
    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PageAdapter;->startPreloadContent(Landroid/print/PageRange;)V

    .line 368
    .end local v1    # "shownPages":Landroid/print/PageRange;
    :cond_0
    return-void
.end method

.method public stopPreloadContent()V
    .locals 2

    .prologue
    .line 371
    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController$PreloadController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/android/printspooler/ui/PageAdapter;

    .line 372
    .local v0, "pageAdapter":Lcom/android/printspooler/ui/PageAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PageAdapter;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373
    invoke-virtual {v0}, Lcom/android/printspooler/ui/PageAdapter;->stopPreloadContent()V

    .line 375
    :cond_0
    return-void
.end method
