.class final Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/ui/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PrinterAvailabilityDetector"
.end annotation


# instance fields
.field private mPosted:Z

.field private mPrinter:Landroid/print/PrinterInfo;

.field private mPrinterUnavailable:Z

.field final synthetic this$0:Lcom/android/printspooler/ui/PrintActivity;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/ui/PrintActivity;)V
    .locals 0

    .prologue
    .line 1694
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/ui/PrintActivity;Lcom/android/printspooler/ui/PrintActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/ui/PrintActivity;
    .param p2, "x1"    # Lcom/android/printspooler/ui/PrintActivity$1;

    .prologue
    .line 1694
    invoke-direct {p0, p1}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;-><init>(Lcom/android/printspooler/ui/PrintActivity;)V

    return-void
.end method

.method private postIfNeeded()V
    .locals 4

    .prologue
    .line 1745
    iget-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPosted:Z

    if-nez v0, :cond_0

    .line 1746
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPosted:Z

    .line 1747
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$2300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v0

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, p0, v2, v3}, Landroid/widget/Spinner;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1749
    :cond_0
    return-void
.end method

.method private unpostIfNeeded()V
    .locals 1

    .prologue
    .line 1752
    iget-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPosted:Z

    if-eqz v0, :cond_0

    .line 1753
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPosted:Z

    .line 1754
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinner:Landroid/widget/Spinner;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintActivity;->access$2300(Lcom/android/printspooler/ui/PrintActivity;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1756
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 1740
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->unpostIfNeeded()V

    .line 1741
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinterUnavailable:Z

    .line 1742
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 1760
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPosted:Z

    .line 1761
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinterUnavailable:Z

    .line 1762
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v0, v1}, Lcom/android/printspooler/ui/PrintActivity;->onPrinterUnavailable(Landroid/print/PrinterInfo;)V

    .line 1763
    return-void
.end method

.method public updatePrinter(Landroid/print/PrinterInfo;)V
    .locals 7
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x3

    const/4 v3, 0x0

    .line 1704
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    # getter for: Lcom/android/printspooler/ui/PrintActivity;->mDestinationSpinnerAdapter:Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;
    invoke-static {v4}, Lcom/android/printspooler/ui/PrintActivity;->access$2200(Lcom/android/printspooler/ui/PrintActivity;)Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/printspooler/ui/PrintActivity$DestinationAdapter;->getPdfPrinter()Landroid/print/PrinterInfo;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/print/PrinterInfo;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1737
    :cond_0
    :goto_0
    return-void

    .line 1708
    :cond_1
    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v4

    if-eq v4, v6, :cond_3

    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v4

    if-eqz v4, :cond_3

    move v0, v2

    .line 1712
    .local v0, "available":Z
    :goto_1
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinter:Landroid/print/PrinterInfo;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/print/PrinterId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1713
    :cond_2
    const/4 v1, 0x1

    .line 1714
    .local v1, "notifyIfAvailable":Z
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->unpostIfNeeded()V

    .line 1715
    iput-boolean v3, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinterUnavailable:Z

    .line 1716
    new-instance v2, Landroid/print/PrinterInfo$Builder;

    invoke-direct {v2, p1}, Landroid/print/PrinterInfo$Builder;-><init>(Landroid/print/PrinterInfo;)V

    invoke-virtual {v2}, Landroid/print/PrinterInfo$Builder;->build()Landroid/print/PrinterInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinter:Landroid/print/PrinterInfo;

    .line 1726
    :goto_2
    if-eqz v0, :cond_8

    .line 1727
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->unpostIfNeeded()V

    .line 1728
    iput-boolean v3, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinterUnavailable:Z

    .line 1729
    if-eqz v1, :cond_0

    .line 1730
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->this$0:Lcom/android/printspooler/ui/PrintActivity;

    iget-object v3, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v2, v3}, Lcom/android/printspooler/ui/PrintActivity;->onPrinterAvailable(Landroid/print/PrinterInfo;)V

    goto :goto_0

    .end local v0    # "available":Z
    .end local v1    # "notifyIfAvailable":Z
    :cond_3
    move v0, v3

    .line 1708
    goto :goto_1

    .line 1718
    .restart local v0    # "available":Z
    :cond_4
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v4

    if-ne v4, v6, :cond_5

    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getStatus()I

    move-result v4

    if-ne v4, v6, :cond_6

    :cond_5
    iget-object v4, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v4

    if-nez v4, :cond_7

    invoke-virtual {p1}, Landroid/print/PrinterInfo;->getCapabilities()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v4

    if-eqz v4, :cond_7

    :cond_6
    move v1, v2

    .line 1723
    .restart local v1    # "notifyIfAvailable":Z
    :goto_3
    iget-object v2, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinter:Landroid/print/PrinterInfo;

    invoke-virtual {v2, p1}, Landroid/print/PrinterInfo;->copyFrom(Landroid/print/PrinterInfo;)V

    goto :goto_2

    .end local v1    # "notifyIfAvailable":Z
    :cond_7
    move v1, v3

    .line 1718
    goto :goto_3

    .line 1733
    .restart local v1    # "notifyIfAvailable":Z
    :cond_8
    iget-boolean v2, p0, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->mPrinterUnavailable:Z

    if-nez v2, :cond_0

    .line 1734
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrintActivity$PrinterAvailabilityDetector;->postIfNeeded()V

    goto :goto_0
.end method
