.class Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;
.super Ljava/lang/Object;
.source "SelectPrinterActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;

.field final synthetic val$adapter:Landroid/widget/ArrayAdapter;

.field final synthetic val$printServices:Ljava/util/List;

.field final synthetic val$viewIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;Landroid/widget/ArrayAdapter;Landroid/content/Intent;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;

    iput-object p2, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->val$adapter:Landroid/widget/ArrayAdapter;

    iput-object p3, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->val$viewIntent:Landroid/content/Intent;

    iput-object p4, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->val$printServices:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 410
    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->val$adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 411
    .local v3, "item":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;

    # getter for: Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->mAddPrintServiceItem:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->access$300(Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 413
    :try_start_0
    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;

    iget-object v6, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->val$viewIntent:Landroid/content/Intent;

    invoke-virtual {v5, v6}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :goto_0
    return-void

    .line 414
    :catch_0
    move-exception v0

    .line 415
    .local v0, "anfe":Landroid/content/ActivityNotFoundException;
    const-string v5, "SelectPrinterFragment"

    const-string v6, "Couldn\'t start add printer activity"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 418
    .end local v0    # "anfe":Landroid/content/ActivityNotFoundException;
    :cond_0
    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->val$printServices:Ljava/util/List;

    invoke-interface {v5, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/printservice/PrintServiceInfo;

    .line 419
    .local v4, "printService":Landroid/printservice/PrintServiceInfo;
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/printservice/PrintServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4}, Landroid/printservice/PrintServiceInfo;->getAddPrintersActivityName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    .local v1, "componentName":Landroid/content/ComponentName;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 423
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 425
    :try_start_1
    iget-object v5, p0, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment$1;->this$0:Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;

    invoke-virtual {v5, v2}, Lcom/android/printspooler/ui/SelectPrinterActivity$AddPrinterAlertDialogFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 426
    :catch_1
    move-exception v0

    .line 427
    .restart local v0    # "anfe":Landroid/content/ActivityNotFoundException;
    const-string v5, "SelectPrinterFragment"

    const-string v6, "Couldn\'t start add printer activity"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
