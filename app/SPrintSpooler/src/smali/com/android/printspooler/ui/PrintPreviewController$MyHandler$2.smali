.class Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;
.super Ljava/lang/Object;
.source "PrintPreviewController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;


# direct methods
.method constructor <init>(Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;->this$1:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;->this$1:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mDocumentPageCount:I
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$400(Lcom/android/printspooler/ui/PrintPreviewController;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 280
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;->this$1:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    iget-object v1, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;->this$1:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    iget-object v1, v1, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mPageAdapter:Lcom/android/printspooler/ui/PageAdapter;
    invoke-static {v1}, Lcom/android/printspooler/ui/PrintPreviewController;->access$000(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/printspooler/ui/PageAdapter;->getFilePageCount()I

    move-result v1

    # setter for: Lcom/android/printspooler/ui/PrintPreviewController;->mDocumentPageCount:I
    invoke-static {v0, v1}, Lcom/android/printspooler/ui/PrintPreviewController;->access$402(Lcom/android/printspooler/ui/PrintPreviewController;I)I

    .line 281
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;->this$1:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    iget-object v0, v0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->this$0:Lcom/android/printspooler/ui/PrintPreviewController;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController;->mActivity:Lcom/android/printspooler/ui/PrintActivity;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController;->access$500(Lcom/android/printspooler/ui/PrintPreviewController;)Lcom/android/printspooler/ui/PrintActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/printspooler/ui/PrintActivity;->updateOptionsUi()V

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler$2;->this$1:Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;

    # getter for: Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->mOnAsyncOperationDoneCallback:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;->access$600(Lcom/android/printspooler/ui/PrintPreviewController$MyHandler;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 284
    return-void
.end method
