.class public Lcom/android/printspooler/ui/PrinterRegistry;
.super Ljava/lang/Object;
.source "PrinterRegistry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/ui/PrinterRegistry$MyHandler;,
        Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    }
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mHandler:Landroid/os/Handler;

.field private final mLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOnPrintersChangeListener:Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

.field private final mPrinters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mReady:Z

.field private final mReadyCallback:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "readyCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mPrinters:Ljava/util/List;

    .line 112
    new-instance v0, Lcom/android/printspooler/ui/PrinterRegistry$1;

    invoke-direct {v0, p0}, Lcom/android/printspooler/ui/PrinterRegistry$1;-><init>(Lcom/android/printspooler/ui/PrinterRegistry;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;

    .line 55
    iput-object p1, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mActivity:Landroid/app/Activity;

    .line 56
    iput-object p2, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mReadyCallback:Ljava/lang/Runnable;

    .line 57
    new-instance v0, Lcom/android/printspooler/ui/PrinterRegistry$MyHandler;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/printspooler/ui/PrinterRegistry$MyHandler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mHandler:Landroid/os/Handler;

    .line 58
    invoke-virtual {p1}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/android/printspooler/ui/PrinterRegistry;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrinterRegistry;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mPrinters:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/printspooler/ui/PrinterRegistry;)Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrinterRegistry;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mOnPrintersChangeListener:Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/printspooler/ui/PrinterRegistry;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrinterRegistry;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/printspooler/ui/PrinterRegistry;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrinterRegistry;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mReady:Z

    return v0
.end method

.method static synthetic access$302(Lcom/android/printspooler/ui/PrinterRegistry;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrinterRegistry;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mReady:Z

    return p1
.end method

.method static synthetic access$400(Lcom/android/printspooler/ui/PrinterRegistry;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrinterRegistry;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mReadyCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/printspooler/ui/PrinterRegistry;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/ui/PrinterRegistry;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private getPrinterProvider()Lcom/android/printspooler/ui/FusedPrintersProvider;
    .locals 3

    .prologue
    .line 108
    iget-object v1, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v0

    .line 109
    .local v0, "loader":Landroid/content/Loader;, "Landroid/content/Loader<*>;"
    check-cast v0, Lcom/android/printspooler/ui/FusedPrintersProvider;

    .end local v0    # "loader":Landroid/content/Loader;, "Landroid/content/Loader<*>;"
    return-object v0
.end method


# virtual methods
.method public addHistoricalPrinter(Landroid/print/PrinterInfo;)V
    .locals 2
    .param p1, "printer"    # Landroid/print/PrinterInfo;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinterProvider()Lcom/android/printspooler/ui/FusedPrintersProvider;

    move-result-object v0

    .line 72
    .local v0, "provider":Lcom/android/printspooler/ui/FusedPrintersProvider;
    if-eqz v0, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinterProvider()Lcom/android/printspooler/ui/FusedPrintersProvider;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider;->addHistoricalPrinter(Landroid/print/PrinterInfo;)V

    .line 75
    :cond_0
    return-void
.end method

.method public areHistoricalPrintersLoaded()Z
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinterProvider()Lcom/android/printspooler/ui/FusedPrintersProvider;

    move-result-object v0

    .line 101
    .local v0, "provider":Lcom/android/printspooler/ui/FusedPrintersProvider;
    if-eqz v0, :cond_0

    .line 102
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinterProvider()Lcom/android/printspooler/ui/FusedPrintersProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/printspooler/ui/FusedPrintersProvider;->areHistoricalPrintersLoaded()Z

    move-result v1

    .line 104
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public forgetFavoritePrinter(Landroid/print/PrinterId;)V
    .locals 1
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinterProvider()Lcom/android/printspooler/ui/FusedPrintersProvider;

    move-result-object v0

    .line 79
    .local v0, "provider":Lcom/android/printspooler/ui/FusedPrintersProvider;
    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider;->forgetFavoritePrinter(Landroid/print/PrinterId;)V

    .line 82
    :cond_0
    return-void
.end method

.method public getPrinters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mPrinters:Ljava/util/List;

    return-object v0
.end method

.method public isFavoritePrinter(Landroid/print/PrinterId;)Z
    .locals 2
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinterProvider()Lcom/android/printspooler/ui/FusedPrintersProvider;

    move-result-object v0

    .line 86
    .local v0, "provider":Lcom/android/printspooler/ui/FusedPrintersProvider;
    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider;->isFavoritePrinter(Landroid/print/PrinterId;)Z

    move-result v1

    .line 89
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOnPrintersChangeListener(Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/android/printspooler/ui/PrinterRegistry;->mOnPrintersChangeListener:Lcom/android/printspooler/ui/PrinterRegistry$OnPrintersChangeListener;

    .line 64
    return-void
.end method

.method public setTrackedPrinter(Landroid/print/PrinterId;)V
    .locals 1
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/printspooler/ui/PrinterRegistry;->getPrinterProvider()Lcom/android/printspooler/ui/FusedPrintersProvider;

    move-result-object v0

    .line 94
    .local v0, "provider":Lcom/android/printspooler/ui/FusedPrintersProvider;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0, p1}, Lcom/android/printspooler/ui/FusedPrintersProvider;->setTrackedPrinter(Landroid/print/PrinterId;)V

    .line 97
    :cond_0
    return-void
.end method
