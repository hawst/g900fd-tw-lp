.class final Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;
.super Lcom/android/printspooler/renderer/IPdfEditor$Stub;
.source "PdfManipulationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/renderer/PdfManipulationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PdfEditorImpl"
.end annotation


# instance fields
.field private mEditor:Landroid/graphics/pdf/PdfEditor;

.field private final mLock:Ljava/lang/Object;

.field final synthetic this$0:Lcom/android/printspooler/renderer/PdfManipulationService;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/renderer/PdfManipulationService;)V
    .locals 1

    .prologue
    .line 209
    iput-object p1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->this$0:Lcom/android/printspooler/renderer/PdfManipulationService;

    invoke-direct {p0}, Lcom/android/printspooler/renderer/IPdfEditor$Stub;-><init>()V

    .line 210
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mLock:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/renderer/PdfManipulationService;Lcom/android/printspooler/renderer/PdfManipulationService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/renderer/PdfManipulationService;
    .param p2, "x1"    # Lcom/android/printspooler/renderer/PdfManipulationService$1;

    .prologue
    .line 209
    invoke-direct {p0, p1}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;-><init>(Lcom/android/printspooler/renderer/PdfManipulationService;)V

    return-void
.end method

.method private throwIfNotOpened()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mEditor:Landroid/graphics/pdf/PdfEditor;

    if-nez v0, :cond_0

    .line 289
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not opened"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_0
    return-void
.end method

.method private throwIfOpened()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mEditor:Landroid/graphics/pdf/PdfEditor;

    if-eqz v0, :cond_0

    .line 283
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already opened"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :cond_0
    return-void
.end method


# virtual methods
.method public closeDocument()V
    .locals 3

    .prologue
    .line 271
    iget-object v1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 272
    :try_start_0
    invoke-direct {p0}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->throwIfNotOpened()V

    .line 274
    const-string v0, "PdfManipulationService"

    const-string v2, "closeDocument()"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mEditor:Landroid/graphics/pdf/PdfEditor;

    invoke-virtual {v0}, Landroid/graphics/pdf/PdfEditor;->close()V

    .line 277
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mEditor:Landroid/graphics/pdf/PdfEditor;

    .line 278
    monitor-exit v1

    .line 279
    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public openDocument(Landroid/os/ParcelFileDescriptor;)I
    .locals 4
    .param p1, "source"    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 216
    iget-object v2, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 218
    :try_start_0
    invoke-direct {p0}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->throwIfOpened()V

    .line 220
    const-string v1, "PdfManipulationService"

    const-string v3, "openDocument()"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    new-instance v1, Landroid/graphics/pdf/PdfEditor;

    invoke-direct {v1, p1}, Landroid/graphics/pdf/PdfEditor;-><init>(Landroid/os/ParcelFileDescriptor;)V

    iput-object v1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mEditor:Landroid/graphics/pdf/PdfEditor;

    .line 223
    iget-object v1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mEditor:Landroid/graphics/pdf/PdfEditor;

    invoke-virtual {v1}, Landroid/graphics/pdf/PdfEditor;->getPageCount()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :try_start_1
    monitor-exit v2

    return v1

    .line 224
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 225
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 226
    const-string v1, "PdfManipulationService"

    const-string v3, "Cannot open file"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 227
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 229
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 224
    :catch_1
    move-exception v1

    move-object v0, v1

    goto :goto_0
.end method

.method public removePages([Landroid/print/PageRange;)V
    .locals 7
    .param p1, "ranges"    # [Landroid/print/PageRange;

    .prologue
    .line 234
    iget-object v5, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 235
    :try_start_0
    invoke-direct {p0}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->throwIfNotOpened()V

    .line 237
    const-string v4, "PdfManipulationService"

    const-string v6, "removePages()"

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-static {p1}, Lcom/android/printspooler/util/PageRangeUtils;->normalize([Landroid/print/PageRange;)[Landroid/print/PageRange;

    move-result-object p1

    .line 242
    array-length v3, p1

    .line 243
    .local v3, "rangeCount":I
    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 244
    aget-object v2, p1, v0

    .line 245
    .local v2, "range":Landroid/print/PageRange;
    invoke-virtual {v2}, Landroid/print/PageRange;->getEnd()I

    move-result v1

    .local v1, "j":I
    :goto_1
    invoke-virtual {v2}, Landroid/print/PageRange;->getStart()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 246
    iget-object v4, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mEditor:Landroid/graphics/pdf/PdfEditor;

    invoke-virtual {v4, v1}, Landroid/graphics/pdf/PdfEditor;->removePage(I)V

    .line 245
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 243
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 249
    .end local v1    # "j":I
    .end local v2    # "range":Landroid/print/PageRange;
    :cond_1
    monitor-exit v5

    .line 250
    return-void

    .line 249
    .end local v0    # "i":I
    .end local v3    # "rangeCount":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public write(Landroid/os/ParcelFileDescriptor;)V
    .locals 4
    .param p1, "destination"    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 254
    iget-object v2, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 256
    :try_start_0
    invoke-direct {p0}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->throwIfNotOpened()V

    .line 258
    const-string v1, "PdfManipulationService"

    const-string v3, "write()"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    iget-object v1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;->mEditor:Landroid/graphics/pdf/PdfEditor;

    invoke-virtual {v1, p1}, Landroid/graphics/pdf/PdfEditor;->write(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    :try_start_1
    monitor-exit v2

    .line 267
    return-void

    .line 261
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 262
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 263
    const-string v1, "PdfManipulationService"

    const-string v3, "Error writing PDF to file."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 264
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 266
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 261
    :catch_1
    move-exception v1

    move-object v0, v1

    goto :goto_0
.end method
