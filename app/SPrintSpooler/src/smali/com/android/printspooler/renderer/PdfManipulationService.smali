.class public final Lcom/android/printspooler/renderer/PdfManipulationService;
.super Landroid/app/Service;
.source "PdfManipulationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/renderer/PdfManipulationService$1;,
        Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;,
        Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 209
    return-void
.end method

.method static synthetic access$200(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 44
    invoke-static {p0}, Lcom/android/printspooler/renderer/PdfManipulationService;->pointsFromMils(I)I

    move-result v0

    return v0
.end method

.method private static pointsFromMils(I)I
    .locals 2
    .param p0, "mils"    # I

    .prologue
    .line 295
    int-to-float v0, p0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x42900000    # 72.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "action":Ljava/lang/String;
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 71
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid intent action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 63
    :sswitch_0
    const-string v2, "com.android.printspooler.renderer.ACTION_GET_RENDERER"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "com.android.printspooler.renderer.ACTION_GET_EDITOR"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 65
    :pswitch_0
    new-instance v1, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;

    invoke-direct {v1, p0, v3}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;-><init>(Lcom/android/printspooler/renderer/PdfManipulationService;Lcom/android/printspooler/renderer/PdfManipulationService$1;)V

    .line 68
    :goto_1
    return-object v1

    :pswitch_1
    new-instance v1, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;

    invoke-direct {v1, p0, v3}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfEditorImpl;-><init>(Lcom/android/printspooler/renderer/PdfManipulationService;Lcom/android/printspooler/renderer/PdfManipulationService$1;)V

    goto :goto_1

    .line 63
    nop

    :sswitch_data_0
    .sparse-switch
        -0x19b83679 -> :sswitch_1
        -0x98cb9a3 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
