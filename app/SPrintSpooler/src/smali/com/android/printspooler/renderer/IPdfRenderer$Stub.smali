.class public abstract Lcom/android/printspooler/renderer/IPdfRenderer$Stub;
.super Landroid/os/Binder;
.source "IPdfRenderer.java"

# interfaces
.implements Lcom/android/printspooler/renderer/IPdfRenderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/renderer/IPdfRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/renderer/IPdfRenderer$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.android.printspooler.renderer.IPdfRenderer"

    invoke-virtual {p0, p0, v0}, Lcom/android/printspooler/renderer/IPdfRenderer$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/printspooler/renderer/IPdfRenderer;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "com.android.printspooler.renderer.IPdfRenderer"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/android/printspooler/renderer/IPdfRenderer;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/android/printspooler/renderer/IPdfRenderer;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/android/printspooler/renderer/IPdfRenderer$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/printspooler/renderer/IPdfRenderer$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 96
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const-string v0, "com.android.printspooler.renderer.IPdfRenderer"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v7

    .line 46
    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.android.printspooler.renderer.IPdfRenderer"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/ParcelFileDescriptor;

    .line 58
    .local v1, "_arg0":Landroid/os/ParcelFileDescriptor;
    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/printspooler/renderer/IPdfRenderer$Stub;->openDocument(Landroid/os/ParcelFileDescriptor;)I

    move-result v6

    .line 59
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 60
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    move v0, v7

    .line 61
    goto :goto_0

    .line 56
    .end local v1    # "_arg0":Landroid/os/ParcelFileDescriptor;
    .end local v6    # "_result":I
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/os/ParcelFileDescriptor;
    goto :goto_1

    .line 65
    .end local v1    # "_arg0":Landroid/os/ParcelFileDescriptor;
    :sswitch_2
    const-string v0, "com.android.printspooler.renderer.IPdfRenderer"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 69
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 71
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 73
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    sget-object v0, Landroid/print/PrintAttributes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/print/PrintAttributes;

    .line 80
    .local v4, "_arg3":Landroid/print/PrintAttributes;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/ParcelFileDescriptor;

    .local v5, "_arg4":Landroid/os/ParcelFileDescriptor;
    :goto_3
    move-object v0, p0

    .line 86
    invoke-virtual/range {v0 .. v5}, Lcom/android/printspooler/renderer/IPdfRenderer$Stub;->renderPage(IIILandroid/print/PrintAttributes;Landroid/os/ParcelFileDescriptor;)V

    move v0, v7

    .line 87
    goto :goto_0

    .line 77
    .end local v4    # "_arg3":Landroid/print/PrintAttributes;
    .end local v5    # "_arg4":Landroid/os/ParcelFileDescriptor;
    :cond_1
    const/4 v4, 0x0

    .restart local v4    # "_arg3":Landroid/print/PrintAttributes;
    goto :goto_2

    .line 84
    :cond_2
    const/4 v5, 0x0

    .restart local v5    # "_arg4":Landroid/os/ParcelFileDescriptor;
    goto :goto_3

    .line 91
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Landroid/print/PrintAttributes;
    .end local v5    # "_arg4":Landroid/os/ParcelFileDescriptor;
    :sswitch_3
    const-string v0, "com.android.printspooler.renderer.IPdfRenderer"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/android/printspooler/renderer/IPdfRenderer$Stub;->closeDocument()V

    move v0, v7

    .line 93
    goto :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
