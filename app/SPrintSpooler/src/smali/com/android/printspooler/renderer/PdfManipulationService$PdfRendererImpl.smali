.class final Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;
.super Lcom/android/printspooler/renderer/IPdfRenderer$Stub;
.source "PdfManipulationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/renderer/PdfManipulationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PdfRendererImpl"
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mLock:Ljava/lang/Object;

.field private mRenderer:Landroid/graphics/pdf/PdfRenderer;

.field final synthetic this$0:Lcom/android/printspooler/renderer/PdfManipulationService;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/renderer/PdfManipulationService;)V
    .locals 1

    .prologue
    .line 76
    iput-object p1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->this$0:Lcom/android/printspooler/renderer/PdfManipulationService;

    invoke-direct {p0}, Lcom/android/printspooler/renderer/IPdfRenderer$Stub;-><init>()V

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mLock:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/renderer/PdfManipulationService;Lcom/android/printspooler/renderer/PdfManipulationService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/renderer/PdfManipulationService;
    .param p2, "x1"    # Lcom/android/printspooler/renderer/PdfManipulationService$1;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;-><init>(Lcom/android/printspooler/renderer/PdfManipulationService;)V

    return-void
.end method

.method private getBitmapForSize(II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v1, -0x1

    .line 184
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 186
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 187
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    .line 193
    :goto_0
    return-object v0

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 191
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    .line 192
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 193
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private throwIfNotOpened()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mRenderer:Landroid/graphics/pdf/PdfRenderer;

    if-nez v0, :cond_0

    .line 204
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not opened"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    return-void
.end method

.method private throwIfOpened()V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mRenderer:Landroid/graphics/pdf/PdfRenderer;

    if-eqz v0, :cond_0

    .line 198
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already opened"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_0
    return-void
.end method


# virtual methods
.method public closeDocument()V
    .locals 3

    .prologue
    .line 173
    iget-object v1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_0
    invoke-direct {p0}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->throwIfNotOpened()V

    .line 176
    const-string v0, "PdfManipulationService"

    const-string v2, "closeDocument()"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mRenderer:Landroid/graphics/pdf/PdfRenderer;

    invoke-virtual {v0}, Landroid/graphics/pdf/PdfRenderer;->close()V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mRenderer:Landroid/graphics/pdf/PdfRenderer;

    .line 180
    monitor-exit v1

    .line 181
    return-void

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public openDocument(Landroid/os/ParcelFileDescriptor;)I
    .locals 4
    .param p1, "source"    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v2, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 86
    :try_start_0
    invoke-direct {p0}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->throwIfOpened()V

    .line 88
    const-string v1, "PdfManipulationService"

    const-string v3, "openDocument()"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v1, Landroid/graphics/pdf/PdfRenderer;

    invoke-direct {v1, p1}, Landroid/graphics/pdf/PdfRenderer;-><init>(Landroid/os/ParcelFileDescriptor;)V

    iput-object v1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mRenderer:Landroid/graphics/pdf/PdfRenderer;

    .line 91
    iget-object v1, p0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mRenderer:Landroid/graphics/pdf/PdfRenderer;

    invoke-virtual {v1}, Landroid/graphics/pdf/PdfRenderer;->getPageCount()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :try_start_1
    monitor-exit v2

    .line 97
    :goto_0
    return v1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/io/IOException;
    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 94
    const-string v1, "PdfManipulationService"

    const-string v3, "Cannot open file"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 95
    const/4 v1, -0x2

    monitor-exit v2

    goto :goto_0

    .line 99
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 96
    :catch_1
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/IllegalStateException;
    const/4 v1, -0x3

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public renderPage(IIILandroid/print/PrintAttributes;Landroid/os/ParcelFileDescriptor;)V
    .locals 24
    .param p1, "pageIndex"    # I
    .param p2, "bitmapWidth"    # I
    .param p3, "bitmapHeight"    # I
    .param p4, "attributes"    # Landroid/print/PrintAttributes;
    .param p5, "destination"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mLock:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21

    .line 107
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->throwIfNotOpened()V

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mRenderer:Landroid/graphics/pdf/PdfRenderer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/pdf/PdfRenderer;->openPage(I)Landroid/graphics/pdf/PdfRenderer$Page;

    move-result-object v16

    .line 111
    .local v16, "page":Landroid/graphics/pdf/PdfRenderer$Page;
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/pdf/PdfRenderer$Page;->getWidth()I

    move-result v19

    .line 112
    .local v19, "srcWidthPts":I
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/pdf/PdfRenderer$Page;->getHeight()I

    move-result v18

    .line 114
    .local v18, "srcHeightPts":I
    invoke-virtual/range {p4 .. p4}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/print/PrintAttributes$MediaSize;->getWidthMils()I

    move-result v20

    # invokes: Lcom/android/printspooler/renderer/PdfManipulationService;->pointsFromMils(I)I
    invoke-static/range {v20 .. v20}, Lcom/android/printspooler/renderer/PdfManipulationService;->access$200(I)I

    move-result v9

    .line 116
    .local v9, "dstWidthPts":I
    invoke-virtual/range {p4 .. p4}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/print/PrintAttributes$MediaSize;->getHeightMils()I

    move-result v20

    # invokes: Lcom/android/printspooler/renderer/PdfManipulationService;->pointsFromMils(I)I
    invoke-static/range {v20 .. v20}, Lcom/android/printspooler/renderer/PdfManipulationService;->access$200(I)I

    move-result v8

    .line 119
    .local v8, "dstHeightPts":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->mRenderer:Landroid/graphics/pdf/PdfRenderer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/pdf/PdfRenderer;->shouldScaleForPrinting()Z

    move-result v17

    .line 120
    .local v17, "scaleContent":Z
    invoke-virtual/range {p4 .. p4}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/print/PrintAttributes$MediaSize;->isPortrait()Z

    move-result v20

    if-nez v20, :cond_1

    const/4 v6, 0x1

    .line 123
    .local v6, "contentLandscape":Z
    :goto_0
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 125
    .local v10, "matrix":Landroid/graphics/Matrix;
    if-eqz v17, :cond_2

    .line 126
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v20, v20, v22

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v20

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 135
    .local v7, "displayScale":F
    :goto_1
    invoke-virtual {v10, v7, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->this$0:Lcom/android/printspooler/renderer/PdfManipulationService;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/printspooler/renderer/PdfManipulationService;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    .line 139
    .local v5, "configuration":Landroid/content/res/Configuration;
    invoke-virtual {v5}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v20

    const/16 v22, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 140
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v7

    sub-float v20, v20, v22

    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 143
    :cond_0
    invoke-virtual/range {p4 .. p4}, Landroid/print/PrintAttributes;->getMinMargins()Landroid/print/PrintAttributes$Margins;

    move-result-object v11

    .line 144
    .local v11, "minMargins":Landroid/print/PrintAttributes$Margins;
    invoke-virtual {v11}, Landroid/print/PrintAttributes$Margins;->getLeftMils()I

    move-result v20

    # invokes: Lcom/android/printspooler/renderer/PdfManipulationService;->pointsFromMils(I)I
    invoke-static/range {v20 .. v20}, Lcom/android/printspooler/renderer/PdfManipulationService;->access$200(I)I

    move-result v13

    .line 145
    .local v13, "paddingLeftPts":I
    invoke-virtual {v11}, Landroid/print/PrintAttributes$Margins;->getTopMils()I

    move-result v20

    # invokes: Lcom/android/printspooler/renderer/PdfManipulationService;->pointsFromMils(I)I
    invoke-static/range {v20 .. v20}, Lcom/android/printspooler/renderer/PdfManipulationService;->access$200(I)I

    move-result v15

    .line 146
    .local v15, "paddingTopPts":I
    invoke-virtual {v11}, Landroid/print/PrintAttributes$Margins;->getRightMils()I

    move-result v20

    # invokes: Lcom/android/printspooler/renderer/PdfManipulationService;->pointsFromMils(I)I
    invoke-static/range {v20 .. v20}, Lcom/android/printspooler/renderer/PdfManipulationService;->access$200(I)I

    move-result v14

    .line 147
    .local v14, "paddingRightPts":I
    invoke-virtual {v11}, Landroid/print/PrintAttributes$Margins;->getBottomMils()I

    move-result v20

    # invokes: Lcom/android/printspooler/renderer/PdfManipulationService;->pointsFromMils(I)I
    invoke-static/range {v20 .. v20}, Lcom/android/printspooler/renderer/PdfManipulationService;->access$200(I)I

    move-result v12

    .line 149
    .local v12, "paddingBottomPts":I
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 150
    .local v4, "clip":Landroid/graphics/Rect;
    int-to-float v0, v13

    move/from16 v20, v0

    mul-float v20, v20, v7

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 151
    int-to-float v0, v15

    move/from16 v20, v0

    mul-float v20, v20, v7

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v4, Landroid/graphics/Rect;->top:I

    .line 152
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v14

    move/from16 v22, v0

    mul-float v22, v22, v7

    sub-float v20, v20, v22

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v4, Landroid/graphics/Rect;->right:I

    .line 153
    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v12

    move/from16 v22, v0

    mul-float v22, v22, v7

    sub-float v20, v20, v22

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 156
    const-string v20, "PdfManipulationService"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Rendering page:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/android/printspooler/renderer/PdfManipulationService$PdfRendererImpl;->getBitmapForSize(II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 160
    .local v3, "bitmap":Landroid/graphics/Bitmap;
    const/16 v20, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v3, v4, v10, v1}, Landroid/graphics/pdf/PdfRenderer$Page;->render(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Matrix;I)V

    .line 162
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/pdf/PdfRenderer$Page;->close()V

    .line 164
    move-object/from16 v0, p5

    invoke-static {v3, v0}, Lcom/android/printspooler/util/BitmapSerializeUtils;->writeBitmapPixels(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    :try_start_1
    invoke-static/range {p5 .. p5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 168
    monitor-exit v21

    .line 169
    return-void

    .line 120
    .end local v3    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "clip":Landroid/graphics/Rect;
    .end local v5    # "configuration":Landroid/content/res/Configuration;
    .end local v6    # "contentLandscape":Z
    .end local v7    # "displayScale":F
    .end local v10    # "matrix":Landroid/graphics/Matrix;
    .end local v11    # "minMargins":Landroid/print/PrintAttributes$Margins;
    .end local v12    # "paddingBottomPts":I
    .end local v13    # "paddingLeftPts":I
    .end local v14    # "paddingRightPts":I
    .end local v15    # "paddingTopPts":I
    :cond_1
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 129
    .restart local v6    # "contentLandscape":Z
    .restart local v10    # "matrix":Landroid/graphics/Matrix;
    :cond_2
    if-eqz v6, :cond_3

    .line 130
    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v8

    move/from16 v22, v0

    div-float v7, v20, v22

    .restart local v7    # "displayScale":F
    goto/16 :goto_1

    .line 132
    .end local v7    # "displayScale":F
    :cond_3
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v9

    move/from16 v22, v0

    div-float v7, v20, v22

    .restart local v7    # "displayScale":F
    goto/16 :goto_1

    .line 166
    .end local v6    # "contentLandscape":Z
    .end local v7    # "displayScale":F
    .end local v8    # "dstHeightPts":I
    .end local v9    # "dstWidthPts":I
    .end local v10    # "matrix":Landroid/graphics/Matrix;
    .end local v16    # "page":Landroid/graphics/pdf/PdfRenderer$Page;
    .end local v17    # "scaleContent":Z
    .end local v18    # "srcHeightPts":I
    .end local v19    # "srcWidthPts":I
    :catchall_0
    move-exception v20

    invoke-static/range {p5 .. p5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v20

    .line 168
    :catchall_1
    move-exception v20

    monitor-exit v21
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v20
.end method
