.class final Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;
.super Landroid/support/v4/widget/ViewDragHelper$Callback;
.source "PrintContentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/printspooler/widget/PrintContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DragCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/printspooler/widget/PrintContentView;


# direct methods
.method private constructor <init>(Lcom/android/printspooler/widget/PrintContentView;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    invoke-direct {p0}, Landroid/support/v4/widget/ViewDragHelper$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/printspooler/widget/PrintContentView;Lcom/android/printspooler/widget/PrintContentView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/printspooler/widget/PrintContentView;
    .param p2, "x1"    # Lcom/android/printspooler/widget/PrintContentView$1;

    .prologue
    .line 391
    invoke-direct {p0, p1}, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;-><init>(Lcom/android/printspooler/widget/PrintContentView;)V

    return-void
.end method


# virtual methods
.method public clampViewPositionVertical(Landroid/view/View;II)I
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "top"    # I
    .param p3, "dy"    # I

    .prologue
    .line 447
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$1100(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 448
    .local v0, "staticOptionBottom":I
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->getOpenedOptionsY()I
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$500(Lcom/android/printspooler/widget/PrintContentView;)I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->getClosedOptionsY()I
    invoke-static {v2}, Lcom/android/printspooler/widget/PrintContentView;->access$600(Lcom/android/printspooler/widget/PrintContentView;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method public getOrderedChildIndex(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 439
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    invoke-virtual {v0}, Lcom/android/printspooler/widget/PrintContentView;->getChildCount()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getViewVerticalDragRange(Landroid/view/View;)I
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 443
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;
    invoke-static {v0}, Lcom/android/printspooler/widget/PrintContentView;->access$800(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    return v0
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
    .locals 4
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "dx"    # I
    .param p5, "dy"    # I

    .prologue
    .line 403
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->isOptionsClosed()Z
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$200(Lcom/android/printspooler/widget/PrintContentView;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->isOptionsClosed()Z
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$200(Lcom/android/printspooler/widget/PrintContentView;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-gtz p5, :cond_1

    .line 416
    :goto_0
    return-void

    .line 407
    :cond_1
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # += operator for: Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I
    invoke-static {v1, p5}, Lcom/android/printspooler/widget/PrintContentView;->access$412(Lcom/android/printspooler/widget/PrintContentView;I)I

    .line 408
    int-to-float v1, p3

    iget-object v2, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->getOpenedOptionsY()I
    invoke-static {v2}, Lcom/android/printspooler/widget/PrintContentView;->access$500(Lcom/android/printspooler/widget/PrintContentView;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->getClosedOptionsY()I
    invoke-static {v2}, Lcom/android/printspooler/widget/PrintContentView;->access$600(Lcom/android/printspooler/widget/PrintContentView;)I

    move-result v2

    iget-object v3, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->getOpenedOptionsY()I
    invoke-static {v3}, Lcom/android/printspooler/widget/PrintContentView;->access$500(Lcom/android/printspooler/widget/PrintContentView;)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 411
    .local v0, "progress":F
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$700(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p5}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 413
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$800(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->notifySubtreeAccessibilityStateChangedIfNeeded()V

    .line 415
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->onDragProgress(F)V
    invoke-static {v1, v0}, Lcom/android/printspooler/widget/PrintContentView;->access$900(Lcom/android/printspooler/widget/PrintContentView;F)V

    goto :goto_0
.end method

.method public onViewReleased(Landroid/view/View;FF)V
    .locals 6
    .param p1, "child"    # Landroid/view/View;
    .param p2, "velocityX"    # F
    .param p3, "velocityY"    # F

    .prologue
    .line 419
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    .line 421
    .local v0, "childTop":I
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->getOpenedOptionsY()I
    invoke-static {v4}, Lcom/android/printspooler/widget/PrintContentView;->access$500(Lcom/android/printspooler/widget/PrintContentView;)I

    move-result v3

    .line 422
    .local v3, "openedOptionsY":I
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->getClosedOptionsY()I
    invoke-static {v4}, Lcom/android/printspooler/widget/PrintContentView;->access$600(Lcom/android/printspooler/widget/PrintContentView;)I

    move-result v1

    .line 424
    .local v1, "closedOptionsY":I
    if-eq v0, v3, :cond_0

    if-ne v0, v1, :cond_1

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    sub-int v4, v3, v1

    div-int/lit8 v4, v4, 0x2

    add-int v2, v1, v4

    .line 429
    .local v2, "halfRange":I
    if-ge v0, v2, :cond_2

    .line 430
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;
    invoke-static {v4}, Lcom/android/printspooler/widget/PrintContentView;->access$1000(Lcom/android/printspooler/widget/PrintContentView;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v4, p1, v5, v1}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    .line 435
    :goto_1
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    invoke-virtual {v4}, Lcom/android/printspooler/widget/PrintContentView;->invalidate()V

    goto :goto_0

    .line 432
    :cond_2
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;
    invoke-static {v4}, Lcom/android/printspooler/widget/PrintContentView;->access$1000(Lcom/android/printspooler/widget/PrintContentView;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v4, p1, v5, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    goto :goto_1
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "pointerId"    # I

    .prologue
    const/4 v0, 0x0

    .line 394
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    invoke-virtual {v1}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateController:Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$100(Lcom/android/printspooler/widget/PrintContentView;)Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;->canCloseOptions()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # invokes: Lcom/android/printspooler/widget/PrintContentView;->isOptionsClosed()Z
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$200(Lcom/android/printspooler/widget/PrintContentView;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateController:Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$100(Lcom/android/printspooler/widget/PrintContentView;)Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;->canOpenOptions()Z

    move-result v1

    if-nez v1, :cond_2

    .line 398
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;->this$0:Lcom/android/printspooler/widget/PrintContentView;

    # getter for: Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;
    invoke-static {v1}, Lcom/android/printspooler/widget/PrintContentView;->access$300(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;

    move-result-object v1

    if-ne p1, v1, :cond_1

    if-nez p2, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method
