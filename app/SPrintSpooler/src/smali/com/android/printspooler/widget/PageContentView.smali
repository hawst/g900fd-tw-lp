.class public Lcom/android/printspooler/widget/PageContentView;
.super Landroid/view/View;
.source "PageContentView.java"

# interfaces
.implements Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;


# instance fields
.field private mColorMode:I

.field private mContentRequested:Z

.field private mEmptyState:Landroid/graphics/drawable/Drawable;

.field private mMediaSize:Landroid/print/PrintAttributes$MediaSize;

.field private mMinMargins:Landroid/print/PrintAttributes$Margins;

.field private mProvider:Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/printspooler/widget/PageContentView;->mColorMode:I

    .line 56
    return-void
.end method

.method private requestPageContentIfNeeded()V
    .locals 6

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PageContentView;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/printspooler/widget/PageContentView;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/printspooler/widget/PageContentView;->mContentRequested:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/widget/PageContentView;->mProvider:Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    if-eqz v0, :cond_0

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/printspooler/widget/PageContentView;->mContentRequested:Z

    .line 121
    iget-object v0, p0, Lcom/android/printspooler/widget/PageContentView;->mProvider:Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    new-instance v1, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;

    invoke-virtual {p0}, Lcom/android/printspooler/widget/PageContentView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/printspooler/widget/PageContentView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/android/printspooler/widget/PageContentView;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    iget-object v5, p0, Lcom/android/printspooler/widget/PageContentView;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/printspooler/model/PageContentRepository$RenderSpec;-><init>(IILandroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V

    invoke-virtual {v0, v1, p0}, Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;->getPageContent(Lcom/android/printspooler/model/PageContentRepository$RenderSpec;Lcom/android/printspooler/model/PageContentRepository$OnPageContentAvailableCallback;)V

    .line 124
    :cond_0
    return-void
.end method


# virtual methods
.method public getPageContentProvider()Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/printspooler/widget/PageContentView;->mProvider:Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    return-object v0
.end method

.method public init(Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;Landroid/graphics/drawable/Drawable;Landroid/print/PrintAttributes$MediaSize;Landroid/print/PrintAttributes$Margins;)V
    .locals 7
    .param p1, "provider"    # Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;
    .param p2, "emptyState"    # Landroid/graphics/drawable/Drawable;
    .param p3, "mediaSize"    # Landroid/print/PrintAttributes$MediaSize;
    .param p4, "minMargins"    # Landroid/print/PrintAttributes$Margins;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 87
    iget-object v6, p0, Lcom/android/printspooler/widget/PageContentView;->mProvider:Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    if-nez v6, :cond_1

    if-eqz p1, :cond_0

    move v3, v4

    .line 89
    .local v3, "providerChanged":Z
    :goto_0
    iget-object v6, p0, Lcom/android/printspooler/widget/PageContentView;->mEmptyState:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_4

    if-eqz p2, :cond_3

    move v0, v4

    .line 91
    .local v0, "loadingDrawableChanged":Z
    :goto_1
    iget-object v6, p0, Lcom/android/printspooler/widget/PageContentView;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    if-nez v6, :cond_7

    if-eqz p3, :cond_6

    move v2, v4

    .line 93
    .local v2, "mediaSizeChanged":Z
    :goto_2
    iget-object v6, p0, Lcom/android/printspooler/widget/PageContentView;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    if-nez v6, :cond_a

    if-eqz p4, :cond_9

    move v1, v4

    .line 96
    .local v1, "marginsChanged":Z
    :goto_3
    if-nez v3, :cond_c

    if-nez v2, :cond_c

    if-nez v1, :cond_c

    if-nez v0, :cond_c

    .line 115
    :goto_4
    return-void

    .end local v0    # "loadingDrawableChanged":Z
    .end local v1    # "marginsChanged":Z
    .end local v2    # "mediaSizeChanged":Z
    .end local v3    # "providerChanged":Z
    :cond_0
    move v3, v5

    .line 87
    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/android/printspooler/widget/PageContentView;->mProvider:Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    invoke-virtual {v6, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_0

    .restart local v3    # "providerChanged":Z
    :cond_3
    move v0, v5

    .line 89
    goto :goto_1

    :cond_4
    iget-object v6, p0, Lcom/android/printspooler/widget/PageContentView;->mEmptyState:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    move v0, v4

    goto :goto_1

    :cond_5
    move v0, v5

    goto :goto_1

    .restart local v0    # "loadingDrawableChanged":Z
    :cond_6
    move v2, v5

    .line 91
    goto :goto_2

    :cond_7
    iget-object v6, p0, Lcom/android/printspooler/widget/PageContentView;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v6, p3}, Landroid/print/PrintAttributes$MediaSize;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    move v2, v4

    goto :goto_2

    :cond_8
    move v2, v5

    goto :goto_2

    .restart local v2    # "mediaSizeChanged":Z
    :cond_9
    move v1, v5

    .line 93
    goto :goto_3

    :cond_a
    iget-object v6, p0, Lcom/android/printspooler/widget/PageContentView;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    invoke-virtual {v6, p4}, Landroid/print/PrintAttributes$Margins;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_b

    move v1, v4

    goto :goto_3

    :cond_b
    move v1, v5

    goto :goto_3

    .line 101
    .restart local v1    # "marginsChanged":Z
    :cond_c
    iput-object p1, p0, Lcom/android/printspooler/widget/PageContentView;->mProvider:Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    .line 102
    iput-object p3, p0, Lcom/android/printspooler/widget/PageContentView;->mMediaSize:Landroid/print/PrintAttributes$MediaSize;

    .line 103
    iput-object p4, p0, Lcom/android/printspooler/widget/PageContentView;->mMinMargins:Landroid/print/PrintAttributes$Margins;

    .line 105
    iput-object p2, p0, Lcom/android/printspooler/widget/PageContentView;->mEmptyState:Landroid/graphics/drawable/Drawable;

    .line 106
    iput-boolean v5, p0, Lcom/android/printspooler/widget/PageContentView;->mContentRequested:Z

    .line 110
    iget-object v4, p0, Lcom/android/printspooler/widget/PageContentView;->mProvider:Lcom/android/printspooler/model/PageContentRepository$PageContentProvider;

    if-nez v4, :cond_d

    invoke-virtual {p0}, Lcom/android/printspooler/widget/PageContentView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, p0, Lcom/android/printspooler/widget/PageContentView;->mEmptyState:Landroid/graphics/drawable/Drawable;

    if-eq v4, v5, :cond_d

    .line 111
    iget-object v4, p0, Lcom/android/printspooler/widget/PageContentView;->mEmptyState:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v4}, Lcom/android/printspooler/widget/PageContentView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 114
    :cond_d
    invoke-direct {p0}, Lcom/android/printspooler/widget/PageContentView;->requestPageContentIfNeeded()V

    goto :goto_4
.end method

.method public onPageContentAvailable(Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 4
    .param p1, "content"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 70
    iget v2, p0, Lcom/android/printspooler/widget/PageContentView;->mColorMode:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 71
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 72
    .local v0, "cm":Landroid/graphics/ColorMatrix;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 73
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 74
    .local v1, "filter":Landroid/graphics/ColorMatrixColorFilter;
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/BitmapDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 78
    .end local v0    # "cm":Landroid/graphics/ColorMatrix;
    .end local v1    # "filter":Landroid/graphics/ColorMatrixColorFilter;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/printspooler/widget/PageContentView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 79
    return-void

    .line 76
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/printspooler/widget/PageContentView;->mContentRequested:Z

    .line 61
    invoke-direct {p0}, Lcom/android/printspooler/widget/PageContentView;->requestPageContentIfNeeded()V

    .line 62
    return-void
.end method

.method public setColorMode(I)V
    .locals 0
    .param p1, "mColorMode"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/android/printspooler/widget/PageContentView;->mColorMode:I

    .line 66
    return-void
.end method
