.class public final Lcom/android/printspooler/widget/CustomErrorEditText;
.super Landroid/widget/EditText;
.source "CustomErrorEditText.java"


# instance fields
.field private mError:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method public getError()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/printspooler/widget/CustomErrorEditText;->mError:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/CharSequence;
    .param p2, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-virtual {p0, v0, v0, p2, v0}, Lcom/android/printspooler/widget/CustomErrorEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 42
    iput-object p1, p0, Lcom/android/printspooler/widget/CustomErrorEditText;->mError:Ljava/lang/CharSequence;

    .line 43
    return-void
.end method
