.class public Lcom/android/printspooler/widget/EmbeddedContentContainer;
.super Landroid/widget/FrameLayout;
.source "EmbeddedContentContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;
    }
.end annotation


# instance fields
.field private mSizeChangeListener:Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "newWidth"    # I
    .param p2, "newHeight"    # I
    .param p3, "oldWidth"    # I
    .param p4, "oldHeight"    # I

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 41
    iget-object v0, p0, Lcom/android/printspooler/widget/EmbeddedContentContainer;->mSizeChangeListener:Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/android/printspooler/widget/EmbeddedContentContainer;->mSizeChangeListener:Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;->onSizeChanged(II)V

    .line 44
    :cond_0
    return-void
.end method

.method public setOnSizeChangeListener(Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/android/printspooler/widget/EmbeddedContentContainer;->mSizeChangeListener:Lcom/android/printspooler/widget/EmbeddedContentContainer$OnSizeChangeListener;

    .line 36
    return-void
.end method
