.class public final Lcom/android/printspooler/widget/PrintOptionsLayout;
.super Landroid/view/ViewGroup;
.source "PrintOptionsLayout.java"


# instance fields
.field private mColumnCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    sget-object v1, Lcom/android/printspooler/R$styleable;->PrintOptionsLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 41
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    .line 42
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 43
    return-void
.end method


# virtual methods
.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 171
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getChildCount()I

    move-result v5

    .line 128
    .local v5, "childCount":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    div-int v15, v5, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    move/from16 v16, v0

    rem-int v16, v5, v16

    add-int v13, v15, v16

    .line 130
    .local v13, "rowCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingStart()I

    move-result v1

    .line 131
    .local v1, "cellStart":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingTop()I

    move-result v2

    .line 133
    .local v2, "cellTop":I
    const/4 v12, 0x0

    .local v12, "row":I
    :goto_0
    if-ge v12, v13, :cond_3

    .line 134
    const/4 v14, 0x0

    .line 136
    .local v14, "rowHeight":I
    const/4 v11, 0x0

    .local v11, "col":I
    :goto_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    if-ge v11, v15, :cond_0

    .line 137
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    mul-int/2addr v15, v12

    add-int v6, v15, v11

    .line 139
    .local v6, "childIndex":I
    if-lt v6, v5, :cond_1

    .line 164
    .end local v6    # "childIndex":I
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingStart()I

    move-result v1

    .line 165
    add-int/2addr v2, v14

    .line 133
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 143
    .restart local v6    # "childIndex":I
    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 145
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 136
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 149
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 151
    .local v8, "childParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v8}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v15

    add-int v7, v1, v15

    .line 152
    .local v7, "childLeft":I
    iget v15, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int v10, v2, v15

    .line 153
    .local v10, "childTop":I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    add-int v9, v7, v15

    .line 154
    .local v9, "childRight":I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int v4, v10, v15

    .line 156
    .local v4, "childBottom":I
    invoke-virtual {v3, v7, v10, v9, v4}, Landroid/view/View;->layout(IIII)V

    .line 158
    invoke-virtual {v8}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result v15

    add-int v1, v9, v15

    .line 160
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    move-result v14

    goto :goto_2

    .line 167
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childBottom":I
    .end local v6    # "childIndex":I
    .end local v7    # "childLeft":I
    .end local v8    # "childParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v9    # "childRight":I
    .end local v10    # "childTop":I
    .end local v11    # "col":I
    .end local v14    # "rowHeight":I
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 22
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 54
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v18

    .line 55
    .local v18, "widthMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v19

    .line 57
    .local v19, "widthSize":I
    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mPaddingLeft:I

    move/from16 v20, v0

    sub-int v20, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mPaddingRight:I

    move/from16 v21, v0

    sub-int v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    move/from16 v21, v0

    div-int v11, v20, v21

    .line 60
    .local v11, "columnWidth":I
    :goto_0
    const/16 v17, 0x0

    .line 61
    .local v17, "width":I
    const/4 v12, 0x0

    .line 62
    .local v12, "height":I
    const/4 v8, 0x0

    .line 64
    .local v8, "childState":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getChildCount()I

    move-result v4

    .line 65
    .local v4, "childCount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    move/from16 v20, v0

    div-int v20, v4, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    move/from16 v21, v0

    rem-int v21, v4, v21

    add-int v14, v20, v21

    .line 67
    .local v14, "rowCount":I
    const/4 v13, 0x0

    .local v13, "row":I
    :goto_1
    if-ge v13, v14, :cond_5

    .line 68
    const/16 v16, 0x0

    .line 69
    .local v16, "rowWidth":I
    const/4 v15, 0x0

    .line 71
    .local v15, "rowHeight":I
    const/4 v10, 0x0

    .local v10, "col":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v10, v0, :cond_0

    .line 72
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    move/from16 v20, v0

    mul-int v20, v20, v13

    add-int v6, v20, v10

    .line 74
    .local v6, "childIndex":I
    if-lt v6, v4, :cond_2

    .line 110
    .end local v6    # "childIndex":I
    :cond_0
    move/from16 v0, v17

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 111
    add-int/2addr v12, v15

    .line 67
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 57
    .end local v4    # "childCount":I
    .end local v8    # "childState":I
    .end local v10    # "col":I
    .end local v11    # "columnWidth":I
    .end local v12    # "height":I
    .end local v13    # "row":I
    .end local v14    # "rowCount":I
    .end local v15    # "rowHeight":I
    .end local v16    # "rowWidth":I
    .end local v17    # "width":I
    :cond_1
    const/4 v11, 0x0

    goto :goto_0

    .line 78
    .restart local v4    # "childCount":I
    .restart local v6    # "childIndex":I
    .restart local v8    # "childState":I
    .restart local v10    # "col":I
    .restart local v11    # "columnWidth":I
    .restart local v12    # "height":I
    .restart local v13    # "row":I
    .restart local v14    # "rowCount":I
    .restart local v15    # "rowHeight":I
    .restart local v16    # "rowWidth":I
    .restart local v17    # "width":I
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 80
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v20

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 71
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 84
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 87
    .local v7, "childParams":Landroid/view/ViewGroup$MarginLayoutParams;
    if-lez v11, :cond_4

    .line 88
    invoke-virtual {v7}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v20

    sub-int v20, v11, v20

    invoke-virtual {v7}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result v21

    sub-int v20, v20, v21

    const/high16 v21, 0x40000000    # 2.0f

    invoke-static/range {v20 .. v21}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 96
    .local v9, "childWidthMeasureSpec":I
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingTop()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingBottom()I

    move-result v21

    add-int v20, v20, v21

    add-int v20, v20, v12

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v21, v0

    move/from16 v0, p2

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getChildMeasureSpec(III)I

    move-result v5

    .line 99
    .local v5, "childHeightMeasureSpec":I
    invoke-virtual {v3, v9, v5}, Landroid/view/View;->measure(II)V

    .line 101
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredState()I

    move-result v20

    move/from16 v0, v20

    invoke-static {v8, v0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->combineMeasuredStates(II)I

    move-result v8

    .line 103
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v20

    invoke-virtual {v7}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v21

    add-int v20, v20, v21

    invoke-virtual {v7}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result v21

    add-int v20, v20, v21

    add-int v16, v16, v20

    .line 106
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v20

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    goto :goto_3

    .line 92
    .end local v5    # "childHeightMeasureSpec":I
    .end local v9    # "childWidthMeasureSpec":I
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingStart()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingEnd()I

    move-result v21

    add-int v20, v20, v21

    add-int v20, v20, v17

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    move/from16 v21, v0

    move/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getChildMeasureSpec(III)I

    move-result v9

    .restart local v9    # "childWidthMeasureSpec":I
    goto :goto_4

    .line 114
    .end local v3    # "child":Landroid/view/View;
    .end local v6    # "childIndex":I
    .end local v7    # "childParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v9    # "childWidthMeasureSpec":I
    .end local v10    # "col":I
    .end local v15    # "rowHeight":I
    .end local v16    # "rowWidth":I
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingStart()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingEnd()I

    move-result v21

    add-int v20, v20, v21

    add-int v17, v17, v20

    .line 115
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getMinimumWidth()I

    move-result v20

    move/from16 v0, v17

    move/from16 v1, v20

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingTop()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getPaddingBottom()I

    move-result v21

    add-int v20, v20, v21

    add-int v12, v12, v20

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->getMinimumHeight()I

    move-result v20

    move/from16 v0, v20

    invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 120
    move/from16 v0, v17

    move/from16 v1, p1

    invoke-static {v0, v1, v8}, Lcom/android/printspooler/widget/PrintOptionsLayout;->resolveSizeAndState(III)I

    move-result v20

    shl-int/lit8 v21, v8, 0x10

    move/from16 v0, p2

    move/from16 v1, v21

    invoke-static {v12, v0, v1}, Lcom/android/printspooler/widget/PrintOptionsLayout;->resolveSizeAndState(III)I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/android/printspooler/widget/PrintOptionsLayout;->setMeasuredDimension(II)V

    .line 123
    return-void
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1, "columnCount"    # I

    .prologue
    .line 46
    iget v0, p0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    if-eq v0, p1, :cond_0

    .line 47
    iput p1, p0, Lcom/android/printspooler/widget/PrintOptionsLayout;->mColumnCount:I

    .line 48
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintOptionsLayout;->requestLayout()V

    .line 50
    :cond_0
    return-void
.end method
