.class public final Lcom/android/printspooler/widget/PrintContentView;
.super Landroid/view/ViewGroup;
.source "PrintContentView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/printspooler/widget/PrintContentView$1;,
        Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;,
        Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;,
        Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;
    }
.end annotation


# instance fields
.field private mClosedOptionsOffsetY:I

.field private mCurrentOptionsOffsetY:I

.field private mDragProgress:F

.field private mDraggableContent:Landroid/view/View;

.field private final mDragger:Landroid/support/v4/widget/ViewDragHelper;

.field private mDynamicContent:Landroid/view/View;

.field private mEmbeddedContentContainer:Landroid/view/View;

.field private mEmbeddedContentScrim:Landroid/view/View;

.field private mExpandCollapseHandle:Landroid/view/View;

.field private mExpandCollapseIcon:Landroid/view/View;

.field private mMoreOptionsButton:Landroid/view/View;

.field private mOldDraggableHeight:I

.field private mOptionsContainer:Landroid/view/ViewGroup;

.field private mOptionsStateChangeListener:Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;

.field private mOptionsStateController:Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

.field private mPrintButton:Landroid/view/View;

.field private final mScrimColor:I

.field private mStaticContent:Landroid/view/View;

.field private mSummaryContent:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    .line 88
    new-instance v0, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/printspooler/widget/PrintContentView$DragCallbacks;-><init>(Lcom/android/printspooler/widget/PrintContentView;Lcom/android/printspooler/widget/PrintContentView$1;)V

    invoke-static {p0, v0}, Landroid/support/v4/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;Landroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mScrimColor:I

    .line 94
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->setChildrenDrawingOrderEnabled(Z)V

    .line 95
    return-void
.end method

.method static synthetic access$100(Lcom/android/printspooler/widget/PrintContentView;)Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateController:Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/printspooler/widget/PrintContentView;)Landroid/support/v4/widget/ViewDragHelper;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/printspooler/widget/PrintContentView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsClosed()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$412(Lcom/android/printspooler/widget/PrintContentView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;
    .param p1, "x1"    # I

    .prologue
    .line 38
    iget v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    return v0
.end method

.method static synthetic access$500(Lcom/android/printspooler/widget/PrintContentView;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->getOpenedOptionsY()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/printspooler/widget/PrintContentView;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->getClosedOptionsY()I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/printspooler/widget/PrintContentView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/printspooler/widget/PrintContentView;F)V
    .locals 0
    .param p0, "x0"    # Lcom/android/printspooler/widget/PrintContentView;
    .param p1, "x1"    # F

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/android/printspooler/widget/PrintContentView;->onDragProgress(F)V

    return-void
.end method

.method private computeScrimColor()I
    .locals 5

    .prologue
    .line 208
    iget v2, p0, Lcom/android/printspooler/widget/PrintContentView;->mScrimColor:I

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    ushr-int/lit8 v1, v2, 0x18

    .line 209
    .local v1, "baseAlpha":I
    int-to-float v2, v1

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragProgress:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 210
    .local v0, "adjustedAlpha":I
    shl-int/lit8 v2, v0, 0x18

    iget v3, p0, Lcom/android/printspooler/widget/PrintContentView;->mScrimColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    return v2
.end method

.method private ensureImeClosedAndInputFocusCleared()V
    .locals 4

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 381
    .local v0, "focused":Landroid/view/View;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 382
    iget-object v2, p0, Lcom/android/printspooler/widget/PrintContentView;->mContext:Landroid/content/Context;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 384
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v1, v0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 387
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 389
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return-void
.end method

.method private getClosedOptionsY()I
    .locals 2

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->getOpenedOptionsY()I

    move-result v0

    iget v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mClosedOptionsOffsetY:I

    add-int/2addr v0, v1

    return v0
.end method

.method private getOpenedOptionsY()I
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    return v0
.end method

.method private isOptionsClosed()Z
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    iget v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mClosedOptionsOffsetY:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onDragProgress(F)V
    .locals 7
    .param p1, "progress"    # F

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 312
    iget v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragProgress:F

    invoke-static {v1, p1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_0

    .line 376
    :goto_0
    return-void

    .line 316
    :cond_0
    iget v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragProgress:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_1

    cmpl-float v1, p1, v3

    if-gtz v1, :cond_2

    :cond_1
    iget v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragProgress:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_3

    cmpg-float v1, p1, v6

    if-gez v1, :cond_3

    .line 318
    :cond_2
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v5}, Landroid/view/ViewGroup;->setLayerType(ILandroid/graphics/Paint;)V

    .line 319
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    invoke-virtual {v1, v2, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 320
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    invoke-virtual {v1, v2, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 321
    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->ensureImeClosedAndInputFocusCleared()V

    .line 323
    :cond_3
    iget v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragProgress:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_4

    cmpl-float v1, p1, v3

    if-eqz v1, :cond_5

    :cond_4
    iget v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragProgress:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_6

    cmpl-float v1, p1, v6

    if-nez v1, :cond_6

    .line 325
    :cond_5
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewGroup;->setLayerType(ILandroid/graphics/Paint;)V

    .line 326
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    invoke-virtual {v1, v4, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 327
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    invoke-virtual {v1, v4, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 328
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    invoke-virtual {v1, v4, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 331
    :cond_6
    iput p1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragProgress:F

    .line 333
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 335
    sub-float v0, v6, p1

    .line 336
    .local v0, "inverseAlpha":F
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 337
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 339
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentScrim:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->computeScrimColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 340
    cmpl-float v1, p1, v3

    if-nez v1, :cond_a

    .line 341
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateChangeListener:Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;

    if-eqz v1, :cond_7

    .line 342
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateChangeListener:Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;

    invoke-interface {v1}, Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;->onOptionsOpened()V

    .line 344
    :cond_7
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mExpandCollapseHandle:Landroid/view/View;

    iget-object v2, p0, Lcom/android/printspooler/widget/PrintContentView;->mContext:Landroid/content/Context;

    const v3, 0x7f09001e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 346
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mContext:Landroid/content/Context;

    const v2, 0x7f090021

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/printspooler/widget/PrintContentView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 347
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 348
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentScrim:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mExpandCollapseIcon:Landroid/view/View;

    const v2, 0x7f020001

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 354
    :goto_1
    cmpl-float v1, p1, v6

    if-nez v1, :cond_b

    .line 355
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateChangeListener:Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;

    if-eqz v1, :cond_8

    .line 356
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateChangeListener:Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;

    invoke-interface {v1}, Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;->onOptionsClosed()V

    .line 358
    :cond_8
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mExpandCollapseHandle:Landroid/view/View;

    iget-object v2, p0, Lcom/android/printspooler/widget/PrintContentView;->mContext:Landroid/content/Context;

    const v3, 0x7f09001d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 360
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mContext:Landroid/content/Context;

    const v2, 0x7f090022

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/printspooler/widget/PrintContentView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_9

    .line 362
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 364
    :cond_9
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 367
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentScrim:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentScrim:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 369
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mExpandCollapseIcon:Landroid/view/View;

    const v2, 0x7f020003

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 351
    :cond_a
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 371
    :cond_b
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_c

    .line 372
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 374
    :cond_c
    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public closeOptions()V
    .locals 4

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    iget-object v2, p0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->getClosedOptionsY()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    .line 128
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->invalidate()V

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->postInvalidateOnAnimation()V

    .line 205
    :cond_0
    return-void
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 165
    return-void
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 308
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 1
    .param p1, "childCount"    # I
    .param p2, "i"    # I

    .prologue
    .line 133
    sub-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public isOptionsOpened()Z
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mExpandCollapseHandle:Landroid/view/View;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_3

    .line 171
    :cond_0
    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateController:Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

    invoke-interface {v0}, Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;->canOpenOptions()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->openOptions()V

    .line 181
    :cond_1
    :goto_0
    return-void

    .line 173
    :cond_2
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsOpened()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateController:Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

    invoke-interface {v0}, Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;->canCloseOptions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->closeOptions()V

    goto :goto_0

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentScrim:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 177
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsOpened()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateController:Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

    invoke-interface {v0}, Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;->canCloseOptions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->closeOptions()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 138
    const v0, 0x7f0d0006

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    .line 139
    const v0, 0x7f0d0008

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    .line 140
    const v0, 0x7f0d000f

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    .line 141
    const v0, 0x7f0d0010

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    .line 142
    const v0, 0x7f0d000b

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    .line 143
    const v0, 0x7f0d0019

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mMoreOptionsButton:Landroid/view/View;

    .line 144
    const v0, 0x7f0d0011

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsContainer:Landroid/view/ViewGroup;

    .line 145
    const v0, 0x7f0d000c

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentContainer:Landroid/view/View;

    .line 146
    const v0, 0x7f0d000e

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentScrim:Landroid/view/View;

    .line 147
    const v0, 0x7f0d001a

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mExpandCollapseHandle:Landroid/view/View;

    .line 148
    const v0, 0x7f0d001b

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mExpandCollapseIcon:Landroid/view/View;

    .line 150
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mExpandCollapseHandle:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->onDragProgress(F)V

    .line 158
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PrintContentView;->setFocusableInTouchMode(Z)V

    .line 159
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 16
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 273
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v12, v0, v1, v2, v13}, Landroid/view/View;->layout(IIII)V

    .line 275
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v12}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_0

    .line 276
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v14, v15

    move/from16 v0, p2

    move/from16 v1, p4

    invoke-virtual {v12, v0, v13, v1, v14}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 280
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    add-int v4, v12, v13

    .line 281
    .local v4, "dynContentTop":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int v3, v4, v12

    .line 283
    .local v3, "dynContentBottom":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    move/from16 v0, p2

    move/from16 v1, p4

    invoke-virtual {v12, v0, v4, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 285
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 288
    .local v7, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/android/printspooler/widget/PrintContentView;->getLayoutDirection()I

    move-result v12

    if-nez v12, :cond_1

    .line 289
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    sub-int v12, p4, v12

    invoke-virtual {v7}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v13

    sub-int v9, v12, v13

    .line 293
    .local v9, "printButtonLeft":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int v11, v3, v12

    .line 294
    .local v11, "printButtonTop":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    add-int v10, v9, v12

    .line 295
    .local v10, "printButtonRight":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int v8, v11, v12

    .line 297
    .local v8, "printButtonBottom":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    invoke-virtual {v12, v9, v11, v10, v8}, Landroid/view/View;->layout(IIII)V

    .line 299
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/printspooler/widget/PrintContentView;->mClosedOptionsOffsetY:I

    add-int/2addr v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    add-int v6, v12, v13

    .line 301
    .local v6, "embContentTop":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentContainer:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int v5, v6, v12

    .line 303
    .local v5, "embContentBottom":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentContainer:Landroid/view/View;

    move/from16 v0, p2

    move/from16 v1, p4

    invoke-virtual {v12, v0, v6, v1, v5}, Landroid/view/View;->layout(IIII)V

    .line 304
    return-void

    .line 291
    .end local v5    # "embContentBottom":I
    .end local v6    # "embContentTop":I
    .end local v8    # "printButtonBottom":I
    .end local v9    # "printButtonLeft":I
    .end local v10    # "printButtonRight":I
    .end local v11    # "printButtonTop":I
    :cond_1
    invoke-virtual {v7}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v12

    add-int v9, p2, v12

    .restart local v9    # "printButtonLeft":I
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v5, 0x0

    .line 223
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsOpened()Z

    move-result v3

    .line 225
    .local v3, "wasOpened":Z
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    invoke-virtual {p0, v4, p1, p2}, Lcom/android/printspooler/widget/PrintContentView;->measureChild(Landroid/view/View;II)V

    .line 227
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v4

    const/16 v6, 0x8

    if-eq v4, v6, :cond_0

    .line 228
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {p0, v4, p1, p2}, Lcom/android/printspooler/widget/PrintContentView;->measureChild(Landroid/view/View;II)V

    .line 231
    :cond_0
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    invoke-virtual {p0, v4, p1, p2}, Lcom/android/printspooler/widget/PrintContentView;->measureChild(Landroid/view/View;II)V

    .line 233
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mPrintButton:Landroid/view/View;

    invoke-virtual {p0, v4, p1, p2}, Lcom/android/printspooler/widget/PrintContentView;->measureChild(Landroid/view/View;II)V

    .line 237
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v4

    iget-object v6, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v4, v6

    iput v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mClosedOptionsOffsetY:I

    .line 240
    iget v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    const/high16 v6, -0x80000000

    if-ne v4, v6, :cond_1

    .line 241
    iget v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mClosedOptionsOffsetY:I

    iput v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    .line 244
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 248
    .local v0, "heightSize":I
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 249
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mStaticContent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v0, v4

    iget-object v6, p0, Lcom/android/printspooler/widget/PrintContentView;->mSummaryContent:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v4, v6

    iget-object v6, p0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v4, v6

    iget-object v6, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v4, v6

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 256
    iget v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mOldDraggableHeight:I

    iget-object v6, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    if-eq v4, v6, :cond_3

    .line 257
    iget v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mOldDraggableHeight:I

    if-eqz v4, :cond_2

    .line 258
    if-eqz v3, :cond_4

    move v4, v5

    :goto_0
    iput v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mCurrentOptionsOffsetY:I

    .line 260
    :cond_2
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mDraggableContent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iput v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mOldDraggableHeight:I

    .line 264
    :cond_3
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 265
    .local v1, "hostHeightMeasureSpec":I
    iget-object v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mEmbeddedContentContainer:Landroid/view/View;

    invoke-virtual {p0, v4, p1, v1}, Lcom/android/printspooler/widget/PrintContentView;->measureChild(Landroid/view/View;II)V

    .line 267
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {v4, p1}, Lcom/android/printspooler/widget/PrintContentView;->resolveSize(II)I

    move-result v4

    invoke-static {v0, p2}, Lcom/android/printspooler/widget/PrintContentView;->resolveSize(II)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/android/printspooler/widget/PrintContentView;->setMeasuredDimension(II)V

    .line 269
    return-void

    .line 258
    .end local v1    # "hostHeightMeasureSpec":I
    :cond_4
    iget v4, p0, Lcom/android/printspooler/widget/PrintContentView;->mClosedOptionsOffsetY:I

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public openOptions()V
    .locals 4

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->isOptionsOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/android/printspooler/widget/PrintContentView;->mDragger:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    iget-object v2, p0, Lcom/android/printspooler/widget/PrintContentView;->mDynamicContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-direct {p0}, Lcom/android/printspooler/widget/PrintContentView;->getOpenedOptionsY()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    .line 119
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PrintContentView;->invalidate()V

    goto :goto_0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0
    .param p1, "disallowIntercept"    # Z

    .prologue
    .line 186
    return-void
.end method

.method public setOpenOptionsController(Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;)V
    .locals 0
    .param p1, "controller"    # Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateController:Lcom/android/printspooler/widget/PrintContentView$OptionsStateController;

    .line 103
    return-void
.end method

.method public setOptionsStateChangeListener(Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/printspooler/widget/PrintContentView;->mOptionsStateChangeListener:Lcom/android/printspooler/widget/PrintContentView$OptionsStateChangeListener;

    .line 99
    return-void
.end method
