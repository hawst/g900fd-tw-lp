.class public final Lcom/android/printspooler/widget/PreviewPageFrame;
.super Landroid/widget/LinearLayout;
.source "PreviewPageFrame.java"


# instance fields
.field private final mNotSelectedAlpha:F

.field private final mNotSelectedElevation:F

.field private final mSelectedElevation:F

.field private final mSelectedPageAlpha:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    iget-object v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mSelectedElevation:F

    .line 42
    iget-object v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mNotSelectedElevation:F

    .line 44
    iget-object v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mSelectedPageAlpha:F

    .line 46
    iget-object v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080001

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mNotSelectedAlpha:F

    .line 48
    return-void
.end method


# virtual methods
.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 53
    const-class v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PreviewPageFrame;->isSelected()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 55
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 60
    const-class v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSelected(Z)V

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 63
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PreviewPageFrame;->isSelected()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 64
    return-void
.end method

.method public setSelected(ZZ)V
    .locals 2
    .param p1, "selected"    # Z
    .param p2, "animate"    # Z

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PreviewPageFrame;->isSelected()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 88
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/printspooler/widget/PreviewPageFrame;->setSelected(Z)V

    .line 71
    if-eqz p1, :cond_2

    .line 72
    if-eqz p2, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PreviewPageFrame;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mSelectedElevation:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationZ(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mSelectedPageAlpha:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 76
    :cond_1
    iget v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mSelectedElevation:F

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PreviewPageFrame;->setTranslationZ(F)V

    .line 77
    iget v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mSelectedPageAlpha:F

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PreviewPageFrame;->setAlpha(F)V

    goto :goto_0

    .line 80
    :cond_2
    if-eqz p2, :cond_3

    .line 81
    invoke-virtual {p0}, Lcom/android/printspooler/widget/PreviewPageFrame;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mNotSelectedElevation:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationZ(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mNotSelectedAlpha:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 84
    :cond_3
    iget v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mNotSelectedElevation:F

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PreviewPageFrame;->setTranslationZ(F)V

    .line 85
    iget v0, p0, Lcom/android/printspooler/widget/PreviewPageFrame;->mNotSelectedAlpha:F

    invoke-virtual {p0, v0}, Lcom/android/printspooler/widget/PreviewPageFrame;->setAlpha(F)V

    goto :goto_0
.end method
