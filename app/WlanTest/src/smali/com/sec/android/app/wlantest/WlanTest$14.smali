.class Lcom/sec/android/app/wlantest/WlanTest$14;
.super Ljava/lang/Object;
.source "WlanTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wlantest/WlanTest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wlantest/WlanTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wlantest/WlanTest;)V
    .locals 0

    .prologue
    .line 890
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 894
    const-string v1, "WlanTest"

    const-string v2, "  ........ in btStartTx......"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v1, v1, Lcom/sec/android/app/wlantest/WlanTest;->btStartTx:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 896
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v1, v1, Lcom/sec/android/app/wlantest/WlanTest;->btStopTx:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 898
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v2, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v2, v2, Lcom/sec/android/app/wlantest/WlanTest;->burst:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/wlantest/WlanTest;->posBurstTx:Ljava/lang/String;

    .line 899
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v2, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v2, v2, Lcom/sec/android/app/wlantest/WlanTest;->payload:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/wlantest/WlanTest;->posPayloadTx:Ljava/lang/String;

    .line 901
    const/4 v0, 0x0

    .line 904
    .local v0, "resultTx":Z
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 941
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 942
    const-string v1, "WlanTest"

    const-string v2, "Tx Test failed!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    :cond_1
    return-void

    .line 905
    :cond_2
    const-string v1, "WlanTest"

    const-string v2, "1.Open DUT - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SB:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posBandTx:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 908
    const-string v1, "WlanTest"

    const-string v2, "2.Set Band - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CH:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posChannelTx:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 911
    const-string v1, "WlanTest"

    const-string v2, "3.Set Channel - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posAntennaTx:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 915
    const-string v1, "WlanTest"

    const-string v2, "10.Set Antenna - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->DR:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posTxRate:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 919
    const-string v1, "WlanTest"

    const-string v2, "4.Set Data Rate - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posPreambleTx:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 922
    const-string v1, "WlanTest"

    const-string v2, "5.Set Preamble or Short Guard Interval - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->BI:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posBurstTx:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 925
    const-string v1, "WlanTest"

    const-string v2, "6.Set Burst Interval - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 927
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->PL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posPayloadTx:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 928
    const-string v1, "WlanTest"

    const-string v2, "7.Set PayLoad - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->BW:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posBandwidthTx:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 931
    const-string v1, "WlanTest"

    const-string v2, "8.Set BandWidth - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posPower:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 934
    const-string v1, "WlanTest"

    const-string v2, "9.Set Tx Gain - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TS:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$14;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanTest;->posPacketTx:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 937
    const-string v1, "WlanTest"

    const-string v2, "11.Tx Start - OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    const/4 v0, 0x1

    goto/16 :goto_0
.end method
