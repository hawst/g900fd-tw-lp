.class public Lcom/sec/android/app/wlantest/WlanTest;
.super Landroid/app/Activity;
.source "WlanTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;,
        Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;,
        Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;,
        Lcom/sec/android/app/wlantest/WlanTest$DutId;,
        Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    }
.end annotation


# static fields
.field private static mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

.field private static mScrollView:Landroid/widget/ScrollView;

.field private static mTextView:Landroid/widget/TextView;

.field static myGUIUpdateHandler:Landroid/os/Handler;

.field private static sWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private StopByCloseDUT:Z

.field absRxTest:Landroid/widget/RelativeLayout;

.field absTxTest:Landroid/widget/RelativeLayout;

.field antennaRx:Landroid/widget/Spinner;

.field antennaTx:Landroid/widget/Spinner;

.field bandRx:Landroid/widget/Spinner;

.field bandTx:Landroid/widget/Spinner;

.field bandwidthTx:Landroid/widget/Spinner;

.field btClearRx:Landroid/widget/Button;

.field btStartRx:Landroid/widget/Button;

.field btStartTx:Landroid/widget/Button;

.field btStopRx:Landroid/widget/Button;

.field btStopTx:Landroid/widget/Button;

.field burst:Landroid/widget/EditText;

.field channelRx:Landroid/widget/Spinner;

.field channelTx:Landroid/widget/Spinner;

.field driverStatus:Ljava/lang/String;

.field error:Landroid/widget/EditText;

.field private isBTOff:Z

.field private isFinishOnPause:Z

.field linStatus:Landroid/widget/LinearLayout;

.field private mBufferedResultMessage:Ljava/lang/String;

.field private mCommandHandler:Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

.field private mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

.field private mDriverUnloadCmdId:I

.field private mDriverUnloadResult:Ljava/lang/String;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mIsScreenOn:Z

.field private mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mWLService:Landroid/os/IBinder;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field packetTx:Landroid/widget/Spinner;

.field payload:Landroid/widget/EditText;

.field per:Landroid/widget/EditText;

.field posAntennaRx:Ljava/lang/String;

.field posAntennaTx:Ljava/lang/String;

.field posBandRx:Ljava/lang/String;

.field posBandTx:Ljava/lang/String;

.field posBandwidthTx:Ljava/lang/String;

.field posBurstTx:Ljava/lang/String;

.field posChannelRx:Ljava/lang/String;

.field posChannelTx:Ljava/lang/String;

.field posPacketTx:Ljava/lang/String;

.field posPayloadTx:Ljava/lang/String;

.field posPower:Ljava/lang/String;

.field posPreambleTx:Ljava/lang/String;

.field posTxRate:Ljava/lang/String;

.field power:Landroid/widget/Spinner;

.field preamble:Landroid/widget/Spinner;

.field received:Landroid/widget/EditText;

.field txRate:Landroid/widget/Spinner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 198
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$1;

    invoke-direct {v0}, Lcom/sec/android/app/wlantest/WlanTest$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mIntentFilter:Landroid/content/IntentFilter;

    .line 166
    iput-boolean v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->StopByCloseDUT:Z

    .line 167
    iput-boolean v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->isFinishOnPause:Z

    .line 168
    iput-boolean v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->isBTOff:Z

    .line 178
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->posAntennaTx:Ljava/lang/String;

    .line 179
    iput-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->driverStatus:Ljava/lang/String;

    .line 184
    iput-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mIsScreenOn:Z

    .line 187
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    .line 195
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I

    .line 196
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;

    .line 264
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wlantest/WlanTest$2;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1573
    return-void
.end method

.method static synthetic access$000()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/app/wlantest/WlanTest;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/app/wlantest/WlanTest;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/wlantest/WlanTest;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/app/wlantest/WlanTest;->convertSubCmd(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanTest;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mCommandHandler:Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

    return-object v0
.end method

.method static synthetic access$400()Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/wlantest/WlanTest$FWMode;)Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    .prologue
    .line 60
    sput-object p0, Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    return-object p0
.end method

.method static synthetic access$600(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanTest;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/wlantest/WlanTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanTest;
    .param p1, "x1"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I

    return p1
.end method

.method static synthetic access$802(Lcom/sec/android/app/wlantest/WlanTest;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-static {p0, p1}, Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p0, "expectedResult"    # Ljava/lang/String;
    .param p1, "timeout"    # I

    .prologue
    .line 1369
    mul-int/lit8 v2, p1, 0xa

    .line 1370
    .local v2, "tryCount":I
    const-string v0, "NG"

    .local v0, "loadResult":Ljava/lang/String;
    move v3, v2

    .line 1371
    .end local v2    # "tryCount":I
    .local v3, "tryCount":I
    :goto_0
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "tryCount":I
    .restart local v2    # "tryCount":I
    if-lez v3, :cond_0

    .line 1372
    const-string v4, "wlan.driver.status"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1374
    .local v1, "prop":Ljava/lang/String;
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "wlan.driver.status : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1375
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1376
    const-string v0, "OK"

    .line 1386
    .end local v1    # "prop":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v0

    .line 1378
    .restart local v1    # "prop":Ljava/lang/String;
    :cond_1
    const-string v4, "failed"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "timeout"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    if-nez v2, :cond_3

    .line 1379
    :cond_2
    const-string v0, "NG"

    .line 1380
    goto :goto_1

    .line 1383
    :cond_3
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move v3, v2

    .line 1384
    .end local v2    # "tryCount":I
    .restart local v3    # "tryCount":I
    goto :goto_0

    .line 1383
    .end local v3    # "tryCount":I
    .restart local v2    # "tryCount":I
    :catch_0
    move-exception v4

    goto :goto_2
.end method

.method private convertSubCmd(Ljava/lang/String;)I
    .locals 2
    .param p1, "subCmd"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x10

    .line 217
    if-nez p1, :cond_0

    .line 218
    const/4 v1, -0x1

    .line 261
    :goto_0
    :sswitch_0
    return v1

    .line 220
    :cond_0
    invoke-static {p1, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 221
    .local v0, "cmd":I
    sparse-switch v0, :sswitch_data_0

    .line 261
    const/4 v1, -0x2

    goto :goto_0

    .line 223
    :sswitch_1
    const/4 v1, 0x0

    goto :goto_0

    .line 225
    :sswitch_2
    const/4 v1, 0x1

    goto :goto_0

    .line 227
    :sswitch_3
    const/4 v1, 0x2

    goto :goto_0

    .line 229
    :sswitch_4
    const/4 v1, 0x5

    goto :goto_0

    .line 231
    :sswitch_5
    const/4 v1, 0x6

    goto :goto_0

    .line 233
    :sswitch_6
    const/4 v1, 0x7

    goto :goto_0

    .line 235
    :sswitch_7
    const/16 v1, 0x8

    goto :goto_0

    .line 237
    :sswitch_8
    const/16 v1, 0x9

    goto :goto_0

    .line 239
    :sswitch_9
    const/16 v1, 0xa

    goto :goto_0

    .line 241
    :sswitch_a
    const/16 v1, 0xb

    goto :goto_0

    .line 243
    :sswitch_b
    const/16 v1, 0xc

    goto :goto_0

    .line 245
    :sswitch_c
    const/16 v1, 0xd

    goto :goto_0

    .line 247
    :sswitch_d
    const/16 v1, 0xe

    goto :goto_0

    .line 249
    :sswitch_e
    const/16 v1, 0xf

    goto :goto_0

    .line 253
    :sswitch_f
    const/16 v1, 0x11

    goto :goto_0

    .line 255
    :sswitch_10
    const/16 v1, 0x12

    goto :goto_0

    .line 257
    :sswitch_11
    const/16 v1, 0x13

    goto :goto_0

    .line 259
    :sswitch_12
    const/16 v1, 0x14

    goto :goto_0

    .line 221
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x3 -> :sswitch_6
        0x4 -> :sswitch_8
        0x5 -> :sswitch_7
        0x6 -> :sswitch_0
        0x7 -> :sswitch_f
        0x100 -> :sswitch_2
        0x101 -> :sswitch_1
        0x102 -> :sswitch_11
        0x103 -> :sswitch_10
        0x200 -> :sswitch_a
        0x201 -> :sswitch_9
        0x300 -> :sswitch_c
        0x301 -> :sswitch_b
        0x400 -> :sswitch_d
        0x401 -> :sswitch_e
        0x600 -> :sswitch_12
    .end sparse-switch
.end method

.method private declared-synchronized disableKeyguard()V
    .locals 2

    .prologue
    .line 1223
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-nez v0, :cond_0

    .line 1224
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardManager:Landroid/app/KeyguardManager;

    const-string v1, "WlanTest"

    invoke-virtual {v0, v1}, Landroid/app/KeyguardManager;->newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 1225
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->disableKeyguard()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1227
    :cond_0
    monitor-exit p0

    return-void

    .line 1223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized enableKeyguard()V
    .locals 1

    .prologue
    .line 1216
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-eqz v0, :cond_0

    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->reenableKeyguard()V

    .line 1218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1220
    :cond_0
    monitor-exit p0

    return-void

    .line 1216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setAllSettings(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 1318
    const-string v5, ","

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1319
    .local v2, "params":[Ljava/lang/String;
    const-string v3, "NG"

    .line 1320
    .local v3, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1322
    .local v0, "i":I
    array-length v5, v2

    const/16 v6, 0x9

    if-eq v5, v6, :cond_0

    .line 1323
    const-string v5, "WlanTest"

    const-string v6, "Set AllSettings failed - The number of parameter was invalid!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 1365
    .end local v3    # "result":Ljava/lang/String;
    .local v4, "result":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 1328
    .end local v4    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    :cond_0
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v6, "0"

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1360
    :cond_1
    :goto_1
    const-string v5, "NG"

    if-ne v3, v5, :cond_2

    .line 1361
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NG_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1362
    const-string v5, "WlanTest"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Set AllSettings failed - NG on "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v4, v3

    .line 1365
    .end local v3    # "result":Ljava/lang/String;
    .restart local v4    # "result":Ljava/lang/String;
    goto :goto_0

    .line 1330
    .end local v4    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    :cond_3
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SB:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aget-object v6, v2, v0

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1

    .line 1331
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_4
    const-string v5, "WlanTest"

    const-string v6, "2.Set Band - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1333
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CH:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-object v6, v2, v1

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1334
    const-string v5, "WlanTest"

    const-string v6, "3.Set Channel - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    aget-object v6, v2, v0

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1

    .line 1337
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_5
    const-string v5, "WlanTest"

    const-string v6, "10.Set Antenna - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->DR:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-object v6, v2, v1

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1340
    const-string v5, "WlanTest"

    const-string v6, "4.Set Data Rate - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    aget-object v6, v2, v0

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto/16 :goto_1

    .line 1343
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_6
    const-string v5, "WlanTest"

    const-string v6, "5.Set Preamble or Short Guard Interval - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->BI:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-object v6, v2, v1

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1346
    const-string v5, "WlanTest"

    const-string v6, "6.Set Burst Interval - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->PL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    aget-object v6, v2, v0

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto/16 :goto_1

    .line 1349
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_7
    const-string v5, "WlanTest"

    const-string v6, "7.Set PayLoad - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1351
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->BW:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-object v6, v2, v1

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1352
    const-string v5, "WlanTest"

    const-string v6, "8.Set BandWidth - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1354
    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    aget-object v6, v2, v0

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto/16 :goto_1

    .line 1355
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_8
    const-string v5, "WlanTest"

    const-string v6, "9.Set Tx Gain - OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    const-string v3, "OK"

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto/16 :goto_1
.end method


# virtual methods
.method public dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "cmdId"    # Lcom/sec/android/app/wlantest/WlanTest$DutId;
    .param p2, "data"    # Ljava/lang/String;

    .prologue
    .line 1232
    const-string v8, "WlanTest"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Enter dutCommand!!! cmdId: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    const-string v8, "WlanTest"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Enter dutCommand!!! data: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1235
    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq p1, v8, :cond_0

    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN2G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq p1, v8, :cond_0

    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN5G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne p1, v8, :cond_1

    .line 1236
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\nTrying to Join ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1237
    .local v4, "joinMsg":Ljava/lang/String;
    iget-boolean v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mIsScreenOn:Z

    if-eqz v8, :cond_3

    .line 1238
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1239
    .local v5, "message":Landroid/os/Message;
    const/16 v8, 0x101

    iput v8, v5, Landroid/os/Message;->what:I

    .line 1240
    iput-object v4, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1241
    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v8, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1247
    .end local v4    # "joinMsg":Ljava/lang/String;
    .end local v5    # "message":Landroid/os/Message;
    :cond_1
    :goto_0
    const-string v1, ""

    .line 1248
    .local v1, "dutResult":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->ALL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne p1, v8, :cond_5

    .line 1249
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mIsScreenOn:Z

    .line 1250
    invoke-direct {p0, p2}, Lcom/sec/android/app/wlantest/WlanTest;->setAllSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1251
    const-string v8, "OK"

    if-ne v1, v8, :cond_4

    .line 1252
    const-string v8, "\nALL Command completed\n"

    iput-object v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    .line 1256
    :goto_1
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mIsScreenOn:Z

    .line 1304
    :goto_2
    iget-boolean v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mIsScreenOn:Z

    if-eqz v8, :cond_2

    .line 1305
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1306
    .restart local v5    # "message":Landroid/os/Message;
    const/16 v8, 0x101

    iput v8, v5, Landroid/os/Message;->what:I

    .line 1307
    iget-object v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    iput-object v8, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1308
    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v8, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1309
    const-string v8, ""

    iput-object v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    .line 1312
    .end local v5    # "message":Landroid/os/Message;
    :cond_2
    return-object v1

    .line 1243
    .end local v1    # "dutResult":Ljava/lang/String;
    .restart local v4    # "joinMsg":Ljava/lang/String;
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    goto :goto_0

    .line 1254
    .end local v4    # "joinMsg":Ljava/lang/String;
    .restart local v1    # "dutResult":Ljava/lang/String;
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\nALL Command failed on "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    goto :goto_1

    .line 1260
    :cond_5
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mWLService:Landroid/os/IBinder;

    if-eqz v8, :cond_7

    .line 1261
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 1262
    .local v2, "dutcmd":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    .line 1263
    .local v7, "reply":Landroid/os/Parcel;
    const-string v8, "android.wifi.IWlanDutService"

    invoke-virtual {v2, v8}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1264
    invoke-virtual {p1}, Lcom/sec/android/app/wlantest/WlanTest$DutId;->ordinal()I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1265
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1266
    iget-object v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mWLService:Landroid/os/IBinder;

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-interface {v8, v9, v2, v7, v10}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1267
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 1268
    invoke-virtual {v7}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1269
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1279
    .end local v2    # "dutcmd":Landroid/os/Parcel;
    .end local v7    # "reply":Landroid/os/Parcel;
    :goto_3
    const-string v8, "WlanTest"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "After Execute DUT Command!!! result : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1281
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1282
    .local v6, "parsedstatus":Ljava/lang/String;
    const-string v8, "NG"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1283
    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq p1, v8, :cond_6

    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN2G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq p1, v8, :cond_6

    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN5G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne p1, v8, :cond_8

    .line 1284
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - Fail to join ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]\n\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1301
    :goto_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/wlantest/WlanTest;->mBufferedResultMessage:Ljava/lang/String;

    goto/16 :goto_2

    .line 1271
    .end local v6    # "parsedstatus":Ljava/lang/String;
    :cond_7
    :try_start_1
    const-string v8, "WlanTest"

    const-string v9, "Can not get WLService"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    const-string v1, "NG"
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 1274
    :catch_0
    move-exception v3

    .line 1275
    .local v3, "ex":Landroid/os/RemoteException;
    const-string v8, "WlanTest"

    const-string v9, "Fail to operate WLService"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1276
    const-string v1, "NG"

    goto/16 :goto_3

    .line 1286
    .end local v3    # "ex":Landroid/os/RemoteException;
    .restart local v6    # "parsedstatus":Ljava/lang/String;
    :cond_8
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Command failed\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 1288
    :cond_9
    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq p1, v8, :cond_a

    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RE:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne p1, v8, :cond_b

    .line 1289
    :cond_a
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " frame count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1290
    const-string v0, "000000"

    .line 1291
    .local v0, "countBase":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    rsub-int/lit8 v10, v10, 0x6

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1292
    const-string v8, "WlanTest"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "frame count : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1295
    .end local v0    # "countBase":Ljava/lang/String;
    :cond_b
    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq p1, v8, :cond_c

    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN2G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq p1, v8, :cond_c

    sget-object v8, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN5G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne p1, v8, :cond_d

    .line 1296
    :cond_c
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - join to ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] successfully\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_4

    .line 1298
    :cond_d
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Command completed\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_4
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1148
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1149
    const-string v0, "WlanTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfiguration Changed : Do nothing!, Firmware mode ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 44
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 310
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 312
    const-string v4, "WlanTest"

    const-string v5, "on create"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const v4, 0x7f020002

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->setContentView(I)V

    .line 329
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Mode : "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wlantest/WlanTest;->getIntent()Landroid/content/Intent;

    move-result-object v12

    const-string v13, "MODE"

    invoke-virtual {v12, v13}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mIntentFilter:Landroid/content/IntentFilter;

    .line 332
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v5, "com.sec.android.app.wlantest.WIFI_TEST_INDICATION"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 334
    const v4, 0x7f050014

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    sput-object v4, Lcom/sec/android/app/wlantest/WlanTest;->mTextView:Landroid/widget/TextView;

    .line 335
    const v4, 0x7f050013

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ScrollView;

    sput-object v4, Lcom/sec/android/app/wlantest/WlanTest;->mScrollView:Landroid/widget/ScrollView;

    .line 337
    const-string v4, "power"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Landroid/os/PowerManager;

    .line 338
    .local v40, "powerManager":Landroid/os/PowerManager;
    const v4, 0x2000000a

    const-string v5, "WlanTest"

    move-object/from16 v0, v40

    invoke-virtual {v0, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 339
    sget-object v4, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 341
    sget-object v4, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    sput-object v4, Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    .line 344
    const-string v4, "keyguard"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/KeyguardManager;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 346
    const-string v4, "wifi"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 348
    new-instance v33, Landroid/os/HandlerThread;

    const-string v4, "Driver Handler Thread"

    move-object/from16 v0, v33

    invoke-direct {v0, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 349
    .local v33, "driverThread":Landroid/os/HandlerThread;
    invoke-virtual/range {v33 .. v33}, Landroid/os/HandlerThread;->start()V

    .line 350
    new-instance v4, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    invoke-virtual/range {v33 .. v33}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;-><init>(Lcom/sec/android/app/wlantest/WlanTest;Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    .line 352
    new-instance v32, Landroid/os/HandlerThread;

    const-string v4, "Command Handler Thread"

    move-object/from16 v0, v32

    invoke-direct {v0, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 353
    .local v32, "commandThread":Landroid/os/HandlerThread;
    invoke-virtual/range {v32 .. v32}, Landroid/os/HandlerThread;->start()V

    .line 354
    new-instance v4, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

    invoke-virtual/range {v32 .. v32}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;-><init>(Lcom/sec/android/app/wlantest/WlanTest;Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mCommandHandler:Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

    .line 356
    const-string v4, "WlanTest"

    const-string v5, "In onCreate...Registering main Receiver[mReceiver]!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/wlantest/WlanTest;->mIntentFilter:Landroid/content/IntentFilter;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/wlantest/WlanTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 359
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v37

    .line 360
    .local v37, "isNormaWifiOn":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiApEnabled()Z

    move-result v36

    .line 361
    .local v36, "isApDriverLoaded":Z
    const/16 v38, 0x1

    .line 362
    .local v38, "isUnloadedSuccessfully":Z
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "...isNormalDriverLoaded : "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v37

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "...isApDriverLoaded : "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wlantest/WlanTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "wifi_scan_always_enabled"

    const/4 v12, 0x0

    invoke-static {v4, v5, v12}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 366
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wlantest/WlanTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "wifi_scan_always_enabled"

    const/4 v12, 0x0

    invoke-static {v4, v5, v12}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 368
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 369
    const-string v4, "unloaded"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "OK"

    if-ne v4, v5, :cond_4

    const/16 v38, 0x1

    .line 373
    :cond_0
    :goto_0
    const/4 v4, 0x1

    move/from16 v0, v37

    if-ne v0, v4, :cond_6

    .line 374
    const-string v4, "WlanTest"

    const-string v5, "...WiFi Driver already loaded!!!Removing the Drivers!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    new-instance v39, Landroid/os/Message;

    invoke-direct/range {v39 .. v39}, Landroid/os/Message;-><init>()V

    .line 377
    .local v39, "message":Landroid/os/Message;
    const/16 v4, 0x101

    move-object/from16 v0, v39

    iput v4, v0, Landroid/os/Message;->what:I

    .line 378
    const-string v4, "\nUnloading Normal Driver and Loading Mfg Driver\n"

    move-object/from16 v0, v39

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 379
    sget-object v4, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 382
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 384
    const-string v4, "unloaded"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "OK"

    if-ne v4, v5, :cond_5

    const/16 v38, 0x1

    .line 402
    .end local v39    # "message":Landroid/os/Message;
    :cond_1
    :goto_1
    const-string v4, "init.svc.wlandutservice"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "running"

    if-eq v4, v5, :cond_8

    .line 403
    const-string v4, "WlanTest"

    const-string v5, "...Start wlandutservice !!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    const-string v4, "wlandutservice"

    invoke-static {v4}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 409
    :goto_2
    const/16 v42, 0x32

    .local v42, "tryCount":I
    move/from16 v43, v42

    .line 410
    .end local v42    # "tryCount":I
    .local v43, "tryCount":I
    :goto_3
    add-int/lit8 v42, v43, -0x1

    .end local v43    # "tryCount":I
    .restart local v42    # "tryCount":I
    if-lez v43, :cond_2

    .line 411
    const-string v4, "init.svc.wlandutservice"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 412
    .local v41, "prop":Ljava/lang/String;
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "init.svc.wlandutservice : "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    const-string v4, "running"

    move-object/from16 v0, v41

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 415
    const-string v4, "WlanDutService"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mWLService:Landroid/os/IBinder;

    .line 416
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mWLService:Landroid/os/IBinder;

    if-eqz v4, :cond_9

    .line 417
    const-string v4, "WlanTest"

    const-string v5, "get WlanDutService successfully"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    .end local v41    # "prop":Ljava/lang/String;
    :cond_2
    new-instance v35, Landroid/content/Intent;

    const-string v4, "android.intent.action.WIFI_DRIVER_INDICATION"

    move-object/from16 v0, v35

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 436
    .local v35, "intent":Landroid/content/Intent;
    const-string v4, "STATUS"

    const-string v5, "ready"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 438
    const-string v4, "WlanTest"

    const-string v5, "onCreate: Sent response intent: STATUS: ready"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    if-eqz v38, :cond_3

    .line 442
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wlantest/WlanTest;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "MODE"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    const-string v5, "manual"

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 443
    new-instance v39, Landroid/os/Message;

    invoke-direct/range {v39 .. v39}, Landroid/os/Message;-><init>()V

    .line 444
    .restart local v39    # "message":Landroid/os/Message;
    const/16 v4, 0x102

    move-object/from16 v0, v39

    iput v4, v0, Landroid/os/Message;->what:I

    .line 445
    const-string v4, "test"

    move-object/from16 v0, v39

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 446
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    .end local v39    # "message":Landroid/os/Message;
    :cond_3
    :goto_4
    const-string v4, "Driver loading started\nPlease wait till status is shown"

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 459
    const-string v4, "WlanTest"

    const-string v5, "In onCreate...Registering ItemSelectedListeners!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    const v4, 0x7f050012

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->linStatus:Landroid/widget/LinearLayout;

    .line 462
    const v4, 0x7f050015

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->absTxTest:Landroid/widget/RelativeLayout;

    .line 463
    const v4, 0x7f05002b

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->absRxTest:Landroid/widget/RelativeLayout;

    .line 465
    const v4, 0x7f05002f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->channelRx:Landroid/widget/Spinner;

    .line 466
    const/high16 v4, 0x7f030000

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v25

    .line 469
    .local v25, "adapter_channelRx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 472
    const v4, 0x7f030001

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v26

    .line 475
    .local v26, "adapter_channelRx5G":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wlantest/WlanTest;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f030002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v30

    .line 481
    .local v30, "channelListRx5G":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->channelRx:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v5, v0, v1}, Lcom/sec/android/app/wlantest/WlanTest$3;-><init>(Lcom/sec/android/app/wlantest/WlanTest;[I)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 506
    const v4, 0x7f05002d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandRx:Landroid/widget/Spinner;

    .line 507
    const v4, 0x7f03000c

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v23

    .line 510
    .local v23, "adapter_bandRx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 512
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandRx:Landroid/widget/Spinner;

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 513
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandRx:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$4;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-direct {v5, v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$4;-><init>(Lcom/sec/android/app/wlantest/WlanTest;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 538
    const v4, 0x7f050030

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 539
    const v4, 0x7f050031

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->antennaRx:Landroid/widget/Spinner;

    .line 540
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->antennaRx:Landroid/widget/Spinner;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 541
    const v4, 0x7f03000f

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v20

    .line 543
    .local v20, "adapter_antennaRx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 545
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->antennaRx:Landroid/widget/Spinner;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 547
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->antennaRx:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$5;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$5;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 564
    const v4, 0x7f050016

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->packetTx:Landroid/widget/Spinner;

    .line 565
    const v4, 0x7f030003

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v27

    .line 568
    .local v27, "adapter_packetTx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 570
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->packetTx:Landroid/widget/Spinner;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 571
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->packetTx:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$6;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$6;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 588
    const v4, 0x7f05001a

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->channelTx:Landroid/widget/Spinner;

    .line 589
    const/high16 v4, 0x7f030000

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v6

    .line 592
    .local v6, "adapter_channelTx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    invoke-virtual {v6, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 595
    const v4, 0x7f030001

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v9

    .line 598
    .local v9, "adapter_channelTx5G":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    invoke-virtual {v9, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 602
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/wlantest/WlanTest;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f030002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v31

    .line 609
    .local v31, "channelListTx5G":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->channelTx:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$7;

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v5, v0, v1}, Lcom/sec/android/app/wlantest/WlanTest$7;-><init>(Lcom/sec/android/app/wlantest/WlanTest;[I)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 635
    const v4, 0x7f05001e

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->txRate:Landroid/widget/Spinner;

    .line 638
    const v4, 0x7f030006

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v8

    .line 646
    .local v8, "adapter_txRate":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    invoke-virtual {v8, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 652
    const v4, 0x7f030007

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v11

    .line 660
    .local v11, "adapter_txRate5G":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    invoke-virtual {v11, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 663
    const v4, 0x7f030008

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v7

    .line 666
    .local v7, "adapter_MIMORate":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    invoke-virtual {v7, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 669
    const v4, 0x7f030009

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v10

    .line 672
    .local v10, "adapter_MIMORate5G":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    invoke-virtual {v10, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 676
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->txRate:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$8;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$8;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 700
    const v4, 0x7f050020

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->power:Landroid/widget/Spinner;

    .line 701
    const v4, 0x7f03000a

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v28

    .line 704
    .local v28, "adapter_power":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 706
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->power:Landroid/widget/Spinner;

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 707
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->power:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$9;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$9;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 723
    const v4, 0x7f050018

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandTx:Landroid/widget/Spinner;

    .line 724
    const v4, 0x7f03000c

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v22

    .line 727
    .local v22, "adapter_band":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 729
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandTx:Landroid/widget/Spinner;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 730
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandTx:Landroid/widget/Spinner;

    new-instance v4, Lcom/sec/android/app/wlantest/WlanTest$10;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v11}, Lcom/sec/android/app/wlantest/WlanTest$10;-><init>(Lcom/sec/android/app/wlantest/WlanTest;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v12, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 777
    const v4, 0x7f050024

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandwidthTx:Landroid/widget/Spinner;

    .line 780
    const v4, 0x7f03000e

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v24

    .line 788
    .local v24, "adapter_bandwidthTx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 790
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandwidthTx:Landroid/widget/Spinner;

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 791
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->bandwidthTx:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$11;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$11;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 809
    const v4, 0x7f05001b

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 810
    const v4, 0x7f05001c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->antennaTx:Landroid/widget/Spinner;

    .line 811
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->antennaTx:Landroid/widget/Spinner;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 812
    const v4, 0x7f03000f

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v21

    .line 814
    .local v21, "adapter_antennaTx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 816
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->antennaTx:Landroid/widget/Spinner;

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 818
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->antennaTx:Landroid/widget/Spinner;

    new-instance v12, Lcom/sec/android/app/wlantest/WlanTest$12;

    move-object/from16 v13, p0

    move-object v14, v6

    move-object v15, v7

    move-object/from16 v16, v9

    move-object/from16 v17, v10

    move-object/from16 v18, v8

    move-object/from16 v19, v11

    invoke-direct/range {v12 .. v19}, Lcom/sec/android/app/wlantest/WlanTest$12;-><init>(Lcom/sec/android/app/wlantest/WlanTest;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v4, v12}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 853
    const v4, 0x7f050022

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->preamble:Landroid/widget/Spinner;

    .line 854
    const v4, 0x7f03000b

    const v5, 0x1090008

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v29

    .line 857
    .local v29, "adapter_preamble":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v4, 0x1090009

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 859
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->preamble:Landroid/widget/Spinner;

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 860
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->preamble:Landroid/widget/Spinner;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$13;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$13;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 877
    const v4, 0x7f050028

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->burst:Landroid/widget/EditText;

    .line 878
    const v4, 0x7f050026

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->payload:Landroid/widget/EditText;

    .line 879
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->burst:Landroid/widget/EditText;

    const-string v5, "100"

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 880
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->payload:Landroid/widget/EditText;

    const-string v5, "1024"

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 883
    const-string v4, "WlanTest"

    const-string v5, "In onCreate...Registering ClickListeners!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    const v4, 0x7f050029

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStartTx:Landroid/widget/Button;

    .line 886
    const v4, 0x7f05002a

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStopTx:Landroid/widget/Button;

    .line 887
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStartTx:Landroid/widget/Button;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 888
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStopTx:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 890
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStartTx:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$14;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$14;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 947
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStopTx:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$15;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$15;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 972
    const v4, 0x7f050038

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStartRx:Landroid/widget/Button;

    .line 973
    const v4, 0x7f050039

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStopRx:Landroid/widget/Button;

    .line 974
    const v4, 0x7f05003a

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btClearRx:Landroid/widget/Button;

    .line 975
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStartRx:Landroid/widget/Button;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 976
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btClearRx:Landroid/widget/Button;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 977
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStopRx:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 979
    const v4, 0x7f050033

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->received:Landroid/widget/EditText;

    .line 980
    const v4, 0x7f050035

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->error:Landroid/widget/EditText;

    .line 981
    const v4, 0x7f050037

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/wlantest/WlanTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->per:Landroid/widget/EditText;

    .line 983
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStartRx:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$16;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$16;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1018
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btStopRx:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$17;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$17;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1057
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->btClearRx:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/wlantest/WlanTest$18;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/wlantest/WlanTest$18;-><init>(Lcom/sec/android/app/wlantest/WlanTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1069
    return-void

    .line 369
    .end local v6    # "adapter_channelTx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v7    # "adapter_MIMORate":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v8    # "adapter_txRate":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v9    # "adapter_channelTx5G":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v10    # "adapter_MIMORate5G":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v11    # "adapter_txRate5G":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v20    # "adapter_antennaRx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v21    # "adapter_antennaTx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v22    # "adapter_band":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v23    # "adapter_bandRx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v24    # "adapter_bandwidthTx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v25    # "adapter_channelRx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v26    # "adapter_channelRx5G":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v27    # "adapter_packetTx":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v28    # "adapter_power":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v29    # "adapter_preamble":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v30    # "channelListRx5G":[I
    .end local v31    # "channelListTx5G":[I
    .end local v35    # "intent":Landroid/content/Intent;
    .end local v42    # "tryCount":I
    :cond_4
    const/16 v38, 0x0

    goto/16 :goto_0

    .line 384
    .restart local v39    # "message":Landroid/os/Message;
    :cond_5
    const/16 v38, 0x0

    goto/16 :goto_1

    .line 385
    .end local v39    # "message":Landroid/os/Message;
    :cond_6
    const/4 v4, 0x1

    move/from16 v0, v36

    if-ne v0, v4, :cond_1

    .line 386
    const-string v4, "WlanTest"

    const-string v5, "...WiFi AP Driver already loaded!!!Removing the Drivers!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    new-instance v39, Landroid/os/Message;

    invoke-direct/range {v39 .. v39}, Landroid/os/Message;-><init>()V

    .line 389
    .restart local v39    # "message":Landroid/os/Message;
    const/16 v4, 0x101

    move-object/from16 v0, v39

    iput v4, v0, Landroid/os/Message;->what:I

    .line 390
    const-string v4, "\nUnloading AP Driver and Loading Mfg Driver\n"

    move-object/from16 v0, v39

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 391
    sget-object v4, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 394
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x0

    const/4 v12, 0x0

    invoke-virtual {v4, v5, v12}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    .line 396
    const-string v4, "unloaded"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "OK"

    if-ne v4, v5, :cond_7

    const/16 v38, 0x1

    :goto_5
    goto/16 :goto_1

    :cond_7
    const/16 v38, 0x0

    goto :goto_5

    .line 406
    .end local v39    # "message":Landroid/os/Message;
    :cond_8
    const-string v4, "WlanTest"

    const-string v5, "...wlservice is already started!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 420
    .restart local v41    # "prop":Ljava/lang/String;
    .restart local v42    # "tryCount":I
    :cond_9
    const-string v4, "WlanTest"

    const-string v5, "fail to get WlanDutService but we will try more!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :cond_a
    if-nez v42, :cond_b

    .line 425
    new-instance v39, Landroid/os/Message;

    invoke-direct/range {v39 .. v39}, Landroid/os/Message;-><init>()V

    .line 426
    .restart local v39    # "message":Landroid/os/Message;
    const/16 v4, 0x101

    move-object/from16 v0, v39

    iput v4, v0, Landroid/os/Message;->what:I

    .line 427
    const-string v4, "\nFail to Launch wlandutservice\nPlease retry!\n"

    move-object/from16 v0, v39

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 428
    sget-object v4, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 431
    .end local v39    # "message":Landroid/os/Message;
    :cond_b
    const-wide/16 v4, 0x64

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 432
    :goto_6
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "tryCount : "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v43, v42

    .line 433
    .end local v42    # "tryCount":I
    .restart local v43    # "tryCount":I
    goto/16 :goto_3

    .line 448
    .end local v41    # "prop":Ljava/lang/String;
    .end local v43    # "tryCount":I
    .restart local v35    # "intent":Landroid/content/Intent;
    .restart local v42    # "tryCount":I
    :catch_0
    move-exception v34

    .line 449
    .local v34, "e":Ljava/lang/NullPointerException;
    const-string v4, "WlanTest"

    const-string v5, "Ignore NullPointerException for check Launcher"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    new-instance v39, Landroid/os/Message;

    invoke-direct/range {v39 .. v39}, Landroid/os/Message;-><init>()V

    .line 451
    .restart local v39    # "message":Landroid/os/Message;
    const/16 v4, 0x102

    move-object/from16 v0, v39

    iput v4, v0, Landroid/os/Message;->what:I

    .line 452
    const-string v4, "test"

    move-object/from16 v0, v39

    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 453
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4

    .line 431
    .end local v34    # "e":Ljava/lang/NullPointerException;
    .end local v35    # "intent":Landroid/content/Intent;
    .end local v39    # "message":Landroid/os/Message;
    .restart local v41    # "prop":Ljava/lang/String;
    :catch_1
    move-exception v4

    goto :goto_6
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1072
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1073
    const-string v0, "Tx Test"

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1074
    const-string v0, "Rx Test"

    invoke-interface {p1, v2, v3, v3, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1075
    const/4 v0, 0x2

    const-string v1, "Status"

    invoke-interface {p1, v2, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1076
    return v3
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1163
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1165
    iget-boolean v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->isFinishOnPause:Z

    if-nez v1, :cond_3

    .line 1166
    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-eq v1, v2, :cond_0

    .line 1167
    const-string v1, "DO NOT RESTART WlanTest!\nPlease wait till status is shown"

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1168
    const-string v1, "mfgloader:-u"

    invoke-static {v1}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 1169
    const-string v1, "unloaded"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;

    .line 1172
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WIFI_DRIVER_INDICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1173
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "STATUS"

    const-string v2, "finish"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1174
    invoke-virtual {p0, v0}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1175
    const-string v1, "WlanTest"

    const-string v2, "onDestroy: Sent response intent: STATUS: finish"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    iget v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I

    if-lez v1, :cond_1

    .line 1178
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.wlantest.WIFI_TEST_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1179
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "CMDID"

    iget v2, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1180
    const-string v1, "S_DATA"

    iget-object v2, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1181
    invoke-virtual {p0, v0}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1182
    const-string v1, "WlanTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDestroy: Sent response intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1183
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I

    .line 1184
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;

    .line 1187
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 1188
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mCommandHandler:Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 1189
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wlantest/WlanTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1191
    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1192
    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1194
    :cond_2
    const-string v1, "Ok! You can use the WlanTest again from now on"

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1195
    const-string v1, "WlanTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "on Destroy ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->isFinishOnPause:Z

    .line 1198
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 1081
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1107
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1083
    :pswitch_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "menu 0"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1084
    const-string v0, "WlanTest"

    const-string v1, "onOptionsItemSelected 0"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1086
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->absTxTest:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1087
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->linStatus:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1088
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->absRxTest:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 1092
    :pswitch_1
    const-string v0, "WlanTest"

    const-string v1, "onOptionsItemSelected 1"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "menu 1"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1094
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->absTxTest:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1095
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->linStatus:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1096
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->absRxTest:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 1100
    :pswitch_2
    const-string v0, "WlanTest"

    const-string v1, "onOptionsItemSelected 2"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "menu 2"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1102
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->absTxTest:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1103
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->linStatus:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1104
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest;->absRxTest:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 1081
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1112
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1113
    const-string v1, "WlanTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "on pause ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    iget v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I

    if-lez v1, :cond_2

    .line 1115
    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-eq v1, v2, :cond_0

    .line 1116
    const-string v1, "DO NOT RESTART WlanTest!\nPlease wait till status is shown"

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1117
    const-string v1, "mfgloader:-u"

    invoke-static {v1}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 1118
    const-string v1, "unloaded"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;

    .line 1121
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WIFI_DRIVER_INDICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1122
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "STATUS"

    const-string v2, "finish"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1123
    invoke-virtual {p0, v0}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1124
    const-string v1, "WlanTest"

    const-string v2, "onPause: Sent response intent: STATUS: finish"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.wlantest.WIFI_TEST_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1127
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "CMDID"

    iget v2, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1128
    const-string v1, "S_DATA"

    iget-object v2, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1129
    invoke-virtual {p0, v0}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1130
    const-string v1, "WlanTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDestroy: Sent response intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1131
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I

    .line 1132
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;

    .line 1134
    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1135
    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1137
    :cond_1
    const-string v1, "Ok! You can use the WlanTest again from now on"

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1138
    const-string v1, "WlanTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "on Destroy ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1139
    iput-boolean v4, p0, Lcom/sec/android/app/wlantest/WlanTest;->isFinishOnPause:Z

    .line 1141
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 1142
    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest;->mCommandHandler:Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 1144
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 1203
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1205
    sget-object v0, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1206
    sget-object v0, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1209
    :cond_0
    const-string v0, "WlanTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "on resume ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    invoke-direct {p0}, Lcom/sec/android/app/wlantest/WlanTest;->disableKeyguard()V

    .line 1212
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1154
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 1156
    invoke-direct {p0}, Lcom/sec/android/app/wlantest/WlanTest;->enableKeyguard()V

    .line 1158
    const-string v0, "WlanTest"

    const-string v1, "on stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    return-void
.end method
