.class final enum Lcom/sec/android/app/wlantest/WlanTest$DutId;
.super Ljava/lang/Enum;
.source "WlanTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wlantest/WlanTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DutId"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/wlantest/WlanTest$DutId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum ALL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum AM:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum BI:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum BW:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum CD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum CH:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum DR:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum GR:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum JN:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum JN2G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum JN5G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum MA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum NF:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum NU:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum PL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum RA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum RE:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum RG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum RP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum RS:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum SA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum SB:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum SC:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum SP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum TE:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum TG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum TM:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum TP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum TS:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum WA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum WD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

.field public static final enum XX:Lcom/sec/android/app/wlantest/WlanTest$DutId;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 99
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "OD"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 101
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "CD"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 103
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "DR"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->DR:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 105
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "AM"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->AM:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 107
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "SA"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 109
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "SP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 111
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "CH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CH:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 113
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "TG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 115
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "BI"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->BI:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 117
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "PL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->PL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 119
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "TS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TS:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 121
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "TP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 123
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "RS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RS:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 125
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "RP"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 127
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "RE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RE:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 129
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "RG"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 131
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "SB"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SB:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 133
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "BW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->BW:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 135
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "NF"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NF:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 137
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "NU"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NU:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 139
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "JN"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 141
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "JN2G"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN2G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 143
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "JN5G"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN5G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 145
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "GR"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->GR:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 147
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "ALL"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->ALL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 149
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "XX"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->XX:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "MA"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->MA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "RA"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "WA"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->WA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 150
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "WD"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->WD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "TE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TE:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "SC"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SC:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "TM"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TM:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 152
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v1, "INVALID"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/wlantest/WlanTest$DutId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 97
    const/16 v0, 0x22

    new-array v0, v0, [Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest$DutId;->OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest$DutId;->DR:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest$DutId;->AM:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CH:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->BI:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->PL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TS:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RS:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RP:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RE:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RG:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SB:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->BW:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NF:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NU:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN2G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->JN5G:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->GR:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->ALL:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->XX:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->MA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->RA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->WA:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->WD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TE:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->SC:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->TM:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/android/app/wlantest/WlanTest$DutId;->INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->$VALUES:[Lcom/sec/android/app/wlantest/WlanTest$DutId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/wlantest/WlanTest$DutId;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 97
    const-class v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/wlantest/WlanTest$DutId;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/sec/android/app/wlantest/WlanTest$DutId;->$VALUES:[Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-virtual {v0}, [Lcom/sec/android/app/wlantest/WlanTest$DutId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/wlantest/WlanTest$DutId;

    return-object v0
.end method
