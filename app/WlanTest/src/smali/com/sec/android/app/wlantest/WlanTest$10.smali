.class Lcom/sec/android/app/wlantest/WlanTest$10;
.super Ljava/lang/Object;
.source "WlanTest.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wlantest/WlanTest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wlantest/WlanTest;

.field final synthetic val$adapter_MIMORate:Landroid/widget/ArrayAdapter;

.field final synthetic val$adapter_MIMORate5G:Landroid/widget/ArrayAdapter;

.field final synthetic val$adapter_channelTx:Landroid/widget/ArrayAdapter;

.field final synthetic val$adapter_channelTx5G:Landroid/widget/ArrayAdapter;

.field final synthetic val$adapter_txRate:Landroid/widget/ArrayAdapter;

.field final synthetic val$adapter_txRate5G:Landroid/widget/ArrayAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wlantest/WlanTest;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;)V
    .locals 0

    .prologue
    .line 730
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iput-object p2, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_channelTx:Landroid/widget/ArrayAdapter;

    iput-object p3, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_MIMORate:Landroid/widget/ArrayAdapter;

    iput-object p4, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_txRate:Landroid/widget/ArrayAdapter;

    iput-object p5, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_channelTx5G:Landroid/widget/ArrayAdapter;

    iput-object p6, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_MIMORate5G:Landroid/widget/ArrayAdapter;

    iput-object p7, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_txRate5G:Landroid/widget/ArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 735
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    add-int/lit8 v1, p3, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/wlantest/WlanTest;->posBandTx:Ljava/lang/String;

    .line 736
    const-string v0, "WlanTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "...."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    if-nez p3, :cond_2

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->posAntennaTx:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 741
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->channelTx:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_channelTx:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 742
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->txRate:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_MIMORate:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 744
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->channelTx:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_channelTx:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->txRate:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_txRate:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0

    .line 747
    :cond_2
    const/4 v0, 0x1

    if-ne v0, p3, :cond_0

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->posAntennaTx:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->channelTx:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_channelTx5G:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->txRate:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_MIMORate5G:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0

    .line 752
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->channelTx:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_channelTx5G:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 753
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v0, v0, Lcom/sec/android/app/wlantest/WlanTest;->txRate:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanTest$10;->val$adapter_txRate5G:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 772
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
