.class Lcom/sec/android/app/wlantest/WlanMacAddress$8;
.super Ljava/lang/Object;
.source "WlanMacAddress.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wlantest/WlanMacAddress;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V
    .locals 0

    .prologue
    .line 499
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 501
    const-string v7, "WlanMac"

    const-string v8, "........Click the Set Country Code....."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v7, v7, Lcom/sec/android/app/wlantest/WlanMacAddress;->mCountryEt:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 504
    .local v0, "country":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v7, v7, Lcom/sec/android/app/wlantest/WlanMacAddress;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "airplane_mode_on"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 506
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 507
    .local v4, "intent":Landroid/content/Intent;
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    invoke-virtual {v7, v4}, Lcom/sec/android/app/wlantest/WlanMacAddress;->sendBroadcast(Landroid/content/Intent;)V

    .line 509
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v7, v7, Lcom/sec/android/app/wlantest/WlanMacAddress;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "airplane_mode_on"

    invoke-static {v7, v8, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v6, :cond_0

    move v5, v6

    .line 511
    .local v5, "isEnabled":Z
    :cond_0
    if-eqz v5, :cond_1

    .line 512
    const-wide/16 v8, 0x1f4

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 515
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 516
    const-string v7, "WlanMac"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "........Country : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v7

    invoke-virtual {v7, v0, v6}, Landroid/net/wifi/WifiManager;->setCountryCode(Ljava/lang/String;Z)V

    .line 518
    const-wide/16 v8, 0x3e8

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 521
    :cond_2
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    .line 523
    .local v3, "initialWifiState":Z
    if-nez v3, :cond_3

    .line 524
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 525
    const/4 v1, 0x0

    .line 527
    .local v1, "i":I
    :goto_2
    const-string v7, "WlanMac"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    const-wide/16 v8, 0x64

    :try_start_2
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 529
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 532
    .end local v2    # "i":I
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v7, v7, Lcom/sec/android/app/wlantest/WlanMacAddress;->mCountrytx:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Current Phone\'s locale : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (Country)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$8;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    const-string v8, "Country Code Change complete"

    invoke-static {v7, v8, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 534
    return-void

    .line 512
    .end local v3    # "initialWifiState":Z
    :catch_0
    move-exception v7

    goto/16 :goto_0

    .line 518
    :catch_1
    move-exception v7

    goto :goto_1

    .line 528
    .restart local v2    # "i":I
    .restart local v3    # "initialWifiState":Z
    :catch_2
    move-exception v7

    goto :goto_3

    :cond_4
    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_2
.end method
