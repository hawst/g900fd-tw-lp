.class Lcom/sec/android/app/wlantest/WlanMacAddress$6;
.super Ljava/lang/Object;
.source "WlanMacAddress.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wlantest/WlanMacAddress;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$6;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 426
    :try_start_0
    const-string v6, "WlanMac"

    const-string v7, "........Click the Changed MAC"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$6;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    .line 428
    .local v3, "initialWifiState":Z
    if-eqz v3, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$6;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 429
    :cond_0
    const/4 v4, 0x0

    .line 430
    .local v4, "macPathEFS":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    .end local v4    # "macPathEFS":Ljava/io/File;
    const-string v6, "/efs/wifi/.mac.info"

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 431
    .restart local v4    # "macPathEFS":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 432
    const-string v6, "WlanMac"

    const-string v7, "........delete MAC Success!!...."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    :goto_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    const-string v7, "macloader"

    invoke-virtual {v6, v7}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v5

    .line 437
    .local v5, "p":Ljava/lang/Process;
    invoke-virtual {v5}, Ljava/lang/Process;->waitFor()I

    .line 438
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$6;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    invoke-virtual {v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getMacAddress()V

    .line 439
    if-eqz v3, :cond_1

    .line 440
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$6;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 441
    const/4 v1, 0x0

    .local v1, "i":I
    move v2, v1

    .line 443
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_1
    const-string v6, "WlanMac"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 444
    const-wide/16 v6, 0x64

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 445
    :goto_2
    :try_start_2
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$6;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v6

    if-eqz v6, :cond_3

    .line 453
    .end local v1    # "i":I
    .end local v3    # "initialWifiState":Z
    .end local v4    # "macPathEFS":Ljava/io/File;
    .end local v5    # "p":Ljava/lang/Process;
    :cond_1
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$6;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v6, v6, Lcom/sec/android/app/wlantest/WlanMacAddress;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 454
    return-void

    .line 434
    .restart local v3    # "initialWifiState":Z
    .restart local v4    # "macPathEFS":Ljava/io/File;
    :cond_2
    :try_start_3
    const-string v6, "WlanMac"

    const-string v7, ".....Mac Delete fail!!....."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 447
    .end local v3    # "initialWifiState":Z
    .end local v4    # "macPathEFS":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "WlanMac"

    const-string v7, "File Close error"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 449
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 450
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 444
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "i":I
    .restart local v3    # "initialWifiState":Z
    .restart local v4    # "macPathEFS":Ljava/io/File;
    .restart local v5    # "p":Ljava/lang/Process;
    :catch_2
    move-exception v6

    goto :goto_2

    :cond_3
    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_1
.end method
