.class Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;
.super Landroid/os/Handler;
.source "WlanTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wlantest/WlanTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CommandHandler"
.end annotation


# instance fields
.field private mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

.field final synthetic this$0:Lcom/sec/android/app/wlantest/WlanTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/wlantest/WlanTest;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1439
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    .line 1440
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1442
    new-instance v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    invoke-direct {v0}, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    .line 1443
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v8, 0x104

    const/16 v7, 0x102

    .line 1447
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1449
    .local v2, "respAction":Landroid/content/Intent;
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 1567
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1569
    :cond_0
    :goto_0
    return-void

    .line 1451
    :pswitch_0
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$400()Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1452
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$400()Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1453
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$400()Landroid/os/PowerManager$WakeLock;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1456
    :cond_1
    const-string v4, "com.sec.android.app.wlantest.WIFI_TEST_RESPONSE"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1457
    const-string v4, "CMDID"

    iget-object v5, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v5, v5, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-virtual {v5}, Lcom/sec/android/app/wlantest/WlanTest$DutId;->ordinal()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1459
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    .line 1461
    .local v0, "args":Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mCurrentCmd.cmdId : <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v6, v6, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">, args.cmdId : <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFirmware : <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1464
    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq v4, v5, :cond_2

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NU:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_3

    :cond_2
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-ne v4, v5, :cond_3

    .line 1465
    const-string v4, "S_DATA"

    const-string v5, "OK"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1466
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1467
    const-string v4, "WlanTest"

    const-string v5, "Sent response intent: OK"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1468
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$400()Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1469
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "release wakelock by "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$400()Landroid/os/PowerManager$WakeLock;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1471
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v4, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v5, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_4

    .line 1472
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Skip Command : Command ID <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "> is already running"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1474
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v4, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_5

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq v4, v5, :cond_5

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-ne v4, v5, :cond_6

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq v4, v5, :cond_6

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NF:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq v4, v5, :cond_6

    .line 1476
    :cond_5
    const-string v4, "WlanTest"

    const-string v5, "Wrong Wlan Test Command."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1477
    const-string v4, "S_DATA"

    const-string v5, "NG"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1478
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1479
    const-string v4, "WlanTest"

    const-string v5, "Sent response intent: NG"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1483
    :cond_6
    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    .line 1485
    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_8

    .line 1486
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-ne v4, v5, :cond_7

    .line 1487
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1488
    .local v1, "message":Landroid/os/Message;
    iput v7, v1, Landroid/os/Message;->what:I

    .line 1489
    const-string v4, "test"

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1490
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;
    invoke-static {v4}, Lcom/sec/android/app/wlantest/WlanTest;->access$600(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1492
    .end local v1    # "message":Landroid/os/Message;
    :cond_7
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NormalMode:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-ne v4, v5, :cond_a

    .line 1493
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1494
    .restart local v1    # "message":Landroid/os/Message;
    iput v8, v1, Landroid/os/Message;->what:I

    .line 1495
    const-string v4, "test"

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1496
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;
    invoke-static {v4}, Lcom/sec/android/app/wlantest/WlanTest;->access$600(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1499
    .end local v1    # "message":Landroid/os/Message;
    :cond_8
    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NF:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_a

    .line 1500
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-ne v4, v5, :cond_9

    .line 1501
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1502
    .restart local v1    # "message":Landroid/os/Message;
    iput v7, v1, Landroid/os/Message;->what:I

    .line 1503
    const-string v4, "normal"

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1504
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;
    invoke-static {v4}, Lcom/sec/android/app/wlantest/WlanTest;->access$600(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1506
    .end local v1    # "message":Landroid/os/Message;
    :cond_9
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->TestMode:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-ne v4, v5, :cond_a

    .line 1507
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1508
    .restart local v1    # "message":Landroid/os/Message;
    iput v8, v1, Landroid/os/Message;->what:I

    .line 1509
    const-string v4, "normal"

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1510
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;
    invoke-static {v4}, Lcom/sec/android/app/wlantest/WlanTest;->access$600(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1521
    .end local v1    # "message":Landroid/os/Message;
    :cond_a
    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NF:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq v4, v5, :cond_b

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NU:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_d

    .line 1522
    :cond_b
    const-string v3, "OK"

    .line 1528
    .local v3, "rspStr":Ljava/lang/String;
    :goto_1
    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq v4, v5, :cond_c

    iget-object v4, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NU:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_e

    .line 1529
    :cond_c
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    if-eq v4, v5, :cond_e

    .line 1530
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mDriverHandler:Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;
    invoke-static {v4}, Lcom/sec/android/app/wlantest/WlanTest;->access$600(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;

    move-result-object v4

    const/16 v5, 0x103

    invoke-virtual {v4, v5}, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1524
    .end local v3    # "rspStr":Ljava/lang/String;
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v5, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iget-object v6, v0, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->data:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1525
    .restart local v3    # "rspStr":Ljava/lang/String;
    const-string v4, "WlanTest"

    const-string v5, " dutCommand() Complete."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1534
    :cond_e
    const-string v4, "S_DATA"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1535
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1536
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sent response intent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1537
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iput-object v5, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    goto/16 :goto_0

    .line 1542
    .end local v0    # "args":Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;
    .end local v3    # "rspStr":Ljava/lang/String;
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v4, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq v4, v5, :cond_0

    .line 1545
    const-string v4, "com.sec.android.app.wlantest.WIFI_TEST_RESPONSE"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1546
    const-string v4, "CMDID"

    iget-object v5, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v5, v5, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-virtual {v5}, Lcom/sec/android/app/wlantest/WlanTest$DutId;->ordinal()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1548
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 1549
    .restart local v3    # "rspStr":Ljava/lang/String;
    const-string v4, "OK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v4, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_f

    .line 1550
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->OD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    const-string v6, "0"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/wlantest/WlanTest;->dutCommand(Lcom/sec/android/app/wlantest/WlanTest$DutId;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1553
    :cond_f
    const-string v4, "S_DATA"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1554
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v4, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->CD:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-eq v4, v5, :cond_10

    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v4, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->NU:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    if-ne v4, v5, :cond_11

    .line 1555
    :cond_10
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    iget-object v5, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    iget-object v5, v5, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    invoke-virtual {v5}, Lcom/sec/android/app/wlantest/WlanTest$DutId;->ordinal()I

    move-result v5

    # setter for: Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadCmdId:I
    invoke-static {v4, v5}, Lcom/sec/android/app/wlantest/WlanTest;->access$702(Lcom/sec/android/app/wlantest/WlanTest;I)I

    .line 1556
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # setter for: Lcom/sec/android/app/wlantest/WlanTest;->mDriverUnloadResult:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$802(Lcom/sec/android/app/wlantest/WlanTest;Ljava/lang/String;)Ljava/lang/String;

    .line 1557
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iput-object v5, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 1558
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    invoke-virtual {v4}, Lcom/sec/android/app/wlantest/WlanTest;->finish()V

    goto/16 :goto_0

    .line 1561
    :cond_11
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->mCurrentCmd:Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;

    sget-object v5, Lcom/sec/android/app/wlantest/WlanTest$DutId;->INVALID:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    iput-object v5, v4, Lcom/sec/android/app/wlantest/WlanTest$CommandArgs;->cmdId:Lcom/sec/android/app/wlantest/WlanTest$DutId;

    .line 1562
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/wlantest/WlanTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1563
    const-string v4, "WlanTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sent response intent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1449
    nop

    :pswitch_data_0
    .packed-switch 0x105
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
