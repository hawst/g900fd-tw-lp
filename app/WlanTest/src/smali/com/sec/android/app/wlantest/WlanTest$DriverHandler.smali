.class Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;
.super Landroid/os/Handler;
.source "WlanTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wlantest/WlanTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DriverHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wlantest/WlanTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/wlantest/WlanTest;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1575
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    .line 1576
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1577
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v7, 0x105

    const/16 v6, 0x101

    const/16 v5, 0xa

    .line 1580
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1581
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1583
    .local v0, "guiMsg":Landroid/os/Message;
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 1664
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1666
    :goto_0
    return-void

    .line 1586
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 1587
    .local v2, "mode":Ljava/lang/String;
    const-string v3, "test"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "normal"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1588
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mfgloader:-l"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 1590
    const-string v3, "ok"

    # invokes: Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v3, v5}, Lcom/sec/android/app/wlantest/WlanTest;->access$900(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1591
    iget-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    check-cast v3, Ljava/lang/String;

    const-string v4, "OK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1592
    const-string v3, "test"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->TestMode:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    :goto_1
    # setter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$502(Lcom/sec/android/app/wlantest/WlanTest$FWMode;)Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    .line 1602
    :goto_2
    iput v7, v1, Landroid/os/Message;->what:I

    .line 1603
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mCommandHandler:Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;
    invoke-static {v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$300(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1605
    iput v6, v0, Landroid/os/Message;->what:I

    .line 1606
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n\n "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Driver loaded ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1607
    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1592
    :cond_1
    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NormalMode:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    goto :goto_1

    .line 1594
    :cond_2
    const-string v3, "mfgloader:-u"

    invoke-static {v3}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 1595
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1596
    :goto_3
    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    # setter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$502(Lcom/sec/android/app/wlantest/WlanTest$FWMode;)Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    goto :goto_2

    .line 1600
    :cond_3
    const-string v3, "NG"

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_2

    .line 1612
    .end local v2    # "mode":Ljava/lang/String;
    :pswitch_1
    iput v7, v1, Landroid/os/Message;->what:I

    .line 1614
    const-string v3, "mfgloader:-u"

    invoke-static {v3}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 1616
    const-string v3, "unloaded"

    # invokes: Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v3, v5}, Lcom/sec/android/app/wlantest/WlanTest;->access$900(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1617
    iget-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    check-cast v3, Ljava/lang/String;

    const-string v4, "OK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1618
    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    # setter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$502(Lcom/sec/android/app/wlantest/WlanTest$FWMode;)Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    .line 1620
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mCommandHandler:Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;
    invoke-static {v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$300(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1622
    iput v6, v0, Landroid/os/Message;->what:I

    .line 1623
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n\n Driver unloaded ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1624
    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1629
    :pswitch_2
    iput v7, v1, Landroid/os/Message;->what:I

    .line 1631
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 1632
    .restart local v2    # "mode":Ljava/lang/String;
    const-string v3, "test"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "normal"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1633
    :cond_5
    const-string v3, "mfgloader:-u"

    invoke-static {v3}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 1634
    const-string v3, "unloaded"

    const/4 v4, 0x5

    # invokes: Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/wlantest/WlanTest;->access$900(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1635
    iget-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    check-cast v3, Ljava/lang/String;

    const-string v4, "OK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1636
    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NotSet:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    # setter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$502(Lcom/sec/android/app/wlantest/WlanTest$FWMode;)Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    .line 1637
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mfgloader:-l"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 1639
    const-string v3, "ok"

    # invokes: Lcom/sec/android/app/wlantest/WlanTest;->checkDriverResult(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v3, v5}, Lcom/sec/android/app/wlantest/WlanTest;->access$900(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1640
    iget-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    check-cast v3, Ljava/lang/String;

    const-string v4, "OK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1641
    const-string v3, "test"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->TestMode:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    :goto_4
    # setter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$502(Lcom/sec/android/app/wlantest/WlanTest$FWMode;)Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    .line 1655
    :goto_5
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanTest$DriverHandler;->this$0:Lcom/sec/android/app/wlantest/WlanTest;

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mCommandHandler:Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;
    invoke-static {v3}, Lcom/sec/android/app/wlantest/WlanTest;->access$300(Lcom/sec/android/app/wlantest/WlanTest;)Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/app/wlantest/WlanTest$CommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1657
    iput v6, v0, Landroid/os/Message;->what:I

    .line 1658
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n\n Driver changed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mFirmware:Lcom/sec/android/app/wlantest/WlanTest$FWMode;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$500()Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1659
    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest;->myGUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1641
    :cond_6
    sget-object v3, Lcom/sec/android/app/wlantest/WlanTest$FWMode;->NormalMode:Lcom/sec/android/app/wlantest/WlanTest$FWMode;

    goto :goto_4

    .line 1643
    :cond_7
    const-string v3, "mfgloader:-u"

    invoke-static {v3}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    .line 1644
    const-wide/16 v4, 0x3e8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    :catch_0
    move-exception v3

    goto :goto_5

    .line 1648
    :cond_8
    const-string v3, "NG"

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_5

    .line 1652
    :cond_9
    const-string v3, "NG"

    iput-object v3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_5

    .line 1595
    :catch_1
    move-exception v3

    goto/16 :goto_3

    .line 1583
    :pswitch_data_0
    .packed-switch 0x102
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
