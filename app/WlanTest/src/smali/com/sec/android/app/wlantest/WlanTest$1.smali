.class final Lcom/sec/android/app/wlantest/WlanTest$1;
.super Landroid/os/Handler;
.source "WlanTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wlantest/WlanTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 201
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 211
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 203
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 204
    .local v0, "str":Ljava/lang/String;
    const-string v1, "WlanTest"

    const-string v2, "Inside Handle Message: EVENT_GUI_UPDATE"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mTextView:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$000()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 206
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mTextView:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$000()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 207
    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$100()Landroid/widget/ScrollView;

    move-result-object v1

    const/4 v2, 0x0

    # getter for: Lcom/sec/android/app/wlantest/WlanTest;->mTextView:Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/wlantest/WlanTest;->access$000()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    goto :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_0
    .end packed-switch
.end method
