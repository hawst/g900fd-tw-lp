.class public Lcom/sec/android/app/wlantest/WlanMacAddress;
.super Landroid/app/Activity;
.source "WlanMacAddress.java"


# instance fields
.field builder:Landroid/app/AlertDialog$Builder;

.field private compatibility_layout:Landroid/widget/LinearLayout;

.field public context:Landroid/content/Context;

.field public currentCountryCode:Ljava/lang/String;

.field doubleclickAntennaSetModeflag:I

.field doubleclickCompatibilityTestModeflag:I

.field doubleclickRFTestModeflag:I

.field public mCountryEt:Landroid/widget/EditText;

.field public mCountrytx:Landroid/widget/TextView;

.field public mRemoveCountry:Landroid/widget/Button;

.field public mSetContry:Landroid/widget/Button;

.field private mWifiFilter:Landroid/content/IntentFilter;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiRestartFlag:Z

.field moduleType:Ljava/lang/String;

.field r1:Landroid/widget/RadioButton;

.field r2:Landroid/widget/RadioButton;

.field r3:Landroid/widget/RadioButton;

.field r4:Landroid/widget/RadioButton;

.field r5:Landroid/widget/RadioButton;

.field public r6:Landroid/widget/Button;

.field rc1:Landroid/widget/RadioButton;

.field rc2:Landroid/widget/RadioButton;

.field public tv:Landroid/widget/TextView;

.field public tv_module:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    iput-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->tv:Landroid/widget/TextView;

    .line 53
    iput-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->currentCountryCode:Ljava/lang/String;

    .line 58
    iput-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->tv_module:Landroid/widget/TextView;

    .line 59
    new-instance v0, Ljava/lang/String;

    const-string v1, "COB"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->moduleType:Ljava/lang/String;

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiRestartFlag:Z

    .line 67
    iput-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->compatibility_layout:Landroid/widget/LinearLayout;

    .line 70
    iput v2, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickRFTestModeflag:I

    .line 74
    iput v2, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickAntennaSetModeflag:I

    .line 79
    iput v2, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/wlantest/WlanMacAddress;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanMacAddress;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiRestartFlag:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanMacAddress;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/wlantest/WlanMacAddress;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wlantest/WlanMacAddress;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getCurrentCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentCountry()Ljava/lang/String;
    .locals 6

    .prologue
    .line 556
    const-string v4, "country_detector"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/CountryDetector;

    .line 557
    .local v1, "countryDetector":Landroid/location/CountryDetector;
    invoke-virtual {v1}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v0

    .line 558
    .local v0, "country":Landroid/location/Country;
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 559
    .local v3, "strSource":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 561
    .local v2, "strCountry":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/location/Country;->getSource()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 577
    :goto_0
    invoke-virtual {v0}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v2

    .line 578
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Current Phone\'s locale : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (Country)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 563
    :pswitch_0
    const-string v3, "Mobile network"

    .line 564
    goto :goto_0

    .line 566
    :pswitch_1
    const-string v3, "Location"

    .line 567
    goto :goto_0

    .line 569
    :pswitch_2
    const-string v3, "SIM\'s country"

    .line 570
    goto :goto_0

    .line 572
    :pswitch_3
    const-string v3, "Phone\'s locale"

    .line 573
    goto :goto_0

    .line 561
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public checkDoubleClickAntennaSetMode(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 818
    iget v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickAntennaSetModeflag:I

    if-ne p1, v0, :cond_0

    .line 819
    const/4 v0, 0x1

    .line 822
    :goto_0
    return v0

    .line 821
    :cond_0
    iput p1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickAntennaSetModeflag:I

    .line 822
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkDoubleClickCompatibilityTestMode(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 827
    iget v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    if-ne p1, v0, :cond_0

    .line 828
    const/4 v0, 0x1

    .line 831
    :goto_0
    return v0

    .line 830
    :cond_0
    iput p1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    .line 831
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkDoubleClickRFTestMode(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 809
    iget v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickRFTestModeflag:I

    if-ne p1, v0, :cond_0

    .line 810
    const/4 v0, 0x1

    .line 813
    :goto_0
    return v0

    .line 812
    :cond_0
    iput p1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickRFTestModeflag:I

    .line 813
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMacAddress()V
    .locals 7

    .prologue
    .line 603
    const/high16 v4, 0x7f050000

    invoke-virtual {p0, v4}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->tv:Landroid/widget/TextView;

    .line 604
    const-string v4, "wifi"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    iput-object v4, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 605
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    .line 606
    .local v2, "initialWifiState":Z
    if-nez v2, :cond_0

    .line 607
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 608
    const/4 v0, 0x0

    .line 610
    .local v0, "i":I
    :goto_0
    const-string v4, "WlanMac"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 612
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 615
    .end local v1    # "i":I
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    .line 616
    .local v3, "wifiInfo":Landroid/net/wifi/WifiInfo;
    const-string v4, "WlanMac"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "wifiInfo : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    if-nez v2, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 618
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->tv:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 619
    return-void

    .line 611
    .end local v3    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v1    # "i":I
    :catch_0
    move-exception v4

    goto :goto_1

    :cond_2
    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public getModuleType()V
    .locals 2

    .prologue
    .line 598
    const v0, 0x7f050005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->tv_module:Landroid/widget/TextView;

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->tv_module:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->moduleType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 600
    return-void
.end method

.method public handleWifiEvent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 636
    iget-boolean v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiRestartFlag:Z

    if-nez v3, :cond_1

    .line 656
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 639
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 640
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 641
    const-string v3, "wifi_state"

    const/4 v4, 0x4

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 642
    .local v1, "state":I
    const-string v3, "wifi"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 643
    .local v2, "wifiMgr":Landroid/net/wifi/WifiManager;
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 649
    :pswitch_1
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 650
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiRestartFlag:Z

    goto :goto_0

    .line 643
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public initCompatibilityView()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 690
    const/4 v2, 0x0

    .line 691
    .local v2, "in":Ljava/io/FileReader;
    const/4 v4, 0x0

    .line 693
    .local v4, "read":Ljava/io/BufferedReader;
    const-string v0, ""

    .line 695
    .local v0, "chk":Ljava/lang/String;
    const v6, 0x7f050007

    invoke-virtual {p0, v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    iput-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    .line 696
    const v6, 0x7f050008

    invoke-virtual {p0, v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    iput-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    .line 699
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    const-string v6, "/data/.frameburst.info"

    invoke-direct {v3, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 700
    .end local v2    # "in":Ljava/io/FileReader;
    .local v3, "in":Ljava/io/FileReader;
    :try_start_1
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 701
    .end local v4    # "read":Ljava/io/BufferedReader;
    .local v5, "read":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v0

    .line 705
    const-string v6, "WIFITTESEST"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "frameburst : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    const-string v6, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 708
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v6, v11}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 709
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v6, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 710
    iput v11, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    .line 720
    :goto_0
    if-eqz v3, :cond_8

    .line 722
    :try_start_3
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 723
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v4, v5

    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v4    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 730
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    new-instance v7, Lcom/sec/android/app/wlantest/WlanMacAddress$11;

    invoke-direct {v7, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$11;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 768
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    new-instance v7, Lcom/sec/android/app/wlantest/WlanMacAddress$12;

    invoke-direct {v7, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$12;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 805
    return-void

    .line 711
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v4    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    :cond_1
    const-string v6, "0"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 712
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v6, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 713
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v6, v11}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 714
    iput v12, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    goto :goto_0

    .line 716
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v6, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 717
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v6, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 724
    :catch_0
    move-exception v1

    .line 725
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "WlanMac"

    const-string v7, "File Close error"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v4    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 726
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto :goto_1

    .line 702
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 703
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 705
    const-string v6, "WIFITTESEST"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "frameburst : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    const-string v6, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 708
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v6, v11}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 709
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v6, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 710
    iput v11, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    .line 720
    :goto_3
    if-eqz v2, :cond_0

    .line 722
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 723
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 724
    :catch_2
    move-exception v1

    .line 725
    const-string v6, "WlanMac"

    const-string v7, "File Close error"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 711
    :cond_3
    const-string v6, "0"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 712
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v6, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 713
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v6, v11}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 714
    iput v12, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    goto :goto_3

    .line 716
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v6, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 717
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v6, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    .line 705
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_4
    const-string v7, "WIFITTESEST"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "frameburst : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    const-string v7, "1"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 708
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v7, v11}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 709
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v7, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 710
    iput v11, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    .line 720
    :goto_5
    if-eqz v2, :cond_5

    .line 722
    :try_start_6
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 723
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 726
    :cond_5
    :goto_6
    throw v6

    .line 711
    :cond_6
    const-string v7, "0"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 712
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v7, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 713
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v7, v11}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 714
    iput v12, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickCompatibilityTestModeflag:I

    goto :goto_5

    .line 716
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc1:Landroid/widget/RadioButton;

    invoke-virtual {v7, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 717
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->rc2:Landroid/widget/RadioButton;

    invoke-virtual {v7, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_5

    .line 724
    :catch_3
    move-exception v1

    .line 725
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 705
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto :goto_4

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v4    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v6

    move-object v4, v5

    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v4    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto :goto_4

    .line 702
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v4    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    :catch_5
    move-exception v1

    move-object v4, v5

    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v4    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v4    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    :cond_8
    move-object v4, v5

    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v4    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_1
.end method

.method public isWifiEnabled()Z
    .locals 3

    .prologue
    .line 622
    const-string v1, "wifi"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 623
    .local v0, "wifiMgr":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 624
    const/4 v1, 0x1

    .line 626
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    iput-object p0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->context:Landroid/content/Context;

    .line 88
    const-string v7, "WIFITEST"

    const-string v8, "on create"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const v7, 0x7f020001

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->setContentView(I)V

    .line 95
    const v7, 0x7f050003

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r1:Landroid/widget/RadioButton;

    .line 96
    const v7, 0x7f050004

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r2:Landroid/widget/RadioButton;

    .line 97
    const v7, 0x7f050009

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r6:Landroid/widget/Button;

    .line 98
    const v7, 0x7f05000c

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mRemoveCountry:Landroid/widget/Button;

    .line 99
    const v7, 0x7f05000d

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mSetContry:Landroid/widget/Button;

    .line 100
    const v7, 0x7f05000b

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mCountryEt:Landroid/widget/EditText;

    .line 101
    const v7, 0x7f05000a

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mCountrytx:Landroid/widget/TextView;

    .line 103
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "wifi_country_code"

    invoke-static {v7, v8}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->currentCountryCode:Ljava/lang/String;

    .line 105
    const-string v7, "WlanMac"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "currentCountryCode :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->currentCountryCode:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->currentCountryCode:Ljava/lang/String;

    if-nez v7, :cond_4

    .line 108
    const-string v7, "WlanMac"

    const-string v8, "currentCountryCode == null"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mCountrytx:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getCurrentCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    :goto_0
    const/4 v2, 0x0

    .line 117
    .local v2, "in":Ljava/io/FileReader;
    const/4 v5, 0x0

    .line 120
    .local v5, "read":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    const-string v7, "/data/.cid.info"

    invoke-direct {v3, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    .end local v2    # "in":Ljava/io/FileReader;
    .local v3, "in":Ljava/io/FileReader;
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_16
    .catchall {:try_start_1 .. :try_end_1} :catchall_a

    .line 122
    .end local v5    # "read":Ljava/io/BufferedReader;
    .local v6, "read":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->moduleType:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_17
    .catchall {:try_start_2 .. :try_end_2} :catchall_b

    .line 126
    if-eqz v3, :cond_11

    .line 128
    :try_start_3
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V

    .line 129
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 136
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    :cond_0
    :goto_1
    const/4 v2, 0x0

    .line 137
    const/4 v5, 0x0

    .line 140
    :try_start_4
    new-instance v3, Ljava/io/FileReader;

    const-string v7, "/data/.psm.info"

    invoke-direct {v3, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_14
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 141
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :try_start_5
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_15
    .catchall {:try_start_5 .. :try_end_5} :catchall_8

    .line 143
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :try_start_6
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "chk":Ljava/lang/String;
    const-string v7, "WIFITEST"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "power mode : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const-string v7, "0"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 148
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r1:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 149
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r2:Landroid/widget/RadioButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 150
    const/4 v7, 0x2

    iput v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickRFTestModeflag:I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_9

    .line 160
    :goto_2
    if-eqz v3, :cond_10

    .line 162
    :try_start_7
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V

    .line 163
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 169
    .end local v0    # "chk":Ljava/lang/String;
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    :cond_1
    :goto_3
    const/4 v2, 0x0

    .line 170
    const/4 v5, 0x0

    .line 172
    :try_start_8
    new-instance v3, Ljava/io/FileReader;

    const-string v7, "/efs/wifi/.mac.info"

    invoke-direct {v3, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_12
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 173
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :try_start_9
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_13
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 176
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :try_start_a
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 177
    .local v4, "mac":Ljava/lang/String;
    const-string v7, "WIFITEST"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "macaddress: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const-string v7, "00:12:3\\p{XDigit}:\\p{XDigit}\\p{XDigit}:\\p{XDigit}\\p{XDigit}:\\p{XDigit}\\p{XDigit}"

    invoke-virtual {v4, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 180
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r6:Landroid/widget/Button;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setEnabled(Z)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_7

    .line 188
    :goto_4
    if-eqz v3, :cond_f

    .line 190
    :try_start_b
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V

    .line 191
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 200
    .end local v3    # "in":Ljava/io/FileReader;
    .end local v4    # "mac":Ljava/lang/String;
    .restart local v2    # "in":Ljava/io/FileReader;
    :cond_2
    :goto_5
    const v7, 0x7f05000f

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r3:Landroid/widget/RadioButton;

    .line 201
    const v7, 0x7f050010

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r4:Landroid/widget/RadioButton;

    .line 202
    const v7, 0x7f050011

    invoke-virtual {p0, v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r5:Landroid/widget/RadioButton;

    .line 205
    :try_start_c
    new-instance v3, Ljava/io/FileReader;

    const-string v7, "/data/.ant.info"

    invoke-direct {v3, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_10
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 206
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :try_start_d
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_11
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 208
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :try_start_e
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 210
    .restart local v0    # "chk":Ljava/lang/String;
    const-string v7, "WIFITEST"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "set antenna : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const-string v7, "1"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 213
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r3:Landroid/widget/RadioButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 214
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r4:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 215
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r5:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 229
    :goto_6
    if-eqz v3, :cond_e

    .line 231
    :try_start_f
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V

    .line 232
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_e

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 241
    .end local v0    # "chk":Ljava/lang/String;
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    :cond_3
    :goto_7
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r1:Landroid/widget/RadioButton;

    new-instance v8, Lcom/sec/android/app/wlantest/WlanMacAddress$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$1;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 280
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r2:Landroid/widget/RadioButton;

    new-instance v8, Lcom/sec/android/app/wlantest/WlanMacAddress$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$2;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r3:Landroid/widget/RadioButton;

    new-instance v8, Lcom/sec/android/app/wlantest/WlanMacAddress$3;

    invoke-direct {v8, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$3;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 354
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r4:Landroid/widget/RadioButton;

    new-instance v8, Lcom/sec/android/app/wlantest/WlanMacAddress$4;

    invoke-direct {v8, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$4;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r5:Landroid/widget/RadioButton;

    new-instance v8, Lcom/sec/android/app/wlantest/WlanMacAddress$5;

    invoke-direct {v8, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$5;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r6:Landroid/widget/Button;

    new-instance v8, Lcom/sec/android/app/wlantest/WlanMacAddress$6;

    invoke-direct {v8, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$6;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 457
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mRemoveCountry:Landroid/widget/Button;

    new-instance v8, Lcom/sec/android/app/wlantest/WlanMacAddress$7;

    invoke-direct {v8, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$7;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 499
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mSetContry:Landroid/widget/Button;

    new-instance v8, Lcom/sec/android/app/wlantest/WlanMacAddress$8;

    invoke-direct {v8, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$8;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    invoke-virtual {p0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getMacAddress()V

    .line 538
    invoke-virtual {p0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getModuleType()V

    .line 540
    new-instance v7, Landroid/content/IntentFilter;

    const-string v8, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiFilter:Landroid/content/IntentFilter;

    .line 541
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiFilter:Landroid/content/IntentFilter;

    const-string v8, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 543
    new-instance v7, Lcom/sec/android/app/wlantest/WlanMacAddress$9;

    invoke-direct {v7, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$9;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    iput-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    .line 550
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    iget-object v8, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/wlantest/WlanMacAddress;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 552
    invoke-virtual {p0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->setNotiDialog()V

    .line 553
    return-void

    .line 110
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->currentCountryCode:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    .line 111
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mCountrytx:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Current Phone\'s locale : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->currentCountryCode:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (Country)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 113
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mCountrytx:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getCurrentCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 130
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 131
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 132
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_1

    .line 123
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 124
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_8
    :try_start_10
    const-string v7, "WIFITEST"

    const-string v8, "ModuleType : COB"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 126
    if-eqz v2, :cond_0

    .line 128
    :try_start_11
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 129
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_2

    goto/16 :goto_1

    .line 130
    :catch_2
    move-exception v1

    .line 131
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 126
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_9
    if-eqz v2, :cond_6

    .line 128
    :try_start_12
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 129
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_3

    .line 132
    :cond_6
    :goto_a
    throw v7

    .line 130
    :catch_3
    move-exception v1

    .line 131
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v8, "WlanMac"

    const-string v9, "File Close error"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 152
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v0    # "chk":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :cond_7
    :try_start_13
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r1:Landroid/widget/RadioButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 153
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r2:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 154
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->doubleclickRFTestModeflag:I
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_4
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    goto/16 :goto_2

    .line 156
    .end local v0    # "chk":Ljava/lang/String;
    :catch_4
    move-exception v1

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 157
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "in":Ljava/io/FileReader;
    :goto_b
    :try_start_14
    const-string v7, "WlanMac"

    const-string v8, "File open error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 160
    if-eqz v2, :cond_1

    .line 162
    :try_start_15
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 163
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_5

    goto/16 :goto_3

    .line 164
    :catch_5
    move-exception v1

    .line 165
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 164
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v0    # "chk":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :catch_6
    move-exception v1

    .line 165
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 166
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_3

    .line 160
    .end local v0    # "chk":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v7

    :goto_c
    if-eqz v2, :cond_8

    .line 162
    :try_start_16
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 163
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_7

    .line 166
    :cond_8
    :goto_d
    throw v7

    .line 164
    :catch_7
    move-exception v1

    .line 165
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v8, "WlanMac"

    const-string v9, "File Close error"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 182
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v4    # "mac":Ljava/lang/String;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :cond_9
    :try_start_17
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r6:Landroid/widget/Button;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setEnabled(Z)V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_8
    .catchall {:try_start_17 .. :try_end_17} :catchall_7

    goto/16 :goto_4

    .line 184
    .end local v4    # "mac":Ljava/lang/String;
    :catch_8
    move-exception v1

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 185
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "in":Ljava/io/FileReader;
    :goto_e
    :try_start_18
    const-string v7, "WlanMac"

    const-string v8, "File open error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    .line 188
    if-eqz v2, :cond_2

    .line 190
    :try_start_19
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 191
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_9

    goto/16 :goto_5

    .line 192
    :catch_9
    move-exception v1

    .line 193
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 192
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v4    # "mac":Ljava/lang/String;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :catch_a
    move-exception v1

    .line 193
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 194
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_5

    .line 188
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "mac":Ljava/lang/String;
    :catchall_2
    move-exception v7

    :goto_f
    if-eqz v2, :cond_a

    .line 190
    :try_start_1a
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 191
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_b

    .line 194
    :cond_a
    :goto_10
    throw v7

    .line 192
    :catch_b
    move-exception v1

    .line 193
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v8, "WlanMac"

    const-string v9, "File Close error"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_10

    .line 216
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v0    # "chk":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :cond_b
    :try_start_1b
    const-string v7, "2"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 217
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r3:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 218
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r4:Landroid/widget/RadioButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 219
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r5:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_c
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    goto/16 :goto_6

    .line 225
    .end local v0    # "chk":Ljava/lang/String;
    :catch_c
    move-exception v1

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 226
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "in":Ljava/io/FileReader;
    :goto_11
    :try_start_1c
    const-string v7, "WlanMac"

    const-string v8, "File open error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_4

    .line 229
    if-eqz v2, :cond_3

    .line 231
    :try_start_1d
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 232
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_d

    goto/16 :goto_7

    .line 233
    :catch_d
    move-exception v1

    .line 234
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 221
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v0    # "chk":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :cond_c
    :try_start_1e
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r3:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 222
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r4:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 223
    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->r5:Landroid/widget/RadioButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_c
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    goto/16 :goto_6

    .line 229
    .end local v0    # "chk":Ljava/lang/String;
    :catchall_3
    move-exception v7

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    :goto_12
    if-eqz v2, :cond_d

    .line 231
    :try_start_1f
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 232
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_f

    .line 235
    :cond_d
    :goto_13
    throw v7

    .line 233
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v0    # "chk":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :catch_e
    move-exception v1

    .line 234
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "WlanMac"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .line 235
    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_7

    .line 233
    .end local v0    # "chk":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v1

    .line 234
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v8, "WlanMac"

    const-string v9, "File Close error"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_13

    .line 229
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_4
    move-exception v7

    goto :goto_12

    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catchall_5
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto :goto_12

    .line 225
    :catch_10
    move-exception v1

    goto :goto_11

    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catch_11
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto :goto_11

    .line 188
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catchall_6
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_f

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :catchall_7
    move-exception v7

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_f

    .line 184
    :catch_12
    move-exception v1

    goto/16 :goto_e

    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catch_13
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_e

    .line 160
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catchall_8
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_c

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :catchall_9
    move-exception v7

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_c

    .line 156
    :catch_14
    move-exception v1

    goto/16 :goto_b

    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catch_15
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_b

    .line 126
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catchall_a
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_9

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :catchall_b
    move-exception v7

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_9

    .line 123
    .end local v2    # "in":Ljava/io/FileReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    :catch_16
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_8

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :catch_17
    move-exception v1

    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_8

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v0    # "chk":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :cond_e
    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_7

    .end local v0    # "chk":Ljava/lang/String;
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v4    # "mac":Ljava/lang/String;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :cond_f
    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_5

    .end local v2    # "in":Ljava/io/FileReader;
    .end local v4    # "mac":Ljava/lang/String;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v0    # "chk":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :cond_10
    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_3

    .end local v0    # "chk":Ljava/lang/String;
    .end local v2    # "in":Ljava/io/FileReader;
    .end local v5    # "read":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/FileReader;
    .restart local v6    # "read":Ljava/io/BufferedReader;
    :cond_11
    move-object v5, v6

    .end local v6    # "read":Ljava/io/BufferedReader;
    .restart local v5    # "read":Ljava/io/BufferedReader;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileReader;
    .restart local v2    # "in":Ljava/io/FileReader;
    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x0

    .line 668
    const-string v0, "Compatibility Test Mode"

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 669
    const v0, 0x7f050006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->compatibility_layout:Landroid/widget/LinearLayout;

    .line 672
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 661
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 662
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 678
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 685
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 681
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->compatibility_layout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 682
    invoke-virtual {p0}, Lcom/sec/android/app/wlantest/WlanMacAddress;->initCompatibilityView()V

    goto :goto_0

    .line 678
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setNotiDialog()V
    .locals 3

    .prologue
    .line 582
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->builder:Landroid/app/AlertDialog$Builder;

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "ATTENTION: Changed values will be applied after rebooting the device."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "Reboot"

    new-instance v2, Lcom/sec/android/app/wlantest/WlanMacAddress$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wlantest/WlanMacAddress$10;-><init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "Later"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 595
    return-void
.end method

.method public stopWifi()V
    .locals 2

    .prologue
    .line 630
    const-string v1, "wifi"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wlantest/WlanMacAddress;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 631
    .local v0, "wifiMgr":Landroid/net/wifi/WifiManager;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiRestartFlag:Z

    .line 632
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 633
    return-void
.end method
