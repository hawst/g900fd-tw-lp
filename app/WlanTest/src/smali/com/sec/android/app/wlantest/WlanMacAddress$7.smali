.class Lcom/sec/android/app/wlantest/WlanMacAddress$7;
.super Ljava/lang/Object;
.source "WlanMacAddress.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wlantest/WlanMacAddress;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 459
    const-string v6, "WlanMac"

    const-string v7, "........Click the Remove Country Code....."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v6, v6, Lcom/sec/android/app/wlantest/WlanMacAddress;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "airplane_mode_on"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 463
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 464
    .local v3, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/wlantest/WlanMacAddress;->sendBroadcast(Landroid/content/Intent;)V

    .line 466
    const-wide/16 v6, 0x3e8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 468
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v6, v6, Lcom/sec/android/app/wlantest/WlanMacAddress;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "airplane_mode_on"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 470
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/wlantest/WlanMacAddress;->sendBroadcast(Landroid/content/Intent;)V

    .line 472
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v6, v6, Lcom/sec/android/app/wlantest/WlanMacAddress;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "airplane_mode_on"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v5, :cond_0

    move v4, v5

    .line 474
    .local v4, "isEnabled":Z
    :cond_0
    if-eqz v4, :cond_1

    .line 475
    const-wide/16 v6, 0x1f4

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 478
    :cond_1
    :goto_1
    const-string v6, "WlanMac"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setCountryCode end 11111 : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    .line 481
    .local v2, "initialWifiState":Z
    if-nez v2, :cond_2

    .line 482
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 483
    const/4 v0, 0x0

    .line 485
    .local v0, "i":I
    :goto_2
    const-string v6, "WlanMac"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    const-wide/16 v6, 0x64

    :try_start_2
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 487
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # getter for: Lcom/sec/android/app/wlantest/WlanMacAddress;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v6}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$100(Lcom/sec/android/app/wlantest/WlanMacAddress;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 490
    .end local v1    # "i":I
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v6, v6, Lcom/sec/android/app/wlantest/WlanMacAddress;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "wifi_country_code"

    const-string v8, ""

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 492
    const-wide/16 v6, 0x3e8

    :try_start_3
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3

    .line 494
    :goto_4
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v6, v6, Lcom/sec/android/app/wlantest/WlanMacAddress;->mCountrytx:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    # invokes: Lcom/sec/android/app/wlantest/WlanMacAddress;->getCurrentCountry()Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/wlantest/WlanMacAddress;->access$200(Lcom/sec/android/app/wlantest/WlanMacAddress;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    iget-object v6, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$7;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    const-string v7, "Country Code Change complete"

    invoke-static {v6, v7, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 496
    return-void

    .line 466
    .end local v2    # "initialWifiState":Z
    .end local v4    # "isEnabled":Z
    :catch_0
    move-exception v6

    goto/16 :goto_0

    .line 475
    .restart local v4    # "isEnabled":Z
    :catch_1
    move-exception v6

    goto/16 :goto_1

    .line 486
    .restart local v1    # "i":I
    .restart local v2    # "initialWifiState":Z
    :catch_2
    move-exception v6

    goto :goto_3

    .line 492
    .end local v1    # "i":I
    :catch_3
    move-exception v6

    goto :goto_4

    .restart local v1    # "i":I
    :cond_3
    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_2
.end method
