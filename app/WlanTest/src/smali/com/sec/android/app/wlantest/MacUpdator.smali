.class public Lcom/sec/android/app/wlantest/MacUpdator;
.super Landroid/content/BroadcastReceiver;
.source "MacUpdator.java"


# instance fields
.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 33
    return-void
.end method

.method private validateMacAddress(Ljava/lang/String;)Z
    .locals 5
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 165
    if-nez p1, :cond_0

    .line 166
    const-string v2, "MacUpdator"

    const-string v3, "MAC is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :goto_0
    return v1

    .line 170
    :cond_0
    :try_start_0
    const-string v2, "\\p{XDigit}\\p{XDigit}:\\p{XDigit}\\p{XDigit}:\\p{XDigit}\\p{XDigit}:\\p{XDigit}\\p{XDigit}:\\p{XDigit}\\p{XDigit}:\\p{XDigit}\\p{XDigit}"

    invoke-virtual {p1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 171
    const-string v2, "MacUpdator"

    const-string v3, "MAC data is valid"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    const/4 v1, 0x1

    goto :goto_0

    .line 174
    :cond_1
    const-string v2, "MacUpdator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MAC data is invalid as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/util/regex/PatternSyntaxException;
    const-string v2, "MacUpdator"

    const-string v3, "The regular expression\'s syntax is invalid"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 38
    const-string v20, "wifi"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/net/wifi/WifiManager;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/wlantest/MacUpdator;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 39
    const-string v20, "MacUpdator"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Wifi is on : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/wlantest/MacUpdator;->mWifiManager:Landroid/net/wifi/WifiManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    const/4 v14, 0x0

    .line 42
    .local v14, "output":Ljava/io/BufferedWriter;
    const/16 v16, 0x0

    .line 44
    .local v16, "outputBackup":Ljava/io/BufferedWriter;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v20

    const-string v21, "com.sec.android.app.wlantest.WIFI_ID_WRITE"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 45
    const-string v20, "MAC_DATA"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 46
    .local v12, "macdata":Ljava/lang/String;
    const-string v20, "MacUpdator"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "new macdata is "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const/4 v13, 0x0

    .line 49
    .local v13, "noErrors":Z
    new-instance v19, Landroid/content/Intent;

    invoke-direct/range {v19 .. v19}, Landroid/content/Intent;-><init>()V

    .line 50
    .local v19, "respAction":Landroid/content/Intent;
    const-string v20, "com.sec.android.app.wlantest.WIFI_ID_RESPONSE"

    invoke-virtual/range {v19 .. v20}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/wlantest/MacUpdator;->validateMacAddress(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 54
    if-eqz v12, :cond_0

    .line 55
    :try_start_0
    new-instance v10, Ljava/io/FileOutputStream;

    const-string v20, "/efs/wifi/.mac.info"

    move-object/from16 v0, v20

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 56
    .local v10, "fos":Ljava/io/FileOutputStream;
    new-instance v11, Ljava/io/FileOutputStream;

    const-string v20, "/efs/wifi/.mac.cob"

    move-object/from16 v0, v20

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 57
    .local v11, "fosBackup":Ljava/io/FileOutputStream;
    new-instance v15, Ljava/io/BufferedWriter;

    new-instance v20, Ljava/io/FileWriter;

    invoke-virtual {v10}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/io/FileWriter;-><init>(Ljava/io/FileDescriptor;)V

    move-object/from16 v0, v20

    invoke-direct {v15, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    .end local v14    # "output":Ljava/io/BufferedWriter;
    .local v15, "output":Ljava/io/BufferedWriter;
    :try_start_1
    new-instance v17, Ljava/io/BufferedWriter;

    new-instance v20, Ljava/io/FileWriter;

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/io/FileWriter;-><init>(Ljava/io/FileDescriptor;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 59
    .end local v16    # "outputBackup":Ljava/io/BufferedWriter;
    .local v17, "outputBackup":Ljava/io/BufferedWriter;
    :try_start_2
    invoke-virtual {v15, v12}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v15}, Ljava/io/BufferedWriter;->flush()V

    .line 61
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/io/FileDescriptor;->sync()V

    .line 62
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 63
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedWriter;->flush()V

    .line 64
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/io/FileDescriptor;->sync()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_d
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    move-object/from16 v16, v17

    .end local v17    # "outputBackup":Ljava/io/BufferedWriter;
    .restart local v16    # "outputBackup":Ljava/io/BufferedWriter;
    move-object v14, v15

    .line 67
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "fosBackup":Ljava/io/FileOutputStream;
    .end local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v14    # "output":Ljava/io/BufferedWriter;
    :cond_0
    const/4 v13, 0x1

    .line 82
    if-eqz v14, :cond_1

    .line 83
    :try_start_3
    invoke-virtual {v14}, Ljava/io/BufferedWriter;->close()V

    .line 85
    :cond_1
    if-eqz v16, :cond_2

    .line 86
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 94
    :cond_2
    :goto_0
    const-string v21, "S_DATA"

    if-eqz v13, :cond_8

    const-string v20, "OK"

    :goto_1
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 96
    const-string v20, "MacUpdator"

    const-string v21, "Sent response intent"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    .end local v12    # "macdata":Ljava/lang/String;
    .end local v13    # "noErrors":Z
    .end local v19    # "respAction":Landroid/content/Intent;
    :cond_3
    :goto_2
    return-void

    .line 88
    .restart local v12    # "macdata":Ljava/lang/String;
    .restart local v13    # "noErrors":Z
    .restart local v19    # "respAction":Landroid/content/Intent;
    :catch_0
    move-exception v7

    .line 89
    .local v7, "e2":Ljava/io/IOException;
    const-string v20, "MacUpdator"

    const-string v21, "File Close error"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 76
    .end local v7    # "e2":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 77
    .local v5, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_4
    const-string v20, "MacUpdator"

    const-string v21, "/efs/wifi/.mac.info doesn\'t exist or there are something wrong on handling it"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 82
    if-eqz v14, :cond_4

    .line 83
    :try_start_5
    invoke-virtual {v14}, Ljava/io/BufferedWriter;->close()V

    .line 85
    :cond_4
    if-eqz v16, :cond_2

    .line 86
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 88
    :catch_2
    move-exception v7

    .line 89
    .restart local v7    # "e2":Ljava/io/IOException;
    const-string v20, "MacUpdator"

    const-string v21, "File Close error"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 78
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    .end local v7    # "e2":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 79
    .local v5, "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 82
    if-eqz v14, :cond_5

    .line 83
    :try_start_7
    invoke-virtual {v14}, Ljava/io/BufferedWriter;->close()V

    .line 85
    :cond_5
    if-eqz v16, :cond_2

    .line 86
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 88
    :catch_4
    move-exception v7

    .line 89
    .restart local v7    # "e2":Ljava/io/IOException;
    const-string v20, "MacUpdator"

    const-string v21, "File Close error"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 81
    .end local v5    # "e":Ljava/io/IOException;
    .end local v7    # "e2":Ljava/io/IOException;
    :catchall_0
    move-exception v20

    .line 82
    :goto_5
    if-eqz v14, :cond_6

    .line 83
    :try_start_8
    invoke-virtual {v14}, Ljava/io/BufferedWriter;->close()V

    .line 85
    :cond_6
    if-eqz v16, :cond_7

    .line 86
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 90
    :cond_7
    :goto_6
    throw v20

    .line 88
    :catch_5
    move-exception v7

    .line 89
    .restart local v7    # "e2":Ljava/io/IOException;
    const-string v21, "MacUpdator"

    const-string v22, "File Close error"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 94
    .end local v7    # "e2":Ljava/io/IOException;
    :cond_8
    const-string v20, "NG"

    goto :goto_1

    .line 97
    .end local v12    # "macdata":Ljava/lang/String;
    .end local v13    # "noErrors":Z
    .end local v19    # "respAction":Landroid/content/Intent;
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v20

    const-string v21, "com.sec.android.app.wlantest.WIFI_ID_READ"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 98
    const-string v20, "MacUpdator"

    const-string v21, "read MAC from /efs/wifi/.mac.info"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v8, 0x0

    .line 101
    .local v8, "efsReader":Ljava/io/FileReader;
    const/4 v3, 0x0

    .line 102
    .local v3, "br":Ljava/io/BufferedReader;
    const/16 v18, 0x0

    .line 104
    .local v18, "readmac":Ljava/lang/String;
    const/4 v13, 0x0

    .line 105
    .restart local v13    # "noErrors":Z
    new-instance v19, Landroid/content/Intent;

    invoke-direct/range {v19 .. v19}, Landroid/content/Intent;-><init>()V

    .line 106
    .restart local v19    # "respAction":Landroid/content/Intent;
    const-string v20, "com.sec.android.app.wlantest.WIFI_ID_RESPONSE"

    invoke-virtual/range {v19 .. v20}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    :try_start_9
    new-instance v9, Ljava/io/FileReader;

    const-string v20, "/efs/wifi/.mac.info"

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_a
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 110
    .end local v8    # "efsReader":Ljava/io/FileReader;
    .local v9, "efsReader":Ljava/io/FileReader;
    :try_start_a
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_b
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 112
    .end local v3    # "br":Ljava/io/BufferedReader;
    .local v4, "br":Ljava/io/BufferedReader;
    :try_start_b
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v18

    .line 113
    if-eqz v18, :cond_e

    .line 114
    const-string v20, "MacUpdator"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "readmac is "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const-string v20, "MacUpdator"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "readmacUpperCase is "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/wlantest/MacUpdator;->validateMacAddress(Ljava/lang/String;)Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    move-result v20

    if-eqz v20, :cond_a

    .line 117
    const/4 v13, 0x1

    .line 127
    :cond_a
    :goto_7
    if-eqz v4, :cond_b

    .line 128
    :try_start_c
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 130
    :cond_b
    if-eqz v9, :cond_c

    .line 131
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    :cond_c
    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    move-object v8, v9

    .line 138
    .end local v9    # "efsReader":Ljava/io/FileReader;
    .restart local v8    # "efsReader":Ljava/io/FileReader;
    :cond_d
    :goto_8
    const-string v21, "S_DATA"

    if-eqz v13, :cond_12

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v20

    :goto_9
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 140
    const-string v20, "MacUpdator"

    const-string v21, "Sent response intent"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 120
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v8    # "efsReader":Ljava/io/FileReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "efsReader":Ljava/io/FileReader;
    :cond_e
    :try_start_d
    const-string v20, "MacUpdator"

    const-string v21, "readmac is null .. !"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_7

    .line 122
    :catch_6
    move-exception v6

    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    move-object v8, v9

    .line 123
    .end local v9    # "efsReader":Ljava/io/FileReader;
    .local v6, "e1":Ljava/io/IOException;
    .restart local v8    # "efsReader":Ljava/io/FileReader;
    :goto_a
    :try_start_e
    const-string v20, "MacUpdator"

    const-string v21, "/efs/wifi/.mac.info doesn\'t exist or there are something wrong on handling it"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 127
    if-eqz v3, :cond_f

    .line 128
    :try_start_f
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 130
    :cond_f
    if-eqz v8, :cond_d

    .line 131
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7

    goto :goto_8

    .line 133
    :catch_7
    move-exception v7

    .line 134
    .restart local v7    # "e2":Ljava/io/IOException;
    const-string v20, "MacUpdator"

    const-string v21, "File Close error"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 133
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v6    # "e1":Ljava/io/IOException;
    .end local v7    # "e2":Ljava/io/IOException;
    .end local v8    # "efsReader":Ljava/io/FileReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "efsReader":Ljava/io/FileReader;
    :catch_8
    move-exception v7

    .line 134
    .restart local v7    # "e2":Ljava/io/IOException;
    const-string v20, "MacUpdator"

    const-string v21, "File Close error"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    move-object v8, v9

    .line 136
    .end local v9    # "efsReader":Ljava/io/FileReader;
    .restart local v8    # "efsReader":Ljava/io/FileReader;
    goto :goto_8

    .line 126
    .end local v7    # "e2":Ljava/io/IOException;
    :catchall_1
    move-exception v20

    .line 127
    :goto_b
    if-eqz v3, :cond_10

    .line 128
    :try_start_10
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 130
    :cond_10
    if-eqz v8, :cond_11

    .line 131
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9

    .line 135
    :cond_11
    :goto_c
    throw v20

    .line 133
    :catch_9
    move-exception v7

    .line 134
    .restart local v7    # "e2":Ljava/io/IOException;
    const-string v21, "MacUpdator"

    const-string v22, "File Close error"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 138
    .end local v7    # "e2":Ljava/io/IOException;
    :cond_12
    const-string v20, "NG"

    goto :goto_9

    .line 126
    .end local v8    # "efsReader":Ljava/io/FileReader;
    .restart local v9    # "efsReader":Ljava/io/FileReader;
    :catchall_2
    move-exception v20

    move-object v8, v9

    .end local v9    # "efsReader":Ljava/io/FileReader;
    .restart local v8    # "efsReader":Ljava/io/FileReader;
    goto :goto_b

    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v8    # "efsReader":Ljava/io/FileReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "efsReader":Ljava/io/FileReader;
    :catchall_3
    move-exception v20

    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    move-object v8, v9

    .end local v9    # "efsReader":Ljava/io/FileReader;
    .restart local v8    # "efsReader":Ljava/io/FileReader;
    goto :goto_b

    .line 122
    :catch_a
    move-exception v6

    goto :goto_a

    .end local v8    # "efsReader":Ljava/io/FileReader;
    .restart local v9    # "efsReader":Ljava/io/FileReader;
    :catch_b
    move-exception v6

    move-object v8, v9

    .end local v9    # "efsReader":Ljava/io/FileReader;
    .restart local v8    # "efsReader":Ljava/io/FileReader;
    goto :goto_a

    .line 81
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v8    # "efsReader":Ljava/io/FileReader;
    .end local v14    # "output":Ljava/io/BufferedWriter;
    .end local v18    # "readmac":Ljava/lang/String;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fosBackup":Ljava/io/FileOutputStream;
    .restart local v12    # "macdata":Ljava/lang/String;
    .restart local v15    # "output":Ljava/io/BufferedWriter;
    :catchall_4
    move-exception v20

    move-object v14, v15

    .end local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v14    # "output":Ljava/io/BufferedWriter;
    goto/16 :goto_5

    .end local v14    # "output":Ljava/io/BufferedWriter;
    .end local v16    # "outputBackup":Ljava/io/BufferedWriter;
    .restart local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v17    # "outputBackup":Ljava/io/BufferedWriter;
    :catchall_5
    move-exception v20

    move-object/from16 v16, v17

    .end local v17    # "outputBackup":Ljava/io/BufferedWriter;
    .restart local v16    # "outputBackup":Ljava/io/BufferedWriter;
    move-object v14, v15

    .end local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v14    # "output":Ljava/io/BufferedWriter;
    goto/16 :goto_5

    .line 78
    .end local v14    # "output":Ljava/io/BufferedWriter;
    .restart local v15    # "output":Ljava/io/BufferedWriter;
    :catch_c
    move-exception v5

    move-object v14, v15

    .end local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v14    # "output":Ljava/io/BufferedWriter;
    goto/16 :goto_4

    .end local v14    # "output":Ljava/io/BufferedWriter;
    .end local v16    # "outputBackup":Ljava/io/BufferedWriter;
    .restart local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v17    # "outputBackup":Ljava/io/BufferedWriter;
    :catch_d
    move-exception v5

    move-object/from16 v16, v17

    .end local v17    # "outputBackup":Ljava/io/BufferedWriter;
    .restart local v16    # "outputBackup":Ljava/io/BufferedWriter;
    move-object v14, v15

    .end local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v14    # "output":Ljava/io/BufferedWriter;
    goto/16 :goto_4

    .line 76
    .end local v14    # "output":Ljava/io/BufferedWriter;
    .restart local v15    # "output":Ljava/io/BufferedWriter;
    :catch_e
    move-exception v5

    move-object v14, v15

    .end local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v14    # "output":Ljava/io/BufferedWriter;
    goto/16 :goto_3

    .end local v14    # "output":Ljava/io/BufferedWriter;
    .end local v16    # "outputBackup":Ljava/io/BufferedWriter;
    .restart local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v17    # "outputBackup":Ljava/io/BufferedWriter;
    :catch_f
    move-exception v5

    move-object/from16 v16, v17

    .end local v17    # "outputBackup":Ljava/io/BufferedWriter;
    .restart local v16    # "outputBackup":Ljava/io/BufferedWriter;
    move-object v14, v15

    .end local v15    # "output":Ljava/io/BufferedWriter;
    .restart local v14    # "output":Ljava/io/BufferedWriter;
    goto/16 :goto_3
.end method
