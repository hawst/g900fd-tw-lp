.class Lcom/sec/android/app/wlantest/WlanMacAddress$5;
.super Ljava/lang/Object;
.source "WlanMacAddress.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wlantest/WlanMacAddress;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wlantest/WlanMacAddress;)V
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$5;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 391
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$5;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/sec/android/app/wlantest/WlanMacAddress;->checkDoubleClickAntennaSetMode(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 419
    :goto_0
    return-void

    .line 394
    :cond_0
    const-string v3, "WlanMac"

    const-string v4, "  ........Set Antenna_1 !!......"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$5;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanMacAddress;->r3:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 396
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$5;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanMacAddress;->r4:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 397
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$5;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanMacAddress;->r5:Landroid/widget/RadioButton;

    invoke-virtual {v3, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 399
    const/4 v1, 0x0

    .line 401
    .local v1, "out":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    const-string v3, "/data/.ant.info"

    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    .end local v1    # "out":Ljava/io/FileWriter;
    .local v2, "out":Ljava/io/FileWriter;
    const/16 v3, 0x33

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(I)V

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$5;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    const-string v4, "Antenna_ALL is set"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 409
    if-eqz v2, :cond_3

    .line 411
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 418
    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/wlantest/WlanMacAddress$5;->this$0:Lcom/sec/android/app/wlantest/WlanMacAddress;

    iget-object v3, v3, Lcom/sec/android/app/wlantest/WlanMacAddress;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 412
    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "WlanMac"

    const-string v4, "File Close error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 414
    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_1

    .line 404
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 405
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v3, "WlanMac"

    const-string v4, "File open error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 409
    if-eqz v1, :cond_1

    .line 411
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 412
    :catch_2
    move-exception v0

    .line 413
    const-string v3, "WlanMac"

    const-string v4, "File Close error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 409
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_3
    if-eqz v1, :cond_2

    .line 411
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 414
    :cond_2
    :goto_4
    throw v3

    .line 412
    :catch_3
    move-exception v0

    .line 413
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "WlanMac"

    const-string v5, "File Close error"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 409
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_3

    .line 404
    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_2

    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :cond_3
    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_1
.end method
