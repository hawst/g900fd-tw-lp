.class Lcom/infraware/polarisoffice5/OfficeLauncherActivity$4;
.super Ljava/lang/Object;
.source "OfficeLauncherActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->checkMultipleLaunch(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/OfficeLauncherActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)V
    .locals 0

    .prologue
    .line 722
    iput-object p1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$4;->this$0:Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    .line 724
    packed-switch p2, :pswitch_data_0

    .line 730
    :goto_0
    return v1

    .line 726
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$4;->this$0:Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->closeLauncher()V

    .line 727
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$4;->this$0:Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

    # getter for: Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_strTxtOpenFilePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->access$000(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->removeOpenfileList(Ljava/lang/String;)V

    goto :goto_0

    .line 724
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
