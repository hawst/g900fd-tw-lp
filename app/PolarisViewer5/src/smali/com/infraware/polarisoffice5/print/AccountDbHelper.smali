.class public Lcom/infraware/polarisoffice5/print/AccountDbHelper;
.super Ljava/lang/Object;
.source "AccountDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/print/AccountDbHelper$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "cloudaccount.db"

.field private static final DATABASE_VERSION:I = 0x1


# instance fields
.field private mCtx:Landroid/content/Context;

.field public mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mDBHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper$DatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mCtx:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 52
    return-void
.end method

.method public deleteColumn(J)Z
    .locals 4
    .param p1, "id"    # J

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v1, "Account"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteColumn(Ljava/lang/String;)Z
    .locals 4
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v1, "Account"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "AccountId=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAccountIdColumns(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "accountId"    # Ljava/lang/String;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "select * from Account where AccountId=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getAllColumns()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v1, "Account"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public insertColumn(Ljava/lang/String;Ljava/lang/String;)J
    .locals 4
    .param p1, "accountid"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;

    .prologue
    .line 56
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 57
    .local v0, "values":Landroid/content/ContentValues;
    const-string/jumbo v1, "AccountId"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string/jumbo v1, "AccountToken"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v2, "Account"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    return-wide v1
.end method

.method public open()Lcom/infraware/polarisoffice5/print/AccountDbHelper;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lcom/infraware/polarisoffice5/print/AccountDbHelper$DatabaseHelper;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mCtx:Landroid/content/Context;

    const-string/jumbo v3, "cloudaccount.db"

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/print/AccountDbHelper$DatabaseHelper;-><init>(Lcom/infraware/polarisoffice5/print/AccountDbHelper;Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDBHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper$DatabaseHelper;

    .line 46
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDBHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper$DatabaseHelper;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/AccountDbHelper$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 47
    return-object p0
.end method

.method public updateColumn(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "id"    # J
    .param p3, "accountid"    # Ljava/lang/String;
    .param p4, "token"    # Ljava/lang/String;

    .prologue
    .line 64
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 65
    .local v0, "values":Landroid/content/ContentValues;
    const-string/jumbo v1, "AccountId"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string/jumbo v1, "AccountToken"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v2, "Account"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
