.class Lcom/infraware/polarisoffice5/print/PrintActivity$14;
.super Landroid/os/Handler;
.source "PrintActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;->initSubmitHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V
    .locals 0

    .prologue
    .line 1146
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$14;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1149
    iget v1, p1, Landroid/os/Message;->what:I

    .line 1152
    .local v1, "id":I
    const/16 v2, 0x3ea

    if-ne v1, v2, :cond_0

    .line 1154
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$14;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const/16 v4, 0x1a

    invoke-virtual {v2, v3, v4}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    .line 1155
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$14;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # invokes: Lcom/infraware/polarisoffice5/print/PrintActivity;->hidePrintOptionLayout()V
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2700(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    .line 1157
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$14;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mstrPdfExportFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2800(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$14;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mstrPdfExportFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2800(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1159
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$14;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mstrPdfExportFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2800(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1160
    .local v0, "delfile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1161
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1164
    .end local v0    # "delfile":Ljava/io/File;
    :cond_0
    return-void
.end method
