.class public Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;
.super Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;
.source "GooglePdfPrintAdapter.java"


# instance fields
.field private mfilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 0
    .param p1, "DocType"    # I
    .param p2, "DocumentName"    # Ljava/lang/String;
    .param p3, "filePath"    # Ljava/lang/String;
    .param p4, "activity"    # Landroid/app/Activity;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p4}, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;-><init>(ILjava/lang/String;Landroid/app/Activity;)V

    .line 23
    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mfilePath:Ljava/lang/String;

    .line 24
    return-void
.end method

.method private makeAllPdfPrintPdf()V
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 113
    iput-boolean v9, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->m_isPossibleMakePdf:Z

    .line 114
    const/4 v3, 0x0

    .line 115
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 117
    .local v5, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mfilePath:Ljava/lang/String;

    invoke-direct {v4, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    sget-object v9, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v9

    invoke-direct {v6, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 120
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .local v6, "fos":Ljava/io/FileOutputStream;
    const v9, 0x8000

    :try_start_2
    new-array v0, v9, [B

    .line 121
    .local v0, "buffer":[B
    const/4 v8, 0x0

    .line 122
    .local v8, "readCount":I
    const/4 v7, 0x0

    .line 124
    .local v7, "loopCount":I
    :goto_0
    const/16 v9, 0x20

    if-ne v7, v9, :cond_1

    .line 125
    const/4 v7, 0x0

    .line 126
    sget-object v9, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v9}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 127
    sget-object v9, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    invoke-virtual {v9}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    .line 138
    :cond_0
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 139
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 140
    sget-object v9, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    iget-object v10, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mPages:[Landroid/print/PageRange;

    invoke-virtual {v9, v10}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V

    move-object v5, v6

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 156
    .end local v0    # "buffer":[B
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "loopCount":I
    .end local v8    # "readCount":I
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :goto_1
    return-void

    .line 132
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "loopCount":I
    .restart local v8    # "readCount":I
    :cond_1
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v8

    .line 133
    if-lez v8, :cond_0

    .line 135
    const/4 v9, 0x0

    invoke-virtual {v6, v0, v9, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 136
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 141
    .end local v0    # "buffer":[B
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "loopCount":I
    .end local v8    # "readCount":I
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 143
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    if-eqz v3, :cond_2

    .line 144
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 145
    :cond_2
    if-eqz v5, :cond_3

    .line 146
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 152
    :cond_3
    :goto_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 153
    sget-object v9, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    iget-object v10, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mActivity:Landroid/app/Activity;

    const v11, 0x7f0701bb

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 148
    :catch_1
    move-exception v2

    .line 149
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 141
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    move-object v5, v6

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method


# virtual methods
.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 10
    .param p1, "pages"    # [Landroid/print/PageRange;
    .param p2, "destination"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 31
    sput-object p4, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .line 32
    sput-object p2, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 33
    sput-object p3, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 34
    sget-object v6, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->MyOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

    invoke-virtual {v6, v7}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 35
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mPages:[Landroid/print/PageRange;

    .line 46
    iget v5, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mTotalPageCount:I

    .line 49
    .local v5, "nPrintPageCount":I
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 50
    .local v2, "isAllPagePrint":Ljava/lang/Boolean;
    if-eqz p1, :cond_1

    aget-object v6, p1, v8

    if-eqz v6, :cond_0

    aget-object v6, p1, v8

    if-eqz v6, :cond_1

    aget-object v6, p1, v8

    sget-object v7, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    invoke-virtual {v6, v7}, Landroid/print/PageRange;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 51
    :cond_0
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 54
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 55
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->makeAllPdfPrintPdf()V

    .line 110
    :goto_0
    return-void

    .line 89
    :cond_2
    const/4 v5, 0x0

    .line 90
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v6, p1

    if-ge v0, v6, :cond_4

    .line 92
    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/print/PageRange;->getStart()I

    move-result v3

    .local v3, "j":I
    :goto_2
    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/print/PageRange;->getEnd()I

    move-result v6

    if-gt v3, v6, :cond_3

    .line 93
    add-int/lit8 v5, v5, 0x1

    .line 92
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 90
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 97
    .end local v3    # "j":I
    :cond_4
    new-array v4, v5, [I

    .line 98
    .local v4, "nPageArray":[I
    const/4 v1, 0x0

    .line 99
    .local v1, "index":I
    const/4 v0, 0x0

    :goto_3
    array-length v6, p1

    if-ge v0, v6, :cond_6

    .line 101
    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/print/PageRange;->getStart()I

    move-result v3

    .restart local v3    # "j":I
    :goto_4
    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/print/PageRange;->getEnd()I

    move-result v6

    if-gt v3, v6, :cond_5

    .line 102
    add-int/lit8 v6, v3, 0x1

    aput v6, v4, v1

    .line 103
    add-int/lit8 v1, v1, 0x1

    .line 101
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 99
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 108
    .end local v3    # "j":I
    :cond_6
    iput-boolean v9, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->m_isPossibleMakePdf:Z

    .line 109
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;->mPrintPath:Ljava/lang/String;

    invoke-virtual {v6, v7, v5, v4}, Lcom/infraware/office/evengine/EvInterface;->IExportPDF(Ljava/lang/String;I[I)V

    goto :goto_0
.end method
