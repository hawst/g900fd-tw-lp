.class Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter$1;
.super Ljava/lang/Object;
.source "GooglePrintBaseAdapter.java"

# interfaces
.implements Landroid/os/CancellationSignal$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->m_isPossibleMakePdf:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->printCancelHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 161
    :cond_0
    return-void
.end method
