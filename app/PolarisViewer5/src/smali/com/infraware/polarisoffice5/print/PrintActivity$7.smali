.class Lcom/infraware/polarisoffice5/print/PrintActivity$7;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;->initRadioButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

.field final synthetic val$printAllRadio:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->val$printAllRadio:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 493
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->val$printAllRadio:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1600(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 496
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 497
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 498
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 500
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$200(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 512
    :goto_0
    return-void

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1600(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 505
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 506
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 507
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 508
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 510
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    goto :goto_0
.end method
