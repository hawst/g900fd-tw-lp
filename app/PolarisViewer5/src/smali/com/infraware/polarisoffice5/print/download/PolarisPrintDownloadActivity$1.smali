.class Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;
.super Landroid/os/Handler;
.source "PolarisPrintDownloadActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v6, 0x7f0702e6

    const v5, 0x104000a

    const/4 v4, 0x1

    .line 94
    const/4 v1, 0x0

    .line 96
    .local v1, "isDestroyed":Z
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->isDestroyed()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 101
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v1, :cond_1

    .line 103
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v4, v2, :cond_2

    .line 104
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0702e7

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1$1;-><init>(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 125
    :cond_0
    :goto_1
    const/4 v2, 0x3

    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v2, v3, :cond_3

    .line 126
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    # invokes: Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->showUpdateCheckDialog()V
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->access$000(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V

    .line 136
    :cond_1
    :goto_2
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string/jumbo v2, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v3, "isDestroyed Function NoSuchMethodError"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const/4 v1, 0x0

    goto :goto_0

    .line 113
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :cond_2
    const/4 v2, 0x2

    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v2, v3, :cond_0

    .line 115
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0702e8

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1$2;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1$2;-><init>(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    .line 128
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    # invokes: Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->dismissUpdateCheckDialog()V
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->access$100(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V

    .line 129
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    # setter for: Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mBIsFinished:Z
    invoke-static {v2, v4}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->access$202(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;Z)Z

    .line 130
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mOkBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->access$300(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 131
    iget v2, p1, Landroid/os/Message;->what:I

    if-nez v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->finish()V

    goto :goto_2
.end method
