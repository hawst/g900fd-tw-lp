.class public abstract Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;
.super Landroid/print/PrintDocumentAdapter;
.source "GooglePrintBaseAdapter.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;
.implements Lcom/infraware/polarisoffice5/print/OfficePrintJop;


# static fields
.field protected static final CANCEL_PRINT:I

.field protected static mCancellationSignal:Landroid/os/CancellationSignal;

.field protected static mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field protected static mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;


# instance fields
.field protected MyOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

.field protected mActivity:Landroid/app/Activity;

.field protected mAttributes:Landroid/print/PrintAttributes;

.field protected mDocType:I

.field protected mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field protected mFileName:Ljava/lang/String;

.field protected mPages:[Landroid/print/PageRange;

.field protected mPrintPath:Ljava/lang/String;

.field private mTempPrintPath:Ljava/lang/String;

.field protected mTotalPageCount:I

.field protected m_bChanged:Z

.field protected m_bControlFlag:Z

.field protected m_isPossibleMakePdf:Z

.field printCancelHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-object v0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 38
    sput-object v0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/app/Activity;)V
    .locals 3
    .param p1, "DocType"    # I
    .param p2, "DocumentName"    # Ljava/lang/String;
    .param p3, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mPrintPath:Ljava/lang/String;

    .line 31
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mFileName:Ljava/lang/String;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mDocType:I

    .line 39
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->m_isPossibleMakePdf:Z

    .line 40
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 43
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->m_bChanged:Z

    .line 44
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mPages:[Landroid/print/PageRange;

    .line 47
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mActivity:Landroid/app/Activity;

    .line 154
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter$1;-><init>(Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->MyOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

    .line 164
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter$2;-><init>(Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->printCancelHandler:Landroid/os/Handler;

    .line 83
    iput p1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mDocType:I

    .line 84
    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mActivity:Landroid/app/Activity;

    .line 85
    if-eqz p2, :cond_0

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lcom/infraware/common/util/FileUtils;->getFileNameWithoutExt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".pdf"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mFileName:Ljava/lang/String;

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    const-string/jumbo v0, "Print_Document.pdf"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mFileName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public doPrint(Landroid/app/Activity;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 52
    const/4 v4, 0x0

    .line 54
    .local v4, "s":Ljava/lang/String;
    :try_start_0
    const-class v5, Landroid/content/Context;

    const-string/jumbo v6, "PRINT_SERVICE"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 56
    .local v1, "f":Ljava/lang/reflect/Field;
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 68
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :goto_0
    if-nez v4, :cond_0

    .line 80
    :goto_1
    return-void

    .line 57
    .restart local v1    # "f":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/IllegalAccessException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 64
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 60
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v1    # "f":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 71
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :cond_0
    invoke-virtual {p1, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/print/PrintManager;

    .line 73
    .local v3, "printManager":Landroid/print/PrintManager;
    new-instance v5, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v5}, Landroid/print/PrintAttributes$Builder;-><init>()V

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/print/PrintAttributes$Builder;->setColorMode(I)Landroid/print/PrintAttributes$Builder;

    move-result-object v5

    sget-object v6, Landroid/print/PrintAttributes$MediaSize;->ISO_A4:Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v5, v6}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    move-result-object v5

    sget-object v6, Landroid/print/PrintAttributes$Margins;->NO_MARGINS:Landroid/print/PrintAttributes$Margins;

    invoke-virtual {v5, v6}, Landroid/print/PrintAttributes$Builder;->setMinMargins(Landroid/print/PrintAttributes$Margins;)Landroid/print/PrintAttributes$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v2

    .line 79
    .local v2, "mPrintAttributes":Landroid/print/PrintAttributes;
    const-string/jumbo v5, "Document Print"

    invoke-virtual {v3, v5, p0, v2}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    goto :goto_1
.end method

.method public isPossibleMakePdf()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->m_isPossibleMakePdf:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public makePdf()V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 195
    iput-boolean v10, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->m_isPossibleMakePdf:Z

    .line 196
    const/4 v4, 0x0

    .line 197
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 200
    .local v6, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/File;

    iget-object v10, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mPrintPath:Ljava/lang/String;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 201
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_0

    .line 247
    .end local v3    # "file":Ljava/io/File;
    :goto_0
    return-void

    .line 207
    .restart local v3    # "file":Ljava/io/File;
    :cond_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    sget-object v10, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v10}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 210
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    const v10, 0x8000

    :try_start_2
    new-array v0, v10, [B

    .line 211
    .local v0, "buffer":[B
    const/4 v9, 0x0

    .line 212
    .local v9, "readCount":I
    const/4 v8, 0x0

    .line 215
    .local v8, "loopCount":I
    :goto_1
    const/16 v10, 0x20

    if-ne v8, v10, :cond_2

    .line 216
    const/4 v8, 0x0

    .line 217
    sget-object v10, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v10}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 218
    sget-object v10, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    invoke-virtual {v10}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    .line 229
    :cond_1
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 230
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 231
    sget-object v10, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    iget-object v11, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mPages:[Landroid/print/PageRange;

    invoke-virtual {v10, v11}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 247
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 223
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    invoke-virtual {v5, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    .line 224
    if-lez v9, :cond_1

    .line 226
    const/4 v10, 0x0

    invoke-virtual {v7, v0, v10, v9}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 227
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 232
    .end local v0    # "buffer":[B
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "loopCount":I
    .end local v9    # "readCount":I
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    if-eqz v4, :cond_3

    .line 235
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 236
    :cond_3
    if-eqz v6, :cond_4

    .line 237
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 243
    :cond_4
    :goto_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 244
    sget-object v10, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    iget-object v11, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mActivity:Landroid/app/Activity;

    const v12, 0x7f0701bb

    invoke-virtual {v11, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 239
    :catch_1
    move-exception v2

    .line 240
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 232
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v1

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public onFinish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->m_isPossibleMakePdf:Z

    .line 127
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mAttributes:Landroid/print/PrintAttributes;

    .line 128
    sput-object v1, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .line 129
    sput-object v1, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 130
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mTempPrintPath:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/lang/String;Z)V

    .line 131
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onFinish()V

    .line 132
    return-void
.end method

.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "oldAttributes"    # Landroid/print/PrintAttributes;
    .param p2, "newAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
    .param p5, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 99
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mAttributes:Landroid/print/PrintAttributes;

    .line 101
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    invoke-virtual {p4}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    .line 118
    :goto_0
    return-void

    .line 106
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mTotalPageCount:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 107
    const-string/jumbo v1, "Page count calculation failed."

    invoke-virtual {p4, v1}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mAttributes:Landroid/print/PrintAttributes;

    invoke-virtual {v1, p1}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->m_bChanged:Z

    .line 112
    new-instance v1, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mFileName:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mTotalPageCount:I

    invoke-virtual {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v0

    .line 117
    .local v0, "info":Landroid/print/PrintDocumentInfo;
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->m_bChanged:Z

    invoke-virtual {p4, v0, v1}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 136
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 137
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mDocType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 140
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x4

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetPrintTotalPage(IIIIIII)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mTotalPageCount:I

    .line 150
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->printFolderInitialize()V

    .line 151
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onStart()V

    .line 152
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mTotalPageCount:I

    goto :goto_0
.end method

.method public abstract onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
.end method

.method public printFolderInitialize()V
    .locals 3

    .prologue
    .line 181
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 182
    .local v0, "defaultDir":Ljava/io/File;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "GooglePrint"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mTempPrintPath:Ljava/lang/String;

    .line 183
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mTempPrintPath:Ljava/lang/String;

    invoke-static {v1}, Lcom/infraware/common/util/FileUtils;->makeDirectories(Ljava/lang/String;)Ljava/lang/String;

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mTempPrintPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "printFile"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".pdf"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;->mPrintPath:Ljava/lang/String;

    .line 185
    return-void
.end method
