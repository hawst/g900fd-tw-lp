.class Lcom/infraware/polarisoffice5/print/PrintActivity$8;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;->initRadioButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

.field final synthetic val$printAllRadio:Landroid/widget/LinearLayout;

.field final synthetic val$printSomeRadio:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->val$printAllRadio:Landroid/widget/LinearLayout;

    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->val$printSomeRadio:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 523
    const/4 v0, 0x0

    .line 525
    .local v0, "isPressed":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 526
    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 527
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 528
    const/4 v0, 0x1

    .line 532
    .end local v1    # "rect":Landroid/graphics/Rect;
    :cond_1
    if-ne v0, v5, :cond_5

    .line 533
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->val$printAllRadio:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 534
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1600(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 540
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v2, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 552
    :cond_3
    :goto_1
    return v4

    .line 536
    :cond_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->val$printSomeRadio:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 537
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 538
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setPressed(Z)V

    goto :goto_0

    .line 543
    :cond_5
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->val$printAllRadio:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 544
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1600(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_1

    .line 546
    :cond_6
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->val$printSomeRadio:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 547
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 548
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$8;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setPressed(Z)V

    goto :goto_1
.end method
