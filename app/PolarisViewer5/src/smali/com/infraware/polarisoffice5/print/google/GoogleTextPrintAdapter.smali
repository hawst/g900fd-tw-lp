.class public Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
.super Landroid/print/PrintDocumentAdapter;
.source "GoogleTextPrintAdapter.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/print/OfficePrintJop;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;
    }
.end annotation


# static fields
.field private static final PrintedTextHeight:F = 20.58f

.field private static final PrintedTextSize:I = 0xc

.field private static final mPrintPath:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurBlockCount:I

.field private mDocumentInfo:Landroid/print/PrintDocumentInfo;

.field private mEditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

.field private mFileName:Ljava/lang/String;

.field private mHeightMargin:F

.field private mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

.field private mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

.field private mPageHeight:F

.field private mPagePerLine:F

.field private mPageWidth:F

.field private mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;

.field private mPrintAttributes:Landroid/print/PrintAttributes;

.field private mTotalBlockCount:I

.field private mTotalPageCount:I

.field private mWidthMargin:F

.field private m_bFirstStart:Z

.field private m_oneLineWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->PRINT_PATH:Ljava/lang/String;

    sput-object v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPrintPath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/infraware/polarisoffice5/text/control/EditCtrl;Ljava/lang/String;)V
    .locals 2
    .param p1, "a_context"    # Landroid/content/Context;
    .param p2, "a_editCtrl"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 143
    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mFileName:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    .line 72
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPagePerLine:F

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_bFirstStart:Z

    .line 144
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mContext:Landroid/content/Context;

    .line 145
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mEditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    .line 146
    if-eqz p3, :cond_0

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3}, Lcom/infraware/common/util/FileUtils;->getFileNameWithoutExt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".pdf"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mFileName:Ljava/lang/String;

    .line 152
    :goto_0
    return-void

    .line 150
    :cond_0
    const-string/jumbo v0, "Print_Document.pdf"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mFileName:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/PrintAttributes;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPrintAttributes:Landroid/print/PrintAttributes;

    return-object v0
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintAttributes;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # Landroid/print/PrintAttributes;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPrintAttributes:Landroid/print/PrintAttributes;

    return-object p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # Landroid/print/pdf/PrintedPdfDocument;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/PrintDocumentInfo;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mDocumentInfo:Landroid/print/PrintDocumentInfo;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/PrintDocumentInfo;)Landroid/print/PrintDocumentInfo;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # Landroid/print/PrintDocumentInfo;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mDocumentInfo:Landroid/print/PrintDocumentInfo;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Lcom/infraware/polarisoffice5/text/manager/LineList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;[Landroid/print/PageRange;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # [Landroid/print/PageRange;
    .param p2, "x2"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->pageInRange([Landroid/print/PageRange;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/graphics/pdf/PdfDocument$Page;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # Landroid/graphics/pdf/PdfDocument$Page;
    .param p2, "x2"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->drawPage(Landroid/graphics/pdf/PdfDocument$Page;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPagePerLine:F

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageHeight:F

    return v0
.end method

.method static synthetic access$302(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iput p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageHeight:F

    return p1
.end method

.method static synthetic access$324(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageHeight:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageHeight:F

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F

    return v0
.end method

.method static synthetic access$402(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iput p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F

    return p1
.end method

.method static synthetic access$424(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F

    return v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mWidthMargin:F

    return v0
.end method

.method static synthetic access$502(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iput p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mWidthMargin:F

    return p1
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mHeightMargin:F

    return v0
.end method

.method static synthetic access$602(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iput p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mHeightMargin:F

    return p1
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/os/CancellationSignal;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # Landroid/os/CancellationSignal;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->getTextLineResizing(Landroid/os/CancellationSignal;)I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    .prologue
    .line 34
    iget v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalPageCount:I

    return v0
.end method

.method static synthetic access$802(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalPageCount:I

    return p1
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;I)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->getPageResizing(I)I

    move-result v0

    return v0
.end method

.method private drawPage(Landroid/graphics/pdf/PdfDocument$Page;I)Z
    .locals 8
    .param p1, "a_Page"    # Landroid/graphics/pdf/PdfDocument$Page;
    .param p2, "a_PageNumber"    # I

    .prologue
    .line 527
    invoke-virtual {p1}, Landroid/graphics/pdf/PdfDocument$Page;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 529
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget v5, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mHeightMargin:F

    .line 530
    .local v5, "titleBaseLine":F
    iget v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mWidthMargin:F

    .line 533
    .local v2, "leftMargin":F
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 534
    .local v3, "paint":Landroid/graphics/Paint;
    const/high16 v6, -0x1000000

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 536
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 537
    const/high16 v6, 0x41400000    # 12.0f

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 539
    const/4 v4, 0x0

    .line 540
    .local v4, "temp":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    int-to-float v6, v1

    iget v7, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPagePerLine:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1

    .line 541
    const v6, 0x41a4a3d7    # 20.58f

    add-float/2addr v5, v6

    .line 542
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/text/manager/LineList;->get()Ljava/lang/String;

    move-result-object v4

    .line 543
    if-eqz v4, :cond_0

    .line 544
    invoke-virtual {v0, v4, v2, v5, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 540
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 546
    :cond_1
    const/4 v6, 0x1

    return v6
.end method

.method private getPageResizing(I)I
    .locals 3
    .param p1, "a_totalLineCount"    # I

    .prologue
    .line 400
    const/4 v0, -0x1

    .line 403
    .local v0, "pageCount":I
    iget v1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageHeight:F

    const v2, 0x41a4a3d7    # 20.58f

    div-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    int-to-float v1, v1

    iput v1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPagePerLine:F

    .line 404
    int-to-float v1, p1

    iget v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPagePerLine:F

    div-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v0, v1

    .line 406
    return v0
.end method

.method private getTextLineResizing(Landroid/os/CancellationSignal;)I
    .locals 22
    .param p1, "cancellationSignal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 259
    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    .line 260
    .local v11, "paint":Landroid/graphics/Paint;
    sget-object v18, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 261
    const/high16 v18, 0x41400000    # 12.0f

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 263
    const/4 v7, 0x0

    .line 264
    .local v7, "isLast":Z
    const/4 v13, 0x0

    .line 265
    .local v13, "startPosition":I
    const/4 v15, 0x0

    .line 267
    .local v15, "textLine":I
    const/16 v16, 0xa

    .line 268
    .local v16, "textLineMax":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F

    move/from16 v18, v0

    const/high16 v19, 0x43020000    # 130.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_0

    .line 269
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x4026000000000000L    # 11.0

    div-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->floor(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    add-int/lit8 v16, v18, -0x2

    .line 272
    :cond_0
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_oneLineWidth:F

    .line 273
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mCurBlockCount:I

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/manager/LineList;->delete()V

    .line 281
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 283
    :cond_1
    new-instance v18, Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Lcom/infraware/polarisoffice5/text/manager/LineList;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 285
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 286
    .local v9, "mBlockText":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mEditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mCurBlockCount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockText(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 288
    const/16 v18, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F

    move/from16 v19, v0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_3

    .line 289
    const/16 v18, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 290
    .local v14, "temp":Ljava/lang/String;
    const-string/jumbo v18, "\n"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 292
    .local v12, "splitString":[Ljava/lang/String;
    move-object v2, v12

    .local v2, "arr$":[Ljava/lang/String;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v10, v2, v6

    .line 293
    .local v10, "oTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 292
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 294
    .end local v10    # "oTemp":Ljava/lang/String;
    :cond_2
    add-int/lit8 v15, v15, 0x1

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishAdd()V

    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    .end local v12    # "splitString":[Ljava/lang/String;
    :goto_1
    move/from16 v18, v15

    .line 390
    .end local v14    # "temp":Ljava/lang/String;
    :goto_2
    return v18

    .line 297
    :cond_3
    const/4 v5, 0x0

    .line 298
    .local v5, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mEditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getActParent()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getPrintStopFlag()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishAdd()V

    .line 300
    const/16 v18, -0x1

    goto :goto_2

    .line 303
    :cond_4
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v18

    if-nez v18, :cond_5

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v18

    if-eqz v18, :cond_6

    .line 304
    :cond_5
    const/16 v18, -0x1

    goto :goto_2

    .line 306
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_oneLineWidth:F

    move/from16 v18, v0

    add-int v19, v13, v5

    add-int/lit8 v19, v19, -0x1

    add-int v20, v13, v5

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v19

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_oneLineWidth:F

    .line 307
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_oneLineWidth:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-gtz v18, :cond_7

    add-int v18, v13, v5

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    const/16 v19, 0xa

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 308
    :cond_7
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_oneLineWidth:F

    .line 309
    add-int v18, v13, v5

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    const/16 v19, 0xa

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 310
    add-int v18, v13, v5

    move/from16 v0, v18

    invoke-virtual {v9, v13, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 311
    .restart local v14    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 312
    add-int/lit8 v15, v15, 0x1

    .line 314
    add-int v18, v13, v5

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    const/16 v19, 0xa

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 315
    add-int/lit8 v13, v13, 0x1

    .line 316
    :cond_8
    add-int/2addr v13, v5

    .line 317
    const/4 v5, 0x0

    .line 297
    .end local v14    # "temp":Ljava/lang/String;
    :cond_9
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 318
    :cond_a
    add-int v18, v13, v5

    add-int/lit8 v18, v18, -0x2

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    const/16 v19, 0x20

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_e

    add-int v18, v13, v5

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    const/16 v19, 0x20

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_e

    .line 319
    add-int v18, v13, v5

    add-int/lit8 v3, v18, -0x2

    .line 320
    .local v3, "breakPosition":I
    add-int/lit8 v18, v3, -0x1

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    const/16 v19, 0x20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 321
    invoke-virtual {v9, v13, v3}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 322
    .restart local v14    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 323
    add-int/lit8 v15, v15, 0x1

    .line 324
    move v13, v3

    .line 325
    const/4 v5, 0x0

    .line 326
    goto :goto_4

    .line 327
    .end local v14    # "temp":Ljava/lang/String;
    :cond_b
    const/16 v17, 0x2

    .line 329
    .local v17, "z":I
    :goto_5
    sub-int v18, v3, v17

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v18

    const/16 v19, 0x20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 330
    sub-int v18, v3, v17

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v9, v13, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 331
    .restart local v14    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 332
    add-int/lit8 v15, v15, 0x1

    .line 333
    sub-int v18, v3, v17

    add-int/lit8 v13, v18, 0x1

    .line 334
    const/4 v5, 0x0

    .line 335
    goto :goto_4

    .line 336
    .end local v14    # "temp":Ljava/lang/String;
    :cond_c
    move/from16 v0, v17

    move/from16 v1, v16

    if-le v0, v1, :cond_d

    .line 337
    add-int/lit8 v18, v3, 0x1

    move/from16 v0, v18

    invoke-virtual {v9, v13, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 338
    .restart local v14    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 339
    add-int/lit8 v15, v15, 0x1

    .line 340
    add-int/lit8 v13, v3, 0x1

    .line 341
    const/4 v5, 0x0

    .line 342
    goto/16 :goto_4

    .line 344
    .end local v14    # "temp":Ljava/lang/String;
    :cond_d
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 349
    .end local v3    # "breakPosition":I
    .end local v17    # "z":I
    :cond_e
    add-int v18, v13, v5

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v9, v13, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 350
    .restart local v14    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 351
    add-int/lit8 v15, v15, 0x1

    .line 352
    add-int v18, v13, v5

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->charAt(I)C
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v18

    const/16 v19, 0xa

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_f

    .line 353
    add-int/lit8 v13, v13, 0x1

    .line 354
    :cond_f
    add-int/lit8 v18, v5, -0x1

    add-int v13, v13, v18

    .line 355
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 358
    .end local v14    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 359
    .local v4, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mCurBlockCount:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalBlockCount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_10

    .line 361
    invoke-virtual {v9, v13}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 362
    .restart local v14    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mCurBlockCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mCurBlockCount:I

    .line 363
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 364
    invoke-virtual {v9, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mEditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mCurBlockCount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockText(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 366
    const/4 v5, 0x0

    .line 367
    const/4 v13, 0x0

    .line 368
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_oneLineWidth:F

    goto/16 :goto_4

    .line 370
    .end local v14    # "temp":Ljava/lang/String;
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mCurBlockCount:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalBlockCount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_11

    if-nez v7, :cond_11

    .line 371
    invoke-virtual {v9, v13}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 372
    .restart local v14    # "temp":Ljava/lang/String;
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 373
    invoke-virtual {v9, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 374
    const/4 v5, 0x0

    .line 375
    const/4 v13, 0x0

    .line 376
    const/4 v7, 0x1

    .line 377
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_oneLineWidth:F

    goto/16 :goto_4

    .line 380
    .end local v14    # "temp":Ljava/lang/String;
    :cond_11
    invoke-virtual {v9, v13}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 381
    .restart local v14    # "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/infraware/polarisoffice5/text/manager/LineList;->add(Ljava/lang/String;)V

    .line 382
    add-int/lit8 v15, v15, 0x1

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/infraware/polarisoffice5/text/manager/LineList;->finishAdd()V

    .line 384
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_oneLineWidth:F

    goto/16 :goto_1
.end method

.method private pageInRange([Landroid/print/PageRange;I)Z
    .locals 3
    .param p1, "pageRanges"    # [Landroid/print/PageRange;
    .param p2, "page"    # I

    .prologue
    .line 515
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 517
    aget-object v2, p1, v0

    invoke-virtual {v2}, Landroid/print/PageRange;->getStart()I

    move-result v1

    .local v1, "j":I
    :goto_1
    aget-object v2, p1, v0

    invoke-virtual {v2}, Landroid/print/PageRange;->getEnd()I

    move-result v2

    if-gt v1, v2, :cond_1

    .line 518
    if-ne p2, v1, :cond_0

    .line 519
    const/4 v2, 0x1

    .line 522
    .end local v1    # "j":I
    :goto_2
    return v2

    .line 517
    .restart local v1    # "j":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 515
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 522
    .end local v1    # "j":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method


# virtual methods
.method public doPrint(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 81
    const/4 v3, 0x0

    .line 83
    .local v3, "s":Ljava/lang/String;
    :try_start_0
    const-class v4, Landroid/content/Context;

    const-string/jumbo v5, "PRINT_SERVICE"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 85
    .local v1, "f":Ljava/lang/reflect/Field;
    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 97
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :goto_0
    if-nez v3, :cond_0

    .line 109
    :goto_1
    return-void

    .line 86
    .restart local v1    # "f":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/IllegalAccessException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 89
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v1    # "f":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 100
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :cond_0
    invoke-virtual {p1, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/print/PrintManager;

    .line 102
    .local v2, "printManager":Landroid/print/PrintManager;
    new-instance v4, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v4}, Landroid/print/PrintAttributes$Builder;-><init>()V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/print/PrintAttributes$Builder;->setColorMode(I)Landroid/print/PrintAttributes$Builder;

    move-result-object v4

    sget-object v5, Landroid/print/PrintAttributes$MediaSize;->ISO_A4:Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v4, v5}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    move-result-object v4

    sget-object v5, Landroid/print/PrintAttributes$Margins;->NO_MARGINS:Landroid/print/PrintAttributes$Margins;

    invoke-virtual {v4, v5}, Landroid/print/PrintAttributes$Builder;->setMinMargins(Landroid/print/PrintAttributes$Margins;)Landroid/print/PrintAttributes$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPrintAttributes:Landroid/print/PrintAttributes;

    .line 108
    const-string/jumbo v4, "Document Print"

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPrintAttributes:Landroid/print/PrintAttributes;

    invoke-virtual {v2, v4, p0, v5}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    goto :goto_1
.end method

.method public isPossibleMakePdf()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public makePdf()V
    .locals 0

    .prologue
    .line 163
    return-void
.end method

.method public onFinish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    invoke-virtual {v0}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 127
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/manager/LineList;->delete()V

    .line 131
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->interrupt()V

    .line 140
    :cond_2
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onFinish()V

    .line 141
    return-void
.end method

.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "oldAttributes"    # Landroid/print/PrintAttributes;
    .param p2, "newAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
    .param p5, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_bFirstStart:Z

    if-nez v0, :cond_1

    .line 171
    invoke-virtual {p2, p1}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mDocumentInfo:Landroid/print/PrintDocumentInfo;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mDocumentInfo:Landroid/print/PrintDocumentInfo;

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPrintAttributes:Landroid/print/PrintAttributes;

    .line 181
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 182
    invoke-virtual {p4}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    goto :goto_0

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    if-nez v0, :cond_3

    .line 187
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;-><init>(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    .line 188
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->start()V

    goto :goto_0

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 192
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->interrupt()V

    .line 194
    :cond_4
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;-><init>(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    .line 195
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mOnlayoutThread:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->start()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->m_bFirstStart:Z

    .line 114
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mEditCtrl:Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->getBlockCount()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalBlockCount:I

    .line 116
    new-instance v0, Lcom/infraware/polarisoffice5/text/manager/LineList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/text/manager/LineList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;

    .line 118
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mHeightMargin:F

    iput v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mWidthMargin:F

    .line 120
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onStart()V

    .line 121
    return-void
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 6
    .param p1, "pages"    # [Landroid/print/PageRange;
    .param p2, "descriptor"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    .line 414
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p4}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    .line 501
    :goto_0
    return-void

    .line 420
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;-><init>(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;[Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Void;

    invoke-virtual {v0, v2, v1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
