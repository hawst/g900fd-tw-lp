.class public Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;
.super Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;
.source "GoogleSheetPrintAdapter.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/app/Activity;)V
    .locals 0
    .param p1, "DocType"    # I
    .param p2, "DocumentName"    # Ljava/lang/String;
    .param p3, "activity"    # Landroid/app/Activity;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;-><init>(ILjava/lang/String;Landroid/app/Activity;)V

    .line 16
    return-void
.end method


# virtual methods
.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 5
    .param p1, "pages"    # [Landroid/print/PageRange;
    .param p2, "destination"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    sput-object p4, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .line 23
    sput-object p2, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 24
    sput-object p3, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 25
    sget-object v1, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->MyOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

    invoke-virtual {v1, v2}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 26
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->mPages:[Landroid/print/PageRange;

    .line 34
    new-array v0, v4, [I

    .line 35
    .local v0, "nPageArray":[I
    aput v3, v0, v3

    .line 37
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->m_isPossibleMakePdf:Z

    .line 38
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;->mPrintPath:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/infraware/office/evengine/EvInterface;->IExportPDF(Ljava/lang/String;I[I)V

    .line 39
    return-void
.end method
