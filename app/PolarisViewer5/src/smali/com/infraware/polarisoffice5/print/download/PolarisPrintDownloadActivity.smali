.class public Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;
.super Landroid/app/Activity;
.source "PolarisPrintDownloadActivity.java"


# static fields
.field private static final NETWORKERR_DATAROAMING_OFF:I = 0x2

.field private static final NETWORKERR_FLIGHTMODE_ON:I = 0x0

.field private static final NETWORKERR_INVALID:I = -0x1

.field private static final NETWORKERR_MMS_EMAIL:I = 0x5

.field private static final NETWORKERR_MOBILEDATA_OFF:I = 0x1

.field private static final NETWORKERR_NO_SIGNAL:I = 0x4

.field private static final NETWORKERR_REACHED_DATALIMIT:I = 0x3

.field public static final TAG:Ljava/lang/String; = "POLARIS Office 5 - Download Polaris Print"

.field private static final UPDATE_CHECK:I = 0x10e


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mBIsFinished:Z

.field private mCancelBtn:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field public mHandler:Landroid/os/Handler;

.field private mOkBtn:Landroid/widget/Button;

.field private mTextContentView:Landroid/widget/TextView;

.field private m_UpdateCheckDialog:Landroid/app/ProgressDialog;

.field private m_oDownLoadClickListener:Landroid/view/View$OnClickListener;

.field private m_oFinishClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 42
    const-string/jumbo v0, "OfficeMainActivity-STUB"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->LOG_CAT:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mBIsFinished:Z

    .line 91
    new-instance v0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$1;-><init>(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mHandler:Landroid/os/Handler;

    .line 153
    new-instance v0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$2;-><init>(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_oFinishClickListener:Landroid/view/View$OnClickListener;

    .line 159
    new-instance v0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity$3;-><init>(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_oDownLoadClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->showUpdateCheckDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->dismissUpdateCheckDialog()V

    return-void
.end method

.method static synthetic access$202(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mBIsFinished:Z

    return p1
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mOkBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->finishActivity()V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->checkNetworkState()V

    return-void
.end method

.method private checkNetworkState()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 180
    invoke-static {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 181
    invoke-static {}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->isRunnging()Z

    move-result v2

    if-nez v2, :cond_0

    .line 182
    new-instance v1, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2, v3}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;-><init>(Landroid/content/Context;ZLandroid/os/Handler;)V

    .line 183
    .local v1, "thread":Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->start()V

    .line 204
    .end local v1    # "thread":Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    const/4 v0, -0x1

    .line 188
    .local v0, "networkStatus":I
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->isFligtMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 189
    const/4 v0, 0x0

    .line 200
    :goto_1
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->sendBroadcastForNetworkErrorPopup(I)V

    .line 201
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mBIsFinished:Z

    .line 202
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_0

    .line 190
    :cond_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->isMobileDataOff()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 191
    const/4 v0, 0x1

    goto :goto_1

    .line 192
    :cond_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->isRoamingOff()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 193
    const/4 v0, 0x2

    goto :goto_1

    .line 194
    :cond_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->isReachToDataLimit()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 195
    const/4 v0, 0x3

    goto :goto_1

    .line 197
    :cond_5
    const/4 v0, 0x4

    goto :goto_1
.end method

.method private dismissUpdateCheckDialog()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_UpdateCheckDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_UpdateCheckDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_UpdateCheckDialog:Landroid/app/ProgressDialog;

    .line 152
    :cond_0
    return-void
.end method

.method private finishActivity()V
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->finish()V

    .line 257
    :cond_0
    return-void
.end method

.method private isFligtMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 230
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isMobileDataOff()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 234
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "mobile_data"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 236
    .local v0, "retVal":Z
    :goto_0
    return v0

    .end local v0    # "retVal":Z
    :cond_0
    move v0, v1

    .line 234
    goto :goto_0
.end method

.method private static isNetworkConnected(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 215
    const/4 v0, 0x0

    .line 216
    .local v0, "bIsConnected":Z
    const-string/jumbo v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 218
    .local v1, "manager":Landroid/net/ConnectivityManager;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 219
    .local v2, "mobile":Landroid/net/NetworkInfo;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 221
    .local v3, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 222
    :cond_0
    const/4 v0, 0x1

    .line 226
    :goto_0
    return v0

    .line 224
    :cond_1
    const-string/jumbo v4, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v5, "isNetworkConnected : network error"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isReachToDataLimit()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 247
    const-string/jumbo v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 248
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isMobilePolicyDataEnable()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isRoamingOff()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 240
    const-string/jumbo v2, "phone"

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 241
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataRoamingEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private sendBroadcastForNetworkErrorPopup(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 207
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 208
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.popupuireceiver"

    const-string/jumbo v2, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    const-string/jumbo v1, "network_err_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 211
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 212
    return-void
.end method

.method private showUpdateCheckDialog()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 141
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_UpdateCheckDialog:Landroid/app/ProgressDialog;

    .line 142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_UpdateCheckDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_UpdateCheckDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 144
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_UpdateCheckDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070302

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_UpdateCheckDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 146
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 85
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 86
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 88
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 89
    return-void
.end method

.method public getVersionName(Landroid/app/Activity;)Ljava/lang/String;
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 168
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 171
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 175
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v3

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v3, "UnKnown"

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 60
    :cond_0
    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->setContentView(I)V

    .line 61
    iput-object p0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mContext:Landroid/content/Context;

    .line 62
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f070034

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 63
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0201d1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    .line 64
    const v0, 0x7f0b01bb

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mTextContentView:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0b01bc

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mCancelBtn:Landroid/widget/Button;

    .line 66
    const v0, 0x7f0b01bd

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mOkBtn:Landroid/widget/Button;

    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mTextContentView:Landroid/widget/TextView;

    const v1, 0x7f0702e9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 68
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mCancelBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_oFinishClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->mOkBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;->m_oDownLoadClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const-string/jumbo v0, "OfficeMainActivity-STUB"

    const-string/jumbo v1, "POLARIS Print 2 Version = 2.0.9300.02"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 77
    return-void
.end method
