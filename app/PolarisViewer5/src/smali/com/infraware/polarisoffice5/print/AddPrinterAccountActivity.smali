.class public Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "AddPrinterAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;
    }
.end annotation


# static fields
.field public static final ACCOUNT_ACTION_ADD:I = 0x65

.field public static final ACCOUNT_ACTION_CHECK:I = 0x64

.field public static final ACCOUNT_ACTION_CLOSE:I = 0x384

.field public static final ACCOUNT_POPUP_MSG:I = 0x64

.field public static final ACCOUNT_POPUP_SET:I = 0xc8


# instance fields
.field private mAccountToken:Ljava/lang/String;

.field private m_etEmail:Landroid/widget/EditText;

.field private m_etPassword:Landroid/widget/EditText;

.field private m_ivTitle:Landroid/widget/ImageButton;

.field private m_layoutError:Landroid/widget/LinearLayout;

.field private m_layoutTitle:Landroid/widget/LinearLayout;

.field private m_nLocaleCode:I

.field private m_nOrientation:I

.field private m_nPopupMsgId:I

.field private m_nServiceType:I

.field private m_oErrorDialog:Landroid/app/AlertDialog;

.field private m_oEventHandler:Landroid/os/Handler;

.field private m_oImmManager:Lcom/infraware/polarisoffice5/common/ImmManager;

.field private m_oMsgDialog:Landroid/app/AlertDialog;

.field private m_oProgressDialog:Landroid/app/ProgressDialog;

.field private m_oSetDialog:Landroid/app/AlertDialog;

.field private m_oToastMsg:Landroid/widget/Toast;

.field private m_runToastMsg:Ljava/lang/Runnable;

.field private m_strToastMsg:Ljava/lang/String;

.field private m_tvError:Landroid/widget/TextView;

.field private m_tvTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutError:Landroid/widget/LinearLayout;

    .line 55
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvTitle:Landroid/widget/TextView;

    .line 56
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    .line 57
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    .line 59
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvError:Landroid/widget/TextView;

    .line 61
    iput v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nOrientation:I

    .line 62
    iput v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nLocaleCode:I

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nServiceType:I

    .line 66
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    .line 67
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oErrorDialog:Landroid/app/AlertDialog;

    .line 68
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oMsgDialog:Landroid/app/AlertDialog;

    .line 69
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oSetDialog:Landroid/app/AlertDialog;

    .line 71
    iput v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nPopupMsgId:I

    .line 73
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->mAccountToken:Ljava/lang/String;

    .line 74
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oToastMsg:Landroid/widget/Toast;

    .line 75
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_strToastMsg:Ljava/lang/String;

    .line 78
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oImmManager:Lcom/infraware/polarisoffice5/common/ImmManager;

    .line 80
    new-instance v0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oEventHandler:Landroid/os/Handler;

    .line 101
    new-instance v0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$2;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_runToastMsg:Ljava/lang/Runnable;

    .line 455
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutError:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->mAccountToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->mAccountToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getAccountToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvError:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oToastMsg:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$302(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oToastMsg:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_strToastMsg:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->setButtonStatus()V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->onToastMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->isValidEmailAddress(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private getAccountToken()Ljava/lang/String;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 414
    const-string/jumbo v1, ""

    .line 416
    .local v1, "authCode":Ljava/lang/String;
    :try_start_0
    new-instance v9, Ljava/net/URL;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "https://www.google.com/accounts/ClientLogin?accountType=HOSTED_OR_GOOGLE&Email="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "&Passwd="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "&service=cloudprint&source=Gulp-CalGulp-1.05"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v11}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 422
    .local v9, "url":Ljava/net/URL;
    invoke-virtual {v9}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->readAll(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    .line 423
    .local v6, "responseContent":Ljava/lang/String;
    const-string/jumbo v11, "\n"

    invoke-virtual {v6, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 425
    .local v8, "split":[Ljava/lang/String;
    move-object v0, v8

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v7, v0, v3

    .line 427
    .local v7, "s":Ljava/lang/String;
    const-string/jumbo v11, "="

    invoke-virtual {v7, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 428
    .local v5, "nvsplit":[Ljava/lang/String;
    array-length v11, v5

    const/4 v12, 0x2

    if-ne v11, v12, :cond_1

    .line 429
    const/4 v11, 0x0

    aget-object v11, v5, v11

    const-string/jumbo v12, "Auth"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 430
    const/4 v11, 0x1

    aget-object v1, v5, v11
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v10, v1

    .line 440
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "nvsplit":[Ljava/lang/String;
    .end local v6    # "responseContent":Ljava/lang/String;
    .end local v7    # "s":Ljava/lang/String;
    .end local v8    # "split":[Ljava/lang/String;
    .end local v9    # "url":Ljava/net/URL;
    :cond_0
    :goto_1
    return-object v10

    .line 425
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v5    # "nvsplit":[Ljava/lang/String;
    .restart local v6    # "responseContent":Ljava/lang/String;
    .restart local v7    # "s":Ljava/lang/String;
    .restart local v8    # "split":[Ljava/lang/String;
    .restart local v9    # "url":Ljava/net/URL;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 435
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "nvsplit":[Ljava/lang/String;
    .end local v6    # "responseContent":Ljava/lang/String;
    .end local v7    # "s":Ljava/lang/String;
    .end local v8    # "split":[Ljava/lang/String;
    .end local v9    # "url":Ljava/net/URL;
    :catch_0
    move-exception v2

    .line 436
    .local v2, "e":Ljava/lang/Exception;
    iget-object v11, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oEventHandler:Landroid/os/Handler;

    const/16 v12, 0x64

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method private isValidEmailAddress(Ljava/lang/String;)Z
    .locals 2
    .param p1, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 350
    const-string/jumbo v0, "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[\\w-]{2,4}$"

    .line 351
    .local v0, "EMAIL_REGEX1":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private onOrientationChanged()V
    .locals 4

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nOrientation:I

    .line 302
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v2

    if-nez v2, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget v3, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nOrientation:I

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 307
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 309
    .local v1, "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v2, 0x41500000    # 13.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 310
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 314
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 315
    const/high16 v2, 0x42400000    # 48.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 316
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    :goto_0
    return-void

    .line 319
    :cond_1
    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 320
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 322
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oImmManager:Lcom/infraware/polarisoffice5/common/ImmManager;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/ImmManager;->showDelayedIme()V

    goto :goto_0
.end method

.method private onToastMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 295
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_strToastMsg:Ljava/lang/String;

    .line 296
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_strToastMsg:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_strToastMsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_runToastMsg:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 298
    :cond_0
    return-void
.end method

.method private readAll(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 444
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 445
    .local v2, "sb":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 447
    .local v0, "br":Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .local v1, "line":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 448
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 449
    const-string/jumbo v3, "\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 451
    :cond_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 452
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private setButtonStatus()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 334
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 335
    .local v0, "email":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 337
    .local v1, "password":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 338
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 342
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 343
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 346
    :goto_1
    return-void

    .line 340
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 345
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_1
.end method

.method private setScreen()V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvTitle:Landroid/widget/TextView;

    const v1, 0x7f070296

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 329
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    const v1, 0x7f07009b

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 330
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    const v1, 0x7f07009d

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 331
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 262
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 263
    .local v0, "nlocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nLocaleCode:I

    if-eq v1, v0, :cond_3

    .line 264
    iput v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nLocaleCode:I

    .line 265
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->setScreen()V

    .line 267
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oMsgDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oMsgDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oMsgDialog:Landroid/app/AlertDialog;

    const v2, 0x7f0700a8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 269
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oMsgDialog:Landroid/app/AlertDialog;

    iget v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nPopupMsgId:I

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oMsgDialog:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f070063

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oSetDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oSetDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 275
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oSetDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 278
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutError:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 280
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvError:Landroid/widget/TextView;

    const v2, 0x7f070091

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 281
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvError:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oErrorDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_3

    .line 285
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oErrorDialog:Landroid/app/AlertDialog;

    invoke-static {p0, v1}, Lcom/infraware/common/util/Utils;->onLocaleChanged(Landroid/content/Context;Landroid/app/AlertDialog;)V

    .line 288
    :cond_3
    iget v1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_4

    .line 289
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->onOrientationChanged()V

    .line 291
    :cond_4
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 292
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v5, 0x7f050087

    const/4 v4, 0x0

    .line 116
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 123
    :cond_0
    const v2, 0x7f030013

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->setContentView(I)V

    .line 125
    const v2, 0x7f0b0031

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutTitle:Landroid/widget/LinearLayout;

    .line 126
    const v2, 0x7f0b0034

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvTitle:Landroid/widget/TextView;

    .line 127
    const v2, 0x7f0b0038

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    .line 129
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvTitle:Landroid/widget/TextView;

    const v3, 0x7f0700d1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 131
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 132
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 134
    const v2, 0x7f0b005e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    .line 135
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHighlightColor(I)V

    .line 138
    const v2, 0x7f0b005f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    .line 139
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHighlightColor(I)V

    .line 142
    const v2, 0x7f0b0060

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutError:Landroid/widget/LinearLayout;

    .line 143
    const v2, 0x7f0b0062

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvError:Landroid/widget/TextView;

    .line 144
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutError:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 146
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_nLocaleCode:I

    .line 149
    new-instance v2, Lcom/infraware/polarisoffice5/common/ImmManager;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/ImmManager;-><init>(Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oImmManager:Lcom/infraware/polarisoffice5/common/ImmManager;

    .line 151
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->onOrientationChanged()V

    .line 153
    new-instance v0, Lcom/infraware/common/util/MailInputFilter;

    invoke-direct {v0, p0}, Lcom/infraware/common/util/MailInputFilter;-><init>(Landroid/content/Context;)V

    .line 154
    .local v0, "filter":Lcom/infraware/common/util/MailInputFilter;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/infraware/common/util/MailInputFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 156
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;

    new-instance v3, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$3;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$3;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 175
    const/4 v2, 0x1

    new-array v1, v2, [Landroid/text/InputFilter;

    .line 176
    .local v1, "passfilters":[Landroid/text/InputFilter;
    new-instance v2, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$4;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$4;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    aput-object v2, v1, v4

    .line 193
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 195
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    new-instance v3, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$5;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$5;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 214
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;

    new-instance v3, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$6;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$6;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 233
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_ivTitle:Landroid/widget/ImageButton;

    new-instance v3, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x15

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 253
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 257
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 258
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 404
    packed-switch p1, :pswitch_data_0

    .line 409
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/accounts/AccountAuthenticatorActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 406
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->setResult(I)V

    .line 407
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->finish()V

    goto :goto_0

    .line 404
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public startProgressing(Landroid/content/Context;I)V
    .locals 3
    .param p1, "aActivityCtx"    # Landroid/content/Context;
    .param p2, "aTitleID"    # I

    .prologue
    const/4 v2, 0x0

    .line 355
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 389
    :goto_0
    return-void

    .line 358
    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v0, p1, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    .line 359
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0, p2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 360
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 361
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 362
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$8;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$8;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 374
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$9;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$9;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 388
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method public stopProgressing()V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 396
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 397
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 398
    :catch_0
    move-exception v0

    goto :goto_0
.end method
