.class public Lcom/infraware/polarisoffice5/print/PrintActivity;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "PrintActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$BaseActivityEventType;
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_WHEEL_BUTTON_TYPE;
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;
.implements Lcom/infraware/office/evengine/E$EV_BOOKMARK_EDITOR_MODE;
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_CURSOR_MODE;
.implements Lcom/infraware/office/evengine/E$EV_ERROR_CODE;
.implements Lcom/infraware/office/evengine/EvListener$PrintListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;
    }
.end annotation


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private MAX_PAGE_NUM:I

.field Watcher:Landroid/text/TextWatcher;

.field private addAccountTokenList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private bEditAccountMode:Z

.field private getDbToken:Ljava/lang/String;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private jsonData:Ljava/lang/String;

.field private mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

.field private mActivity:Landroid/app/Activity;

.field private mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

.field private mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mDbAccountCount:I

.field private mDbAccountTokenList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field private mEditAccountListener:Landroid/view/View$OnClickListener;

.field public mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mJsonArray:Lorg/json/JSONArray;

.field private mJsonObject:Lorg/json/JSONObject;

.field private mListThreadHandler:Landroid/os/Handler;

.field private mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

.field private mMimeType:Ljava/lang/String;

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

.field private mPageRangeCnt:I

.field private mPopoverListener:Landroid/view/View$OnClickListener;

.field private mPrintAllCheckBox:Landroid/widget/CheckBox;

.field private mPrintRangeEditText:Landroid/widget/EditText;

.field private mPrintSomeCheckBox:Landroid/widget/CheckBox;

.field private mSubmitThreadHandler:Landroid/os/Handler;

.field private m_AccountIndex:I

.field private m_Authtoken:Ljava/lang/String;

.field private m_ToastMsg:Landroid/widget/Toast;

.field m_aPrintListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_bShowPrintOption:Z

.field private m_nCopy:I

.field private m_nErrMsgId:I

.field private m_oAccountArray:[Landroid/accounts/Account;

.field private m_oAccountManager:Landroid/accounts/AccountManager;

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;

.field private m_oPrintListAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private m_oPrintListView:Landroid/widget/ListView;

.field private m_sPrinterId:Ljava/lang/String;

.field private m_strFilePath:Ljava/lang/String;

.field private mstrPdfExportFilePath:Ljava/lang/String;

.field private printArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 75
    const-string/jumbo v0, "PrintActivity"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->LOG_CAT:Ljava/lang/String;

    .line 76
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    .line 77
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    .line 78
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPopoverListener:Landroid/view/View$OnClickListener;

    .line 79
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 86
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;

    .line 87
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;

    .line 88
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    .line 109
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->bEditAccountMode:Z

    .line 113
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_aPrintListItem:Ljava/util/ArrayList;

    .line 114
    iput-object p0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    .line 117
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;

    .line 118
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 145
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    .line 148
    iput v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->MAX_PAGE_NUM:I

    .line 340
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$6;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->Watcher:Landroid/text/TextWatcher;

    .line 952
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$11;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$11;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    .line 1019
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$12;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$12;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1228
    iput v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    .line 1273
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    .line 1307
    return-void
.end method

.method private ConvertPageRange()[I
    .locals 20

    .prologue
    .line 667
    const/4 v12, 0x0

    .local v12, "nStartPos":I
    const/4 v10, 0x0

    .line 668
    .local v10, "nEndPos":I
    const/4 v5, 0x0

    .local v5, "bRange":Z
    const/4 v4, 0x1

    .line 669
    .local v4, "bIsCharBefore":Z
    const/4 v3, 0x0

    .line 670
    .local v3, "arrRange2":[I
    const/4 v6, 0x0

    .line 672
    .local v6, "cnt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_18

    .line 674
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 676
    .local v9, "listRange":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    .line 677
    .local v15, "strRange":Ljava/lang/String;
    const-string/jumbo v16, ""

    .line 678
    .local v16, "temp":Ljava/lang/String;
    if-nez v15, :cond_0

    .line 680
    const/4 v9, 0x0

    .line 681
    const/16 v18, 0x0

    .line 818
    .end local v9    # "listRange":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v15    # "strRange":Ljava/lang/String;
    .end local v16    # "temp":Ljava/lang/String;
    :goto_0
    return-object v18

    .line 685
    .restart local v9    # "listRange":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v15    # "strRange":Ljava/lang/String;
    .restart local v16    # "temp":Ljava/lang/String;
    :cond_0
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    .line 687
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    if-ge v7, v0, :cond_e

    .line 689
    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Character;->isDigit(C)Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 691
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 692
    const/4 v4, 0x0

    .line 687
    :cond_1
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 694
    :cond_2
    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v18

    const/16 v19, 0x2c

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v18

    const/16 v19, 0x2d

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    .line 696
    const v18, 0x7f070097

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V

    .line 697
    const/16 v18, 0x0

    goto :goto_0

    .line 699
    :cond_3
    if-eqz v4, :cond_4

    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v5, v0, :cond_1

    .line 702
    :cond_4
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v4, v0, :cond_5

    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Character;->isDigit(C)Z

    move-result v18

    if-nez v18, :cond_5

    .line 704
    const v18, 0x7f070097

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V

    .line 705
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 707
    :cond_5
    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v18

    const/16 v19, 0x2d

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 709
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->ConvertStrtoInt(Ljava/lang/String;)I

    move-result v12

    .line 710
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v12, v0, :cond_6

    .line 712
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 714
    :cond_6
    const-string/jumbo v16, ""

    .line 715
    const/4 v5, 0x1

    .line 748
    :cond_7
    :goto_3
    const/4 v4, 0x1

    goto :goto_2

    .line 717
    :cond_8
    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v18

    const/16 v19, 0x2c

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 719
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->ConvertStrtoInt(Ljava/lang/String;)I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 720
    .local v11, "nItem":Ljava/lang/Integer;
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 722
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 724
    :cond_9
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v5, v0, :cond_d

    .line 725
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->ConvertStrtoInt(Ljava/lang/String;)I

    move-result v10

    .line 726
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v10, v0, :cond_a

    .line 728
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 730
    :cond_a
    const-string/jumbo v16, ""

    .line 732
    if-le v12, v10, :cond_b

    .line 734
    move/from16 v17, v12

    .line 735
    .local v17, "tempNum":I
    move v12, v10

    .line 736
    move/from16 v10, v17

    .line 738
    .end local v17    # "tempNum":I
    :cond_b
    move v14, v12

    .local v14, "num":I
    :goto_4
    if-gt v14, v10, :cond_c

    .line 739
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 738
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 740
    :cond_c
    const/4 v5, 0x0

    goto :goto_3

    .line 744
    .end local v14    # "num":I
    :cond_d
    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 745
    const-string/jumbo v16, ""

    goto :goto_3

    .line 752
    .end local v11    # "nItem":Ljava/lang/Integer;
    :cond_e
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v18

    if-eqz v18, :cond_12

    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v5, v0, :cond_12

    .line 753
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->ConvertStrtoInt(Ljava/lang/String;)I

    move-result v10

    .line 754
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v10, v0, :cond_f

    .line 756
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 758
    :cond_f
    const-string/jumbo v16, ""

    .line 760
    if-le v12, v10, :cond_10

    .line 762
    move/from16 v17, v12

    .line 763
    .restart local v17    # "tempNum":I
    move v12, v10

    .line 764
    move/from16 v10, v17

    .line 766
    .end local v17    # "tempNum":I
    :cond_10
    move v14, v12

    .restart local v14    # "num":I
    :goto_5
    if-gt v14, v10, :cond_11

    .line 767
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 766
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 768
    :cond_11
    const/4 v5, 0x0

    .line 789
    .end local v14    # "num":I
    :goto_6
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v2, v0, [I

    .line 790
    .local v2, "arrRange":[I
    const/4 v8, 0x0

    .local v8, "idx":I
    :goto_7
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v8, v0, :cond_15

    .line 792
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->ConvertStrtoInt(Ljava/lang/String;)I

    move-result v18

    aput v18, v2, v8

    .line 790
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 772
    .end local v2    # "arrRange":[I
    .end local v8    # "idx":I
    :cond_12
    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 773
    .local v13, "nlastVal":Ljava/lang/Integer;
    if-eqz v16, :cond_14

    const-string/jumbo v18, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_14

    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Character;->isDigit(C)Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_14

    .line 775
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->ConvertStrtoInt(Ljava/lang/String;)I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 776
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_13

    .line 778
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 780
    :cond_13
    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 784
    :cond_14
    const v18, 0x7f070097

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V

    .line 785
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 794
    .end local v13    # "nlastVal":Ljava/lang/Integer;
    .restart local v2    # "arrRange":[I
    .restart local v8    # "idx":I
    :cond_15
    invoke-static {v2}, Ljava/util/Arrays;->sort([I)V

    .line 795
    array-length v0, v2

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v3, v0, [I

    .line 797
    const/4 v6, 0x0

    .line 799
    const/4 v8, 0x0

    :goto_8
    array-length v0, v2

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v8, v0, :cond_18

    .line 801
    aget v18, v2, v8

    if-eqz v18, :cond_16

    .line 803
    if-nez v8, :cond_17

    .line 805
    aget v18, v2, v8

    aput v18, v3, v6

    .line 806
    add-int/lit8 v6, v6, 0x1

    .line 799
    :cond_16
    :goto_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 808
    :cond_17
    add-int/lit8 v18, v6, -0x1

    aget v18, v3, v18

    aget v19, v2, v8

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_16

    .line 810
    aget v18, v2, v8

    aput v18, v3, v6

    .line 811
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 817
    .end local v2    # "arrRange":[I
    .end local v7    # "i":I
    .end local v8    # "idx":I
    .end local v9    # "listRange":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v15    # "strRange":Ljava/lang/String;
    .end local v16    # "temp":Ljava/lang/String;
    :cond_18
    move-object/from16 v0, p0

    iput v6, v0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageRangeCnt:I

    move-object/from16 v18, v3

    .line 818
    goto/16 :goto_0
.end method

.method private ConvertStrtoInt(Ljava/lang/String;)I
    .locals 6
    .param p1, "strIntVal"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 637
    const/4 v2, 0x0

    .line 638
    .local v2, "nVal":I
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-ne v3, v4, :cond_2

    .line 641
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 642
    iget v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->MAX_PAGE_NUM:I

    if-gt v2, v3, :cond_0

    if-ge v2, v4, :cond_3

    .line 644
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07024a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 645
    .local v1, "formattedString":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 646
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->MAX_PAGE_NUM:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 647
    :cond_1
    const/4 v3, -0x1

    .line 662
    .end local v1    # "formattedString":Ljava/lang/String;
    :goto_0
    return v3

    .line 650
    :catch_0
    move-exception v0

    .line 652
    .local v0, "exception":Ljava/lang/NumberFormatException;
    const-string/jumbo v3, "PrintActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " is not a number"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 653
    goto :goto_0

    .line 658
    .end local v0    # "exception":Ljava/lang/NumberFormatException;
    :cond_2
    const-string/jumbo v3, "PrintActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " is not a number"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 659
    goto :goto_0

    :cond_3
    move v3, v2

    .line 662
    goto :goto_0
.end method

.method private SubmitThreadStart()V
    .locals 8

    .prologue
    .line 1169
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMimeType:Ljava/lang/String;

    const-string/jumbo v1, "text/plain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1170
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mSubmitThreadHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_sPrinterId:Ljava/lang/String;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMimeType:Ljava/lang/String;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_strFilePath:Ljava/lang/String;

    iget v7, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nCopy:I

    invoke-direct/range {v0 .. v7}, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->start()V

    .line 1173
    :goto_0
    return-void

    .line 1172
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mSubmitThreadHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_sPrinterId:Ljava/lang/String;

    const-string/jumbo v5, "application/pdf"

    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    iget v7, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nCopy:I

    invoke-direct/range {v0 .. v7}, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->start()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/print/PrintActivity;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/print/PrintActivity;)Lcom/infraware/polarisoffice5/print/AccountDbHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->showPrintOptionLayout(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/print/PrintActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # I

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/infraware/polarisoffice5/print/PrintActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->bEditAccountMode:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/infraware/polarisoffice5/print/PrintActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->bEditAccountMode:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/infraware/polarisoffice5/print/PrintActivity;)Lorg/json/JSONArray;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mJsonArray:Lorg/json/JSONArray;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_sPrinterId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->jsonData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->jsonData:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->parseJson(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/infraware/polarisoffice5/print/PrintActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->hidePrintOptionLayout()V

    return-void
.end method

.method static synthetic access$2800(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/print/PrintActivity;)[Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mListThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/print/PrintActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountTokenList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->addAccountTokenList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$902(Lcom/infraware/polarisoffice5/print/PrintActivity;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/print/PrintActivity;
    .param p1, "x1"    # Landroid/widget/ArrayAdapter;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListAdapter:Landroid/widget/ArrayAdapter;

    return-object p1
.end method

.method private doWhileCursorToArray(II)V
    .locals 6
    .param p1, "nAccountIndex"    # I
    .param p2, "nImageID"    # I

    .prologue
    .line 1104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    .line 1105
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->getAllColumns()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    .line 1106
    const/4 v5, 0x0

    .line 1108
    .local v5, "tmpStr":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1109
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 1110
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v2, "AccountId"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1111
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1112
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/4 v3, 0x0

    const/4 v4, 0x1

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZLjava/lang/String;)V

    .line 1113
    add-int/lit8 p1, p1, 0x1

    .line 1114
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountTokenList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v3, "AccountToken"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1116
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountCount:I

    .line 1117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1118
    return-void
.end method

.method private getDbAccountId(Ljava/lang/String;)Z
    .locals 5
    .param p1, "accountId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 1121
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    .line 1122
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    invoke-virtual {v2, p1}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->getAccountIdColumns(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    .line 1124
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1125
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v4, "AccountId"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1127
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v4, "AccountToken"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->getDbToken:Ljava/lang/String;

    .line 1141
    :cond_1
    :goto_0
    return v1

    .line 1132
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    const-string/jumbo v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_3

    .line 1134
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    const-string/jumbo v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1136
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    aget-object v2, v2, v0

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eq v2, v1, :cond_1

    .line 1134
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1141
    .end local v0    # "i":I
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private hidePrintOptionLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 453
    const v3, 0x7f0b01c6

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 454
    .local v1, "PrintOptionLayout":Landroid/widget/LinearLayout;
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 456
    const v3, 0x7f0b01c5

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 457
    .local v0, "PrintListView":Landroid/widget/ListView;
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 459
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v4, 0x7f0701ad

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 460
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v4, 0x7f020003

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonImage(I)V

    .line 461
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/16 v4, 0x68

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setEvent(I)V

    .line 462
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 464
    const v3, 0x7f0b0167

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 465
    .local v2, "title":Landroid/widget/TextView;
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 466
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_bShowPrintOption:Z

    .line 468
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 472
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 475
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 476
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 477
    :cond_1
    return-void
.end method

.method private initData()V
    .locals 4

    .prologue
    .line 404
    new-instance v0, Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    .line 405
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->open()Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    .line 407
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountTokenList:Ljava/util/ArrayList;

    .line 409
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    .line 410
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->getAllColumns()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    .line 411
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 413
    iget v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_AccountIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_AccountIndex:I

    .line 414
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountTokenList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v3, "AccountToken"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountCount:I

    .line 417
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 419
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_aPrintListItem:Ljava/util/ArrayList;

    .line 421
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;

    .line 423
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;

    .line 425
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->addAccountTokenList:Ljava/util/ArrayList;

    .line 429
    return-void
.end method

.method private initListThreadHandler()V
    .locals 1

    .prologue
    .line 1047
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$13;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mListThreadHandler:Landroid/os/Handler;

    .line 1076
    return-void
.end method

.method private initRadioButton()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 480
    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 481
    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 482
    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 483
    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 486
    const v4, 0x7f0b01c9

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 487
    .local v1, "printAllRadio":Landroid/widget/LinearLayout;
    const v4, 0x7f0b01cb

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 490
    .local v2, "printSomeRadio":Landroid/widget/LinearLayout;
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$7;

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity$7;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;Landroid/widget/LinearLayout;)V

    .line 515
    .local v0, "clickListener":Landroid/view/View$OnClickListener;
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 516
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 520
    new-instance v3, Lcom/infraware/polarisoffice5/print/PrintActivity$8;

    invoke-direct {v3, p0, v1, v2}, Lcom/infraware/polarisoffice5/print/PrintActivity$8;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;)V

    .line 556
    .local v3, "touchListener":Landroid/view/View$OnTouchListener;
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 557
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 560
    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    new-instance v5, Lcom/infraware/polarisoffice5/print/PrintActivity$9;

    invoke-direct {v5, p0, v2, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity$9;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;Landroid/widget/LinearLayout;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 612
    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    new-instance v5, Lcom/infraware/polarisoffice5/print/PrintActivity$10;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$10;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/common/WheelButton;->setOnKeypadListener(Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;)V

    .line 633
    return-void
.end method

.method private initSubmitHandler()V
    .locals 1

    .prologue
    .line 1146
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$14;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$14;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mSubmitThreadHandler:Landroid/os/Handler;

    .line 1166
    return-void
.end method

.method private onToastMessage(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 1276
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1277
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    .line 1280
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 1281
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1282
    return-void

    .line 1279
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method private onToastMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "strMsg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1285
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1286
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    .line 1289
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 1290
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1291
    return-void

    .line 1288
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private parseJson(Ljava/lang/String;)V
    .locals 8
    .param p1, "jsonData"    # Ljava/lang/String;

    .prologue
    .line 1080
    :try_start_0
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 1081
    if-nez p1, :cond_1

    .line 1100
    :cond_0
    :goto_0
    return-void

    .line 1083
    :cond_1
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mJsonObject:Lorg/json/JSONObject;

    .line 1084
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mJsonObject:Lorg/json/JSONObject;

    if-eqz v6, :cond_0

    .line 1086
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mJsonObject:Lorg/json/JSONObject;

    const-string/jumbo v7, "printers"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    iput-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mJsonArray:Lorg/json/JSONArray;

    .line 1088
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mJsonArray:Lorg/json/JSONArray;

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 1089
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mJsonArray:Lorg/json/JSONArray;

    invoke-virtual {v6, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 1090
    .local v5, "temp":Lorg/json/JSONObject;
    const-string/jumbo v6, "id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1091
    .local v2, "printerId":Ljava/lang/String;
    const-string/jumbo v6, "displayName"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1092
    .local v3, "printerName":Ljava/lang/String;
    const-string/jumbo v6, "proxy"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1094
    .local v4, "printerProxy":Ljava/lang/String;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1088
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1096
    .end local v1    # "i":I
    .end local v2    # "printerId":Ljava/lang/String;
    .end local v3    # "printerName":Ljava/lang/String;
    .end local v4    # "printerProxy":Ljava/lang/String;
    .end local v5    # "temp":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 1097
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private showPrintOptionLayout(Ljava/lang/String;)V
    .locals 7
    .param p1, "strSelectedPrinterName"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    .line 432
    const v3, 0x7f0b01c5

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 433
    .local v0, "PrintListView":Landroid/widget/ListView;
    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 436
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v4, 0x7f020009

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonImage(I)V

    .line 437
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/16 v4, 0x51

    invoke-virtual {v3, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setEvent(I)V

    .line 439
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v3, p1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(Ljava/lang/String;)V

    .line 440
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 442
    const v3, 0x7f0b0167

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 443
    .local v2, "title":Landroid/widget/TextView;
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 445
    const v3, 0x7f0b01c6

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 446
    .local v1, "PrintOptionLayout":Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 447
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_bShowPrintOption:Z

    .line 448
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 450
    return-void
.end method


# virtual methods
.method public AccountEditmodePopupWindow(ILandroid/view/View;I)V
    .locals 11
    .param p1, "parentCase"    # I
    .param p2, "caller"    # Landroid/view/View;
    .param p3, "parentID"    # I

    .prologue
    const/4 v6, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 904
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 905
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-nez v0, :cond_0

    .line 906
    new-instance v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mEditAccountListener:Landroid/view/View$OnClickListener;

    invoke-direct {v0, p0, v6, p2, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;-><init>(Landroid/app/Activity;ILandroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    .line 911
    :goto_0
    const v0, 0x7f0b0014

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    .line 913
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v0, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 914
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v4}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setOutsideTouchable(Z)V

    .line 918
    const/4 v2, 0x0

    .line 920
    .local v2, "nImageID":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->removeAllItems()V

    .line 922
    const v2, 0x7f020066

    .line 925
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 926
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    aget-object v5, v5, v1

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZLjava/lang/String;)V

    .line 925
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 908
    .end local v1    # "i":I
    .end local v2    # "nImageID":I
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v6, p2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setMenuType(ILandroid/view/View;)V

    .line 909
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mEditAccountListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 929
    .restart local v1    # "i":I
    .restart local v2    # "nImageID":I
    :cond_1
    invoke-direct {p0, v1, v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->doWhileCursorToArray(II)V

    .line 932
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v6, 0x92

    const v0, 0x7f0700d1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    move v7, v3

    move v8, v3

    move v9, v4

    invoke-virtual/range {v5 .. v10}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZLjava/lang/String;)V

    .line 933
    return-void
.end method

.method public ExportToPdf()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1191
    const/4 v1, 0x0

    .line 1193
    .local v1, "nPrintRange":[I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-ne v3, v5, :cond_0

    .line 1195
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->ConvertPageRange()[I

    move-result-object v1

    .line 1197
    if-nez v1, :cond_1

    .line 1199
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v3

    const/16 v4, 0x1a

    invoke-virtual {v3, p0, v4}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    .line 1225
    :goto_0
    return-void

    .line 1205
    :cond_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageRangeCnt:I

    .line 1210
    :cond_1
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1211
    .local v2, "savePath":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_strFilePath:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/infraware/common/util/FileUtils;->isSavableDirectory(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {}, Lcom/infraware/common/util/FileUtils;->isSdcard()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1213
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1214
    .local v0, "LOCAL_ROOT_PATH":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_strFilePath:Ljava/lang/String;

    invoke-static {v4}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1217
    .end local v0    # "LOCAL_ROOT_PATH":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMimeType:Ljava/lang/String;

    const-string/jumbo v4, "application/pdf"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v5, :cond_3

    .line 1218
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Lcom/infraware/common/util/FileUtils;->getFileNameWithoutExt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "_copy.pdf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    .line 1221
    :goto_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    iget v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageRangeCnt:I

    invoke-virtual {v3, v4, v5, v1}, Lcom/infraware/office/evengine/EvInterface;->IExportPDF(Ljava/lang/String;I[I)V

    goto :goto_0

    .line 1220
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Lcom/infraware/common/util/FileUtils;->getFileNameWithoutExt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ".pdf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mstrPdfExportFilePath:Ljava/lang/String;

    goto :goto_1
.end method

.method public OnSaveDoc(I)V
    .locals 2
    .param p1, "bOk"    # I

    .prologue
    const v0, 0x7f070230

    .line 1233
    sparse-switch p1, :sswitch_data_0

    .line 1264
    const v0, 0x7f070265

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    .line 1267
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    if-lez v0, :cond_0

    .line 1268
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-virtual {v0, p0, v1}, Lcom/infraware/polarisoffice5/common/DlgFactory;->dismisProgress(Landroid/app/Activity;I)V

    .line 1269
    iget v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V

    .line 1271
    :cond_0
    return-void

    .line 1235
    :sswitch_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->SubmitThreadStart()V

    goto :goto_0

    .line 1239
    :sswitch_1
    const v0, 0x7f07019e

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1243
    :sswitch_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1247
    :sswitch_3
    const v0, 0x7f0700a7

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1251
    :sswitch_4
    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1255
    :sswitch_5
    const v0, 0x7f070090

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1259
    :sswitch_6
    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1233
    :sswitch_data_0
    .sparse-switch
        -0x12 -> :sswitch_6
        -0x11 -> :sswitch_5
        -0x10 -> :sswitch_4
        -0x1 -> :sswitch_3
        0x1 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_2
    .end sparse-switch
.end method

.method public RequestPrintIntent()V
    .locals 2

    .prologue
    .line 1178
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nCopy:I

    .line 1179
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->getIntData()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_nCopy:I

    .line 1180
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->showDialog(I)V

    .line 1182
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMimeType:Ljava/lang/String;

    const-string/jumbo v1, "text/plain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1183
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->SubmitThreadStart()V

    .line 1188
    :goto_0
    return-void

    .line 1185
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->ExportToPdf()V

    goto :goto_0
.end method

.method public actionBarPopupWindow(ILandroid/view/View;I)V
    .locals 11
    .param p1, "parentCase"    # I
    .param p2, "caller"    # Landroid/view/View;
    .param p3, "parentID"    # I

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 866
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 867
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->dismissMenuPopup()Z

    .line 900
    :goto_0
    return-void

    .line 871
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-nez v0, :cond_2

    .line 872
    new-instance v0, Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPopoverListener:Landroid/view/View$OnClickListener;

    invoke-direct {v0, p0, v5, p2, v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;-><init>(Landroid/app/Activity;ILandroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    .line 880
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v0, v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 881
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v4}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setOutsideTouchable(Z)V

    .line 886
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    if-nez v0, :cond_1

    .line 888
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    .line 889
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    const-string/jumbo v3, "com.google"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    .line 892
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 893
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    aget-object v3, v3, v1

    iget-object v5, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZLjava/lang/String;)V

    .line 892
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 875
    .end local v1    # "i":I
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0, v5, p2}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setMenuType(ILandroid/view/View;)V

    .line 876
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->removeAllItems()V

    .line 877
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPopoverListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->setClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 896
    .restart local v1    # "i":I
    :cond_3
    invoke-direct {p0, v1, v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->doWhileCursorToArray(II)V

    .line 899
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    const/16 v6, 0x103

    const v0, 0x7f070127

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    move v7, v2

    move v8, v2

    move v9, v4

    invoke-virtual/range {v5 .. v10}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->addMenu(IIZZLjava/lang/String;)V

    goto :goto_0
.end method

.method public actionTitleBarButtonClick(ILandroid/view/View;)V
    .locals 3
    .param p1, "eEventType"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const v2, 0x7f0b0014

    .line 830
    sparse-switch p1, :sswitch_data_0

    .line 851
    :goto_0
    return-void

    .line 833
    :sswitch_0
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 834
    const v0, 0x7f070092

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V

    goto :goto_0

    .line 837
    :cond_0
    const/4 v0, 0x6

    const v1, 0x7f020003

    invoke-virtual {p0, v0, p2, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->actionBarPopupWindow(ILandroid/view/View;I)V

    .line 838
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v0, :cond_1

    .line 840
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 841
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->show()V

    goto :goto_0

    .line 844
    :cond_1
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 848
    :sswitch_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->RequestPrintIntent()V

    goto :goto_0

    .line 830
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_1
        0x68 -> :sswitch_0
    .end sparse-switch
.end method

.method public dismissMenuPopup()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 854
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v1, :cond_0

    .line 855
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->bEditAccountMode:Z

    .line 856
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->dismiss()V

    .line 857
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    .line 858
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 859
    const/4 v0, 0x1

    .line 861
    :cond_0
    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 966
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/common/baseactivity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 967
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 968
    packed-switch p1, :pswitch_data_0

    .line 1008
    :cond_0
    :goto_0
    return-void

    .line 971
    :pswitch_0
    const-string/jumbo v0, "AccountToken"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 972
    .local v7, "Accounttoken":Ljava/lang/String;
    iput-object v7, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    .line 973
    const-string/jumbo v0, "GoogleAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 975
    .local v8, "accountId":Ljava/lang/String;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 976
    invoke-direct {p0, v8}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getDbAccountId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 978
    const v0, 0x7f0701b0

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V

    goto :goto_0

    .line 985
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->addAccountTokenList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 986
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mListThreadHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    const-string/jumbo v6, "AddAccount"

    invoke-direct/range {v0 .. v6}, Lcom/infraware/polarisoffice5/print/PrinterListThread;-><init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/PrinterListThread;->start()V

    .line 988
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    invoke-virtual {v0, v8, v1}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 990
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 991
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 993
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 999
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 1000
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 1001
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 968
    nop

    :pswitch_data_0
    .packed-switch 0x1002
        :pswitch_0
    .end packed-switch
.end method

.method public onAddAccount()V
    .locals 3

    .prologue
    .line 1012
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1014
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "print_account"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1015
    const/16 v1, 0x1002

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1017
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 937
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->initRadioButton()V

    .line 939
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 941
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->dismissMenuPopup()Z

    .line 950
    :goto_0
    return-void

    .line 944
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_bShowPrintOption:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 946
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->hidePrintOptionLayout()V

    goto :goto_0

    .line 949
    :cond_1
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1296
    invoke-static {}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->configurationChanged()V

    .line 1298
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->refresh()V

    .line 1303
    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1304
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    invoke-super/range {p0 .. p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 155
    const v0, 0x7f03003e

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->setContentView(I)V

    .line 156
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mContext:Landroid/content/Context;

    .line 158
    new-instance v0, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f0b01c3

    const/4 v2, 0x2

    const/16 v3, 0x68

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 160
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f0701ad

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 161
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f020003

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonImage(I)V

    .line 162
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 165
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;

    .line 166
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 167
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 170
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$1;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPopoverListener:Landroid/view/View$OnClickListener;

    .line 209
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$2;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mEditAccountListener:Landroid/view/View$OnClickListener;

    .line 236
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->initData()V

    .line 237
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    .line 238
    .local v11, "intent":Landroid/content/Intent;
    const-string/jumbo v0, "key_filename"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_strFilePath:Ljava/lang/String;

    .line 239
    new-instance v9, Ljava/io/File;

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_strFilePath:Ljava/lang/String;

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 241
    .local v9, "file":Ljava/io/File;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v13

    .line 243
    .local v13, "mtm":Landroid/webkit/MimeTypeMap;
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    .line 245
    .local v10, "fileExtension":Ljava/lang/String;
    invoke-virtual {v13, v10}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMimeType:Ljava/lang/String;

    .line 247
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mMimeType:Ljava/lang/String;

    const-string/jumbo v1, "text/plain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 249
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 250
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p0}, Lcom/infraware/office/evengine/EvInterface;->ISetPrintListener(Lcom/infraware/office/evengine/EvListener$PrintListener;)V

    .line 251
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->MAX_PAGE_NUM:I

    .line 256
    :goto_0
    const v0, 0x7f0b01c5

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListView:Landroid/widget/ListView;

    .line 257
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 258
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 260
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 262
    .local v14, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListView:Landroid/widget/ListView;

    invoke-virtual {v0, v14}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 263
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListView:Landroid/widget/ListView;

    new-instance v1, Lcom/infraware/polarisoffice5/print/PrintActivity$3;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$3;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 273
    const v0, 0x7f0b01ce

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/WheelButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    .line 274
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    const/4 v1, 0x2

    const/16 v2, 0x1d

    invoke-virtual {v0, p0, v1, v2}, Lcom/infraware/polarisoffice5/common/WheelButton;->initLayout(Landroid/content/Context;II)V

    .line 275
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    const/4 v1, 0x1

    const/16 v2, 0x14

    const/16 v3, 0x14

    const/4 v4, 0x1

    const/4 v5, 0x1

    const v6, 0x7f0701b5

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/polarisoffice5/common/WheelButton;->addIntData(IIIIII)V

    .line 276
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setTitleVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setEndMarginVisivility(I)V

    .line 278
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPageCopyCnt:Lcom/infraware/polarisoffice5/common/WheelButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setEnabled(Z)V

    .line 280
    const v0, 0x7f0b01ca

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintAllCheckBox:Landroid/widget/CheckBox;

    .line 281
    const v0, 0x7f0b01cd

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;

    .line 282
    const v0, 0x7f0b01cc

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    .line 284
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->Watcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 285
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->initRadioButton()V

    .line 286
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->initSubmitHandler()V

    .line 287
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->initListThreadHandler()V

    .line 289
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 290
    const v0, 0x7f070092

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V

    .line 338
    :cond_0
    :goto_1
    return-void

    .line 254
    .end local v14    # "selector":Landroid/graphics/drawable/Drawable;
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->MAX_PAGE_NUM:I

    goto/16 :goto_0

    .line 294
    .restart local v14    # "selector":Landroid/graphics/drawable/Drawable;
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    .line 295
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    if-eqz v0, :cond_3

    .line 296
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    const-string/jumbo v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    .line 297
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->getAllColumns()Landroid/database/Cursor;

    move-result-object v8

    .line 299
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    const-string/jumbo v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_4

    .line 301
    const/4 v12, 0x0

    .line 302
    .local v12, "mPopupDialog":Landroid/app/AlertDialog;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0701cd

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0700d9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07005f

    new-instance v2, Lcom/infraware/polarisoffice5/print/PrintActivity$5;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$5;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070061

    new-instance v2, Lcom/infraware/polarisoffice5/print/PrintActivity$4;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/print/PrintActivity$4;-><init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v12

    .line 318
    const/4 v0, 0x0

    invoke-virtual {v12, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 319
    invoke-virtual {v12}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 322
    .end local v12    # "mPopupDialog":Landroid/app/AlertDialog;
    :cond_4
    const/4 v7, 0x0

    .line 323
    .local v7, "accounts":[Landroid/accounts/Account;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    if-eqz v0, :cond_5

    .line 324
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    const-string/jumbo v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 326
    :cond_5
    if-eqz v7, :cond_6

    array-length v0, v7

    if-lez v0, :cond_6

    .line 328
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mListThreadHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/infraware/polarisoffice5/print/PrinterListThread;-><init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/PrinterListThread;->start()V

    goto/16 :goto_1

    .line 330
    :cond_6
    iget v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountCount:I

    if-lez v0, :cond_0

    .line 332
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mListThreadHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountTokenList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v6, "AddAccount"

    invoke-direct/range {v0 .. v6}, Lcom/infraware/polarisoffice5/print/PrinterListThread;-><init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/PrinterListThread;->start()V

    goto/16 :goto_1
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 395
    packed-switch p1, :pswitch_data_0

    .line 399
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 397
    :pswitch_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createPrintProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$PrintProgressDlg;

    move-result-object v0

    goto :goto_0

    .line 395
    nop

    :pswitch_data_0
    .packed-switch 0x1a
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 122
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_aPrintListItem:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_aPrintListItem:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 125
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_aPrintListItem:Ljava/util/ArrayList;

    .line 126
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->close()V

    .line 129
    :cond_0
    const v1, 0x7f0b01c3

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 130
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->unbindDrawables(Landroid/view/View;)V

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;

    if-eqz v1, :cond_2

    .line 135
    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/print/PrintActivity$CloseActionReceiver;

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 136
    :cond_2
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onDestroy()V

    .line 137
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 1
    .param p1, "nLocale"    # I

    .prologue
    .line 823
    const v0, 0x7f0701ad

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->setTitle(I)V

    .line 824
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->m_aPrintListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 825
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity;->mAdapter:Lcom/infraware/polarisoffice5/common/MultiAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->notifyDataSetChanged()V

    .line 827
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onPause()V

    .line 142
    return-void
.end method
