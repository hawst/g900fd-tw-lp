.class Lcom/infraware/polarisoffice5/print/PrinterListThread$GetAuthTokenCallback;
.super Ljava/lang/Object;
.source "PrinterListThread.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/PrinterListThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetAuthTokenCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrinterListThread;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/print/PrinterListThread;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread$GetAuthTokenCallback;->this$0:Lcom/infraware/polarisoffice5/print/PrinterListThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/print/PrinterListThread;Lcom/infraware/polarisoffice5/print/PrinterListThread$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/print/PrinterListThread;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/print/PrinterListThread$1;

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/PrinterListThread$GetAuthTokenCallback;-><init>(Lcom/infraware/polarisoffice5/print/PrinterListThread;)V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p1, "result":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 168
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 185
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v1

    .line 177
    .local v1, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v1}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 178
    .end local v1    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v1

    .line 180
    .local v1, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v1}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_0

    .line 181
    .end local v1    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v1

    .line 183
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
