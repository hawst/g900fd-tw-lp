.class public Lcom/infraware/polarisoffice5/print/PrinterListThread;
.super Ljava/lang/Thread;
.source "PrinterListThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/print/PrinterListThread$1;,
        Lcom/infraware/polarisoffice5/print/PrinterListThread$GetAuthTokenCallback;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private Token:Ljava/lang/String;

.field private conn:Ljava/net/HttpURLConnection;

.field private mAccountArray:[Landroid/accounts/Account;

.field private mAccountManager:Landroid/accounts/AccountManager;

.field private mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

.field private mContext:Landroid/app/Activity;

.field private mCount:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mInputStream:Ljava/io/InputStream;

.field private selectAccount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 28
    const-string/jumbo v0, "PrinterListThread"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->TAG:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    .line 40
    const-string/jumbo v0, "1"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mCount:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->selectAccount:I

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 1
    .param p1, "mContext"    # Landroid/app/Activity;
    .param p2, "mAccountManager"    # Landroid/accounts/AccountManager;
    .param p3, "mAccountArray"    # [Landroid/accounts/Account;
    .param p4, "mHandler"    # Landroid/os/Handler;
    .param p5, "Token"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 28
    const-string/jumbo v0, "PrinterListThread"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->TAG:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    .line 40
    const-string/jumbo v0, "1"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mCount:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->selectAccount:I

    .line 49
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    .line 50
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mAccountManager:Landroid/accounts/AccountManager;

    .line 51
    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mAccountArray:[Landroid/accounts/Account;

    .line 52
    iput-object p4, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    .line 53
    iput-object p5, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;I)V
    .locals 1
    .param p1, "mContext"    # Landroid/app/Activity;
    .param p2, "mAccountManager"    # Landroid/accounts/AccountManager;
    .param p3, "mAccountArray"    # [Landroid/accounts/Account;
    .param p4, "mHandler"    # Landroid/os/Handler;
    .param p5, "Token"    # Ljava/lang/String;
    .param p6, "selectAccount"    # I

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 28
    const-string/jumbo v0, "PrinterListThread"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->TAG:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    .line 40
    const-string/jumbo v0, "1"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mCount:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->selectAccount:I

    .line 73
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    .line 74
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mAccountManager:Landroid/accounts/AccountManager;

    .line 75
    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mAccountArray:[Landroid/accounts/Account;

    .line 76
    iput-object p4, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    .line 77
    iput-object p5, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    .line 78
    iput p6, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->selectAccount:I

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mContext"    # Landroid/app/Activity;
    .param p2, "mAccountManager"    # Landroid/accounts/AccountManager;
    .param p3, "mAccountArray"    # [Landroid/accounts/Account;
    .param p4, "mHandler"    # Landroid/os/Handler;
    .param p5, "Token"    # Ljava/lang/String;
    .param p6, "mCount"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 28
    const-string/jumbo v0, "PrinterListThread"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->TAG:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    .line 40
    const-string/jumbo v0, "1"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mCount:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->selectAccount:I

    .line 61
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    .line 62
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mAccountManager:Landroid/accounts/AccountManager;

    .line 63
    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mAccountArray:[Landroid/accounts/Account;

    .line 64
    iput-object p4, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    .line 65
    iput-object p5, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    .line 66
    iput-object p6, p0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mCount:Ljava/lang/String;

    .line 67
    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 83
    invoke-super/range {p0 .. p0}, Ljava/lang/Thread;->run()V

    .line 85
    const/4 v15, 0x0

    .line 86
    .local v15, "result":Ljava/lang/String;
    const/4 v14, 0x0

    .line 90
    .local v14, "read":I
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 91
    new-instance v17, Ljava/net/URL;

    const-string/jumbo v1, "http://www.google.com/cloudprint/search"

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 95
    .local v17, "url":Ljava/net/URL;
    const/4 v8, 0x0

    .line 96
    .local v8, "accFut":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mCount:Ljava/lang/String;

    const-string/jumbo v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mAccountManager:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mAccountArray:[Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->selectAccount:I

    aget-object v2, v2, v3

    const-string/jumbo v3, "cloudprint"

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    new-instance v6, Lcom/infraware/polarisoffice5/print/PrinterListThread$GetAuthTokenCallback;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v7}, Lcom/infraware/polarisoffice5/print/PrinterListThread$GetAuthTokenCallback;-><init>(Lcom/infraware/polarisoffice5/print/PrinterListThread;Lcom/infraware/polarisoffice5/print/PrinterListThread$1;)V

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v8

    .line 101
    invoke-interface {v8}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    .line 102
    .local v9, "authTokenBundle":Landroid/os/Bundle;
    const-string/jumbo v1, "authtoken"

    invoke-virtual {v9, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    .line 105
    .end local v9    # "authTokenBundle":Landroid/os/Bundle;
    :cond_0
    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->conn:Ljava/net/HttpURLConnection;

    .line 106
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->conn:Ljava/net/HttpURLConnection;

    const-string/jumbo v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "GoogleLogin auth="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->conn:Ljava/net/HttpURLConnection;

    const-string/jumbo v2, "POST"

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 108
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->conn:Ljava/net/HttpURLConnection;

    const/16 v2, 0x1388

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 109
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->conn:Ljava/net/HttpURLConnection;

    const/16 v2, 0x1388

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 110
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->conn:Ljava/net/HttpURLConnection;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 111
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->conn:Ljava/net/HttpURLConnection;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 113
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mInputStream:Ljava/io/InputStream;

    .line 115
    const/16 v1, 0x400

    new-array v10, v1, [B

    .line 117
    .local v10, "buffer":[B
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1, v10}, Ljava/io/InputStream;->read([B)I

    move-result v14

    const/4 v1, -0x1

    if-eq v14, v1, :cond_2

    .line 118
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v1, v10, v2, v14}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 125
    .end local v8    # "accFut":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .end local v10    # "buffer":[B
    .end local v17    # "url":Ljava/net/URL;
    :catch_0
    move-exception v12

    .line 127
    .local v12, "e":Ljava/net/MalformedURLException;
    :try_start_1
    const-string/jumbo v1, "PrinterListThread"

    invoke-virtual {v12}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 141
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 142
    .local v11, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "token"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v1, "jsonData"

    invoke-virtual {v11, v1, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 147
    new-instance v13, Landroid/os/Message;

    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    .line 148
    .local v13, "message":Landroid/os/Message;
    const/16 v1, 0x3e8

    iput v1, v13, Landroid/os/Message;->what:I

    .line 149
    invoke-virtual {v13, v11}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    .line 160
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v12    # "e":Ljava/net/MalformedURLException;
    .end local v13    # "message":Landroid/os/Message;
    :cond_1
    :goto_1
    return-void

    .line 120
    .restart local v8    # "accFut":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .restart local v10    # "buffer":[B
    .restart local v17    # "url":Ljava/net/URL;
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 123
    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    .end local v15    # "result":Ljava/lang/String;
    .local v16, "result":Ljava/lang/String;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 141
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 142
    .restart local v11    # "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "token"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v1, "jsonData"

    move-object/from16 v0, v16

    invoke-virtual {v11, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_3

    .line 147
    new-instance v13, Landroid/os/Message;

    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    .line 148
    .restart local v13    # "message":Landroid/os/Message;
    const/16 v1, 0x3e8

    iput v1, v13, Landroid/os/Message;->what:I

    .line 149
    invoke-virtual {v13, v11}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v13    # "message":Landroid/os/Message;
    :cond_3
    :goto_2
    move-object/from16 v15, v16

    .line 157
    .end local v16    # "result":Ljava/lang/String;
    .restart local v15    # "result":Ljava/lang/String;
    goto :goto_1

    .line 128
    .end local v8    # "accFut":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .end local v10    # "buffer":[B
    .end local v17    # "url":Ljava/net/URL;
    :catch_1
    move-exception v12

    .line 129
    .local v12, "e":Ljava/io/IOException;
    :try_start_5
    const-string/jumbo v1, "PrinterListThread"

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 139
    :try_start_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 141
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 142
    .restart local v11    # "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "token"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v1, "jsonData"

    invoke-virtual {v11, v1, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 147
    new-instance v13, Landroid/os/Message;

    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    .line 148
    .restart local v13    # "message":Landroid/os/Message;
    const/16 v1, 0x3e8

    iput v1, v13, Landroid/os/Message;->what:I

    .line 149
    invoke-virtual {v13, v11}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_1

    .line 153
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v13    # "message":Landroid/os/Message;
    :catch_2
    move-exception v12

    .line 154
    const-string/jumbo v1, "PrinterListThread"

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    const v3, 0x7f070092

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_3
    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto/16 :goto_1

    .line 130
    .end local v12    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v12

    .line 132
    .local v12, "e":Landroid/accounts/OperationCanceledException;
    :try_start_7
    invoke-virtual {v12}, Landroid/accounts/OperationCanceledException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 139
    :try_start_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 141
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 142
    .restart local v11    # "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "token"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v1, "jsonData"

    invoke-virtual {v11, v1, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 147
    new-instance v13, Landroid/os/Message;

    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    .line 148
    .restart local v13    # "message":Landroid/os/Message;
    const/16 v1, 0x3e8

    iput v1, v13, Landroid/os/Message;->what:I

    .line 149
    invoke-virtual {v13, v11}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_1

    .line 153
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v13    # "message":Landroid/os/Message;
    :catch_4
    move-exception v12

    .line 154
    .local v12, "e":Ljava/io/IOException;
    const-string/jumbo v1, "PrinterListThread"

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    const v3, 0x7f070092

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 133
    .end local v12    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v12

    .line 135
    .local v12, "e":Landroid/accounts/AuthenticatorException;
    :try_start_9
    invoke-virtual {v12}, Landroid/accounts/AuthenticatorException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 139
    :try_start_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 141
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 142
    .restart local v11    # "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "token"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v1, "jsonData"

    invoke-virtual {v11, v1, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 147
    new-instance v13, Landroid/os/Message;

    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    .line 148
    .restart local v13    # "message":Landroid/os/Message;
    const/16 v1, 0x3e8

    iput v1, v13, Landroid/os/Message;->what:I

    .line 149
    invoke-virtual {v13, v11}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_1

    .line 153
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v13    # "message":Landroid/os/Message;
    :catch_6
    move-exception v12

    .line 154
    .local v12, "e":Ljava/io/IOException;
    const-string/jumbo v1, "PrinterListThread"

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    const v3, 0x7f070092

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 137
    .end local v12    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 139
    :try_start_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 141
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 142
    .restart local v11    # "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "token"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->Token:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v2, "jsonData"

    invoke-virtual {v11, v2, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_4

    .line 147
    new-instance v13, Landroid/os/Message;

    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    .line 148
    .restart local v13    # "message":Landroid/os/Message;
    const/16 v2, 0x3e8

    iput v2, v13, Landroid/os/Message;->what:I

    .line 149
    invoke-virtual {v13, v11}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 137
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v13    # "message":Landroid/os/Message;
    :cond_4
    :goto_4
    throw v1

    .line 153
    :catch_7
    move-exception v12

    .line 154
    .restart local v12    # "e":Ljava/io/IOException;
    const-string/jumbo v2, "PrinterListThread"

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    const v4, 0x7f070092

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto :goto_4

    .line 153
    .local v12, "e":Ljava/net/MalformedURLException;
    :catch_8
    move-exception v12

    .line 154
    .local v12, "e":Ljava/io/IOException;
    const-string/jumbo v1, "PrinterListThread"

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    const v3, 0x7f070092

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 153
    .end local v12    # "e":Ljava/io/IOException;
    .end local v15    # "result":Ljava/lang/String;
    .restart local v8    # "accFut":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .restart local v10    # "buffer":[B
    .restart local v16    # "result":Ljava/lang/String;
    .restart local v17    # "url":Ljava/net/URL;
    :catch_9
    move-exception v12

    .line 154
    .restart local v12    # "e":Ljava/io/IOException;
    const-string/jumbo v1, "PrinterListThread"

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;->mContext:Landroid/app/Activity;

    const v3, 0x7f070092

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto/16 :goto_2
.end method
