.class Lcom/infraware/polarisoffice5/print/PrintActivity$12;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V
    .locals 0

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$12;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1024
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x0

    .line 1026
    .local v1, "temp":Lorg/json/JSONObject;
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$12;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mJsonArray:Lorg/json/JSONArray;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1900(Lcom/infraware/polarisoffice5/print/PrintActivity;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2, p3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 1027
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$12;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const-string/jumbo v3, "id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_sPrinterId:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2002(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1032
    :goto_0
    if-eqz v1, :cond_0

    .line 1035
    :try_start_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$12;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const-string/jumbo v3, "displayName"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/infraware/polarisoffice5/print/PrintActivity;->showPrintOptionLayout(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1300(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1042
    :cond_0
    :goto_1
    return-void

    .line 1028
    :catch_0
    move-exception v0

    .line 1029
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 1036
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 1037
    .restart local v0    # "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
