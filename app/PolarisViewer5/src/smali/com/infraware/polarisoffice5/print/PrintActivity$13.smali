.class Lcom/infraware/polarisoffice5/print/PrintActivity$13;
.super Landroid/os/Handler;
.source "PrintActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;->initListThreadHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V
    .locals 0

    .prologue
    .line 1047
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 1049
    const-string/jumbo v2, "PrintActivity"

    const-string/jumbo v3, "handleMessage"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1051
    iget v1, p1, Landroid/os/Message;->what:I

    .line 1054
    .local v1, "id":I
    const/16 v2, 0x3e8

    if-ne v1, v2, :cond_0

    .line 1055
    const-string/jumbo v2, "PrintActivity"

    const-string/jumbo v3, "MESSAGE_GET_PRINT_LIST"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1061
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const-string/jumbo v3, "jsonData"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->jsonData:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2102(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1063
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const-string/jumbo v3, "token"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$502(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1065
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->jsonData:Ljava/lang/String;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/infraware/polarisoffice5/print/PrintActivity;->parseJson(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2200(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)V

    .line 1068
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2300(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f03003f

    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->printArrayList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    # setter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$902(Lcom/infraware/polarisoffice5/print/PrintActivity;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;

    .line 1069
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2500(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$900(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1071
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2500(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$13;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$2600(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1074
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method
