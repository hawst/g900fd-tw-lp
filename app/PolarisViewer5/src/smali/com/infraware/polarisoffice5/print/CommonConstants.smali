.class public Lcom/infraware/polarisoffice5/print/CommonConstants;
.super Ljava/lang/Object;
.source "CommonConstants.java"


# static fields
.field public static final AUTHORIZATION:Ljava/lang/String; = "Authorization"

.field public static final AddAccount:Ljava/lang/String; = "AddAccount"

.field public static final CLOUD_PRINT:Ljava/lang/String; = "cloudprint"

.field public static final CLOUD_PRINT_CAPABILITY_URL:Ljava/lang/String; = "http://www.google.com/cloudprint/printer"

.field public static final CLOUD_PRINT_SUBMIT_URL:Ljava/lang/String; = "http://www.google.com/cloudprint/submit"

.field public static final CLOUD_PRINT_URL:Ljava/lang/String; = "http://www.google.com/cloudprint/search"

.field public static final GET_PRINTER_ID:Ljava/lang/String; = "Printer Id"

.field public static final GET_PRINT_CAPABILITY_JSON_DATA:Ljava/lang/String; = "capabilityJsonData"

.field public static final GET_PRINT_LIST_JSON_DATA:Ljava/lang/String; = "jsonData"

.field public static final GET_PRINT_LIST_TOKEN:Ljava/lang/String; = "token"

.field public static final GOOGLE_ACCOUNT:Ljava/lang/String; = "com.google"

.field public static final HEADER_VALUE:Ljava/lang/String; = "GoogleLogin auth="

.field public static final JSON_ARRAY_NAME:Ljava/lang/String; = "printers"

.field public static final JSON_DISPLAYNAME:Ljava/lang/String; = "displayName"

.field public static final JSON_ID:Ljava/lang/String; = "id"

.field public static final JSON_PROXY:Ljava/lang/String; = "proxy"

.field public static final MESSAGE_ADD_ACCOUNT:I = 0x3eb

.field public static final MESSAGE_GET_ACCOUNT_EDIT:I = 0x3ec

.field public static final MESSAGE_GET_PRINT_CAPABILITY:I = 0x3e9

.field public static final MESSAGE_GET_PRINT_LIST:I = 0x3e8

.field public static final MESSAGE_GET_PRINT_SUBMIT:I = 0x3ea

.field public static final POST_METHOD:Ljava/lang/String; = "POST"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
