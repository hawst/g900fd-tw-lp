.class Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$3;
.super Ljava/lang/Object;
.source "AddPrinterAccountActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$3;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 159
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 164
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$3;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutError:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$100(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$3;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # invokes: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->setButtonStatus()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$500(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    .line 172
    return-void
.end method
