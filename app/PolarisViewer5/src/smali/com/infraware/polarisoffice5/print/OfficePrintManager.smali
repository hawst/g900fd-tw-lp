.class public Lcom/infraware/polarisoffice5/print/OfficePrintManager;
.super Ljava/lang/Object;
.source "OfficePrintManager.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;


# static fields
.field public static mInstance:Lcom/infraware/polarisoffice5/print/OfficePrintManager;


# instance fields
.field private mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    .line 23
    return-void
.end method

.method public static getInsTance()Lcom/infraware/polarisoffice5/print/OfficePrintManager;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mInstance:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mInstance:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    .line 28
    sget-object v0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mInstance:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    .line 31
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mInstance:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    goto :goto_0
.end method


# virtual methods
.method public isPossibleMakePdf()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    if-nez v0, :cond_0

    .line 74
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/print/OfficePrintJop;->isPossibleMakePdf()Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_1

    .line 78
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/print/OfficePrintJop;->isPossibleMakePdf()Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public makePdf()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    if-nez v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/print/OfficePrintJop;->makePdf()V

    goto :goto_0
.end method

.method public print(Landroid/app/Activity;Lcom/infraware/office/baseframe/EvBaseView;ILjava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Lcom/infraware/office/baseframe/EvBaseView;
    .param p3, "DocmentType"    # I
    .param p4, "filePath"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_POLARIS_PRINT()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    new-instance v0, Lcom/infraware/polarisoffice5/print/others/PolarisPrint;

    invoke-direct {v0, p2, p4}, Lcom/infraware/polarisoffice5/print/others/PolarisPrint;-><init>(Lcom/infraware/office/baseframe/EvBaseView;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    .line 55
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    invoke-interface {v0, p1}, Lcom/infraware/polarisoffice5/print/OfficePrintJop;->doPrint(Landroid/app/Activity;)V

    .line 56
    return-void

    .line 41
    :cond_0
    invoke-static {p1}, Lcom/infraware/common/define/CMModelDefine$B;->USE_GOOGLE_PRINT(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    const/4 v0, 0x5

    if-ne p3, v0, :cond_1

    .line 43
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;

    invoke-static {p4}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p3, v1, p4, p1}, Lcom/infraware/polarisoffice5/print/google/GooglePdfPrintAdapter;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    goto :goto_0

    .line 45
    :cond_1
    const/4 v0, 0x2

    if-ne p3, v0, :cond_2

    .line 46
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;

    invoke-static {p4}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p3, v1, p1}, Lcom/infraware/polarisoffice5/print/google/GoogleSheetPrintAdapter;-><init>(ILjava/lang/String;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    goto :goto_0

    .line 49
    :cond_2
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;

    invoke-static {p4}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p3, v1, p1}, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;-><init>(ILjava/lang/String;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    goto :goto_0

    .line 53
    :cond_3
    new-instance v0, Lcom/infraware/polarisoffice5/print/others/DefaultPrint;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/print/others/DefaultPrint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    goto :goto_0
.end method

.method public print(Landroid/app/Activity;Lcom/infraware/polarisoffice5/text/control/EditCtrl;Ljava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "editCtrl"    # Lcom/infraware/polarisoffice5/text/control/EditCtrl;
    .param p3, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_POLARIS_PRINT()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    new-instance v0, Lcom/infraware/polarisoffice5/print/others/PolarisPrintText;

    invoke-direct {v0, p3}, Lcom/infraware/polarisoffice5/print/others/PolarisPrintText;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    .line 67
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    invoke-interface {v0, p1}, Lcom/infraware/polarisoffice5/print/OfficePrintJop;->doPrint(Landroid/app/Activity;)V

    .line 69
    :cond_1
    return-void

    .line 63
    :cond_2
    invoke-static {p1}, Lcom/infraware/common/define/CMModelDefine$B;->USE_GOOGLE_PRINT(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    invoke-static {p3}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;-><init>(Landroid/content/Context;Lcom/infraware/polarisoffice5/text/control/EditCtrl;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->mOfficePrintJob:Lcom/infraware/polarisoffice5/print/OfficePrintJop;

    goto :goto_0
.end method
