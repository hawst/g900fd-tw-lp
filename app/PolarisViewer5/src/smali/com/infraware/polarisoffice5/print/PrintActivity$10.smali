.class Lcom/infraware/polarisoffice5/print/PrintActivity$10;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;->initRadioButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V
    .locals 0

    .prologue
    .line 612
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHideKeypad()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 624
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 625
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 626
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 627
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 629
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    .line 631
    :cond_0
    return-void
.end method

.method public onShowKeypad()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 615
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 617
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 619
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$200(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$10;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 621
    :cond_0
    return-void
.end method
