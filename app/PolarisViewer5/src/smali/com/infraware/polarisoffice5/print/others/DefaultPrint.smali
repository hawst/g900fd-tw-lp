.class public Lcom/infraware/polarisoffice5/print/others/DefaultPrint;
.super Ljava/lang/Object;
.source "DefaultPrint.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/print/OfficePrintJop;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doPrint(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 20
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    .line 21
    .local v0, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    instance-of v2, p1, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IsWebMode()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 23
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070316

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 24
    .local v1, "mToast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 31
    .end local v1    # "mToast":Landroid/widget/Toast;
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "FT03"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 29
    const/16 v2, 0x1e

    invoke-virtual {p1, v2}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method public isPossibleMakePdf()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public makePdf()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method
