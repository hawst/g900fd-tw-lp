.class Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;
.super Landroid/os/AsyncTask;
.source "GoogleTextPrintAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

.field final synthetic val$callback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

.field final synthetic val$cancellationSignal:Landroid/os/CancellationSignal;

.field final synthetic val$descriptor:Landroid/os/ParcelFileDescriptor;

.field final synthetic val$pages:[Landroid/print/PageRange;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;[Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$cancellationSignal:Landroid/os/CancellationSignal;

    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$callback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    iput-object p4, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$pages:[Landroid/print/PageRange;

    iput-object p5, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$descriptor:Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 420
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 13
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v12, 0x0

    .line 434
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$1200(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/manager/LineList;->reset()V

    .line 436
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v8

    if-nez v8, :cond_0

    .line 437
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    new-instance v9, Landroid/print/pdf/PrintedPdfDocument;

    iget-object v10, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$200(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/content/Context;

    move-result-object v10

    iget-object v11, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPrintAttributes:Landroid/print/PrintAttributes;
    invoke-static {v11}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$000(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/PrintAttributes;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Landroid/print/pdf/PrintedPdfDocument;-><init>(Landroid/content/Context;Landroid/print/PrintAttributes;)V

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8, v9}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;

    .line 439
    :cond_0
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalPageCount:I
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$800(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)I

    move-result v6

    .line 440
    .local v6, "totalPageCount":I
    const/4 v2, 0x0

    .line 441
    .local v2, "index":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_4

    .line 442
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$cancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v8}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 443
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$callback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    invoke-virtual {v8}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    .line 444
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v8

    invoke-virtual {v8}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 445
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8, v12}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;

    .line 486
    :goto_1
    return-object v12

    .line 448
    :cond_1
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$pages:[Landroid/print/PageRange;

    # invokes: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->pageInRange([Landroid/print/PageRange;I)Z
    invoke-static {v8, v9, v1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$1300(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;[Landroid/print/PageRange;I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 450
    :try_start_0
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/print/pdf/PrintedPdfDocument;->startPage(I)Landroid/graphics/pdf/PdfDocument$Page;

    move-result-object v5

    .line 451
    .local v5, "page":Landroid/graphics/pdf/PdfDocument$Page;
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # invokes: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->drawPage(Landroid/graphics/pdf/PdfDocument$Page;I)Z
    invoke-static {v8, v5, v2}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$1400(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/graphics/pdf/PdfDocument$Page;I)Z

    .line 452
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/print/pdf/PrintedPdfDocument;->finishPage(Landroid/graphics/pdf/PdfDocument$Page;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 453
    add-int/lit8 v2, v2, 0x1

    .line 441
    .end local v5    # "page":Landroid/graphics/pdf/PdfDocument$Page;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 454
    :catch_0
    move-exception v3

    .line 455
    .local v3, "ise":Ljava/lang/IllegalStateException;
    invoke-virtual {v3}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 456
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8, v12}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;

    goto :goto_1

    .line 458
    .end local v3    # "ise":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v4

    .line 459
    .local v4, "ne":Ljava/lang/NullPointerException;
    invoke-virtual {v4}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 464
    .end local v4    # "ne":Ljava/lang/NullPointerException;
    :cond_3
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$pages:[Landroid/print/PageRange;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$pages:[Landroid/print/PageRange;

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    aget-object v8, v8, v9

    invoke-virtual {v8}, Landroid/print/PageRange;->getEnd()I

    move-result v8

    if-le v1, v8, :cond_5

    .line 477
    :cond_4
    :try_start_1
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v8

    new-instance v9, Ljava/io/FileOutputStream;

    iget-object v10, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$descriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v10}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-virtual {v8, v9}, Landroid/print/pdf/PrintedPdfDocument;->writeTo(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v8

    invoke-virtual {v8}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 483
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8, v12}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;

    .line 485
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$callback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$pages:[Landroid/print/PageRange;

    invoke-virtual {v8, v9}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V

    goto :goto_1

    .line 467
    :cond_5
    const/4 v7, 0x0

    .local v7, "z":I
    :goto_2
    int-to-float v8, v7

    iget-object v9, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPagePerLine:F
    invoke-static {v9}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$1500(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F

    move-result v9

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    .line 468
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mLinelist:Lcom/infraware/polarisoffice5/text/manager/LineList;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$1200(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Lcom/infraware/polarisoffice5/text/manager/LineList;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/text/manager/LineList;->get()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 467
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 478
    .end local v7    # "z":I
    :catch_2
    move-exception v0

    .line 479
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$callback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 482
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v8

    invoke-virtual {v8}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 483
    iget-object v8, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v8, v12}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;

    goto/16 :goto_1

    .line 482
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    iget-object v9, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v9}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v9

    invoke-virtual {v9}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 483
    iget-object v9, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v9, v12}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;

    .line 482
    throw v8
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 420
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 492
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$callback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    invoke-virtual {v0}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    .line 495
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v0

    invoke-virtual {v0}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;

    .line 499
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;->val$cancellationSignal:Landroid/os/CancellationSignal;

    new-instance v1, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1$1;-><init>(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$1;)V

    invoke-virtual {v0, v1}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 430
    return-void
.end method
