.class Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;
.super Ljava/lang/Thread;
.source "AddPrinterAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RetriveAccountTask"
.end annotation


# instance fields
.field handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V
    .locals 1

    .prologue
    .line 455
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 456
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->handler:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;

    .prologue
    .line 455
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 460
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 462
    :try_start_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # invokes: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getAccountToken()Ljava/lang/String;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$1300(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->mAccountToken:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$1202(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 463
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->mAccountToken:Ljava/lang/String;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$1200(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 464
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->handler:Landroid/os/Handler;

    new-instance v4, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask$1;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask$1;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 482
    :goto_0
    return-void

    .line 471
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 472
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->mAccountToken:Ljava/lang/String;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$1200(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Ljava/lang/String;

    move-result-object v2

    .line 473
    .local v2, "strAccountToken":Ljava/lang/String;
    const-string/jumbo v3, "AccountToken"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 474
    const-string/jumbo v3, "GoogleAccount"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$700(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 475
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    const/4 v4, -0x1

    invoke-virtual {v3, v4, v1}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 476
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->finish()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 479
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "strAccountToken":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 480
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
