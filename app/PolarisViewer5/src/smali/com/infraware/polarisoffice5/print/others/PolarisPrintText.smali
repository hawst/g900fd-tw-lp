.class public Lcom/infraware/polarisoffice5/print/others/PolarisPrintText;
.super Ljava/lang/Object;
.source "PolarisPrintText.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/print/OfficePrintJop;


# instance fields
.field private mstrOpenFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "strOpenFilePath"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrintText;->mstrOpenFilePath:Ljava/lang/String;

    .line 17
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrintText;->mstrOpenFilePath:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public doPrint(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 23
    const-string/jumbo v1, "com.infraware.polarisprint2"

    invoke-static {p1, v1}, Lcom/infraware/common/util/Utils;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 25
    .local v0, "oIntent":Landroid/content/Intent;
    const-string/jumbo v1, "com.infraware.polarisprint2.PRINTVIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    const-string/jumbo v1, "com.infraware.polarisprint2"

    const-string/jumbo v2, "com.infraware.polarisprint2.OfficeLauncherActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 27
    const-string/jumbo v1, "key_filename"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrintText;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 35
    :goto_0
    return-void

    .line 32
    .end local v0    # "oIntent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    .restart local v0    # "oIntent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public isPossibleMakePdf()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public makePdf()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method
