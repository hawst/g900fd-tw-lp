.class public Lcom/infraware/polarisoffice5/print/others/PolarisPrint;
.super Ljava/lang/Object;
.source "PolarisPrint.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/print/OfficePrintJop;


# instance fields
.field private mView:Lcom/infraware/office/baseframe/EvBaseView;

.field private mstrOpenFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseView;Ljava/lang/String;)V
    .locals 1
    .param p1, "View"    # Lcom/infraware/office/baseframe/EvBaseView;
    .param p2, "strOpenFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrint;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    .line 18
    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrint;->mstrOpenFilePath:Ljava/lang/String;

    .line 21
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrint;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    .line 22
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrint;->mstrOpenFilePath:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public doPrint(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 27
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    .line 30
    .local v0, "evInterface":Lcom/infraware/office/evengine/EvInterface;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrint;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->GetOpenType()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IDocumentModified_Editor()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 33
    :cond_0
    const/16 v2, 0x1c

    invoke-virtual {p1, v2}, Landroid/app/Activity;->showDialog(I)V

    .line 49
    :goto_0
    return-void

    .line 38
    :cond_1
    const-string/jumbo v2, "com.infraware.polarisprint2"

    invoke-static {p1, v2}, Lcom/infraware/common/util/Utils;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 39
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 40
    .local v1, "oIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.infraware.polarisprint2.PRINTVIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string/jumbo v2, "com.infraware.polarisprint2"

    const-string/jumbo v3, "com.infraware.polarisprint2.OfficeLauncherActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    const-string/jumbo v2, "key_filename"

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/others/PolarisPrint;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 46
    .end local v1    # "oIntent":Landroid/content/Intent;
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/infraware/polarisoffice5/print/download/PolarisPrintDownloadActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .restart local v1    # "oIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public isPossibleMakePdf()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public makePdf()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method
