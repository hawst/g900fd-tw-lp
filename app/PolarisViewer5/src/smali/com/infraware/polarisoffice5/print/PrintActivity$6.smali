.class Lcom/infraware/polarisoffice5/print/PrintActivity$6;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 9
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 360
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 361
    .local v1, "CurPos":I
    if-eqz v1, :cond_0

    .line 363
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 365
    .local v0, "CurChar":C
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-nez v5, :cond_0

    const/16 v5, 0x2c

    if-eq v0, v5, :cond_0

    const/16 v5, 0x2d

    if-eq v0, v5, :cond_0

    .line 367
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v6, v6, Lcom/infraware/polarisoffice5/print/PrintActivity;->Watcher:Landroid/text/TextWatcher;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 368
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 370
    .local v3, "PreFix":Ljava/lang/String;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 371
    .local v2, "PostFix":Ljava/lang/String;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const v6, 0x7f070185

    # invokes: Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(I)V
    invoke-static {v5, v6}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1500(Lcom/infraware/polarisoffice5/print/PrintActivity;I)V

    .line 373
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v6, v6, Lcom/infraware/polarisoffice5/print/PrintActivity;->Watcher:Landroid/text/TextWatcher;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 374
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 378
    .end local v0    # "CurChar":C
    .end local v2    # "PostFix":Ljava/lang/String;
    .end local v3    # "PreFix":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 379
    .local v4, "strRange":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v5, v8, :cond_2

    .line 381
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5, v7}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 388
    :goto_0
    return-void

    .line 385
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$6;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5, v8}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 348
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 355
    return-void
.end method
