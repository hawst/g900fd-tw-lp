.class Lcom/infraware/polarisoffice5/print/PrintActivity$1;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 174
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v6

    .line 175
    .local v6, "id":I
    const/16 v0, 0x92

    if-ne v6, v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onAddAccount()V

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    const/16 v0, 0x103

    if-ne v6, v0, :cond_2

    .line 181
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const v1, 0x7f0b0014

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 182
    .local v14, "caller2":Landroid/view/View;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const/4 v1, 0x6

    const v2, 0x7f020003

    invoke-virtual {v0, v1, v14, v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->AccountEditmodePopupWindow(ILandroid/view/View;I)V

    .line 183
    const/4 v0, 0x1

    invoke-virtual {v14, v0}, Landroid/view/View;->setSelected(Z)V

    .line 184
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$000(Lcom/infraware/polarisoffice5/print/PrintActivity;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mMenuPopup:Lcom/infraware/office/actionbar/ActionBarPopupMenu;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$000(Lcom/infraware/polarisoffice5/print/PrintActivity;)Lcom/infraware/office/actionbar/ActionBarPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionBarPopupMenu;->show()V

    goto :goto_0

    .line 189
    .end local v14    # "caller2":Landroid/view/View;
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string/jumbo v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-ge v6, v0, :cond_3

    .line 191
    new-instance v0, Lcom/infraware/polarisoffice5/print/PrinterListThread;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$200(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$300(Lcom/infraware/polarisoffice5/print/PrintActivity;)[Landroid/accounts/Account;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mListThreadHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_Authtoken:Ljava/lang/String;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$500(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, Lcom/infraware/polarisoffice5/print/PrinterListThread;-><init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/PrinterListThread;->start()V

    .line 204
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->dismissMenuPopup()Z

    goto :goto_0

    .line 193
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string/jumbo v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountCount:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$600(Lcom/infraware/polarisoffice5/print/PrintActivity;)I

    move-result v1

    add-int/2addr v0, v1

    if-ge v6, v0, :cond_4

    .line 195
    new-instance v7, Lcom/infraware/polarisoffice5/print/PrinterListThread;

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$200(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/app/Activity;

    move-result-object v8

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v9

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$300(Lcom/infraware/polarisoffice5/print/PrintActivity;)[Landroid/accounts/Account;

    move-result-object v10

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mListThreadHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/os/Handler;

    move-result-object v11

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountTokenList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string/jumbo v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v1, v1

    sub-int v1, v6, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const-string/jumbo v13, "AddAccount"

    invoke-direct/range {v7 .. v13}, Lcom/infraware/polarisoffice5/print/PrinterListThread;-><init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/print/PrinterListThread;->start()V

    goto :goto_1

    .line 200
    :cond_4
    new-instance v7, Lcom/infraware/polarisoffice5/print/PrinterListThread;

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$200(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/app/Activity;

    move-result-object v8

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v9

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountArray:[Landroid/accounts/Account;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$300(Lcom/infraware/polarisoffice5/print/PrintActivity;)[Landroid/accounts/Account;

    move-result-object v10

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mListThreadHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/os/Handler;

    move-result-object v11

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->addAccountTokenList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$800(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "AddAccount"

    invoke-direct/range {v7 .. v13}, Lcom/infraware/polarisoffice5/print/PrinterListThread;-><init>(Landroid/app/Activity;Landroid/accounts/AccountManager;[Landroid/accounts/Account;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/print/PrinterListThread;->start()V

    .line 202
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oPrintListAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$900(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto/16 :goto_1
.end method
