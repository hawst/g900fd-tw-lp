.class Lcom/infraware/polarisoffice5/print/PrintActivity$2;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 213
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 214
    .local v0, "id":I
    const/16 v2, 0x92

    if-ne v0, v2, :cond_1

    .line 216
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->onAddAccount()V

    .line 232
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->dismissMenuPopup()Z

    .line 233
    return-void

    .line 220
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string/jumbo v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 222
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const v4, 0x7f0701ae

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1000(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 224
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string/jumbo v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v2, v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountCount:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$600(Lcom/infraware/polarisoffice5/print/PrintActivity;)I

    move-result v3

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_0

    .line 226
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mDbAccountTokenList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string/jumbo v4, "com.google"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v3, v3

    sub-int v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 227
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->items:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->m_oAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$100(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string/jumbo v4, "com.google"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v3, v3

    sub-int v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 228
    .local v1, "tmpStr":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mAccountDbOpenHelper:Lcom/infraware/polarisoffice5/print/AccountDbHelper;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1200(Lcom/infraware/polarisoffice5/print/PrintActivity;)Lcom/infraware/polarisoffice5/print/AccountDbHelper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/print/AccountDbHelper;->deleteColumn(Ljava/lang/String;)Z

    .line 229
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$2;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    const v5, 0x7f0701af

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/print/PrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/infraware/polarisoffice5/print/PrintActivity;->onToastMessage(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1000(Lcom/infraware/polarisoffice5/print/PrintActivity;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
