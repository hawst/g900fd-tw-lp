.class public final Lcom/infraware/polarisoffice5/print/CloudPrintAccountDatabase$CreateDB;
.super Ljava/lang/Object;
.source "CloudPrintAccountDatabase.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/CloudPrintAccountDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateDB"
.end annotation


# static fields
.field public static final AccountId:Ljava/lang/String; = "AccountId"

.field public static final AccountToken:Ljava/lang/String; = "AccountToken"

.field public static final _CREATE:Ljava/lang/String; = "create table Account(_id integer primary key autoincrement, AccountId text not null , AccountToken text not null );"

.field public static final _TABLENAME:Ljava/lang/String; = "Account"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
