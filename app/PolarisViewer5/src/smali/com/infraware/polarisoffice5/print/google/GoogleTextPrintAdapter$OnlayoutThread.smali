.class Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;
.super Ljava/lang/Thread;
.source "GoogleTextPrintAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OnlayoutThread"
.end annotation


# instance fields
.field callback:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

.field cancellationSignal:Landroid/os/CancellationSignal;

.field newAttributes:Landroid/print/PrintAttributes;

.field oldAttributes:Landroid/print/PrintAttributes;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;)V
    .locals 0
    .param p2, "oldAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "newAttributes"    # Landroid/print/PrintAttributes;
    .param p4, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p5, "callback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 208
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->oldAttributes:Landroid/print/PrintAttributes;

    .line 209
    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->newAttributes:Landroid/print/PrintAttributes;

    .line 210
    iput-object p4, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->cancellationSignal:Landroid/os/CancellationSignal;

    .line 211
    iput-object p5, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->callback:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    .line 212
    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPrintAttributes:Landroid/print/PrintAttributes;
    invoke-static {p1, p3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$002(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintAttributes;

    .line 213
    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/high16 v9, 0x40000000    # 2.0f

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    .line 217
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    new-instance v3, Landroid/print/pdf/PrintedPdfDocument;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$200(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->newAttributes:Landroid/print/PrintAttributes;

    invoke-direct {v3, v4, v5}, Landroid/print/pdf/PrintedPdfDocument;-><init>(Landroid/content/Context;Landroid/print/PrintAttributes;)V

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/pdf/PrintedPdfDocument;)Landroid/print/pdf/PrintedPdfDocument;

    .line 219
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v3

    invoke-virtual {v3}, Landroid/print/pdf/PrintedPdfDocument;->getPageHeight()I

    move-result v3

    int-to-float v3, v3

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageHeight:F
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$302(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F

    .line 220
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPdfDocument:Landroid/print/pdf/PrintedPdfDocument;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/pdf/PrintedPdfDocument;

    move-result-object v3

    invoke-virtual {v3}, Landroid/print/pdf/PrintedPdfDocument;->getPageWidth()I

    move-result v3

    int-to-float v3, v3

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$402(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F

    .line 222
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$400(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F

    move-result v3

    float-to-double v3, v3

    const-wide/high16 v5, 0x402a000000000000L    # 13.0

    mul-double/2addr v3, v5

    div-double/2addr v3, v7

    double-to-float v3, v3

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mWidthMargin:F
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$502(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F

    .line 223
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageHeight:F
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$300(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F

    move-result v3

    float-to-double v3, v3

    const-wide/high16 v5, 0x4026000000000000L    # 11.0

    mul-double/2addr v3, v5

    div-double/2addr v3, v7

    double-to-float v3, v3

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mHeightMargin:F
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$602(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F

    .line 225
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mWidthMargin:F
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$500(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F

    move-result v3

    mul-float/2addr v3, v9

    # -= operator for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageWidth:F
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$424(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F

    .line 226
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mHeightMargin:F
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$600(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)F

    move-result v3

    mul-float/2addr v3, v9

    # -= operator for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mPageHeight:F
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$324(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;F)F

    .line 230
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->cancellationSignal:Landroid/os/CancellationSignal;

    # invokes: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->getTextLineResizing(Landroid/os/CancellationSignal;)I
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$700(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/os/CancellationSignal;)I

    move-result v1

    .line 231
    .local v1, "totalLineCount":I
    if-ne v1, v10, :cond_0

    .line 232
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->callback:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v2}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    .line 254
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # invokes: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->getPageResizing(I)I
    invoke-static {v3, v1}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$900(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;I)I

    move-result v3

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalPageCount:I
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$802(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;I)I

    .line 238
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalPageCount:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$800(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)I

    move-result v2

    if-ne v2, v10, :cond_1

    .line 239
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->callback:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v2}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    goto :goto_0

    .line 242
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalPageCount:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$800(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)I

    move-result v2

    if-nez v2, :cond_2

    .line 243
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->callback:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    const-string/jumbo v3, "failed to print"

    invoke-virtual {v2, v3}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    .line 247
    :cond_2
    new-instance v2, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mFileName:Ljava/lang/String;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$1000(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mTotalPageCount:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$800(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v0

    .line 252
    .local v0, "info":Landroid/print/PrintDocumentInfo;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # setter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mDocumentInfo:Landroid/print/PrintDocumentInfo;
    invoke-static {v2, v0}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$1102(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;Landroid/print/PrintDocumentInfo;)Landroid/print/PrintDocumentInfo;

    .line 253
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->callback:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter$OnlayoutThread;->this$0:Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;

    # getter for: Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->mDocumentInfo:Landroid/print/PrintDocumentInfo;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;->access$1100(Lcom/infraware/polarisoffice5/print/google/GoogleTextPrintAdapter;)Landroid/print/PrintDocumentInfo;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    goto :goto_0
.end method
