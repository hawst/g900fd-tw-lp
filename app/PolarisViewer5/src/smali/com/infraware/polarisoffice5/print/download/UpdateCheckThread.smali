.class public Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;
.super Ljava/lang/Thread;
.source "UpdateCheckThread.java"


# static fields
.field public static final DISSMISS_PROGRESS:I = 0x4

.field public static final RESULT_NETWORK_ERROR:I = 0x1

.field public static final RESULT_UPDATE_AVAILIABLE:I = 0x0

.field public static final RESULT_UPDATE_NOT_FOUND:I = 0x2

.field public static final SHOW_PROGRESS:I = 0x3

.field private static final TAG:Ljava/lang/String; = "POLARIS Office 5 - Download Polaris Print"

.field private static mRunning:Z


# instance fields
.field mContext:Landroid/content/Context;

.field private mFromReceiver:Z

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mRunning:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromReceiver"    # Z

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    .line 53
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 55
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLandroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromReceiver"    # Z
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    .line 59
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 60
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    .line 61
    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    .line 62
    return-void
.end method

.method private checkUpdate(Ljava/net/URL;)Z
    .locals 12
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 136
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 138
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 140
    .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v8

    .line 142
    .local v8, "urlConnection":Ljava/net/URLConnection;
    const/16 v9, 0x1194

    invoke-virtual {v8, v9}, Ljava/net/URLConnection;->setReadTimeout(I)V

    .line 144
    const/16 v9, 0x1194

    invoke-virtual {v8, v9}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 146
    invoke-virtual {v8}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v3, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 148
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 152
    .local v4, "parserEvent":I
    const-string/jumbo v2, ""

    .local v2, "id":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 154
    .local v5, "result":Ljava/lang/String;
    :goto_0
    const/4 v9, 0x1

    if-eq v4, v9, :cond_3

    .line 156
    const/4 v9, 0x2

    if-ne v4, v9, :cond_1

    .line 158
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 160
    .local v6, "tag":Ljava/lang/String;
    const-string/jumbo v9, "appId"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 162
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    .line 164
    .local v7, "type":I
    const/4 v9, 0x4

    if-ne v7, v9, :cond_0

    .line 166
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    .line 168
    const-string/jumbo v9, "POLARIS Office 5 - Download Polaris Print"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "appId : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    .end local v7    # "type":I
    :cond_0
    const-string/jumbo v9, "resultCode"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 176
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    .line 178
    .restart local v7    # "type":I
    const/4 v9, 0x4

    if-ne v7, v9, :cond_1

    .line 180
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v5

    .line 182
    const-string/jumbo v9, "POLARIS Office 5 - Download Polaris Print"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "result : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    .end local v6    # "tag":Ljava/lang/String;
    .end local v7    # "type":I
    :cond_1
    const/4 v9, 0x3

    if-ne v4, v9, :cond_2

    .line 192
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 194
    .restart local v6    # "tag":Ljava/lang/String;
    const-string/jumbo v9, "appInfo"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 196
    invoke-direct {p0, v2, v5}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string/jumbo v2, ""

    .line 200
    const-string/jumbo v5, ""

    .line 206
    .end local v6    # "tag":Ljava/lang/String;
    :cond_2
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v4

    goto/16 :goto_0

    .line 210
    .end local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "parserEvent":I
    .end local v5    # "result":Ljava/lang/String;
    .end local v8    # "urlConnection":Ljava/net/URLConnection;
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string/jumbo v9, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v10, "xml parsing error"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 216
    const/4 v9, 0x0

    .line 245
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_1
    return v9

    .line 218
    :catch_1
    move-exception v0

    .line 220
    .local v0, "e":Ljava/net/SocketException;
    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    .line 222
    const-string/jumbo v9, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v10, "network is unavailable"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const/4 v9, 0x0

    goto :goto_1

    .line 226
    .end local v0    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v0

    .line 228
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    .line 230
    const-string/jumbo v9, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v10, "URL check time is longer than Define.TIMEOUT."

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v9, 0x0

    goto :goto_1

    .line 235
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :catch_3
    move-exception v0

    .line 237
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 239
    const-string/jumbo v9, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v10, "network error"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const/4 v9, 0x0

    goto :goto_1

    .line 245
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v2    # "id":Ljava/lang/String;
    .restart local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v4    # "parserEvent":I
    .restart local v5    # "result":Ljava/lang/String;
    .restart local v8    # "urlConnection":Ljava/net/URLConnection;
    :cond_3
    const/4 v9, 0x1

    goto :goto_1
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 8

    .prologue
    .line 403
    const/4 v5, 0x0

    .line 404
    .local v5, "s":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    const-string/jumbo v7, "/system/csc/sales_code.dat"

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 405
    .local v4, "mFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 407
    const/16 v7, 0x14

    new-array v0, v7, [B

    .line 409
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 413
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    move-object v2, v3

    .line 422
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :goto_0
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-eqz v7, :cond_1

    .line 424
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v5    # "s":Ljava/lang/String;
    .local v6, "s":Ljava/lang/String;
    move-object v5, v6

    .line 437
    .end local v0    # "buffer":[B
    .end local v2    # "in":Ljava/io/InputStream;
    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v5

    .line 415
    .restart local v0    # "buffer":[B
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 417
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 428
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :try_start_2
    new-instance v6, Ljava/lang/String;

    const-string/jumbo v7, "FAIL"

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .end local v5    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    move-object v5, v6

    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    goto :goto_1

    .line 431
    :catch_1
    move-exception v1

    .line 433
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private getMCC()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 306
    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 307
    .local v3, "telMgr":Landroid/telephony/TelephonyManager;
    const-string/jumbo v0, ""

    .line 309
    .local v0, "mcc":Ljava/lang/String;
    if-nez v3, :cond_0

    move-object v1, v0

    .line 338
    .end local v0    # "mcc":Ljava/lang/String;
    .local v1, "mcc":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 313
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 315
    .local v2, "networkOperator":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 327
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 328
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 330
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 338
    .end local v0    # "mcc":Ljava/lang/String;
    .restart local v1    # "mcc":Ljava/lang/String;
    goto :goto_0

    .line 318
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :pswitch_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 320
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 321
    goto :goto_1

    .line 323
    :cond_1
    const-string/jumbo v0, ""

    .line 324
    goto :goto_1

    .line 334
    :cond_2
    const-string/jumbo v0, ""

    goto :goto_1

    .line 315
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private getMNC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 345
    const-string/jumbo v0, ""

    .line 347
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 349
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-nez v2, :cond_1

    .line 361
    .end local v0    # "mnc":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 354
    .restart local v0    # "mnc":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 355
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 357
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getPD()Ljava/lang/String;
    .locals 5

    .prologue
    .line 468
    const-string/jumbo v2, ""

    .line 469
    .local v2, "rtn_str":Ljava/lang/String;
    const/4 v1, 0x0

    .line 471
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string/jumbo v3, "/sdcard/go_to_andromeda.test"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 475
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 476
    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 478
    const-string/jumbo v3, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v4, "go_to_andromeda.test is exist"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    const-string/jumbo v2, "1"

    .line 489
    :goto_0
    return-object v2

    .line 483
    :cond_0
    const-string/jumbo v3, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v4, "go_to_andromeda.test is not exist"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 486
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 253
    const-string/jumbo v3, "com.infraware.polarisprint2"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v3, "2"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 255
    const-string/jumbo v3, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v4, "Found POLARIS Print 2 Update"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 258
    .local v2, "msg":Landroid/os/Message;
    const/4 v3, 0x0

    iput v3, v2, Landroid/os/Message;->what:I

    .line 259
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 261
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    if-eqz v3, :cond_0

    .line 302
    :goto_0
    return-void

    .line 264
    :cond_0
    const-string/jumbo v3, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v4, "send Intent!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 267
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "com.sec.android.app.samsungapps"

    const-string/jumbo v4, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    const-string/jumbo v3, "directcall"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 271
    const-string/jumbo v3, "callerType"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 273
    const-string/jumbo v3, "GUID"

    const-string/jumbo v4, "com.infraware.polarisprint2"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    const v3, 0x14000020

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 278
    :try_start_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 279
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v4, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread$1;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread$1;-><init>(Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 295
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_1
    const-string/jumbo v3, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v4, "Not found POLARIS Print 2 Update"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 299
    .restart local v2    # "msg":Landroid/os/Message;
    const/4 v3, 0x2

    iput v3, v2, Landroid/os/Message;->what:I

    .line 300
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private isCSCExistFile()Z
    .locals 6

    .prologue
    .line 444
    const/4 v2, 0x0

    .line 446
    .local v2, "result":Z
    new-instance v1, Ljava/io/File;

    const-string/jumbo v3, "/system/csc/sales_code.dat"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 450
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    .line 451
    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 453
    const-string/jumbo v3, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v4, "CSC is not exist"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    :cond_0
    :goto_0
    return v2

    .line 456
    :catch_0
    move-exception v0

    .line 458
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "POLARIS Office 5 - Download Polaris Print"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "isCSCExistFile::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isRunnging()Z
    .locals 1

    .prologue
    .line 66
    sget-boolean v0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mRunning:Z

    return v0
.end method


# virtual methods
.method public getCSC()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 369
    const-string/jumbo v0, ""

    .line 370
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 374
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->isCSCExistFile()Z

    move-result v2

    if-eq v2, v3, :cond_0

    .line 396
    :goto_0
    return-object v0

    .line 379
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 380
    if-nez v1, :cond_1

    .line 382
    const-string/jumbo v2, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 386
    :cond_1
    const-string/jumbo v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 388
    const-string/jumbo v2, "POLARIS Office 5 - Download Polaris Print"

    const-string/jumbo v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 392
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 71
    sput-boolean v9, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mRunning:Z

    .line 73
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 74
    .local v1, "msg":Landroid/os/Message;
    const/4 v6, 0x3

    iput v6, v1, Landroid/os/Message;->what:I

    .line 75
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 77
    const-string/jumbo v6, "POLARIS Office 5 - Download Polaris Print"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "start update check (from Receiver : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const-string/jumbo v3, "http://hub.samsungapps.com/product/appCheck.as?"

    .line 83
    .local v3, "server_url":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "appInfo=com.infraware.polarisprint2@2.0.9300.02"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 85
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 86
    .local v4, "strModel":Ljava/lang/String;
    const-string/jumbo v6, "SAMSUNG-SM-N900A"

    invoke-virtual {v4, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_0

    .line 87
    const-string/jumbo v4, "SM-N900A"

    .line 89
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&deviceId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 91
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&mcc="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->getMCC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 93
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&mnc="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->getMNC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 95
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&csc="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->getCSC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 97
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&openApi="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 99
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "&pd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->getPD()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 102
    const/4 v2, 0x0

    .line 106
    .local v2, "result":Z
    :try_start_0
    const-string/jumbo v6, "POLARIS Office 5 - Download Polaris Print"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "check url : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 109
    .local v5, "url":Ljava/net/URL;
    invoke-direct {p0, v5}, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->checkUpdate(Ljava/net/URL;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 116
    if-nez v2, :cond_1

    .line 117
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    if-nez v6, :cond_1

    .line 118
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 119
    iput v9, v1, Landroid/os/Message;->what:I

    .line 120
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 123
    :cond_1
    const-string/jumbo v6, "POLARIS Office 5 - Download Polaris Print"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "end update check : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    .end local v5    # "url":Ljava/net/URL;
    :goto_0
    sput-boolean v10, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mRunning:Z

    .line 126
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e1":Ljava/net/MalformedURLException;
    :try_start_1
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    if-nez v2, :cond_2

    .line 117
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    if-nez v6, :cond_2

    .line 118
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 119
    iput v9, v1, Landroid/os/Message;->what:I

    .line 120
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 123
    :cond_2
    const-string/jumbo v6, "POLARIS Office 5 - Download Polaris Print"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "end update check : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 116
    .end local v0    # "e1":Ljava/net/MalformedURLException;
    :catchall_0
    move-exception v6

    if-nez v2, :cond_3

    .line 117
    iget-boolean v7, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mFromReceiver:Z

    if-nez v7, :cond_3

    .line 118
    iget-object v7, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 119
    iput v9, v1, Landroid/os/Message;->what:I

    .line 120
    iget-object v7, p0, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 123
    :cond_3
    const-string/jumbo v7, "POLARIS Office 5 - Download Polaris Print"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "end update check : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    sput-boolean v10, Lcom/infraware/polarisoffice5/print/download/UpdateCheckThread;->mRunning:Z

    .line 116
    throw v6
.end method
