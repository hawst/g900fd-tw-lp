.class public Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;
.super Ljava/lang/Thread;
.source "PrinterSubmitThread.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private Token:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

.field private mContentType:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mFileName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private m_nCopy:I

.field private result:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 32
    const-string/jumbo v0, "PrinterSubmitThread"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->TAG:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    .line 42
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->result:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mHandler"    # Landroid/os/Handler;
    .param p3, "Token"    # Ljava/lang/String;
    .param p4, "id"    # Ljava/lang/String;
    .param p5, "mContentType"    # Ljava/lang/String;
    .param p6, "mFileName"    # Ljava/lang/String;
    .param p7, "m_nCopy"    # I

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 32
    const-string/jumbo v0, "PrinterSubmitThread"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->TAG:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    .line 42
    iput-object v1, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->result:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    .line 56
    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->Token:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->id:Ljava/lang/String;

    .line 58
    iput-object p5, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mContentType:Ljava/lang/String;

    .line 59
    iput-object p6, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mFileName:Ljava/lang/String;

    .line 60
    iput p7, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->m_nCopy:I

    .line 61
    return-void
.end method


# virtual methods
.method public makeSubmitRequest()Ljava/lang/String;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    const/4 v4, 0x0

    .line 120
    .local v4, "Read":I
    const/4 v5, 0x0

    .line 122
    .local v5, "SubmitResult":Ljava/lang/String;
    const-string/jumbo v3, "UTF-8"

    .line 123
    .local v3, "MULTIPART_ENCODE":Ljava/lang/String;
    new-instance v12, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v12}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 124
    .local v12, "oHttpClient":Lorg/apache/http/client/HttpClient;
    new-instance v9, Lorg/apache/http/client/methods/HttpPost;

    const-string/jumbo v17, "http://www.google.com/cloudprint/submit?output=json"

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 125
    .local v9, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    const-string/jumbo v17, "Authorization"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "GoogleLogin auth="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->Token:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    new-instance v11, Lorg/apache/http/entity/mime/MultipartEntity;

    sget-object v17, Lorg/apache/http/entity/mime/HttpMultipartMode;->BROWSER_COMPATIBLE:Lorg/apache/http/entity/mime/HttpMultipartMode;

    const/16 v18, 0x0

    const-string/jumbo v19, "UTF-8"

    invoke-static/range {v19 .. v19}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v11, v0, v1, v2}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 129
    .local v11, "oEntity":Lorg/apache/http/entity/mime/MultipartEntity;
    const-string/jumbo v17, "printerid"

    new-instance v18, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->id:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mContentType:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string/jumbo v21, "UTF-8"

    invoke-static/range {v21 .. v21}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v21

    invoke-direct/range {v18 .. v21}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 130
    const-string/jumbo v17, "contentType"

    new-instance v18, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mContentType:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mContentType:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string/jumbo v21, "UTF-8"

    invoke-static/range {v21 .. v21}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v21

    invoke-direct/range {v18 .. v21}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 131
    const-string/jumbo v14, "{\"capabilities\":[{\"name\":\"psk:JobCopiesAllDocuments\",\"type\":\"ParameterDef\",\"value\":\""

    .line 132
    .local v14, "prefix":Ljava/lang/String;
    const-string/jumbo v13, "\"}]}"

    .line 133
    .local v13, "postfix":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "%d"

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->m_nCopy:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 134
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 136
    new-instance v16, Lorg/apache/http/entity/mime/content/StringBody;

    const-string/jumbo v17, "UTF-8"

    invoke-static/range {v17 .. v17}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v14, v1}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 137
    .local v16, "strCapabilities":Lorg/apache/http/entity/mime/content/StringBody;
    const-string/jumbo v17, "capabilities"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 139
    const-string/jumbo v17, "title"

    new-instance v18, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mFileName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mContentType:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string/jumbo v21, "UTF-8"

    invoke-static/range {v21 .. v21}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v21

    invoke-direct/range {v18 .. v21}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mFileName:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 143
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mFileName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 146
    .local v7, "file":Ljava/io/File;
    new-instance v8, Lorg/apache/http/entity/mime/content/FileBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mContentType:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v8, v7, v0}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 148
    .local v8, "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    const-string/jumbo v17, "content"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0, v8}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 149
    invoke-virtual {v9, v11}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 150
    invoke-interface {v12, v9}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v15

    .line 151
    .local v15, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v10

    .line 152
    .local v10, "oContent":Ljava/io/InputStream;
    const/16 v17, 0x400

    move/from16 v0, v17

    new-array v6, v0, [B

    .line 153
    .local v6, "buffer":[B
    :goto_0
    invoke-virtual {v10, v6}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v4, v0, :cond_1

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 144
    .end local v6    # "buffer":[B
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    .end local v10    # "oContent":Ljava/io/InputStream;
    .end local v15    # "response":Lorg/apache/http/HttpResponse;
    :cond_0
    const/16 v17, 0x0

    .line 159
    :goto_1
    return-object v17

    .line 156
    .restart local v6    # "buffer":[B
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "fileBody":Lorg/apache/http/entity/mime/content/FileBody;
    .restart local v10    # "oContent":Ljava/io/InputStream;
    .restart local v15    # "response":Lorg/apache/http/HttpResponse;
    :cond_1
    const-string/jumbo v17, "PrinterSubmitThread"

    const-string/jumbo v18, "resultSubmit get input string !!!!!!!!!!!"

    invoke-static/range {v17 .. v18}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    new-instance v5, Ljava/lang/String;

    .end local v5    # "SubmitResult":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v17

    const-string/jumbo v18, "euc-kr"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v5, v0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .restart local v5    # "SubmitResult":Ljava/lang/String;
    move-object/from16 v17, v5

    .line 159
    goto :goto_1
.end method

.method public resultJsonParsing(Ljava/lang/String;)V
    .locals 10
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 92
    const/4 v5, 0x0

    .line 93
    .local v5, "resultId":Ljava/lang/String;
    const/4 v6, 0x0

    .line 94
    .local v6, "status":Ljava/lang/String;
    const/4 v0, 0x0

    .line 95
    .local v0, "Error_code":Ljava/lang/String;
    const/4 v1, 0x0

    .line 97
    .local v1, "Message":Ljava/lang/String;
    const/4 v3, 0x0

    .line 100
    .local v3, "json":Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    .end local v3    # "json":Lorg/json/JSONObject;
    .local v4, "json":Lorg/json/JSONObject;
    :try_start_1
    const-string/jumbo v7, "job"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 103
    const/4 v3, 0x0

    .line 105
    .end local v4    # "json":Lorg/json/JSONObject;
    .restart local v3    # "json":Lorg/json/JSONObject;
    :try_start_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 106
    .end local v3    # "json":Lorg/json/JSONObject;
    .restart local v4    # "json":Lorg/json/JSONObject;
    :try_start_3
    const-string/jumbo v7, "id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 107
    const-string/jumbo v7, "status"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 108
    const-string/jumbo v7, "errorCode"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    const-string/jumbo v7, "message"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 110
    const-string/jumbo v7, "PrinterSubmitThread"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "message = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v3, v4

    .line 116
    .end local v4    # "json":Lorg/json/JSONObject;
    .restart local v3    # "json":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v2

    .line 114
    .local v2, "e":Lorg/json/JSONException;
    :goto_1
    const-string/jumbo v7, "PrinterSubmitThread"

    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    .end local v2    # "e":Lorg/json/JSONException;
    .end local v3    # "json":Lorg/json/JSONObject;
    .restart local v4    # "json":Lorg/json/JSONObject;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "json":Lorg/json/JSONObject;
    .restart local v3    # "json":Lorg/json/JSONObject;
    goto :goto_1
.end method

.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x3ea

    .line 67
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 68
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->makeSubmitRequest()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->result:Ljava/lang/String;

    .line 69
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->result:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->resultJsonParsing(Ljava/lang/String;)V

    .line 71
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mByteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    const-string/jumbo v2, "PrinterSubmitThread"

    const-string/jumbo v3, "finally"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 80
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 81
    .local v1, "message":Landroid/os/Message;
    iput v5, v1, Landroid/os/Message;->what:I

    .line 82
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 85
    .end local v1    # "message":Landroid/os/Message;
    :cond_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v2, "PrinterSubmitThread"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    const-string/jumbo v2, "PrinterSubmitThread"

    const-string/jumbo v3, "finally"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 80
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 81
    .restart local v1    # "message":Landroid/os/Message;
    iput v5, v1, Landroid/os/Message;->what:I

    .line 82
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    goto :goto_0

    .line 78
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "message":Landroid/os/Message;
    :catchall_0
    move-exception v2

    const-string/jumbo v3, "PrinterSubmitThread"

    const-string/jumbo v4, "finally"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 80
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 81
    .restart local v1    # "message":Landroid/os/Message;
    iput v5, v1, Landroid/os/Message;->what:I

    .line 82
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrinterSubmitThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 78
    .end local v1    # "message":Landroid/os/Message;
    :cond_1
    throw v2
.end method
