.class public Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;
.super Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;
.source "GoogleDocumentPrintAdapter.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/app/Activity;)V
    .locals 0
    .param p1, "DocType"    # I
    .param p2, "DocumentName"    # Ljava/lang/String;
    .param p3, "activity"    # Landroid/app/Activity;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/print/google/GooglePrintBaseAdapter;-><init>(ILjava/lang/String;Landroid/app/Activity;)V

    .line 17
    return-void
.end method


# virtual methods
.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 10
    .param p1, "pages"    # [Landroid/print/PageRange;
    .param p2, "destination"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 24
    sput-object p4, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mWriteResultCallback:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .line 25
    sput-object p2, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 26
    sput-object p3, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 27
    sget-object v6, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mCancellationSignal:Landroid/os/CancellationSignal;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->MyOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

    invoke-virtual {v6, v7}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 28
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mPages:[Landroid/print/PageRange;

    .line 39
    iget v5, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mTotalPageCount:I

    .line 42
    .local v5, "nPrintPageCount":I
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 43
    .local v2, "isAllPagePrint":Ljava/lang/Boolean;
    if-eqz p1, :cond_1

    aget-object v6, p1, v8

    if-eqz v6, :cond_0

    aget-object v6, p1, v8

    if-eqz v6, :cond_1

    aget-object v6, p1, v8

    sget-object v7, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    invoke-virtual {v6, v7}, Landroid/print/PageRange;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 44
    :cond_0
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 47
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 48
    new-array v4, v9, [I

    .line 49
    .local v4, "nPageArray":[I
    aput v8, v4, v8

    .line 52
    iput-boolean v9, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->m_isPossibleMakePdf:Z

    .line 53
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mPrintPath:Ljava/lang/String;

    invoke-virtual {v6, v7, v8, v4}, Lcom/infraware/office/evengine/EvInterface;->IExportPDF(Ljava/lang/String;I[I)V

    .line 108
    :goto_0
    return-void

    .line 87
    .end local v4    # "nPageArray":[I
    :cond_2
    const/4 v5, 0x0

    .line 88
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v6, p1

    if-ge v0, v6, :cond_4

    .line 90
    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/print/PageRange;->getStart()I

    move-result v3

    .local v3, "j":I
    :goto_2
    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/print/PageRange;->getEnd()I

    move-result v6

    if-gt v3, v6, :cond_3

    .line 91
    add-int/lit8 v5, v5, 0x1

    .line 90
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 88
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 95
    .end local v3    # "j":I
    :cond_4
    new-array v4, v5, [I

    .line 96
    .restart local v4    # "nPageArray":[I
    const/4 v1, 0x0

    .line 97
    .local v1, "index":I
    const/4 v0, 0x0

    :goto_3
    array-length v6, p1

    if-ge v0, v6, :cond_6

    .line 99
    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/print/PageRange;->getStart()I

    move-result v3

    .restart local v3    # "j":I
    :goto_4
    aget-object v6, p1, v0

    invoke-virtual {v6}, Landroid/print/PageRange;->getEnd()I

    move-result v6

    if-gt v3, v6, :cond_5

    .line 100
    add-int/lit8 v6, v3, 0x1

    aput v6, v4, v1

    .line 101
    add-int/lit8 v1, v1, 0x1

    .line 99
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 97
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 106
    .end local v3    # "j":I
    :cond_6
    iput-boolean v9, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->m_isPossibleMakePdf:Z

    .line 107
    iget-object v6, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/print/google/GoogleDocumentPrintAdapter;->mPrintPath:Ljava/lang/String;

    invoke-virtual {v6, v7, v5, v4}, Lcom/infraware/office/evengine/EvInterface;->IExportPDF(Ljava/lang/String;I[I)V

    goto :goto_0
.end method
