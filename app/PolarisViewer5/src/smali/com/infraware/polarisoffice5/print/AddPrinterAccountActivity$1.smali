.class Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;
.super Landroid/os/Handler;
.source "AddPrinterAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 82
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 83
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$000(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$000(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$000(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 86
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 98
    :goto_0
    :sswitch_0
    return-void

    .line 88
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_layoutError:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$100(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvError:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$200(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f070091

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 90
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_tvError:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$200(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 95
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->finish()V

    goto :goto_0

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x65 -> :sswitch_0
        0x384 -> :sswitch_2
    .end sparse-switch
.end method
