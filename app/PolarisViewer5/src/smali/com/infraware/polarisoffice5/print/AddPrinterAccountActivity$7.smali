.class Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;
.super Ljava/lang/Object;
.source "AddPrinterAccountActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 237
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$700(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "email":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    const-string/jumbo v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 240
    .local v1, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etEmail:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$700(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 241
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_etPassword:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$800(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 243
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # invokes: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->isValidEmailAddress(Ljava/lang/String;)Z
    invoke-static {v2, v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$900(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->m_oEventHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$1000(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 248
    :goto_0
    return-void

    .line 247
    :cond_0
    new-instance v2, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$7;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;-><init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$1;)V

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$RetriveAccountTask;->start()V

    goto :goto_0
.end method
