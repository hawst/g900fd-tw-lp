.class Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$4;
.super Ljava/lang/Object;
.source "AddPrinterAccountActivity.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$4;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v2, 0x0

    .line 180
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    sub-int v4, p6, p5

    sub-int/2addr v3, v4

    rsub-int/lit8 v1, v3, 0x32

    .line 182
    .local v1, "nRest":I
    if-gez v1, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-object v2

    .line 185
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-le v3, v1, :cond_0

    .line 186
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$4;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    const v3, 0x7f07008f

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity$4;->this$0:Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;

    # invokes: Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->onToastMessage(Ljava/lang/String;)V
    invoke-static {v2, v0}, Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;->access$600(Lcom/infraware/polarisoffice5/print/AddPrinterAccountActivity;Ljava/lang/String;)V

    .line 188
    const/4 v2, 0x0

    invoke-interface {p1, v2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0
.end method
