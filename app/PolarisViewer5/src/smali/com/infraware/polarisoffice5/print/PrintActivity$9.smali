.class Lcom/infraware/polarisoffice5/print/PrintActivity$9;
.super Ljava/lang/Object;
.source "PrintActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/print/PrintActivity;->initRadioButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field isClicked:Z

.field final synthetic this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

.field final synthetic val$clickListener:Landroid/view/View$OnClickListener;

.field final synthetic val$printSomeRadio:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/print/PrintActivity;Landroid/widget/LinearLayout;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 560
    iput-object p1, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->val$printSomeRadio:Landroid/widget/LinearLayout;

    iput-object p3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->val$clickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->isClicked:Z

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 565
    const/4 v0, 0x0

    .line 566
    .local v0, "isPressed":Z
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-direct {v1, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 568
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 569
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 570
    const/4 v0, 0x1

    .line 571
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->isClicked:Z

    .line 585
    :cond_1
    :goto_0
    if-ne v0, v5, :cond_6

    .line 586
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->val$printSomeRadio:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 587
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 588
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setPressed(Z)V

    .line 596
    :goto_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v5, :cond_3

    .line 597
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->isClicked:Z

    if-ne v3, v5, :cond_3

    .line 598
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->isClicked:Z

    .line 599
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->val$clickListener:Landroid/view/View$OnClickListener;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 600
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 601
    .local v2, "strRange":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v5, :cond_7

    .line 602
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v3, v6}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 608
    .end local v2    # "strRange":Ljava/lang/String;
    :cond_3
    :goto_2
    return v6

    .line 574
    :cond_4
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->isClicked:Z

    goto :goto_0

    .line 577
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 578
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->isClicked:Z

    if-ne v3, v5, :cond_1

    .line 579
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_1

    .line 580
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->isClicked:Z

    goto :goto_0

    .line 591
    :cond_6
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->val$printSomeRadio:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 592
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintSomeCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1700(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 593
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    # getter for: Lcom/infraware/polarisoffice5/print/PrintActivity;->mPrintRangeEditText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/print/PrintActivity;->access$1400(Lcom/infraware/polarisoffice5/print/PrintActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setPressed(Z)V

    goto :goto_1

    .line 604
    .restart local v2    # "strRange":Ljava/lang/String;
    :cond_7
    iget-object v3, p0, Lcom/infraware/polarisoffice5/print/PrintActivity$9;->this$0:Lcom/infraware/polarisoffice5/print/PrintActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/print/PrintActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v3, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    goto :goto_2
.end method
