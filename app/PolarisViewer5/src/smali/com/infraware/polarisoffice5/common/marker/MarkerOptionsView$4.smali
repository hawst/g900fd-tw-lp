.class Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;
.super Ljava/lang/Object;
.source "MarkerOptionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 250
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$300(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$300(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 252
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 253
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # setter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$502(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;I)I

    .line 254
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$300(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 258
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 259
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # setter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I
    invoke-static {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$502(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;I)I

    .line 260
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    goto :goto_0
.end method
