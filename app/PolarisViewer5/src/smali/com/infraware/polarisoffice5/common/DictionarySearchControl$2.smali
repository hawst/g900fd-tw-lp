.class Lcom/infraware/polarisoffice5/common/DictionarySearchControl$2;
.super Landroid/os/Handler;
.source "DictionarySearchControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setDictionLoadingHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/DictionarySearchControl;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl$2;->this$0:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 642
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 645
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl$2;->this$0:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDBListName()Z

    move-result v0

    .line 647
    .local v0, "isGetDBListName":Z
    if-ne v0, v3, :cond_1

    .line 648
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl$2;->this$0:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    # getter for: Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->access$300(Lcom/infraware/polarisoffice5/common/DictionarySearchControl;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl$2;->this$0:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    # getter for: Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->access$300(Lcom/infraware/polarisoffice5/common/DictionarySearchControl;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 649
    invoke-static {v3}, Lcom/infraware/common/define/CMModelDefine$Diotek;->setLoadComplete(Z)V

    goto :goto_0

    .line 653
    :cond_1
    invoke-static {v2}, Lcom/infraware/common/define/CMModelDefine$Diotek;->setLoadComplete(Z)V

    goto :goto_0

    .line 657
    .end local v0    # "isGetDBListName":Z
    :pswitch_1
    invoke-static {v2}, Lcom/infraware/common/define/CMModelDefine$Diotek;->setLoadComplete(Z)V

    goto :goto_0

    .line 642
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
