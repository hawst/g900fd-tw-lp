.class Lcom/infraware/polarisoffice5/common/ColorPickerActivity$4;
.super Ljava/lang/Object;
.source "ColorPickerActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$4;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 180
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0050

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0051

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0052

    if-ne v0, v1, :cond_1

    .line 183
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 184
    invoke-static {p1}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    .line 190
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$4;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    goto :goto_0
.end method
