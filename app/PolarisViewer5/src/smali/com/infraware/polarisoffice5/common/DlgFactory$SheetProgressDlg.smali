.class public Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;
.super Landroid/app/ProgressDialog;
.source "DlgFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/DlgFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SheetProgressDlg"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V
    .locals 4
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 298
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;->this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;

    .line 305
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v0

    invoke-direct {p0, p2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 306
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;->requestWindowFeature(I)Z

    .line 307
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 308
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;->setMessage(Ljava/lang/CharSequence;)V

    .line 309
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;->setCancelable(Z)V

    .line 310
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;->setCanceledOnTouchOutside(Z)V

    .line 311
    return-void
.end method
