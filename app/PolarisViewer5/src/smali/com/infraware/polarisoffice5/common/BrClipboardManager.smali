.class public Lcom/infraware/polarisoffice5/common/BrClipboardManager;
.super Ljava/lang/Object;
.source "BrClipboardManager.java"


# static fields
.field public static final CLIPBOARDMANAGER_CF_HTML:Ljava/lang/String; = "html"

.field public static final CLIPBOARDMANAGER_CF_IMGPATH:Ljava/lang/String; = "imgpath"

.field public static final CLIPBOARDMANAGER_CF_TEXT:Ljava/lang/String; = "text"

.field public static final CLIPBOARDMANAGER_DATA:Ljava/lang/String; = "_brclipboard.dat"

.field public static final CLIPBOARDMANAGER_GET:Ljava/lang/String; = "get"

.field public static final CLIPBOARDMANAGER_SET:Ljava/lang/String; = "set"

.field public static final CLIPBOARDMANAGER_USE_GET:I = 0x1

.field public static final CLIPBOARDMANAGER_USE_NONE:I = 0x0

.field public static final CLIPBOARDMANAGER_USE_SET:I = 0x2


# instance fields
.field mData0:[B

.field mData1:[B

.field mData2:[B

.field mData3:[B

.field mData4:[B

.field mFormat:[Ljava/lang/String;

.field m_bReadFile:Z

.field m_bSetData:Z

.field m_nUseType:I

.field m_pszCaller:Ljava/lang/String;

.field m_pszSaveCaller:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszCaller:Ljava/lang/String;

    .line 30
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszSaveCaller:Ljava/lang/String;

    .line 31
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bReadFile:Z

    .line 32
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    .line 34
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    .line 35
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    .line 36
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    .line 37
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    .line 38
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    .line 39
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    .line 47
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    .line 48
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bReadFile:Z

    .line 49
    iput v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    .line 51
    invoke-static {}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->createClipboardDirectoty()Z

    .line 52
    return-void
.end method

.method private byteToString([B)Ljava/lang/String;
    .locals 5
    .param p1, "in"    # [B

    .prologue
    .line 652
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 654
    .local v2, "r":Ljava/lang/String;
    if-eqz p1, :cond_0

    array-length v4, p1

    if-nez v4, :cond_1

    :cond_0
    move-object v3, v2

    .line 678
    .end local v2    # "r":Ljava/lang/String;
    .local v3, "r":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 659
    .end local v3    # "r":Ljava/lang/Object;
    .restart local v2    # "r":Ljava/lang/String;
    :cond_1
    array-length v1, p1

    .line 661
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_4

    .line 663
    aget-byte v4, p1, v0

    if-nez v4, :cond_3

    .line 665
    if-eqz v0, :cond_2

    .line 667
    const/4 v2, 0x0

    .line 668
    new-instance v2, Ljava/lang/String;

    .end local v2    # "r":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-direct {v2, p1, v4, v0}, Ljava/lang/String;-><init>([BII)V

    .restart local v2    # "r":Ljava/lang/String;
    :cond_2
    move-object v3, v2

    .line 671
    .restart local v3    # "r":Ljava/lang/Object;
    goto :goto_0

    .line 661
    .end local v3    # "r":Ljava/lang/Object;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 676
    :cond_4
    new-instance v2, Ljava/lang/String;

    .end local v2    # "r":Ljava/lang/String;
    invoke-direct {v2, p1}, Ljava/lang/String;-><init>([B)V

    .restart local v2    # "r":Ljava/lang/String;
    move-object v3, v2

    .line 678
    .restart local v3    # "r":Ljava/lang/Object;
    goto :goto_0
.end method


# virtual methods
.method public Close()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 554
    iget v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 556
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    if-eqz v0, :cond_0

    .line 558
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->saveDataFile()Z

    .line 562
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->initData()V

    .line 563
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszCaller:Ljava/lang/String;

    .line 564
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bReadFile:Z

    .line 565
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    .line 566
    iput v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    .line 567
    const/4 v0, 0x1

    return v0
.end method

.method public Empty()V
    .locals 1

    .prologue
    .line 548
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->initData()V

    .line 549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    .line 550
    return-void
.end method

.method public GetCaller()Ljava/lang/String;
    .locals 2

    .prologue
    .line 414
    iget v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 416
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bReadFile:Z

    if-nez v0, :cond_0

    .line 417
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->readDataFile()Z

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszSaveCaller:Ljava/lang/String;

    .line 422
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszCaller:Ljava/lang/String;

    goto :goto_0
.end method

.method public GetData(Ljava/lang/String;)[B
    .locals 8
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0xa

    const/4 v4, 0x0

    .line 572
    iget v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    .line 646
    :cond_0
    :goto_0
    return-object v4

    .line 575
    :cond_1
    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    if-nez v5, :cond_0

    .line 578
    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bReadFile:Z

    if-nez v5, :cond_2

    .line 579
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->readDataFile()Z

    .line 582
    :cond_2
    if-eqz p1, :cond_0

    .line 584
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 587
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 589
    .local v0, "bformat":[B
    const/4 v3, 0x0

    .line 591
    .local v3, "temp_format":Ljava/lang/String;
    array-length v5, v0

    if-le v5, v7, :cond_5

    .line 593
    new-instance v3, Ljava/lang/String;

    .end local v3    # "temp_format":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-direct {v3, v0, v5, v7}, Ljava/lang/String;-><init>([BII)V

    .line 598
    .restart local v3    # "temp_format":Ljava/lang/String;
    :goto_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    const/4 v5, 0x5

    if-ge v2, v5, :cond_0

    .line 600
    const/4 v1, 0x0

    .line 601
    .local v1, "finish":Z
    packed-switch v2, :pswitch_data_0

    .line 625
    :cond_3
    :goto_3
    if-nez v1, :cond_0

    .line 628
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 630
    packed-switch v2, :pswitch_data_1

    .line 598
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 596
    .end local v1    # "finish":Z
    .end local v2    # "i":I
    :cond_5
    move-object v3, p1

    goto :goto_1

    .line 604
    .restart local v1    # "finish":Z
    .restart local v2    # "i":I
    :pswitch_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    if-nez v5, :cond_3

    .line 605
    const/4 v1, 0x1

    goto :goto_3

    .line 608
    :pswitch_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    if-nez v5, :cond_3

    .line 609
    const/4 v1, 0x1

    goto :goto_3

    .line 612
    :pswitch_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    if-nez v5, :cond_3

    .line 613
    const/4 v1, 0x1

    goto :goto_3

    .line 616
    :pswitch_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    if-nez v5, :cond_3

    .line 617
    const/4 v1, 0x1

    goto :goto_3

    .line 620
    :pswitch_4
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    if-nez v5, :cond_3

    .line 621
    const/4 v1, 0x1

    goto :goto_3

    .line 633
    :pswitch_5
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    goto :goto_0

    .line 635
    :pswitch_6
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    goto :goto_0

    .line 637
    :pswitch_7
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    goto :goto_0

    .line 639
    :pswitch_8
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    goto :goto_0

    .line 641
    :pswitch_9
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    goto :goto_0

    .line 601
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 630
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public Open(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "pszOpenType"    # Ljava/lang/String;
    .param p2, "pszCaller"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x0

    .line 377
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->initData()V

    .line 379
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 381
    :cond_0
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszCaller:Ljava/lang/String;

    .line 394
    :goto_0
    iput v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    .line 396
    if-eqz p1, :cond_1

    .line 398
    const-string/jumbo v1, "get"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 400
    const/4 v1, 0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    .line 408
    :cond_1
    :goto_1
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bReadFile:Z

    .line 409
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    .line 410
    return-void

    .line 385
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 386
    .local v0, "bcaller":[B
    array-length v1, v0

    if-le v1, v3, :cond_3

    .line 388
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Ljava/lang/String;-><init>([BII)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszCaller:Ljava/lang/String;

    goto :goto_0

    .line 391
    :cond_3
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszCaller:Ljava/lang/String;

    goto :goto_0

    .line 402
    .end local v0    # "bcaller":[B
    :cond_4
    const-string/jumbo v1, "set"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 404
    const/4 v1, 0x2

    iput v1, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    goto :goto_1
.end method

.method public SetData(Ljava/lang/String;[B)Z
    .locals 11
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "pData"    # [B

    .prologue
    const/16 v10, 0xa

    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 432
    iget v7, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    const/4 v8, 0x2

    if-eq v7, v8, :cond_1

    .line 544
    :cond_0
    :goto_0
    return v5

    .line 435
    :cond_1
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 438
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    .line 441
    array-length v7, p2

    if-eqz v7, :cond_0

    .line 444
    iget-boolean v7, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    if-nez v7, :cond_2

    .line 446
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->initData()V

    .line 447
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bSetData:Z

    .line 450
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 452
    .local v1, "bformat":[B
    const/4 v3, 0x0

    .line 454
    .local v3, "temp_format":Ljava/lang/String;
    array-length v7, v1

    if-le v7, v10, :cond_4

    .line 456
    new-instance v3, Ljava/lang/String;

    .end local v3    # "temp_format":Ljava/lang/String;
    invoke-direct {v3, v1, v5, v10}, Ljava/lang/String;-><init>([BII)V

    .line 461
    .restart local v3    # "temp_format":Ljava/lang/String;
    :goto_1
    const/4 v0, 0x0

    .line 463
    .local v0, "add_index":I
    const/4 v4, 0x0

    .line 465
    .local v4, "tmpData":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    const/4 v5, 0x5

    if-ge v2, v5, :cond_3

    .line 467
    packed-switch v2, :pswitch_data_0

    .line 486
    :goto_3
    if-nez v4, :cond_5

    .line 523
    :cond_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    aput-object v3, v5, v0

    .line 525
    packed-switch v0, :pswitch_data_1

    :goto_4
    move v5, v6

    .line 544
    goto :goto_0

    .line 459
    .end local v0    # "add_index":I
    .end local v2    # "i":I
    .end local v4    # "tmpData":[B
    :cond_4
    move-object v3, p1

    goto :goto_1

    .line 470
    .restart local v0    # "add_index":I
    .restart local v2    # "i":I
    .restart local v4    # "tmpData":[B
    :pswitch_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    .line 471
    goto :goto_3

    .line 473
    :pswitch_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    .line 474
    goto :goto_3

    .line 476
    :pswitch_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    .line 477
    goto :goto_3

    .line 479
    :pswitch_3
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    .line 480
    goto :goto_3

    .line 482
    :pswitch_4
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    goto :goto_3

    .line 489
    :cond_5
    add-int/lit8 v0, v2, 0x1

    .line 491
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_6

    .line 493
    packed-switch v2, :pswitch_data_2

    .line 517
    :goto_5
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    aput-object v3, v5, v2

    move v5, v6

    .line 518
    goto :goto_0

    .line 496
    :pswitch_5
    iput-object v9, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    .line 497
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    goto :goto_5

    .line 500
    :pswitch_6
    iput-object v9, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    .line 501
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    goto :goto_5

    .line 504
    :pswitch_7
    iput-object v9, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    .line 505
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    goto :goto_5

    .line 508
    :pswitch_8
    iput-object v9, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    .line 509
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    goto :goto_5

    .line 512
    :pswitch_9
    iput-object v9, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    .line 513
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    goto :goto_5

    .line 465
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 528
    :pswitch_a
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    goto :goto_4

    .line 531
    :pswitch_b
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    goto :goto_4

    .line 534
    :pswitch_c
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    goto :goto_4

    .line 537
    :pswitch_d
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    goto/16 :goto_4

    .line 540
    :pswitch_e
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    goto/16 :goto_4

    .line 467
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 525
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 493
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method initData()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 61
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    const/4 v7, 0x3

    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    const/4 v9, 0x4

    const-string/jumbo v10, ""

    aput-object v10, v8, v9

    aput-object v10, v6, v7

    aput-object v10, v4, v5

    aput-object v10, v2, v3

    aput-object v10, v0, v1

    .line 62
    iput-object v11, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    .line 63
    iput-object v11, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    .line 64
    iput-object v11, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    .line 65
    iput-object v11, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    .line 66
    iput-object v11, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    .line 68
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszSaveCaller:Ljava/lang/String;

    .line 69
    return-void
.end method

.method readDataFile()Z
    .locals 19

    .prologue
    .line 73
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_0

    .line 74
    const/16 v17, 0x0

    .line 249
    :goto_0
    return v17

    .line 76
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->initData()V

    .line 79
    sget-object v12, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardRootPath:Ljava/lang/String;

    .line 80
    .local v12, "path":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 81
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "_brclipboard.dat"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 83
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    .local v7, "f":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_1

    .line 86
    const/16 v17, 0x0

    goto :goto_0

    .line 88
    :cond_1
    const/4 v8, 0x0

    .line 91
    .local v8, "fIn":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    .end local v8    # "fIn":Ljava/io/FileInputStream;
    .local v9, "fIn":Ljava/io/FileInputStream;
    if-nez v9, :cond_3

    .line 99
    const/16 v17, 0x0

    goto :goto_0

    .line 93
    .end local v9    # "fIn":Ljava/io/FileInputStream;
    .restart local v8    # "fIn":Ljava/io/FileInputStream;
    :catch_0
    move-exception v6

    .line 94
    .local v6, "e":Ljava/io/FileNotFoundException;
    const/4 v8, 0x0

    .line 98
    if-nez v8, :cond_4

    .line 99
    const/16 v17, 0x0

    goto :goto_0

    .line 98
    .end local v6    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v17

    if-nez v8, :cond_2

    .line 99
    const/16 v17, 0x0

    goto :goto_0

    :cond_2
    throw v17

    .end local v8    # "fIn":Ljava/io/FileInputStream;
    .restart local v9    # "fIn":Ljava/io/FileInputStream;
    :cond_3
    move-object v8, v9

    .line 102
    .end local v9    # "fIn":Ljava/io/FileInputStream;
    .restart local v8    # "fIn":Ljava/io/FileInputStream;
    :cond_4
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v17

    move-wide/from16 v0, v17

    long-to-int v11, v0

    .line 116
    .local v11, "f_len":I
    const/4 v10, 0x0

    .line 118
    .local v10, "f_index":I
    const/16 v17, 0x10

    move/from16 v0, v17

    if-ge v11, v0, :cond_5

    .line 122
    :try_start_1
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 129
    :goto_1
    const/4 v8, 0x0

    .line 130
    const/16 v17, 0x0

    goto :goto_0

    .line 124
    :catch_1
    move-exception v6

    .line 127
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 133
    .end local v6    # "e":Ljava/io/IOException;
    :cond_5
    const/16 v17, 0x10

    move/from16 v0, v17

    new-array v3, v0, [B

    .line 134
    .local v3, "buf16":[B
    const/16 v17, 0xa

    move/from16 v0, v17

    new-array v2, v0, [B

    .line 135
    .local v2, "buf10":[B
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v4, v0, [B

    .line 136
    .local v4, "buf4":[B
    const/4 v14, 0x0

    .line 140
    .local v14, "tempData":[B
    :try_start_2
    invoke-virtual {v8, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 141
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszSaveCaller:Ljava/lang/String;

    .line 142
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->byteToString([B)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszSaveCaller:Ljava/lang/String;

    .line 144
    const/16 v10, 0x10

    .line 146
    const/16 v16, 0x0

    .line 148
    .local v16, "tempFormat":Ljava/lang/String;
    const/4 v15, 0x0

    .line 150
    .local v15, "tempDataLen":I
    const/4 v5, 0x0

    .line 152
    .local v5, "data_index":I
    :cond_6
    add-int/lit8 v17, v11, -0xa

    move/from16 v0, v17

    if-gt v10, v0, :cond_7

    .line 154
    invoke-virtual {v8, v2}, Ljava/io/FileInputStream;->read([B)I

    .line 155
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->byteToString([B)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v16

    .line 156
    add-int/lit8 v10, v10, 0xa

    .line 158
    add-int/lit8 v17, v11, -0xa

    move/from16 v0, v17

    if-le v10, v0, :cond_8

    .line 232
    :cond_7
    :goto_2
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 239
    :goto_3
    const/4 v8, 0x0

    .line 242
    .end local v5    # "data_index":I
    .end local v15    # "tempDataLen":I
    .end local v16    # "tempFormat":Ljava/lang/String;
    :goto_4
    const/4 v14, 0x0

    .line 243
    const/4 v3, 0x0

    .line 244
    const/4 v2, 0x0

    .line 245
    const/4 v4, 0x0

    .line 247
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_bReadFile:Z

    .line 249
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 161
    .restart local v5    # "data_index":I
    .restart local v15    # "tempDataLen":I
    .restart local v16    # "tempFormat":Ljava/lang/String;
    :cond_8
    :try_start_4
    invoke-virtual {v8, v2}, Ljava/io/FileInputStream;->read([B)I

    .line 162
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->byteToString([B)Ljava/lang/String;

    move-result-object v13

    .line 163
    .local v13, "str_length":Ljava/lang/String;
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 165
    add-int/lit8 v10, v10, 0xa

    .line 167
    const/16 v17, 0x1

    move/from16 v0, v17

    if-lt v15, v0, :cond_7

    add-int v17, v10, v15

    move/from16 v0, v17

    if-gt v0, v11, :cond_7

    .line 170
    new-array v14, v15, [B

    .line 171
    invoke-virtual {v8, v14}, Ljava/io/FileInputStream;->read([B)I

    .line 174
    add-int/2addr v10, v15

    .line 176
    add-int/lit8 v17, v11, -0x4

    move/from16 v0, v17

    if-le v10, v0, :cond_9

    .line 178
    const/4 v14, 0x0

    .line 179
    goto :goto_2

    .line 182
    :cond_9
    invoke-virtual {v8, v4}, Ljava/io/FileInputStream;->read([B)I

    .line 183
    add-int/lit8 v10, v10, 0x4

    .line 185
    const/16 v17, 0x0

    aget-byte v17, v4, v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    const/16 v17, 0x1

    aget-byte v17, v4, v17

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    const/16 v17, 0x2

    aget-byte v17, v4, v17

    const/16 v18, -0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    const/16 v17, 0x3

    aget-byte v17, v4, v17

    const/16 v18, -0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 188
    packed-switch v5, :pswitch_data_0

    .line 211
    :goto_5
    add-int/lit8 v5, v5, 0x1

    .line 212
    const/4 v14, 0x0

    .line 213
    const/16 v16, 0x0

    .line 215
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ne v5, v0, :cond_6

    goto :goto_2

    .line 191
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v16, v17, v18

    .line 192
    invoke-virtual {v14}, [B->clone()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 225
    .end local v5    # "data_index":I
    .end local v13    # "str_length":Ljava/lang/String;
    .end local v15    # "tempDataLen":I
    .end local v16    # "tempFormat":Ljava/lang/String;
    :catch_2
    move-exception v6

    .line 226
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 232
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 239
    :goto_6
    const/4 v8, 0x0

    .line 240
    goto/16 :goto_4

    .line 195
    .end local v6    # "e":Ljava/io/IOException;
    .restart local v5    # "data_index":I
    .restart local v13    # "str_length":Ljava/lang/String;
    .restart local v15    # "tempDataLen":I
    .restart local v16    # "tempFormat":Ljava/lang/String;
    :pswitch_1
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    aput-object v16, v17, v18

    .line 196
    invoke-virtual {v14}, [B->clone()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_5

    .line 230
    .end local v5    # "data_index":I
    .end local v13    # "str_length":Ljava/lang/String;
    .end local v15    # "tempDataLen":I
    .end local v16    # "tempFormat":Ljava/lang/String;
    :catchall_1
    move-exception v17

    .line 232
    :try_start_8
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 239
    :goto_7
    const/4 v8, 0x0

    throw v17

    .line 199
    .restart local v5    # "data_index":I
    .restart local v13    # "str_length":Ljava/lang/String;
    .restart local v15    # "tempDataLen":I
    .restart local v16    # "tempFormat":Ljava/lang/String;
    :pswitch_2
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    aput-object v16, v17, v18

    .line 200
    invoke-virtual {v14}, [B->clone()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    goto :goto_5

    .line 203
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aput-object v16, v17, v18

    .line 204
    invoke-virtual {v14}, [B->clone()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    goto :goto_5

    .line 207
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x4

    aput-object v16, v17, v18

    .line 208
    invoke-virtual {v14}, [B->clone()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_5

    .line 221
    :cond_a
    const/4 v14, 0x0

    .line 222
    goto/16 :goto_2

    .line 234
    .end local v13    # "str_length":Ljava/lang/String;
    :catch_3
    move-exception v6

    .line 237
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 234
    .end local v5    # "data_index":I
    .end local v15    # "tempDataLen":I
    .end local v16    # "tempFormat":Ljava/lang/String;
    :catch_4
    move-exception v6

    .line 237
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 234
    .end local v6    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v6

    .line 237
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method saveDataFile()Z
    .locals 17

    .prologue
    .line 254
    move-object/from16 v0, p0

    iget v15, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_nUseType:I

    const/16 v16, 0x2

    move/from16 v0, v16

    if-eq v15, v0, :cond_0

    .line 255
    const/4 v15, 0x0

    .line 371
    :goto_0
    return v15

    .line 257
    :cond_0
    sget-object v12, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->mClipboardRootPath:Ljava/lang/String;

    .line 258
    .local v12, "path":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 259
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "_brclipboard.dat"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 261
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 263
    .local v7, "f":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_1

    .line 266
    :try_start_0
    invoke-virtual {v7}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :cond_1
    const/16 v15, 0x1ff

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v7, v15, v0}, Lcom/infraware/common/util/FileUtils;->changePermissons(Ljava/io/File;IZ)V

    .line 276
    const/4 v8, 0x0

    .line 278
    .local v8, "fOut":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v8, Ljava/io/FileOutputStream;

    .end local v8    # "fOut":Ljava/io/FileOutputStream;
    invoke-direct {v8, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 284
    .restart local v8    # "fOut":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszCaller:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v10

    .line 285
    .local v10, "len":I
    const/16 v15, 0x10

    new-array v11, v15, [B

    .line 287
    .local v11, "nullbuf":[B
    const/4 v4, 0x0

    .line 291
    .local v4, "caller_len":I
    if-lez v10, :cond_2

    .line 293
    :try_start_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->m_pszCaller:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 294
    .local v3, "caller":[B
    array-length v4, v3

    .line 295
    invoke-virtual {v8, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 298
    .end local v3    # "caller":[B
    :cond_2
    const/16 v15, 0x10

    if-ge v4, v15, :cond_3

    .line 299
    const/4 v15, 0x0

    rsub-int/lit8 v16, v4, 0x10

    move/from16 v0, v16

    invoke-virtual {v8, v11, v15, v0}, Ljava/io/FileOutputStream;->write([BII)V

    .line 302
    :cond_3
    const/4 v14, 0x0

    .line 304
    .local v14, "tmpData":[B
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    const/4 v15, 0x5

    if-ge v9, v15, :cond_4

    .line 306
    packed-switch v9, :pswitch_data_0

    .line 325
    :goto_2
    if-nez v14, :cond_5

    .line 353
    :cond_4
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 371
    .end local v9    # "i":I
    .end local v14    # "tmpData":[B
    :goto_3
    const/4 v15, 0x1

    goto :goto_0

    .line 268
    .end local v4    # "caller_len":I
    .end local v8    # "fOut":Ljava/io/FileOutputStream;
    .end local v10    # "len":I
    .end local v11    # "nullbuf":[B
    :catch_0
    move-exception v5

    .line 270
    .local v5, "e":Ljava/io/IOException;
    const/4 v15, 0x0

    goto :goto_0

    .line 279
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 280
    .local v5, "e":Ljava/io/FileNotFoundException;
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 309
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "caller_len":I
    .restart local v8    # "fOut":Ljava/io/FileOutputStream;
    .restart local v9    # "i":I
    .restart local v10    # "len":I
    .restart local v11    # "nullbuf":[B
    .restart local v14    # "tmpData":[B
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData0:[B

    .line 310
    goto :goto_2

    .line 312
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData1:[B

    .line 313
    goto :goto_2

    .line 315
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData2:[B

    .line 316
    goto :goto_2

    .line 318
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData3:[B

    .line 319
    goto :goto_2

    .line 321
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mData4:[B

    goto :goto_2

    .line 328
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    aget-object v15, v15, v9

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v10

    .line 330
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/common/BrClipboardManager;->mFormat:[Ljava/lang/String;

    aget-object v15, v15, v9

    invoke-virtual {v15}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 331
    .local v2, "bformat":[B
    array-length v10, v2

    .line 333
    invoke-virtual {v8, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 334
    const/16 v15, 0xa

    if-ge v10, v15, :cond_6

    .line 335
    const/4 v15, 0x0

    rsub-int/lit8 v16, v10, 0xa

    move/from16 v0, v16

    invoke-virtual {v8, v11, v15, v0}, Ljava/io/FileOutputStream;->write([BII)V

    .line 337
    :cond_6
    array-length v15, v14

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    .line 338
    .local v13, "str":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 340
    .local v1, "bdatalen":[B
    array-length v10, v1

    .line 341
    invoke-virtual {v8, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 342
    const/16 v15, 0xa

    if-ge v10, v15, :cond_7

    .line 343
    const/4 v15, 0x0

    rsub-int/lit8 v16, v10, 0xa

    move/from16 v0, v16

    invoke-virtual {v8, v11, v15, v0}, Ljava/io/FileOutputStream;->write([BII)V

    .line 345
    :cond_7
    invoke-virtual {v8, v14}, Ljava/io/FileOutputStream;->write([B)V

    .line 347
    const/4 v15, -0x1

    invoke-virtual {v8, v15}, Ljava/io/FileOutputStream;->write(I)V

    .line 348
    const/4 v15, -0x2

    invoke-virtual {v8, v15}, Ljava/io/FileOutputStream;->write(I)V

    .line 349
    const/4 v15, -0x3

    invoke-virtual {v8, v15}, Ljava/io/FileOutputStream;->write(I)V

    .line 350
    const/4 v15, -0x4

    invoke-virtual {v8, v15}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 304
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 355
    .end local v1    # "bdatalen":[B
    .end local v2    # "bformat":[B
    .end local v9    # "i":I
    .end local v13    # "str":Ljava/lang/String;
    .end local v14    # "tmpData":[B
    :catch_2
    move-exception v5

    .line 357
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 361
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 368
    :goto_4
    const/4 v8, 0x0

    goto :goto_3

    .line 363
    :catch_3
    move-exception v6

    .line 366
    .local v6, "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 306
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
