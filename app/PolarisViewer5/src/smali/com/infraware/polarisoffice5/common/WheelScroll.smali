.class public Lcom/infraware/polarisoffice5/common/WheelScroll;
.super Landroid/widget/HorizontalScrollView;
.source "WheelScroll.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;,
        Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;,
        Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;
    }
.end annotation


# instance fields
.field mClickListener:Landroid/view/View$OnClickListener;

.field private mFinalX:I

.field private mInstantSelection:I

.field protected mScrollTimer:Ljava/util/Timer;

.field private mTargetX:I

.field private mTimerHandler:Landroid/os/Handler;

.field private mTimerRunnable:Ljava/lang/Runnable;

.field mTimerSyncObj:Ljava/lang/Object;

.field private mView:Lcom/infraware/polarisoffice5/common/WheelView;

.field protected onItemScrollListener:Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;

.field protected onItemSelectListener:Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput v2, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mInstantSelection:I

    .line 97
    new-instance v0, Lcom/infraware/polarisoffice5/common/WheelScroll$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll$2;-><init>(Lcom/infraware/polarisoffice5/common/WheelScroll;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mClickListener:Landroid/view/View$OnClickListener;

    .line 106
    iput v2, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mFinalX:I

    .line 107
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerHandler:Landroid/os/Handler;

    .line 108
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerRunnable:Ljava/lang/Runnable;

    .line 322
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    .line 323
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerSyncObj:Ljava/lang/Object;

    .line 32
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSmoothScrollingEnabled(Z)V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerHandler:Landroid/os/Handler;

    .line 34
    new-instance v0, Lcom/infraware/polarisoffice5/common/WheelScroll$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll$1;-><init>(Lcom/infraware/polarisoffice5/common/WheelScroll;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerRunnable:Ljava/lang/Runnable;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/WheelScroll;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelScroll;

    .prologue
    .line 20
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mFinalX:I

    return v0
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/common/WheelScroll;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelScroll;
    .param p1, "x1"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mFinalX:I

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/WheelScroll;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelScroll;
    .param p1, "x1"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->onScrollEnd(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/WheelScroll;)Lcom/infraware/polarisoffice5/common/WheelView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelScroll;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/WheelScroll;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelScroll;

    .prologue
    .line 20
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mInstantSelection:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/WheelScroll;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelScroll;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->scrollToPositon(IZ)V

    return-void
.end method

.method private onScrollEnd(I)V
    .locals 4
    .param p1, "endX"    # I

    .prologue
    .line 300
    const-string/jumbo v1, "WWWW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onScrollEnd "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTargetX:I

    if-eq v1, p1, :cond_1

    .line 302
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getScrollX()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/WheelView;->getChildPosition(I)I

    move-result v0

    .line 303
    .local v0, "pos":I
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelection(I)V

    .line 304
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->onItemScrollListener:Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;

    if-eqz v1, :cond_0

    .line 305
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->onItemScrollListener:Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/WheelView;->getCurrentPosition()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;->onItemScroll(I)V

    .line 311
    .end local v0    # "pos":I
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->onItemScrollListener:Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;

    if-eqz v1, :cond_0

    .line 308
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->onItemScrollListener:Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/WheelView;->getCurrentPosition()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;->onItemScroll(I)V

    goto :goto_0
.end method

.method private onScrollStart(I)V
    .locals 3
    .param p1, "startX"    # I

    .prologue
    .line 295
    const-string/jumbo v0, "WWWW"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onScrollStart :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    return-void
.end method

.method private scrollToPositon(IZ)V
    .locals 3
    .param p1, "childPosition"    # I
    .param p2, "scroll"    # Z

    .prologue
    .line 269
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/WheelView;->getChildScrollDistance(I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTargetX:I

    .line 271
    :cond_0
    const-string/jumbo v0, "WWWW"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "scrollToPositon ==>mTargetX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTargetX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getScrollX()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTargetX:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getScrollX()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 291
    :goto_0
    return-void

    .line 277
    :cond_1
    if-eqz p2, :cond_2

    .line 280
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTargetX:I

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->timerScrollX(I)V

    goto :goto_0

    .line 283
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getWidth()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_3

    .line 285
    const-string/jumbo v0, "WWWW"

    const-string/jumbo v1, "returned"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 289
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTargetX:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->scrollTo(II)V

    goto :goto_0
.end method

.method private setSelection(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 113
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-nez v1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 116
    :cond_0
    if-gez p1, :cond_1

    .line 117
    const/4 p1, 0x0

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getDataCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_2

    .line 120
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getDataCount()I

    move-result v1

    add-int/lit8 p1, v1, -0x1

    .line 122
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/common/WheelView;->setCurrentPosition(I)V

    .line 124
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getWidth()I

    move-result v0

    .line 125
    .local v0, "wid":I
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getWidth()I

    move-result v1

    if-ge v1, v0, :cond_4

    .line 126
    :cond_3
    iput p1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mInstantSelection:I

    goto :goto_0

    .line 129
    :cond_4
    const/4 v1, -0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mInstantSelection:I

    .line 130
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->scrollToPositon(IZ)V

    goto :goto_0
.end method

.method private timerScrollX(I)V
    .locals 1
    .param p1, "xpos"    # I

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getScrollX()I

    move-result v0

    .line 318
    .local v0, "cur":I
    invoke-virtual {p0, v0, p1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->startTimer(II)V

    .line 320
    return-void
.end method


# virtual methods
.method public finalizeThis()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 53
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerRunnable:Ljava/lang/Runnable;

    .line 54
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerHandler:Landroid/os/Handler;

    .line 56
    :cond_0
    return-void
.end method

.method public getData()I
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-nez v0, :cond_0

    .line 186
    const/4 v0, 0x0

    .line 187
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getData(I)I

    move-result v0

    goto :goto_0
.end method

.method public getData(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-nez v0, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 194
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/WheelView;->getData(I)I

    move-result v0

    goto :goto_0
.end method

.method public getIndex(I)I
    .locals 1
    .param p1, "data"    # I

    .prologue
    .line 178
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-nez v0, :cond_0

    .line 179
    const/4 v0, 0x0

    .line 180
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/WheelView;->indexOf(I)I

    move-result v0

    goto :goto_0
.end method

.method public getMaxDataIndex()I
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-nez v0, :cond_0

    .line 172
    const/4 v0, 0x0

    .line 173
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelView;->getDataCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 549
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mInstantSelection:I

    return v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelView;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getView()Lcom/infraware/polarisoffice5/common/WheelView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    return-object v0
.end method

.method public goEnd()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelView;->getDataCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelection(I)V

    .line 203
    return-void
.end method

.method public goLeft(I)V
    .locals 3
    .param p1, "amount"    # I

    .prologue
    const/4 v2, 0x1

    .line 207
    const/4 v0, 0x0

    .line 209
    .local v0, "newPosition":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-eqz v1, :cond_2

    .line 210
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getCurrentPosition()I

    move-result v1

    sub-int v0, v1, p1

    .line 212
    if-gez v0, :cond_0

    .line 213
    const/4 v0, 0x0

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getCurrentPosition()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 217
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelView;->setCurrentPosition(I)V

    .line 218
    invoke-direct {p0, v0, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->scrollToPositon(IZ)V

    .line 224
    :cond_1
    :goto_0
    return-void

    .line 222
    :cond_2
    invoke-direct {p0, v0, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->scrollToPositon(IZ)V

    goto :goto_0
.end method

.method public goRight(I)V
    .locals 3
    .param p1, "amount"    # I

    .prologue
    const/4 v2, 0x1

    .line 228
    const/4 v0, 0x0

    .line 230
    .local v0, "newPosition":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-eqz v1, :cond_2

    .line 231
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getCurrentPosition()I

    move-result v1

    add-int v0, v1, p1

    .line 232
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getDataCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getDataCount()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getCurrentPosition()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 237
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelView;->setCurrentPosition(I)V

    .line 238
    invoke-direct {p0, v0, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->scrollToPositon(IZ)V

    .line 244
    :cond_1
    :goto_0
    return-void

    .line 242
    :cond_2
    invoke-direct {p0, v0, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->scrollToPositon(IZ)V

    goto :goto_0
.end method

.method public goStart()V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelection(I)V

    .line 199
    return-void
.end method

.method public initView(Landroid/content/Context;Ljava/util/ArrayList;IIIII)V
    .locals 8
    .param p1, "c"    # Landroid/content/Context;
    .param p3, "dataType"    # I
    .param p4, "textSizeCenter"    # I
    .param p5, "textSizeSide"    # I
    .param p6, "visibleViews"    # I
    .param p7, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;IIIII)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const v0, 0x7f0b027e

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/WheelView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    .line 93
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/polarisoffice5/common/WheelView;->init(Lcom/infraware/polarisoffice5/common/WheelScroll;Ljava/util/ArrayList;IIIII)V

    .line 95
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 6
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    :goto_0
    return-void

    .line 253
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mFinalX:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 255
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/common/WheelScroll;->onScrollStart(I)V

    .line 257
    :cond_1
    iput p1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mFinalX:I

    .line 258
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 259
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 261
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    .line 263
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    new-instance v1, Landroid/graphics/Rect;

    add-int/lit8 v2, p1, -0x64

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/WheelView;->getWidth()I

    move-result v4

    add-int/2addr v4, p1

    add-int/lit8 v4, v4, 0x64

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/common/WheelView;->getHeight()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 504
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onSizeChanged(IIII)V

    .line 506
    new-instance v0, Lcom/infraware/polarisoffice5/common/WheelScroll$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll$4;-><init>(Lcom/infraware/polarisoffice5/common/WheelScroll;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->post(Ljava/lang/Runnable;)Z

    .line 530
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 541
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    const/4 v0, 0x1

    .line 544
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setOnItemScrollListener(Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->onItemScrollListener:Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;

    .line 65
    return-void
.end method

.method public setOnItemSelectListener(Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;)V
    .locals 0
    .param p1, "l"    # Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->onItemSelectListener:Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;

    .line 60
    return-void
.end method

.method public setSelectionDirect(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 138
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    if-nez v1, :cond_0

    .line 166
    :goto_0
    return-void

    .line 141
    :cond_0
    if-gez p1, :cond_1

    .line 142
    const/4 p1, 0x0

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getDataCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_2

    .line 145
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getDataCount()I

    move-result v1

    add-int/lit8 p1, v1, -0x1

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/common/WheelView;->setCurrentPosition(I)V

    .line 149
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getWidth()I

    move-result v0

    .line 150
    .local v0, "wid":I
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelView;->getWidth()I

    move-result v1

    if-ge v1, v0, :cond_4

    .line 151
    :cond_3
    iput p1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mInstantSelection:I

    .line 152
    new-instance v1, Lcom/infraware/polarisoffice5/common/WheelScroll$3;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll$3;-><init>(Lcom/infraware/polarisoffice5/common/WheelScroll;)V

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 163
    :cond_4
    const/4 v1, -0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mInstantSelection:I

    .line 164
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->scrollToPositon(IZ)V

    goto :goto_0
.end method

.method public setView(Lcom/infraware/polarisoffice5/common/WheelView;)V
    .locals 0
    .param p1, "view"    # Lcom/infraware/polarisoffice5/common/WheelView;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    .line 79
    return-void
.end method

.method public startTimer(II)V
    .locals 16
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 325
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_0

    .line 346
    :goto_0
    return-void

    .line 327
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerSyncObj:Ljava/lang/Object;

    monitor-enter v15

    .line 328
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    if-eqz v2, :cond_1

    .line 329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    .line 330
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    .line 333
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    if-nez v2, :cond_2

    .line 335
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    .line 336
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getWidth()I

    move-result v14

    .line 337
    .local v14, "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mView:Lcom/infraware/polarisoffice5/common/WheelView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/WheelView;->getVisibleWidth()I

    move-result v7

    .line 338
    .local v7, "unit_width":I
    sub-int v2, p1, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-ge v2, v14, :cond_3

    .line 339
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    new-instance v2, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;

    const/4 v4, 0x1

    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v2 .. v7}, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;-><init>(Lcom/infraware/polarisoffice5/common/WheelScroll;ZIII)V

    const-wide/16 v10, 0x1e

    const-wide/16 v12, 0x1e

    move-object v9, v2

    invoke-virtual/range {v8 .. v13}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 344
    .end local v7    # "unit_width":I
    .end local v14    # "width":I
    :cond_2
    :goto_1
    monitor-exit v15

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 341
    .restart local v7    # "unit_width":I
    .restart local v14    # "width":I
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    new-instance v2, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;

    const/4 v4, 0x0

    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v2 .. v7}, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;-><init>(Lcom/infraware/polarisoffice5/common/WheelScroll;ZIII)V

    const-wide/16 v10, 0x1e

    const-wide/16 v12, 0x1e

    move-object v9, v2

    invoke-virtual/range {v8 .. v13}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
