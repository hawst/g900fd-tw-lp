.class public Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
.super Landroid/widget/ImageView;
.source "MarkerPreviewImageView.java"


# instance fields
.field private mBorderPaint:Landroid/graphics/Paint;

.field mBorderThick:I

.field private mContext:Landroid/content/Context;

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private mPenStyle:I

.field private mPt:[Landroid/graphics/PointF;

.field private s_BSMatrix10by4:[F

.field private s_BSMatrix4by4:[F

.field private x:[F

.field private y:[F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    .line 23
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPenStyle:I

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mContext:Landroid/content/Context;

    .line 25
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderThick:I

    .line 26
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 28
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    .line 29
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    .line 33
    const/16 v0, 0x28

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    .line 46
    const/16 v0, 0x10

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix4by4:[F

    .line 56
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mContext:Landroid/content/Context;

    .line 57
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->init()V

    .line 58
    return-void

    .line 33
    :array_0
    .array-data 4
        0x3df8d4fe    # 0.1215f
        0x3f283c13
        0x3e627984
        0x392ec355    # 1.66667E-4f
        0x3daec33e
        0x3f21735f
        0x3e90b9af
        0x3aaec33b
        0x3d6a2798
        0x3f17152a
        0x3eb242e7
        0x3b9374bc    # 0.0045f
        0x3d1374bc    # 0.036f
        0x3f09e60f
        0x3ed44f30
        0x3c2ec33e
        0x3caaaaaa
        0x3ef55555
        0x3ef55555
        0x3caaaaaa
        0x3c2ec33d
        0x3ed44f30
        0x3f09e60f
        0x3d1374bc    # 0.036f
        0x3b9374bc    # 0.0045f
        0x3eb242e7
        0x3f17152a
        0x3d6a2798
        0x3aaec33b
        0x3e90b9af
        0x3f21735f
        0x3daec33e
        0x392ec355    # 1.66667E-4f
        0x3e627984
        0x3f283c13
        0x3df8d4fe    # 0.1215f
        0x0
        0x3e2aaaab
        0x3f2aaaab
        0x3e2aaaab
    .end array-data

    .line 46
    :array_1
    .array-data 4
        0x3d900000    # 0.0703125f
        0x3f1caaab
        0x3ea15555
        0x3b2aaaac
        0x3caaaaaa
        0x3ef55555
        0x3ef55555
        0x3caaaaaa
        0x3b2aaaa8
        0x3ea15555
        0x3f1caaab
        0x3d900000    # 0.0703125f
        0x0
        0x3e2aaaab
        0x3f2aaaab
        0x3e2aaaab
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    .line 23
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPenStyle:I

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mContext:Landroid/content/Context;

    .line 25
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderThick:I

    .line 26
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 28
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    .line 29
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    .line 33
    const/16 v0, 0x28

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    .line 46
    const/16 v0, 0x10

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix4by4:[F

    .line 62
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mContext:Landroid/content/Context;

    .line 63
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->init()V

    .line 64
    return-void

    .line 33
    :array_0
    .array-data 4
        0x3df8d4fe    # 0.1215f
        0x3f283c13
        0x3e627984
        0x392ec355    # 1.66667E-4f
        0x3daec33e
        0x3f21735f
        0x3e90b9af
        0x3aaec33b
        0x3d6a2798
        0x3f17152a
        0x3eb242e7
        0x3b9374bc    # 0.0045f
        0x3d1374bc    # 0.036f
        0x3f09e60f
        0x3ed44f30
        0x3c2ec33e
        0x3caaaaaa
        0x3ef55555
        0x3ef55555
        0x3caaaaaa
        0x3c2ec33d
        0x3ed44f30
        0x3f09e60f
        0x3d1374bc    # 0.036f
        0x3b9374bc    # 0.0045f
        0x3eb242e7
        0x3f17152a
        0x3d6a2798
        0x3aaec33b
        0x3e90b9af
        0x3f21735f
        0x3daec33e
        0x392ec355    # 1.66667E-4f
        0x3e627984
        0x3f283c13
        0x3df8d4fe    # 0.1215f
        0x0
        0x3e2aaaab
        0x3f2aaaab
        0x3e2aaaab
    .end array-data

    .line 46
    :array_1
    .array-data 4
        0x3d900000    # 0.0703125f
        0x3f1caaab
        0x3ea15555
        0x3b2aaaac
        0x3caaaaaa
        0x3ef55555
        0x3ef55555
        0x3caaaaaa
        0x3b2aaaa8
        0x3ea15555
        0x3f1caaab
        0x3d900000    # 0.0703125f
        0x0
        0x3e2aaaab
        0x3f2aaaab
        0x3e2aaaab
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 67
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    .line 23
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPenStyle:I

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mContext:Landroid/content/Context;

    .line 25
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderThick:I

    .line 26
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 28
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    .line 29
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    .line 33
    const/16 v0, 0x28

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    .line 46
    const/16 v0, 0x10

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix4by4:[F

    .line 68
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->init()V

    .line 69
    return-void

    .line 33
    :array_0
    .array-data 4
        0x3df8d4fe    # 0.1215f
        0x3f283c13
        0x3e627984
        0x392ec355    # 1.66667E-4f
        0x3daec33e
        0x3f21735f
        0x3e90b9af
        0x3aaec33b
        0x3d6a2798
        0x3f17152a
        0x3eb242e7
        0x3b9374bc    # 0.0045f
        0x3d1374bc    # 0.036f
        0x3f09e60f
        0x3ed44f30
        0x3c2ec33e
        0x3caaaaaa
        0x3ef55555
        0x3ef55555
        0x3caaaaaa
        0x3c2ec33d
        0x3ed44f30
        0x3f09e60f
        0x3d1374bc    # 0.036f
        0x3b9374bc    # 0.0045f
        0x3eb242e7
        0x3f17152a
        0x3d6a2798
        0x3aaec33b
        0x3e90b9af
        0x3f21735f
        0x3daec33e
        0x392ec355    # 1.66667E-4f
        0x3e627984
        0x3f283c13
        0x3df8d4fe    # 0.1215f
        0x0
        0x3e2aaaab
        0x3f2aaaab
        0x3e2aaaab
    .end array-data

    .line 46
    :array_1
    .array-data 4
        0x3d900000    # 0.0703125f
        0x3f1caaab
        0x3ea15555
        0x3b2aaaac
        0x3caaaaaa
        0x3ef55555
        0x3ef55555
        0x3caaaaaa
        0x3b2aaaa8
        0x3ea15555
        0x3f1caaab
        0x3d900000    # 0.0703125f
        0x0
        0x3e2aaaab
        0x3f2aaaab
        0x3e2aaaab
    .end array-data
.end method

.method private InkPenDraw(Landroid/graphics/Canvas;Landroid/graphics/Paint;[Landroid/graphics/PointF;Z)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "aPaint"    # Landroid/graphics/Paint;
    .param p3, "aPt"    # [Landroid/graphics/PointF;
    .param p4, "bInk"    # Z

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 186
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 187
    .local v0, "pt":Landroid/graphics/PointF;
    const/4 v1, 0x0

    .local v1, "t":I
    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_1

    .line 188
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x0

    aget v2, v2, v3

    aget-object v3, p3, v6

    iget v3, v3, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    aget-object v4, p3, v7

    iget v4, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x2

    aget v3, v3, v4

    aget-object v4, p3, v8

    iget v4, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x3

    aget v3, v3, v4

    aget-object v4, p3, v9

    iget v4, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 190
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x0

    aget v2, v2, v3

    aget-object v3, p3, v6

    iget v3, v3, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    aget-object v4, p3, v7

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x2

    aget v3, v3, v4

    aget-object v4, p3, v8

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x3

    aget v3, v3, v4

    aget-object v4, p3, v9

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 193
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 195
    if-eqz p4, :cond_0

    rem-int/lit8 v2, v1, 0x3

    if-eqz v2, :cond_0

    .line 196
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x3fef5c28f5c28f5cL    # 0.98

    mul-double/2addr v2, v4

    double-to-float v2, v2

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 197
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    invoke-virtual {p1, v2, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 187
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 200
    :cond_1
    return-void
.end method

.method private getPenData([Landroid/graphics/PointF;)V
    .locals 9
    .param p1, "aPt"    # [Landroid/graphics/PointF;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 174
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 175
    .local v0, "pt":Landroid/graphics/PointF;
    const/4 v1, 0x0

    .local v1, "t":I
    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    .line 176
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x0

    aget v2, v2, v3

    aget-object v3, p1, v5

    iget v3, v3, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    aget-object v4, p1, v6

    iget v4, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x2

    aget v3, v3, v4

    aget-object v4, p1, v7

    iget v4, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x3

    aget v3, v3, v4

    aget-object v4, p1, v8

    iget v4, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 178
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x0

    aget v2, v2, v3

    aget-object v3, p1, v5

    iget v3, v3, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    aget-object v4, p1, v6

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x2

    aget v3, v3, v4

    aget-object v4, p1, v7

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->s_BSMatrix10by4:[F

    mul-int/lit8 v4, v1, 0x4

    add-int/lit8 v4, v4, 0x3

    aget v3, v3, v4

    aget-object v4, p1, v8

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 181
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 175
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 183
    :cond_0
    return-void
.end method

.method private init()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 73
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0201ef

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 74
    .local v1, "bmp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v0, v5, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 75
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    sget-object v5, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v6, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 76
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v9, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderThick:I

    .line 79
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 80
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0501bf

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderThick:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 85
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    .line 86
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 87
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 88
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 89
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 90
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    const v6, -0x1cd2f1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    const/16 v6, 0x28

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 96
    const/4 v2, 0x0

    .line 97
    .local v2, "correctValY":F
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.infraware.polarisoffice5.ppt.SlideShowActivity"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 98
    const/high16 v2, -0x3f800000    # -4.0f

    .line 100
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, -0x3df00000    # -36.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v10

    .line 101
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x422c0000    # 43.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v10

    .line 103
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v9

    .line 104
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41d80000    # 27.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v9

    .line 106
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x425c0000    # 55.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v11

    .line 107
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41900000    # 18.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v11

    .line 109
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x42b00000    # 88.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v12

    .line 110
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41900000    # 18.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v12

    .line 112
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x431b0000    # 155.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v13

    .line 113
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x420c0000    # 35.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    aput v6, v5, v13

    .line 115
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    const/4 v6, 0x5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v8, 0x433c0000    # 188.0f

    invoke-static {v7, v8}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    int-to-float v7, v7

    aput v7, v5, v6

    .line 116
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    const/4 v6, 0x5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v8, 0x420c0000    # 35.0f

    invoke-static {v7, v8}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    int-to-float v7, v7

    aput v7, v5, v6

    .line 118
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    const/4 v6, 0x6

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v8, 0x435e0000    # 222.0f

    invoke-static {v7, v8}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    int-to-float v7, v7

    aput v7, v5, v6

    .line 119
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    const/4 v6, 0x6

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v8, 0x41b40000    # 22.5f

    invoke-static {v7, v8}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    int-to-float v7, v7

    aput v7, v5, v6

    .line 121
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    const/4 v6, 0x7

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v8, 0x437f0000    # 255.0f

    invoke-static {v7, v8}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    int-to-float v7, v7

    aput v7, v5, v6

    .line 122
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    const/4 v6, 0x7

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v8, 0x41200000    # 10.0f

    invoke-static {v7, v8}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    int-to-float v7, v7

    aput v7, v5, v6

    .line 123
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    .line 125
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    aput-object v6, v5, v10

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    aput-object v6, v5, v9

    .line 126
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    aput-object v6, v5, v11

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    aput-object v6, v5, v12

    .line 127
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    aget v6, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    aget v7, v7, v9

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 128
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    const/4 v5, 0x5

    if-ge v4, v5, :cond_2

    .line 129
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v13, :cond_1

    .line 130
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    aget-object v5, v5, v3

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    add-int v7, v3, v4

    aget v6, v6, v7

    iput v6, v5, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    aget-object v5, v5, v3

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    add-int v7, v3, v4

    aget v6, v6, v7

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 129
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 132
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    invoke-direct {p0, v5}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getPenData([Landroid/graphics/PointF;)V

    .line 128
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 135
    .end local v3    # "i":I
    :cond_2
    iput v10, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPenStyle:I

    .line 136
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x1

    .line 204
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 206
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getWidth()I

    move-result v0

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderThick:I

    sub-int/2addr v0, v2

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getHeight()I

    move-result v0

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderThick:I

    sub-int/2addr v0, v2

    int-to-float v4, v0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mBorderPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 209
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPenStyle:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 210
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 211
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v8

    .line 212
    .local v8, "nPenSize":F
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    aget v1, v1, v9

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    aget v2, v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 213
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_0
    const/4 v0, 0x5

    if-ge v7, v0, :cond_1

    .line 214
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/4 v0, 0x4

    if-ge v6, v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    add-int v2, v6, v7

    aget v1, v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    add-int v2, v6, v7

    aget v1, v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 214
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    invoke-direct {p0, p1, v0, v1, v9}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->InkPenDraw(Landroid/graphics/Canvas;Landroid/graphics/Paint;[Landroid/graphics/PointF;Z)V

    .line 213
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 219
    .end local v6    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 225
    .end local v7    # "j":I
    .end local v8    # "nPenSize":F
    :goto_2
    return-void

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_2
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 162
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    .line 163
    .local v0, "alpha":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 164
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 165
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->invalidate()V

    .line 166
    return-void
.end method

.method public setPenStyle(I)V
    .locals 6
    .param p1, "nPenStyle"    # I

    .prologue
    const/4 v5, 0x1

    .line 145
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 146
    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    .line 147
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41b00000    # 22.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x41d00000    # 26.0f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 148
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x435e0000    # 222.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x41c80000    # 25.0f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 158
    :cond_0
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPenStyle:I

    .line 159
    return-void

    .line 150
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPath:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    aget v3, v3, v5

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    aget v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 151
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    .line 152
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    .line 153
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->x:[F

    add-int v4, v0, v1

    aget v3, v3, v4

    iput v3, v2, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->y:[F

    add-int v4, v0, v1

    aget v3, v3, v4

    iput v3, v2, Landroid/graphics/PointF;->y:F

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 155
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPt:[Landroid/graphics/PointF;

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getPenData([Landroid/graphics/PointF;)V

    .line 151
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setThickness(I)V
    .locals 2
    .param p1, "thickness"    # I

    .prologue
    .line 139
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 140
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->invalidate()V

    .line 141
    return-void
.end method

.method public setTransparency(I)V
    .locals 1
    .param p1, "transparency"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 170
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->invalidate()V

    .line 171
    return-void
.end method
