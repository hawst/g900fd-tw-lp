.class public Lcom/infraware/polarisoffice5/common/ColorPickerHueView;
.super Landroid/view/View;
.source "ColorPickerHueView.java"


# instance fields
.field private mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

.field private mHue:F

.field private mHueBarColors:[I

.field private mHueBarRect:Landroid/graphics/Rect;

.field private mHuePoint:Landroid/graphics/Bitmap;

.field private mHueViewRect:Landroid/graphics/Rect;

.field private mNormalPoint:Landroid/graphics/Bitmap;

.field private mPaint:Landroid/graphics/Paint;

.field private mPortrait:Z

.field private mPressPoint:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    .line 22
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueViewRect:Landroid/graphics/Rect;

    .line 23
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    .line 24
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    .line 25
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 26
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    .line 27
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    .line 22
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueViewRect:Landroid/graphics/Rect;

    .line 23
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    .line 24
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    .line 25
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 26
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    .line 27
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    .line 22
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueViewRect:Landroid/graphics/Rect;

    .line 23
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    .line 24
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    .line 25
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 26
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    .line 27
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .line 56
    return-void
.end method

.method private SetHueBarColors(I)V
    .locals 10
    .param p1, "count"    # I

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0xff

    .line 77
    new-array v6, p1, [I

    iput-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    .line 79
    const/4 v1, 0x0

    .line 80
    .local v1, "index":I
    div-int/lit8 v4, p1, 0x6

    .line 81
    .local v4, "quotient":I
    rem-int/lit8 v5, p1, 0x6

    .line 82
    .local v5, "remainder":I
    const/high16 v6, 0x437f0000    # 255.0f

    add-int/lit8 v7, v4, -0x1

    int-to-float v7, v7

    div-float v3, v6, v7

    .line 83
    .local v3, "offset":F
    const/4 v0, 0x0

    .local v0, "i":I
    move v2, v1

    .end local v1    # "index":I
    .local v2, "index":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 84
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    int-to-float v7, v0

    mul-float/2addr v7, v3

    float-to-int v7, v7

    invoke-static {v8, v8, v7, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 83
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_0

    .line 85
    :cond_0
    if-lez v5, :cond_a

    .line 86
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-static {v8, v8, v8, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 88
    :goto_1
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :goto_2
    if-ge v0, v4, :cond_1

    .line 89
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    int-to-float v7, v0

    mul-float/2addr v7, v3

    float-to-int v7, v7

    rsub-int v7, v7, 0xff

    invoke-static {v8, v7, v8, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 88
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_2

    .line 90
    :cond_1
    const/4 v6, 0x1

    if-le v5, v6, :cond_9

    .line 91
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-static {v8, v9, v8, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 93
    :goto_3
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :goto_4
    if-ge v0, v4, :cond_2

    .line 94
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    int-to-float v7, v0

    mul-float/2addr v7, v3

    float-to-int v7, v7

    invoke-static {v8, v9, v8, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 93
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_4

    .line 95
    :cond_2
    const/4 v6, 0x2

    if-le v5, v6, :cond_8

    .line 96
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-static {v8, v9, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 98
    :goto_5
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :goto_6
    if-ge v0, v4, :cond_3

    .line 99
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    int-to-float v7, v0

    mul-float/2addr v7, v3

    float-to-int v7, v7

    rsub-int v7, v7, 0xff

    invoke-static {v8, v9, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 98
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_6

    .line 100
    :cond_3
    const/4 v6, 0x3

    if-le v5, v6, :cond_7

    .line 101
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-static {v8, v9, v9, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 103
    :goto_7
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :goto_8
    if-ge v0, v4, :cond_4

    .line 104
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    int-to-float v7, v0

    mul-float/2addr v7, v3

    float-to-int v7, v7

    invoke-static {v8, v7, v9, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 103
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_8

    .line 105
    :cond_4
    const/4 v6, 0x4

    if-le v5, v6, :cond_6

    .line 106
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-static {v8, v8, v9, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 108
    :goto_9
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :goto_a
    if-ge v0, v4, :cond_5

    .line 109
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    int-to-float v7, v0

    mul-float/2addr v7, v3

    float-to-int v7, v7

    rsub-int v7, v7, 0xff

    invoke-static {v8, v8, v9, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v2

    .line 108
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_a

    .line 110
    :cond_5
    return-void

    :cond_6
    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_9

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :cond_7
    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_7

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :cond_8
    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_5

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :cond_9
    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto/16 :goto_3

    .end local v1    # "index":I
    .restart local v2    # "index":I
    :cond_a
    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto/16 :goto_1
.end method


# virtual methods
.method public Init()V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPaint:Landroid/graphics/Paint;

    .line 60
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 62
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPortrait:Z

    .line 64
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020042

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 65
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020041

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    .line 73
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    .line 74
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPortrait:Z

    .line 69
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020044

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 70
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020043

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getH()F
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x43b40000    # 360.0f

    .line 136
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPortrait:Z

    if-eqz v0, :cond_2

    .line 141
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    array-length v0, v0

    if-ge v7, v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    aget v1, v1, v7

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v7

    int-to-float v1, v0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v7

    int-to-float v3, v0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 141
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    div-float/2addr v1, v8

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int v6, v0, v1

    .line 147
    .local v6, "huePoint":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v6, v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 151
    .end local v6    # "huePoint":I
    .end local v7    # "i":I
    :cond_2
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    array-length v0, v0

    if-ge v7, v0, :cond_3

    .line 152
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarColors:[I

    aget v1, v1, v7

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 153
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v7

    int-to-float v2, v0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v7

    int-to-float v4, v0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 151
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 156
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    div-float/2addr v1, v8

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int v6, v0, v1

    .line 157
    .restart local v6    # "huePoint":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v6, v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 115
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 117
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPortrait:Z

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueViewRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getPaddingRight()I

    move-result v3

    sub-int v3, p1, v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getPaddingBottom()I

    move-result v4

    sub-int v4, p2, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 119
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueViewRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 120
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->SetHueBarColors(I)V

    .line 130
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 131
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueViewRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getPaddingRight()I

    move-result v3

    sub-int v3, p1, v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getPaddingBottom()I

    move-result v4

    sub-int v4, p2, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 125
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueViewRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 126
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->SetHueBarColors(I)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v5, 0x43b40000    # 360.0f

    .line 163
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    .line 165
    .local v0, "oldHue":F
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPortrait:Z

    if-eqz v3, :cond_1

    .line 167
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 190
    :pswitch_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    .line 229
    :goto_0
    iget v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    sub-float v3, v0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v3, v3

    const-wide v5, 0x3fb999999999999aL    # 0.1

    cmpl-double v3, v3, v5

    if-lez v3, :cond_0

    .line 230
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    if-eqz v3, :cond_0

    .line 232
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onChangeHue(F)V

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->invalidate()V

    .line 238
    const/4 v3, 0x1

    return v3

    .line 170
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 171
    .local v1, "x":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 172
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v3

    .line 173
    int-to-float v3, v1

    mul-float/2addr v3, v5

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    .line 175
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPressPoint:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 181
    .end local v1    # "x":I
    :pswitch_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    .line 182
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 183
    .restart local v1    # "x":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 184
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v3

    .line 185
    int-to-float v3, v1

    mul-float/2addr v3, v5

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    .line 186
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 196
    .end local v1    # "x":I
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 220
    :pswitch_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 199
    :pswitch_4
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 201
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 202
    .local v2, "y":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 203
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    .line 204
    int-to-float v3, v2

    mul-float/2addr v3, v5

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    .line 205
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mPressPoint:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 211
    .end local v2    # "y":I
    :pswitch_6
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    .line 212
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 213
    .restart local v2    # "y":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 214
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    .line 215
    int-to-float v3, v2

    mul-float/2addr v3, v5

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHueBarRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    .line 216
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHuePoint:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 196
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method public setHue(F)V
    .locals 0
    .param p1, "hue"    # F

    .prologue
    .line 33
    iput p1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mHue:F

    .line 34
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->invalidate()V

    .line 35
    return-void
.end method

.method public setParent(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V
    .locals 0
    .param p1, "cpa"    # Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .line 43
    return-void
.end method
