.class public Lcom/infraware/polarisoffice5/common/PopupMenuWindow;
.super Landroid/widget/PopupWindow;
.source "PopupMenuWindow.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_CURSOR_MODE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_STATUS;


# static fields
.field static final POPUPMENU_AROW_UNDERCENTER:I = 0x5

.field static final POPUPMENU_AROW_UNDERLEFT:I = 0x4

.field static final POPUPMENU_AROW_UNDERRIGHT:I = 0x6

.field static final POPUPMENU_AROW_UPCENTER:I = 0x2

.field static final POPUPMENU_AROW_UPLEFT:I = 0x1

.field static final POPUPMENU_AROW_UPRIGHT:I = 0x3

.field static final POPUPMENU_ARROW_GAB:I = 0x21

.field static final POPUPMENU_MAX_ITEM_SIZE:I = 0xa

.field public static final POPUPMENU_MSG_ENDTIMER:I = 0x2

.field public static final POPUPMENU_MSG_NONE:I = 0x0

.field public static final POPUPMENU_MSG_STARTTIMER:I = 0x1

.field static final POPUPMENU_TYPE_CARET:I = 0x1

.field static final POPUPMENU_TYPE_GENERAL:I = 0x0

.field static final POPUPMENU_TYPE_ROTATE:I = 0x2

.field static final POPUPMENU_VERTICAL_GAP:I = 0x19

.field static final POPUPMENU_WAIT_TIME:I = 0x32

.field public static final POPUP_CMDID_ADD_STICKYNOTE:I = 0xc

.field public static final POPUP_CMDID_COPY:I = 0x1

.field public static final POPUP_CMDID_DELETE_ANNOT:I = 0xe

.field public static final POPUP_CMDID_MORE:I = 0x5

.field public static final POPUP_CMDID_RUN_HYPERLINK:I = 0x12

.field public static final POPUP_CMDID_RUN_MEMO:I = 0x11

.field public static final POPUP_CMDID_SELECT_ALL:I = 0x10


# instance fields
.field mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

.field mCmdArray:[I

.field mDocType:I

.field mHandler:Landroid/os/Handler;

.field mInterface:Lcom/infraware/office/evengine/EvInterface;

.field public mIsMemoAndHyperlink:Z

.field private mIsShowMoreWindow:Z

.field public mIsUpperExist:Z

.field mItemSize:I

.field mLinearLayout:Landroid/widget/LinearLayout;

.field private mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

.field mParentRect:Landroid/graphics/Rect;

.field mParentView:Landroid/view/View;

.field public mPdfAnnotType:I

.field mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

.field public mPopupRect:Landroid/graphics/Rect;

.field mPopupType:I

.field mRootFrame:Landroid/widget/FrameLayout;

.field mScrollView:Landroid/widget/HorizontalScrollView;

.field mSelectRect:Landroid/graphics/Rect;

.field private mTouchUpShowFlag:Z

.field mWaitRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Landroid/view/View;Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;)V
    .locals 7
    .param p1, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p2, "parentView"    # Landroid/view/View;
    .param p3, "op"    # Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x2

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 135
    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 80
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 81
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 82
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 84
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    .line 86
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    .line 87
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    .line 88
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    .line 90
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 91
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 92
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    .line 94
    iput v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    .line 95
    iput v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    .line 97
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    .line 103
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mHandler:Landroid/os/Handler;

    .line 106
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    .line 108
    iput v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    .line 110
    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .line 114
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupRect:Landroid/graphics/Rect;

    .line 118
    iput v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPdfAnnotType:I

    .line 119
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsMemoAndHyperlink:Z

    .line 121
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mTouchUpShowFlag:Z

    .line 138
    new-instance v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-direct {v0, p1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    .line 140
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mHandler:Landroid/os/Handler;

    .line 142
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 143
    invoke-virtual {p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    .line 144
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 145
    invoke-virtual {p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getClipboardhelper()Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 146
    :cond_0
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 147
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .line 149
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    .line 151
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setOutsideTouchable(Z)V

    .line 152
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setFocusable(Z)V

    .line 154
    new-instance v0, Landroid/widget/HorizontalScrollView;

    invoke-direct {v0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 155
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 158
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 164
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    .line 165
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 167
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow$1;-><init>(Lcom/infraware/polarisoffice5/common/PopupMenuWindow;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;)V

    .line 195
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    const v1, 0x7f020122

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setBackgroundResource(I)V

    .line 200
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setOutsideTouchable(Z)V

    .line 202
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 207
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setContentView(Landroid/view/View;)V

    .line 211
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 226
    return-void
.end method

.method private AddButton(I)V
    .locals 2
    .param p1, "cmd_id"    # I

    .prologue
    .line 1884
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIsEncryptDoc()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1902
    :cond_0
    :goto_0
    return-void

    .line 1896
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    .line 1899
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    iget v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    aput p1, v0, v1

    .line 1901
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    goto :goto_0
.end method

.method private deleteButton(I)V
    .locals 5
    .param p1, "cmd"    # I

    .prologue
    .line 815
    const/4 v0, -0x1

    .line 816
    .local v0, "idx":I
    const/4 v1, 0x0

    .local v1, "k":I
    :goto_0
    iget v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    if-ge v1, v2, :cond_0

    .line 817
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    aget v2, v2, v1

    if-ne v2, p1, :cond_1

    .line 818
    move v0, v1

    .line 823
    :cond_0
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 824
    move v1, v0

    :goto_1
    iget v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_2

    .line 825
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    add-int/lit8 v4, v1, 0x1

    aget v3, v3, v4

    aput v3, v2, v1

    .line 824
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 816
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 828
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    iget v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 829
    iget v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    .line 831
    :cond_3
    return-void
.end method

.method private getResId(I)I
    .locals 1
    .param p1, "cmd_id"    # I

    .prologue
    .line 2159
    const v0, 0x7f0200d6

    .line 2161
    .local v0, "res_id":I
    sparse-switch p1, :sswitch_data_0

    .line 2210
    :goto_0
    return v0

    .line 2163
    :sswitch_0
    const v0, 0x7f0200d6

    .line 2164
    goto :goto_0

    .line 2166
    :sswitch_1
    const v0, 0x7f0200e7

    .line 2167
    goto :goto_0

    .line 2189
    :sswitch_2
    const v0, 0x7f0200ed

    .line 2190
    goto :goto_0

    .line 2195
    :sswitch_3
    const v0, 0x7f0200dc

    .line 2196
    goto :goto_0

    .line 2201
    :sswitch_4
    const v0, 0x7f0200d3

    .line 2202
    goto :goto_0

    .line 2204
    :sswitch_5
    const v0, 0x7f0200e4

    .line 2205
    goto :goto_0

    .line 2207
    :sswitch_6
    const v0, 0x7f0200df

    goto :goto_0

    .line 2161
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
        0xc -> :sswitch_2
        0xe -> :sswitch_3
        0x10 -> :sswitch_4
        0x11 -> :sswitch_5
        0x12 -> :sswitch_6
    .end sparse-switch
.end method

.method private getShowMenuPoint(II)Landroid/graphics/Point;
    .locals 26
    .param p1, "popup_w"    # I
    .param p2, "popup_h"    # I

    .prologue
    .line 1048
    new-instance v11, Landroid/graphics/Point;

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v11, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 1053
    .local v11, "pt":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v21, v0

    const/high16 v22, 0x40c00000    # 6.0f

    invoke-static/range {v21 .. v22}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    .line 1055
    .local v7, "dip6":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    .line 1058
    .local v10, "pc":Landroid/graphics/Rect;
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    add-int v21, v21, v22

    sub-int v21, v21, p1

    div-int/lit8 v21, v21, 0x2

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    .line 1059
    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    add-int v21, v21, v22

    sub-int v21, v21, p2

    div-int/lit8 v21, v21, 0x2

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->y:I

    .line 1061
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    .line 1062
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1064
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_1

    .line 1065
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1067
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_2

    .line 1068
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1070
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_3

    .line 1071
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1073
    :cond_3
    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    .line 1074
    .local v15, "rect_top":Landroid/graphics/Rect;
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 1075
    .local v17, "rect_y_center":Landroid/graphics/Rect;
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 1076
    .local v12, "rect_bottom":Landroid/graphics/Rect;
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 1077
    .local v13, "rect_left":Landroid/graphics/Rect;
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 1078
    .local v16, "rect_x_center":Landroid/graphics/Rect;
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 1080
    .local v14, "rect_right":Landroid/graphics/Rect;
    const/4 v5, 0x0

    .line 1082
    .local v5, "bOk":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    add-int v21, v21, v22

    div-int/lit8 v6, v21, 0x2

    .line 1084
    .local v6, "center":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    .line 1086
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getTextMarkInfo_for_popup()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-result-object v19

    .line 1087
    .local v19, "text_mark":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v8

    .line 1089
    .local v8, "icon_height":I
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    sub-int v24, v24, v8

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1090
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1091
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    add-int v22, v22, v8

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1094
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1095
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move-object/from16 v0, v16

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1096
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1127
    .end local v8    # "icon_height":I
    .end local v19    # "text_mark":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    :goto_0
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    .line 1128
    :cond_4
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    add-int v21, v21, v22

    div-int/lit8 v21, v21, 0x2

    move/from16 v0, v21

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->top:I

    move/from16 v0, v21

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1129
    :cond_5
    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v12, Landroid/graphics/Rect;->top:I

    .line 1131
    :cond_6
    iget v0, v13, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    iget v0, v13, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_7

    iget v0, v13, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v13, Landroid/graphics/Rect;->right:I

    .line 1132
    :cond_7
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_8

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    add-int v21, v21, v22

    div-int/lit8 v21, v21, 0x2

    move/from16 v0, v21

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->left:I

    move/from16 v0, v21

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1133
    :cond_8
    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_9

    iget v0, v14, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v14, Landroid/graphics/Rect;->left:I

    .line 1136
    :cond_9
    move/from16 v18, p2

    .line 1138
    .local v18, "t_height":I
    const/16 v20, 0x0

    .line 1139
    .local v20, "ymax_type":I
    const/4 v9, 0x0

    .line 1141
    .local v9, "max_height":I
    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v21

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_10

    .line 1143
    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v21

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_f

    .line 1145
    const/16 v20, 0x0

    .line 1146
    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v9

    .line 1168
    :goto_1
    move/from16 v0, v18

    if-gt v0, v9, :cond_14

    .line 1170
    div-int/lit8 v21, p1, 0x2

    sub-int v21, v6, v21

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    .line 1173
    packed-switch v20, :pswitch_data_0

    .line 1202
    :cond_a
    :goto_2
    iget v0, v11, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v0, v7, :cond_b

    .line 1203
    iput v7, v11, Landroid/graphics/Point;->x:I

    .line 1205
    :cond_b
    iget v0, v11, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    add-int v21, v21, p1

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    sub-int v22, v22, v7

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_c

    .line 1206
    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    sub-int v21, v21, v7

    sub-int v21, v21, p1

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    .line 1225
    :cond_c
    :goto_3
    return-object v11

    .line 1099
    .end local v9    # "max_height":I
    .end local v18    # "t_height":I
    .end local v20    # "ymax_type":I
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 1102
    move v8, v7

    .line 1104
    .restart local v8    # "icon_height":I
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1105
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1106
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    add-int v22, v22, v8

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1108
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move-object/from16 v0, v16

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 1116
    .end local v8    # "icon_height":I
    :cond_e
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    mul-int/lit8 v25, v7, 0x2

    sub-int v24, v24, v25

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1117
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    mul-int/lit8 v23, v7, 0x2

    add-int v22, v22, v23

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    mul-int/lit8 v25, v7, 0x2

    sub-int v24, v24, v25

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1118
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    mul-int/lit8 v23, v7, 0x2

    add-int v22, v22, v23

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1120
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    add-int v21, v21, v7

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v23, v0

    mul-int/lit8 v24, v7, 0x2

    sub-int v23, v23, v24

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    mul-int/lit8 v22, v7, 0x2

    add-int v21, v21, v22

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    mul-int/lit8 v24, v7, 0x2

    sub-int v23, v23, v24

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move-object/from16 v0, v16

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    mul-int/lit8 v22, v7, 0x2

    add-int v21, v21, v22

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    add-int v22, v22, v7

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    sub-int v23, v23, v7

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v7

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 1150
    .restart local v9    # "max_height":I
    .restart local v18    # "t_height":I
    .restart local v20    # "ymax_type":I
    :cond_f
    const/16 v20, 0x2

    .line 1151
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v9

    goto/16 :goto_1

    .line 1156
    :cond_10
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v21

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_11

    .line 1158
    const/16 v20, 0x2

    .line 1159
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v9

    goto/16 :goto_1

    .line 1163
    :cond_11
    const/16 v20, 0x1

    .line 1164
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v9

    goto/16 :goto_1

    .line 1176
    :pswitch_0
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    sub-int v21, v21, p2

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->y:I

    goto/16 :goto_2

    .line 1179
    :pswitch_1
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    add-int v21, v21, v22

    sub-int v21, v21, v18

    div-int/lit8 v21, v21, 0x2

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->y:I

    .line 1180
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v21, v0

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    .line 1183
    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v21

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_13

    .line 1184
    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    .line 1191
    :cond_12
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->width()I

    move-result v22

    sub-int v22, v22, p1

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_a

    .line 1192
    div-int/lit8 v21, p1, 0x2

    sub-int v21, v6, v21

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    goto/16 :goto_2

    .line 1187
    :cond_13
    iget v0, v13, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    sub-int v21, v21, p1

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    .line 1188
    iget v0, v11, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_12

    .line 1189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    goto :goto_4

    .line 1196
    :pswitch_2
    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->y:I

    goto/16 :goto_2

    .line 1211
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v21, v0

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    .line 1214
    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v21

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_16

    .line 1215
    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    .line 1221
    :cond_15
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->width()I

    move-result v22

    sub-int v22, v22, p1

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_c

    .line 1222
    div-int/lit8 v21, p1, 0x2

    sub-int v21, v6, v21

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    goto/16 :goto_3

    .line 1217
    :cond_16
    iget v0, v13, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    sub-int v21, v21, p1

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    .line 1218
    iget v0, v11, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_15

    .line 1219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v11, Landroid/graphics/Point;->x:I

    goto :goto_5

    .line 1173
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getShowMorePoint(II)Landroid/graphics/Point;
    .locals 30
    .param p1, "more_w"    # I
    .param p2, "more_h"    # I

    .prologue
    .line 1231
    new-instance v13, Landroid/graphics/Point;

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v13, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 1236
    .local v13, "pt":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    const/high16 v26, 0x40c00000    # 6.0f

    invoke-static/range {v25 .. v26}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    .line 1238
    .local v6, "dip6":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    .line 1243
    .local v12, "pc":Landroid/graphics/Rect;
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v25, v25, p1

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    .line 1244
    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v25, v25, p2

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->y:I

    .line 1246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_0

    .line 1247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1249
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_1

    .line 1250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1252
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_2

    .line 1253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1255
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_3

    .line 1256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1258
    :cond_3
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 1259
    .local v17, "rect_top":Landroid/graphics/Rect;
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 1260
    .local v19, "rect_y_center":Landroid/graphics/Rect;
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 1261
    .local v14, "rect_bottom":Landroid/graphics/Rect;
    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    .line 1262
    .local v15, "rect_left":Landroid/graphics/Rect;
    new-instance v18, Landroid/graphics/Rect;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Rect;-><init>()V

    .line 1263
    .local v18, "rect_x_center":Landroid/graphics/Rect;
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 1265
    .local v16, "rect_right":Landroid/graphics/Rect;
    const/4 v5, 0x0

    .line 1267
    .local v5, "bOk":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v25

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_c

    .line 1269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getTextMarkInfo_for_popup()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-result-object v22

    .line 1270
    .local v22, "text_mark":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->height()I

    move-result v7

    .line 1272
    .local v7, "icon_height":I
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    sub-int v28, v28, v7

    sub-int v28, v28, v6

    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1273
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v19

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1274
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    add-int v26, v26, v7

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1277
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v16

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1310
    .end local v7    # "icon_height":I
    .end local v22    # "text_mark":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    :goto_0
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_4

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1311
    :cond_4
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_5

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    add-int v25, v25, v26

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->top:I

    move/from16 v0, v25

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1312
    :cond_5
    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_6

    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v14, Landroid/graphics/Rect;->top:I

    .line 1314
    :cond_6
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_7

    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1315
    :cond_7
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_8

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    add-int v25, v25, v26

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->left:I

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1316
    :cond_8
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_9

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1320
    :cond_9
    const/16 v24, 0x0

    .line 1321
    .local v24, "ymax_type":I
    const/4 v9, 0x0

    .line 1323
    .local v9, "max_height":I
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v25

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_f

    .line 1325
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v25

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_e

    .line 1327
    const/16 v24, 0x0

    .line 1328
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v9

    .line 1350
    :goto_1
    move/from16 v0, p2

    if-gt v0, v9, :cond_11

    .line 1353
    packed-switch v24, :pswitch_data_0

    .line 1366
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v25, v25, p1

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    .line 1369
    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-ge v0, v6, :cond_a

    .line 1370
    iput v6, v13, Landroid/graphics/Point;->x:I

    .line 1372
    :cond_a
    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    add-int v25, v25, p1

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    sub-int v26, v26, v6

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_b

    .line 1373
    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    sub-int v25, v25, v6

    sub-int v25, v25, p1

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    .line 1477
    :cond_b
    :goto_3
    return-object v13

    .line 1282
    .end local v9    # "max_height":I
    .end local v24    # "ymax_type":I
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_d

    .line 1285
    move v7, v6

    .line 1287
    .restart local v7    # "icon_height":I
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1288
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v19

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1289
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    add-int v26, v26, v7

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1291
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v16

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 1299
    .end local v7    # "icon_height":I
    :cond_d
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    mul-int/lit8 v29, v6, 0x2

    sub-int v28, v28, v29

    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1300
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    mul-int/lit8 v27, v6, 0x2

    add-int v26, v26, v27

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    mul-int/lit8 v29, v6, 0x2

    sub-int v28, v28, v29

    move-object/from16 v0, v19

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1301
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    mul-int/lit8 v27, v6, 0x2

    add-int v26, v26, v27

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1303
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v27, v0

    mul-int/lit8 v28, v6, 0x2

    sub-int v27, v27, v28

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    mul-int/lit8 v26, v6, 0x2

    add-int v25, v25, v26

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    mul-int/lit8 v28, v6, 0x2

    sub-int v27, v27, v28

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    mul-int/lit8 v26, v6, 0x2

    add-int v25, v25, v26

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v26, v26, v6

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v27, v0

    sub-int v27, v27, v6

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v6

    move-object/from16 v0, v16

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 1332
    .restart local v9    # "max_height":I
    .restart local v24    # "ymax_type":I
    :cond_e
    const/16 v24, 0x2

    .line 1333
    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v9

    goto/16 :goto_1

    .line 1338
    :cond_f
    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v25

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_10

    .line 1340
    const/16 v24, 0x2

    .line 1341
    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v9

    goto/16 :goto_1

    .line 1345
    :cond_10
    const/16 v24, 0x1

    .line 1346
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v9

    goto/16 :goto_1

    .line 1356
    :pswitch_0
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    sub-int v25, v25, p2

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->y:I

    goto/16 :goto_2

    .line 1359
    :pswitch_1
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v25, v25, p2

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->y:I

    goto/16 :goto_2

    .line 1362
    :pswitch_2
    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->y:I

    goto/16 :goto_2

    .line 1379
    :cond_11
    const/16 v23, 0x2

    .line 1382
    .local v23, "xxx_type":I
    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v25

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_18

    .line 1384
    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v25

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_17

    .line 1386
    const/16 v23, 0x0

    .line 1419
    :cond_12
    :goto_4
    new-instance v11, Landroid/graphics/Point;

    invoke-direct {v11}, Landroid/graphics/Point;-><init>()V

    .line 1420
    .local v11, "outSize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1421
    iget v0, v11, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    .line 1422
    .local v21, "screenWidth":I
    iget v0, v11, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    .line 1426
    .local v20, "screenHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 1429
    .local v8, "indicatorPos":I
    sub-int v25, v20, v8

    mul-int/lit8 v26, v6, 0x2

    sub-int v25, v25, v26

    move/from16 v0, p2

    move/from16 v1, v25

    if-le v0, v1, :cond_13

    .line 1431
    sub-int v25, v20, v8

    mul-int/lit8 v26, v6, 0x2

    sub-int p2, v25, v26

    .line 1432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setHeight(I)V

    .line 1435
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v25, v25, p2

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->y:I

    .line 1436
    iget v0, v13, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    add-int v26, v8, v6

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_14

    .line 1437
    add-int v25, v8, v6

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->y:I

    .line 1439
    :cond_14
    iget v0, v13, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    add-int v25, v25, p2

    sub-int v26, v20, v6

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_15

    .line 1440
    sub-int v25, v20, v6

    sub-int v25, v25, p2

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->y:I

    .line 1443
    :cond_15
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getAppRect(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v10

    .line 1444
    .local v10, "mwRect":Landroid/graphics/Rect;
    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v25

    if-nez v25, :cond_16

    if-eqz v10, :cond_16

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    iget v0, v13, Landroid/graphics/Point;->y:I

    move/from16 v26, v0

    add-int v25, v25, v26

    add-int v25, v25, p2

    sub-int v26, v20, v6

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_16

    .line 1445
    sub-int v25, v20, v6

    sub-int v25, v25, p2

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->y:I

    .line 1451
    :cond_16
    packed-switch v23, :pswitch_data_1

    goto/16 :goto_3

    .line 1454
    :pswitch_3
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    sub-int v25, v25, p1

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    .line 1456
    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v26, v0

    add-int v26, v26, v6

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_b

    .line 1457
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    goto/16 :goto_3

    .line 1390
    .end local v8    # "indicatorPos":I
    .end local v10    # "mwRect":Landroid/graphics/Rect;
    .end local v11    # "outSize":Landroid/graphics/Point;
    .end local v20    # "screenHeight":I
    .end local v21    # "screenWidth":I
    :cond_17
    const/16 v23, 0x2

    goto/16 :goto_4

    .line 1395
    :cond_18
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v25

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_19

    .line 1397
    const/16 v23, 0x2

    goto/16 :goto_4

    .line 1401
    :cond_19
    const/16 v23, 0x1

    .line 1403
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v25

    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_1a

    .line 1405
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v25

    move/from16 v0, v25

    move/from16 v1, p1

    if-lt v0, v1, :cond_12

    .line 1406
    const/16 v23, 0x2

    goto/16 :goto_4

    .line 1410
    :cond_1a
    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v25

    move/from16 v0, v25

    move/from16 v1, p1

    if-lt v0, v1, :cond_12

    .line 1411
    const/16 v23, 0x1

    goto/16 :goto_4

    .line 1461
    .restart local v8    # "indicatorPos":I
    .restart local v10    # "mwRect":Landroid/graphics/Rect;
    .restart local v11    # "outSize":Landroid/graphics/Point;
    .restart local v20    # "screenHeight":I
    .restart local v21    # "screenWidth":I
    :pswitch_4
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v25, v25, p1

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    .line 1462
    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v26, v0

    add-int v26, v26, v6

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_1b

    .line 1463
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    goto/16 :goto_3

    .line 1466
    :cond_1b
    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    add-int v25, v25, p1

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    sub-int v26, v26, v6

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_b

    .line 1467
    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    sub-int v25, v25, v6

    sub-int v25, v25, p1

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    goto/16 :goto_3

    .line 1471
    :pswitch_5
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    .line 1472
    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v25, v0

    add-int v25, v25, p1

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    sub-int v26, v26, v6

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_b

    .line 1473
    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    sub-int v25, v25, v6

    sub-int v25, v25, p1

    move/from16 v0, v25

    iput v0, v13, Landroid/graphics/Point;->x:I

    goto/16 :goto_3

    .line 1353
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1451
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getShowPoint(IIII)Landroid/graphics/Rect;
    .locals 32
    .param p1, "popup_w"    # I
    .param p2, "popup_h"    # I
    .param p3, "more_w"    # I
    .param p4, "more_h"    # I

    .prologue
    .line 1482
    new-instance v15, Landroid/graphics/Rect;

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    move/from16 v0, v28

    move/from16 v1, v29

    move/from16 v2, v30

    move/from16 v3, v31

    invoke-direct {v15, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1486
    .local v15, "rc":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v28, v0

    const/high16 v29, 0x40200000    # 2.5f

    invoke-static/range {v28 .. v29}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    .line 1487
    .local v7, "dip25":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v28, v0

    const/high16 v29, 0x40c00000    # 6.0f

    invoke-static/range {v28 .. v29}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v8

    .line 1489
    .local v8, "dip6":I
    new-instance v14, Landroid/graphics/Point;

    invoke-direct {v14}, Landroid/graphics/Point;-><init>()V

    .line 1490
    .local v14, "outSize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v14}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1491
    iget v0, v14, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    .line 1492
    .local v23, "screenWidth":I
    iget v0, v14, Landroid/graphics/Point;->y:I

    move/from16 v22, v0

    .line 1494
    .local v22, "screenHeight":I
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 1495
    .local v13, "outRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1496
    iget v10, v13, Landroid/graphics/Rect;->top:I

    .line 1498
    .local v10, "indicatorPos":I
    sub-int v28, v22, v10

    mul-int/lit8 v29, v8, 0x2

    sub-int v28, v28, v29

    move/from16 v0, p4

    move/from16 v1, v28

    if-le v0, v1, :cond_0

    .line 1500
    sub-int v28, v22, v10

    mul-int/lit8 v29, v8, 0x2

    sub-int p4, v28, v29

    .line 1501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setHeight(I)V

    .line 1505
    :cond_0
    sub-int v28, v23, p1

    sub-int v28, v28, v7

    sub-int v28, v28, p3

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1506
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, p1

    add-int v28, v28, v7

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1507
    add-int v28, v22, v10

    sub-int v28, v28, p2

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1508
    add-int v28, v22, v10

    sub-int v28, v28, p4

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    .line 1511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    if-gez v28, :cond_1

    .line 1512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1514
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v0, v10, :cond_2

    .line 1515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iput v10, v0, Landroid/graphics/Rect;->top:I

    .line 1517
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    move/from16 v0, v28

    move/from16 v1, v23

    if-le v0, v1, :cond_3

    .line 1518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move/from16 v0, v23

    move-object/from16 v1, v28

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1520
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    move/from16 v0, v28

    move/from16 v1, v22

    if-le v0, v1, :cond_4

    .line 1521
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move/from16 v0, v22

    move-object/from16 v1, v28

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1523
    :cond_4
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 1524
    .local v19, "rect_top":Landroid/graphics/Rect;
    new-instance v21, Landroid/graphics/Rect;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Rect;-><init>()V

    .line 1525
    .local v21, "rect_y_center":Landroid/graphics/Rect;
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 1526
    .local v16, "rect_bottom":Landroid/graphics/Rect;
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 1527
    .local v17, "rect_left":Landroid/graphics/Rect;
    new-instance v20, Landroid/graphics/Rect;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Rect;-><init>()V

    .line 1528
    .local v20, "rect_x_center":Landroid/graphics/Rect;
    new-instance v18, Landroid/graphics/Rect;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Rect;-><init>()V

    .line 1530
    .local v18, "rect_right":Landroid/graphics/Rect;
    const/4 v5, 0x0

    .line 1532
    .local v5, "bOk":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v28

    const/16 v29, 0x3

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_e

    .line 1534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getTextMarkInfo_for_popup()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-result-object v25

    .line 1535
    .local v25, "text_mark":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->startImageRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->height()I

    move-result v9

    .line 1537
    .local v9, "icon_height":I
    add-int v28, v10, v8

    sub-int v29, v23, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v30, v0

    sub-int v30, v30, v9

    sub-int v30, v30, v8

    move-object/from16 v0, v19

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    add-int v28, v28, v8

    sub-int v29, v23, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v30, v0

    sub-int v30, v30, v8

    move-object/from16 v0, v21

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    add-int v28, v28, v9

    add-int v28, v28, v8

    sub-int v29, v23, v8

    sub-int v30, v22, v8

    move-object/from16 v0, v16

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1542
    add-int v28, v10, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    sub-int v29, v29, v8

    sub-int v30, v22, v8

    move-object/from16 v0, v17

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1543
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, v8

    add-int v29, v10, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v30, v0

    sub-int v30, v30, v8

    sub-int v31, v22, v8

    move-object/from16 v0, v20

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1544
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    add-int v28, v28, v8

    add-int v29, v10, v8

    sub-int v30, v23, v8

    sub-int v31, v22, v8

    move-object/from16 v0, v18

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1574
    .end local v9    # "icon_height":I
    .end local v25    # "text_mark":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    :goto_0
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_5

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1575
    :cond_5
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_6

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v29, v0

    add-int v28, v28, v29

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Landroid/graphics/Rect;->top:I

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1576
    :cond_6
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_7

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1578
    :cond_7
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_8

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1579
    :cond_8
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_9

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v29, v0

    add-int v28, v28, v29

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->left:I

    move/from16 v0, v28

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1580
    :cond_9
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_a

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1583
    :cond_a
    add-int v28, p2, v7

    add-int v24, v28, p4

    .line 1585
    .local v24, "t_height":I
    const/16 v27, 0x0

    .line 1586
    .local v27, "ymax_type":I
    const/4 v11, 0x0

    .line 1588
    .local v11, "max_height":I
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v28

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_11

    .line 1590
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v28

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_10

    .line 1592
    const/16 v27, 0x0

    .line 1593
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v11

    .line 1615
    :goto_1
    move/from16 v0, v24

    if-gt v0, v11, :cond_14

    .line 1618
    packed-switch v27, :pswitch_data_0

    .line 1634
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v29, v0

    add-int v28, v28, v29

    div-int/lit8 v6, v28, 0x2

    .line 1635
    .local v6, "center":I
    div-int/lit8 v28, p1, 0x2

    sub-int v28, v6, v28

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1637
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v0, v8, :cond_b

    .line 1638
    iput v8, v15, Landroid/graphics/Rect;->left:I

    .line 1640
    :cond_b
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, p1

    sub-int v29, v23, v8

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_c

    .line 1641
    sub-int v28, v23, v8

    sub-int v28, v28, p1

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1642
    :cond_c
    div-int/lit8 v28, v23, 0x2

    move/from16 v0, v28

    if-ge v6, v0, :cond_13

    .line 1643
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1843
    .end local v6    # "center":I
    :cond_d
    :goto_3
    return-object v15

    .line 1547
    .end local v11    # "max_height":I
    .end local v24    # "t_height":I
    .end local v27    # "ymax_type":I
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_f

    .line 1551
    move v9, v8

    .line 1553
    .restart local v9    # "icon_height":I
    add-int v28, v10, v8

    sub-int v29, v23, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v30, v0

    sub-int v30, v30, v8

    move-object/from16 v0, v19

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1554
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    add-int v28, v28, v8

    sub-int v29, v23, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v30, v0

    sub-int v30, v30, v8

    move-object/from16 v0, v21

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    add-int v28, v28, v9

    add-int v28, v28, v8

    sub-int v29, v23, v8

    sub-int v30, v22, v8

    move-object/from16 v0, v16

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1557
    add-int v28, v10, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    sub-int v29, v29, v8

    sub-int v30, v22, v8

    move-object/from16 v0, v17

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1558
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, v8

    add-int v29, v10, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v30, v0

    sub-int v30, v30, v8

    sub-int v31, v22, v8

    move-object/from16 v0, v20

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    add-int v28, v28, v8

    add-int v29, v10, v8

    sub-int v30, v23, v8

    sub-int v31, v22, v8

    move-object/from16 v0, v18

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 1565
    .end local v9    # "icon_height":I
    :cond_f
    add-int v28, v10, v8

    sub-int v29, v23, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v30, v0

    mul-int/lit8 v31, v8, 0x2

    sub-int v30, v30, v31

    move-object/from16 v0, v19

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    mul-int/lit8 v29, v8, 0x2

    add-int v28, v28, v29

    sub-int v29, v23, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v30, v0

    mul-int/lit8 v31, v8, 0x2

    sub-int v30, v30, v31

    move-object/from16 v0, v21

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1567
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    mul-int/lit8 v29, v8, 0x2

    add-int v28, v28, v29

    sub-int v29, v23, v8

    sub-int v30, v22, v8

    move-object/from16 v0, v16

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1569
    add-int v28, v10, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    mul-int/lit8 v30, v8, 0x2

    sub-int v29, v29, v30

    sub-int v30, v22, v8

    move-object/from16 v0, v17

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    mul-int/lit8 v29, v8, 0x2

    add-int v28, v28, v29

    add-int v29, v10, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v30, v0

    mul-int/lit8 v31, v8, 0x2

    sub-int v30, v30, v31

    sub-int v31, v22, v8

    move-object/from16 v0, v20

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    mul-int/lit8 v29, v8, 0x2

    add-int v28, v28, v29

    add-int v29, v10, v8

    sub-int v30, v23, v8

    sub-int v31, v22, v8

    move-object/from16 v0, v18

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 1597
    .restart local v11    # "max_height":I
    .restart local v24    # "t_height":I
    .restart local v27    # "ymax_type":I
    :cond_10
    const/16 v27, 0x2

    .line 1598
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v11

    goto/16 :goto_1

    .line 1603
    :cond_11
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v28

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_12

    .line 1605
    const/16 v27, 0x2

    .line 1606
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v11

    goto/16 :goto_1

    .line 1610
    :cond_12
    const/16 v27, 0x1

    .line 1611
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v11

    goto/16 :goto_1

    .line 1621
    :pswitch_0
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, p2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1622
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, v24

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_2

    .line 1625
    :pswitch_1
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v29, v0

    add-int v28, v28, v29

    sub-int v28, v28, v24

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1626
    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    add-int v28, v28, p2

    add-int v28, v28, v7

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_2

    .line 1629
    :pswitch_2
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1630
    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    add-int v28, v28, p2

    add-int v28, v28, v7

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_2

    .line 1645
    .restart local v6    # "center":I
    :cond_13
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, p1

    sub-int v28, v28, p3

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    goto/16 :goto_3

    .line 1652
    .end local v6    # "center":I
    :cond_14
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v28

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_17

    .line 1654
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v28

    move/from16 v0, v28

    move/from16 v1, p3

    if-lt v0, v1, :cond_1a

    mul-int/lit8 v28, v8, 0x2

    sub-int v28, v23, v28

    add-int v29, p3, p1

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_1a

    .line 1657
    packed-switch v27, :pswitch_data_1

    .line 1674
    :goto_4
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    sub-int v28, v28, p3

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1675
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    add-int v28, v28, v7

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1677
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, p1

    sub-int v29, v23, v8

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_15

    .line 1679
    sub-int v28, v23, v8

    sub-int v28, v28, p1

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1680
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    sub-int v28, v28, v7

    sub-int v28, v28, p3

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1683
    :cond_15
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    add-int v29, v8, v10

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_16

    .line 1684
    add-int v28, v8, v10

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    .line 1685
    :cond_16
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    add-int v28, v28, p4

    sub-int v29, v22, v8

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_d

    .line 1686
    sub-int v28, v22, v8

    sub-int v28, v28, p4

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_3

    .line 1660
    :pswitch_3
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, p2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1661
    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto :goto_4

    .line 1665
    :pswitch_4
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v29, v0

    add-int v28, v28, v29

    sub-int v28, v28, p2

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1666
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v29, v0

    add-int v28, v28, v29

    sub-int v28, v28, p4

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_4

    .line 1669
    :pswitch_5
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1670
    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    add-int v28, v28, p2

    sub-int v28, v28, p4

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_4

    .line 1693
    :cond_17
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v28

    move/from16 v0, v28

    move/from16 v1, p3

    if-lt v0, v1, :cond_1a

    mul-int/lit8 v28, v8, 0x2

    sub-int v28, v23, v28

    add-int v29, p3, p1

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_1a

    .line 1696
    packed-switch v27, :pswitch_data_2

    .line 1713
    :goto_5
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1714
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    sub-int v28, v28, v7

    sub-int v28, v28, p1

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1716
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v0, v8, :cond_18

    .line 1718
    iput v8, v15, Landroid/graphics/Rect;->left:I

    .line 1719
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, p1

    add-int v28, v28, v7

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1722
    :cond_18
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    add-int v29, v8, v10

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_19

    .line 1723
    add-int v28, v8, v10

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    .line 1724
    :cond_19
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    add-int v28, v28, p4

    sub-int v29, v22, v8

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_d

    .line 1725
    sub-int v28, v22, v8

    sub-int v28, v28, p4

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_3

    .line 1699
    :pswitch_6
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v28, v28, p2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1700
    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto :goto_5

    .line 1704
    :pswitch_7
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v29, v0

    add-int v28, v28, v29

    sub-int v28, v28, p2

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1705
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v29, v0

    add-int v28, v28, v29

    sub-int v28, v28, p4

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_5

    .line 1708
    :pswitch_8
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1709
    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    add-int v28, v28, p2

    sub-int v28, v28, p4

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_5

    .line 1731
    :cond_1a
    const/16 v26, 0x0

    .line 1732
    .local v26, "xxx_type":I
    const/4 v12, 0x0

    .line 1734
    .local v12, "max_width":I
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v28

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_1d

    .line 1736
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v28

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_1c

    .line 1738
    const/16 v26, 0x0

    .line 1739
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v12

    .line 1764
    :goto_6
    sub-int v28, v22, v10

    mul-int/lit8 v29, v8, 0x2

    sub-int v28, v28, v29

    move/from16 v0, v24

    move/from16 v1, v28

    if-le v0, v1, :cond_1b

    .line 1766
    sub-int v28, v22, v10

    mul-int/lit8 v29, v8, 0x2

    sub-int v28, v28, v29

    sub-int v28, v28, p2

    sub-int p4, v28, v7

    .line 1767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setHeight(I)V

    .line 1769
    add-int v28, p2, v7

    add-int v24, v28, p4

    .line 1772
    :cond_1b
    add-int v28, v22, v10

    sub-int v28, v28, v24

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    .line 1773
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    add-int v28, v28, p4

    add-int v28, v28, v7

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 1775
    packed-switch v26, :pswitch_data_3

    goto/16 :goto_3

    .line 1778
    :pswitch_9
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    sub-int v28, v28, p1

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1779
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    sub-int v28, v28, p3

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1780
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_1f

    .line 1782
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v0, v8, :cond_d

    .line 1784
    iput v8, v15, Landroid/graphics/Rect;->left:I

    .line 1785
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, p1

    sub-int v28, v28, p3

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    goto/16 :goto_3

    .line 1743
    :cond_1c
    const/16 v26, 0x2

    .line 1744
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v12

    goto/16 :goto_6

    .line 1749
    :cond_1d
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v28

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_1e

    .line 1751
    const/16 v26, 0x2

    .line 1752
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v12

    goto/16 :goto_6

    .line 1756
    :cond_1e
    const/16 v26, 0x1

    .line 1757
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v12

    goto/16 :goto_6

    .line 1790
    :cond_1f
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v0, v8, :cond_d

    .line 1792
    iput v8, v15, Landroid/graphics/Rect;->right:I

    .line 1793
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    add-int v28, v28, p3

    sub-int v28, v28, p1

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    goto/16 :goto_3

    .line 1798
    :pswitch_a
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v29, v0

    add-int v28, v28, v29

    sub-int v28, v28, p1

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1799
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v29, v0

    add-int v28, v28, v29

    sub-int v28, v28, p3

    div-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1800
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_21

    .line 1802
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    div-int/lit8 v29, p1, 0x2

    add-int v28, v28, v29

    div-int/lit8 v29, v23, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_20

    .line 1803
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    goto/16 :goto_3

    .line 1805
    :cond_20
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, p1

    sub-int v28, v28, p3

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    goto/16 :goto_3

    .line 1809
    :cond_21
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    div-int/lit8 v29, p1, 0x2

    add-int v28, v28, v29

    div-int/lit8 v29, v23, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_22

    .line 1810
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    goto/16 :goto_3

    .line 1812
    :cond_22
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    add-int v28, v28, p3

    sub-int v28, v28, p1

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    goto/16 :goto_3

    .line 1818
    :pswitch_b
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1819
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1820
    move/from16 v0, p1

    move/from16 v1, p3

    if-le v0, v1, :cond_23

    .line 1822
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    add-int v28, v28, p1

    sub-int v29, v23, v8

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_d

    .line 1824
    sub-int v28, v23, v8

    sub-int v28, v28, p1

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 1825
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    goto/16 :goto_3

    .line 1830
    :cond_23
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    add-int v28, v28, p3

    sub-int v29, v23, v8

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_d

    .line 1832
    sub-int v28, v23, v8

    sub-int v28, v28, p3

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 1833
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v28, v0

    move/from16 v0, v28

    iput v0, v15, Landroid/graphics/Rect;->left:I

    goto/16 :goto_3

    .line 1618
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1657
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1696
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1775
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private getString(I)Ljava/lang/String;
    .locals 3
    .param p1, "cmd_id"    # I

    .prologue
    .line 2215
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2216
    .local v0, "r":Landroid/content/res/Resources;
    const-string/jumbo v1, ""

    .line 2218
    .local v1, "str":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 2276
    :goto_0
    :sswitch_0
    return-object v1

    .line 2220
    :sswitch_1
    const v2, 0x7f0702a9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2221
    goto :goto_0

    .line 2226
    :sswitch_2
    const v2, 0x7f0702c4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2227
    goto :goto_0

    .line 2260
    :sswitch_3
    const v2, 0x7f07010f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2261
    goto :goto_0

    .line 2266
    :sswitch_4
    const v2, 0x7f0701ec

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2267
    goto :goto_0

    .line 2218
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_2
        0xc -> :sswitch_0
        0xe -> :sswitch_3
        0x10 -> :sswitch_4
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method private isExistCmd(I)Z
    .locals 2
    .param p1, "cmd"    # I

    .prologue
    .line 806
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    iget v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    if-ge v0, v1, :cond_1

    .line 807
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 808
    const/4 v1, 0x1

    .line 811
    :goto_1
    return v1

    .line 806
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 811
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private setRect(IIII)Z
    .locals 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1856
    const/4 v2, 0x0

    .line 1857
    .local v2, "ret":Z
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1858
    .local v1, "r":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1859
    .local v0, "p":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    invoke-virtual {v3, v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1860
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v4, p1

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 1861
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v4, p2

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 1862
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v4, p3

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 1863
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v4, p4

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 1865
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1867
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, v1, Landroid/graphics/Rect;->left:I

    if-le v3, v4, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, v1, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    if-gt v3, v4, :cond_1

    .line 1876
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 1877
    const/4 v0, 0x0

    .line 1879
    return v2

    .line 1872
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected Excute(I)V
    .locals 13
    .param p1, "cmd_id"    # I

    .prologue
    const/16 v1, 0x2e

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1914
    sparse-switch p1, :sswitch_data_0

    .line 2066
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismiss()V

    .line 2067
    :goto_1
    return-void

    .line 1916
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v2, v1}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 1918
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    if-ne v0, v4, :cond_0

    .line 1920
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v7, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 1921
    .local v7, "SheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual {v7, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->setValueCopied(Z)V

    .line 1922
    invoke-virtual {v7, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->setFormatCopied(Z)V

    goto :goto_0

    .line 1938
    .end local v7    # "SheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDictionaryPanelMain()Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDictionaryPanelMain()Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1939
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDictionaryPanelMain()Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 1940
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    if-lez v0, :cond_0

    .line 1942
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getWidth()I

    move-result v11

    .line 1943
    .local v11, "more_width":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getHeight()I

    move-result v10

    .line 1944
    .local v10, "more_height":I
    invoke-direct {p0, v11, v10}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->getShowMorePoint(II)Landroid/graphics/Point;

    move-result-object v12

    .line 1945
    .local v12, "pt_more":Landroid/graphics/Point;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    iget v3, v12, Landroid/graphics/Point;->x:I

    iget v4, v12, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0

    .line 1950
    .end local v10    # "more_height":I
    .end local v11    # "more_width":I
    .end local v12    # "pt_more":Landroid/graphics/Point;
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2, v1, v3}, Lcom/infraware/office/evengine/EvInterface;->ICharInsert(III)V

    goto :goto_0

    .line 2007
    :sswitch_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismiss()V

    .line 2009
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 2011
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->selectAll()V

    goto :goto_1

    .line 2015
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISelectAll()V

    .line 2016
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->show()V

    goto :goto_1

    .line 2022
    :sswitch_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v1, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 2026
    :sswitch_5
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    if-ne v0, v4, :cond_3

    .line 2029
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v6

    .line 2030
    .local v6, "sheetHyperlinkInfo":Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    if-eqz v6, :cond_0

    iget v0, v6, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    if-eqz v0, :cond_0

    .line 2032
    iget v0, v6, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_0

    .line 2037
    :sswitch_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v6, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runHyperlink(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2034
    :sswitch_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v6, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runEmail(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2041
    :sswitch_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v6, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runSMS(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2044
    :sswitch_9
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 2051
    .end local v6    # "sheetHyperlinkInfo":Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v9

    .line 2052
    .local v9, "hyperlinkInfo":Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;
    if-eqz v9, :cond_0

    iget v0, v9, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    if-ne v0, v3, :cond_0

    .line 2055
    iget-object v8, v9, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    .line 2056
    .local v8, "hyperLink":Ljava/lang/String;
    const-string/jumbo v0, "HTTP"

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "Http"

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2058
    :cond_4
    const-string/jumbo v0, "H(TTP|ttp)"

    const-string/jumbo v1, "http"

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2060
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0, v8}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runHyperlink(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1914
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x5 -> :sswitch_1
        0x10 -> :sswitch_3
        0x11 -> :sswitch_4
        0x12 -> :sswitch_5
    .end sparse-switch

    .line 2032
    :sswitch_data_1
    .sparse-switch
        0x3 -> :sswitch_6
        0xf -> :sswitch_7
        0x1f -> :sswitch_8
        0x20 -> :sswitch_9
    .end sparse-switch
.end method

.method public InitItem()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1905
    iput v4, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    .line 1906
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mSelectRect:Landroid/graphics/Rect;

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    iput v4, v2, Landroid/graphics/Rect;->top:I

    iput v4, v1, Landroid/graphics/Rect;->right:I

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 1907
    iput v4, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    .line 1908
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1909
    iput v4, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPdfAnnotType:I

    .line 1910
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsShowMoreWindow:Z

    .line 1911
    return-void
.end method

.method public dismiss()V
    .locals 2

    .prologue
    .line 2119
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2122
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2129
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->InitItem()V

    .line 2131
    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 2137
    return-void
.end method

.method public dismissAll()V
    .locals 0

    .prologue
    .line 2150
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismiss()V

    .line 2151
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissMorePopup()V

    .line 2152
    return-void
.end method

.method public dismissMorePopup()V
    .locals 1

    .prologue
    .line 2141
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    if-eqz v0, :cond_0

    .line 2143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2144
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->dismiss()V

    .line 2146
    :cond_0
    return-void
.end method

.method public doAction()V
    .locals 33

    .prologue
    .line 835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v28

    if-eqz v28, :cond_0

    .line 837
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v28, v0

    const/16 v29, 0x5

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_0

    .line 838
    const/16 v28, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->deleteButton(I)V

    .line 843
    :cond_0
    const/4 v14, 0x0

    .line 844
    .local v14, "more_width":I
    const/4 v13, 0x0

    .line 846
    .local v13, "more_height":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsShowMoreWindow:Z

    move/from16 v28, v0

    if-nez v28, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPdfAnnotType:I

    move/from16 v28, v0

    if-eqz v28, :cond_2

    .line 848
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPdfAnnotType:I

    move/from16 v30, v0

    invoke-virtual/range {v28 .. v30}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->preProcess(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;I)V

    .line 850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getWidth()I

    move-result v14

    .line 851
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getHeight()I

    move-result v13

    .line 854
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    move/from16 v28, v0

    if-nez v28, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    move/from16 v28, v0

    if-nez v28, :cond_4

    .line 1044
    :cond_3
    :goto_0
    return-void

    .line 861
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    move/from16 v28, v0

    if-eqz v28, :cond_5

    .line 866
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    move/from16 v28, v0

    if-lez v28, :cond_5

    .line 867
    const/16 v28, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 875
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 877
    .local v5, "context":Landroid/content/Context;
    const/high16 v28, 0x3f800000    # 1.0f

    move/from16 v0, v28

    invoke-static {v5, v0}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v6

    .line 878
    .local v6, "dip_len":F
    const/high16 v28, 0x3f800000    # 1.0f

    move/from16 v0, v28

    invoke-static {v5, v0}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v28

    move/from16 v0, v28

    int-to-float v7, v0

    .line 881
    .local v7, "dip_len2":F
    const/4 v8, 0x0

    .line 883
    .local v8, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->width()I

    move-result v23

    .line 884
    .local v23, "pw":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->height()I

    move-result v18

    .line 885
    .local v18, "ph":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    .line 886
    .local v17, "pTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    .line 888
    .local v16, "pBottom":I
    if-nez v23, :cond_6

    if-eqz v18, :cond_3

    .line 892
    :cond_6
    const/high16 v28, 0x3f000000    # 0.5f

    mul-float v28, v28, v7

    move/from16 v0, v28

    float-to-int v10, v0

    .line 894
    .local v10, "line_width":I
    new-instance v15, Lcom/infraware/polarisoffice5/common/PopupMenuWindow$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow$3;-><init>(Lcom/infraware/polarisoffice5/common/PopupMenuWindow;)V

    .line 905
    .local v15, "ocl":Landroid/view/View$OnClickListener;
    const/4 v8, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v8, v0, :cond_9

    .line 907
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    move-object/from16 v28, v0

    aget v28, v28, v8

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->getResId(I)I

    move-result v24

    .line 909
    .local v24, "res_id":I
    if-lez v8, :cond_7

    .line 910
    new-instance v27, Landroid/view/View;

    move-object/from16 v0, v27

    invoke-direct {v0, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 911
    .local v27, "sepiv":Landroid/view/View;
    new-instance v28, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v29, -0x1

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-direct {v0, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 912
    const v28, -0xff9e8d

    invoke-virtual/range {v27 .. v28}, Landroid/view/View;->setBackgroundColor(I)V

    .line 916
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 919
    .end local v27    # "sepiv":Landroid/view/View;
    :cond_7
    new-instance v9, Landroid/widget/ImageView;

    invoke-direct {v9, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 922
    .local v9, "iv":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    move-object/from16 v28, v0

    aget v28, v28, v8

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setId(I)V

    .line 923
    move/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 924
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    move-object/from16 v28, v0

    aget v28, v28, v8

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->getString(I)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 927
    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 929
    const v28, 0x7f020010

    move/from16 v0, v28

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 943
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    move-object/from16 v28, v0

    aget v28, v28, v8

    const/16 v29, 0x5

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_8

    .line 944
    new-instance v28, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v29, 0x423c0000    # 47.0f

    mul-float v29, v29, v6

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v29, v0

    const/high16 v30, 0x42180000    # 38.0f

    mul-float v30, v30, v6

    move/from16 v0, v30

    float-to-int v0, v0

    move/from16 v30, v0

    invoke-direct/range {v28 .. v30}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 952
    :goto_2
    invoke-virtual {v9, v15}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 954
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 905
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 946
    :cond_8
    new-instance v28, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v29, 0x425c0000    # 55.0f

    mul-float v29, v29, v6

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v29, v0

    const/high16 v30, 0x42180000    # 38.0f

    mul-float v30, v30, v6

    move/from16 v0, v30

    float-to-int v0, v0

    move/from16 v30, v0

    invoke-direct/range {v28 .. v30}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 957
    .end local v9    # "iv":Landroid/widget/ImageView;
    .end local v24    # "res_id":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Landroid/widget/LinearLayout;->measure(II)V

    .line 959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v12

    .line 960
    .local v12, "linear_width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v11

    .line 962
    .local v11, "linear_height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/HorizontalScrollView;->getPaddingLeft()I

    move-result v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/widget/HorizontalScrollView;->getPaddingRight()I

    move-result v29

    add-int v26, v28, v29

    .line 963
    .local v26, "s_padding_width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/HorizontalScrollView;->getPaddingTop()I

    move-result v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/widget/HorizontalScrollView;->getPaddingBottom()I

    move-result v29

    add-int v25, v28, v29

    .line 974
    .local v25, "s_padding_height":I
    add-int v20, v12, v26

    .line 975
    .local v20, "popup_width":I
    add-int v19, v11, v25

    .line 976
    .local v19, "popup_height":I
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setWidth(I)V

    .line 977
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setHeight(I)V

    .line 994
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    move/from16 v28, v0

    if-lez v28, :cond_b

    .line 996
    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->getShowMenuPoint(II)Landroid/graphics/Point;

    move-result-object v21

    .line 997
    .local v21, "pt_menu":Landroid/graphics/Point;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v28

    if-eqz v28, :cond_a

    .line 999
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v29, v0

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->getWidth()I

    move-result v30

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->getHeight()I

    move-result v31

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->update(IIII)V

    goto/16 :goto_0

    .line 1002
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v30, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->showAtLocation(Landroid/view/View;III)V

    goto/16 :goto_0

    .line 1006
    .end local v21    # "pt_menu":Landroid/graphics/Point;
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->getShowMorePoint(II)Landroid/graphics/Point;

    move-result-object v22

    .line 1007
    .local v22, "pt_more":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isShowing()Z

    move-result v28

    if-eqz v28, :cond_c

    .line 1009
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v29, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getWidth()I

    move-result v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getHeight()I

    move-result v32

    invoke-virtual/range {v28 .. v32}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->update(IIII)V

    goto/16 :goto_0

    .line 1011
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v31, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v32, v0

    invoke-virtual/range {v28 .. v32}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->showAtLocation(Landroid/view/View;III)V

    goto/16 :goto_0
.end method

.method public finalizePopupMenuWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 235
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 237
    :cond_0
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    .line 239
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mHandler:Landroid/os/Handler;

    .line 241
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->finalizePopupMoreWindow()V

    .line 242
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    .line 244
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->removeAllViews()V

    .line 245
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 247
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 248
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 249
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 250
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .line 252
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    .line 253
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 254
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 255
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mRootFrame:Landroid/widget/FrameLayout;

    .line 256
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentRect:Landroid/graphics/Rect;

    .line 257
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mCmdArray:[I

    .line 258
    return-void
.end method

.method public getPopupMore()Lcom/infraware/polarisoffice5/common/PopupMoreWindow;
    .locals 1

    .prologue
    .line 2302
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 2296
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297
    const/4 v0, 0x1

    .line 2298
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public isTouchUpShowFlag()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mTouchUpShowFlag:Z

    return v0
.end method

.method public recievePdfAnnot(IIIIZ)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .param p5, "bUsePropertiesMenu"    # Z

    .prologue
    .line 2072
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 2073
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->InitItem()V

    .line 2075
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPdfAnnotType:I

    .line 2079
    if-eqz p5, :cond_0

    .line 2081
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPdfAnnotType:I

    .line 2085
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    .line 2087
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    .line 2088
    return-void
.end method

.method public recievePdfSelectAnnots(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 2106
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 2107
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->InitItem()V

    .line 2108
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 2110
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPdfAnnotType:I

    .line 2111
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    .line 2112
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    .line 2113
    return-void
.end method

.method public recievePdfSingleTap(IIZ)V
    .locals 1
    .param p1, "posX"    # I
    .param p2, "posY"    # I
    .param p3, "bVisibleSticky"    # Z

    .prologue
    .line 2092
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 2093
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->InitItem()V

    .line 2095
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 2096
    if-eqz p3, :cond_0

    .line 2097
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 2099
    :cond_0
    invoke-direct {p0, p1, p2, p1, p2}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    .line 2101
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    .line 2102
    return-void
.end method

.method public setTouchUpShowFlag(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mTouchUpShowFlag:Z

    .line 126
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x32

    .line 275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mTouchUpShowFlag:Z

    .line 277
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    if-nez v0, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 292
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_3

    .line 294
    new-instance v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow$2;-><init>(Lcom/infraware/polarisoffice5/common/PopupMenuWindow;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    .line 306
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 284
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismiss()V

    goto :goto_1

    .line 310
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 314
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mWaitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public showAsDropDown(Landroid/view/View;II)V
    .locals 0
    .param p1, "anchor"    # Landroid/view/View;
    .param p2, "xoff"    # I
    .param p3, "yoff"    # I

    .prologue
    .line 270
    invoke-super {p0, p1, p2, p3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 271
    return-void
.end method

.method public showFromRunnable()V
    .locals 41

    .prologue
    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v36

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_2

    .line 323
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v36, v0

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v36, v0

    const/16 v37, 0x3

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v36, v0

    const/16 v37, 0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_2

    .line 327
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsEditInTextboxMode()Z

    move-result v36

    if-eqz v36, :cond_2

    .line 329
    const/16 v36, 0x0

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsMemoAndHyperlink:Z

    .line 803
    :cond_1
    :goto_0
    return-void

    .line 335
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->InitItem()V

    .line 337
    const/16 v36, 0x1

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsShowMoreWindow:Z

    .line 339
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsMemoAndHyperlink:Z

    move/from16 v36, v0

    if-eqz v36, :cond_f

    .line 341
    const/16 v36, 0x0

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsShowMoreWindow:Z

    .line 342
    const/16 v36, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 343
    const/16 v36, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 344
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v36, v0

    const/16 v37, 0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_4

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 347
    .local v19, "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 348
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    .line 472
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    :cond_3
    :goto_1
    const/16 v36, 0x0

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsMemoAndHyperlink:Z

    goto/16 :goto_0

    .line 350
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v36, v0

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_3

    .line 354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v12

    .line 356
    .local v12, "mBWPInfo":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    iget v6, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nCaretMode:I

    .line 358
    .local v6, "caret_mode":I
    const/16 v36, 0x1

    move/from16 v0, v36

    if-ne v6, v0, :cond_5

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v8

    .line 362
    .local v8, "ci":Lcom/infraware/office/evengine/EV$CARET_INFO;
    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nX:I

    move/from16 v36, v0

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nY:I

    move/from16 v37, v0

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nX:I

    move/from16 v38, v0

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nWidth:I

    move/from16 v39, v0

    add-int v38, v38, v39

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nY:I

    move/from16 v39, v0

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nHeight:I

    move/from16 v40, v0

    add-int v39, v39, v40

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 363
    const/16 v36, 0x1

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    .line 364
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto :goto_1

    .line 367
    .end local v8    # "ci":Lcom/infraware/office/evengine/EV$CARET_INFO;
    :cond_5
    const/16 v36, 0x2

    move/from16 v0, v36

    if-eq v6, v0, :cond_6

    const/16 v36, 0x3

    move/from16 v0, v36

    if-ne v6, v0, :cond_7

    .line 370
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 371
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 372
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_1

    .line 374
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    :cond_7
    const/16 v36, 0x5

    move/from16 v0, v36

    if-ne v6, v0, :cond_8

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 378
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 379
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_1

    .line 381
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_9

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 385
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 386
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_1

    .line 388
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    :cond_9
    const/16 v36, 0x7

    move/from16 v0, v36

    if-ne v6, v0, :cond_3

    .line 391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 392
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    new-instance v29, Landroid/graphics/Rect;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/Rect;-><init>()V

    .line 393
    .local v29, "rect1":Landroid/graphics/Rect;
    move-object/from16 v0, v19

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    move/from16 v36, v0

    const/16 v37, 0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_c

    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-result-object v30

    .line 397
    .local v30, "rect_info":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    if-eqz v36, :cond_b

    .line 399
    new-instance v7, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v37, v0

    add-int v36, v36, v37

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    const/high16 v37, 0x40000000    # 2.0f

    div-float v36, v36, v37

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v38, v0

    add-int v37, v37, v38

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    const/high16 v38, 0x40000000    # 2.0f

    div-float v37, v37, v38

    move/from16 v0, v36

    move/from16 v1, v37

    invoke-direct {v7, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 401
    .local v7, "center":Landroid/graphics/PointF;
    new-instance v21, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 402
    .local v21, "p0":Landroid/graphics/PointF;
    new-instance v22, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    move-object/from16 v0, v22

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 403
    .local v22, "p1":Landroid/graphics/PointF;
    new-instance v23, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 404
    .local v23, "p2":Landroid/graphics/PointF;
    new-instance v24, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    move-object/from16 v0, v24

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 407
    .local v24, "p3":Landroid/graphics/PointF;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    move-object/from16 v0, v21

    move/from16 v1, v36

    invoke-static {v0, v7, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v25

    .line 408
    .local v25, "r0":Landroid/graphics/PointF;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    move-object/from16 v0, v22

    move/from16 v1, v36

    invoke-static {v0, v7, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v26

    .line 409
    .local v26, "r1":Landroid/graphics/PointF;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    move-object/from16 v0, v23

    move/from16 v1, v36

    invoke-static {v0, v7, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v27

    .line 410
    .local v27, "r2":Landroid/graphics/PointF;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    move-object/from16 v0, v24

    move/from16 v1, v36

    invoke-static {v0, v7, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v28

    .line 413
    .local v28, "r3":Landroid/graphics/PointF;
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v36, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v36

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v37, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v38, v0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->max(FF)F

    move-result v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v36

    move/from16 v0, v36

    float-to-int v14, v0

    .line 414
    .local v14, "max_x":I
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v36, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v36

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v37, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v38, v0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->min(FF)F

    move-result v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v36

    move/from16 v0, v36

    float-to-int v0, v0

    move/from16 v16, v0

    .line 415
    .local v16, "min_x":I
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v36, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v36

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v38, v0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->max(FF)F

    move-result v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v36

    move/from16 v0, v36

    float-to-int v15, v0

    .line 416
    .local v15, "max_y":I
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v36, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v36

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v38, v0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->min(FF)F

    move-result v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v36

    move/from16 v0, v36

    float-to-int v0, v0

    move/from16 v17, v0

    .line 427
    .local v17, "min_y":I
    const/16 v28, 0x0

    move-object/from16 v27, v28

    .local v27, "r2":Ljava/lang/Object;
    move-object/from16 v26, v28

    .local v26, "r1":Ljava/lang/Object;
    move-object/from16 v25, v28

    .local v25, "r0":Ljava/lang/Object;
    move-object/from16 v24, v28

    .local v24, "p3":Ljava/lang/Object;
    move-object/from16 v23, v28

    .local v23, "p2":Ljava/lang/Object;
    move-object/from16 v22, v28

    .local v22, "p1":Ljava/lang/Object;
    move-object/from16 v21, v28

    .local v21, "p0":Ljava/lang/Object;
    move-object/from16 v7, v28

    .line 429
    .local v7, "center":Ljava/lang/Object;
    move-object/from16 v0, v29

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2, v14, v15}, Landroid/graphics/Rect;->set(IIII)V

    .line 446
    .end local v7    # "center":Ljava/lang/Object;
    .end local v14    # "max_x":I
    .end local v15    # "max_y":I
    .end local v16    # "min_x":I
    .end local v17    # "min_y":I
    .end local v21    # "p0":Ljava/lang/Object;
    .end local v22    # "p1":Ljava/lang/Object;
    .end local v23    # "p2":Ljava/lang/Object;
    .end local v24    # "p3":Ljava/lang/Object;
    .end local v25    # "r0":Ljava/lang/Object;
    .end local v26    # "r1":Ljava/lang/Object;
    .end local v27    # "r2":Ljava/lang/Object;
    .end local v28    # "r3":Landroid/graphics/PointF;
    :goto_2
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    move/from16 v36, v0

    if-eqz v36, :cond_a

    .line 447
    const/16 v36, 0x2

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    .line 465
    .end local v30    # "rect_info":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    :cond_a
    :goto_3
    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v36, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v37, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v38, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 466
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_1

    .line 441
    .restart local v30    # "rect_info":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    :cond_b
    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, v29

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_2

    .line 449
    .end local v30    # "rect_info":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    :cond_c
    move-object/from16 v0, v19

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    move/from16 v36, v0

    const/16 v37, 0x1f

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_d

    .line 451
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getMultiSelectRect()Landroid/graphics/Rect;

    move-result-object v36

    move-object/from16 v0, v29

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_3

    .line 455
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    if-nez v36, :cond_e

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getWidth()I

    move-result v35

    .line 458
    .local v35, "w":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getHeight()I

    move-result v9

    .line 459
    .local v9, "h":I
    div-int/lit8 v36, v35, 0x2

    div-int/lit8 v37, v9, 0x2

    div-int/lit8 v38, v35, 0x2

    div-int/lit8 v39, v9, 0x2

    move-object/from16 v0, v29

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_3

    .line 462
    .end local v9    # "h":I
    .end local v35    # "w":I
    :cond_e
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, v29

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_3

    .line 474
    .end local v6    # "caret_mode":I
    .end local v12    # "mBWPInfo":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .end local v29    # "rect1":Landroid/graphics/Rect;
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v36, v0

    const/16 v37, 0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v36

    const/16 v37, 0x26

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_1b

    .line 476
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v32, v0

    check-cast v32, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 478
    .local v32, "sSheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    check-cast v36, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v36

    if-nez v36, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v36

    if-eqz v36, :cond_17

    .line 481
    :cond_11
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 484
    const/4 v5, 0x0

    .line 485
    .local v5, "bAddMore":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    check-cast v36, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsMemo()Z

    move-result v36

    if-eqz v36, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_12

    .line 488
    const/4 v5, 0x1

    .line 490
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v33

    .line 491
    .local v33, "sheetHyperlinkInfo":Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    if-eqz v33, :cond_13

    move-object/from16 v0, v33

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    move/from16 v36, v0

    if-nez v36, :cond_16

    .line 499
    :cond_13
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_15

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    check-cast v36, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v36

    if-nez v36, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    check-cast v36, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleMergeCell()Z

    move-result v36

    if-eqz v36, :cond_15

    .line 501
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    check-cast v36, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellText()Ljava/lang/String;

    move-result-object v36

    const-string/jumbo v37, ""

    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-nez v36, :cond_15

    .line 502
    if-nez v5, :cond_15

    .line 537
    .end local v5    # "bAddMore":Z
    .end local v33    # "sheetHyperlinkInfo":Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    :cond_15
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 538
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_1a

    .line 540
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0

    .line 494
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .restart local v5    # "bAddMore":Z
    .restart local v33    # "sheetHyperlinkInfo":Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    :cond_16
    if-nez v5, :cond_13

    .line 496
    const/4 v5, 0x1

    goto/16 :goto_4

    .line 512
    .end local v5    # "bAddMore":Z
    .end local v33    # "sheetHyperlinkInfo":Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;
    :cond_17
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 513
    const/4 v11, 0x0

    .line 515
    .local v11, "isChart":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-result-object v36

    move-object/from16 v0, v36

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    move/from16 v36, v0

    if-lez v36, :cond_19

    .line 517
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-result-object v36

    move-object/from16 v0, v36

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->nObjectCount:I

    move/from16 v36, v0

    move/from16 v0, v36

    if-ge v10, v0, :cond_19

    .line 519
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getMultiInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTI_INFO;->mMultiItems:[Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;

    move-object/from16 v36, v0

    aget-object v31, v36, v10

    .line 520
    .local v31, "sObjectMultiInfo":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;
    move-object/from16 v0, v31

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;->mObjectType:I

    move/from16 v36, v0

    const/16 v37, 0x8

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_18

    .line 521
    const/4 v11, 0x1

    .line 517
    :cond_18
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 524
    .end local v10    # "i":I
    .end local v31    # "sObjectMultiInfo":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_MULTIITEM_INFO;
    :cond_19
    if-nez v11, :cond_15

    .line 526
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x8

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x5

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x6

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x9

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x1f

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_15

    goto/16 :goto_5

    .line 541
    .end local v11    # "isChart":Z
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v37

    move-object/from16 v0, v37

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v37, v0

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_1

    invoke-virtual/range {v32 .. v32}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v36

    if-eqz v36, :cond_1

    .line 544
    invoke-virtual/range {v32 .. v32}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    move-object/from16 v34, v0

    .line 545
    .local v34, "tSelectedRange":Lcom/infraware/office/evengine/EV$RANGE;
    move-object/from16 v0, v34

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nLeft:I

    move/from16 v36, v0

    move-object/from16 v0, v34

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nTop:I

    move/from16 v37, v0

    move-object/from16 v0, v34

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nRight:I

    move/from16 v38, v0

    move-object/from16 v0, v34

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nBottom:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 546
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0

    .line 550
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .end local v32    # "sSheetEditorActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .end local v34    # "tSelectedRange":Lcom/infraware/office/evengine/EV$RANGE;
    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v36, v0

    const/16 v37, 0x5

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_20

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 552
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getTextMarkInfo_for_popup()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;

    move-result-object v20

    .line 553
    .local v20, "oti":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    move-object/from16 v0, v19

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mObjectType:I

    move/from16 v36, v0

    const/16 v37, 0x3

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_1f

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;->mIsDrawBar:Z

    move/from16 v36, v0

    if-nez v36, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v36

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_1f

    .line 555
    :cond_1c
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 557
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_1d

    .line 559
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0

    .line 562
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    move/from16 v36, v0

    if-lez v36, :cond_1e

    .line 563
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismiss()V

    goto/16 :goto_0

    .line 565
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupMore:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->dismiss()V

    goto/16 :goto_0

    .line 569
    :cond_1f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mItemSize:I

    move/from16 v36, v0

    if-nez v36, :cond_1

    .line 570
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismiss()V

    goto/16 :goto_0

    .line 576
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .end local v20    # "oti":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_TEXT_INFO;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v12

    .line 577
    .restart local v12    # "mBWPInfo":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPCellStatusInfo()I

    move-result v13

    .line 579
    .local v13, "mCellStatus":I
    iget v6, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nCaretMode:I

    .line 581
    .restart local v6    # "caret_mode":I
    const/16 v36, 0x1

    move/from16 v0, v36

    if-ne v6, v0, :cond_23

    .line 584
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    move/from16 v0, v36

    and-int/lit16 v0, v0, 0x400

    move/from16 v36, v0

    if-eqz v36, :cond_21

    .line 585
    const/16 v36, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 587
    :cond_21
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    const/high16 v37, 0x400000

    and-int v36, v36, v37

    if-eqz v36, :cond_22

    .line 588
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 596
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/evengine/EvInterface;->IGetCaretInfo_Editor()Lcom/infraware/office/evengine/EV$CARET_INFO;

    move-result-object v8

    .line 597
    .restart local v8    # "ci":Lcom/infraware/office/evengine/EV$CARET_INFO;
    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nX:I

    move/from16 v36, v0

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nY:I

    move/from16 v37, v0

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nX:I

    move/from16 v38, v0

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nWidth:I

    move/from16 v39, v0

    add-int v38, v38, v39

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nY:I

    move/from16 v39, v0

    iget v0, v8, Lcom/infraware/office/evengine/EV$CARET_INFO;->nHeight:I

    move/from16 v40, v0

    add-int v39, v39, v40

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 598
    const/16 v36, 0x1

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    .line 599
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0

    .line 609
    .end local v8    # "ci":Lcom/infraware/office/evengine/EV$CARET_INFO;
    :cond_23
    const/16 v36, 0x2

    move/from16 v0, v36

    if-eq v6, v0, :cond_24

    const/16 v36, 0x3

    move/from16 v0, v36

    if-ne v6, v0, :cond_28

    .line 611
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v36

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_26

    .line 613
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 628
    :cond_25
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 629
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 631
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0

    .line 618
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    :cond_26
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    move/from16 v0, v36

    and-int/lit16 v0, v0, 0x400

    move/from16 v36, v0

    if-eqz v36, :cond_27

    .line 619
    const/16 v36, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 621
    :cond_27
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    const/high16 v37, 0x400000

    and-int v36, v36, v37

    if-eqz v36, :cond_25

    .line 622
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    goto :goto_7

    .line 633
    :cond_28
    const/16 v36, 0x5

    move/from16 v0, v36

    if-ne v6, v0, :cond_2b

    .line 635
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    move/from16 v0, v36

    and-int/lit16 v0, v0, 0x400

    move/from16 v36, v0

    if-eqz v36, :cond_29

    .line 636
    const/16 v36, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 638
    :cond_29
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    const/high16 v37, 0x400000

    and-int v36, v36, v37

    if-eqz v36, :cond_2a

    .line 639
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 643
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 644
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 646
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0

    .line 648
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_2e

    .line 650
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    move/from16 v0, v36

    and-int/lit16 v0, v0, 0x400

    move/from16 v36, v0

    if-eqz v36, :cond_2c

    .line 651
    const/16 v36, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 653
    :cond_2c
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    const/high16 v37, 0x400000

    and-int v36, v36, v37

    if-eqz v36, :cond_2d

    .line 654
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 659
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 660
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 661
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0

    .line 663
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    :cond_2e
    const/16 v36, 0x7

    move/from16 v0, v36

    if-ne v6, v0, :cond_3a

    .line 665
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    move/from16 v0, v36

    and-int/lit16 v0, v0, 0x400

    move/from16 v36, v0

    if-eqz v36, :cond_2f

    .line 666
    const/16 v36, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 668
    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v18

    .line 670
    .local v18, "object_type":I
    const/16 v36, 0xe

    move/from16 v0, v18

    move/from16 v1, v36

    if-eq v0, v1, :cond_31

    .line 671
    iget v0, v12, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    move/from16 v36, v0

    const/high16 v37, 0x400000

    and-int v36, v36, v37

    if-eqz v36, :cond_30

    .line 672
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->AddButton(I)V

    .line 674
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    move-object/from16 v36, v0

    if-eqz v36, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->hasText()Z

    move-result v36

    if-eqz v36, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v36, v0

    if-eqz v36, :cond_31

    .line 684
    :cond_31
    const/16 v36, 0x8

    move/from16 v0, v18

    move/from16 v1, v36

    if-ne v0, v1, :cond_34

    .line 714
    :cond_32
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;

    move-result-object v19

    .line 715
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    new-instance v29, Landroid/graphics/Rect;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/Rect;-><init>()V

    .line 716
    .restart local v29    # "rect1":Landroid/graphics/Rect;
    move-object/from16 v0, v19

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    move/from16 v36, v0

    const/16 v37, 0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_37

    .line 718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-result-object v30

    .line 719
    .restart local v30    # "rect_info":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    if-eqz v36, :cond_36

    .line 722
    new-instance v7, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v37, v0

    add-int v36, v36, v37

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    const/high16 v37, 0x40000000    # 2.0f

    div-float v36, v36, v37

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v38, v0

    add-int v37, v37, v38

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    const/high16 v38, 0x40000000    # 2.0f

    div-float v37, v37, v38

    move/from16 v0, v36

    move/from16 v1, v37

    invoke-direct {v7, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 724
    .local v7, "center":Landroid/graphics/PointF;
    new-instance v21, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 725
    .local v21, "p0":Landroid/graphics/PointF;
    new-instance v22, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    move-object/from16 v0, v22

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 726
    .local v22, "p1":Landroid/graphics/PointF;
    new-instance v23, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 727
    .local v23, "p2":Landroid/graphics/PointF;
    new-instance v24, Landroid/graphics/PointF;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    move-object/from16 v0, v24

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 730
    .local v24, "p3":Landroid/graphics/PointF;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    move-object/from16 v0, v21

    move/from16 v1, v36

    invoke-static {v0, v7, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v25

    .line 731
    .local v25, "r0":Landroid/graphics/PointF;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    move-object/from16 v0, v22

    move/from16 v1, v36

    invoke-static {v0, v7, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v26

    .line 732
    .local v26, "r1":Landroid/graphics/PointF;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    move-object/from16 v0, v23

    move/from16 v1, v36

    invoke-static {v0, v7, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v27

    .line 733
    .local v27, "r2":Landroid/graphics/PointF;
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->rotateAngle:I

    move/from16 v36, v0

    move-object/from16 v0, v24

    move/from16 v1, v36

    invoke-static {v0, v7, v1}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;I)Landroid/graphics/PointF;

    move-result-object v28

    .line 736
    .restart local v28    # "r3":Landroid/graphics/PointF;
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v36, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v36

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v37, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v38, v0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->max(FF)F

    move-result v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v36

    move/from16 v0, v36

    float-to-int v14, v0

    .line 737
    .restart local v14    # "max_x":I
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v36, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v36

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v37, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v38, v0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->min(FF)F

    move-result v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v36

    move/from16 v0, v36

    float-to-int v0, v0

    move/from16 v16, v0

    .line 738
    .restart local v16    # "min_x":I
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v36, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v36

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v38, v0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->max(FF)F

    move-result v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v36

    move/from16 v0, v36

    float-to-int v15, v0

    .line 739
    .restart local v15    # "max_y":I
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v36, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v36

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v38, v0

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->min(FF)F

    move-result v37

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v36

    move/from16 v0, v36

    float-to-int v0, v0

    move/from16 v17, v0

    .line 749
    .restart local v17    # "min_y":I
    const/16 v28, 0x0

    move-object/from16 v27, v28

    .local v27, "r2":Ljava/lang/Object;
    move-object/from16 v26, v28

    .local v26, "r1":Ljava/lang/Object;
    move-object/from16 v25, v28

    .local v25, "r0":Ljava/lang/Object;
    move-object/from16 v24, v28

    .local v24, "p3":Ljava/lang/Object;
    move-object/from16 v23, v28

    .local v23, "p2":Ljava/lang/Object;
    move-object/from16 v22, v28

    .local v22, "p1":Ljava/lang/Object;
    move-object/from16 v21, v28

    .local v21, "p0":Ljava/lang/Object;
    move-object/from16 v7, v28

    .line 751
    .local v7, "center":Ljava/lang/Object;
    move-object/from16 v0, v29

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2, v14, v15}, Landroid/graphics/Rect;->set(IIII)V

    .line 768
    .end local v7    # "center":Ljava/lang/Object;
    .end local v14    # "max_x":I
    .end local v15    # "max_y":I
    .end local v16    # "min_x":I
    .end local v17    # "min_y":I
    .end local v21    # "p0":Ljava/lang/Object;
    .end local v22    # "p1":Ljava/lang/Object;
    .end local v23    # "p2":Ljava/lang/Object;
    .end local v24    # "p3":Ljava/lang/Object;
    .end local v25    # "r0":Ljava/lang/Object;
    .end local v26    # "r1":Ljava/lang/Object;
    .end local v27    # "r2":Ljava/lang/Object;
    .end local v28    # "r3":Landroid/graphics/PointF;
    :goto_9
    move-object/from16 v0, v30

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    move/from16 v36, v0

    if-eqz v36, :cond_33

    .line 769
    const/16 v36, 0x2

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mPopupType:I

    .line 786
    .end local v30    # "rect_info":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    :cond_33
    :goto_a
    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v36, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v37, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v38, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 787
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0

    .line 691
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .end local v29    # "rect1":Landroid/graphics/Rect;
    :cond_34
    const/16 v36, 0x11

    move/from16 v0, v18

    move/from16 v1, v36

    if-eq v0, v1, :cond_32

    .line 693
    const/16 v36, 0x11

    move/from16 v0, v18

    move/from16 v1, v36

    if-eq v0, v1, :cond_32

    .line 695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    const/16 v37, 0xa

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/evengine/EvInterface;->IGetTextWrapType()I

    move-result v36

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    move-result-object v36

    move-object/from16 v0, v36

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->bRotationEnabled:I

    move/from16 v36, v0

    if-eqz v36, :cond_32

    .line 703
    :cond_35
    if-eqz v18, :cond_32

    .line 707
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIsEncryptDoc()Z

    move-result v36

    if-eqz v36, :cond_32

    .line 709
    const/16 v36, 0x0

    move/from16 v0, v36

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mIsShowMoreWindow:Z

    goto/16 :goto_8

    .line 763
    .restart local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .restart local v29    # "rect1":Landroid/graphics/Rect;
    .restart local v30    # "rect_info":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    :cond_36
    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, v29

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_9

    .line 771
    .end local v30    # "rect_info":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;
    :cond_37
    move-object/from16 v0, v19

    iget v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->mBaseType:I

    move/from16 v36, v0

    const/16 v37, 0x1f

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_38

    .line 773
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getMultiSelectRect()Landroid/graphics/Rect;

    move-result-object v36

    move-object/from16 v0, v29

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_a

    .line 777
    :cond_38
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v36

    if-nez v36, :cond_39

    .line 778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getWidth()I

    move-result v35

    .line 779
    .restart local v35    # "w":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getHeight()I

    move-result v9

    .line 780
    .restart local v9    # "h":I
    div-int/lit8 v36, v35, 0x2

    div-int/lit8 v37, v9, 0x2

    div-int/lit8 v38, v35, 0x2

    div-int/lit8 v39, v9, 0x2

    move-object/from16 v0, v29

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_a

    .line 782
    .end local v9    # "h":I
    .end local v35    # "w":I
    :cond_39
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->startRangePoint:Landroid/graphics/Point;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v37, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v38, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;->endRangePoint:Landroid/graphics/Point;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v39, v0

    move-object/from16 v0, v29

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_a

    .line 792
    .end local v18    # "object_type":I
    .end local v19    # "oi":Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_INFO;
    .end local v29    # "rect1":Landroid/graphics/Rect;
    :cond_3a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mDocType:I

    move/from16 v36, v0

    const/16 v37, 0x3

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_1

    if-nez v6, :cond_1

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    move-object/from16 v36, v0

    if-eqz v36, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->hasText()Z

    move-result v36

    if-eqz v36, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v36, v0

    if-eqz v36, :cond_1

    .line 796
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getWidth()I

    move-result v35

    .line 797
    .restart local v35    # "w":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->mParentView:Landroid/view/View;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getHeight()I

    move-result v9

    .line 798
    .restart local v9    # "h":I
    div-int/lit8 v36, v35, 0x2

    div-int/lit8 v37, v9, 0x2

    div-int/lit8 v38, v35, 0x2

    div-int/lit8 v39, v9, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->setRect(IIII)Z

    .line 799
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->doAction()V

    goto/16 :goto_0
.end method
