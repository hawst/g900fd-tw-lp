.class public Lcom/infraware/polarisoffice5/common/ImmManager;
.super Ljava/lang/Object;
.source "ImmManager.java"


# static fields
.field private static final DELAY_TIME:I = 0x12c


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mDelayHandleTask:Ljava/lang/Runnable;

.field private mDelayHandler:Landroid/os/Handler;

.field private mDiaglog:Landroid/app/AlertDialog;

.field private mInputFlag:I

.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "a_activity"    # Landroid/app/Activity;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mActivity:Landroid/app/Activity;

    .line 76
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/ImmManager;->init()V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/app/AlertDialog;)V
    .locals 0
    .param p1, "a_activity"    # Landroid/app/Activity;
    .param p2, "a_dialog"    # Landroid/app/AlertDialog;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mActivity:Landroid/app/Activity;

    .line 83
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDiaglog:Landroid/app/AlertDialog;

    .line 84
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/ImmManager;->init()V

    .line 85
    return-void
.end method

.method public static hideDisposableIme(Landroid/app/Activity;)V
    .locals 4
    .param p0, "a_activity"    # Landroid/app/Activity;

    .prologue
    .line 55
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "focusedView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 57
    const-string/jumbo v2, "input_method"

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 58
    .local v1, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 60
    .end local v1    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method public static hideDisposableIme(Landroid/app/Activity;Landroid/app/AlertDialog;)V
    .locals 4
    .param p0, "a_activity"    # Landroid/app/Activity;
    .param p1, "a_dialog"    # Landroid/app/AlertDialog;

    .prologue
    .line 65
    invoke-virtual {p1}, Landroid/app/AlertDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, "focusedView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 67
    const-string/jumbo v2, "input_method"

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 68
    .local v1, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 70
    .end local v1    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandler:Landroid/os/Handler;

    .line 91
    new-instance v0, Lcom/infraware/polarisoffice5/common/ImmManager$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ImmManager$2;-><init>(Lcom/infraware/polarisoffice5/common/ImmManager;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandleTask:Ljava/lang/Runnable;

    .line 98
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mActivity:Landroid/app/Activity;

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 100
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mInputFlag:I

    .line 102
    return-void
.end method

.method public static showDisposableIme(Landroid/app/Activity;)V
    .locals 5
    .param p0, "a_activity"    # Landroid/app/Activity;

    .prologue
    .line 34
    move-object v0, p0

    .line 35
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Lcom/infraware/polarisoffice5/common/ImmManager$1;

    invoke-direct {v1, v0}, Lcom/infraware/polarisoffice5/common/ImmManager$1;-><init>(Landroid/app/Activity;)V

    .line 49
    .local v1, "delayHandleTask":Ljava/lang/Runnable;
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 50
    .local v2, "delayHandler":Landroid/os/Handler;
    const-wide/16 v3, 0x12c

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 51
    return-void
.end method


# virtual methods
.method public hideForcedIme()V
    .locals 4

    .prologue
    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "focusedView":Landroid/view/View;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDiaglog:Landroid/app/AlertDialog;

    if-nez v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 155
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 157
    :cond_0
    return-void

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDiaglog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandleTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ImmManager;->hideForcedIme()V

    .line 111
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 116
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SHOW_RESUME_KEYPAD()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandleTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 119
    :cond_0
    return-void
.end method

.method public removeKeyboardCallback()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandleTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandleTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 144
    :cond_0
    return-void
.end method

.method public showDelayedIme()V
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDelayHandleTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 138
    return-void
.end method

.method public showForcedIme()V
    .locals 3

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 125
    .local v0, "focusedView":Landroid/view/View;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDiaglog:Landroid/app/AlertDialog;

    if-nez v1, :cond_1

    .line 126
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 130
    :goto_0
    if-eqz v0, :cond_0

    .line 131
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mInputFlag:I

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 132
    :cond_0
    return-void

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ImmManager;->mDiaglog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
