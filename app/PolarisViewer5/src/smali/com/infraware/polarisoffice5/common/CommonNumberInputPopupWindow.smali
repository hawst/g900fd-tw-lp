.class public Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
.super Landroid/widget/PopupWindow;
.source "CommonNumberInputPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;
    }
.end annotation


# static fields
.field public static final INPUT_TYPE_FLOAT:I = 0x1

.field public static final INPUT_TYPE_INTEGER:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "CommonNumberInputPopupWindow"

.field public static final POPUP_TYPE_DEFAULT:I = 0x0

.field public static final POPUP_TYPE_SPINNER:I = 0x1

.field public static mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;


# instance fields
.field private button0:Landroid/widget/Button;

.field private button1:Landroid/widget/Button;

.field private button2:Landroid/widget/Button;

.field private button3:Landroid/widget/Button;

.field private button4:Landroid/widget/Button;

.field private button5:Landroid/widget/Button;

.field private button6:Landroid/widget/Button;

.field private button7:Landroid/widget/Button;

.field private button8:Landroid/widget/Button;

.field private button9:Landroid/widget/Button;

.field private button_del:Landroid/widget/Button;

.field private button_dot:Landroid/widget/Button;

.field canMinus:Z

.field isDel:Z

.field isDot:Z

.field private mActivity:Landroid/app/Activity;

.field private mBtnOk:Landroid/widget/ImageButton;

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mConfirmClickListener:Landroid/view/View$OnClickListener;

.field private mDefaultValue:I

.field private mEditNumber:Landroid/widget/EditText;

.field private mFieldType:I

.field private mListener:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;

.field private mMax:I

.field private mMaxLen:I

.field private mMin:I

.field private mMinLen:I

.field private mPopupType:I

.field private mTitle:Landroid/widget/TextView;

.field private mTitleRes:I

.field private mValue:I

.field private m_ToastMsg:Landroid/widget/Toast;

.field savePos:I

.field private textwatcher:Landroid/text/TextWatcher;

.field wasSecondnumberZero:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "aActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Landroid/widget/PopupWindow;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    .line 69
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->isDot:Z

    .line 70
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->isDel:Z

    .line 71
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->wasSecondnumberZero:Z

    .line 72
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->canMinus:Z

    .line 73
    iput v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->savePos:I

    .line 251
    new-instance v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$2;-><init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mConfirmClickListener:Landroid/view/View$OnClickListener;

    .line 258
    new-instance v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;-><init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    .line 346
    new-instance v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;-><init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->textwatcher:Landroid/text/TextWatcher;

    .line 94
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    .line 95
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "CommonNumberInputPopupWindow create failed, activity is NULL"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->onCreate()V

    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->onConfirmClick()V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMaxLen:I

    return v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMinLen:I

    return v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->warning()V

    return-void
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I

    return v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mBtnOk:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public static configurationChanged()V
    .locals 1

    .prologue
    .line 525
    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->dismiss()V

    .line 527
    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->onCreateView()V

    .line 528
    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->update()V

    .line 529
    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->show()V

    .line 531
    :cond_0
    return-void
.end method

.method public static createInputNumberPopupWindow(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 80
    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    :try_start_0
    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->dismiss()V

    .line 83
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :cond_0
    :goto_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    .line 89
    sget-object v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopup:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    return-object v0

    .line 84
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initializeShow()V
    .locals 3

    .prologue
    .line 467
    const-string/jumbo v0, "CommonNumberInputPopupWindow"

    const-string/jumbo v1, "show input number popup"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string/jumbo v0, "CommonNumberInputPopupWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "popup type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopupType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , input type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , default value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mDefaultValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , min : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , mMax : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , title id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mTitleRes:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->onCreateView()V

    .line 471
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->initEvent()V

    .line 472
    return-void
.end method

.method private onConfirmClick()V
    .locals 2

    .prologue
    .line 236
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    if-nez v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mValue:I

    .line 241
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mValue:I

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mDefaultValue:I

    if-eq v0, v1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mListener:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mListener:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mValue:I

    int-to-float v1, v1

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;->onConfirmed(F)V

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->dismiss()V

    .line 249
    return-void

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mValue:I

    goto :goto_0
.end method

.method private onCreate()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mDefaultValue:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopupType:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    .line 105
    iput v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mTitleRes:I

    .line 107
    const/4 v0, -0x2

    invoke-virtual {p0, v2, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setWindowLayoutMode(II)V

    .line 109
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 110
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setInputMethodMode(I)V

    .line 111
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setFocusable(Z)V

    .line 112
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setTouchable(Z)V

    .line 113
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setOutsideTouchable(Z)V

    .line 114
    new-instance v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$1;-><init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 124
    return-void
.end method

.method private onToastMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "strMsg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 450
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->m_ToastMsg:Landroid/widget/Toast;

    .line 454
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->m_ToastMsg:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 455
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 456
    return-void

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private warning()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x41200000    # 10.0f

    .line 434
    const/4 v0, 0x0

    .line 435
    .local v0, "formattedString":Ljava/lang/String;
    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    if-nez v1, :cond_1

    .line 436
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07024a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_0

    .line 438
    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->onToastMessage(Ljava/lang/String;)V

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07024b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 442
    if-eqz v0, :cond_0

    .line 443
    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->onToastMessage(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getDefaultValue()I
    .locals 1

    .prologue
    .line 516
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mDefaultValue:I

    return v0
.end method

.method public getFiledType()I
    .locals 1

    .prologue
    .line 520
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    return v0
.end method

.method public getInputNumberListener()Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mListener:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;

    return-object v0
.end method

.method public getMax()I
    .locals 1

    .prologue
    .line 519
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I

    return v0
.end method

.method public getMin()I
    .locals 1

    .prologue
    .line 518
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    return v0
.end method

.method public getPopupType()I
    .locals 1

    .prologue
    .line 521
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopupType:I

    return v0
.end method

.method public getTitleRes()I
    .locals 1

    .prologue
    .line 517
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mTitleRes:I

    return v0
.end method

.method public initEvent()V
    .locals 12

    .prologue
    const v11, 0x7f02004c

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 160
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mTitleRes:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 161
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mTitle:Landroid/widget/TextView;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mTitleRes:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 164
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    const/16 v3, 0x1000

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 166
    new-array v0, v7, [Landroid/text/InputFilter;

    .line 167
    .local v0, "filters":[Landroid/text/InputFilter;
    const-string/jumbo v2, "%d"

    new-array v3, v7, [Ljava/lang/Object;

    iget v4, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMaxLen:I

    .line 168
    const-string/jumbo v2, "%d"

    new-array v3, v7, [Ljava/lang/Object;

    iget v4, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMinLen:I

    .line 170
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    if-ne v2, v7, :cond_2

    .line 171
    :cond_1
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMaxLen:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMaxLen:I

    .line 174
    :cond_2
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMaxLen:I

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v8

    .line 175
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 177
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    if-nez v2, :cond_9

    .line 178
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mValue:I

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    if-ge v2, v3, :cond_8

    .line 179
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 186
    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->textwatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 189
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button0:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button1:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button2:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button3:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button4:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button5:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button6:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button7:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button8:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button9:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button0:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_del:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 203
    .local v1, "resource":Landroid/content/res/Resources;
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    if-ne v2, v7, :cond_4

    .line 205
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 206
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    const v3, 0x7f080011

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x7f050122

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v10, v9, v3, v4}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 207
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    invoke-virtual {v2, v11}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 209
    :cond_4
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    if-gez v2, :cond_5

    .line 211
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    const-string/jumbo v3, "-"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    const v3, 0x41d2a3d7    # 26.33f

    invoke-virtual {v2, v7, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 213
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    const v3, 0x7f080011

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x7f050122

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v10, v9, v3, v4}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 214
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    invoke-virtual {v2, v11}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 217
    :cond_5
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    if-eq v2, v7, :cond_6

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    if-gez v2, :cond_7

    .line 218
    :cond_6
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    :cond_7
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_a

    .line 222
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mBtnOk:Landroid/widget/ImageButton;

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 223
    iput-boolean v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->canMinus:Z

    .line 232
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mBtnOk:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mConfirmClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    return-void

    .line 181
    .end local v1    # "resource":Landroid/content/res/Resources;
    :cond_8
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    const-string/jumbo v3, "%d"

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mValue:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 183
    :cond_9
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    if-ne v2, v7, :cond_3

    .line 184
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    const-string/jumbo v3, "%.1f"

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mValue:I

    int-to-float v5, v5

    const/high16 v6, 0x41200000    # 10.0f

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 227
    .restart local v1    # "resource":Landroid/content/res/Resources;
    :cond_a
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mBtnOk:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 228
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->canMinus:Z

    goto :goto_1
.end method

.method public onCreateView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 127
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 129
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .line 131
    .local v1, "inputNumberView":Landroid/view/View;
    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopupType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 132
    const v2, 0x7f030011

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 136
    :goto_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setContentView(Landroid/view/View;)V

    .line 138
    const v2, 0x7f0b0058

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mTitle:Landroid/widget/TextView;

    .line 139
    const v2, 0x7f0b0059

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mBtnOk:Landroid/widget/ImageButton;

    .line 140
    const v2, 0x7f0b005a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;

    .line 142
    const v2, 0x7f0b0101

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button0:Landroid/widget/Button;

    .line 143
    const v2, 0x7f0b00f4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button1:Landroid/widget/Button;

    .line 144
    const v2, 0x7f0b00f5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button2:Landroid/widget/Button;

    .line 145
    const v2, 0x7f0b00f6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button3:Landroid/widget/Button;

    .line 146
    const v2, 0x7f0b00f8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button4:Landroid/widget/Button;

    .line 147
    const v2, 0x7f0b00f9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button5:Landroid/widget/Button;

    .line 148
    const v2, 0x7f0b00fa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button6:Landroid/widget/Button;

    .line 149
    const v2, 0x7f0b00fc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button7:Landroid/widget/Button;

    .line 150
    const v2, 0x7f0b00fd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button8:Landroid/widget/Button;

    .line 151
    const v2, 0x7f0b00fe

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button9:Landroid/widget/Button;

    .line 152
    const v2, 0x7f0b0102

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_del:Landroid/widget/Button;

    .line 153
    const v2, 0x7f0b0100

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;

    .line 154
    return-void

    .line 134
    :cond_0
    const v2, 0x7f030010

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public setDefaultValue(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    .locals 0
    .param p1, "aDefaultValue"    # I

    .prologue
    .line 476
    iput p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mDefaultValue:I

    iput p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mValue:I

    .line 477
    return-object p0
.end method

.method public setFieldType(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    .locals 0
    .param p1, "aFieldType"    # I

    .prologue
    .line 496
    iput p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I

    .line 497
    return-object p0
.end method

.method public setInputNumberListener(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    .locals 0
    .param p1, "aListener"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;

    .prologue
    .line 506
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mListener:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;

    .line 507
    return-object p0
.end method

.method public setMax(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    .locals 0
    .param p1, "aMaxValue"    # I

    .prologue
    .line 491
    iput p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I

    .line 492
    return-object p0
.end method

.method public setMin(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    .locals 0
    .param p1, "aMinValue"    # I

    .prologue
    .line 486
    iput p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I

    .line 487
    return-object p0
.end method

.method public setPopupType(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    .locals 0
    .param p1, "aType"    # I

    .prologue
    .line 501
    iput p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mPopupType:I

    .line 502
    return-object p0
.end method

.method public setTitle(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    .locals 0
    .param p1, "aTitleRes"    # I

    .prologue
    .line 481
    iput p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mTitleRes:I

    .line 482
    return-object p0
.end method

.method public show()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 461
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->initializeShow()V

    .line 462
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 463
    return-void
.end method
