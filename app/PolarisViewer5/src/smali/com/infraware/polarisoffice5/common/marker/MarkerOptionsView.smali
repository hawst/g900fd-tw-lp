.class public Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
.super Landroid/widget/LinearLayout;
.source "MarkerOptionsView.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_PEN_MODE;


# static fields
.field private static final HIGHLIGHTER_TRANSPARENCY:I = 0x80

.field private static final PEN_TRANSPARENCY:I = 0xff


# instance fields
.field private final ADUJUST_MARKER_THICKNESS:I

.field private final MAXTHICKNESS:I

.field private final MINTHICKNESS:I

.field private mColor:I

.field private mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColorClickListener:Landroid/view/View$OnClickListener;

.field mContext:Landroid/content/Context;

.field private mHighlighterType:Landroid/widget/ImageView;

.field private mMarkerThicknessMaXBtn:Landroid/widget/ImageView;

.field private mMarkerThicknessMinBtn:Landroid/widget/ImageView;

.field mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mPenType:Landroid/widget/ImageView;

.field private mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

.field private mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mThickness:I

.field private mThicknessBar:Landroid/widget/SeekBar;

.field private mType:I

.field private mTypeClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 57
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    .line 22
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThicknessBar:Landroid/widget/SeekBar;

    .line 24
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;

    .line 25
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;

    .line 27
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mMarkerThicknessMinBtn:Landroid/widget/ImageView;

    .line 28
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mMarkerThicknessMaXBtn:Landroid/widget/ImageView;

    .line 30
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 31
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 32
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 33
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 36
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 39
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 40
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 41
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I

    .line 44
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I

    .line 45
    const v0, -0x19f0f1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    .line 49
    iput v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->ADUJUST_MARKER_THICKNESS:I

    .line 51
    const/16 v0, 0x19

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->MAXTHICKNESS:I

    .line 52
    iput v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->MINTHICKNESS:I

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    .line 235
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 247
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    .line 265
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    .line 300
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 58
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    .line 59
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->initResource()V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    .line 22
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThicknessBar:Landroid/widget/SeekBar;

    .line 24
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;

    .line 25
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;

    .line 27
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mMarkerThicknessMinBtn:Landroid/widget/ImageView;

    .line 28
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mMarkerThicknessMaXBtn:Landroid/widget/ImageView;

    .line 30
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 31
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 32
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 33
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 36
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 39
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 40
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 41
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I

    .line 44
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I

    .line 45
    const v0, -0x19f0f1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    .line 49
    iput v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->ADUJUST_MARKER_THICKNESS:I

    .line 51
    const/16 v0, 0x19

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->MAXTHICKNESS:I

    .line 52
    iput v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->MINTHICKNESS:I

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    .line 235
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 247
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$4;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    .line 265
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    .line 300
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 64
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    .line 65
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->initResource()V

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .prologue
    .line 19
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I

    return v0
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThicknessBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I

    return p1
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    .prologue
    .line 19
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    return v0
.end method

.method static synthetic access$802(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    return p1
.end method

.method private getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    .locals 2

    .prologue
    .line 208
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 232
    :goto_0
    return-object v0

    .line 210
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 211
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 212
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 213
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 214
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 215
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 216
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 217
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 218
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 219
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 220
    :cond_5
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 221
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 222
    :cond_6
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_7

    .line 223
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 224
    :cond_7
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_8

    .line 225
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 226
    :cond_8
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_9

    .line 227
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 228
    :cond_9
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_a

    .line 229
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto/16 :goto_0

    .line 230
    :cond_a
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_b

    .line 231
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto/16 :goto_0

    .line 232
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private initResource()V
    .locals 5

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 70
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03002c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 71
    .local v1, "linearLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->addView(Landroid/view/View;)V

    .line 74
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    const v4, 0x43808000    # 257.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 76
    const v2, 0x7f0b0149

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    .line 77
    const v2, 0x7f0b014e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThicknessBar:Landroid/widget/SeekBar;

    .line 78
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThicknessBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mSeekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 80
    const v2, 0x7f0b014b

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;

    .line 81
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 83
    const v2, 0x7f0b014c

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;

    .line 84
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 87
    const v2, 0x7f0b0150

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 88
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 90
    const v2, 0x7f0b0151

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 91
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 93
    const v2, 0x7f0b0152

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 94
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 96
    const v2, 0x7f0b0153

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 97
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 99
    const v2, 0x7f0b0154

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 100
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 102
    const v2, 0x7f0b0155

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 103
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 105
    const v2, 0x7f0b0156

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 106
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 108
    const v2, 0x7f0b0157

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 109
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 111
    const v2, 0x7f0b0158

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 112
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 114
    const v2, 0x7f0b0159

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 115
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 117
    const v2, 0x7f0b015a

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 118
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 120
    const v2, 0x7f0b015b

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 121
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 124
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 125
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 126
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 127
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 128
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 129
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 130
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 131
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050061

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 132
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050062

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 133
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050063

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 134
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050064

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 136
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f020118

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 138
    const v2, 0x7f0b014d

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mMarkerThicknessMinBtn:Landroid/widget/ImageView;

    .line 139
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mMarkerThicknessMinBtn:Landroid/widget/ImageView;

    new-instance v3, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    const v2, 0x7f0b014f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mMarkerThicknessMaXBtn:Landroid/widget/ImageView;

    .line 152
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mMarkerThicknessMaXBtn:Landroid/widget/ImageView;

    new-instance v3, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$2;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$2;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    return v0
.end method

.method public getThickness()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I

    return v0
.end method

.method public getTransparency()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I

    if-nez v0, :cond_0

    .line 202
    const/16 v0, 0xff

    .line 204
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x80

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I

    return v0
.end method

.method public initOptions(III)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "thickness"    # I
    .param p3, "color"    # I

    .prologue
    const/4 v3, 0x1

    .line 167
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I

    .line 168
    iput p2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I

    .line 169
    iput p3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    .line 171
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setThickness(I)V

    .line 172
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setColor(I)V

    .line 173
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThicknessBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 175
    iget v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mType:I

    if-nez v1, :cond_1

    .line 176
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    .line 177
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPenType:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 183
    :goto_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    .line 184
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 186
    :cond_0
    return-void

    .line 180
    .end local v0    # "view":Landroid/view/View;
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    .line 181
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mHighlighterType:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method public setOtherColor(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 290
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    .line 291
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 292
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 293
    :cond_0
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    .line 294
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setColor(I)V

    .line 297
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setColor(I)V

    .line 298
    return-void
.end method
