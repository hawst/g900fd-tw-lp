.class Lcom/infraware/polarisoffice5/common/ClearEditText$1;
.super Ljava/lang/Object;
.source "ClearEditText.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/ClearEditText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/ClearEditText;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ClearEditText$1;->this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 68
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ClearEditText$1;->this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;

    # getter for: Lcom/infraware/polarisoffice5/common/ClearEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/ClearEditText;->access$000(Lcom/infraware/polarisoffice5/common/ClearEditText;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ClearEditText$1;->this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;

    # getter for: Lcom/infraware/polarisoffice5/common/ClearEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/ClearEditText;->access$000(Lcom/infraware/polarisoffice5/common/ClearEditText;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->clearDictionaryPanel()Z

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText$1;->this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;

    .line 73
    .local v0, "et":Lcom/infraware/polarisoffice5/common/ClearEditText;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    if-nez v1, :cond_2

    .line 84
    :cond_1
    :goto_0
    return v4

    .line 76
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 79
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ClearEditText$1;->this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;

    # getter for: Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/ClearEditText;->access$100(Lcom/infraware/polarisoffice5/common/ClearEditText;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 81
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/ClearEditText;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ClearEditText$1;->this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ClearEditText;->handleClearButton()V

    goto :goto_0
.end method
