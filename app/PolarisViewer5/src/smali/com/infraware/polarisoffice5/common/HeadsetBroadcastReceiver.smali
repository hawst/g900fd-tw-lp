.class public Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HeadsetBroadcastReceiver.java"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mbCheck:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->mHandler:Landroid/os/Handler;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->mbCheck:Z

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    const-string/jumbo v0, "state"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    .line 19
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->mbCheck:Z

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 21
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->mbCheck:Z

    .line 29
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-string/jumbo v0, "state"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 26
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->mbCheck:Z

    goto :goto_0
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/HeadsetBroadcastReceiver;->mHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method
