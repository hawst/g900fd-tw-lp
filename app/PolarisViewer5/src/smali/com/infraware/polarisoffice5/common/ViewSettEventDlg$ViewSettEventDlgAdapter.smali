.class Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;
.super Landroid/widget/BaseAdapter;
.source "ViewSettEventDlg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewSettEventDlgAdapter"
.end annotation


# instance fields
.field private m_oInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Landroid/content/Context;I)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "eventType"    # I

    .prologue
    .line 438
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 439
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    .line 440
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 449
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 450
    const/4 v0, 0x0

    .line 452
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 457
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 476
    if-nez p2, :cond_1

    .line 477
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f03005b

    invoke-virtual {v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 478
    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-direct {v0, v3, v7}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;)V

    .line 479
    .local v0, "holder":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;
    const v3, 0x7f0b0103

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->txt:Landroid/widget/TextView;

    .line 480
    const v3, 0x7f0b0278

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->check:Landroid/widget/CheckBox;

    .line 482
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 487
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 489
    .local v1, "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->check:Landroid/widget/CheckBox;

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 491
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget v3, v3, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->text_res_id:I

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 492
    .local v2, "text":Ljava/lang/String;
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->txt:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    iget-boolean v3, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->enable:Z

    if-eqz v3, :cond_2

    .line 496
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->check:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 497
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->txt:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 506
    :goto_1
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->check:Landroid/widget/CheckBox;

    iget-boolean v4, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 508
    const-string/jumbo v3, "SINGLE_SLIDE_VIEW"

    iget-object v4, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 510
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 511
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->txt:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 512
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->check:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 514
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->check:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 522
    :cond_0
    :goto_2
    invoke-virtual {p2, v5}, Landroid/view/View;->setSelected(Z)V

    .line 523
    return-object p2

    .line 485
    .end local v0    # "holder":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;
    .end local v1    # "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    .end local v2    # "text":Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;

    .restart local v0    # "holder":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;
    goto :goto_0

    .line 502
    .restart local v1    # "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    .restart local v2    # "text":Ljava/lang/String;
    :cond_2
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->txt:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 503
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->check:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_1

    .line 517
    :cond_3
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->txt:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 518
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;->check:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_2
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 464
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->enable:Z

    if-nez v0, :cond_0

    .line 467
    const/4 v0, 0x0

    .line 469
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
