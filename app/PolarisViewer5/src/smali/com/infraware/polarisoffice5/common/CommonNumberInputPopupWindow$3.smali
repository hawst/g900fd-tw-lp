.class Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;
.super Ljava/lang/Object;
.source "CommonNumberInputPopupWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

.field value:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V
    .locals 1

    .prologue
    .line 258
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 264
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    .line 266
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 330
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMaxLen:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$400(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v1

    if-gt v0, v1, :cond_3

    .line 332
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 334
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    const-string/jumbo v3, ""

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-ne v0, v4, :cond_2

    .line 338
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 341
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 343
    :cond_3
    return-void

    .line 269
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 270
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 272
    :cond_4
    const-string/jumbo v0, "0"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 275
    :pswitch_2
    const-string/jumbo v0, "1"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 278
    :pswitch_3
    const-string/jumbo v0, "2"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 281
    :pswitch_4
    const-string/jumbo v0, "3"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 284
    :pswitch_5
    const-string/jumbo v0, "4"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 287
    :pswitch_6
    const-string/jumbo v0, "5"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 290
    :pswitch_7
    const-string/jumbo v0, "6"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 293
    :pswitch_8
    const-string/jumbo v0, "7"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 296
    :pswitch_9
    const-string/jumbo v0, "8"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 299
    :pswitch_a
    const-string/jumbo v0, "9"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 302
    :pswitch_b
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 304
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 306
    :cond_5
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 309
    :pswitch_c
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 311
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_6

    .line 313
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 315
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v4, :cond_0

    .line 317
    const-string/jumbo v0, "."

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 320
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$300(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v0

    if-gez v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->canMinus:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-ne v0, v1, :cond_9

    .line 324
    :cond_8
    const-string/jumbo v0, "-"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 326
    :cond_9
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$3;->value:Ljava/lang/String;

    goto/16 :goto_0

    .line 266
    :pswitch_data_0
    .packed-switch 0x7f0b00f4
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_c
        :pswitch_1
        :pswitch_b
    .end packed-switch
.end method
