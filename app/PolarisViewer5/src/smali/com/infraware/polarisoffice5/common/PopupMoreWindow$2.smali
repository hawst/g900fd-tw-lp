.class Lcom/infraware/polarisoffice5/common/PopupMoreWindow$2;
.super Ljava/lang/Object;
.source "PopupMoreWindow.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/PopupMoreWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/PopupMoreWindow;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$2;->this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    .line 251
    const/4 v2, 0x4

    if-eq p2, v2, :cond_0

    const/16 v2, 0x6f

    if-ne p2, v2, :cond_2

    .line 253
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$2;->this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->dismiss()V

    .line 271
    :cond_1
    :goto_0
    return v1

    .line 255
    :cond_2
    const/16 v2, 0x14

    if-eq p2, v2, :cond_3

    const/16 v2, 0x13

    if-ne p2, v2, :cond_1

    .line 257
    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 259
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$2;->this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    if-ne p1, v2, :cond_1

    .line 261
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$2;->this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 262
    .local v0, "focusv":Landroid/view/View;
    if-eqz v0, :cond_4

    .line 264
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 266
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method
