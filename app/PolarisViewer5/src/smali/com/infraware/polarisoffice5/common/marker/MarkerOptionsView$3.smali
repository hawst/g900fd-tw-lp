.class Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;
.super Ljava/lang/Object;
.source "MarkerOptionsView.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 242
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # setter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I
    invoke-static {v0, p2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$002(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;I)I

    .line 243
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setThickness(I)V

    .line 244
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 239
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 237
    return-void
.end method
