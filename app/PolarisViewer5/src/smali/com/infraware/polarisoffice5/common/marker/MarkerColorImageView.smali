.class public Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
.super Landroid/widget/ImageView;
.source "MarkerColorImageView.java"


# static fields
.field public static final UPDATE_STATE:I = 0x1


# instance fields
.field private mBgImageRect:Landroid/graphics/Rect;

.field private mBgPress:Landroid/graphics/drawable/BitmapDrawable;

.field private mBgSelect:Landroid/graphics/drawable/BitmapDrawable;

.field private mBorderPaint:Landroid/graphics/Paint;

.field private mBorderThick:I

.field private mColor:I

.field private mForegroundNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

.field private mHandler:Landroid/os/Handler;

.field private mIcon:Landroid/graphics/drawable/BitmapDrawable;

.field private mIconImageRect:Landroid/graphics/Rect;

.field private mOutLine:Landroid/graphics/drawable/NinePatchDrawable;

.field private mPaint:Landroid/graphics/Paint;

.field private mbPress:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    .line 20
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgSelect:Landroid/graphics/drawable/BitmapDrawable;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgPress:Landroid/graphics/drawable/BitmapDrawable;

    .line 23
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIconImageRect:Landroid/graphics/Rect;

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgImageRect:Landroid/graphics/Rect;

    .line 26
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mColor:I

    .line 27
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mbPress:Z

    .line 29
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mForegroundNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    .line 30
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mOutLine:Landroid/graphics/drawable/NinePatchDrawable;

    .line 32
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 33
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    .line 36
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView$1;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mHandler:Landroid/os/Handler;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    .line 20
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgSelect:Landroid/graphics/drawable/BitmapDrawable;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgPress:Landroid/graphics/drawable/BitmapDrawable;

    .line 23
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIconImageRect:Landroid/graphics/Rect;

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgImageRect:Landroid/graphics/Rect;

    .line 26
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mColor:I

    .line 27
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mbPress:Z

    .line 29
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mForegroundNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    .line 30
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mOutLine:Landroid/graphics/drawable/NinePatchDrawable;

    .line 32
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 33
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    .line 36
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView$1;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mHandler:Landroid/os/Handler;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    .line 20
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgSelect:Landroid/graphics/drawable/BitmapDrawable;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgPress:Landroid/graphics/drawable/BitmapDrawable;

    .line 23
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIconImageRect:Landroid/graphics/Rect;

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBgImageRect:Landroid/graphics/Rect;

    .line 26
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mColor:I

    .line 27
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mbPress:Z

    .line 29
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mForegroundNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    .line 30
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mOutLine:Landroid/graphics/drawable/NinePatchDrawable;

    .line 32
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 33
    iput v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    .line 36
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView$1;-><init>(Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mHandler:Landroid/os/Handler;

    .line 55
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mColor:I

    return v0
.end method

.method public initResource(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mForegroundNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    .line 60
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mOutLine:Landroid/graphics/drawable/NinePatchDrawable;

    .line 62
    const/4 v0, 0x1

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    .line 64
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 65
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0501be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 69
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 72
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mColor:I

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 75
    return-void
.end method

.method public initResource(Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 80
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mForegroundNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    .line 81
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mOutLine:Landroid/graphics/drawable/NinePatchDrawable;

    .line 83
    const/4 v0, 0x1

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    .line 85
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    .line 86
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0501be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 88
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    .line 91
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 163
    new-instance v6, Landroid/graphics/Rect;

    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getHeight()I

    move-result v4

    iget v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    sub-int/2addr v4, v5

    invoke-direct {v6, v0, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 166
    .local v6, "fillRect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0, v9, v6, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 176
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getWidth()I

    move-result v0

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    sub-int/2addr v0, v2

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getHeight()I

    move-result v0

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderThick:I

    sub-int/2addr v0, v2

    int-to-float v4, v0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mBorderPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 179
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->isSelected()Z

    move-result v0

    if-eq v0, v8, :cond_2

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mbPress:Z

    if-ne v0, v8, :cond_3

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mForegroundNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v7, v7, v1, v2}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 181
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mForegroundNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 189
    :goto_0
    return-void

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mOutLine:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v7, v7, v1, v2}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mOutLine:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 123
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 141
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    .line 125
    :pswitch_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mbPress:Z

    .line 126
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->invalidate()V

    goto :goto_0

    .line 129
    :pswitch_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 130
    .local v0, "viewRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 131
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mbPress:Z

    .line 132
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->invalidate()V

    goto :goto_0

    .line 137
    .end local v0    # "viewRect":Landroid/graphics/Rect;
    :pswitch_2
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mbPress:Z

    .line 138
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->invalidate()V

    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mColor:I

    .line 95
    return-void
.end method

.method public setSelected(Z)V
    .locals 2
    .param p1, "selected"    # Z

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 104
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 105
    return-void
.end method
