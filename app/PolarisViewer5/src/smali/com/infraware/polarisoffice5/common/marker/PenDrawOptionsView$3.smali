.class Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;
.super Ljava/lang/Object;
.source "PenDrawOptionsView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 386
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 440
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 389
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070125

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 392
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07016d

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 395
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f0700e4

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 398
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f0701e2

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 403
    :pswitch_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07006c

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 406
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070074

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 409
    :pswitch_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070072

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 412
    :pswitch_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070071

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 415
    :pswitch_9
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07006d

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 418
    :pswitch_a
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070075

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 421
    :pswitch_b
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070076

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 424
    :pswitch_c
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070070

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 427
    :pswitch_d
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070077

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 430
    :pswitch_e
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07006f

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 433
    :pswitch_f
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07006e

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 436
    :pswitch_10
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070073

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b018b
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method
