.class Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;
.super Ljava/lang/Object;
.source "ColorPickerActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/ColorPickerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const v6, 0x7f070104

    const/16 v5, 0xff

    const/4 v4, 0x0

    .line 215
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mProtectTextWatcher:Z
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$300(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 221
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "value":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 223
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 224
    const-string/jumbo v1, "0"

    .line 225
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0

    .line 230
    :cond_2
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->safeParseInt(Ljava/lang/String;)I

    move-result v0

    .line 231
    .local v0, "nValue":I
    if-ltz v0, :cond_3

    if-gt v0, v5, :cond_3

    .line 232
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onChangeR(I)V

    goto :goto_0

    .line 235
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string/jumbo v3, "255"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 236
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/EditText;->setSelection(II)V

    .line 237
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onToastMessage(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$400(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 241
    .end local v0    # "nValue":I
    .end local v1    # "value":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 242
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 243
    .restart local v1    # "value":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 244
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 245
    const-string/jumbo v1, "0"

    .line 246
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/EditText;->setSelection(II)V

    goto/16 :goto_0

    .line 251
    :cond_5
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->safeParseInt(Ljava/lang/String;)I

    move-result v0

    .line 252
    .restart local v0    # "nValue":I
    if-ltz v0, :cond_6

    if-gt v0, v5, :cond_6

    .line 253
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onChangeG(I)V

    goto/16 :goto_0

    .line 256
    :cond_6
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string/jumbo v3, "255"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/EditText;->setSelection(II)V

    .line 258
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onToastMessage(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$400(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 262
    .end local v0    # "nValue":I
    .end local v1    # "value":Ljava/lang/String;
    :cond_7
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 263
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 264
    .restart local v1    # "value":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 265
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_8

    .line 266
    const-string/jumbo v1, "0"

    .line 267
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 268
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/EditText;->setSelection(II)V

    goto/16 :goto_0

    .line 272
    :cond_8
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->safeParseInt(Ljava/lang/String;)I

    move-result v0

    .line 273
    .restart local v0    # "nValue":I
    if-ltz v0, :cond_9

    if-gt v0, v5, :cond_9

    .line 274
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onChangeB(I)V

    goto/16 :goto_0

    .line 277
    :cond_9
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string/jumbo v3, "255"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/EditText;->setSelection(II)V

    .line 279
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;->this$0:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onToastMessage(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->access$400(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 209
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 202
    return-void
.end method
