.class public Lcom/infraware/polarisoffice5/common/ClearEditText;
.super Landroid/widget/EditText;
.source "ClearEditText.java"


# instance fields
.field private ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private mDrawables:Landroid/graphics/drawable/StateListDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 27
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->init()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 39
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->init()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 33
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->init()V

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/ClearEditText;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ClearEditText;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/ClearEditText;)Landroid/graphics/drawable/StateListDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ClearEditText;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method


# virtual methods
.method public AddParent(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p1, "ebva"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->ebva:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 44
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    .line 48
    return-void
.end method

.method handleClearButton()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 125
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/ClearEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 136
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v3

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/ClearEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method init()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 51
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    .line 52
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x10100a7

    aput v2, v1, v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02027c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 53
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    new-array v1, v4, [I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02027d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 56
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ClearEditText;->mDrawables:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    .line 60
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->handleClearButton()V

    .line 64
    new-instance v0, Lcom/infraware/polarisoffice5/common/ClearEditText$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ClearEditText$1;-><init>(Lcom/infraware/polarisoffice5/common/ClearEditText;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 89
    new-instance v0, Lcom/infraware/polarisoffice5/common/ClearEditText$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ClearEditText$2;-><init>(Lcom/infraware/polarisoffice5/common/ClearEditText;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 107
    return-void
.end method
