.class Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;
.super Ljava/lang/Object;
.source "VideoFrameLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->removeVideoCtrl()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V
    .locals 0

    .prologue
    .line 461
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 464
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$100(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 467
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$300(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Landroid/widget/VideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$300(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$700(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 470
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$700(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 474
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # setter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z
    invoke-static {v0, v2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$102(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;Z)Z

    .line 475
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    .line 477
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$600(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 483
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVisibility(I)V

    .line 485
    :cond_3
    return-void
.end method
