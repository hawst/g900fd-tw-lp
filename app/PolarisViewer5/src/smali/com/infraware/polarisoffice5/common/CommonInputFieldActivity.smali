.class public Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;
.super Landroid/app/Activity;
.source "CommonInputFieldActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;


# instance fields
.field editNameWatcher:Landroid/text/TextWatcher;

.field private mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

.field private mDocType:I

.field private mFieldType:I

.field private mInputField:Landroid/widget/EditText;

.field private mTitleResId:I

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mDocType:I

    .line 78
    new-instance v0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity$1;-><init>(Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->editNameWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;)Lcom/infraware/office/actionbar/ActionTitleBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    return-object v0
.end method


# virtual methods
.method public actionTitleBarButtonClick()V
    .locals 3

    .prologue
    .line 61
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 63
    .local v0, "result":Landroid/content/Intent;
    const-string/jumbo v1, "key_filed_type"

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mFieldType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    const-string/jumbo v1, "key_filed_set_value"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->setResult(ILandroid/content/Intent;)V

    .line 67
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->finish()V

    .line 68
    return-void
.end method

.method public editTextFilter()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 71
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mDocType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/16 v4, 0x1f

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 74
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->editNameWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f030012

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->setContentView(I)V

    .line 33
    new-instance v0, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f0b001d

    const/4 v2, 0x1

    const/16 v3, 0x32

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 34
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_filed_type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mFieldType:I

    .line 35
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_title_text"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mTitleResId:I

    .line 36
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_filed_get_value"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mValue:Ljava/lang/String;

    .line 37
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "docType"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mDocType:I

    .line 38
    const v0, 0x7f0b005d

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;

    .line 39
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mTitleResId:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 40
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonText(I)V

    .line 41
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mValue:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 45
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->editTextFilter()V

    .line 46
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 49
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 50
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 55
    const-string/jumbo v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 56
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 57
    return-void
.end method
