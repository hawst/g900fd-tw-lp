.class Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;
.super Ljava/lang/Object;
.source "CommonNumberInputActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 14
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v13, 0x6

    const/4 v9, 0x5

    const/high16 v10, 0x41200000    # 10.0f

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 416
    const/4 v5, 0x0

    .line 417
    .local v5, "isError":Z
    const/4 v4, 0x0

    .line 418
    .local v4, "intValue":I
    const/4 v3, 0x0

    .line 420
    .local v3, "floatValue":F
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$400(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/Button;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 422
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->length()I

    move-result v8

    if-ne v7, v8, :cond_8

    .line 424
    :cond_0
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    iput-boolean v12, v7, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->canMinus:Z

    .line 432
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v7

    if-eqz v7, :cond_6

    .line 434
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v6

    .line 437
    .local v6, "nNumLen":I
    const/4 v0, 0x0

    .line 438
    .local v0, "bCheckLimit":Z
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinLen:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$500(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-lt v6, v7, :cond_9

    .line 439
    const/4 v0, 0x1

    .line 445
    :cond_2
    :goto_1
    if-eqz v0, :cond_6

    .line 448
    :try_start_0
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-eq v7, v13, :cond_3

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-eq v7, v9, :cond_3

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    const/16 v8, 0x23

    if-ne v7, v8, :cond_a

    .line 449
    :cond_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 459
    :goto_2
    if-nez v5, :cond_6

    .line 461
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-eq v7, v13, :cond_4

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-eq v7, v9, :cond_4

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    const/16 v8, 0x23

    if-ne v7, v8, :cond_b

    .line 465
    :cond_4
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-lt v4, v7, :cond_5

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-le v4, v7, :cond_6

    .line 471
    :cond_5
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 472
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 473
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    const-string/jumbo v8, "%d"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 474
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    .line 475
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # invokes: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->warning()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$600(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)V

    .line 524
    .end local v0    # "bCheckLimit":Z
    .end local v6    # "nNumLen":I
    :cond_6
    :goto_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 525
    .local v1, "check":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_7

    const-string/jumbo v7, "^\\s*$"

    invoke-virtual {v1, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    const-string/jumbo v7, "^\\.*$"

    invoke-virtual {v1, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    const-string/jumbo v7, "^\\-*$"

    invoke-virtual {v1, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 526
    :cond_7
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v7, v11}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 529
    :goto_4
    return-void

    .line 428
    .end local v1    # "check":Ljava/lang/String;
    :cond_8
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    iput-boolean v11, v7, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->canMinus:Z

    goto/16 :goto_0

    .line 440
    .restart local v0    # "bCheckLimit":Z
    .restart local v6    # "nNumLen":I
    :cond_9
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    if-gez v7, :cond_2

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinLen:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$500(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne v6, v7, :cond_2

    .line 441
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 451
    :cond_a
    :try_start_1
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v7

    mul-float/2addr v7, v10

    float-to-int v7, v7

    int-to-float v3, v7

    goto/16 :goto_2

    .line 453
    :catch_0
    move-exception v2

    .line 455
    .local v2, "e":Ljava/lang/NumberFormatException;
    const/4 v5, 0x1

    .line 456
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # invokes: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->warning()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$600(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)V

    goto/16 :goto_2

    .line 482
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_b
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-ltz v7, :cond_c

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    int-to-float v7, v7

    cmpl-float v7, v3, v7

    if-lez v7, :cond_6

    .line 486
    :cond_c
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    int-to-float v7, v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 487
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I

    move-result v7

    int-to-float v7, v7

    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 488
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    const-string/jumbo v8, "%.1f"

    new-array v9, v12, [Ljava/lang/Object;

    div-float v10, v3, v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 489
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    .line 490
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    # invokes: Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->warning()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->access$600(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)V

    goto/16 :goto_3

    .line 527
    .end local v0    # "bCheckLimit":Z
    .end local v6    # "nNumLen":I
    .restart local v1    # "check":Ljava/lang/String;
    :cond_d
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v7, v12}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    goto/16 :goto_4
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 411
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 407
    return-void
.end method
