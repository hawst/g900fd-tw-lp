.class public Lcom/infraware/polarisoffice5/common/PopupMoreWindow;
.super Landroid/widget/PopupWindow;
.source "PopupMoreWindow.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_CURSOR_MODE;
.implements Lcom/infraware/office/evengine/E$EV_EDIT_OBJECT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_STATUS;


# static fields
.field static final INLINEPOPUP_GAB:I = 0xa

.field static final INLINEPOPUP_MAX_ITEM_SIZE:I = 0xf

.field static final INLINEPOPUP_SHOWING_TIME:I = 0x2710

.field static final INLINEPOPUP_WAIT_TIME:I = 0x12c

.field static final MORE_CMDID_ADD_CONTACT:I = 0x1c

.field static final MORE_CMDID_ANIMATION:I = 0x47

.field static final MORE_CMDID_AUTOFIT_COLUMNS_WIDTH:I = 0x35

.field static final MORE_CMDID_AUTOFIT_ROW_HEIGHT:I = 0x38

.field static final MORE_CMDID_CALL:I = 0x1a

.field static final MORE_CMDID_CONDITIONAL_FORMAT:I = 0x41

.field static final MORE_CMDID_COPY_FORMAT:I = 0x43

.field static final MORE_CMDID_CREATE_HIGHLIGHT:I = 0x3f

.field static final MORE_CMDID_CREATE_STRIKEOUT:I = 0x42

.field static final MORE_CMDID_CREATE_UNDERLINE:I = 0x40

.field static final MORE_CMDID_DICTIONARY_SEARCH:I = 0x3d

.field static final MORE_CMDID_EDIT_ANNOT:I = 0x52

.field static final MORE_CMDID_FILTER:I = 0x31

.field static final MORE_CMDID_GOOGLE_SEARCH:I = 0x1f

.field static final MORE_CMDID_HIDE_COLUMNS:I = 0x34

.field static final MORE_CMDID_HIDE_ROWS:I = 0x37

.field static final MORE_CMDID_PROPERTIES_ANNOT:I = 0x53

.field static final MORE_CMDID_RUN_HYPERLINK:I = 0x17

.field static final MORE_CMDID_RUN_SHEET_HYPERLINK:I = 0x3e

.field static final MORE_CMDID_SEARCH_MENU:I = 0x50

.field static final MORE_CMDID_SELECT_ALL_CELLS:I = 0x27

.field static final MORE_CMDID_SELECT_COLUMNS:I = 0x2b

.field static final MORE_CMDID_SELECT_ROWS:I = 0x2a

.field static final MORE_CMDID_SHARE:I = 0x1e

.field static final MORE_CMDID_SMS:I = 0x1b

.field static final MORE_CMDID_SPELL_CHECK:I = 0x44

.field static final MORE_CMDID_TTS:I = 0x1d

.field static final MORE_CMDID_UNHIDE_COLUMNS:I = 0x3a

.field static final MORE_CMDID_UNHIDE_ROWS:I = 0x3b

.field static final MORE_CMDID_VIEW_MEMO:I = 0x57

.field static final MORE_CMDID_WIKI_SEARCH:I = 0x20

.field static final POPUP_CMDID_CELLMARK:I = 0x56


# instance fields
.field context:Landroid/content/Context;

.field hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

.field private isHyperlink:Z

.field mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field protected mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

.field mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

.field mCmdArray:[I

.field mDisableArray:[Z

.field private mDocType:I

.field mExteranlCall:Lcom/infraware/polarisoffice5/common/ExternalCallManager;

.field mInterface:Lcom/infraware/office/evengine/EvInterface;

.field mItemSize:I

.field mLinearLayout:Landroid/widget/LinearLayout;

.field private mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

.field private mOnKeyListener:Landroid/view/View$OnKeyListener;

.field mRootFrame:Landroid/widget/FrameLayout;

.field mScrollView:Landroid/widget/ScrollView;

.field mSheetEditorActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

.field mViewArray:[Landroid/view/View;

.field private motionEventRect:Landroid/graphics/Rect;

.field sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 7
    .param p1, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    const/16 v1, 0xf

    const/4 v2, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 154
    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 126
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCmdArray:[I

    .line 127
    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDisableArray:[Z

    .line 128
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mViewArray:[Landroid/view/View;

    .line 130
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 131
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 132
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 134
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    .line 135
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 136
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    .line 138
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .line 140
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 142
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mExteranlCall:Lcom/infraware/polarisoffice5/common/ExternalCallManager;

    .line 143
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 144
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 147
    iput v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    .line 149
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->context:Landroid/content/Context;

    .line 150
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .line 151
    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mSheetEditorActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 248
    new-instance v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$2;-><init>(Lcom/infraware/polarisoffice5/common/PopupMoreWindow;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 157
    invoke-virtual {p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    .line 159
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 161
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mSheetEditorActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 164
    :cond_0
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    .line 166
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    .line 167
    invoke-virtual {p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getClipboardhelper()Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 168
    :cond_1
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 170
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setOutsideTouchable(Z)V

    .line 171
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setFocusable(Z)V

    .line 174
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->context:Landroid/content/Context;

    .line 176
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    .line 177
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 178
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 180
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 181
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 182
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 185
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    .line 186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 188
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$1;-><init>(Lcom/infraware/polarisoffice5/common/PopupMoreWindow;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 204
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 205
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    const v1, 0x7f020122

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setBackgroundResource(I)V

    .line 207
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setOutsideTouchable(Z)V

    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 211
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 217
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setContentView(Landroid/view/View;)V

    .line 219
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    .line 220
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocusFromTouch()Z

    .line 221
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 223
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 227
    return-void
.end method

.method private addButtonofText()V
    .locals 5

    .prologue
    .line 1544
    iget v3, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 1575
    :cond_0
    :goto_0
    return-void

    .line 1547
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v0

    .line 1548
    .local v0, "boi":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    iget v2, v0, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    .line 1549
    .local v2, "nStatus":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPCellStatusInfo()I

    move-result v1

    .line 1552
    .local v1, "nCellStatus":I
    and-int/lit8 v3, v1, 0x20

    if-eqz v3, :cond_0

    .line 1553
    const/16 v3, 0x56

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto :goto_0
.end method

.method private checkEncryptDocCmd(I)Z
    .locals 2
    .param p1, "cmd_id"    # I

    .prologue
    const/4 v0, 0x1

    .line 1061
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getIsEncryptDoc()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 1062
    sparse-switch p1, :sswitch_data_0

    .line 1116
    :cond_0
    :goto_0
    return v0

    .line 1113
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1062
    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x34 -> :sswitch_0
        0x35 -> :sswitch_0
        0x37 -> :sswitch_0
        0x38 -> :sswitch_0
        0x3a -> :sswitch_0
        0x3b -> :sswitch_0
        0x3e -> :sswitch_0
        0x57 -> :sswitch_0
    .end sparse-switch
.end method

.method private excuteHyperlinkButton()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1040
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    if-eqz v0, :cond_0

    .line 1041
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    sparse-switch v0, :sswitch_data_0

    .line 1057
    :cond_0
    :goto_0
    return-void

    .line 1043
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runEmail(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 1046
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runHyperlink(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 1050
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runSMS(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 1053
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x2e

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_0

    .line 1041
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0xf -> :sswitch_0
        0x1f -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method private getResString(I)Ljava/lang/String;
    .locals 4
    .param p1, "cmd_id"    # I

    .prologue
    const v3, 0x7f0701e3

    .line 1578
    const-string/jumbo v1, ""

    .line 1580
    .local v1, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1582
    .local v0, "r":Landroid/content/res/Resources;
    packed-switch p1, :pswitch_data_0

    .line 1770
    :goto_0
    :pswitch_0
    return-object v1

    .line 1585
    :pswitch_1
    const v2, 0x7f07018f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1586
    goto :goto_0

    .line 1594
    :pswitch_2
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1595
    goto :goto_0

    .line 1597
    :pswitch_3
    const v2, 0x7f0700e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1598
    goto :goto_0

    .line 1600
    :pswitch_4
    const v2, 0x7f070224

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1601
    goto :goto_0

    .line 1603
    :pswitch_5
    const v2, 0x7f0700d0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1604
    goto :goto_0

    .line 1606
    :pswitch_6
    const v2, 0x7f070310

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1607
    goto :goto_0

    .line 1609
    :pswitch_7
    const v2, 0x7f070200

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1610
    goto :goto_0

    .line 1612
    :pswitch_8
    const v2, 0x7f0702c7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1613
    goto :goto_0

    .line 1615
    :pswitch_9
    const v2, 0x7f070168

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1616
    goto :goto_0

    .line 1618
    :pswitch_a
    const v2, 0x7f070252

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1619
    goto :goto_0

    .line 1621
    :pswitch_b
    const v2, 0x7f070115

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1622
    goto :goto_0

    .line 1625
    :pswitch_c
    const-string/jumbo v1, "Highlight"

    .line 1626
    goto :goto_0

    .line 1628
    :pswitch_d
    const-string/jumbo v1, "Underline"

    .line 1629
    goto :goto_0

    .line 1631
    :pswitch_e
    const-string/jumbo v1, "Strikeout"

    .line 1632
    goto :goto_0

    .line 1653
    :pswitch_f
    const v2, 0x7f0701ed

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1654
    goto :goto_0

    .line 1662
    :pswitch_10
    const v2, 0x7f0701f0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1663
    goto :goto_0

    .line 1665
    :pswitch_11
    const v2, 0x7f0701ef

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1666
    goto :goto_0

    .line 1677
    :pswitch_12
    const v2, 0x7f07010b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1678
    goto/16 :goto_0

    .line 1680
    :pswitch_13
    const v2, 0x7f07022d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1681
    goto/16 :goto_0

    .line 1695
    :pswitch_14
    const v2, 0x7f070134

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1696
    goto/16 :goto_0

    .line 1704
    :pswitch_15
    const v2, 0x7f07016a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1705
    goto/16 :goto_0

    .line 1707
    :pswitch_16
    const v2, 0x7f0700da

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1708
    goto/16 :goto_0

    .line 1713
    :pswitch_17
    const v2, 0x7f07016b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1714
    goto/16 :goto_0

    .line 1716
    :pswitch_18
    const v2, 0x7f0700db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1717
    goto/16 :goto_0

    .line 1722
    :pswitch_19
    const v2, 0x7f070247

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1723
    goto/16 :goto_0

    .line 1725
    :pswitch_1a
    const v2, 0x7f070248

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1726
    goto/16 :goto_0

    .line 1732
    :pswitch_1b
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1733
    goto/16 :goto_0

    .line 1736
    :pswitch_1c
    const v2, 0x7f070109

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1737
    goto/16 :goto_0

    .line 1747
    :pswitch_1d
    const v2, 0x7f0700d4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1748
    goto/16 :goto_0

    .line 1761
    :pswitch_1e
    const v2, 0x7f0702c6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1762
    goto/16 :goto_0

    .line 1764
    :pswitch_1f
    const v2, 0x7f0702c5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1765
    goto/16 :goto_0

    .line 1767
    :pswitch_20
    const v2, 0x7f0702c8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1582
    nop

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_b
        :pswitch_1b
        :pswitch_c
        :pswitch_d
        :pswitch_1c
        :pswitch_e
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_1d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
        :pswitch_0
        :pswitch_0
        :pswitch_20
        :pswitch_1
    .end packed-switch
.end method

.method private resizePixelByWidth(II)I
    .locals 1
    .param p1, "width"    # I
    .param p2, "pixel"    # I

    .prologue
    .line 1788
    mul-int v0, p2, p1

    div-int/lit16 v0, v0, 0x2d0

    return v0
.end method


# virtual methods
.method public AddButton(I)V
    .locals 4
    .param p1, "cmd_id"    # I

    .prologue
    .line 1121
    const/16 v1, 0x50

    if-ne p1, v1, :cond_2

    .line 1122
    const-string/jumbo v1, "android.intent.action.WEB_SEARCH"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v1, v2}, Lcom/infraware/common/util/Utils;->isIntentSafe(Ljava/lang/String;Landroid/app/Activity;)Z

    move-result v0

    .line 1124
    .local v0, "isPossibeSearch":Z
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1147
    .end local v0    # "isPossibeSearch":Z
    :cond_0
    :goto_0
    return-void

    .line 1126
    .restart local v0    # "isPossibeSearch":Z
    :cond_1
    if-nez v0, :cond_2

    .line 1127
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->isLoadComplete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1132
    .end local v0    # "isPossibeSearch":Z
    :cond_2
    const/16 v1, 0x17

    if-ne p1, v1, :cond_3

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HYPERLINK()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1135
    :cond_3
    const/16 v1, 0x47

    if-eq p1, v1, :cond_0

    .line 1138
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->checkEncryptDocCmd(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1141
    iget v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    const/16 v2, 0xf

    if-eq v1, v2, :cond_0

    .line 1144
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCmdArray:[I

    iget v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    aput p1, v1, v2

    .line 1145
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDisableArray:[Z

    iget v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    const/4 v3, 0x0

    aput-boolean v3, v1, v2

    .line 1146
    iget v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    goto :goto_0
.end method

.method public AddButton(IZ)V
    .locals 2
    .param p1, "cmd_id"    # I
    .param p2, "bDisable"    # Z

    .prologue
    .line 1151
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->checkEncryptDocCmd(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1160
    :cond_0
    :goto_0
    return-void

    .line 1154
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    .line 1157
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCmdArray:[I

    iget v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    aput p1, v0, v1

    .line 1158
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDisableArray:[Z

    iget v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    aput-boolean p2, v0, v1

    .line 1159
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    goto :goto_0
.end method

.method protected Excute(I)V
    .locals 12
    .param p1, "cmd_id"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x5

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 1168
    packed-switch p1, :pswitch_data_0

    .line 1522
    :cond_0
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->InitItem()V

    .line 1523
    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1525
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v7

    .line 1526
    .local v7, "ev":Lcom/infraware/office/baseframe/EvBaseView;
    if-eqz v7, :cond_1

    .line 1528
    iget-object v0, v7, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1529
    iget-object v0, v7, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismiss()V

    .line 1532
    :cond_1
    return-void

    .line 1172
    .end local v7    # "ev":Lcom/infraware/office/baseframe/EvBaseView;
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_0

    .line 1192
    :pswitch_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HYPERLINK()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1196
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    .line 1197
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v1, :cond_2

    .line 1198
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->excuteHyperlinkButton()V

    goto :goto_0

    .line 1200
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    iget-object v8, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    .line 1201
    .local v8, "hyperLink":Ljava/lang/String;
    const-string/jumbo v0, "HTTP"

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "Http"

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1203
    :cond_3
    const-string/jumbo v0, "H(TTP|ttp)"

    const-string/jumbo v1, "http"

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1206
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0, v8}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runHyperlink(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 1216
    .end local v8    # "hyperLink":Ljava/lang/String;
    :pswitch_3
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    .line 1217
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v1, :cond_5

    .line 1218
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->excuteHyperlinkButton()V

    goto :goto_0

    .line 1220
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runCall(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1227
    :pswitch_4
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    .line 1228
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v1, :cond_6

    .line 1229
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->excuteHyperlinkButton()V

    goto/16 :goto_0

    .line 1231
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runSMS(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1238
    :pswitch_5
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    .line 1239
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v1, :cond_7

    .line 1240
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->excuteHyperlinkButton()V

    goto/16 :goto_0

    .line 1242
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->run_addContact(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1248
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1252
    :pswitch_7
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v3, :cond_8

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_8

    .line 1253
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runShare()V

    goto/16 :goto_0

    .line 1256
    :cond_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    .line 1257
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->runShare()V

    goto/16 :goto_0

    .line 1260
    :pswitch_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnSearchMenu()V

    goto/16 :goto_0

    .line 1263
    :pswitch_9
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v3, :cond_9

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_9

    .line 1264
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runGoogleSearch()V

    goto/16 :goto_0

    .line 1267
    :cond_9
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    .line 1268
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->runGoogleSearch()V

    goto/16 :goto_0

    .line 1271
    :pswitch_a
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v3, :cond_a

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_a

    .line 1272
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runWikipediaSearch()V

    goto/16 :goto_0

    .line 1275
    :cond_a
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    .line 1276
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->runWikipediaSearch(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    goto/16 :goto_0

    .line 1279
    :pswitch_b
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v3, :cond_b

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_b

    .line 1280
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runDictionarySearch()V

    goto/16 :goto_0

    .line 1283
    :cond_b
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    .line 1284
    const-string/jumbo v10, ""

    .line 1285
    .local v10, "select":Ljava/lang/String;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v1, :cond_c

    .line 1286
    iget-object v11, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v11, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 1287
    .local v11, "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellText()Ljava/lang/String;

    move-result-object v10

    .line 1292
    .end local v11    # "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    .line 1293
    .local v9, "msg":Landroid/os/Message;
    const/16 v0, 0x378

    iput v0, v9, Landroid/os/Message;->what:I

    .line 1294
    iput-object v10, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1295
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->messageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1289
    .end local v9    # "msg":Landroid/os/Message;
    :cond_c
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    .line 1300
    .end local v10    # "select":Ljava/lang/String;
    :pswitch_c
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v3, :cond_d

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_d

    .line 1301
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runCreateHighLight()V

    goto/16 :goto_0

    .line 1305
    :cond_d
    :pswitch_d
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v3, :cond_e

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_e

    .line 1306
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runCreateUnderline()V

    goto/16 :goto_0

    .line 1310
    :cond_e
    :pswitch_e
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v3, :cond_f

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_f

    .line 1311
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->runCreateStrikeout()V

    goto/16 :goto_0

    .line 1339
    :cond_f
    :pswitch_f
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1347
    :pswitch_10
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1351
    :pswitch_11
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1383
    :pswitch_12
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    if-ne v0, v1, :cond_0

    .line 1384
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v2, v1}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    .line 1386
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mSheetEditorActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->setFormatCopied(Z)V

    goto/16 :goto_0

    .line 1443
    :pswitch_13
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1447
    :pswitch_14
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1455
    :pswitch_15
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1459
    :pswitch_16
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1467
    :pswitch_17
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1471
    :pswitch_18
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1483
    :pswitch_19
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->excuteHyperlinkButton()V

    goto/16 :goto_0

    .line 1486
    :pswitch_1a
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1495
    :pswitch_1b
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCallbackListener:Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;

    const/16 v1, 0x35

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/infraware/office/baseframe/gestureproc/EvGestureCallback;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_0

    .line 1512
    :pswitch_1c
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->dismiss()V

    .line 1513
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->printAnnot()V

    goto/16 :goto_0

    .line 1516
    :pswitch_1d
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ICaretMark(II)V

    .line 1517
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1518
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 1168
    nop

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_b
        :pswitch_19
        :pswitch_c
        :pswitch_d
        :pswitch_1a
        :pswitch_e
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_1c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1d
        :pswitch_1
    .end packed-switch
.end method

.method public InitItem()V
    .locals 1

    .prologue
    .line 1163
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    .line 1164
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1165
    return-void
.end method

.method public dismiss()V
    .locals 0

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->InitItem()V

    .line 1538
    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1540
    return-void
.end method

.method public finalizePopupMoreWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 232
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->removeAllViews()V

    .line 233
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 234
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 235
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    .line 236
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mRootFrame:Landroid/widget/FrameLayout;

    .line 238
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 239
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mClipboard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 240
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 242
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCmdArray:[I

    .line 243
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .line 245
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 246
    return-void
.end method

.method public preProcess(Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;I)V
    .locals 29
    .param p1, "objproc"    # Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;
    .param p2, "pdfAnnotType"    # I

    .prologue
    .line 284
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setWidth(I)V

    .line 285
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setHeight(I)V

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    if-nez v25, :cond_1

    .line 995
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    .line 292
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isShowing()Z

    move-result v25

    if-eqz v25, :cond_2

    .line 293
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->dismiss()V

    .line 298
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->InitItem()V

    .line 301
    const/16 v17, 0x0

    .line 303
    .local v17, "mCellStatus":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_9

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_8

    .line 306
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 318
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPGrapAttrInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;

    move-result-object v12

    .line 320
    .local v12, "graphicAtt":Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getObjectType()I

    move-result v25

    packed-switch v25, :pswitch_data_0

    .line 854
    :cond_3
    :goto_2
    :pswitch_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x5

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_4

    .line 856
    const/16 v25, 0x1

    move/from16 v0, p2

    move/from16 v1, v25

    if-ne v0, v1, :cond_36

    .line 858
    const/16 v25, 0x52

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 873
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    move/from16 v25, v0

    if-eqz v25, :cond_0

    .line 876
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 878
    .local v10, "context":Landroid/content/Context;
    const/4 v13, 0x0

    .line 886
    .local v13, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v25

    move-object/from16 v0, v25

    iget v11, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 887
    .local v11, "getPhoneWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_5

    .line 888
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v25

    move-object/from16 v0, v25

    iget v11, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 890
    :cond_5
    const/16 v25, 0x1b4

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v11, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->resizePixelByWidth(II)I

    move-result v6

    .line 891
    .local v6, "MORE_POPUP_MAXWIDTH":I
    const/16 v25, 0x54

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v11, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->resizePixelByWidth(II)I

    move-result v3

    .line 892
    .local v3, "ITEM_MINHEIGHT":I
    const/16 v25, 0x78

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v11, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->resizePixelByWidth(II)I

    move-result v2

    .line 893
    .local v2, "ITEM_MAXHEIGHT":I
    const/16 v25, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v11, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->resizePixelByWidth(II)I

    move-result v4

    .line 894
    .local v4, "ITEM_PADDING_LR":I
    const/16 v25, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v11, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->resizePixelByWidth(II)I

    move-result v5

    .line 897
    .local v5, "ITEM_PADDING_TB":I
    const/4 v13, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-ge v13, v0, :cond_38

    .line 898
    if-lez v13, :cond_6

    .line 899
    new-instance v14, Landroid/view/View;

    invoke-direct {v14, v10}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 900
    .local v14, "iv":Landroid/view/View;
    const v25, -0xdba492

    move/from16 v0, v25

    invoke-virtual {v14, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 904
    new-instance v25, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v26, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v27, v0

    const/high16 v28, 0x3f000000    # 0.5f

    invoke-static/range {v27 .. v28}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v27

    invoke-direct/range {v25 .. v27}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 906
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 909
    .end local v14    # "iv":Landroid/view/View;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCmdArray:[I

    move-object/from16 v25, v0

    aget v25, v25, v13

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getResString(I)Ljava/lang/String;

    move-result-object v23

    .line 910
    .local v23, "str":Ljava/lang/String;
    new-instance v7, Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-direct {v7, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 912
    .local v7, "b":Landroid/widget/Button;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mViewArray:[Landroid/view/View;

    move-object/from16 v25, v0

    aput-object v7, v25, v13

    .line 913
    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 914
    const/16 v25, 0x1

    const/high16 v26, 0x41600000    # 14.0f

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v7, v0, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 919
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mCmdArray:[I

    move-object/from16 v25, v0

    aget v25, v25, v13

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setId(I)V

    .line 920
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 922
    const v25, 0x7f020010

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 923
    const/16 v25, -0x1

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 926
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDisableArray:[Z

    move-object/from16 v25, v0

    aget-boolean v25, v25, v13

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_7

    .line 928
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 929
    const v25, -0x777778

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 933
    :cond_7
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setSingleLine(Z)V

    .line 934
    invoke-virtual {v7, v4, v5, v4, v5}, Landroid/widget/Button;->setPadding(IIII)V

    .line 935
    invoke-virtual {v7, v6}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 943
    new-instance v25, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v26, -0x1

    const/16 v27, -0x2

    invoke-direct/range {v25 .. v27}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 946
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setClickable(Z)V

    .line 948
    const/16 v25, 0x13

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setGravity(I)V

    .line 952
    new-instance v25, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$3;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$3;-><init>(Lcom/infraware/polarisoffice5/common/PopupMoreWindow;)V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 958
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 897
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_4

    .line 308
    .end local v2    # "ITEM_MAXHEIGHT":I
    .end local v3    # "ITEM_MINHEIGHT":I
    .end local v4    # "ITEM_PADDING_LR":I
    .end local v5    # "ITEM_PADDING_TB":I
    .end local v6    # "MORE_POPUP_MAXWIDTH":I
    .end local v7    # "b":Landroid/widget/Button;
    .end local v10    # "context":Landroid/content/Context;
    .end local v11    # "getPhoneWidth":I
    .end local v12    # "graphicAtt":Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    .end local v13    # "i":I
    .end local v23    # "str":Ljava/lang/String;
    :cond_8
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    goto/16 :goto_1

    .line 311
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    move/from16 v25, v0

    if-nez v25, :cond_b

    .line 313
    :cond_a
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    goto/16 :goto_1

    .line 315
    :cond_b
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    goto/16 :goto_1

    .line 323
    .restart local v12    # "graphicAtt":Lcom/infraware/office/evengine/EV$BWP_GRAP_ATTR_INFO;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_d

    .line 325
    const/16 v25, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 326
    const/16 v25, 0x50

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 389
    :cond_c
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMotionEventRect()Landroid/graphics/Rect;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->motionEventRect:Landroid/graphics/Rect;

    goto/16 :goto_2

    .line 330
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x5

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_e

    .line 331
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->addButtonofText()V

    .line 332
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    move/from16 v25, v0

    if-eqz v25, :cond_11

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-nez v25, :cond_f

    .line 334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 336
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 338
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_11

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "http"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "Http"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "HTTP"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "mailto:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 343
    :cond_10
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v25

    if-nez v25, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bAutoHyper:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x5

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_11

    .line 356
    :cond_11
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_15

    .line 358
    :cond_12
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v24

    .line 359
    .local v24, "strText":Ljava/lang/String;
    if-eqz v24, :cond_c

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v25

    if-lez v25, :cond_c

    .line 360
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v25

    if-nez v25, :cond_13

    .line 362
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->supportFocusTTS()Z

    move-result v9

    .line 364
    .local v9, "bSupportFocus":Z
    if-eqz v9, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/infraware/common/util/Utils;->isActiveTTS(Landroid/content/Context;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 365
    const/16 v25, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 368
    .end local v9    # "bSupportFocus":Z
    :cond_13
    const/16 v25, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 369
    const/16 v25, 0x50

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_5

    .line 348
    .end local v24    # "strText":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "tel:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_11

    .line 349
    const/16 v25, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 350
    const/16 v25, 0x1b

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 351
    const/16 v25, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_6

    .line 376
    :cond_15
    const/16 v25, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 377
    const/16 v25, 0x50

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_5

    .line 395
    :pswitch_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_16

    .line 396
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-nez v25, :cond_3

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 400
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    goto/16 :goto_2

    .line 409
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x5

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_3

    .line 411
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_17

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    move/from16 v25, v0

    if-nez v25, :cond_17

    .line 413
    const/4 v8, 0x1

    .line 414
    .local v8, "bMulti":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_17

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetTextWrapType()I

    move-result v18

    .line 416
    .local v18, "nType":I
    const/16 v25, 0x1

    move/from16 v0, v18

    move/from16 v1, v25

    if-ne v0, v1, :cond_17

    .line 426
    .end local v8    # "bMulti":Z
    .end local v18    # "nType":I
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_18

    .line 427
    const/16 v25, 0x47

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 430
    :cond_18
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 431
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-nez v25, :cond_19

    .line 432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 434
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 436
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "http"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "Http"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "HTTP"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "mailto:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 442
    :cond_1a
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 443
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v25

    if-nez v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bAutoHyper:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_3

    goto/16 :goto_2

    .line 462
    :pswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_1b

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    move/from16 v25, v0

    if-nez v25, :cond_1b

    .line 464
    const/4 v8, 0x1

    .line 465
    .restart local v8    # "bMulti":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1b

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetTextWrapType()I

    move-result v18

    .line 467
    .restart local v18    # "nType":I
    const/16 v25, 0x1

    move/from16 v0, v18

    move/from16 v1, v25

    if-ne v0, v1, :cond_1b

    .line 477
    .end local v8    # "bMulti":Z
    .end local v18    # "nType":I
    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 478
    const/16 v25, 0x47

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 488
    :pswitch_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x5

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_1c

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    move/from16 v25, v0

    if-nez v25, :cond_1c

    .line 491
    const/4 v8, 0x1

    .line 492
    .restart local v8    # "bMulti":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1c

    .line 493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetTextWrapType()I

    move-result v18

    .line 494
    .restart local v18    # "nType":I
    const/16 v25, 0x1

    move/from16 v0, v18

    move/from16 v1, v25

    if-ne v0, v1, :cond_1c

    .line 504
    .end local v8    # "bMulti":Z
    .end local v18    # "nType":I
    :cond_1c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_3

    goto/16 :goto_2

    .line 520
    :pswitch_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_1d

    .line 521
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    move/from16 v25, v0

    if-nez v25, :cond_1d

    .line 522
    const/4 v8, 0x1

    .line 523
    .restart local v8    # "bMulti":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1d

    .line 524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetTextWrapType()I

    move-result v18

    .line 525
    .restart local v18    # "nType":I
    const/16 v25, 0x1

    move/from16 v0, v18

    move/from16 v1, v25

    if-ne v0, v1, :cond_1d

    .line 544
    .end local v8    # "bMulti":Z
    .end local v18    # "nType":I
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1e

    .line 545
    const/16 v25, 0x47

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 551
    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 552
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 558
    :pswitch_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 559
    const/16 v25, 0x47

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 564
    :pswitch_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_23

    .line 566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    move/from16 v25, v0

    if-nez v25, :cond_1f

    .line 567
    const/4 v8, 0x1

    .line 568
    .restart local v8    # "bMulti":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1f

    .line 569
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetTextWrapType()I

    move-result v18

    .line 570
    .restart local v18    # "nType":I
    const/16 v25, 0x1

    move/from16 v0, v18

    move/from16 v1, v25

    if-ne v0, v1, :cond_1f

    .line 579
    .end local v8    # "bMulti":Z
    .end local v18    # "nType":I
    :cond_1f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_20

    .line 580
    const/16 v25, 0x47

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 583
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-nez v25, :cond_21

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 587
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 589
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "http"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "Http"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "HTTP"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "mailto:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 595
    :cond_22
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 611
    :cond_23
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-nez v25, :cond_24

    .line 613
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 615
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 618
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 620
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 626
    :pswitch_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_25

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvEditGestureProc;->mIsMultiSelectionMode:Z

    move/from16 v25, v0

    if-nez v25, :cond_25

    .line 628
    const/4 v8, 0x1

    .line 629
    .restart local v8    # "bMulti":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_25

    .line 630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetTextWrapType()I

    move-result v18

    .line 631
    .restart local v18    # "nType":I
    const/16 v25, 0x1

    move/from16 v0, v18

    move/from16 v1, v25

    if-ne v0, v1, :cond_25

    .line 641
    .end local v8    # "bMulti":Z
    .end local v18    # "nType":I
    :cond_25
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_26

    .line 642
    const/16 v25, 0x47

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 648
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPCellStatusInfo()I

    move-result v17

    .line 649
    move/from16 v0, v17

    and-int/lit16 v0, v0, 0x400

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 650
    const/16 v25, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 653
    :pswitch_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_2c

    .line 654
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v25

    if-nez v25, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v25

    if-eqz v25, :cond_3

    .line 655
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v25

    if-nez v25, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleMergeCell()Z

    move-result v25

    if-eqz v25, :cond_2a

    .line 657
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-nez v25, :cond_29

    .line 658
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 660
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 668
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsMemo()Z

    move-result v25

    if-eqz v25, :cond_2a

    .line 669
    const/16 v25, 0x57

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 681
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v25

    if-nez v25, :cond_2b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleMergeCell()Z

    move-result v25

    if-eqz v25, :cond_3

    .line 682
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellText()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_3

    .line 683
    const/16 v25, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 684
    const/16 v25, 0x50

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 693
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPCellStatusInfo()I

    move-result v17

    .line 694
    and-int/lit8 v25, v17, 0x20

    if-eqz v25, :cond_3

    .line 697
    const/16 v25, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 698
    const/16 v25, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 699
    const/16 v25, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 704
    :pswitch_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_2d

    .line 707
    const/16 v25, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 715
    const/16 v25, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 716
    const/16 v25, 0x3a

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 719
    const/16 v25, 0x37

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 720
    const/16 v25, 0x3b

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 721
    const/16 v25, 0x38

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 726
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPCellStatusInfo()I

    move-result v17

    .line 739
    const/16 v25, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 740
    const/16 v25, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 742
    const/16 v25, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 757
    :pswitch_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_2e

    .line 758
    const/16 v25, 0x47

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 768
    :cond_2e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_30

    .line 769
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 770
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-nez v25, :cond_2f

    .line 771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetHyperLinkInfo()Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .line 773
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 776
    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->sheetHyperlinkInfo:Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->nType:I

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 778
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 783
    :cond_30
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 784
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 786
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 795
    :pswitch_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_31

    .line 797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v26

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    move-object/from16 v26, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v25

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    check-cast v25, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellText()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-lez v25, :cond_3

    .line 800
    const/16 v25, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 801
    const/16 v25, 0x50

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 806
    :cond_31
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x5

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_32

    .line 807
    invoke-direct/range {p0 .. p0}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->addButtonofText()V

    .line 809
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mObjectProc:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getRectInfo()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    .line 812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getMotionEventRect()Landroid/graphics/Rect;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->motionEventRect:Landroid/graphics/Rect;

    .line 814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-nez v25, :cond_33

    .line 815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/infraware/office/evengine/EvInterface;->IGetHyperLinkInfo_Editor()Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .line 816
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->isHyperlink:Z

    .line 818
    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->bUse:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 819
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-lez v25, :cond_3

    .line 821
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "http"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "Http"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "HTTP"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "mailto:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_35

    .line 826
    :cond_34
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 831
    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->hyperlinkInfo:Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string/jumbo v26, "tel:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 832
    const/16 v25, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 833
    const/16 v25, 0x1b

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 834
    const/16 v25, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 842
    :pswitch_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mDocType:I

    move/from16 v25, v0

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 843
    const/16 v25, 0x47

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_2

    .line 860
    :cond_36
    const/16 v25, 0x2

    move/from16 v0, p2

    move/from16 v1, v25

    if-ne v0, v1, :cond_37

    .line 862
    const/16 v25, 0x52

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    .line 863
    const/16 v25, 0x53

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_3

    .line 865
    :cond_37
    const/16 v25, 0x3

    move/from16 v0, p2

    move/from16 v1, v25

    if-ne v0, v1, :cond_4

    .line 867
    const/16 v25, 0x53

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->AddButton(I)V

    goto/16 :goto_3

    .line 966
    .restart local v2    # "ITEM_MAXHEIGHT":I
    .restart local v3    # "ITEM_MINHEIGHT":I
    .restart local v4    # "ITEM_PADDING_LR":I
    .restart local v5    # "ITEM_PADDING_TB":I
    .restart local v6    # "MORE_POPUP_MAXWIDTH":I
    .restart local v10    # "context":Landroid/content/Context;
    .restart local v11    # "getPhoneWidth":I
    .restart local v13    # "i":I
    :cond_38
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/widget/LinearLayout;->measure(II)V

    .line 968
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v16

    .line 969
    .local v16, "linear_width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v15

    .line 971
    .local v15, "linear_height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/ScrollView;->getPaddingLeft()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/ScrollView;->getPaddingRight()I

    move-result v26

    add-int v22, v25, v26

    .line 972
    .local v22, "s_padding_width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/ScrollView;->getPaddingTop()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/ScrollView;->getPaddingBottom()I

    move-result v26

    add-int v21, v25, v26

    .line 983
    .local v21, "s_padding_height":I
    add-int v20, v16, v22

    .line 984
    .local v20, "popup_width":I
    add-int v19, v15, v21

    .line 985
    .local v19, "popup_height":I
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setWidth(I)V

    .line 986
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setHeight(I)V

    .line 989
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mItemSize:I

    move/from16 v25, v0

    if-nez v25, :cond_0

    .line 991
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setHeight(I)V

    .line 992
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->setWidth(I)V

    goto/16 :goto_0

    .line 320
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_9
        :pswitch_a
        :pswitch_1
        :pswitch_8
        :pswitch_7
        :pswitch_2
        :pswitch_2
        :pswitch_b
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_d
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public showAsDropDown(Landroid/view/View;II)V
    .locals 0
    .param p1, "anchor"    # Landroid/view/View;
    .param p2, "xoff"    # I
    .param p3, "yoff"    # I

    .prologue
    .line 279
    invoke-super {p0, p1, p2, p3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 280
    return-void
.end method

.method public supportFocusTTS()Z
    .locals 14

    .prologue
    .line 999
    const/4 v3, 0x1

    .line 1002
    .local v3, "bSupportFocusTTSFlag":Z
    const/4 v1, 0x0

    .line 1004
    .local v1, "bNoteEditMode":Z
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v10}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v5

    .line 1005
    .local v5, "editStatus":J
    const-wide/32 v10, 0x10000000

    and-long/2addr v10, v5

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-eqz v10, :cond_2

    .line 1006
    const/4 v1, 0x0

    .line 1016
    :goto_0
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v10}, Lcom/infraware/office/evengine/EvInterface;->IGetBWPOpInfo_Editor()Lcom/infraware/office/evengine/EV$BWP_OP_INFO;

    move-result-object v7

    .line 1017
    .local v7, "mBWP_OP_Info":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    iget v10, v7, Lcom/infraware/office/evengine/EV$BWP_OP_INFO;->nStatusOP:I

    int-to-long v8, v10

    .line 1018
    .local v8, "nStatusOP":J
    const/4 v0, 0x0

    .line 1019
    .local v0, "bHeaderFooterEditMode":Z
    const-wide/32 v10, 0x4000000

    and-long/2addr v10, v8

    const-wide/32 v12, 0x4000000

    cmp-long v10, v10, v12

    if-eqz v10, :cond_0

    const-wide/32 v10, 0x8000000

    and-long/2addr v10, v8

    const-wide/32 v12, 0x8000000

    cmp-long v10, v10, v12

    if-nez v10, :cond_1

    .line 1022
    :cond_0
    const/4 v0, 0x1

    .line 1026
    :cond_1
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v10}, Lcom/infraware/office/evengine/EvInterface;->IGetSummaryData()Lcom/infraware/office/evengine/EV$SUMMARY_DATA;

    move-result-object v4

    .line 1029
    .local v4, "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v10}, Lcom/infraware/office/evengine/EvInterface;->IIsSupportFocusTextToSpeach()Z

    move-result v2

    .line 1031
    .local v2, "bSupport":Z
    if-nez v1, :cond_4

    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    iget v10, v4, Lcom/infraware/office/evengine/EV$SUMMARY_DATA;->nWords:I

    if-eqz v10, :cond_4

    .line 1032
    const/4 v3, 0x1

    .line 1036
    :goto_1
    return v3

    .line 1009
    .end local v0    # "bHeaderFooterEditMode":Z
    .end local v2    # "bSupport":Z
    .end local v4    # "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    .end local v7    # "mBWP_OP_Info":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    .end local v8    # "nStatusOP":J
    :cond_2
    const-wide/32 v10, -0x80000000

    and-long/2addr v10, v5

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-eqz v10, :cond_3

    .line 1010
    const/4 v1, 0x0

    goto :goto_0

    .line 1012
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 1034
    .restart local v0    # "bHeaderFooterEditMode":Z
    .restart local v2    # "bSupport":Z
    .restart local v4    # "data":Lcom/infraware/office/evengine/EV$SUMMARY_DATA;
    .restart local v7    # "mBWP_OP_Info":Lcom/infraware/office/evengine/EV$BWP_OP_INFO;
    .restart local v8    # "nStatusOP":J
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method
