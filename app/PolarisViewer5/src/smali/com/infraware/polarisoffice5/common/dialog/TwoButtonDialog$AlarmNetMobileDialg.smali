.class Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;
.super Ljava/lang/Object;
.source "TwoButtonDialog.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlarmNetMobileDialg"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$1;

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;-><init>(Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;)V

    return-void
.end method


# virtual methods
.method public varargs inflateDialog([Ljava/lang/Object;)V
    .locals 8
    .param p1, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 134
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    const v5, 0x7f07005a

    iput v5, v4, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mTitleId:I

    .line 135
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    const v5, 0x7f070063

    iput v5, v4, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mPositiveId:I

    .line 136
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    const v5, 0x7f07005f

    iput v5, v4, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mNegativeId:I

    .line 138
    const/4 v4, 0x0

    aget-object v0, p1, v4

    check-cast v0, Landroid/app/Activity;

    .line 140
    .local v0, "activity":Landroid/app/Activity;
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 141
    .local v2, "inflater":Landroid/view/LayoutInflater;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    const v5, 0x7f030006

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, v4, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mDialogView:Landroid/view/View;

    .line 142
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mDialogView:Landroid/view/View;

    const v5, 0x7f0b003b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f07005b

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 144
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mDialogView:Landroid/view/View;

    const v5, 0x7f0b003c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 146
    .local v1, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v3, v4, Landroid/util/DisplayMetrics;->density:F

    .line 147
    .local v3, "scale":F
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v4

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v5, v3

    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v5, v6

    float-to-int v5, v5

    add-int/2addr v4, v5

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v5

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v6

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v7

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 152
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    const/4 v4, 0x1

    aget-object v4, p1, v4

    check-cast v4, Landroid/content/DialogInterface$OnClickListener;

    iput-object v4, v5, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 153
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    const/4 v4, 0x2

    aget-object v4, p1, v4

    check-cast v4, Landroid/content/DialogInterface$OnCancelListener;

    iput-object v4, v5, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 154
    return-void
.end method

.method public onLocaleChanged()V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mDialogView:Landroid/view/View;

    const v1, 0x7f0b003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f07005b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 160
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;->this$0:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mDialogView:Landroid/view/View;

    const v1, 0x7f0b003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v1, 0x7f0700a3

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 161
    return-void
.end method
