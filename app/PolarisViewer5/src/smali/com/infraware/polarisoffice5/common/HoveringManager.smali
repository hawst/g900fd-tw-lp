.class public Lcom/infraware/polarisoffice5/common/HoveringManager;
.super Ljava/lang/Object;
.source "HoveringManager.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;


# static fields
.field private static mHoverListener:Landroid/view/View$OnHoverListener;

.field private static m_ButtonHoverListener:Landroid/view/View$OnHoverListener;

.field private static m_SubtitleHoverListener:Landroid/view/View$OnHoverListener;

.field private static m_TextEditorTitleHoverListener:Landroid/view/View$OnHoverListener;

.field private static m_TitleHoverListener:Landroid/view/View$OnHoverListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388
    new-instance v0, Lcom/infraware/polarisoffice5/common/HoveringManager$1;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager$1;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->m_TitleHoverListener:Landroid/view/View$OnHoverListener;

    .line 411
    new-instance v0, Lcom/infraware/polarisoffice5/common/HoveringManager$2;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager$2;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->m_SubtitleHoverListener:Landroid/view/View$OnHoverListener;

    .line 468
    new-instance v0, Lcom/infraware/polarisoffice5/common/HoveringManager$3;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager$3;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 522
    new-instance v0, Lcom/infraware/polarisoffice5/common/HoveringManager$4;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager$4;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->m_TextEditorTitleHoverListener:Landroid/view/View$OnHoverListener;

    .line 546
    new-instance v0, Lcom/infraware/polarisoffice5/common/HoveringManager$5;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/HoveringManager$5;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->m_ButtonHoverListener:Landroid/view/View$OnHoverListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dismissImageButtonHover(Landroid/app/Activity;I)Z
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "id"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 312
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 324
    :goto_0
    return v2

    .line 315
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 316
    .local v1, "imageButton":Landroid/widget/ImageButton;
    invoke-virtual {v1}, Landroid/widget/ImageButton;->isHovered()Z

    move-result v4

    if-ne v4, v3, :cond_1

    .line 318
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setHovered(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move v2, v3

    .line 320
    goto :goto_0

    .line 322
    .end local v1    # "imageButton":Landroid/widget/ImageButton;
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static setButtonTextHoveringListener(Landroid/widget/Button;)Z
    .locals 2
    .param p0, "button"    # Landroid/widget/Button;

    .prologue
    const/4 v0, 0x0

    .line 366
    if-nez p0, :cond_1

    .line 373
    :cond_0
    :goto_0
    return v0

    .line 369
    :cond_1
    invoke-virtual {p0}, Landroid/widget/Button;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372
    sget-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->m_ButtonHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p0, v0}, Landroid/widget/Button;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 373
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setHoveringButton(Landroid/app/Activity;)Z
    .locals 40
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 33
    invoke-static/range {p0 .. p0}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v39

    if-nez v39, :cond_0

    .line 34
    const/16 v39, 0x0

    .line 305
    :goto_0
    return v39

    .line 51
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/filemanager/file/FileBaseActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_2

    .line 54
    const v39, 0x7f0b0037

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v39

    check-cast v39, Landroid/widget/ImageButton;

    .line 301
    :cond_1
    :goto_1
    const/16 v39, 0x1

    goto :goto_0

    .line 78
    :cond_2
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_7

    .line 80
    const v39, 0x7f0b0002

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 81
    .local v3, "btnActionBarFile":Landroid/widget/ImageButton;
    const v39, 0x7f0b000a

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    .line 82
    .local v5, "btnActionBarMenu":Landroid/widget/ImageButton;
    const v39, 0x7f0b0064

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageButton;

    .line 83
    .local v16, "btnFindSetBtn":Landroid/widget/ImageButton;
    const v39, 0x7f0b0066

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageButton;

    .line 84
    .local v14, "btnFindPrevBtn":Landroid/widget/ImageButton;
    const v39, 0x7f0b0065

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    .line 86
    .local v11, "btnFindNextBtn":Landroid/widget/ImageButton;
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 87
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 88
    const/16 v39, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 89
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v14, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 90
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 92
    const v39, 0x7f070131

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 93
    const v39, 0x7f070052

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 95
    const v39, 0x7f070255

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v16

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 96
    const v39, 0x7f070059

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v14, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 97
    const v39, 0x7f070053

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 100
    const v39, 0x7f0b0008

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 101
    .local v2, "btnActionBarDraw":Landroid/widget/ImageButton;
    const v39, 0x7f0b000c

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageButton;

    .line 102
    .local v22, "btnPenDrawToolbarPen":Landroid/widget/ImageButton;
    const v39, 0x7f0b000d

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageButton;

    .line 103
    .local v20, "btnPenDrawToolbarLasso":Landroid/widget/ImageButton;
    const v39, 0x7f0b000e

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageButton;

    .line 104
    .local v18, "btnPenDrawToolbarErase":Landroid/widget/ImageButton;
    const v39, 0x7f0b000f

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/ImageButton;

    .line 105
    .local v19, "btnPenDrawToolbarEraseAll":Landroid/widget/ImageButton;
    const v39, 0x7f0b0010

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageButton;

    .line 107
    .local v21, "btnPenDrawToolbarPanning":Landroid/widget/ImageButton;
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 108
    const/16 v39, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 109
    const/16 v39, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 110
    const/16 v39, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 111
    const/16 v39, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 112
    const/16 v39, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 114
    const v39, 0x7f070051

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 115
    const v39, 0x7f070051

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v22

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 116
    const v39, 0x7f070057

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v20

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    const v39, 0x7f070055

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v18

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 118
    const v39, 0x7f070056

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v19

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 119
    const v39, 0x7f070054

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v21

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 124
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_3

    .line 139
    :cond_3
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_4

    .line 141
    const v39, 0x7f0b0009

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 142
    .local v4, "btnActionBarFind":Landroid/widget/ImageButton;
    const v39, 0x7f0b0005

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    .line 143
    .local v9, "btnActionBarUndo":Landroid/widget/ImageButton;
    const v39, 0x7f0b0006

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 146
    .local v6, "btnActionBarRedo":Landroid/widget/ImageButton;
    const v39, 0x7f0b006b

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/ImageButton;

    .line 147
    .local v29, "btnReplaceNextBtn":Landroid/widget/ImageButton;
    const v39, 0x7f0b006e

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/ImageButton;

    .line 148
    .local v28, "btnReplaceBtn":Landroid/widget/ImageButton;
    const v39, 0x7f0b006f

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/ImageButton;

    .line 149
    .local v27, "btnReplaceAllBtn":Landroid/widget/ImageButton;
    const v39, 0x7f0b006a

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/ImageButton;

    .line 151
    .local v32, "btnReplaceSetBtn":Landroid/widget/ImageButton;
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 152
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 153
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v6, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 156
    const/16 v39, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 157
    const/16 v39, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 158
    const/16 v39, 0x1

    move-object/from16 v0, v27

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 159
    const/16 v39, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 161
    const v39, 0x7f070303

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 162
    const v39, 0x7f070244

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 163
    const v39, 0x7f0701cb

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v6, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 166
    const v39, 0x7f070194

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v29

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 167
    const v39, 0x7f070306

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 168
    const v39, 0x7f0701d2

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v27

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 169
    const v39, 0x7f0702ac

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v32

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 175
    .end local v4    # "btnActionBarFind":Landroid/widget/ImageButton;
    .end local v6    # "btnActionBarRedo":Landroid/widget/ImageButton;
    .end local v9    # "btnActionBarUndo":Landroid/widget/ImageButton;
    .end local v27    # "btnReplaceAllBtn":Landroid/widget/ImageButton;
    .end local v28    # "btnReplaceBtn":Landroid/widget/ImageButton;
    .end local v29    # "btnReplaceNextBtn":Landroid/widget/ImageButton;
    .end local v32    # "btnReplaceSetBtn":Landroid/widget/ImageButton;
    :cond_4
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_5

    .line 183
    :cond_5
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_6

    .line 185
    const v39, 0x7f0b01ed

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 186
    .local v7, "btnActionBarSlideShow":Landroid/widget/ImageView;
    const v39, 0x7f0b01f2

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 188
    .local v8, "btnActionBarSlideShow1":Landroid/widget/ImageView;
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 189
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 191
    const v39, 0x7f070218

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 192
    const v39, 0x7f070218

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 198
    .end local v7    # "btnActionBarSlideShow":Landroid/widget/ImageView;
    .end local v8    # "btnActionBarSlideShow1":Landroid/widget/ImageView;
    :cond_6
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_1

    goto/16 :goto_1

    .line 222
    .end local v2    # "btnActionBarDraw":Landroid/widget/ImageButton;
    .end local v3    # "btnActionBarFile":Landroid/widget/ImageButton;
    .end local v5    # "btnActionBarMenu":Landroid/widget/ImageButton;
    .end local v11    # "btnFindNextBtn":Landroid/widget/ImageButton;
    .end local v14    # "btnFindPrevBtn":Landroid/widget/ImageButton;
    .end local v16    # "btnFindSetBtn":Landroid/widget/ImageButton;
    .end local v18    # "btnPenDrawToolbarErase":Landroid/widget/ImageButton;
    .end local v19    # "btnPenDrawToolbarEraseAll":Landroid/widget/ImageButton;
    .end local v20    # "btnPenDrawToolbarLasso":Landroid/widget/ImageButton;
    .end local v21    # "btnPenDrawToolbarPanning":Landroid/widget/ImageButton;
    .end local v22    # "btnPenDrawToolbarPen":Landroid/widget/ImageButton;
    :cond_7
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_8

    .line 224
    const v39, 0x7f0b000a

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/ImageView;

    .line 225
    .local v33, "btnSave":Landroid/widget/ImageView;
    const v39, 0x7f0b024a

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/ImageView;

    .line 226
    .local v34, "btnUndo":Landroid/widget/ImageView;
    const v39, 0x7f0b024b

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    .line 227
    .local v24, "btnRedo":Landroid/widget/ImageView;
    const v39, 0x7f0b024c

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 228
    .local v10, "btnFind":Landroid/widget/ImageView;
    const v39, 0x7f0b024d

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/ImageView;

    .line 229
    .local v23, "btnProperties":Landroid/widget/ImageView;
    const v39, 0x7f0b024e

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 230
    .local v17, "btnMenu":Landroid/widget/ImageView;
    const v39, 0x7f0b0250

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageButton;

    .line 231
    .local v13, "btnFindOption":Landroid/widget/ImageButton;
    const v39, 0x7f0b0251

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageButton;

    .line 232
    .local v12, "btnFindNextFind":Landroid/widget/ImageButton;
    const v39, 0x7f0b0252

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageButton;

    .line 233
    .local v15, "btnFindPrevFind":Landroid/widget/ImageButton;
    const v39, 0x7f0b0257

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageButton;

    .line 234
    .local v31, "btnReplaceOption":Landroid/widget/ImageButton;
    const v39, 0x7f0b0258

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/ImageButton;

    .line 235
    .local v30, "btnReplaceNextFind":Landroid/widget/ImageButton;
    const v39, 0x7f0b025c

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/ImageButton;

    .line 236
    .local v25, "btnReplace":Landroid/widget/ImageButton;
    const v39, 0x7f0b025d

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/Button;

    .line 238
    .local v26, "btnReplaceAll":Landroid/widget/Button;
    const/16 v39, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 239
    const/16 v39, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 240
    const/16 v39, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 241
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 242
    const/16 v39, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 243
    const/16 v39, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 244
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 245
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 246
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 247
    const/16 v39, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 248
    const/16 v39, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 249
    const/16 v39, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 250
    const/16 v39, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setHoverPopupType(I)V

    .line 252
    const v39, 0x7f070131

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 253
    const v39, 0x7f070244

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v34

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 254
    const v39, 0x7f0701cb

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 255
    const v39, 0x7f070303

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 256
    const v39, 0x7f0702fc

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 257
    const v39, 0x7f0702c4

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v17

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 258
    const v39, 0x7f0702ac

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 259
    const v39, 0x7f070053

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 260
    const v39, 0x7f070059

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 261
    const v39, 0x7f0702ac

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v31

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 262
    const v39, 0x7f070194

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v30

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 263
    const v39, 0x7f070306

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v25

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 264
    const v39, 0x7f0701d2

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v26

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 303
    .end local v10    # "btnFind":Landroid/widget/ImageView;
    .end local v12    # "btnFindNextFind":Landroid/widget/ImageButton;
    .end local v13    # "btnFindOption":Landroid/widget/ImageButton;
    .end local v15    # "btnFindPrevFind":Landroid/widget/ImageButton;
    .end local v17    # "btnMenu":Landroid/widget/ImageView;
    .end local v23    # "btnProperties":Landroid/widget/ImageView;
    .end local v24    # "btnRedo":Landroid/widget/ImageView;
    .end local v25    # "btnReplace":Landroid/widget/ImageButton;
    .end local v26    # "btnReplaceAll":Landroid/widget/Button;
    .end local v30    # "btnReplaceNextFind":Landroid/widget/ImageButton;
    .end local v31    # "btnReplaceOption":Landroid/widget/ImageButton;
    .end local v33    # "btnSave":Landroid/widget/ImageView;
    .end local v34    # "btnUndo":Landroid/widget/ImageView;
    :catch_0
    move-exception v35

    .line 305
    .local v35, "e":Ljava/lang/Exception;
    const/16 v39, 0x0

    goto/16 :goto_0

    .line 270
    .end local v35    # "e":Ljava/lang/Exception;
    :cond_8
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_9

    .line 272
    const v39, 0x7f0b0209

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/ImageView;

    .line 273
    .local v38, "viewSlideMode":Landroid/widget/ImageView;
    const v39, 0x7f0b020a

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/ImageView;

    .line 274
    .local v37, "viewPointerMode":Landroid/widget/ImageView;
    const v39, 0x7f0b020b

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v36

    check-cast v36, Landroid/widget/ImageView;

    .line 276
    .local v36, "viewPenMode":Landroid/widget/ImageView;
    const/16 v39, 0x1

    invoke-virtual/range {v38 .. v39}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 277
    const/16 v39, 0x1

    move-object/from16 v0, v37

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 278
    const/16 v39, 0x1

    move-object/from16 v0, v36

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 280
    const v39, 0x7f0702dd

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 281
    const v39, 0x7f0702dc

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 282
    const v39, 0x7f070299

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 287
    .end local v36    # "viewPenMode":Landroid/widget/ImageView;
    .end local v37    # "viewPointerMode":Landroid/widget/ImageView;
    .end local v38    # "viewSlideMode":Landroid/widget/ImageView;
    :cond_9
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    move/from16 v39, v0

    if-eqz v39, :cond_1

    .line 289
    const v39, 0x7f0b0209

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/ImageView;

    .line 290
    .restart local v38    # "viewSlideMode":Landroid/widget/ImageView;
    const v39, 0x7f0b020a

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/ImageView;

    .line 291
    .restart local v37    # "viewPointerMode":Landroid/widget/ImageView;
    const v39, 0x7f0b020b

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v36

    check-cast v36, Landroid/widget/ImageView;

    .line 293
    .restart local v36    # "viewPenMode":Landroid/widget/ImageView;
    const/16 v39, 0x1

    invoke-virtual/range {v38 .. v39}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 294
    const/16 v39, 0x1

    move-object/from16 v0, v37

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 295
    const/16 v39, 0x1

    move-object/from16 v0, v36

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 297
    const v39, 0x7f0702dd

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 298
    const v39, 0x7f0702dc

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 299
    const v39, 0x7f070299

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1
.end method

.method public static setHoveringListener(Landroid/view/View;)Z
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 378
    if-nez p0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return v0

    .line 381
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    sget-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 385
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setListSubtitleHoveringListener(Landroid/widget/TextView;)Z
    .locals 2
    .param p0, "tvTextview"    # Landroid/widget/TextView;

    .prologue
    const/4 v0, 0x0

    .line 342
    if-nez p0, :cond_1

    .line 349
    :cond_0
    :goto_0
    return v0

    .line 345
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 348
    sget-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->m_SubtitleHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 349
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setListTitleHoveringListzener(Landroid/widget/TextView;)Z
    .locals 2
    .param p0, "tvTextview"    # Landroid/widget/TextView;

    .prologue
    const/4 v0, 0x0

    .line 330
    if-nez p0, :cond_1

    .line 337
    :cond_0
    :goto_0
    return v0

    .line 333
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 336
    sget-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->m_TitleHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 337
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setTextEditorListTitleHoveringListener(Landroid/widget/TextView;)Z
    .locals 2
    .param p0, "tvTextview"    # Landroid/widget/TextView;

    .prologue
    const/4 v0, 0x0

    .line 354
    if-nez p0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return v0

    .line 357
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    sget-object v0, Lcom/infraware/polarisoffice5/common/HoveringManager;->m_TextEditorTitleHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 361
    const/4 v0, 0x1

    goto :goto_0
.end method
