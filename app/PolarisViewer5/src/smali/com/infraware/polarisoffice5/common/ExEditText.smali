.class public Lcom/infraware/polarisoffice5/common/ExEditText;
.super Landroid/widget/EditText;
.source "ExEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/ExEditText$OnKeyPreImeListener;
    }
.end annotation


# instance fields
.field protected mOnKeyPreImeListener:Lcom/infraware/polarisoffice5/common/ExEditText$OnKeyPreImeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExEditText;->mOnKeyPreImeListener:Lcom/infraware/polarisoffice5/common/ExEditText$OnKeyPreImeListener;

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExEditText;->mOnKeyPreImeListener:Lcom/infraware/polarisoffice5/common/ExEditText$OnKeyPreImeListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/polarisoffice5/common/ExEditText$OnKeyPreImeListener;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 44
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnKeyPreImeListener(Lcom/infraware/polarisoffice5/common/ExEditText$OnKeyPreImeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/infraware/polarisoffice5/common/ExEditText$OnKeyPreImeListener;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ExEditText;->mOnKeyPreImeListener:Lcom/infraware/polarisoffice5/common/ExEditText$OnKeyPreImeListener;

    .line 24
    return-void
.end method
