.class public Lcom/infraware/polarisoffice5/common/DictionarySearchControl;
.super Ljava/lang/Object;
.source "DictionarySearchControl.java"


# static fields
.field private static final DEBUG_EXTRACT:Z = false

.field public static final DICTIONARY_DB_LANGUAGE_COUNT:I = 0x4

.field private static final LOG_CAT:Ljava/lang/String; = "DictionarySearchControl"

.field public static final PREFERENCE_DICTIONARY:Ljava/lang/String; = "pref_dict"

.field public static final PREFERENCE_DIC_TYPE:Ljava/lang/String; = "dic_type"

.field private static final USE_SHORTEN_MEANING:Z

.field private static instance:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

.field private static isBinding:Z

.field private static mCurDicType:I

.field private static mDictionaryLoadingHandler:Landroid/os/Handler;

.field private static mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

.field private static mInitCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

.field private static mTypeface:Landroid/graphics/Typeface;

.field private static m_MeanTextView:Landroid/webkit/WebView;


# instance fields
.field private isGetDB:Z

.field private mContentsType:I

.field private mDicType:I

.field private mIsTxt:Z

.field private mMeanKinds:I

.field private mMeanType:I

.field private mSearchType:I

.field private mWordList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_Context:Landroid/content/Context;

.field private m_DicInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private m_DicKeyword:Landroid/text/Spanned;

.field private m_DicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private m_Handler:Landroid/os/Handler;

.field private m_Keyword:Ljava/lang/String;

.field private m_Meaning:Landroid/text/Spanned;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    sput-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->instance:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 39
    sput-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_MeanTextView:Landroid/webkit/WebView;

    .line 42
    sput-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mTypeface:Landroid/graphics/Typeface;

    .line 49
    const/16 v0, 0xb04

    sput v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    .line 51
    sput-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isBinding:Z

    .line 615
    new-instance v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl$1;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl$1;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mInitCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    .line 36
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicKeyword:Landroid/text/Spanned;

    .line 38
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    .line 40
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDicType:I

    .line 44
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mSearchType:I

    .line 45
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mMeanType:I

    .line 46
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mContentsType:I

    .line 47
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mMeanKinds:I

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mWordList:Ljava/util/ArrayList;

    .line 56
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mIsTxt:Z

    .line 92
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    .line 94
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    .line 96
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isGetDB:Z

    .line 171
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isGetDB:Z

    .line 172
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    .line 173
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setDictionLoadingHandler()V

    .line 192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    .line 195
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    .line 36
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicKeyword:Landroid/text/Spanned;

    .line 38
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    .line 40
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDicType:I

    .line 44
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mSearchType:I

    .line 45
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mMeanType:I

    .line 46
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mContentsType:I

    .line 47
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mMeanKinds:I

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mWordList:Ljava/util/ArrayList;

    .line 56
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mIsTxt:Z

    .line 92
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    .line 94
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    .line 96
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isGetDB:Z

    .line 209
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    .line 210
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    .line 211
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setDictionLoadingHandler()V

    .line 224
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "keyword"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    .line 36
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicKeyword:Landroid/text/Spanned;

    .line 38
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    .line 40
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDicType:I

    .line 44
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mSearchType:I

    .line 45
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mMeanType:I

    .line 46
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mContentsType:I

    .line 47
    iput v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mMeanKinds:I

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mWordList:Ljava/util/ArrayList;

    .line 56
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mIsTxt:Z

    .line 92
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    .line 94
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    .line 96
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isGetDB:Z

    .line 228
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    .line 229
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    .line 230
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 231
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setDictionLoadingHandler()V

    .line 233
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 234
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 247
    :cond_0
    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 27
    sput-boolean p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isBinding:Z

    return p0
.end method

.method static synthetic access$100(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 27
    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->prepareTypeface(Z)V

    return-void
.end method

.method static synthetic access$200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDictionaryLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/DictionarySearchControl;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static bindService(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 154
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-nez v0, :cond_0

    .line 157
    :try_start_0
    new-instance v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mInitCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    invoke-direct {v0, p0, v1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :cond_0
    :goto_0
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isServiceBinded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 165
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->bindservice()Z

    .line 167
    :cond_1
    return-void

    .line 158
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private getDictionaryList([Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "list"    # [Ljava/lang/CharSequence;

    .prologue
    .line 1036
    if-nez p1, :cond_1

    .line 1047
    :cond_0
    :goto_0
    return-void

    .line 1039
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 1042
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 1044
    aget-object v1, p1, v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getAvaliableDBType(I)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->insertList(Ljava/lang/CharSequence;I)V

    .line 1042
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1046
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isGetDB:Z

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->instance:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    if-nez v0, :cond_0

    .line 146
    new-instance v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->instance:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 148
    :cond_0
    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->bindService(Landroid/content/Context;)V

    .line 149
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->instance:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    return-object v0
.end method

.method private getMeaningWordListItem()V
    .locals 5

    .prologue
    .line 1287
    sget-object v3, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v3}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getDicType()I

    move-result v0

    .line 1288
    .local v0, "dicType":I
    sget-object v3, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v3}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getKeyword()Ljava/lang/String;

    move-result-object v2

    .line 1289
    .local v2, "keyword":Ljava/lang/String;
    sget-object v3, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    sget-object v4, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v4}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getKeywordPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getEntryId(I)Lcom/diotek/diodict/core/engine/EntryId;

    move-result-object v1

    .line 1291
    .local v1, "entryId":Lcom/diotek/diodict/core/engine/EntryId;
    invoke-direct {p0, v0, v2, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->updateMeaing(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V

    .line 1292
    return-void
.end method

.method private insertList(Ljava/lang/CharSequence;I)V
    .locals 2
    .param p1, "list"    # Ljava/lang/CharSequence;
    .param p2, "dicType"    # I

    .prologue
    .line 1107
    new-instance v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/DictionaryInfo;-><init>()V

    .line 1108
    .local v0, "dicInfo":Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicName:Ljava/lang/String;

    .line 1109
    iput p2, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    .line 1111
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1112
    return-void
.end method

.method private isDicInstalled()Z
    .locals 1

    .prologue
    .line 1277
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1278
    const/4 v0, 0x1

    .line 1279
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isExistDicDB(I)Z
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 967
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 968
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-ne v1, p1, :cond_0

    .line 969
    const/4 v1, 0x1

    .line 971
    :goto_1
    return v1

    .line 967
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 971
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static prepareTypeface(Z)V
    .locals 3
    .param p0, "isReset"    # Z

    .prologue
    .line 667
    if-eqz p0, :cond_0

    .line 668
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mTypeface:Landroid/graphics/Typeface;

    if-eqz v1, :cond_0

    .line 669
    const/4 v1, 0x0

    sput-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mTypeface:Landroid/graphics/Typeface;

    .line 673
    :cond_0
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-nez v1, :cond_2

    .line 686
    :cond_1
    :goto_0
    return-void

    .line 676
    :cond_2
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    sget v2, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    invoke-virtual {v1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getFontPath(I)Ljava/lang/String;

    move-result-object v0

    .line 678
    .local v0, "fontPath":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 680
    :try_start_0
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mTypeface:Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 681
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private resetData()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 574
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    .line 575
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicKeyword:Landroid/text/Spanned;

    .line 577
    return-void
.end method

.method private resetDioDicList()V
    .locals 1

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1032
    return-void
.end method

.method private sendNotInstalledMessage()V
    .locals 3

    .prologue
    .line 597
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 598
    return-void
.end method

.method private setDictionLoadingHandler()V
    .locals 1

    .prologue
    .line 637
    new-instance v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl$2;-><init>(Lcom/infraware/polarisoffice5/common/DictionarySearchControl;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDictionaryLoadingHandler:Landroid/os/Handler;

    .line 663
    return-void
.end method

.method private setSearchFail()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 335
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    const v1, 0x7f0702d7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    .line 336
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 337
    return-void
.end method

.method private updateMeaing(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V
    .locals 7
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 1283
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_MeanTextView:Landroid/webkit/WebView;

    iget v5, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mContentsType:I

    iget v6, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mMeanKinds:I

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->updateMeaing(Landroid/webkit/WebView;ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;II)Z

    .line 1284
    return-void
.end method


# virtual methods
.method public getAvaliableDBType(I)I
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 706
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v0, p1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getAvaliableDBType(I)I

    move-result v0

    return v0
.end method

.method public getDBListName()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 690
    sget-object v3, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-nez v3, :cond_1

    .line 701
    :cond_0
    :goto_0
    return v1

    .line 693
    :cond_1
    sget-object v3, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v3}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->reqDBList()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 696
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getDBListName(Z)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 698
    .local v0, "csList":[Ljava/lang/CharSequence;
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDictionaryList([Ljava/lang/CharSequence;)V

    move v1, v2

    .line 701
    goto :goto_0
.end method

.method public getDicInfo()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDicList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDicListNSortFromDiotek()V
    .locals 3

    .prologue
    .line 788
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-nez v1, :cond_0

    .line 799
    :goto_0
    return-void

    .line 794
    :cond_0
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getDBListName(Z)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 796
    .local v0, "csList":[Ljava/lang/CharSequence;
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->resetDioDicList()V

    .line 797
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDictionaryList([Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public getKeyword()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicKeyword:Landroid/text/Spanned;

    return-object v0
.end method

.method public getMeaning()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    return-object v0
.end method

.method public getSortedAllList()[Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 803
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 805
    .local v3, "size":I
    new-array v2, v3, [Ljava/lang/CharSequence;

    .line 807
    .local v2, "newList":[Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 809
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicName:Ljava/lang/String;

    .line 810
    .local v0, "dic":Ljava/lang/CharSequence;
    aput-object v0, v2, v1

    .line 807
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 812
    .end local v0    # "dic":Ljava/lang/CharSequence;
    :cond_0
    return-object v2
.end method

.method public getUserSelectDictionary(I)Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1191
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1193
    .local v0, "size":I
    if-le v0, p1, :cond_0

    .line 1195
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iput p1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->index:I

    .line 1196
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    .line 1198
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    return-object v0
.end method

.method public initDB()V
    .locals 0

    .prologue
    .line 851
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->inputInitData()V

    .line 852
    return-void
.end method

.method public inputInitData()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 856
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    const-string/jumbo v4, "pref_dict"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 857
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 859
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v3, "dic_type"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 861
    .local v2, "type":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_2

    .line 863
    :cond_0
    const-string/jumbo v3, "dic_type"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 962
    :cond_1
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 963
    return-void

    .line 866
    :cond_2
    if-nez v2, :cond_3

    .line 868
    const-string/jumbo v4, "dic_type"

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v3, v3, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 873
    :cond_3
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isExistDicDB(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 875
    const-string/jumbo v4, "dic_type"

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v3, v3, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public isBindedService()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 777
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-nez v1, :cond_1

    .line 783
    :cond_0
    :goto_0
    return v0

    .line 780
    :cond_1
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isServiceBinded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 783
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public readDB(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/DictionaryInfo;>;"
    const/4 v4, 0x0

    .line 111
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    const-string/jumbo v3, "pref_dict"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 113
    .local v1, "pref":Landroid/content/SharedPreferences;
    new-instance v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/DictionaryInfo;-><init>()V

    .line 115
    .local v0, "newDic":Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    const-string/jumbo v2, "dic_type"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    .line 116
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    return-void
.end method

.method public readDictionaryDB()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 106
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->readDB(Ljava/util/ArrayList;)V

    .line 107
    return-void
.end method

.method public saveSeletedDictionary(I)V
    .locals 5
    .param p1, "dicType"    # I

    .prologue
    .line 732
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    const-string/jumbo v3, "pref_dict"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 733
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 735
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "dic_type"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 736
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 737
    return-void
.end method

.method public setConfigChangeSearch(Lcom/infraware/polarisoffice5/common/DictionaryInfo;)Z
    .locals 7
    .param p1, "info"    # Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 471
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->resetData()V

    .line 473
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mIsTxt:Z

    if-nez v1, :cond_0

    .line 474
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 476
    :cond_0
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-nez v1, :cond_1

    .line 478
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->sendNotInstalledMessage()V

    .line 479
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0702d9

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 555
    :goto_0
    return v1

    .line 490
    :cond_1
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->prepareTypeface(Z)V

    .line 492
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_2

    .line 493
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0702d8

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 494
    goto :goto_0

    .line 502
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->initDB()V

    .line 505
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isDicInstalled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 507
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->sendNotInstalledMessage()V

    .line 508
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0702e2

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 509
    goto :goto_0

    .line 513
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 515
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-eqz v1, :cond_6

    .line 516
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    .line 522
    :cond_4
    :goto_1
    if-eqz p1, :cond_5

    .line 523
    iget v1, p1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    .line 525
    :cond_5
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    iget v6, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mSearchType:I

    invoke-virtual {v1, v4, v5, v6}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->searchWord(Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 528
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getKeyword()Ljava/lang/String;

    move-result-object v0

    .line 529
    .local v0, "rKeyword":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 530
    const-string/jumbo v1, "\\\u00b7"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 531
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 533
    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_7

    .line 534
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getKeyword()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 542
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getMeaningWordListItem()V

    .line 547
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .end local v0    # "rKeyword":Ljava/lang/String;
    :goto_2
    move v1, v3

    .line 555
    goto/16 :goto_0

    .line 518
    :cond_6
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    goto :goto_1

    .line 538
    .restart local v0    # "rKeyword":Ljava/lang/String;
    :cond_7
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setSearchFail()V

    move v1, v3

    .line 539
    goto/16 :goto_0

    .line 552
    .end local v0    # "rKeyword":Ljava/lang/String;
    :cond_8
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    const v4, 0x7f0702d7

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    .line 553
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    .line 200
    return-void
.end method

.method public setDioDictService()V
    .locals 4

    .prologue
    .line 1309
    :try_start_0
    new-instance v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mInitCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1314
    :goto_0
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isServiceBinded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1316
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->bindservice()Z

    .line 1317
    const/4 v0, 0x1

    sput-boolean v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isBinding:Z

    .line 1319
    :cond_0
    return-void

    .line 1310
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setFont(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 1
    .param p1, "keywordField"    # Landroid/widget/TextView;
    .param p2, "meanField"    # Landroid/widget/TextView;

    .prologue
    .line 560
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mTypeface:Landroid/graphics/Typeface;

    if-eqz v0, :cond_0

    .line 562
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 563
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 565
    :cond_0
    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    .line 205
    return-void
.end method

.method public setKeywordSearch(Ljava/lang/String;Lcom/infraware/polarisoffice5/common/DictionaryInfo;)Z
    .locals 7
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 384
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->resetData()V

    .line 385
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 387
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-nez v1, :cond_0

    .line 389
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->sendNotInstalledMessage()V

    .line 390
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0702d9

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 466
    :goto_0
    return v1

    .line 401
    :cond_0
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->prepareTypeface(Z)V

    .line 403
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 404
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0702d8

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 405
    goto :goto_0

    .line 413
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->initDB()V

    .line 416
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isDicInstalled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 418
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->sendNotInstalledMessage()V

    .line 419
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0702e2

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 420
    goto :goto_0

    .line 424
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 426
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-eqz v1, :cond_5

    .line 427
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    .line 433
    :cond_3
    :goto_1
    if-eqz p2, :cond_4

    .line 434
    iget v1, p2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    .line 436
    :cond_4
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    iget v6, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mSearchType:I

    invoke-virtual {v1, v4, v5, v6}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->searchWord(Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 439
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getKeyword()Ljava/lang/String;

    move-result-object v0

    .line 440
    .local v0, "rKeyword":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 441
    const-string/jumbo v1, "\\\u00b7"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 442
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 444
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_6

    .line 445
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getKeyword()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 453
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getMeaningWordListItem()V

    .line 458
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .end local v0    # "rKeyword":Ljava/lang/String;
    :goto_2
    move v1, v3

    .line 466
    goto/16 :goto_0

    .line 429
    :cond_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    goto :goto_1

    .line 449
    .restart local v0    # "rKeyword":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setSearchFail()V

    move v1, v3

    .line 450
    goto/16 :goto_0

    .line 463
    .end local v0    # "rKeyword":Ljava/lang/String;
    :cond_7
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    const v4, 0x7f0702d7

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    .line 464
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2
.end method

.method public setSearch(Lcom/infraware/polarisoffice5/common/DictionaryInfo;Z)V
    .locals 7
    .param p1, "info"    # Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    .param p2, "isPdf"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 251
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->resetData()V

    .line 254
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 256
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-nez v1, :cond_0

    .line 258
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->sendNotInstalledMessage()V

    .line 259
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0702d9

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 331
    :goto_0
    return-void

    .line 269
    :cond_0
    invoke-static {v5}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->prepareTypeface(Z)V

    .line 271
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 272
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0702d8

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 280
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->initDB()V

    .line 283
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->isDicInstalled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 285
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->sendNotInstalledMessage()V

    .line 286
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0702e2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 291
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 293
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-eqz v1, :cond_5

    .line 294
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    .line 299
    :cond_3
    :goto_1
    if-eqz p1, :cond_4

    .line 300
    iget v1, p1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    .line 302
    :cond_4
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    iget v4, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mSearchType:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->searchWord(Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 304
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getKeyword()Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, "rKeyword":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 306
    const-string/jumbo v1, "\\\u00b7"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 307
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 309
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_6

    .line 310
    sget-object v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getKeyword()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    .line 320
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getMeaningWordListItem()V

    .line 323
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 296
    .end local v0    # "rKeyword":Ljava/lang/String;
    :cond_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    sput v1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mCurDicType:I

    goto :goto_1

    .line 314
    .restart local v0    # "rKeyword":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setSearchFail()V

    goto/16 :goto_0

    .line 327
    .end local v0    # "rKeyword":Ljava/lang/String;
    :cond_7
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Context:Landroid/content/Context;

    const v2, 0x7f0702d7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Keyword:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Meaning:Landroid/text/Spanned;

    .line 329
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_Handler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method

.method public setTxt(Z)V
    .locals 0
    .param p1, "txt"    # Z

    .prologue
    .line 100
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mIsTxt:Z

    .line 101
    return-void
.end method

.method public setWebView(Landroid/webkit/WebView;)V
    .locals 0
    .param p1, "meanView"    # Landroid/webkit/WebView;

    .prologue
    .line 569
    sput-object p1, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_MeanTextView:Landroid/webkit/WebView;

    .line 570
    return-void
.end method

.method public stopDictionaryService()V
    .locals 1

    .prologue
    .line 602
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    if-eqz v0, :cond_1

    .line 603
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isServiceBinded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->unbindService()V

    .line 606
    sget-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->onDestroy()V

    .line 608
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->mDioDictService:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    .line 610
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 611
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->m_DicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 612
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$Diotek;->initialize()V

    .line 613
    return-void
.end method
