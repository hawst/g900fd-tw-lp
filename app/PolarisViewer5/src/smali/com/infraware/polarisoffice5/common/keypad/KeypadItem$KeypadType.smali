.class public interface abstract Lcom/infraware/polarisoffice5/common/keypad/KeypadItem$KeypadType;
.super Ljava/lang/Object;
.source "KeypadItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "KeypadType"
.end annotation


# static fields
.field public static final TYPE_CELL_FORMAT_ACCOUNTING:I = 0x6

.field public static final TYPE_CELL_FORMAT_COMMA:I = 0x8

.field public static final TYPE_CELL_FORMAT_NUMBER_DEC:I = 0xa

.field public static final TYPE_CELL_FORMAT_NUMBER_INC:I = 0x9

.field public static final TYPE_CELL_FORMAT_PERCENTAGE:I = 0x7

.field public static final TYPE_CHANGE_KEYPAD_ANDROID:I = 0xb

.field public static final TYPE_CONFIRM:I = 0x5

.field public static final TYPE_DELETE:I = 0x1

.field public static final TYPE_DOT:I = 0x2

.field public static final TYPE_INSERT:I = 0x0

.field public static final TYPE_POS_NEV:I = 0x4

.field public static final TYPE_TOGGLE:I = 0x3
