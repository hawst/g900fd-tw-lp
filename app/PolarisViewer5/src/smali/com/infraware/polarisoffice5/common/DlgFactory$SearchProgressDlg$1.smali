.class Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg$1;
.super Ljava/lang/Object;
.source "DlgFactory.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg$1;->this$1:Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg$1;->val$this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;

    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 182
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISearchStop()V

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg$1;->val$activity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg$1;->val$activity:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->CancelFind()V

    .line 188
    :cond_1
    return-void
.end method
