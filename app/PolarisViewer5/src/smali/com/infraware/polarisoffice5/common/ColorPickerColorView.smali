.class public Lcom/infraware/polarisoffice5/common/ColorPickerColorView;
.super Landroid/view/View;
.source "ColorPickerColorView.java"


# instance fields
.field private mColor:I

.field private mOrgColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 15
    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mColor:I

    .line 16
    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mOrgColor:I

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mColor:I

    .line 16
    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mOrgColor:I

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;
    .param p3, "arg2"    # I

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mColor:I

    .line 16
    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mOrgColor:I

    .line 34
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->getWidth()I

    move-result v8

    .line 58
    .local v8, "w":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->getHeight()I

    move-result v6

    .line 60
    .local v6, "h":I
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 62
    .local v5, "paint":Landroid/graphics/Paint;
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mColor:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    div-int/lit8 v7, v6, 0x2

    .line 66
    .local v7, "half":I
    int-to-float v3, v8

    int-to-float v4, v7

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 67
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mOrgColor:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 68
    int-to-float v2, v7

    int-to-float v3, v8

    int-to-float v4, v6

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 70
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 71
    return-void
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mColor:I

    return v0
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mColor:I

    .line 39
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->invalidate()V

    .line 40
    return-void
.end method

.method public setOrgColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->mOrgColor:I

    .line 44
    return-void
.end method
