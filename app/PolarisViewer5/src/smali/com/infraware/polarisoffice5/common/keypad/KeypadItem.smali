.class public Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
.super Ljava/lang/Object;
.source "KeypadItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/keypad/KeypadItem$KeypadType;
    }
.end annotation


# instance fields
.field public mType:I

.field public mValue:Ljava/lang/String;


# direct methods
.method private constructor <init>(I)V
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p1, p0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;->mType:I

    .line 37
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;->mValue:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static createConfirmItem(Ljava/lang/String;)Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    .locals 4
    .param p0, "nDirection"    # Ljava/lang/String;

    .prologue
    .line 76
    const-string/jumbo v1, "L"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "T"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "R"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "B"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "N"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Confirm Item direct invalid, param is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;-><init>(I)V

    .line 80
    .local v0, "item":Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    iput-object p0, v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;->mValue:Ljava/lang/String;

    .line 81
    return-object v0
.end method

.method public static createDeleteItem()Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;-><init>(I)V

    .line 50
    .local v0, "item":Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    return-object v0
.end method

.method public static createDotItem()Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;-><init>(I)V

    .line 55
    .local v0, "item":Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    return-object v0
.end method

.method public static createFormatItem(ILjava/lang/String;)Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    .locals 4
    .param p0, "nType"    # I
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 92
    const/4 v1, 0x6

    if-eq v1, p0, :cond_0

    const/4 v1, 0x7

    if-eq v1, p0, :cond_0

    const/16 v1, 0x8

    if-eq v1, p0, :cond_0

    const/16 v1, 0x9

    if-eq v1, p0, :cond_0

    const/16 v1, 0xa

    if-eq v1, p0, :cond_0

    .line 94
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Format Item type is invalid, param is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;-><init>(I)V

    .line 96
    .local v0, "item":Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    if-eqz p1, :cond_1

    const-string/jumbo v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 97
    iput-object p1, v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;->mValue:Ljava/lang/String;

    .line 98
    :cond_1
    return-object v0
.end method

.method public static createInsertItem(Ljava/lang/String;)Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    .locals 2
    .param p0, "nValue"    # Ljava/lang/String;

    .prologue
    .line 43
    new-instance v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;-><init>(I)V

    .line 44
    .local v0, "item":Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    iput-object p0, v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;->mValue:Ljava/lang/String;

    .line 45
    return-object v0
.end method

.method public static createPosNevItem()Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;-><init>(I)V

    .line 66
    .local v0, "item":Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    return-object v0
.end method

.method public static createToggleItem(Ljava/lang/String;)Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    .locals 2
    .param p0, "aCmd"    # Ljava/lang/String;

    .prologue
    .line 59
    new-instance v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;-><init>(I)V

    .line 60
    .local v0, "item":Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    iput-object p0, v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;->mValue:Ljava/lang/String;

    .line 61
    return-object v0
.end method

.method public static createToggleKeypad()Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;-><init>(I)V

    .line 71
    .local v0, "item":Lcom/infraware/polarisoffice5/common/keypad/KeypadItem;
    return-object v0
.end method
