.class final Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;
.super Ljava/lang/Object;
.source "DlgFactory.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/DlgFactory;->AlertConfirm(Landroid/app/Activity;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "MyRunnable"
.end annotation


# instance fields
.field mContent:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "content"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;->this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;->mContext:Landroid/content/Context;

    .line 128
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;->mContent:Ljava/lang/String;

    .line 129
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 133
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 134
    .local v0, "ab":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;->mContent:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 135
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 136
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 137
    .local v1, "dialog":Landroid/app/AlertDialog;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 138
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 139
    return-void
.end method
