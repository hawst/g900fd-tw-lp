.class Lcom/infraware/polarisoffice5/common/VideoFrameLayout$6;
.super Ljava/lang/Object;
.source "VideoFrameLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoViewListener()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$6;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 308
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$6;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$300(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Landroid/widget/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/VideoView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 316
    :cond_0
    :goto_0
    return v3

    .line 311
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 312
    .local v0, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 314
    .local v1, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 315
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$6;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onPauseVideo()V

    goto :goto_0
.end method
