.class public Lcom/infraware/polarisoffice5/common/dialog/DialogManager;
.super Ljava/lang/Object;
.source "DialogManager.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/common/dialog/BaseDialog$Finalizable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;,
        Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager;


# instance fields
.field private mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

.field private mType:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method private create(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;Lcom/infraware/polarisoffice5/common/dialog/DialogManager;)Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;
    .locals 6
    .param p1, "a_context"    # Landroid/content/Context;
    .param p2, "a_type"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
    .param p3, "sInstance"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    .prologue
    .line 125
    :try_start_0
    invoke-virtual {p2}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->getDialogClass()Ljava/lang/Class;

    move-result-object v1

    .line 126
    .local v1, "dialogClass":Ljava/lang/Class;
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-class v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    aput-object v5, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 128
    .local v0, "con":Ljava/lang/reflect/Constructor;
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    .line 129
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    .end local v0    # "con":Ljava/lang/reflect/Constructor;
    .end local v1    # "dialogClass":Ljava/lang/Class;
    :goto_0
    return-object v3

    .line 130
    :catch_0
    move-exception v2

    .line 131
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 133
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->sInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->sInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    .line 58
    :cond_0
    sget-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->sInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    return-object v0
.end method


# virtual methods
.method public FinalizeDialogInstance()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    .line 65
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public isValidType(Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;)Z
    .locals 5
    .param p1, "a_type"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 107
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    if-eqz v4, :cond_0

    move v1, v2

    .line 108
    .local v1, "isVaildInstance":Z
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mType:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    if-ne v4, p1, :cond_1

    move v0, v2

    .line 109
    .local v0, "isSameType":Z
    :goto_1
    if-ne v1, v2, :cond_2

    if-ne v0, v2, :cond_2

    .line 112
    :goto_2
    return v2

    .end local v0    # "isSameType":Z
    .end local v1    # "isVaildInstance":Z
    :cond_0
    move v1, v3

    .line 107
    goto :goto_0

    .restart local v1    # "isVaildInstance":Z
    :cond_1
    move v0, v3

    .line 108
    goto :goto_1

    .restart local v0    # "isSameType":Z
    :cond_2
    move v2, v3

    .line 112
    goto :goto_2
.end method

.method public onCancel()V
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;->cancel()V

    .line 89
    :cond_0
    return-void
.end method

.method public onDismiss()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;->dismiss()V

    .line 83
    :cond_0
    return-void
.end method

.method public onLocaleChanged()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;->onLocaleChanged()V

    .line 95
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;->onPause()V

    .line 71
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;->onResume()V

    .line 77
    :cond_0
    return-void
.end method

.method public varargs show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "a_context"    # Landroid/content/Context;
    .param p2, "a_type"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
    .param p3, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 117
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mType:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 118
    sget-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->sInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    invoke-direct {p0, p1, p2, v0}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->create(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;Lcom/infraware/polarisoffice5/common/dialog/DialogManager;)Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    .line 119
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->mDialogInstance:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;

    invoke-interface {v0, p3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;->show([Ljava/lang/Object;)V

    .line 120
    return-void
.end method
