.class public Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;
.super Landroid/app/ProgressDialog;
.source "DlgFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/DlgFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SearchProgressDlg"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V
    .locals 3
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x1

    .line 171
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;->this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;

    .line 172
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v0

    invoke-direct {p0, p2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 174
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;->setMessage(Ljava/lang/CharSequence;)V

    .line 176
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;->setIndeterminate(Z)V

    .line 177
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;->setCancelable(Z)V

    .line 178
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;->setCanceledOnTouchOutside(Z)V

    .line 179
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg$1;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 190
    return-void
.end method
