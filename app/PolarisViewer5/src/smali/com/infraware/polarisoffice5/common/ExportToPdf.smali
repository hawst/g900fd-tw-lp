.class public Lcom/infraware/polarisoffice5/common/ExportToPdf;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "ExportToPdf.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;,
        Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;
    }
.end annotation


# instance fields
.field private mEditIcon:Landroid/widget/ImageView;

.field mEditTextClick:Landroid/view/View$OnClickListener;

.field private mExtentionType:Landroid/widget/TextView;

.field private mFilePathError:Landroid/widget/TextView;

.field private mFilePathErrorLayout:Landroid/widget/LinearLayout;

.field private mFilename:Landroid/widget/TextView;

.field private mFolderpath:Landroid/widget/TextView;

.field private mSavePendrawCheckBox:Landroid/widget/CheckBox;

.field private m_ToastMsg:Landroid/widget/Toast;

.field private m_bUseEmail:Z

.field private m_etFileName:Landroid/widget/EditText;

.field private m_nLocaleCode:I

.field private m_nMode:I

.field private m_nOrientation:I

.field private m_nType:I

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;

.field private m_oInputFilter:Lcom/infraware/filemanager/file/FileInputFilter;

.field private m_oItem:Lcom/infraware/filemanager/file/FileListItem;

.field private m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

.field private m_oTextWatcher:Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;

.field private m_strCurPath:Ljava/lang/String;

.field private m_strOriginFile:Ljava/lang/String;

.field private m_strStorageId:Ljava/lang/String;

.field private m_strTargetId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oInputFilter:Lcom/infraware/filemanager/file/FileInputFilter;

    .line 49
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 50
    new-instance v0, Lcom/infraware/filemanager/file/FileListItem;

    invoke-direct {v0}, Lcom/infraware/filemanager/file/FileListItem;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    .line 51
    iput v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nOrientation:I

    .line 57
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_bUseEmail:Z

    .line 59
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_strCurPath:Ljava/lang/String;

    .line 60
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_strTargetId:Ljava/lang/String;

    .line 61
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_strStorageId:Ljava/lang/String;

    .line 63
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 64
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;

    .line 65
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oTextWatcher:Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;

    .line 66
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mExtentionType:Landroid/widget/TextView;

    .line 67
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilename:Landroid/widget/TextView;

    .line 68
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFolderpath:Landroid/widget/TextView;

    .line 69
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathError:Landroid/widget/TextView;

    .line 70
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    .line 71
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mEditIcon:Landroid/widget/ImageView;

    .line 72
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    .line 74
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mSavePendrawCheckBox:Landroid/widget/CheckBox;

    .line 343
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_ToastMsg:Landroid/widget/Toast;

    .line 457
    new-instance v0, Lcom/infraware/polarisoffice5/common/ExportToPdf$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf$3;-><init>(Lcom/infraware/polarisoffice5/common/ExportToPdf;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mEditTextClick:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/ExportToPdf;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ExportToPdf;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/ExportToPdf;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ExportToPdf;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->checkFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/ExportToPdf;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ExportToPdf;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_strOriginFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/ExportToPdf;)Lcom/infraware/filemanager/file/FileListItem;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ExportToPdf;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    return-object v0
.end method

.method private checkFile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "newName"    # Ljava/lang/String;

    .prologue
    .line 356
    if-eqz p1, :cond_0

    const-string/jumbo v3, ""

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 359
    :cond_0
    const/4 v2, 0x0

    .line 371
    :goto_0
    return v2

    .line 361
    :cond_1
    const/4 v2, 0x0

    .line 362
    .local v2, "result":Z
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v4}, Lcom/infraware/filemanager/file/FileListItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "pdf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 363
    .local v1, "file":Ljava/io/File;
    const v3, 0x7f0b0088

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 364
    .local v0, "btnSave":Landroid/widget/Button;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 365
    const/4 v2, 0x0

    .line 368
    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 369
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 367
    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public actionTitleBarButtonClick()V
    .locals 5

    .prologue
    .line 468
    const v3, 0x7f0b007c

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 470
    .local v0, "et":Landroid/widget/EditText;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v4}, Lcom/infraware/filemanager/file/FileListItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ".pdf"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 472
    .local v1, "filepath":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->checkFile(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 473
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oTextWatcher:Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 475
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 476
    .local v2, "result":Landroid/content/Intent;
    const-string/jumbo v3, "key_new_file"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 477
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_bUseEmail:Z

    if-eqz v3, :cond_0

    .line 478
    const-string/jumbo v3, "PDFSaveToEmail"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 479
    :cond_0
    const-string/jumbo v3, "key_save_include_pendraw"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mSavePendrawCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 480
    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 481
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->setResult(ILandroid/content/Intent;)V

    .line 482
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->finish()V

    .line 485
    .end local v2    # "result":Landroid/content/Intent;
    :goto_0
    return-void

    .line 484
    :cond_1
    const/4 v3, -0x3

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->onToastMessage(I)V

    goto :goto_0
.end method

.method public getSdcardIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 338
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 339
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 340
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 341
    return-object v0
.end method

.method public isSdCardAction(Ljava/lang/String;)V
    .locals 4
    .param p1, "nowAction"    # Ljava/lang/String;

    .prologue
    .line 288
    const-string/jumbo v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 290
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_strOriginFile:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 293
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->finish()V

    .line 306
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 297
    .restart local v1    # "file":Ljava/io/File;
    :cond_1
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 298
    .restart local v1    # "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 300
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 302
    const v2, 0x7f0b0082

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 303
    .local v0, "btnFolder":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v2, v2, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 417
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/common/baseactivity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 419
    const/4 v5, -0x1

    if-ne p2, v5, :cond_1

    .line 420
    const v5, 0x7f0b0080

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 421
    .local v3, "ivLocationIcon":Landroid/widget/ImageView;
    const v5, 0x7f0b0081

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 422
    .local v4, "tvLocationName":Landroid/widget/TextView;
    const v5, 0x7f0b0082

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 424
    .local v0, "btnFolder":Landroid/widget/TextView;
    const-string/jumbo v5, "key_new_folder"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 426
    .local v2, "folderName":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getIcon(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 427
    const v5, 0x7f070033

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 429
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "FT03"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v5, v8, :cond_0

    .line 430
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 431
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07001e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    :cond_0
    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 443
    const v5, 0x7f0b007c

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 446
    .local v1, "et":Landroid/widget/EditText;
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->checkFile(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 448
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5, v7}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 449
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 456
    .end local v0    # "btnFolder":Landroid/widget/TextView;
    .end local v1    # "et":Landroid/widget/EditText;
    .end local v2    # "folderName":Ljava/lang/String;
    .end local v3    # "ivLocationIcon":Landroid/widget/ImageView;
    .end local v4    # "tvLocationName":Landroid/widget/TextView;
    :cond_1
    :goto_1
    return-void

    .line 432
    .restart local v0    # "btnFolder":Landroid/widget/TextView;
    .restart local v2    # "folderName":Ljava/lang/String;
    .restart local v3    # "ivLocationIcon":Landroid/widget/ImageView;
    .restart local v4    # "tvLocationName":Landroid/widget/TextView;
    :cond_2
    const-string/jumbo v5, "/storage/extSdCard"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 433
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07002a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 434
    :cond_3
    const-string/jumbo v5, "/storage/UsbDrive"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 435
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07002b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 436
    :cond_4
    const-string/jumbo v5, "/storage/PersonalPage"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 437
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07002c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 452
    .restart local v1    # "et":Landroid/widget/EditText;
    :cond_5
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5, v8}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 453
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 117
    const v10, 0x7f030017

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->setContentView(I)V

    .line 118
    new-instance v10, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v11, 0x7f0b001d

    const/4 v12, 0x2

    const/4 v13, 0x2

    invoke-direct {v10, p0, v11, v12, v13}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 119
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v11, 0x7f07012f

    invoke-virtual {v10, v11}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 120
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v11, 0x7f02000b

    const v12, 0x7f070065

    invoke-virtual {v10, v11, v12}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonImage_ex(II)V

    .line 121
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v10}, Lcom/infraware/office/actionbar/ActionTitleBar;->changeActionTitleBarHeight()V

    .line 122
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v10}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 124
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-static {v10}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v10

    iput v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nLocaleCode:I

    .line 125
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    iput v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nOrientation:I

    .line 126
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 128
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 135
    :goto_0
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    const/4 v11, 0x1

    iput v11, v10, Lcom/infraware/filemanager/file/FileListItem;->type:I

    .line 137
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 139
    .local v3, "i":Landroid/content/Intent;
    const v10, 0x7f0b0086

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathError:Landroid/widget/TextView;

    .line 140
    const v10, 0x7f0b0084

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    .line 141
    const-string/jumbo v10, "key_filename"

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 142
    .local v8, "nType":I
    const v10, 0x7f0b007d

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mExtentionType:Landroid/widget/TextView;

    .line 143
    const v10, 0x7f0b007b

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mEditIcon:Landroid/widget/ImageView;

    .line 144
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mExtentionType:Landroid/widget/TextView;

    const-string/jumbo v11, ".pdf"

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    const v10, 0x7f0b007a

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilename:Landroid/widget/TextView;

    .line 147
    const v10, 0x7f0b007e

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFolderpath:Landroid/widget/TextView;

    .line 149
    const-string/jumbo v10, "key_content_mode"

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 150
    .local v7, "nOption":I
    const/high16 v10, 0x10000

    div-int v10, v7, v10

    iput v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nMode:I

    .line 151
    const/high16 v10, 0x10000

    rem-int v10, v7, v10

    iput v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nType:I

    .line 153
    const-string/jumbo v10, "PDFSaveToEmail"

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 154
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_bUseEmail:Z

    .line 156
    :cond_0
    const-string/jumbo v10, "key_current_file"

    invoke-virtual {v3, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "curFile":Ljava/lang/String;
    const/16 v10, 0x2f

    invoke-virtual {v1, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 158
    .local v4, "index":I
    if-ltz v4, :cond_7

    .line 160
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    const/4 v11, 0x0

    invoke-virtual {v1, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 162
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/infraware/common/util/Utils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->isKkDisuseExternal()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 163
    :cond_1
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-static {v10}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->isSdCardFile(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 164
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 166
    :cond_2
    add-int/lit8 v10, v4, 0x1

    invoke-virtual {v1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    sget v11, Lcom/infraware/filemanager/define/FMDefine;->MAX_FILENAME_LENGTH:I

    if-le v10, v11, :cond_3

    .line 168
    const/4 v10, 0x0

    sget v11, Lcom/infraware/filemanager/define/FMDefine;->MAX_FILENAME_LENGTH:I

    invoke-virtual {v1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 171
    :cond_3
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    const-string/jumbo v11, "data/com."

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    sget-object v11, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 172
    :cond_4
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    .line 177
    :cond_5
    :goto_1
    const/16 v10, 0x2e

    invoke-virtual {v1, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 178
    if-ltz v4, :cond_8

    .line 179
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    const/4 v11, 0x0

    invoke-virtual {v1, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    .line 183
    :goto_2
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget v11, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nType:I

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/infraware/common/util/FileUtils;->getFileExtension(IZ)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    .line 184
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->ext:Ljava/lang/String;

    if-nez v10, :cond_9

    .line 186
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->setResult(I)V

    .line 187
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->finish()V

    .line 284
    :goto_3
    return-void

    .line 132
    .end local v1    # "curFile":Ljava/lang/String;
    .end local v3    # "i":Landroid/content/Intent;
    .end local v4    # "index":I
    .end local v7    # "nOption":I
    .end local v8    # "nType":I
    :cond_6
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getWindow()Landroid/view/Window;

    move-result-object v10

    iget v11, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nOrientation:I

    invoke-static {v10, v11}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    goto/16 :goto_0

    .line 175
    .restart local v1    # "curFile":Ljava/lang/String;
    .restart local v3    # "i":Landroid/content/Intent;
    .restart local v4    # "index":I
    .restart local v7    # "nOption":I
    .restart local v8    # "nType":I
    :cond_7
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    goto :goto_1

    .line 181
    :cond_8
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iput-object v1, v10, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    goto :goto_2

    .line 191
    :cond_9
    iget v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nMode:I

    if-nez v10, :cond_c

    .line 192
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v10}, Lcom/infraware/filemanager/file/FileListItem;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_strOriginFile:Ljava/lang/String;

    .line 202
    :goto_4
    new-instance v10, Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {v10}, Lcom/infraware/common/event/SdCardEvent;-><init>()V

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 203
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {v10, p0}, Lcom/infraware/common/event/SdCardEvent;->setSdListener(Lcom/infraware/common/event/SdCardListener;)V

    .line 204
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getSdcardIntentFilter()Landroid/content/IntentFilter;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 206
    new-instance v10, Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;

    invoke-direct {v10, p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/common/ExportToPdf;)V

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;

    .line 207
    new-instance v10, Landroid/content/IntentFilter;

    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 208
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v11, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v10, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 209
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;

    iget-object v11, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v10, v11}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 211
    const v10, 0x7f0b007c

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    .line 212
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f050087

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setHighlightColor(I)V

    .line 213
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    iget-object v11, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v11, v11, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v12, v12, Lcom/infraware/filemanager/file/FileListItem;->name:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Landroid/widget/EditText;->setSelection(II)V

    .line 217
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    iget-object v11, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mEditTextClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    new-instance v10, Lcom/infraware/filemanager/file/FileInputFilter;

    invoke-direct {v10, p0}, Lcom/infraware/filemanager/file/FileInputFilter;-><init>(Landroid/content/Context;)V

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oInputFilter:Lcom/infraware/filemanager/file/FileInputFilter;

    .line 220
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    iget-object v11, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oInputFilter:Lcom/infraware/filemanager/file/FileInputFilter;

    invoke-virtual {v11}, Lcom/infraware/filemanager/file/FileInputFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 222
    const v10, 0x7f0b0082

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 223
    .local v0, "btnFolder":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    const v10, 0x7f0b0080

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 226
    .local v5, "ivLocationIcon":Landroid/widget/ImageView;
    const v10, 0x7f0b0081

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 228
    .local v9, "tvLocationName":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getIcon(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 229
    const v10, 0x7f070033

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 231
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "FT03"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_a

    .line 232
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 233
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f07001e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    :cond_a
    :goto_5
    const v10, 0x7f0b007f

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 243
    .local v2, "folderButton":Landroid/widget/LinearLayout;
    new-instance v10, Lcom/infraware/polarisoffice5/common/ExportToPdf$1;

    invoke-direct {v10, p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf$1;-><init>(Lcom/infraware/polarisoffice5/common/ExportToPdf;)V

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    new-instance v6, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;

    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v10}, Lcom/infraware/office/actionbar/ActionTitleBar;->getButton()Landroid/widget/Button;

    move-result-object v10

    invoke-direct {v6, p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;-><init>(Lcom/infraware/polarisoffice5/common/ExportToPdf;Landroid/widget/Button;)V

    .line 255
    .local v6, "m_oTextWatcher":Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    invoke-virtual {v10, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 257
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->checkFile(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 258
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 259
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 263
    :cond_b
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getWindow()Landroid/view/Window;

    move-result-object v10

    const/16 v11, 0x15

    invoke-virtual {v10, v11}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 266
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 268
    const v10, 0x7f0b0083

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mSavePendrawCheckBox:Landroid/widget/CheckBox;

    .line 270
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mSavePendrawCheckBox:Landroid/widget/CheckBox;

    new-instance v11, Lcom/infraware/polarisoffice5/common/ExportToPdf$2;

    invoke-direct {v11, p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf$2;-><init>(Lcom/infraware/polarisoffice5/common/ExportToPdf;)V

    invoke-virtual {v10, v11}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 193
    .end local v0    # "btnFolder":Landroid/widget/TextView;
    .end local v2    # "folderButton":Landroid/widget/LinearLayout;
    .end local v5    # "ivLocationIcon":Landroid/widget/ImageView;
    .end local v6    # "m_oTextWatcher":Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;
    .end local v9    # "tvLocationName":Landroid/widget/TextView;
    :cond_c
    iget v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_nMode:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_d

    .line 194
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    invoke-virtual {v10}, Lcom/infraware/filemanager/file/FileListItem;->getPath()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_strOriginFile:Ljava/lang/String;

    goto/16 :goto_4

    .line 197
    :cond_d
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->setResult(I)V

    .line 198
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->finish()V

    goto/16 :goto_3

    .line 234
    .restart local v0    # "btnFolder":Landroid/widget/TextView;
    .restart local v5    # "ivLocationIcon":Landroid/widget/ImageView;
    .restart local v9    # "tvLocationName":Landroid/widget/TextView;
    :cond_e
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    const-string/jumbo v11, "/storage/extSdCard"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 235
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f07002a

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 236
    :cond_f
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    const-string/jumbo v11, "/storage/UsbDrive"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 237
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f07002b

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 238
    :cond_10
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;

    iget-object v10, v10, Lcom/infraware/filemanager/file/FileListItem;->path:Ljava/lang/String;

    const-string/jumbo v11, "/storage/PersonalPage"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 239
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f07002c

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;

    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ExportToPdf$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 412
    :cond_1
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onDestroy()V

    .line 413
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 2
    .param p1, "nLocale"    # I

    .prologue
    .line 310
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilename:Landroid/widget/TextView;

    const v1, 0x7f070294

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 311
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFolderpath:Landroid/widget/TextView;

    const v1, 0x7f07013b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 315
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mSavePendrawCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mSavePendrawCheckBox:Landroid/widget/CheckBox;

    const v1, 0x7f0701e6

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathError:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathError:Landroid/widget/TextView;

    const v1, 0x7f070257

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_etFileName:Landroid/widget/EditText;

    const v1, 0x7f07009c

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 320
    :cond_2
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 402
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onPause()V

    .line 403
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 377
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onResume()V

    .line 379
    const v2, 0x7f0b007c

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 380
    .local v0, "et":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "newName":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 384
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 385
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 398
    :goto_0
    return-void

    .line 389
    :cond_1
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->checkFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 390
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 391
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 395
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 396
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onToastMessage(I)V
    .locals 4
    .param p1, "err"    # I

    .prologue
    const/4 v3, 0x0

    .line 345
    invoke-static {p0, p1}, Lcom/infraware/filemanager/porting/FileError;->getErrorMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, "errMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 347
    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_ToastMsg:Landroid/widget/Toast;

    .line 351
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_ToastMsg:Landroid/widget/Toast;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 352
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 353
    return-void

    .line 349
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
