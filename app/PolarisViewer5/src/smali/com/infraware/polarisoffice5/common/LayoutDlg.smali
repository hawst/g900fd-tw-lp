.class public Lcom/infraware/polarisoffice5/common/LayoutDlg;
.super Landroid/app/AlertDialog;
.source "LayoutDlg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;,
        Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;
    }
.end annotation


# instance fields
.field mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

.field mEventType:I

.field mHandler:Landroid/os/Handler;

.field mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I
    .param p3, "aEventType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p4, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    .line 162
    new-instance v0, Lcom/infraware/polarisoffice5/common/LayoutDlg$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/LayoutDlg$2;-><init>(Lcom/infraware/polarisoffice5/common/LayoutDlg;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 47
    iput p3, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mEventType:I

    .line 49
    invoke-direct {p0, p4}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->createView(Ljava/util/ArrayList;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "aEventType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p3, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 162
    new-instance v0, Lcom/infraware/polarisoffice5/common/LayoutDlg$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/LayoutDlg$2;-><init>(Lcom/infraware/polarisoffice5/common/LayoutDlg;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 41
    iput p2, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mEventType:I

    .line 42
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->createView(Ljava/util/ArrayList;)V

    .line 43
    return-void
.end method

.method private createView(Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    const/4 v12, 0x1

    .line 54
    const-string/jumbo v1, "LayoutDlg "

    const-string/jumbo v2, " "

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    .line 57
    .local v8, "inflater":Landroid/view/LayoutInflater;
    const/4 v9, 0x0

    .line 58
    .local v9, "items":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    .line 61
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v4, "listItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v6, v9

    .local v6, "arr$":[Ljava/lang/String;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v10, :cond_0

    aget-object v11, v6, v7

    .line 63
    .local v11, "str":Ljava/lang/String;
    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 65
    .end local v11    # "str":Ljava/lang/String;
    :cond_0
    const v1, 0x7f07012e

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->setTitle(I)V

    .line 67
    new-instance v0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mEventType:I

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;-><init>(Lcom/infraware/polarisoffice5/common/LayoutDlg;Landroid/content/Context;ILjava/util/List;Ljava/util/ArrayList;)V

    .line 69
    .local v0, "adapter":Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;
    new-instance v1, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mListView:Landroid/widget/ListView;

    .line 70
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 71
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/infraware/polarisoffice5/common/LayoutDlg$1;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/LayoutDlg$1;-><init>(Lcom/infraware/polarisoffice5/common/LayoutDlg;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 88
    invoke-virtual {p0, v12}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->setCancelable(Z)V

    .line 89
    invoke-virtual {p0, v12}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->setCanceledOnTouchOutside(Z)V

    .line 91
    return-void
.end method


# virtual methods
.method public setEventHandler(Landroid/os/Handler;)Landroid/app/AlertDialog;
    .locals 0
    .param p1, "aHandler"    # Landroid/os/Handler;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mHandler:Landroid/os/Handler;

    .line 172
    return-object p0
.end method
