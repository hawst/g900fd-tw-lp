.class Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;
.super Ljava/lang/Object;
.source "OneButtonDialog.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReplaceResultDialog"
.end annotation


# instance fields
.field private mTotalCnt:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V

    return-void
.end method

.method private setMessge()V
    .locals 5

    .prologue
    .line 88
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->mTotalCnt:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 89
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f0701d4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mStrMsg:Ljava/lang/String;

    .line 92
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f0701d9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->mTotalCnt:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mStrMsg:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public varargs inflateDialog([Ljava/lang/Object;)V
    .locals 2
    .param p1, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 73
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->mTotalCnt:I

    .line 74
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    const/4 v0, 0x1

    aget-object v0, p1, v0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 75
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    const/4 v0, 0x2

    aget-object v0, p1, v0

    check-cast v0, Landroid/content/DialogInterface$OnCancelListener;

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 76
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    const/4 v0, 0x3

    aget-object v0, p1, v0

    check-cast v0, Landroid/content/DialogInterface$OnKeyListener;

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 78
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->setMessge()V

    .line 79
    return-void
.end method

.method public onLocaleChanged()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;->setMessge()V

    .line 84
    return-void
.end method
