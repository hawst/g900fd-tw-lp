.class public Lcom/infraware/polarisoffice5/common/ColorPickerActivity;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "ColorPickerActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;
    }
.end annotation


# instance fields
.field private mColor:I

.field private mColorBlueInput:Landroid/widget/EditText;

.field private mColorGreenInput:Landroid/widget/EditText;

.field private mColorRedInput:Landroid/widget/EditText;

.field private mColorView:Lcom/infraware/polarisoffice5/common/ColorPickerColorView;

.field private mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

.field private final mMAX:I

.field private final mMIN:I

.field private mMainLayout:Landroid/widget/LinearLayout;

.field private mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

.field private mProtectTextWatcher:Z

.field private mToast:Landroid/widget/Toast;

.field private mType:I

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;

.field resetColorWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainLayout:Landroid/widget/LinearLayout;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    .line 36
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorView:Lcom/infraware/polarisoffice5/common/ColorPickerColorView;

    .line 39
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    .line 40
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    .line 41
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    .line 43
    iput v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mType:I

    .line 44
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    .line 45
    iput v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMIN:I

    .line 46
    const/16 v0, 0xff

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMAX:I

    .line 48
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mToast:Landroid/widget/Toast;

    .line 50
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mProtectTextWatcher:Z

    .line 53
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 196
    new-instance v0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$5;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->resetColorWatcher:Landroid/text/TextWatcher;

    .line 493
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mProtectTextWatcher:Z

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ColorPickerActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onToastMessage(Ljava/lang/String;)V

    return-void
.end method

.method private fillColorInfo(IZ)V
    .locals 4
    .param p1, "color"    # I
    .param p2, "bFromKey"    # Z

    .prologue
    .line 317
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorView:Lcom/infraware/polarisoffice5/common/ColorPickerColorView;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->setColor(I)V

    .line 320
    if-nez p2, :cond_0

    .line 321
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mProtectTextWatcher:Z

    .line 322
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    .line 323
    .local v0, "colorNum":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 324
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v0

    .line 325
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 326
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 327
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 330
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 331
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 334
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mProtectTextWatcher:Z

    .line 336
    .end local v0    # "colorNum":I
    :cond_0
    return-void
.end method

.method private onToastMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 287
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 288
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mToast:Landroid/widget/Toast;

    .line 291
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mToast:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 292
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 293
    return-void

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static safeParseInt(Ljava/lang/String;)I
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 340
    const/4 v1, 0x0

    .line 343
    .local v1, "r":I
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 350
    :goto_0
    return v1

    .line 345
    :catch_0
    move-exception v0

    .line 347
    .local v0, "e":Ljava/lang/NumberFormatException;
    const/4 v1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public actionTitleBarButtonClick()V
    .locals 3

    .prologue
    .line 355
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 356
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "color_type"

    iget v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 357
    const-string/jumbo v1, "color_value"

    iget v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 358
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 359
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->finish()V

    .line 360
    return-void
.end method

.method public onChangeB(I)V
    .locals 10
    .param p1, "v"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 435
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v3

    .line 436
    .local v3, "r":I
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 437
    .local v2, "g":I
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 439
    .local v0, "b":I
    if-eq p1, v0, :cond_0

    .line 441
    move v0, p1

    .line 443
    const/16 v4, 0xff

    invoke-static {v4, v3, v2, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    .line 445
    const/4 v4, 0x3

    new-array v1, v4, [F

    .line 446
    .local v1, "f":[F
    invoke-static {v3, v2, v0, v1}, Landroid/graphics/Color;->RGBToHSV(III[F)V

    .line 448
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    aget v5, v1, v8

    aget v6, v1, v9

    const/4 v7, 0x2

    aget v7, v1, v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->setHSV(FFF)V

    .line 449
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    aget v5, v1, v8

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->setHue(F)V

    .line 451
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-direct {p0, v4, v9}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->fillColorInfo(IZ)V

    .line 453
    .end local v1    # "f":[F
    :cond_0
    return-void
.end method

.method public onChangeG(I)V
    .locals 10
    .param p1, "v"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 413
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v3

    .line 414
    .local v3, "r":I
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 415
    .local v2, "g":I
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 417
    .local v0, "b":I
    if-eq p1, v2, :cond_0

    .line 419
    move v2, p1

    .line 421
    const/16 v4, 0xff

    invoke-static {v4, v3, v2, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    .line 423
    const/4 v4, 0x3

    new-array v1, v4, [F

    .line 424
    .local v1, "f":[F
    invoke-static {v3, v2, v0, v1}, Landroid/graphics/Color;->RGBToHSV(III[F)V

    .line 426
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    aget v5, v1, v8

    aget v6, v1, v9

    const/4 v7, 0x2

    aget v7, v1, v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->setHSV(FFF)V

    .line 427
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    aget v5, v1, v8

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->setHue(F)V

    .line 429
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-direct {p0, v4, v9}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->fillColorInfo(IZ)V

    .line 431
    .end local v1    # "f":[F
    :cond_0
    return-void
.end method

.method public onChangeHue(F)V
    .locals 5
    .param p1, "hue"    # F

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 364
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 365
    .local v0, "f":[F
    aput p1, v0, v4

    .line 366
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getS()F

    move-result v1

    aput v1, v0, v2

    .line 367
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getV()F

    move-result v1

    aput v1, v0, v3

    .line 369
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    .line 371
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    aget v2, v0, v2

    aget v3, v0, v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->setHSV(FFF)V

    .line 373
    iget v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-direct {p0, v1, v4}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->fillColorInfo(IZ)V

    .line 374
    return-void
.end method

.method public onChangeR(I)V
    .locals 10
    .param p1, "v"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 391
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v3

    .line 392
    .local v3, "r":I
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 393
    .local v2, "g":I
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 395
    .local v0, "b":I
    if-eq p1, v3, :cond_0

    .line 397
    move v3, p1

    .line 399
    const/16 v4, 0xff

    invoke-static {v4, v3, v2, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    .line 401
    const/4 v4, 0x3

    new-array v1, v4, [F

    .line 402
    .local v1, "f":[F
    invoke-static {v3, v2, v0, v1}, Landroid/graphics/Color;->RGBToHSV(III[F)V

    .line 404
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    aget v5, v1, v8

    aget v6, v1, v9

    const/4 v7, 0x2

    aget v7, v1, v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->setHSV(FFF)V

    .line 405
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    aget v5, v1, v8

    invoke-virtual {v4, v5}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->setHue(F)V

    .line 407
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-direct {p0, v4, v9}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->fillColorInfo(IZ)V

    .line 409
    .end local v1    # "f":[F
    :cond_0
    return-void
.end method

.method public onChangeSV(FF)V
    .locals 3
    .param p1, "s"    # F
    .param p2, "v"    # F

    .prologue
    const/4 v2, 0x0

    .line 379
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 380
    .local v0, "f":[F
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->getH()F

    move-result v1

    aput v1, v0, v2

    .line 381
    const/4 v1, 0x1

    aput p1, v0, v1

    .line 382
    const/4 v1, 0x2

    aput p2, v0, v1

    .line 384
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    .line 386
    iget v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-direct {p0, v1, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->fillColorInfo(IZ)V

    .line 387
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x2

    const/high16 v7, 0x41800000    # 16.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 59
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v2, 0x7f03000f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->setContentView(I)V

    .line 63
    new-instance v2, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;

    .line 64
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 65
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v3, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 66
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v2, v3}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 68
    new-instance v2, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v3, 0x7f0b001d

    const/16 v4, 0x33

    invoke-direct {v2, p0, v3, v8, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 69
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v3, 0x7f070105

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 70
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 73
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 74
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "color_type"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mType:I

    .line 75
    const-string/jumbo v2, "color_value"

    const/high16 v3, -0x10000

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    .line 77
    const v2, 0x7f0b004e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorView:Lcom/infraware/polarisoffice5/common/ColorPickerColorView;

    .line 79
    const v2, 0x7f0b0050

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    .line 80
    const v2, 0x7f0b0051

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    .line 81
    const v2, 0x7f0b0052

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    .line 83
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_0

    .line 88
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    invoke-virtual {v2, v5, v7}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 89
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    invoke-virtual {v2, v5, v7}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 90
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    invoke-virtual {v2, v5, v7}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 95
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorView:Lcom/infraware/polarisoffice5/common/ColorPickerColorView;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/ColorPickerColorView;->setOrgColor(I)V

    .line 96
    iget v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-direct {p0, v2, v6}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->fillColorInfo(IZ)V

    .line 98
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 101
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 102
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 104
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->resetColorWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 105
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->resetColorWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 106
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->resetColorWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 108
    const v2, 0x7f0b0053

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    .line 109
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    invoke-virtual {v2, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->setParent(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V

    .line 110
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->Init()V

    .line 111
    const v2, 0x7f0b0054

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    .line 112
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    invoke-virtual {v2, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->setParent(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V

    .line 113
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->Init()V

    .line 115
    const/4 v2, 0x3

    new-array v0, v2, [F

    .line 116
    .local v0, "f":[F
    iget v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColor:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-static {v2, v3, v4, v0}, Landroid/graphics/Color;->RGBToHSV(III[F)V

    .line 118
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mHueView:Lcom/infraware/polarisoffice5/common/ColorPickerHueView;

    aget v3, v0, v6

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/ColorPickerHueView;->setHue(F)V

    .line 119
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainView:Lcom/infraware/polarisoffice5/common/ColorPickerMainView;

    aget v3, v0, v6

    aget v4, v0, v5

    aget v5, v0, v8

    invoke-virtual {v2, v3, v4, v5}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->setHSV(FFF)V

    .line 121
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_ANDROID_KEYPAD()Z

    move-result v2

    if-nez v2, :cond_1

    .line 122
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    new-instance v3, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$1;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 139
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    new-instance v3, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$2;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$2;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 156
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    new-instance v3, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$3;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$3;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 194
    :goto_0
    return-void

    .line 174
    :cond_1
    const v2, 0x7f0b004c

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainLayout:Landroid/widget/LinearLayout;

    .line 175
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mMainLayout:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$4;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity$4;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/ColorPickerActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 489
    :cond_0
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onDestroy()V

    .line 490
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 464
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 465
    sparse-switch p1, :sswitch_data_0

    .line 479
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 467
    :sswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->finish()V

    goto :goto_0

    .line 470
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 471
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    goto :goto_0

    .line 472
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 473
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    goto :goto_0

    .line 474
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 475
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    goto :goto_0

    .line 465
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x42 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLocaleChange(I)V
    .locals 0
    .param p1, "nLocale"    # I

    .prologue
    .line 460
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorRedInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorBlueInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 305
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->mColorGreenInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 307
    :cond_2
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onPause()V

    .line 309
    return-void
.end method
