.class Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;
.super Ljava/lang/Object;
.source "VideoFrameLayout.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoViewListener()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 263
    const-string/jumbo v0, "VideoFrameLayout"

    const-string/jumbo v1, "onPrepared"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$100(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    # setter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoW:I
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$402(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;I)I

    .line 269
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    # setter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoH:I
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$502(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;I)I

    .line 271
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoW:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$400(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoH:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$500(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)I

    move-result v0

    if-nez v0, :cond_4

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    .line 274
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 275
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 282
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->showVideoProgress(Z)V

    .line 283
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$300(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 284
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$600(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$600(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$600(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IDocumentModified_Editor()Z

    move-result v1

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_bBackupDocModifiedFlag:Z

    .line 289
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    goto :goto_0

    .line 280
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->resizeVideo()V

    goto :goto_1
.end method
