.class public Lcom/infraware/polarisoffice5/common/WheelView;
.super Landroid/widget/TextView;
.source "WheelView.java"


# instance fields
.field private mBeforeSpace:I

.field private mColorSelection:I

.field private mColorSide:I

.field private mCurrentPosition:I

.field mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDataSize:I

.field private mDataType:I

.field private mOneUnitWidth:I

.field private mPaint:Landroid/graphics/Paint;

.field private mParentWidth:I

.field private mScroll:Lcom/infraware/polarisoffice5/common/WheelScroll;

.field private mTextSizeSelection:I

.field private mTextSizeSide:I

.field private mUnitCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mScroll:Lcom/infraware/polarisoffice5/common/WheelScroll;

    .line 24
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    .line 25
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    .line 30
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    .line 35
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mParentWidth:I

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mScroll:Lcom/infraware/polarisoffice5/common/WheelScroll;

    .line 24
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    .line 25
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    .line 30
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    .line 35
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mParentWidth:I

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mScroll:Lcom/infraware/polarisoffice5/common/WheelScroll;

    .line 24
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    .line 25
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    .line 30
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    .line 35
    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mParentWidth:I

    .line 45
    return-void
.end method

.method private drawText(Landroid/graphics/Canvas;IZ)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "position"    # I
    .param p3, "bSelect"    # Z

    .prologue
    const/high16 v8, 0x42c80000    # 100.0f

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 118
    if-ltz p2, :cond_0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-le p2, v5, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    if-eqz p3, :cond_2

    .line 123
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mColorSelection:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 124
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mTextSizeSelection:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 132
    :goto_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 133
    .local v2, "tvrc":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    const-string/jumbo v6, "1"

    invoke-virtual {v5, v6, v9, v7, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 134
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelView;->getHeight()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v4, v5, -0x1

    .line 136
    .local v4, "y":I
    const/4 v3, 0x0

    .line 137
    .local v3, "x":I
    const/4 v1, 0x0

    .line 138
    .local v1, "text":Ljava/lang/String;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 139
    .local v0, "data":I
    iget v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mDataType:I

    packed-switch v5, :pswitch_data_0

    .line 172
    :goto_2
    iget v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mBeforeSpace:I

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    mul-int/2addr v6, p2

    add-int/2addr v5, v6

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-int v7, v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    add-int v3, v5, v6

    .line 173
    int-to-float v5, v3

    int-to-float v6, v4

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 128
    .end local v0    # "data":I
    .end local v1    # "text":Ljava/lang/String;
    .end local v2    # "tvrc":Landroid/graphics/Rect;
    .end local v3    # "x":I
    .end local v4    # "y":I
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mColorSide:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 129
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mTextSizeSide:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_1

    .line 142
    .restart local v0    # "data":I
    .restart local v1    # "text":Ljava/lang/String;
    .restart local v2    # "tvrc":Landroid/graphics/Rect;
    .restart local v3    # "x":I
    .restart local v4    # "y":I
    :pswitch_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 143
    goto :goto_2

    .line 146
    :pswitch_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 147
    rem-int/lit8 v5, v0, 0xa

    if-nez v5, :cond_3

    .line 148
    const-string/jumbo v5, "%d"

    new-array v6, v7, [Ljava/lang/Object;

    div-int/lit8 v7, v0, 0xa

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 150
    :cond_3
    const-string/jumbo v5, "%1.1f"

    new-array v6, v7, [Ljava/lang/Object;

    int-to-float v7, v0

    const/high16 v8, 0x41200000    # 10.0f

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 152
    goto :goto_2

    .line 155
    :pswitch_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 156
    rem-int/lit8 v5, v0, 0x64

    if-nez v5, :cond_4

    .line 158
    const-string/jumbo v5, "%d"

    new-array v6, v7, [Ljava/lang/Object;

    div-int/lit8 v7, v0, 0x64

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 160
    :cond_4
    rem-int/lit8 v5, v0, 0xa

    if-nez v5, :cond_5

    .line 162
    const-string/jumbo v5, "%1.1f"

    new-array v6, v7, [Ljava/lang/Object;

    int-to-float v7, v0

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 166
    :cond_5
    const-string/jumbo v5, "%1.2f"

    new-array v6, v7, [Ljava/lang/Object;

    int-to-float v7, v0

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getChildPosition(I)I
    .locals 3
    .param p1, "scroll"    # I

    .prologue
    .line 197
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    rem-int v1, p1, v2

    .line 198
    .local v1, "remain":I
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    div-int v0, p1, v2

    .line 200
    .local v0, "nextPosition":I
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    sub-int/2addr v2, v1

    if-le v1, v2, :cond_0

    .line 201
    add-int/lit8 v0, v0, 0x1

    .line 203
    .end local v0    # "nextPosition":I
    :cond_0
    return v0
.end method

.method public getChildScrollDistance(I)I
    .locals 1
    .param p1, "childPosition"    # I

    .prologue
    .line 193
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    mul-int/2addr v0, p1

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mCurrentPosition:I

    return v0
.end method

.method public getData(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 213
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 214
    .local v0, "data":I
    return v0
.end method

.method public getDataCount()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mDataSize:I

    return v0
.end method

.method public getVisibleWidth()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mParentWidth:I

    return v0
.end method

.method public indexOf(I)I
    .locals 2
    .param p1, "data"    # I

    .prologue
    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public init(Lcom/infraware/polarisoffice5/common/WheelScroll;Ljava/util/ArrayList;IIIII)V
    .locals 2
    .param p1, "scroll"    # Lcom/infraware/polarisoffice5/common/WheelScroll;
    .param p3, "dataType"    # I
    .param p4, "textSizeSelection"    # I
    .param p5, "TextSizeSide"    # I
    .param p6, "visibleViews"    # I
    .param p7, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/infraware/polarisoffice5/common/WheelScroll;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;IIIII)V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, -0x1

    .line 59
    iput p6, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    .line 61
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mScroll:Lcom/infraware/polarisoffice5/common/WheelScroll;

    .line 63
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    .line 65
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mDataSize:I

    .line 67
    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mCurrentPosition:I

    .line 69
    iput p3, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mDataType:I

    .line 71
    const v0, -0xff836d

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mColorSelection:I

    .line 72
    const v0, -0xc3c3c4

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mColorSide:I

    .line 75
    const/16 v0, 0x1e

    if-ne p7, v0, :cond_0

    .line 76
    const v0, -0xa34b01

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mColorSelection:I

    .line 77
    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mColorSide:I

    .line 80
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 84
    iput p4, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mTextSizeSelection:I

    .line 85
    iput p5, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mTextSizeSide:I

    .line 86
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 92
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 94
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mScroll:Lcom/infraware/polarisoffice5/common/WheelScroll;

    if-eqz v1, :cond_0

    .line 101
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    if-eqz v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mScroll:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 106
    .local v0, "nPos":I
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    if-le v1, v4, :cond_2

    .line 107
    add-int/lit8 v1, v0, -0x3

    invoke-direct {p0, p1, v1, v3}, Lcom/infraware/polarisoffice5/common/WheelView;->drawText(Landroid/graphics/Canvas;IZ)V

    .line 108
    :cond_2
    add-int/lit8 v1, v0, -0x2

    invoke-direct {p0, p1, v1, v3}, Lcom/infraware/polarisoffice5/common/WheelView;->drawText(Landroid/graphics/Canvas;IZ)V

    .line 109
    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, p1, v1, v3}, Lcom/infraware/polarisoffice5/common/WheelView;->drawText(Landroid/graphics/Canvas;IZ)V

    .line 110
    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/infraware/polarisoffice5/common/WheelView;->drawText(Landroid/graphics/Canvas;IZ)V

    .line 111
    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, p1, v1, v3}, Lcom/infraware/polarisoffice5/common/WheelView;->drawText(Landroid/graphics/Canvas;IZ)V

    .line 112
    add-int/lit8 v1, v0, 0x2

    invoke-direct {p0, p1, v1, v3}, Lcom/infraware/polarisoffice5/common/WheelView;->drawText(Landroid/graphics/Canvas;IZ)V

    .line 113
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    if-le v1, v4, :cond_0

    .line 114
    add-int/lit8 v1, v0, 0x3

    invoke-direct {p0, p1, v1, v3}, Lcom/infraware/polarisoffice5/common/WheelView;->drawText(Landroid/graphics/Canvas;IZ)V

    goto :goto_0
.end method

.method public setCurrentPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mCurrentPosition:I

    .line 178
    return-void
.end method

.method public setParentWidth(I)V
    .locals 3
    .param p1, "width"    # I

    .prologue
    .line 220
    iput p1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mParentWidth:I

    .line 221
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    if-eqz v1, :cond_0

    .line 223
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mParentWidth:I

    if-lez v1, :cond_0

    .line 225
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mParentWidth:I

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    .line 227
    const/4 v0, 0x0

    .line 229
    .local v0, "wid":I
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mUnitCount:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 230
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    mul-int v0, v1, v2

    .line 231
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mBeforeSpace:I

    .line 237
    :goto_0
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/WheelView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 242
    .end local v0    # "wid":I
    :cond_0
    return-void

    .line 233
    .restart local v0    # "wid":I
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    mul-int v0, v1, v2

    .line 234
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mOneUnitWidth:I

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelView;->mBeforeSpace:I

    goto :goto_0
.end method
