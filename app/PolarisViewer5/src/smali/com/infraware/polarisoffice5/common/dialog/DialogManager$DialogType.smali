.class public final enum Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
.super Ljava/lang/Enum;
.source "DialogManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/dialog/DialogManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DialogType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum EDIT_PROTECT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum PRINT_NO_CONTENT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum PROTECT_READ_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum READ_PASSWORD_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum REPLACE_RESULT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum SEARCH_RESULT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum SHEET_DELETE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum SHEET_MERGE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

.field public static final enum WRITE_PASSWORD_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;


# instance fields
.field private mClass:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "SEARCH_RESULT"

    const-class v2, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {v0, v1, v4, v2}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->SEARCH_RESULT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 17
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "REPLACE_RESULT"

    const-class v2, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {v0, v1, v5, v2}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->REPLACE_RESULT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 18
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "PROTECT_READ_ONLY"

    const-class v2, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {v0, v1, v6, v2}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->PROTECT_READ_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 19
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "SHEET_MERGE"

    const-class v2, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    invoke-direct {v0, v1, v7, v2}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->SHEET_MERGE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 20
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "SHEET_DELETE"

    const-class v2, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    invoke-direct {v0, v1, v8, v2}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->SHEET_DELETE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 21
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "WRITE_PASSWORD_ONLY"

    const/4 v2, 0x5

    const-class v3, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->WRITE_PASSWORD_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 22
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "READ_PASSWORD_ONLY"

    const/4 v2, 0x6

    const-class v3, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->READ_PASSWORD_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 23
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "EDIT_PROTECT"

    const/4 v2, 0x7

    const-class v3, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->EDIT_PROTECT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 24
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "PRINT_NO_CONTENT"

    const/16 v2, 0x8

    const-class v3, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->PRINT_NO_CONTENT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 25
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "ARARM_WLAN_USE"

    const/16 v2, 0x9

    const-class v3, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 26
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "ARARM_MOBILE_USE"

    const/16 v2, 0xa

    const-class v3, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 28
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const-string/jumbo v1, "ARARM_NETWORK_NOT_USE"

    const/16 v2, 0xb

    const-class v3, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    .line 14
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    sget-object v1, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->SEARCH_RESULT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->REPLACE_RESULT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->PROTECT_READ_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->SHEET_MERGE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->SHEET_DELETE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->WRITE_PASSWORD_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->READ_PASSWORD_ONLY:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->EDIT_PROTECT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->PRINT_NO_CONTENT:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->$VALUES:[Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .locals 0
    .param p3, "a_class"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->mClass:Ljava/lang/Class;

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    return-object v0
.end method

.method public static values()[Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->$VALUES:[Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    invoke-virtual {v0}, [Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    return-object v0
.end method


# virtual methods
.method public getDialogClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->mClass:Ljava/lang/Class;

    return-object v0
.end method
