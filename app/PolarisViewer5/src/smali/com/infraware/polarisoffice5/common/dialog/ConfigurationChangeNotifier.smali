.class public Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;
.super Ljava/lang/Object;
.source "ConfigurationChangeNotifier.java"


# instance fields
.field public observers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->observers:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public notifyObserver(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 21
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->observers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 22
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->observers:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;

    .line 24
    .local v1, "ob":Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;
    if-eqz v1, :cond_0

    .line 25
    invoke-interface {v1, p1}, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 21
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    .end local v1    # "ob":Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;
    :cond_1
    return-void
.end method

.method public registerObserver(Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;

    .prologue
    .line 11
    if-eqz p1, :cond_0

    .line 12
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 13
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;

    .prologue
    .line 16
    if-eqz p1, :cond_0

    .line 17
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 18
    :cond_0
    return-void
.end method
