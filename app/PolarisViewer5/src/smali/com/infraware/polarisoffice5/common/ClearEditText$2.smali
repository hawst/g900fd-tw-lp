.class Lcom/infraware/polarisoffice5/common/ClearEditText$2;
.super Ljava/lang/Object;
.source "ClearEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/ClearEditText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/ClearEditText;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ClearEditText$2;->this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 100
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 105
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ClearEditText$2;->this$0:Lcom/infraware/polarisoffice5/common/ClearEditText;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ClearEditText;->handleClearButton()V

    .line 95
    return-void
.end method
