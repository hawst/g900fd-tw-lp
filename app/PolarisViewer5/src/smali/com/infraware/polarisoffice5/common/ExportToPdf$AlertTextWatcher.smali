.class Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;
.super Ljava/lang/Object;
.source "ExportToPdf.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/ExportToPdf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlertTextWatcher"
.end annotation


# instance fields
.field private button:Landroid/widget/Button;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/common/ExportToPdf;Landroid/widget/Button;)V
    .locals 0
    .param p2, "button"    # Landroid/widget/Button;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;->button:Landroid/widget/Button;

    .line 81
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 85
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 89
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v4, 0x0

    .line 93
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    .local v1, "value":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 96
    .local v0, "nLen":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    # getter for: Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->access$000(Lcom/infraware/polarisoffice5/common/ExportToPdf;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 98
    if-nez v0, :cond_0

    .line 100
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v2, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 113
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    # invokes: Lcom/infraware/polarisoffice5/common/ExportToPdf;->checkFile(Ljava/lang/String;)Z
    invoke-static {v2, v1}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->access$100(Lcom/infraware/polarisoffice5/common/ExportToPdf;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 107
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    # getter for: Lcom/infraware/polarisoffice5/common/ExportToPdf;->mFilePathErrorLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->access$000(Lcom/infraware/polarisoffice5/common/ExportToPdf;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 108
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v2, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$AlertTextWatcher;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/ExportToPdf;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    goto :goto_0
.end method
