.class public Lcom/infraware/polarisoffice5/common/keypad/SheetKeypadUtil;
.super Ljava/lang/Object;
.source "SheetKeypadUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSelectRemoveString(Ljava/lang/String;II)Ljava/lang/String;
    .locals 7
    .param p0, "inputText"    # Ljava/lang/String;
    .param p1, "selectStart"    # I
    .param p2, "selectEnd"    # I

    .prologue
    .line 65
    const-string/jumbo v0, ""

    .line 67
    .local v0, "convertString":Ljava/lang/String;
    sub-int v4, p2, p1

    .line 68
    .local v4, "selectLength":I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 69
    .local v2, "inputArray":[C
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    sub-int v6, p2, p1

    sub-int/2addr v5, v6

    new-array v3, v5, [C

    .line 70
    .local v3, "replaceArray":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 71
    if-ge v1, p1, :cond_1

    .line 72
    aget-char v5, v2, v1

    aput-char v5, v3, v1

    .line 70
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    :cond_1
    if-gt p1, v1, :cond_2

    if-lt v1, p2, :cond_0

    .line 76
    :cond_2
    sub-int v5, v1, v4

    aget-char v6, v2, v1

    aput-char v6, v3, v5

    goto :goto_1

    .line 79
    :cond_3
    invoke-static {v3}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    .line 80
    return-object v0
.end method

.method public static getTextIndex(Landroid/app/Activity;Landroid/widget/EditText;Landroid/view/MotionEvent;)I
    .locals 10
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "v"    # Landroid/widget/EditText;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 16
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v5, v7

    .line 17
    .local v5, "positionX":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v6, v7

    .line 19
    .local v6, "positionY":I
    if-nez p1, :cond_0

    .line 20
    const/4 v4, 0x0

    .line 52
    :goto_0
    return v4

    .line 22
    :cond_0
    const/4 v4, 0x0

    .line 25
    .local v4, "off":I
    :try_start_0
    invoke-virtual {p1}, Landroid/widget/EditText;->getTotalPaddingLeft()I

    move-result v7

    sub-int/2addr v5, v7

    .line 26
    invoke-virtual {p1}, Landroid/widget/EditText;->getTotalPaddingTop()I

    move-result v7

    sub-int/2addr v6, v7

    .line 28
    invoke-virtual {p1}, Landroid/widget/EditText;->getScrollX()I

    move-result v7

    add-int/2addr v5, v7

    .line 29
    invoke-virtual {p1}, Landroid/widget/EditText;->getScrollY()I

    move-result v7

    add-int/2addr v6, v7

    .line 31
    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/keypad/SheetKeypadUtil;->getcontentViewTop(Landroid/app/Activity;)I

    move-result v7

    sub-int/2addr v6, v7

    .line 33
    invoke-virtual {p1}, Landroid/widget/EditText;->getLineHeight()I

    move-result v7

    div-int v3, v6, v7

    .line 35
    .local v3, "lines":I
    invoke-virtual {p1}, Landroid/widget/EditText;->getLineHeight()I

    move-result v7

    rem-int v7, v6, v7

    if-eqz v7, :cond_1

    .line 36
    add-int/lit8 v3, v3, 0x1

    .line 39
    :cond_1
    invoke-virtual {p1}, Landroid/widget/EditText;->getLineCount()I

    move-result v7

    if-le v3, v7, :cond_2

    .line 40
    const/4 v3, -0x1

    .line 43
    :cond_2
    invoke-virtual {p1}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 45
    .local v1, "layout":Landroid/text/Layout;
    invoke-virtual {v1, v6}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v2

    .line 46
    .local v2, "line":I
    int-to-float v7, v5

    invoke-virtual {v1, v2, v7}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    goto :goto_0

    .line 47
    .end local v1    # "layout":Landroid/text/Layout;
    .end local v2    # "line":I
    .end local v3    # "lines":I
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v7, "#####"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception has occured : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static getcontentViewTop(Landroid/app/Activity;)I
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 56
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 57
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 58
    .local v2, "window":Landroid/view/Window;
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 59
    const v3, 0x1020002

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    .line 61
    .local v0, "contentViewTop":I
    return v0
.end method
