.class public interface abstract Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;
.super Ljava/lang/Object;
.source "DialogManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/dialog/DialogManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Dialogable"
.end annotation


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract dismiss()V
.end method

.method public abstract isShowing()Z
.end method

.method public abstract onLocaleChanged()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public varargs abstract show([Ljava/lang/Object;)V
.end method
