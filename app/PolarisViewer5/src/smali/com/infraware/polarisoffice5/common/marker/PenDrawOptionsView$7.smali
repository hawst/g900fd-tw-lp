.class Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;
.super Ljava/lang/Object;
.source "PenDrawOptionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 555
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$2200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v8

    .line 556
    .local v8, "view":Landroid/view/View;
    if-eqz v8, :cond_0

    .line 557
    invoke-virtual {v8, v2}, Landroid/view/View;->setSelected(Z)V

    .line 559
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b01a3

    if-ne v0, v1, :cond_1

    .line 561
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    const/16 v1, 0x3a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 562
    new-instance v7, Landroid/content/Intent;

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-direct {v7, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 564
    .local v7, "colorPicker":Landroid/content/Intent;
    const-string/jumbo v0, "color_type"

    invoke-virtual {v7, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 565
    const-string/jumbo v0, "color_value"

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$2300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 566
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    const/16 v1, 0x15

    invoke-virtual {v0, v7, v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 567
    invoke-virtual {p1, v9}, Landroid/view/View;->setSelected(Z)V

    .line 574
    .end local v7    # "colorPicker":Landroid/content/Intent;
    .end local p1    # "v":Landroid/view/View;
    :goto_0
    return-void

    .line 570
    .restart local p1    # "v":Landroid/view/View;
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$2400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 571
    invoke-virtual {p1, v9}, Landroid/view/View;->setSelected(Z)V

    .line 572
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    check-cast p1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    # setter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$2302(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I

    .line 573
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$2300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setColor(I)V

    goto :goto_0
.end method
