.class public Lcom/infraware/polarisoffice5/common/StateToggleButton;
.super Landroid/widget/CheckBox;
.source "StateToggleButton.java"


# instance fields
.field private mBtnDrawableID:I

.field protected mInit:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mBtnDrawableID:I

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mBtnDrawableID:I

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mBtnDrawableID:I

    .line 33
    return-void
.end method


# virtual methods
.method public GetState()I
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    if-eqz v0, :cond_1

    .line 44
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    const/16 v0, 0xb

    goto :goto_0

    .line 54
    :cond_2
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public SetButtonImg(I)V
    .locals 0
    .param p1, "btnID"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mBtnDrawableID:I

    .line 38
    return-void
.end method

.method public SetState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 74
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->GetState()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 105
    :goto_0
    return-void

    .line 77
    :cond_0
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 80
    :sswitch_0
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    .line 81
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->setChecked(Z)V

    goto :goto_0

    .line 84
    :sswitch_1
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    .line 85
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->setChecked(Z)V

    goto :goto_0

    .line 89
    :sswitch_2
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    .line 90
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->setChecked(Z)V

    goto :goto_0

    .line 94
    :sswitch_3
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    .line 95
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->setChecked(Z)V

    goto :goto_0

    .line 77
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0xa -> :sswitch_2
        0xb -> :sswitch_3
    .end sparse-switch
.end method

.method protected dispatchSetPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/widget/CheckBox;->dispatchSetPressed(Z)V

    .line 154
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 199
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mInit:Z

    .line 204
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/CheckBox;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 158
    invoke-super {p0}, Landroid/widget/CheckBox;->drawableStateChanged()V

    .line 159
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 1
    .param p1, "extraSpace"    # I

    .prologue
    .line 183
    invoke-super {p0, p1}, Landroid/widget/CheckBox;->onCreateDrawableState(I)[I

    move-result-object v0

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 110
    invoke-super {p0, p1}, Landroid/widget/CheckBox;->onDraw(Landroid/graphics/Canvas;)V

    .line 113
    const/4 v0, 0x0

    .line 115
    .local v0, "img":Landroid/graphics/Bitmap;
    iget v8, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mBtnDrawableID:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget v9, p0, Lcom/infraware/polarisoffice5/common/StateToggleButton;->mBtnDrawableID:I

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 135
    :cond_0
    if-eqz v0, :cond_1

    .line 137
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 138
    .local v7, "paint":Landroid/graphics/Paint;
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 140
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->getHeight()I

    move-result v1

    .line 141
    .local v1, "nHeight":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/StateToggleButton;->getWidth()I

    move-result v6

    .line 142
    .local v6, "nWidth":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 143
    .local v2, "nImgHeight":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 145
    .local v3, "nImgWidth":I
    sub-int v8, v6, v3

    div-int/lit8 v4, v8, 0x2

    .line 146
    .local v4, "nLeft":I
    sub-int v8, v1, v2

    div-int/lit8 v5, v8, 0x2

    .line 147
    .local v5, "nTop":I
    int-to-float v8, v4

    int-to-float v9, v5

    invoke-virtual {p1, v0, v8, v9, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 149
    .end local v1    # "nHeight":I
    .end local v2    # "nImgHeight":I
    .end local v3    # "nImgWidth":I
    .end local v4    # "nLeft":I
    .end local v5    # "nTop":I
    .end local v6    # "nWidth":I
    .end local v7    # "paint":Landroid/graphics/Paint;
    :cond_1
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 209
    const/4 v0, 0x0

    return v0
.end method
