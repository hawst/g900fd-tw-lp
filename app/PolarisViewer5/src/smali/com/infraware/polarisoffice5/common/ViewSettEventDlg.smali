.class public Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
.super Landroid/app/AlertDialog;
.source "ViewSettEventDlg.java"

# interfaces
.implements Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;
.implements Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$Holder;,
        Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;,
        Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    }
.end annotation


# static fields
.field public static final MEMO:Ljava/lang/String; = "MEMO"

.field public static final NO_MARGIN_VIEW:Ljava/lang/String; = "NO_MARGIN_VIEW"

.field public static final REFLOW_TEXT:Ljava/lang/String; = "REFLOW_TEXT"

.field public static final RULER:Ljava/lang/String; = "RULER"

.field public static final SINGLE_SLIDE_VIEW:Ljava/lang/String; = "SINGLE_SLIDE_VIEW"

.field public static final SMART_GUIDE:Ljava/lang/String; = "SMART_GUIDE"

.field public static final SPELL_CHECK:Ljava/lang/String; = "SPELL_CHECK"


# instance fields
.field MEMO_STR:I

.field NO_MARGIN_VIEW_STR:I

.field REFLOW_TEXT_STR:I

.field RULER_STR:I

.field SINGLE_SLIDE_VIEW_STR:I

.field SMART_GUIDE_STR:I

.field SPELL_CHECK_STR:I

.field private mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

.field public mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

.field mDoneClickListener:Landroid/content/DialogInterface$OnClickListener;

.field mEventType:I

.field mHandler:Landroid/os/Handler;

.field mListItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;",
            ">;"
        }
    .end annotation
.end field

.field mListView:Landroid/widget/ListView;

.field public mScs:Landroid/view/textservice/SpellCheckerSession;

.field public mTsm:Landroid/view/textservice/TextServicesManager;

.field mbCheck1:Z

.field mbCheck2:Z

.field mbCheck3:Z

.field mbCheck4:Z

.field mbCheck5:Z

.field mbCheck6:Z

.field mbCheck7:Z

.field mbEnable5:Z


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)V
    .locals 2
    .param p1, "context"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p2, "aEventType"    # I

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    .line 61
    const v0, 0x7f07018d

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->MEMO_STR:I

    .line 62
    const v0, 0x7f0701e1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->RULER_STR:I

    .line 63
    const v0, 0x7f07022c

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SPELL_CHECK_STR:I

    .line 64
    const v0, 0x7f07015c

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->NO_MARGIN_VIEW_STR:I

    .line 65
    const v0, 0x7f0701cc

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->REFLOW_TEXT_STR:I

    .line 66
    const v0, 0x7f070210

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SINGLE_SLIDE_VIEW_STR:I

    .line 67
    const v0, 0x7f07020f

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SMART_GUIDE_STR:I

    .line 70
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck1:Z

    .line 71
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck2:Z

    .line 72
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck3:Z

    .line 73
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck4:Z

    .line 74
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck5:Z

    .line 75
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck6:Z

    .line 76
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck7:Z

    .line 79
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbEnable5:Z

    .line 381
    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mDoneClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 427
    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$3;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 111
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 112
    iput p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mEventType:I

    .line 114
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->registerObserver(Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;)V

    .line 119
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;II)V
    .locals 2
    .param p1, "context"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p2, "theme"    # I
    .param p3, "aEventType"    # I

    .prologue
    const/4 v1, 0x0

    .line 122
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    .line 61
    const v0, 0x7f07018d

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->MEMO_STR:I

    .line 62
    const v0, 0x7f0701e1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->RULER_STR:I

    .line 63
    const v0, 0x7f07022c

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SPELL_CHECK_STR:I

    .line 64
    const v0, 0x7f07015c

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->NO_MARGIN_VIEW_STR:I

    .line 65
    const v0, 0x7f0701cc

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->REFLOW_TEXT_STR:I

    .line 66
    const v0, 0x7f070210

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SINGLE_SLIDE_VIEW_STR:I

    .line 67
    const v0, 0x7f07020f

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SMART_GUIDE_STR:I

    .line 70
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck1:Z

    .line 71
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck2:Z

    .line 72
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck3:Z

    .line 73
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck4:Z

    .line 74
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck5:Z

    .line 75
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck6:Z

    .line 76
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck7:Z

    .line 79
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbEnable5:Z

    .line 381
    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mDoneClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 427
    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$3;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 123
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 124
    iput p3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mEventType:I

    .line 126
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->registerObserver(Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;)V

    .line 131
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->isItemDefaultChecked()Z

    move-result v0

    return v0
.end method

.method private init()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 282
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 283
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f03005a

    invoke-virtual {v6, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 284
    .local v7, "parentView":Landroid/widget/LinearLayout;
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setView(Landroid/view/View;)V

    .line 285
    const v0, 0x7f07024e

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setTitle(I)V

    .line 288
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const-string/jumbo v1, "textservices"

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/textservice/TextServicesManager;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mTsm:Landroid/view/textservice/TextServicesManager;

    .line 290
    const v0, 0x102000a

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListView:Landroid/widget/ListView;

    .line 291
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    .line 294
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    if-ne v0, v9, :cond_3

    .line 296
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "MEMO"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->MEMO_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck1:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "NO_MARGIN_VIEW"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->NO_MARGIN_VIEW_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck4:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "REFLOW_TEXT"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->REFLOW_TEXT_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck5:Z

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbEnable5:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZZ)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    :cond_0
    :goto_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mEventType:I

    invoke-direct {v0, p0, v1, v2}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    .line 347
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 348
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 371
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070061

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mDoneClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 372
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setButton2(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 375
    invoke-virtual {p0, v9}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setCancelable(Z)V

    .line 376
    invoke-virtual {p0, v9}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setCanceledOnTouchOutside(Z)V

    .line 378
    return-void

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "MEMO"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->MEMO_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck1:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 305
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "RULER"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->RULER_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck2:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mTsm:Landroid/view/textservice/TextServicesManager;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v5, v1, p0, v8}, Landroid/view/textservice/TextServicesManager;->newSpellCheckerSession(Landroid/os/Bundle;Ljava/util/Locale;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;Z)Landroid/view/textservice/SpellCheckerSession;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mScs:Landroid/view/textservice/SpellCheckerSession;

    .line 308
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mScs:Landroid/view/textservice/SpellCheckerSession;

    if-eqz v0, :cond_2

    .line 309
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "SPELL_CHECK"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SPELL_CHECK_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck3:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "NO_MARGIN_VIEW"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->NO_MARGIN_VIEW_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck4:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "REFLOW_TEXT"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->REFLOW_TEXT_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck5:Z

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbEnable5:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZZ)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 317
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 319
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->IsViewerMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 321
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "SINGLE_SLIDE_VIEW"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SINGLE_SLIDE_VIEW_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck6:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "SMART_GUIDE"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SMART_GUIDE_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck7:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 326
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "SINGLE_SLIDE_VIEW"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SINGLE_SLIDE_VIEW_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck6:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "SMART_GUIDE"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SMART_GUIDE_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck7:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mTsm:Landroid/view/textservice/TextServicesManager;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v5, v1, p0, v8}, Landroid/view/textservice/TextServicesManager;->newSpellCheckerSession(Landroid/os/Bundle;Ljava/util/Locale;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;Z)Landroid/view/textservice/SpellCheckerSession;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mScs:Landroid/view/textservice/SpellCheckerSession;

    .line 330
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mScs:Landroid/view/textservice/SpellCheckerSession;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "SPELL_CHECK"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SPELL_CHECK_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck3:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 335
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_6

    .line 336
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "MEMO"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->MEMO_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck1:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "NO_MARGIN_VIEW"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->NO_MARGIN_VIEW_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck4:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "REFLOW_TEXT"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->REFLOW_TEXT_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck5:Z

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbEnable5:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZZ)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 340
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocExtensionType()I

    move-result v0

    const/16 v1, 0x25

    if-ne v0, v1, :cond_0

    .line 342
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "NO_MARGIN_VIEW"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->NO_MARGIN_VIEW_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck4:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    const-string/jumbo v2, "REFLOW_TEXT"

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->REFLOW_TEXT_STR:I

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck5:Z

    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbEnable5:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;-><init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZZ)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private isItemDefaultChecked()Z
    .locals 4

    .prologue
    .line 587
    const/4 v0, 0x0

    .line 589
    .local v0, "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    const/4 v1, 0x0

    .local v1, "position":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 590
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    check-cast v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 592
    .restart local v0    # "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "MEMO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck1:Z

    iget-boolean v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v3, :cond_6

    :cond_0
    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "RULER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck2:Z

    iget-boolean v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v3, :cond_6

    :cond_1
    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "SPELL_CHECK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck3:Z

    iget-boolean v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v3, :cond_6

    :cond_2
    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "NO_MARGIN_VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck4:Z

    iget-boolean v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v3, :cond_6

    :cond_3
    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "REFLOW_TEXT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck5:Z

    iget-boolean v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v3, :cond_6

    :cond_4
    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "SINGLE_SLIDE_VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck6:Z

    iget-boolean v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v3, :cond_6

    :cond_5
    iget-object v2, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "SMART_GUIDE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck7:Z

    iget-boolean v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-eq v2, v3, :cond_7

    .line 599
    :cond_6
    const/4 v2, 0x1

    .line 602
    :goto_1
    return v2

    .line 589
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 602
    :cond_8
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->removeObserver(Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;)V

    .line 582
    :cond_0
    invoke-super {p0}, Landroid/app/AlertDialog;->dismiss()V

    .line 583
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v4, 0x1

    .line 553
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_2

    .line 554
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 555
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 557
    .local v0, "dialogItem":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v5, "SINGLE_SLIDE_VIEW"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 559
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    instance-of v3, v3, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    if-eqz v3, :cond_0

    .line 560
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .line 562
    .local v2, "pptMainActivity":Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getContinuMode()Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v4

    :goto_1
    iput-boolean v3, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 554
    .end local v2    # "pptMainActivity":Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 562
    .restart local v2    # "pptMainActivity":Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 568
    .end local v0    # "dialogItem":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    .end local v1    # "i":I
    .end local v2    # "pptMainActivity":Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->notifyDataSetChanged()V

    .line 569
    const v3, 0x7f07024e

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setTitle(I)V

    .line 570
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    const v4, 0x7f070061

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    .line 571
    const/4 v3, -0x2

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    const v4, 0x7f07005f

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    .line 572
    return-void
.end method

.method public onGetSentenceSuggestions([Landroid/view/textservice/SentenceSuggestionsInfo;)V
    .locals 0
    .param p1, "arg0"    # [Landroid/view/textservice/SentenceSuggestionsInfo;

    .prologue
    .line 549
    return-void
.end method

.method public onGetSuggestions([Landroid/view/textservice/SuggestionsInfo;)V
    .locals 0
    .param p1, "arg0"    # [Landroid/view/textservice/SuggestionsInfo;

    .prologue
    .line 543
    return-void
.end method

.method public setDefaultChecked(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "checked"    # Z

    .prologue
    .line 134
    if-nez p1, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    const-string/jumbo v0, "MEMO"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck1:Z

    goto :goto_0

    .line 139
    :cond_2
    const-string/jumbo v0, "RULER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 140
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck2:Z

    goto :goto_0

    .line 142
    :cond_3
    const-string/jumbo v0, "SPELL_CHECK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 143
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck3:Z

    goto :goto_0

    .line 145
    :cond_4
    const-string/jumbo v0, "NO_MARGIN_VIEW"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 146
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck4:Z

    goto :goto_0

    .line 148
    :cond_5
    const-string/jumbo v0, "REFLOW_TEXT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 149
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck5:Z

    goto :goto_0

    .line 151
    :cond_6
    const-string/jumbo v0, "SINGLE_SLIDE_VIEW"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 152
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck6:Z

    goto :goto_0

    .line 154
    :cond_7
    const-string/jumbo v0, "SMART_GUIDE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbCheck7:Z

    goto :goto_0
.end method

.method public setDefaultEnable(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    .line 161
    if-nez p1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    const-string/jumbo v0, "REFLOW_TEXT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mbEnable5:Z

    goto :goto_0
.end method

.method public setEnableItem(Ljava/lang/String;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 173
    .local v1, "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v2, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->enable:Z

    goto :goto_0

    .line 176
    .end local v1    # "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->notifyDataSetChanged()V

    .line 177
    return-void
.end method

.method public setEventHandler(Landroid/os/Handler;)Landroid/app/AlertDialog;
    .locals 0
    .param p1, "aHandler"    # Landroid/os/Handler;

    .prologue
    .line 535
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mHandler:Landroid/os/Handler;

    .line 536
    return-object p0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 273
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->init()V

    .line 275
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    .line 278
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 279
    return-void
.end method

.method public uncheckOtherItems(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;)V
    .locals 6
    .param p1, "item"    # Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 181
    iget-object v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "SINGLE_SLIDE_VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "NO_MARGIN_VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 187
    iget-boolean v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v5, :cond_2

    .line 189
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 190
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 192
    .local v1, "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v2, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "REFLOW_TEXT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 193
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 200
    .end local v0    # "i":I
    .end local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    :cond_2
    iget-object v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "REFLOW_TEXT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 201
    iget-boolean v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v5, :cond_3

    .line 202
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 203
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 205
    .restart local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v2, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "NO_MARGIN_VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 206
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 214
    .end local v0    # "i":I
    .end local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    :cond_3
    iget-object v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "NO_MARGIN_VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 215
    iget-boolean v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v5, :cond_4

    .line 217
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 218
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 220
    .restart local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v2, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "RULER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 221
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 228
    .end local v0    # "i":I
    .end local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    :cond_4
    iget-object v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "RULER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 229
    iget-boolean v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v5, :cond_5

    .line 230
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 231
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 233
    .restart local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v2, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "NO_MARGIN_VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 234
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 242
    .end local v0    # "i":I
    .end local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    :cond_5
    iget-object v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "MEMO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 243
    iget-boolean v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v5, :cond_6

    .line 245
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 246
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 248
    .restart local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v2, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "REFLOW_TEXT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 249
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 256
    .end local v0    # "i":I
    .end local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    :cond_6
    iget-object v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "REFLOW_TEXT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 257
    iget-boolean v2, p1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-ne v2, v5, :cond_0

    .line 258
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_6
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 259
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 261
    .restart local v1    # "it":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-object v2, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    const-string/jumbo v3, "MEMO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 262
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    goto/16 :goto_0

    .line 189
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 202
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 217
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    .line 230
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_4

    .line 245
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 258
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_6
.end method
