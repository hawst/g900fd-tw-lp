.class Lcom/infraware/polarisoffice5/common/QuickScrollView$1;
.super Ljava/lang/Object;
.source "QuickScrollView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/QuickScrollView;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/QuickScrollView;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 115
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 116
    .local v2, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    .line 118
    .local v3, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 172
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # setter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z
    invoke-static {v4, v5}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$202(Lcom/infraware/polarisoffice5/common/QuickScrollView;Z)Z

    :cond_0
    :goto_0
    move v4, v5

    .line 176
    :goto_1
    return v4

    .line 122
    :pswitch_0
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->initGuideTextToSpeech()V

    .line 124
    new-instance v1, Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I
    invoke-static {v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$000(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I

    move-result v6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    iget v7, v7, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_WIDTH:I

    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I
    invoke-static {v8}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$000(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I

    move-result v8

    iget-object v9, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    iget v9, v9, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_HEIGHT:I

    add-int/2addr v8, v9

    invoke-direct {v1, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 125
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 126
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I
    invoke-static {v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$000(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I

    move-result v6

    sub-int v6, v3, v6

    # setter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mDownPos:I
    invoke-static {v5, v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$102(Lcom/infraware/polarisoffice5/common/QuickScrollView;I)I

    .line 127
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # setter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z
    invoke-static {v5, v4}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$202(Lcom/infraware/polarisoffice5/common/QuickScrollView;Z)Z

    goto :goto_1

    .line 137
    .end local v1    # "rect":Landroid/graphics/Rect;
    :pswitch_1
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->initGuideTextToSpeech()V

    .line 139
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z
    invoke-static {v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$200(Lcom/infraware/polarisoffice5/common/QuickScrollView;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 141
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mDownPos:I
    invoke-static {v5}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$100(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I

    move-result v5

    sub-int v0, v3, v5

    .line 143
    .local v0, "calc":I
    if-gez v0, :cond_1

    .line 144
    const/4 v0, 0x0

    .line 146
    :cond_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I
    invoke-static {v5}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$300(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I

    move-result v5

    if-le v0, v5, :cond_2

    .line 147
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I
    invoke-static {v5}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$300(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I

    move-result v0

    .line 149
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I
    invoke-static {v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$300(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I

    move-result v6

    # invokes: Lcom/infraware/polarisoffice5/common/QuickScrollView;->Pos2Scroll(II)V
    invoke-static {v5, v0, v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$400(Lcom/infraware/polarisoffice5/common/QuickScrollView;II)V

    goto :goto_1

    .line 156
    .end local v0    # "calc":I
    :pswitch_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # setter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z
    invoke-static {v4, v5}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$202(Lcom/infraware/polarisoffice5/common/QuickScrollView;Z)Z

    .line 157
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->onGuideStartTexToSpeech()V

    goto :goto_0

    .line 161
    :pswitch_3
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->hide()V

    .line 164
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # setter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z
    invoke-static {v6, v5}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$202(Lcom/infraware/polarisoffice5/common/QuickScrollView;Z)Z

    .line 165
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # invokes: Lcom/infraware/polarisoffice5/common/QuickScrollView;->showScroll()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$500(Lcom/infraware/polarisoffice5/common/QuickScrollView;)V

    .line 167
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->onGuideStartTexToSpeech()V

    goto/16 :goto_1

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
