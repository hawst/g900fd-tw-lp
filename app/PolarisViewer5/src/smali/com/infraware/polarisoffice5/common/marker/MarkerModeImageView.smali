.class public Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;
.super Landroid/widget/ImageView;
.source "MarkerModeImageView.java"


# instance fields
.field private mColorRect:Landroid/graphics/Rect;

.field private mPaint:Landroid/graphics/Paint;

.field private mbMarker:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 13
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mPaint:Landroid/graphics/Paint;

    .line 14
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mbMarker:Z

    .line 19
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->init()V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mPaint:Landroid/graphics/Paint;

    .line 14
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mbMarker:Z

    .line 24
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->init()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 13
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mPaint:Landroid/graphics/Paint;

    .line 14
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mbMarker:Z

    .line 29
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->init()V

    .line 30
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 33
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mPaint:Landroid/graphics/Paint;

    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 35
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mPaint:Landroid/graphics/Paint;

    const v1, -0x1cd2f1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    .line 37
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-static {v1, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x41eaa3d7    # 29.33f

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x42000000    # 32.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x41f55c29    # 30.67f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 40
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 67
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mbMarker:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 69
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 9
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 52
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 53
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    if-eqz v3, :cond_0

    if-eq p1, p2, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x42100000    # 36.0f

    invoke-static {v3, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v1

    .line 55
    .local v1, "size":I
    sub-int v3, p1, v1

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 56
    .local v0, "left":I
    sub-int v3, p2, v1

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 57
    .local v2, "top":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x40c00000    # 6.0f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x41eaa3d7    # 29.33f

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x42000000    # 32.0f

    invoke-static {v6, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x41f55c29    # 30.67f

    invoke-static {v7, v8}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 60
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mColorRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 62
    .end local v0    # "left":I
    .end local v1    # "size":I
    .end local v2    # "top":I
    :cond_0
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    return-void
.end method

.method public setMarker(Z)V
    .locals 0
    .param p1, "bMarker"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerModeImageView;->mbMarker:Z

    .line 44
    return-void
.end method
