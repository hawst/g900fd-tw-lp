.class Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
.super Ljava/lang/Object;
.source "ViewSettEventDlg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DialogItem"
.end annotation


# instance fields
.field checked:Z

.field enable:Z

.field key:Ljava/lang/String;

.field text_res_id:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZ)V
    .locals 1
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "text_res_id"    # I
    .param p4, "checked"    # Z

    .prologue
    .line 87
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->enable:Z

    .line 88
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    .line 89
    iput p3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->text_res_id:I

    .line 90
    iput-boolean p4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 91
    return-void
.end method

.method public constructor <init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;Ljava/lang/String;IZZ)V
    .locals 1
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "text_res_id"    # I
    .param p4, "checked"    # Z
    .param p5, "enable"    # Z

    .prologue
    .line 93
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->enable:Z

    .line 94
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->key:Ljava/lang/String;

    .line 95
    iput p3, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->text_res_id:I

    .line 96
    iput-boolean p4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 97
    iput-boolean p5, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->enable:Z

    .line 98
    return-void
.end method


# virtual methods
.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 102
    return-void
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->enable:Z

    .line 106
    return-void
.end method
