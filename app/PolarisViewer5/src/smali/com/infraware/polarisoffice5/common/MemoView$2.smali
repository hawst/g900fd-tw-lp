.class Lcom/infraware/polarisoffice5/common/MemoView$2;
.super Ljava/lang/Object;
.source "MemoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/MemoView;->init(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/MemoView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/MemoView;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, -0x1

    .line 125
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$000(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 126
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetNextCommentText()V

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$200(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v3

    if-eq v3, v5, :cond_0

    .line 132
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 133
    .local v0, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$200(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v3

    iput v3, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 134
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    const/16 v4, 0xb

    invoke-virtual {v3, v4, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 135
    iget v3, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    if-eq v3, v5, :cond_0

    .line 137
    iget v2, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 138
    .local v2, "next_id":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$300(Lcom/infraware/polarisoffice5/common/MemoView;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 141
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->getText()Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "newset":Ljava/lang/String;
    iput-object v1, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strMemo:Ljava/lang/String;

    .line 143
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$200(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v3

    iput v3, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 145
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 148
    .end local v1    # "newset":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v3, v2}, Lcom/infraware/polarisoffice5/common/MemoView;->setActiveMemoId(I)V

    .line 149
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->clear()V

    .line 151
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$200(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v3

    if-eq v3, v5, :cond_3

    .line 153
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$200(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v3

    iput v3, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 154
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView$2;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    const/16 v4, 0xc

    invoke-virtual {v3, v4, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 156
    :cond_3
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->clear()V

    goto :goto_0
.end method
