.class public Lcom/infraware/polarisoffice5/common/PasswordActivity;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "PasswordActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;
    }
.end annotation


# instance fields
.field private mEditText:Landroid/widget/EditText;

.field mPasswordTextWatcher:Landroid/text/TextWatcher;

.field private mTextView:Landroid/widget/TextView;

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;

.field private mbRefail:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 24
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mEditText:Landroid/widget/EditText;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mbRefail:Z

    .line 26
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mTextView:Landroid/widget/TextView;

    .line 29
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;

    .line 30
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 69
    new-instance v0, Lcom/infraware/polarisoffice5/common/PasswordActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/PasswordActivity$1;-><init>(Lcom/infraware/polarisoffice5/common/PasswordActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mPasswordTextWatcher:Landroid/text/TextWatcher;

    .line 171
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/PasswordActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/PasswordActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/PasswordActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/PasswordActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public actionTitleBarButtonClick()V
    .locals 3

    .prologue
    .line 145
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "password":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 148
    .local v1, "result":Landroid/content/Intent;
    const-string/jumbo v2, "key_new_file"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->setResult(ILandroid/content/Intent;)V

    .line 150
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->finish()V

    .line 156
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 121
    .local v0, "result":Landroid/content/Intent;
    const-string/jumbo v1, "key_new_file"

    const-string/jumbo v2, "exit"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->setResult(ILandroid/content/Intent;)V

    .line 123
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->finish()V

    .line 124
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onBackPressed()V

    .line 125
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 46
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->setContentView(I)V

    .line 51
    new-instance v0, Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/common/PasswordActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;

    .line 52
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 53
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "REFAIL"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 57
    const v0, 0x7f0b017e

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mEditText:Landroid/widget/EditText;

    .line 58
    const v0, 0x7f0b017f

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mTextView:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mTextView:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 60
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 61
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mPasswordTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 62
    new-instance v0, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f0b001d

    const/4 v2, 0x2

    const/16 v3, 0x46

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 63
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f07009d

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 64
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 65
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0, v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 66
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "REFAIL"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mbRefail:Z

    .line 67
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/common/PasswordActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 107
    :cond_0
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onDestroy()V

    .line 108
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 0
    .param p1, "nLocale"    # I

    .prologue
    .line 114
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 133
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onPause()V

    .line 134
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onResume()V

    .line 36
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mbRefail:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 39
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 41
    :cond_0
    return-void
.end method
