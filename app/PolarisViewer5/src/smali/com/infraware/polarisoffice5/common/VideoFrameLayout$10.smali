.class Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;
.super Ljava/lang/Object;
.source "VideoFrameLayout.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoViewListener()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v4, 0x1

    .line 355
    const-string/jumbo v1, "VideoFrameLayout"

    const-string/jumbo v2, "onError"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$100(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 372
    :goto_0
    return v4

    .line 359
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->removeVideoCtrl()V

    .line 361
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$600(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v2, v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 363
    .local v0, "oDialog":Landroid/app/AlertDialog;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$600(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v1

    const v2, 0x7f0702e4

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 364
    const/4 v1, -0x2

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    # getter for: Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->access$600(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v2

    const v3, 0x7f070063

    invoke-virtual {v2, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10$1;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 371
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
