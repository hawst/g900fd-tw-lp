.class public Lcom/infraware/polarisoffice5/common/ExternalCallManager;
.super Ljava/lang/Object;
.source "ExternalCallManager.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCTYPE;


# static fields
.field private static final Dictionary_URL_infix_wiki:Ljava/lang/String; = ".wikipedia.org/wiki/"

.field private static final Dictionary_URL_prefix_wiki:Ljava/lang/String; = "http://"

.field private static m_strLookupUrl:Ljava/lang/String;

.field private static m_strLookupWord:Ljava/lang/String;

.field private static m_strTargetLanguage:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;)V
    .locals 0
    .param p1, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p2, "hyperlink"    # Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static buildLookupUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "select"    # Ljava/lang/String;
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 114
    sput-object p1, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->m_strTargetLanguage:Ljava/lang/String;

    .line 115
    sput-object p0, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->m_strLookupWord:Ljava/lang/String;

    .line 119
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->m_strTargetLanguage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".wikipedia.org/wiki/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->m_strLookupWord:Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-static {v2, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->m_strLookupUrl:Ljava/lang/String;

    .line 121
    sget-object v1, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->m_strLookupUrl:Ljava/lang/String;

    const-string/jumbo v2, "\\+"

    const-string/jumbo v3, "%20"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->m_strLookupUrl:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public static removeHyperlink(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;)V
    .locals 3
    .param p0, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "hyperlink"    # Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;

    .prologue
    .line 56
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HYPERLINK()Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p1, Lcom/infraware/office/evengine/EV$HYPERLINK_INFO;->szHyperText:Ljava/lang/String;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IHyperlinkEditor(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static runCall(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V
    .locals 3
    .param p0, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "hyperlink"    # Ljava/lang/String;

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.CALL"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 66
    return-void
.end method

.method public static runEmail(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V
    .locals 6
    .param p0, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "hyperlink"    # Ljava/lang/String;

    .prologue
    .line 91
    :try_start_0
    const-string/jumbo v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "address_arr":[Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v0, v1, v3

    .line 93
    .local v0, "address":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SENDTO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "mailto:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 94
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "address_arr":[Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v3

    goto :goto_0

    .line 95
    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public static runHyperlink(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V
    .locals 7
    .param p0, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "hyperlink"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HYPERLINK()Z

    move-result v4

    if-nez v4, :cond_0

    .line 53
    :goto_0
    return-void

    .line 35
    :cond_0
    const-string/jumbo v4, ":"

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 37
    .local v2, "dotdot_index":I
    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 38
    const-string/jumbo v4, "ExternalCallManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "cannot run hyperlink - data: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "dataType":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "dataContent":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 49
    :try_start_0
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v3

    .line 51
    .local v3, "e":Landroid/content/ActivityNotFoundException;
    const-string/jumbo v4, "ExternalCallManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "cannot run hyperlink - data type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "data: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static runSMS(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V
    .locals 5
    .param p0, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "hyperlink"    # Ljava/lang/String;

    .prologue
    .line 69
    const-string/jumbo v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 70
    .local v1, "phone_arr":[Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v0, v1, v3

    .line 72
    .local v0, "number":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SENDTO"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v2, "sendIntent":Landroid/content/Intent;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "smsto:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 74
    invoke-virtual {p0, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 75
    return-void
.end method

.method public static runWikipediaSearch(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 7
    .param p0, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 131
    const-string/jumbo v2, ""

    .line 132
    .local v2, "select":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    move-object v3, p0

    .line 133
    check-cast v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 134
    .local v3, "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getCellText()Ljava/lang/String;

    move-result-object v2

    .line 139
    .end local v3    # "sheetActivity":Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "locale":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 141
    const-string/jumbo v1, "en"

    .line 143
    :cond_0
    if-eqz v2, :cond_1

    .line 145
    invoke-static {v2, v1}, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->buildLookupUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    sget-object v5, Lcom/infraware/polarisoffice5/common/ExternalCallManager;->m_strLookupUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 147
    .local v4, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {v0, v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 148
    .local v0, "it":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 150
    .end local v0    # "it":Landroid/content/Intent;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_1
    return-void

    .line 136
    .end local v1    # "locale":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/office/evengine/EvInterface;->IGetMarkString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static run_addContact(Lcom/infraware/office/baseframe/EvBaseViewerActivity;Ljava/lang/String;)V
    .locals 6
    .param p0, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p1, "hyperlink"    # Ljava/lang/String;

    .prologue
    .line 79
    const-string/jumbo v4, ":"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "phone_arr":[Ljava/lang/String;
    const/4 v4, 0x1

    aget-object v2, v3, v4

    .line 82
    .local v2, "number":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.INSERT"

    sget-object v5, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 83
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 84
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v4, "phone"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 87
    return-void
.end method
