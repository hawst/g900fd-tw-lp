.class Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;
.super Ljava/lang/Object;
.source "OneButtonDialog.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchResultDialog"
.end annotation


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mSearchContent:Ljava/lang/String;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V

    return-void
.end method

.method private setMessge()V
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->mBundle:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->mSearchContent:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mStrMsg:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method public varargs inflateDialog([Ljava/lang/Object;)V
    .locals 2
    .param p1, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 103
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->mBundle:Landroid/os/Bundle;

    .line 104
    const/4 v0, 0x1

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->mSearchContent:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    const/4 v0, 0x2

    aget-object v0, p1, v0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 106
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    const/4 v0, 0x3

    aget-object v0, p1, v0

    check-cast v0, Landroid/content/DialogInterface$OnCancelListener;

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 107
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->this$0:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;

    const/4 v0, 0x4

    aget-object v0, p1, v0

    check-cast v0, Landroid/content/DialogInterface$OnKeyListener;

    iput-object v0, v1, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 109
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->setMessge()V

    .line 110
    return-void
.end method

.method public onLocaleChanged()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;->setMessge()V

    .line 115
    return-void
.end method
