.class Lcom/infraware/polarisoffice5/common/WheelButton$5;
.super Ljava/lang/Object;
.source "WheelButton.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/WheelButton;->onSizeChanged(IIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/WheelButton;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/WheelButton;)V
    .locals 0

    .prologue
    .line 952
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton$5;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 955
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton$5;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    # getter for: Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->access$300(Lcom/infraware/polarisoffice5/common/WheelButton;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 970
    :goto_0
    return-void

    .line 958
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton$5;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->getIntData()I

    move-result v0

    .line 959
    .local v0, "curData":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton$5;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    # invokes: Lcom/infraware/polarisoffice5/common/WheelButton;->setSelection(I)V
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->access$000(Lcom/infraware/polarisoffice5/common/WheelButton;I)V

    goto :goto_0

    .line 965
    .end local v0    # "curData":I
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton$5;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->getFloatData()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 966
    .local v0, "curData":F
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton$5;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    # invokes: Lcom/infraware/polarisoffice5/common/WheelButton;->setSelection(F)V
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->access$400(Lcom/infraware/polarisoffice5/common/WheelButton;F)V

    goto :goto_0

    .line 955
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
