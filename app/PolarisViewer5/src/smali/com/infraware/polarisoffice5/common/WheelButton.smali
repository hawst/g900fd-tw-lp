.class public Lcom/infraware/polarisoffice5/common/WheelButton;
.super Landroid/widget/LinearLayout;
.source "WheelButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/infraware/office/baseframe/EvBaseE$BaseActivityEventType;
.implements Lcom/infraware/polarisoffice5/common/LocaleChangeListener;
.implements Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;
.implements Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;
    }
.end annotation


# static fields
.field static final DATA_FLOAT:I = 0x1

.field static final DATA_FLOAT2:I = 0x2

.field static final DATA_INT:I = 0x0

.field public static final TYPE_03_PANEL:I = 0x4

.field public static final TYPE_03_SMALL:I = 0x2

.field public static final TYPE_03_WIDE:I = 0x3

.field public static final TYPE_05:I = 0x5


# instance fields
.field private mButtonUint:I

.field private mContext:Landroid/content/Context;

.field private mCurrentSelection:I

.field protected mDecButton:Landroid/widget/ImageButton;

.field private mEnd:Z

.field private mFontSizeCenter:I

.field private mFontSizeSide:I

.field protected mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

.field private mHandler:Landroid/os/Handler;

.field protected mInsButton:Landroid/widget/ImageButton;

.field mKeyListener:Landroid/view/View$OnKeyListener;

.field private mOnKeypadListener:Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;

.field private mSpinType:I

.field private mType:I

.field private mVisibleViews:I

.field private m_bNeedCallHandler:Z

.field private m_bUseKeypad:Z

.field private m_nMax:I

.field private m_nMin:I

.field private m_nTitle:I

.field private m_nType:I

.field private m_nWheelType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 83
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 47
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bUseKeypad:Z

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mOnKeypadListener:Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;

    .line 765
    new-instance v0, Lcom/infraware/polarisoffice5/common/WheelButton$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/WheelButton$2;-><init>(Lcom/infraware/polarisoffice5/common/WheelButton;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 84
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    .line 85
    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mButtonUint:I

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    .line 87
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 88
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bUseKeypad:Z

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x1

    .line 74
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 47
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bUseKeypad:Z

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mOnKeypadListener:Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;

    .line 765
    new-instance v0, Lcom/infraware/polarisoffice5/common/WheelButton$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/WheelButton$2;-><init>(Lcom/infraware/polarisoffice5/common/WheelButton;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 75
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    .line 76
    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mButtonUint:I

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    .line 78
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 79
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bUseKeypad:Z

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/WheelButton;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelButton;
    .param p1, "x1"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setSelection(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/WheelButton;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelButton;
    .param p1, "x1"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->sendMessage(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/WheelButton;)Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelButton;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mOnKeypadListener:Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/WheelButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelButton;

    .prologue
    .line 28
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/WheelButton;F)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/WheelButton;
    .param p1, "x1"    # F

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setSelection(F)V

    return-void
.end method

.method private addFloatData(FFIFFIIII)V
    .locals 13
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F
    .param p3, "dataCount"    # I
    .param p4, "addMount"    # F
    .param p5, "initData"    # F
    .param p6, "titleId"    # I
    .param p7, "fontSizeCenter"    # I
    .param p8, "fontSizeSide"    # I
    .param p9, "visibleViews"    # I

    .prologue
    .line 724
    const/4 v1, 0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    .line 725
    const/4 v1, -0x1

    move/from16 v0, p6

    if-eq v0, v1, :cond_0

    .line 727
    move/from16 v0, p6

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    .line 728
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/infraware/polarisoffice5/common/WheelButton;->setTitle(II)V

    .line 731
    :cond_0
    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    .line 732
    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v1, p2

    float-to-int v1, v1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    .line 734
    const/high16 v1, 0x41200000    # 10.0f

    mul-float v1, v1, p5

    float-to-int v12, v1

    .line 735
    .local v12, "nInitData":I
    const/high16 v1, 0x41200000    # 10.0f

    mul-float v1, v1, p4

    float-to-int v11, v1

    .line 738
    .local v11, "nAddMount":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 739
    .local v3, "intList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, p3

    if-ge v10, v0, :cond_2

    .line 741
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    mul-int v2, v10, v11

    add-int v9, v1, v2

    .line 742
    .local v9, "data":I
    if-ne v9, v12, :cond_1

    .line 743
    iput v10, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    .line 746
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-le v9, v1, :cond_3

    .line 754
    .end local v9    # "data":I
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nWheelType:I

    move/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    invoke-virtual/range {v1 .. v8}, Lcom/infraware/polarisoffice5/common/WheelScroll;->initView(Landroid/content/Context;Ljava/util/ArrayList;IIIII)V

    .line 755
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setOnItemSelectListener(Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;)V

    .line 756
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setOnItemScrollListener(Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;)V

    .line 758
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 760
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getView()Lcom/infraware/polarisoffice5/common/WheelView;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/WheelView;->setCurrentPosition(I)V

    .line 761
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelectionDirect(I)V

    .line 762
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->setEnd()V

    .line 763
    return-void

    .line 750
    .restart local v9    # "data":I
    :cond_3
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    add-int/lit8 v10, v10, 0x1

    goto :goto_0
.end method

.method private addFloatData2(FFIFIIII)V
    .locals 9
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F
    .param p3, "dataCount"    # I
    .param p4, "initData"    # F
    .param p5, "titleId"    # I
    .param p6, "fontSizeCenter"    # I
    .param p7, "fontSizeSide"    # I
    .param p8, "visibleViews"    # I

    .prologue
    .line 651
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    .line 652
    const/4 v0, -0x1

    if-eq v0, p5, :cond_0

    .line 654
    iput p5, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    .line 655
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setTitle(II)V

    .line 658
    :cond_0
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    .line 659
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    .line 660
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, p4

    float-to-int v0, v0

    int-to-float p4, v0

    .line 665
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 666
    .local v2, "intList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    const/16 v0, 0x4b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 669
    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 670
    const/16 v0, 0x96

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    const/16 v0, 0xe1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    const/16 v0, 0x12c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 673
    const/16 v0, 0x1c2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674
    const/16 v0, 0x258

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 676
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    .line 677
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, p3, :cond_1

    .line 679
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p4, v0

    if-nez v0, :cond_3

    .line 681
    iput v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    .line 686
    :cond_1
    const/4 v0, -0x1

    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    if-ne v0, v1, :cond_2

    .line 687
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    .line 690
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    iget v7, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nWheelType:I

    move v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/polarisoffice5/common/WheelScroll;->initView(Landroid/content/Context;Ljava/util/ArrayList;IIIII)V

    .line 691
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setOnItemSelectListener(Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;)V

    .line 692
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setOnItemScrollListener(Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;)V

    .line 694
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 700
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getView()Lcom/infraware/polarisoffice5/common/WheelView;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelView;->setCurrentPosition(I)V

    .line 701
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelectionDirect(I)V

    .line 716
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->setEnd()V

    .line 717
    return-void

    .line 677
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method private addIntData(IIIIIIIII)V
    .locals 11
    .param p1, "minValue"    # I
    .param p2, "maxValue"    # I
    .param p3, "dataCount"    # I
    .param p4, "addMount"    # I
    .param p5, "initData"    # I
    .param p6, "titleId"    # I
    .param p7, "fontSizeCenter"    # I
    .param p8, "fontSizeSide"    # I
    .param p9, "visibleViews"    # I

    .prologue
    .line 576
    const/4 v1, 0x0

    iput v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    .line 577
    move/from16 v0, p6

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    .line 578
    iput p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    .line 579
    iput p2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    .line 581
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 582
    .local v3, "intList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, p3, :cond_1

    .line 584
    mul-int v1, v10, p4

    add-int v9, p1, v1

    .line 585
    .local v9, "data":I
    move/from16 v0, p5

    if-ne v9, v0, :cond_0

    .line 587
    iput v10, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    .line 590
    :cond_0
    if-le v9, p2, :cond_3

    .line 597
    .end local v9    # "data":I
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nWheelType:I

    move/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    invoke-virtual/range {v1 .. v8}, Lcom/infraware/polarisoffice5/common/WheelScroll;->initView(Landroid/content/Context;Ljava/util/ArrayList;IIIII)V

    .line 598
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setOnItemSelectListener(Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemSelectListener;)V

    .line 599
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setOnItemScrollListener(Lcom/infraware/polarisoffice5/common/WheelScroll$OnItemScrollListener;)V

    .line 601
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getView()Lcom/infraware/polarisoffice5/common/WheelView;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/WheelView;->setCurrentPosition(I)V

    .line 602
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 604
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getView()Lcom/infraware/polarisoffice5/common/WheelView;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/WheelView;->setCurrentPosition(I)V

    .line 605
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mCurrentSelection:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelectionDirect(I)V

    .line 608
    const/4 v1, -0x1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    if-eq v1, v2, :cond_2

    .line 609
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/infraware/polarisoffice5/common/WheelButton;->setTitle(II)V

    .line 611
    :cond_2
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nWheelType:I

    const/16 v2, 0x1e

    if-ne v1, v2, :cond_4

    .line 619
    :goto_1
    return-void

    .line 594
    .restart local v9    # "data":I
    :cond_3
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 582
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 616
    .end local v9    # "data":I
    :cond_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->setEnd()V

    goto :goto_1
.end method

.method private buttonEnable()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 494
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 495
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 497
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getSelectedItemPosition()I

    move-result v0

    .line 498
    .local v0, "pos":I
    if-nez v0, :cond_0

    .line 500
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 501
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 513
    :goto_0
    return-void

    .line 503
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getMaxDataIndex()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 505
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 506
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 510
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 511
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method private buttonEnable(FFF)V
    .locals 3
    .param p1, "min"    # F
    .param p2, "max"    # F
    .param p3, "init"    # F

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 470
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 471
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 473
    cmpl-float v0, p1, p3

    if-nez v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 476
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 489
    :goto_0
    return-void

    .line 478
    :cond_0
    cmpl-float v0, p2, p3

    if-nez v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 481
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 485
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 486
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method private buttonEnable(III)V
    .locals 3
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "init"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 447
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 448
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 450
    if-ne p1, p3, :cond_0

    .line 452
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 453
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 466
    :goto_0
    return-void

    .line 455
    :cond_0
    if-ne p2, p3, :cond_1

    .line 457
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 458
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 463
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method private sendMessage(I)V
    .locals 4
    .param p1, "nScrollPos"    # I

    .prologue
    .line 793
    const-string/jumbo v1, "#####"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "m_bNeedCallHandler called....... "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    if-nez v1, :cond_1

    .line 796
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 797
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable()V

    .line 809
    :cond_0
    :goto_0
    return-void

    .line 801
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    if-eqz v1, :cond_0

    .line 803
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 804
    .local v0, "msg":Landroid/os/Message;
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mSpinType:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 805
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getData(I)I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 806
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 807
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable()V

    goto :goto_0
.end method

.method private setEnd()V
    .locals 5

    .prologue
    const v4, 0x7f0b0172

    const/4 v3, 0x0

    .line 98
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    if-eqz v2, :cond_0

    .line 100
    const v2, 0x7f0b0173

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 101
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 110
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v0, "dividerViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/infraware/common/util/Utils;->setSeparateLine(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private setSelection(F)V
    .locals 5
    .param p1, "data"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 366
    const/4 v0, 0x0

    .line 367
    .local v0, "nData":I
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    if-ne v3, v2, :cond_2

    .line 369
    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v2, p1

    float-to-int v0, v2

    .line 376
    :goto_0
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    if-ge v0, v2, :cond_0

    .line 377
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    .line 379
    :cond_0
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-le v0, v2, :cond_1

    .line 380
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    .line 382
    :cond_1
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    if-ne v0, v2, :cond_3

    .line 384
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 397
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getIndex(I)I

    move-result v1

    .line 398
    .local v1, "sel":I
    const-string/jumbo v2, "#####"

    const-string/jumbo v3, "m_bSendMsg ..false....... "

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 400
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelectionDirect(I)V

    .line 402
    return-void

    .line 373
    .end local v1    # "sel":I
    :cond_2
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, p1

    float-to-int v0, v2

    goto :goto_0

    .line 386
    :cond_3
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-ne v0, v2, :cond_4

    .line 388
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1

    .line 392
    :cond_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 393
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1
.end method

.method private setSelection(I)V
    .locals 4
    .param p1, "data"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 337
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    if-ge p1, v1, :cond_0

    .line 338
    iget p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    .line 340
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-le p1, v1, :cond_1

    .line 341
    iget p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    .line 343
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    if-ne p1, v1, :cond_2

    .line 345
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 357
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getIndex(I)I

    move-result v0

    .line 358
    .local v0, "sel":I
    const-string/jumbo v1, "#####"

    const-string/jumbo v2, "m_bSendMsg ..false....... "

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 360
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelectionDirect(I)V

    .line 362
    return-void

    .line 347
    .end local v0    # "sel":I
    :cond_2
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-ne p1, v1, :cond_3

    .line 349
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 353
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 354
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method private setSelectionDirect(F)V
    .locals 5
    .param p1, "data"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 405
    const/4 v0, 0x0

    .line 406
    .local v0, "nData":I
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    if-ne v3, v2, :cond_2

    .line 408
    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v2, p1

    float-to-int v0, v2

    .line 416
    :goto_0
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    if-ge v0, v2, :cond_0

    .line 417
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    .line 419
    :cond_0
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-le v0, v2, :cond_1

    .line 420
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    .line 423
    :cond_1
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    if-ne v0, v2, :cond_3

    .line 425
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 438
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getIndex(I)I

    move-result v1

    .line 440
    .local v1, "sel":I
    const-string/jumbo v2, "#####"

    const-string/jumbo v3, "m_bSendMsg ..false....... "

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 442
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelectionDirect(I)V

    .line 443
    return-void

    .line 412
    .end local v1    # "sel":I
    :cond_2
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, p1

    float-to-int v0, v2

    goto :goto_0

    .line 427
    :cond_3
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-ne v0, v2, :cond_4

    .line 429
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1

    .line 433
    :cond_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 434
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1
.end method

.method private setSelectionDirect(I)V
    .locals 4
    .param p1, "data"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 307
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    if-ge p1, v1, :cond_0

    .line 308
    iget p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    .line 310
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-le p1, v1, :cond_1

    .line 311
    iget p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    .line 313
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    if-ne p1, v1, :cond_2

    .line 315
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 328
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getIndex(I)I

    move-result v0

    .line 329
    .local v0, "sel":I
    const-string/jumbo v1, "#####"

    const-string/jumbo v2, "m_bSendMsg ..false....... "

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 331
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setSelectionDirect(I)V

    .line 333
    return-void

    .line 317
    .end local v0    # "sel":I
    :cond_2
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    if-ne p1, v1, :cond_3

    .line 319
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 323
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 324
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public addFloatData(FFIFFI)V
    .locals 10
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F
    .param p3, "dataCount"    # I
    .param p4, "addMount"    # F
    .param p5, "initData"    # F
    .param p6, "titleId"    # I

    .prologue
    .line 633
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 634
    iget v7, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    iget v9, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    invoke-direct/range {v0 .. v9}, Lcom/infraware/polarisoffice5/common/WheelButton;->addFloatData(FFIFFIIII)V

    .line 636
    invoke-direct {p0, p1, p2, p5}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable(FFF)V

    .line 638
    return-void
.end method

.method public addFloatData(FFIFFIZ)V
    .locals 11
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F
    .param p3, "dataCount"    # I
    .param p4, "addMount"    # F
    .param p5, "initData"    # F
    .param p6, "titleId"    # I
    .param p7, "end"    # Z

    .prologue
    .line 623
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 624
    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    iget v9, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    iget v10, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v10}, Lcom/infraware/polarisoffice5/common/WheelButton;->addFloatData(FFIFFIIII)V

    .line 627
    move/from16 v0, p5

    invoke-direct {p0, p1, p2, v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable(FFF)V

    .line 628
    return-void
.end method

.method public addFloatData2(FFIFIZ)V
    .locals 9
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F
    .param p3, "dataCount"    # I
    .param p4, "initData"    # F
    .param p5, "titleId"    # I
    .param p6, "end"    # Z

    .prologue
    .line 642
    iput-boolean p6, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 643
    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    iget v7, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/infraware/polarisoffice5/common/WheelButton;->addFloatData2(FFIFIIII)V

    .line 645
    invoke-direct {p0, p1, p2, p4}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable(FFF)V

    .line 646
    return-void
.end method

.method public addHandler(Landroid/os/Handler;I)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "spinType"    # I

    .prologue
    .line 788
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mHandler:Landroid/os/Handler;

    .line 789
    iput p2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mSpinType:I

    .line 790
    return-void
.end method

.method public addIntData(IIIIII)V
    .locals 10
    .param p1, "minValue"    # I
    .param p2, "maxValue"    # I
    .param p3, "dataCount"    # I
    .param p4, "addMount"    # I
    .param p5, "initData"    # I
    .param p6, "titleId"    # I

    .prologue
    .line 550
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 551
    iget v7, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    iget v9, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    invoke-direct/range {v0 .. v9}, Lcom/infraware/polarisoffice5/common/WheelButton;->addIntData(IIIIIIIII)V

    .line 552
    invoke-direct {p0, p1, p2, p5}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable(III)V

    .line 553
    return-void
.end method

.method public addIntData(IIIIIII)V
    .locals 11
    .param p1, "minValue"    # I
    .param p2, "maxValue"    # I
    .param p3, "dataCount"    # I
    .param p4, "addMount"    # I
    .param p5, "initData"    # I
    .param p6, "buttonUnit"    # I
    .param p7, "titleId"    # I

    .prologue
    .line 565
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 566
    move/from16 v0, p6

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mButtonUint:I

    .line 567
    move/from16 v0, p7

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    .line 568
    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    iget v9, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    iget v10, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    invoke-direct/range {v1 .. v10}, Lcom/infraware/polarisoffice5/common/WheelButton;->addIntData(IIIIIIIII)V

    .line 570
    move/from16 v0, p5

    invoke-direct {p0, p1, p2, v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable(III)V

    .line 571
    return-void
.end method

.method public addIntData(IIIIIIIZ)V
    .locals 11
    .param p1, "minValue"    # I
    .param p2, "maxValue"    # I
    .param p3, "dataCount"    # I
    .param p4, "addMount"    # I
    .param p5, "initData"    # I
    .param p6, "buttonUnit"    # I
    .param p7, "titleId"    # I
    .param p8, "end"    # Z

    .prologue
    .line 557
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 558
    move/from16 v0, p6

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mButtonUint:I

    .line 559
    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    iget v9, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    iget v10, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    invoke-direct/range {v1 .. v10}, Lcom/infraware/polarisoffice5/common/WheelButton;->addIntData(IIIIIIIII)V

    .line 560
    move/from16 v0, p5

    invoke-direct {p0, p1, p2, v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable(III)V

    .line 561
    return-void
.end method

.method public addIntData(IIIIIIZ)V
    .locals 11
    .param p1, "minValue"    # I
    .param p2, "maxValue"    # I
    .param p3, "dataCount"    # I
    .param p4, "addMount"    # I
    .param p5, "initData"    # I
    .param p6, "titleId"    # I
    .param p7, "end"    # Z

    .prologue
    .line 543
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mEnd:Z

    .line 544
    iget v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    iget v9, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    iget v10, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v10}, Lcom/infraware/polarisoffice5/common/WheelButton;->addIntData(IIIIIIIII)V

    .line 545
    move/from16 v0, p5

    invoke-direct {p0, p1, p2, v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable(III)V

    .line 546
    return-void
.end method

.method public getFloatData()Ljava/lang/Float;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 528
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    if-nez v1, :cond_0

    .line 529
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 538
    :goto_0
    return-object v1

    .line 531
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getSelectedItemPosition()I

    move-result v0

    .line 532
    .local v0, "pos":I
    if-gez v0, :cond_1

    .line 533
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    goto :goto_0

    .line 535
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 536
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getData(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    goto :goto_0

    .line 538
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getData(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    goto :goto_0
.end method

.method public getIntData()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 517
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    if-nez v2, :cond_1

    .line 524
    :cond_0
    :goto_0
    return v1

    .line 520
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getSelectedItemPosition()I

    move-result v0

    .line 521
    .local v0, "pos":I
    if-ltz v0, :cond_0

    .line 524
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getData(I)I

    move-result v1

    goto :goto_0
.end method

.method public initLayout(Landroid/content/Context;II)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "display_type"    # I
    .param p3, "wheel_type"    # I

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x0

    const/high16 v6, 0x41980000    # 19.0f

    const/high16 v5, 0x41700000    # 15.0f

    const/4 v4, 0x3

    .line 139
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    .line 140
    iput p3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nWheelType:I

    .line 141
    iput p2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mType:I

    .line 143
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 144
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03005c

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 146
    const v2, 0x7f0b027a

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    .line 147
    const v2, 0x7f0b027f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    .line 148
    const v2, 0x7f0b027c

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/WheelScroll;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    .line 149
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 151
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 162
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 164
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    const/high16 v3, 0x41900000    # 18.0f

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    .line 165
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    .line 166
    iput v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    .line 182
    :cond_0
    :goto_0
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nWheelType:I

    const/16 v3, 0x1e

    if-ne v2, v3, :cond_1

    .line 183
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    const v3, 0x7f020244

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 184
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    const v3, 0x7f02023f

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 186
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    const v3, 0x7f020010

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 187
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    const v3, 0x7f020010

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 189
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 192
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 193
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mHandler:Landroid/os/Handler;

    .line 195
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2, v7}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setVerticalScrollBarEnabled(Z)V

    .line 196
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v2, v7}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setHorizontalScrollBarEnabled(Z)V

    .line 198
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 202
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 204
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    new-instance v3, Lcom/infraware/polarisoffice5/common/WheelButton$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/WheelButton$1;-><init>(Lcom/infraware/polarisoffice5/common/WheelButton;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 219
    return-void

    .line 168
    :cond_2
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mType:I

    if-ne v2, v4, :cond_3

    .line 170
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    invoke-static {v2, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    .line 171
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    invoke-static {v2, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    .line 172
    iput v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    goto :goto_0

    .line 175
    :cond_3
    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mType:I

    if-ne v2, v8, :cond_0

    .line 177
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    invoke-static {v2, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeCenter:I

    .line 178
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mContext:Landroid/content/Context;

    invoke-static {v2, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mFontSizeSide:I

    .line 179
    iput v8, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mVisibleViews:I

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 224
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 225
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 226
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 227
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 228
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 229
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 243
    :goto_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable()V

    .line 244
    return-void

    .line 233
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mButtonUint:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->goLeft(I)V

    goto :goto_0

    .line 238
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mButtonUint:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->goRight(I)V

    goto :goto_0

    .line 229
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b027a -> :sswitch_0
        0x7f0b027f -> :sswitch_1
    .end sparse-switch
.end method

.method public onItemScroll(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 898
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->sendMessage(I)V

    .line 899
    return-void
.end method

.method public onItemSelect(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v6, 0x1

    .line 814
    const-string/jumbo v3, "#####"

    const-string/jumbo v4, "Wheel Button onClick "

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    const-string/jumbo v3, "#####"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "WheelType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nWheelType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", Type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "Max: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", Min: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "Title: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bUseKeypad:Z

    if-nez v3, :cond_1

    .line 889
    :cond_0
    :goto_0
    return-void

    .line 819
    :cond_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable()V

    .line 821
    const/4 v2, 0x0

    .line 822
    .local v2, "sel":I
    iget v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    if-nez v3, :cond_4

    .line 824
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->getIntData()I

    move-result v2

    .line 837
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->createInputNumberPopupWindow(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v1

    .line 839
    .local v1, "popupWindow":Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setDefaultValue(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMin:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setMin(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nMax:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setMax(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setTitle(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setFieldType(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setPopupType(I)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v3

    new-instance v4, Lcom/infraware/polarisoffice5/common/WheelButton$3;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/common/WheelButton$3;-><init>(Lcom/infraware/polarisoffice5/common/WheelButton;)V

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setInputNumberListener(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;)Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->show()V

    .line 855
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mOnKeypadListener:Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;

    if-eqz v3, :cond_3

    .line 856
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mOnKeypadListener:Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;

    invoke-interface {v3}, Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;->onShowKeypad()V

    .line 859
    :cond_3
    new-instance v3, Lcom/infraware/polarisoffice5/common/WheelButton$4;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/common/WheelButton$4;-><init>(Lcom/infraware/polarisoffice5/common/WheelButton;)V

    invoke-virtual {v1, v3}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    goto :goto_0

    .line 826
    .end local v1    # "popupWindow":Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
    :cond_4
    iget v3, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    if-ne v6, v3, :cond_5

    .line 828
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->getFloatData()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 829
    .local v0, "f":F
    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v3, v0

    float-to-int v2, v3

    .line 830
    goto :goto_1

    .line 831
    .end local v0    # "f":F
    :cond_5
    const/4 v3, 0x2

    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    if-ne v3, v4, :cond_2

    .line 833
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->getFloatData()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 834
    .restart local v0    # "f":F
    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v3, v0

    float-to-int v2, v3

    goto :goto_1
.end method

.method public onLocaleChanged()V
    .locals 3

    .prologue
    .line 927
    const/4 v1, -0x1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    if-eq v1, v2, :cond_0

    .line 929
    const v1, 0x7f0b0167

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 930
    .local v0, "title":Landroid/widget/TextView;
    iget v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 933
    .end local v0    # "title":Landroid/widget/TextView;
    :cond_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->configurationChanged()V

    .line 934
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 250
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bNeedCallHandler:Z

    .line 252
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 253
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 254
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 255
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 257
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 271
    :goto_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable()V

    .line 272
    return v1

    .line 261
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->goStart()V

    goto :goto_0

    .line 266
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->goEnd()V

    goto :goto_0

    .line 257
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b027a -> :sswitch_0
        0x7f0b027f -> :sswitch_1
    .end sparse-switch
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v1, 0x0

    .line 941
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 943
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getView()Lcom/infraware/polarisoffice5/common/WheelView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 972
    :cond_0
    :goto_0
    return-void

    .line 946
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 947
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 952
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/polarisoffice5/common/WheelButton$5;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/WheelButton$5;-><init>(Lcom/infraware/polarisoffice5/common/WheelButton;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public setData(FZ)V
    .locals 2
    .param p1, "data"    # F
    .param p2, "bScroll"    # Z

    .prologue
    .line 293
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 295
    :cond_0
    if-eqz p2, :cond_2

    .line 296
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setSelection(F)V

    .line 304
    :cond_1
    :goto_0
    return-void

    .line 298
    :cond_2
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setSelectionDirect(F)V

    goto :goto_0
.end method

.method public setData(IZ)V
    .locals 1
    .param p1, "data"    # I
    .param p2, "bScroll"    # Z

    .prologue
    .line 277
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nType:I

    if-nez v0, :cond_0

    .line 279
    if-eqz p2, :cond_1

    .line 280
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setSelection(I)V

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/WheelButton;->setSelectionDirect(I)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const v1, 0x3e99999a    # 0.3f

    .line 903
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 905
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setEnabled(Z)V

    .line 906
    if-eqz p1, :cond_0

    .line 908
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setAlpha(F)V

    .line 909
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 910
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 912
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/WheelButton;->buttonEnable()V

    .line 922
    :goto_0
    return-void

    .line 916
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->setAlpha(F)V

    .line 917
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 918
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 919
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 920
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mInsButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setEndMarginVisivility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 134
    const v0, 0x7f0b0173

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 135
    const v0, 0x7f0b0172

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 136
    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 785
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mHandler:Landroid/os/Handler;

    .line 786
    return-void
.end method

.method public setOnKeypadListener(Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;)V
    .locals 0
    .param p1, "keypadListener"    # Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;

    .prologue
    .line 892
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->mOnKeypadListener:Lcom/infraware/polarisoffice5/common/WheelButton$OnkeyPadListener;

    .line 893
    return-void
.end method

.method public setTitle(II)V
    .locals 3
    .param p1, "resId"    # I
    .param p2, "visibility"    # I

    .prologue
    .line 114
    const/4 v1, -0x1

    if-ne v1, p1, :cond_0

    .line 125
    :goto_0
    return-void

    .line 117
    :cond_0
    iput p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_nTitle:I

    .line 119
    const v1, 0x7f0b0167

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 120
    .local v0, "title":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 122
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public setTitleVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 129
    const v1, 0x7f0b0167

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 130
    .local v0, "title":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    return-void
.end method

.method public setUseKeypad(Z)V
    .locals 0
    .param p1, "useKeypad"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton;->m_bUseKeypad:Z

    .line 94
    return-void
.end method
