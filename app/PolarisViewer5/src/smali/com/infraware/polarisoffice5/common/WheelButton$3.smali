.class Lcom/infraware/polarisoffice5/common/WheelButton$3;
.super Ljava/lang/Object;
.source "WheelButton.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$InputNumberListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/WheelButton;->onItemSelect(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/WheelButton;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/WheelButton;)V
    .locals 0

    .prologue
    .line 845
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelButton$3;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfirmed(F)V
    .locals 3
    .param p1, "nValue"    # F

    .prologue
    .line 848
    const-string/jumbo v0, "#####"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Wheel Button input number value is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton$3;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    float-to-int v1, p1

    # invokes: Lcom/infraware/polarisoffice5/common/WheelButton;->setSelection(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->access$000(Lcom/infraware/polarisoffice5/common/WheelButton;I)V

    .line 850
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelButton$3;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/WheelButton$3;->this$0:Lcom/infraware/polarisoffice5/common/WheelButton;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/WheelButton;->mGallery:Lcom/infraware/polarisoffice5/common/WheelScroll;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/WheelScroll;->getPosition()I

    move-result v1

    # invokes: Lcom/infraware/polarisoffice5/common/WheelButton;->sendMessage(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/WheelButton;->access$100(Lcom/infraware/polarisoffice5/common/WheelButton;I)V

    .line 851
    return-void
.end method
