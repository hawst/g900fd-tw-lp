.class Lcom/infraware/polarisoffice5/common/LayoutDlg$1;
.super Ljava/lang/Object;
.source "LayoutDlg.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/LayoutDlg;->createView(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/LayoutDlg;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string/jumbo v1, "LayoutDlg"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " , select pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 78
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;

    iget v1, v1, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mEventType:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 79
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 80
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/LayoutDlg;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 82
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/LayoutDlg;->dismiss()V

    .line 83
    return-void
.end method
