.class public Lcom/infraware/polarisoffice5/common/QuickScrollView;
.super Landroid/view/View;
.source "QuickScrollView.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_HID_ACTION;
.implements Lcom/infraware/office/evengine/E$EV_KEY_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SCROLL_COMMAND_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SCROLL_FACTOR_TYPE;


# static fields
.field static final QUICKSCROLL_DELAY_TIME:I = 0xc8

.field static final QUICKSCROLL_SHOWING_TIME:I = 0x6a4


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mDownPos:I

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mFinalPos:I

.field private mHideHandler:Landroid/os/Handler;

.field private mHideTimerTask:Ljava/lang/Runnable;

.field private mMaxPos:I

.field private mParentLayout:Landroid/widget/RelativeLayout;

.field private mQuickNormal:Landroid/graphics/Bitmap;

.field private mQuickPress:Landroid/graphics/Bitmap;

.field private mShowHandler:Landroid/os/Handler;

.field private mShowTimerTask:Ljava/lang/Runnable;

.field private mType:I

.field private mUserMode:Z

.field quickScroll_THUMB_HEIGHT:I

.field quickScroll_THUMB_WIDTH:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0xa

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 84
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 33
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mParentLayout:Landroid/widget/RelativeLayout;

    .line 36
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 38
    iput v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mDownPos:I

    .line 39
    iput v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    .line 42
    iput v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    .line 43
    iput v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mType:I

    .line 44
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z

    .line 46
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    .line 47
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideTimerTask:Ljava/lang/Runnable;

    .line 49
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    .line 50
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowTimerTask:Ljava/lang/Runnable;

    .line 55
    iput v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_WIDTH:I

    .line 56
    iput v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_HEIGHT:I

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickPress:Landroid/graphics/Bitmap;

    .line 59
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickNormal:Landroid/graphics/Bitmap;

    .line 87
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201d2

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickNormal:Landroid/graphics/Bitmap;

    .line 91
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_WIDTH:I

    .line 92
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_HEIGHT:I

    .line 94
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    .line 96
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mType:I

    .line 98
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 100
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0b002c

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mParentLayout:Landroid/widget/RelativeLayout;

    .line 102
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_WIDTH:I

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 103
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 104
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 105
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 107
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mParentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 110
    new-instance v1, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView$1;-><init>(Lcom/infraware/polarisoffice5/common/QuickScrollView;)V

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 181
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201d3

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickPress:Landroid/graphics/Bitmap;

    .line 183
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->setVisibility(I)V

    .line 184
    return-void
.end method

.method private Pos2Scroll(II)V
    .locals 13
    .param p1, "pos"    # I
    .param p2, "max"    # I

    .prologue
    const/4 v3, 0x0

    .line 333
    if-eqz p1, :cond_0

    if-ne p1, p2, :cond_2

    .line 334
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    if-ne v0, p1, :cond_3

    .line 388
    :cond_1
    :goto_0
    return-void

    .line 338
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_1

    .line 342
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v10

    .line 344
    .local v10, "si":Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    iget v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_HEIGHT:I

    add-int v7, v0, v1

    .line 346
    .local v7, "core_height":I
    iget v0, v10, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nHeight:I

    sub-int v6, v0, v7

    .line 348
    .local v6, "available":I
    if-lez v6, :cond_1

    .line 378
    iget v9, v10, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosY:I

    .line 380
    .local v9, "oldPos":I
    int-to-double v0, v6

    int-to-double v4, p1

    int-to-double v11, p2

    div-double/2addr v4, v11

    mul-double/2addr v0, v4

    double-to-int v8, v0

    .line 382
    .local v8, "newPos":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x6

    const/4 v2, -0x1

    sub-int v4, v8, v9

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->IScroll(IIIII)V

    .line 384
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->updateHideTimer()V

    .line 386
    iput p1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    .line 387
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->invalidate()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .prologue
    .line 31
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .prologue
    .line 31
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mDownPos:I

    return v0
.end method

.method static synthetic access$102(Lcom/infraware/polarisoffice5/common/QuickScrollView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mDownPos:I

    return p1
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/QuickScrollView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z

    return v0
.end method

.method static synthetic access$202(Lcom/infraware/polarisoffice5/common/QuickScrollView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z

    return p1
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/QuickScrollView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .prologue
    .line 31
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/QuickScrollView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->Pos2Scroll(II)V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/common/QuickScrollView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->showScroll()V

    return-void
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/common/QuickScrollView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/QuickScrollView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private showScroll()V
    .locals 4

    .prologue
    .line 274
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 278
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    .line 279
    new-instance v0, Lcom/infraware/polarisoffice5/common/QuickScrollView$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView$3;-><init>(Lcom/infraware/polarisoffice5/common/QuickScrollView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideTimerTask:Ljava/lang/Runnable;

    .line 293
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideTimerTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x6a4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 298
    :goto_0
    return-void

    .line 296
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->updateHideTimer()V

    goto :goto_0
.end method

.method private updateHideTimer()V
    .locals 4

    .prologue
    .line 266
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideTimerTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 268
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideTimerTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x6a4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 270
    :cond_0
    return-void
.end method

.method private updateShowTimer()V
    .locals 4

    .prologue
    .line 258
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowTimerTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 260
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowTimerTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 262
    :cond_0
    return-void
.end method


# virtual methods
.method public QuickScrollFinalize()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideTimerTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 65
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideTimerTask:Ljava/lang/Runnable;

    .line 66
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowTimerTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 71
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowTimerTask:Ljava/lang/Runnable;

    .line 72
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    .line 75
    :cond_1
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    .line 76
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 78
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickPress:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 79
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 80
    return-void
.end method

.method public getUserMode()Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z

    return v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowTimerTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideTimerTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 325
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 327
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->setVisibility(I)V

    .line 329
    :cond_2
    return-void
.end method

.method public initGuideTextToSpeech()V
    .locals 3

    .prologue
    .line 426
    iget v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 427
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    if-eqz v1, :cond_0

    .line 428
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .line 429
    .local v0, "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 430
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->initGuideTextToSpeech()V

    .line 434
    .end local v0    # "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    :cond_0
    return-void
.end method

.method public isShown()Z
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 248
    const/4 v0, 0x1

    .line 249
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 194
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickPress:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 199
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 200
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mQuickNormal:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onGuideStartTexToSpeech()V
    .locals 3

    .prologue
    .line 437
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->IS_CANE()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 438
    iget v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 439
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    if-eqz v1, :cond_0

    .line 440
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    .line 441
    .local v0, "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 442
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/word/WordEditorActivity;->onGuideStartDelayTextToSpeech()V

    .line 447
    .end local v0    # "wordEditorActivity":Lcom/infraware/polarisoffice5/word/WordEditorActivity;
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 307
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_HEIGHT:I

    sub-int v0, p2, v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    .line 308
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    if-gez v0, :cond_0

    .line 309
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    .line 311
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 312
    return-void
.end method

.method public scroll2Pos()Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 392
    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z

    if-eqz v4, :cond_1

    .line 394
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->updateHideTimer()V

    .line 422
    :cond_0
    :goto_0
    return v3

    .line 399
    :cond_1
    iget v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    iget v5, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_HEIGHT:I

    add-int v0, v4, v5

    .line 401
    .local v0, "core_height":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetScrollInfo_Editor()Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;

    move-result-object v2

    .line 403
    .local v2, "si":Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;
    iget v4, v2, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nHeight:I

    if-le v4, v0, :cond_0

    .line 406
    iget v4, v2, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nHeight:I

    sub-int v1, v4, v0

    .line 408
    .local v1, "real_si_height":I
    iget v4, v2, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosY:I

    if-gt v4, v1, :cond_0

    .line 412
    iget v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    int-to-double v4, v4

    iget v6, v2, Lcom/infraware/office/evengine/EV$SCROLLINFO_EDITOR;->nCurPosY:I

    int-to-double v6, v6

    int-to-double v8, v1

    div-double/2addr v6, v8

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    .line 415
    iget v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    const/4 v5, 0x5

    if-ge v4, v5, :cond_3

    .line 416
    iput v3, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    .line 420
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->invalidate()V

    .line 422
    const/4 v3, 0x1

    goto :goto_0

    .line 417
    :cond_3
    iget v3, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    iget v4, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    add-int/lit8 v4, v4, -0x5

    if-le v3, v4, :cond_2

    .line 418
    iget v3, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    iput v3, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mFinalPos:I

    goto :goto_1
.end method

.method public show(I)V
    .locals 4
    .param p1, "view_height"    # I

    .prologue
    const/4 v2, 0x2

    .line 204
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mType:I

    if-ne v0, v2, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 210
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v2, :cond_0

    .line 215
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->quickScroll_THUMB_HEIGHT:I

    sub-int v0, p1, v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mMaxPos:I

    .line 217
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 219
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->hide()V

    .line 222
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->scroll2Pos()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    if-nez v0, :cond_4

    .line 226
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    .line 227
    new-instance v0, Lcom/infraware/polarisoffice5/common/QuickScrollView$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView$2;-><init>(Lcom/infraware/polarisoffice5/common/QuickScrollView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowTimerTask:Ljava/lang/Runnable;

    .line 235
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView;->mShowTimerTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 239
    :cond_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->updateShowTimer()V

    goto :goto_0
.end method
