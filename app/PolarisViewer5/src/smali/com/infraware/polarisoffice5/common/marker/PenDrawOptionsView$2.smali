.class Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;
.super Ljava/lang/Object;
.source "PenDrawOptionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 361
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b014d

    if-ne v0, v1, :cond_1

    .line 362
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 364
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 365
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setThickness(I)V

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b014f

    if-ne v0, v1, :cond_0

    .line 371
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    const/16 v1, 0x32

    if-ge v0, v1, :cond_0

    .line 373
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 374
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setThickness(I)V

    goto :goto_0
.end method
