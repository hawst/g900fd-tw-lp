.class Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;
.super Ljava/lang/Object;
.source "ViewSettEventDlg.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 385
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mHandler:Landroid/os/Handler;

    if-eqz v4, :cond_8

    .line 387
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 388
    .local v1, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mEventType:I

    iput v4, v1, Landroid/os/Message;->what:I

    .line 390
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 392
    .local v0, "data":Landroid/os/Bundle;
    const/4 v2, 0x0

    .local v2, "position":I
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_7

    .line 393
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget v3, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->text_res_id:I

    .line 395
    .local v3, "str_res_id":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->MEMO_STR:I

    if-ne v3, v4, :cond_1

    .line 396
    const-string/jumbo v5, "MEMO"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget-boolean v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 392
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 398
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->RULER_STR:I

    if-ne v3, v4, :cond_2

    .line 399
    const-string/jumbo v5, "RULER"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget-boolean v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 401
    :cond_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SPELL_CHECK_STR:I

    if-ne v3, v4, :cond_3

    .line 402
    const-string/jumbo v5, "SPELL_CHECK"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget-boolean v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 404
    :cond_3
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->NO_MARGIN_VIEW_STR:I

    if-ne v3, v4, :cond_4

    .line 405
    const-string/jumbo v5, "NO_MARGIN_VIEW"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget-boolean v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 407
    :cond_4
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->REFLOW_TEXT_STR:I

    if-ne v3, v4, :cond_5

    .line 408
    const-string/jumbo v5, "REFLOW_TEXT"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget-boolean v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 410
    :cond_5
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SINGLE_SLIDE_VIEW_STR:I

    if-ne v3, v4, :cond_6

    .line 411
    const-string/jumbo v5, "SINGLE_SLIDE_VIEW"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget-boolean v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 413
    :cond_6
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->SMART_GUIDE_STR:I

    if-ne v3, v4, :cond_0

    .line 414
    const-string/jumbo v5, "SMART_GUIDE"

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    iget-boolean v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 418
    .end local v3    # "str_res_id":I
    :cond_7
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 419
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 422
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "position":I
    :cond_8
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$2;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->dismiss()V

    .line 423
    return-void
.end method
