.class Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;
.super Ljava/lang/Object;
.source "CommonNumberInputPopupWindow.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V
    .locals 0

    .prologue
    .line 346
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 13
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/high16 v10, 0x41200000    # 10.0f

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 354
    const/4 v5, 0x0

    .line 355
    .local v5, "isError":Z
    const/4 v4, 0x0

    .line 356
    .local v4, "intValue":I
    const/4 v3, 0x0

    .line 358
    .local v3, "floatValue":F
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->button_dot:Landroid/widget/Button;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$500(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/Button;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 360
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v7

    if-nez v7, :cond_6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->length()I

    move-result v8

    if-ne v7, v8, :cond_6

    .line 362
    :cond_0
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    iput-boolean v11, v7, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->canMinus:Z

    .line 370
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v7

    if-eqz v7, :cond_4

    .line 372
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v6

    .line 375
    .local v6, "nNumLen":I
    const/4 v0, 0x0

    .line 376
    .local v0, "bCheckLimit":Z
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMinLen:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$600(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    if-lt v6, v7, :cond_7

    .line 377
    const/4 v0, 0x1

    .line 383
    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    .line 386
    :try_start_0
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    if-nez v7, :cond_9

    .line 387
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 397
    :goto_2
    if-nez v5, :cond_4

    .line 399
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mFieldType:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    if-nez v7, :cond_a

    .line 401
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$300(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    if-lt v4, v7, :cond_3

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$800(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    if-le v4, v7, :cond_4

    .line 403
    :cond_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$300(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 404
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$800(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 405
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    const-string/jumbo v8, "%d"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    .line 407
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # invokes: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->warning()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V

    .line 424
    .end local v0    # "bCheckLimit":Z
    .end local v6    # "nNumLen":I
    :cond_4
    :goto_3
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 425
    .local v1, "check":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    const-string/jumbo v7, "^\\s*$"

    invoke-virtual {v1, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string/jumbo v7, "^\\.*$"

    invoke-virtual {v1, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string/jumbo v7, "^\\-*$"

    invoke-virtual {v1, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 426
    :cond_5
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mBtnOk:Landroid/widget/ImageButton;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$900(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/ImageButton;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 429
    :goto_4
    return-void

    .line 366
    .end local v1    # "check":Ljava/lang/String;
    :cond_6
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    iput-boolean v12, v7, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->canMinus:Z

    goto/16 :goto_0

    .line 378
    .restart local v0    # "bCheckLimit":Z
    .restart local v6    # "nNumLen":I
    :cond_7
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$300(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    if-gez v7, :cond_8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMinLen:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$600(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne v6, v7, :cond_8

    .line 379
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 380
    :cond_8
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "0"

    invoke-virtual {v7, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-ne v7, v11, :cond_2

    .line 381
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 389
    :cond_9
    :try_start_1
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v7

    mul-float/2addr v7, v10

    float-to-int v7, v7

    int-to-float v3, v7

    goto/16 :goto_2

    .line 391
    :catch_0
    move-exception v2

    .line 393
    .local v2, "e":Ljava/lang/NumberFormatException;
    const/4 v5, 0x1

    .line 394
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # invokes: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->warning()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V

    goto/16 :goto_2

    .line 412
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_a
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$300(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-ltz v7, :cond_b

    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$800(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    int-to-float v7, v7

    cmpl-float v7, v3, v7

    if-lez v7, :cond_4

    .line 414
    :cond_b
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMin:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$300(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    int-to-float v7, v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 415
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mMax:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$800(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)I

    move-result v7

    int-to-float v7, v7

    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 416
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    const-string/jumbo v8, "%.1f"

    new-array v9, v11, [Ljava/lang/Object;

    div-float v10, v3, v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mEditNumber:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    .line 418
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # invokes: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->warning()V
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)V

    goto/16 :goto_3

    .line 428
    .end local v0    # "bCheckLimit":Z
    .end local v6    # "nNumLen":I
    .restart local v1    # "check":Ljava/lang/String;
    :cond_c
    iget-object v7, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow$4;->this$0:Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->mBtnOk:Landroid/widget/ImageButton;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;->access$900(Lcom/infraware/polarisoffice5/common/CommonNumberInputPopupWindow;)Landroid/widget/ImageButton;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_4
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 350
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 348
    return-void
.end method
