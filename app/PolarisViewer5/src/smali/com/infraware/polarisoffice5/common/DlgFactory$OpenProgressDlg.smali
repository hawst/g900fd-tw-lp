.class public Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;
.super Landroid/app/ProgressDialog;
.source "DlgFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/DlgFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OpenProgressDlg"
.end annotation


# instance fields
.field mActivity:Landroid/app/Activity;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 149
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;->this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;

    .line 150
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v0

    invoke-direct {p0, p2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 152
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;->mActivity:Landroid/app/Activity;

    .line 154
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;->setMessage(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {p0, p3}, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;->setTitle(Ljava/lang/CharSequence;)V

    .line 157
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;->setIndeterminate(Z)V

    .line 158
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;->setCancelable(Z)V

    .line 159
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;->setCanceledOnTouchOutside(Z)V

    .line 160
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg$1;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg$1;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;Lcom/infraware/polarisoffice5/common/DlgFactory;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 166
    return-void
.end method
