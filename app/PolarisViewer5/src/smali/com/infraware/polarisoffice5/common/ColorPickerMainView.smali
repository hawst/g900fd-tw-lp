.class public Lcom/infraware/polarisoffice5/common/ColorPickerMainView;
.super Landroid/view/View;
.source "ColorPickerMainView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field private mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mH:F

.field private mHandler:Landroid/os/Handler;

.field private mMainColors:[I

.field private mMainViewBitmap:Landroid/graphics/Bitmap;

.field private mMainViewCanvas:Landroid/graphics/Canvas;

.field private mMainViewRect:Landroid/graphics/Rect;

.field private mNormalPoint:Landroid/graphics/Bitmap;

.field private mPaint:Landroid/graphics/Paint;

.field private mPoint:Landroid/graphics/Bitmap;

.field private mPressPoint:Landroid/graphics/Bitmap;

.field private mS:F

.field private mTouchPoint:Landroid/graphics/Point;

.field private mV:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    .line 30
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainColors:[I

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    .line 33
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewCanvas:Landroid/graphics/Canvas;

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewBitmap:Landroid/graphics/Bitmap;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 36
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mNormalPoint:Landroid/graphics/Bitmap;

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    .line 38
    new-instance v0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView$1;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerMainView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mHandler:Landroid/os/Handler;

    .line 66
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .line 100
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    .line 30
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainColors:[I

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    .line 33
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewCanvas:Landroid/graphics/Canvas;

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewBitmap:Landroid/graphics/Bitmap;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 36
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mNormalPoint:Landroid/graphics/Bitmap;

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    .line 38
    new-instance v0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView$1;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerMainView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mHandler:Landroid/os/Handler;

    .line 66
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .line 100
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    .line 30
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainColors:[I

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    .line 33
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewCanvas:Landroid/graphics/Canvas;

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewBitmap:Landroid/graphics/Bitmap;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 36
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mNormalPoint:Landroid/graphics/Bitmap;

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    .line 38
    new-instance v0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView$1;-><init>(Lcom/infraware/polarisoffice5/common/ColorPickerMainView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mHandler:Landroid/os/Handler;

    .line 66
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .line 100
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 113
    return-void
.end method

.method private DrawMainView()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 141
    iget v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mH:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 155
    :cond_0
    return-void

    .line 143
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewCanvas:Landroid/graphics/Canvas;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPaint:Landroid/graphics/Paint;

    if-eqz v2, :cond_0

    .line 144
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    const/high16 v3, 0x43800000    # 256.0f

    div-float v13, v2, v3

    .line 145
    .local v13, "offset":F
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 146
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    const/16 v2, 0x100

    if-ge v12, v2, :cond_0

    .line 147
    const/4 v2, 0x2

    new-array v5, v2, [I

    .line 148
    .local v5, "colors":[I
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainColors:[I

    aget v3, v3, v12

    aput v3, v5, v2

    .line 149
    const/4 v2, 0x1

    const/high16 v3, -0x1000000

    aput v3, v5, v2

    .line 150
    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v4, v2

    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 151
    .local v0, "shader":Landroid/graphics/Shader;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 152
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v12

    mul-float v7, v2, v13

    int-to-float v2, v12

    mul-float v9, v2, v13

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v10, v2

    iget-object v11, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPaint:Landroid/graphics/Paint;

    move v8, v1

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 146
    add-int/lit8 v12, v12, 0x1

    goto :goto_0
.end method


# virtual methods
.method public Init()V
    .locals 2

    .prologue
    .line 116
    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p0}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 117
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPaint:Landroid/graphics/Paint;

    .line 118
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020049

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPressPoint:Landroid/graphics/Bitmap;

    .line 119
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020048

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mNormalPoint:Landroid/graphics/Bitmap;

    .line 120
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    .line 122
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mH:F

    .line 125
    return-void
.end method

.method public UpdateMainColors(F)V
    .locals 5
    .param p1, "hue"    # F

    .prologue
    .line 128
    const/4 v2, 0x3

    new-array v0, v2, [F

    .line 129
    .local v0, "hsv":[F
    const/4 v2, 0x0

    aput p1, v0, v2

    .line 130
    const/4 v2, 0x2

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v0, v2

    .line 131
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v2, 0x100

    if-ge v1, v2, :cond_0

    .line 133
    const/4 v2, 0x1

    int-to-float v3, v1

    const/high16 v4, 0x437f0000    # 255.0f

    div-float/2addr v3, v4

    aput v3, v0, v2

    .line 134
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainColors:[I

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v3

    aput v3, v2, v1

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->DrawMainView()V

    .line 138
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v7, 0x3a83126f    # 0.001f

    .line 223
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 224
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 225
    .local v3, "y":F
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    float-to-int v5, v2

    float-to-int v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Point;->set(II)V

    .line 227
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mS:F

    .line 228
    .local v0, "oldS":F
    iget v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mV:F

    .line 230
    .local v1, "oldV":F
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-gez v4, :cond_0

    .line 231
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iput v5, v4, Landroid/graphics/Point;->x:I

    .line 233
    :cond_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_1

    .line 234
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Landroid/graphics/Point;->x:I

    .line 236
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    cmpg-float v4, v3, v4

    if-gez v4, :cond_2

    .line 237
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iput v5, v4, Landroid/graphics/Point;->y:I

    .line 239
    :cond_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_3

    .line 240
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Landroid/graphics/Point;->y:I

    .line 242
    :cond_3
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    iput v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mS:F

    .line 243
    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    iput v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mV:F

    .line 245
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 260
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->invalidate()V

    .line 263
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v4, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 265
    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mS:F

    sub-float v4, v0, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v7

    if-gtz v4, :cond_4

    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mV:F

    sub-float v4, v1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v7

    if-lez v4, :cond_5

    .line 267
    :cond_4
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    if-eqz v4, :cond_5

    .line 268
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    iget v5, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mS:F

    iget v6, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mV:F

    invoke-virtual {v4, v5, v6}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->onChangeSV(FF)V

    .line 271
    :cond_5
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 272
    const/4 v4, 0x1

    return v4

    .line 249
    :pswitch_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPressPoint:Landroid/graphics/Bitmap;

    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 255
    :pswitch_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mNormalPoint:Landroid/graphics/Bitmap;

    iput-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getS()F
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mS:F

    return v0
.end method

.method public getV()F
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mV:F

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 287
    const/4 v0, 0x0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 205
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 208
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 211
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x2

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v2, v2, -0x2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v4, v4, 0x2

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 212
    .local v0, "temp":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 213
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-lt v1, v2, :cond_2

    .line 214
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mPoint:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 291
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 294
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 297
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 300
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 315
    const/4 v0, 0x0

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v3, 0x2

    .line 162
    const/4 v1, 0x1

    .line 163
    .local v1, "port":Z
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/ColorPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v3, :cond_0

    .line 164
    const/4 v1, 0x0

    .line 167
    :cond_0
    if-eq p2, p1, :cond_1

    .line 169
    if-eqz v1, :cond_4

    .line 170
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 171
    .local v0, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v0, Landroid/os/Message;->what:I

    .line 172
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 173
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 186
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getPaddingRight()I

    move-result v5

    sub-int v5, p1, v5

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->getPaddingBottom()I

    move-result v6

    sub-int v6, p2, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 188
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-lez v2, :cond_2

    .line 190
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewBitmap:Landroid/graphics/Bitmap;

    .line 192
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewCanvas:Landroid/graphics/Canvas;

    .line 193
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->DrawMainView()V

    .line 196
    :cond_2
    iget v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mH:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    .line 197
    iget v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mH:F

    iget v3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mS:F

    iget v4, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mV:F

    invoke-virtual {p0, v2, v3, v4}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->setHSV(FFF)V

    .line 199
    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 201
    return-void

    .line 177
    :cond_4
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 178
    .restart local v0    # "msg":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 179
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 180
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public setHSV(FFF)V
    .locals 3
    .param p1, "h"    # F
    .param p2, "s"    # F
    .param p3, "v"    # F

    .prologue
    .line 71
    iget v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mH:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->UpdateMainColors(F)V

    .line 75
    :cond_0
    iput p1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mH:F

    .line 76
    iput p2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mS:F

    .line 77
    iput p3, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mV:F

    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mTouchPoint:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mMainViewRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 84
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->invalidate()V

    .line 85
    return-void
.end method

.method public setParent(Lcom/infraware/polarisoffice5/common/ColorPickerActivity;)V
    .locals 0
    .param p1, "cpa"    # Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ColorPickerMainView;->mColorPicker:Lcom/infraware/polarisoffice5/common/ColorPickerActivity;

    .line 89
    return-void
.end method
