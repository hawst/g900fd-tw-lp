.class public abstract Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;
.super Ljava/lang/Object;
.source "BaseDialog.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/dialog/BaseDialog$Finalizable;
    }
.end annotation


# instance fields
.field protected mBuilder:Landroid/app/AlertDialog$Builder;

.field protected mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field protected mClickListener:Landroid/content/DialogInterface$OnClickListener;

.field protected mContext:Landroid/content/Context;

.field protected mDialog:Landroid/app/AlertDialog;

.field mDialogDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field protected mDialogView:Landroid/view/View;

.field protected mFinalizableInstance:Lcom/infraware/polarisoffice5/common/dialog/BaseDialog$Finalizable;

.field protected mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field protected mMessageId:I

.field protected mNegativeId:I

.field protected mNeutralId:I

.field protected mPositiveId:I

.field protected mStrMsg:Ljava/lang/String;

.field protected mTitleId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;Lcom/infraware/polarisoffice5/common/dialog/DialogManager;)V
    .locals 3
    .param p1, "a_context"    # Landroid/content/Context;
    .param p2, "a_type"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
    .param p3, "a_Instance"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog$1;-><init>(Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialogDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 36
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->init()V

    .line 37
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mFinalizableInstance:Lcom/infraware/polarisoffice5/common/dialog/BaseDialog$Finalizable;

    .line 38
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mContext:Landroid/content/Context;

    .line 39
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    .line 40
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 92
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 86
    :cond_0
    return-void
.end method

.method protected getDefaultValue()I
    .locals 1

    .prologue
    .line 165
    const/4 v0, -0x1

    return v0
.end method

.method protected varargs inflateDialog([Ljava/lang/Object;)V
    .locals 3
    .param p1, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 137
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mTitleId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 138
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mTitleId:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 140
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mMessageId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 141
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mMessageId:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mStrMsg:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mStrMsg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 146
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mPositiveId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 147
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mPositiveId:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 149
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNeutralId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 150
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNeutralId:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 152
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNegativeId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 153
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNegativeId:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 155
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    if-eqz v0, :cond_6

    .line 156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 158
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v0, :cond_7

    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 161
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialogView:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 162
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialogView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 163
    :cond_8
    return-void
.end method

.method public init()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mContext:Landroid/content/Context;

    .line 50
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mTitleId:I

    .line 51
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mMessageId:I

    .line 52
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mStrMsg:Ljava/lang/String;

    .line 53
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mPositiveId:I

    .line 54
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNeutralId:I

    .line 55
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNegativeId:I

    .line 56
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 57
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 59
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLocaleChanged()V
    .locals 3

    .prologue
    .line 116
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mTitleId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mTitleId:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 119
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mMessageId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 120
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mMessageId:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mStrMsg:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mStrMsg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 125
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mPositiveId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 126
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mPositiveId:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 128
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNegativeId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 129
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNegativeId:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 131
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNeutralId:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->getDefaultValue()I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 132
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mNeutralId:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 133
    :cond_5
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public varargs show([Ljava/lang/Object;)V
    .locals 2
    .param p1, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->inflateDialog([Ljava/lang/Object;)V

    .line 66
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialogDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 68
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 69
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 70
    return-void
.end method
