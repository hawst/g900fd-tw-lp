.class public Lcom/infraware/polarisoffice5/common/EditInputFilter;
.super Ljava/lang/Object;
.source "EditInputFilter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private filters:[Landroid/text/InputFilter;

.field private m_ToastMsg:Landroid/widget/Toast;

.field private m_nMaxLength:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->filters:[Landroid/text/InputFilter;

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 15
    sget v0, Lcom/infraware/filemanager/define/FMDefine;->MAX_FILENAME_LENGTH:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_nMaxLength:I

    .line 28
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->context:Landroid/content/Context;

    .line 30
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->filters:[Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Lcom/infraware/polarisoffice5/common/EditInputFilter$1;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/EditInputFilter$1;-><init>(Lcom/infraware/polarisoffice5/common/EditInputFilter;)V

    aput-object v2, v0, v1

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/EditInputFilter;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .prologue
    .line 11
    iget v0, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_nMaxLength:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/EditInputFilter;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/EditInputFilter;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->onToastMessage()V

    return-void
.end method

.method private onToastMessage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 50
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07012a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, "formattedString":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_nMaxLength:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "errMsg":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v2, :cond_0

    .line 54
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->context:Landroid/content/Context;

    invoke-static {v2, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 58
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 59
    return-void

    .line 56
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v2, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getFilters()[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->filters:[Landroid/text/InputFilter;

    return-object v0
.end method

.method public setMaxLength(I)V
    .locals 0
    .param p1, "maxLength"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_nMaxLength:I

    .line 25
    return-void
.end method
