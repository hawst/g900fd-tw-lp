.class public Lcom/infraware/polarisoffice5/common/MultiAdapter;
.super Landroid/widget/BaseAdapter;
.source "MultiAdapter.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_FONTLIST_TYPE;


# static fields
.field public static final MAIN_ACTION_BAR:I = 0x1

.field public static final MAIN_ACTION_BAR_INSERT:I = 0x2


# instance fields
.field arSrcItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;"
        }
    .end annotation
.end field

.field mContext:Landroid/content/Context;

.field mEditBtn:Landroid/widget/Button;

.field mHandler:Landroid/os/Handler;

.field mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "arItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/MultiListItem;>;"
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mEditBtn:Landroid/widget/Button;

    .line 37
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mHandler:Landroid/os/Handler;

    .line 39
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mContext:Landroid/content/Context;

    .line 42
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 43
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    .line 44
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "aHandler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/MultiListItem;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "arItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/MultiListItem;>;"
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mEditBtn:Landroid/widget/Button;

    .line 37
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mHandler:Landroid/os/Handler;

    .line 39
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mContext:Landroid/content/Context;

    .line 48
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 49
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    .line 50
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mHandler:Landroid/os/Handler;

    .line 51
    return-void
.end method


# virtual methods
.method public InsertItem(ILcom/infraware/polarisoffice5/common/MultiListItem;)V
    .locals 1
    .param p1, "location"    # I
    .param p2, "item"    # Lcom/infraware/polarisoffice5/common/MultiListItem;

    .prologue
    .line 418
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 419
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/infraware/polarisoffice5/common/MultiListItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->getItem(I)Lcom/infraware/polarisoffice5/common/MultiListItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 63
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v0, v0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 85
    if-nez p2, :cond_0

    .line 86
    const/4 v8, 0x0

    .line 87
    .local v8, "res":I
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    packed-switch v10, :pswitch_data_0

    .line 116
    :goto_0
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 119
    .end local v8    # "res":I
    :cond_0
    const/4 v2, 0x0

    .line 120
    .local v2, "Text":Landroid/widget/TextView;
    const/4 v3, 0x0

    .line 121
    .local v3, "Text2":Landroid/widget/TextView;
    const/4 v6, 0x0

    .line 122
    .local v6, "imgIcon":Landroid/widget/ImageView;
    const/4 v9, 0x0

    .line 123
    .local v9, "view":Lcom/infraware/polarisoffice5/common/ColorView;
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    packed-switch v10, :pswitch_data_1

    .line 383
    :cond_1
    :goto_1
    :pswitch_0
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mContext:Landroid/content/Context;

    instance-of v10, v10, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    if-eqz v10, :cond_2

    .line 384
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 385
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 386
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setListSubtitleHoveringListener(Landroid/widget/TextView;)Z

    .line 388
    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 389
    invoke-static {v6}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setHoveringListener(Landroid/view/View;)Z

    .line 393
    :cond_2
    return-object p2

    .line 89
    .end local v2    # "Text":Landroid/widget/TextView;
    .end local v3    # "Text2":Landroid/widget/TextView;
    .end local v6    # "imgIcon":Landroid/widget/ImageView;
    .end local v9    # "view":Lcom/infraware/polarisoffice5/common/ColorView;
    .restart local v8    # "res":I
    :pswitch_1
    const v8, 0x1090003

    .line 90
    goto :goto_0

    .line 92
    :pswitch_2
    const v8, 0x7f030027

    .line 93
    goto :goto_0

    .line 95
    :pswitch_3
    const v8, 0x7f03002a

    .line 96
    goto :goto_0

    .line 98
    :pswitch_4
    const v8, 0x7f030026

    .line 99
    goto :goto_0

    .line 101
    :pswitch_5
    const v8, 0x7f030023

    .line 102
    goto :goto_0

    .line 104
    :pswitch_6
    const v8, 0x7f030029

    .line 105
    goto :goto_0

    .line 107
    :pswitch_7
    const v8, 0x7f030028

    .line 108
    goto :goto_0

    .line 110
    :pswitch_8
    const v8, 0x7f030025

    .line 111
    goto :goto_0

    .line 113
    :pswitch_9
    const v8, 0x7f030024

    goto :goto_0

    .line 127
    .end local v8    # "res":I
    .restart local v2    # "Text":Landroid/widget/TextView;
    .restart local v3    # "Text2":Landroid/widget/TextView;
    .restart local v6    # "imgIcon":Landroid/widget/ImageView;
    .restart local v9    # "view":Lcom/infraware/polarisoffice5/common/ColorView;
    :pswitch_a
    const v10, 0x7f0b0104

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "Text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 129
    .restart local v2    # "Text":Landroid/widget/TextView;
    if-eqz v2, :cond_3

    .line 131
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_5

    .line 133
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_STANDARD_UI()Z

    move-result v10

    if-eqz v10, :cond_4

    const v10, -0xd7d7d8

    :goto_2
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 138
    :cond_3
    :goto_3
    const v10, 0x7f0b0106

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "imgIcon":Landroid/widget/ImageView;
    check-cast v6, Landroid/widget/ImageView;

    .line 139
    .restart local v6    # "imgIcon":Landroid/widget/ImageView;
    if-eqz v6, :cond_1

    .line 141
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 142
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_7

    .line 143
    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_1

    .line 133
    :cond_4
    const v10, -0xd7d7d8

    goto :goto_2

    .line 135
    :cond_5
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_STANDARD_UI()Z

    move-result v10

    if-eqz v10, :cond_6

    const v10, -0x4a4a4b

    :goto_4
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    :cond_6
    const v10, -0x4a4a4b

    goto :goto_4

    .line 145
    :cond_7
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_1

    .line 149
    :pswitch_b
    const v10, 0x7f0b0104

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "Text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 150
    .restart local v2    # "Text":Landroid/widget/TextView;
    if-eqz v2, :cond_8

    .line 151
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_9

    .line 153
    const v10, -0xa0a0b

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 177
    :goto_5
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setSelected(Z)V

    .line 179
    :cond_8
    const v10, 0x7f0b0106

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "imgIcon":Landroid/widget/ImageView;
    check-cast v6, Landroid/widget/ImageView;

    .line 180
    .restart local v6    # "imgIcon":Landroid/widget/ImageView;
    if-eqz v6, :cond_1

    .line 182
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 183
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_a

    .line 184
    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_1

    .line 155
    :cond_9
    const v10, -0x9c9690

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_5

    .line 186
    :cond_a
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_1

    .line 211
    :pswitch_c
    const v10, 0x7f0b010a

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "Text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 213
    .restart local v2    # "Text":Landroid/widget/TextView;
    if-eqz v2, :cond_b

    .line 214
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :cond_b
    const v10, 0x7f0b010b

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .end local v9    # "view":Lcom/infraware/polarisoffice5/common/ColorView;
    check-cast v9, Lcom/infraware/polarisoffice5/common/ColorView;

    .line 217
    .restart local v9    # "view":Lcom/infraware/polarisoffice5/common/ColorView;
    if-eqz v9, :cond_1

    .line 219
    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/infraware/polarisoffice5/common/ColorView;->setVisibility(I)V

    .line 220
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mColor:I

    invoke-virtual {v9, v10}, Lcom/infraware/polarisoffice5/common/ColorView;->setColor(I)V

    goto/16 :goto_1

    .line 224
    :pswitch_d
    const v10, 0x7f0b0104

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "Text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 225
    .restart local v2    # "Text":Landroid/widget/TextView;
    if-eqz v2, :cond_c

    .line 227
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    :cond_c
    const v10, 0x7f0b0105

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/ListImgRightButton;

    .line 232
    .local v0, "RightBtn":Lcom/infraware/polarisoffice5/common/ListImgRightButton;
    if-eqz v0, :cond_1

    .line 234
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_d

    .line 235
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setVisibility(I)V

    .line 236
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setTag(Ljava/lang/Object;)V

    .line 237
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setClickable(Z)V

    .line 238
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setFocusable(Z)V

    .line 239
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setImageResource(I)V

    .line 241
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mHandler:Landroid/os/Handler;

    if-eqz v10, :cond_1

    .line 243
    new-instance v10, Lcom/infraware/polarisoffice5/common/MultiAdapter$1;

    invoke-direct {v10, p0}, Lcom/infraware/polarisoffice5/common/MultiAdapter$1;-><init>(Lcom/infraware/polarisoffice5/common/MultiAdapter;)V

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 258
    :cond_d
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/common/ListImgRightButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 262
    .end local v0    # "RightBtn":Lcom/infraware/polarisoffice5/common/ListImgRightButton;
    :pswitch_e
    const v10, 0x7f0b0104

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "Text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 263
    .restart local v2    # "Text":Landroid/widget/TextView;
    if-eqz v2, :cond_e

    .line 264
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    :cond_e
    const v10, 0x7f0b010d

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "Text2":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 267
    .restart local v3    # "Text2":Landroid/widget/TextView;
    if-eqz v3, :cond_f

    .line 268
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText2:Ljava/lang/String;

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    :cond_f
    const v10, 0x7f0b0105

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    .line 272
    .local v7, "imgRightBtn":Landroid/widget/ImageButton;
    if-eqz v7, :cond_1

    .line 274
    new-instance v10, Lcom/infraware/polarisoffice5/common/MultiAdapter$2;

    invoke-direct {v10, p0}, Lcom/infraware/polarisoffice5/common/MultiAdapter$2;-><init>(Lcom/infraware/polarisoffice5/common/MultiAdapter;)V

    invoke-virtual {v7, v10}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 283
    .end local v7    # "imgRightBtn":Landroid/widget/ImageButton;
    :pswitch_f
    const v10, 0x7f0b010c

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "imgIcon":Landroid/widget/ImageView;
    check-cast v6, Landroid/widget/ImageView;

    .line 284
    .restart local v6    # "imgIcon":Landroid/widget/ImageView;
    const-string/jumbo v11, "MultiAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "position = "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v12, "  IconResource = "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mItemId:I

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v11, v10}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mItemId:I

    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 289
    :pswitch_10
    const v10, 0x7f0b0104

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "Text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 290
    .restart local v2    # "Text":Landroid/widget/TextView;
    if-eqz v2, :cond_10

    .line 293
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 295
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    .line 297
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setListSubtitleHoveringListener(Landroid/widget/TextView;)Z

    .line 300
    :cond_10
    const v10, 0x7f0b0109

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 301
    .local v4, "checkBox1":Landroid/widget/CheckBox;
    if-eqz v4, :cond_1

    .line 303
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 304
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 305
    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 306
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mRightBtnState:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_11

    .line 307
    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 310
    :goto_6
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    invoke-virtual {v4, v10}, Landroid/widget/CheckBox;->setButtonDrawable(I)V

    goto/16 :goto_1

    .line 309
    :cond_11
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_6

    .line 314
    .end local v4    # "checkBox1":Landroid/widget/CheckBox;
    :pswitch_11
    const v10, 0x7f0b0104

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "Text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 315
    .restart local v2    # "Text":Landroid/widget/TextView;
    if-eqz v2, :cond_12

    .line 316
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_15

    .line 318
    const v10, -0xa0a0b

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 322
    :cond_12
    :goto_7
    const v10, 0x7f0b0106

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "imgIcon":Landroid/widget/ImageView;
    check-cast v6, Landroid/widget/ImageView;

    .line 323
    .restart local v6    # "imgIcon":Landroid/widget/ImageView;
    if-eqz v6, :cond_13

    .line 325
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 326
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    const/4 v11, 0x1

    if-ne v10, v11, :cond_16

    .line 327
    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 353
    :cond_13
    :goto_8
    const v10, 0x7f0b0107

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 354
    .local v5, "checkBox2":Landroid/widget/CheckBox;
    if-eqz v5, :cond_14

    .line 356
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 357
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 358
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 359
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mRightBtnState:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_17

    .line 360
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 363
    :goto_9
    iget-object v10, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v10, v10, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    invoke-virtual {v5, v10}, Landroid/widget/CheckBox;->setButtonDrawable(I)V

    .line 366
    :cond_14
    const v10, 0x7f0b0108

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 367
    .local v1, "RightBtn2":Landroid/widget/TextView;
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    const/16 v10, 0x8

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 370
    const/4 v10, 0x2

    if-ne p1, v10, :cond_18

    .line 371
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    const/16 v10, 0x8

    invoke-virtual {v5, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 373
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 320
    .end local v1    # "RightBtn2":Landroid/widget/TextView;
    .end local v5    # "checkBox2":Landroid/widget/CheckBox;
    :cond_15
    const v10, -0x9c9690

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_7

    .line 329
    :cond_16
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_8

    .line 362
    .restart local v5    # "checkBox2":Landroid/widget/CheckBox;
    :cond_17
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_9

    .line 375
    .restart local v1    # "RightBtn2":Landroid/widget/TextView;
    :cond_18
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 376
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 377
    const/16 v10, 0x8

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 123
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 71
    const/16 v0, 0x9

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 401
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v0, v0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    if-ne v0, v1, :cond_0

    .line 402
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 409
    :goto_0
    return v0

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v0, v0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    if-ne v0, v2, :cond_1

    .line 404
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    goto :goto_0

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget v0, v0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    if-ne v0, v2, :cond_2

    .line 407
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/MultiListItem;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 409
    goto :goto_0
.end method

.method public onDrop(II)V
    .locals 1
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 422
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->getItem(I)Lcom/infraware/polarisoffice5/common/MultiListItem;

    move-result-object v0

    .line 423
    .local v0, "temp":Lcom/infraware/polarisoffice5/common/MultiListItem;
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->removeItem(I)V

    .line 424
    invoke-virtual {p0, p2, v0}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->InsertItem(ILcom/infraware/polarisoffice5/common/MultiListItem;)V

    .line 425
    return-void
.end method

.method public onRemove(I)V
    .locals 1
    .param p1, "which"    # I

    .prologue
    .line 413
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/MultiAdapter;->removeItem(I)V

    goto :goto_0
.end method

.method public removeItem(I)V
    .locals 1
    .param p1, "location"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->arSrcItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 398
    return-void
.end method

.method public setButton(Landroid/widget/Button;)V
    .locals 0
    .param p1, "nEditBtn"    # Landroid/widget/Button;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/MultiAdapter;->mEditBtn:Landroid/widget/Button;

    .line 76
    return-void
.end method
