.class Lcom/infraware/polarisoffice5/common/MemoView$3;
.super Ljava/lang/Object;
.source "MemoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/MemoView;->init(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/MemoView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/MemoView;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x1

    .line 167
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->access$000(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 168
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->ISheetDeleteCommentText()V

    .line 169
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->access$400(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->HideMemo()V

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->access$200(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v2

    if-eq v2, v4, :cond_0

    .line 176
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 177
    .local v0, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->access$200(Lcom/infraware/polarisoffice5/common/MemoView;)I

    move-result v2

    iput v2, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 178
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # getter for: Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    move-result v1

    .line 179
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    const/4 v3, 0x0

    # setter for: Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/common/MemoView;->access$302(Lcom/infraware/polarisoffice5/common/MemoView;Z)Z

    .line 180
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    # setter for: Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I
    invoke-static {v2, v4}, Lcom/infraware/polarisoffice5/common/MemoView;->access$202(Lcom/infraware/polarisoffice5/common/MemoView;I)I

    .line 181
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView$3;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->hide()V

    goto :goto_0
.end method
