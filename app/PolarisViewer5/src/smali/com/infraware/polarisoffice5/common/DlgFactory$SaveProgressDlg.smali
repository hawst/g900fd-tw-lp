.class public Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;
.super Landroid/app/ProgressDialog;
.source "DlgFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/DlgFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SaveProgressDlg"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V
    .locals 2
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;

    .line 196
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v0

    invoke-direct {p0, p2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 198
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setMessage(Ljava/lang/CharSequence;)V

    .line 201
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg$1;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 214
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setCancelable(Z)V

    .line 215
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setCanceledOnTouchOutside(Z)V

    .line 216
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg$2;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 227
    return-void
.end method

.method constructor <init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "str"    # Ljava/lang/String;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;

    .line 230
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v0

    invoke-direct {p0, p2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 232
    invoke-virtual {p0, p3}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setMessage(Ljava/lang/CharSequence;)V

    .line 235
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg$3;

    invoke-direct {v1, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg$3;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;Lcom/infraware/polarisoffice5/common/DlgFactory;)V

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 243
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setCancelable(Z)V

    .line 244
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setCanceledOnTouchOutside(Z)V

    .line 245
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg$4;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg$4;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;Lcom/infraware/polarisoffice5/common/DlgFactory;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 252
    return-void
.end method
