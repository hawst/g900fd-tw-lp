.class Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;
.super Ljava/lang/Object;
.source "MarkerOptionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 268
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # invokes: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$600(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    .line 269
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 272
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 273
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$700(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 274
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    if-eqz v1, :cond_2

    .line 275
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickColorPicker(Landroid/view/View;)V

    .line 286
    .end local p1    # "v":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 278
    .restart local p1    # "v":Landroid/view/View;
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    if-eqz v1, :cond_1

    .line 279
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    invoke-virtual {v1, p1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onClickColorPicker(Landroid/view/View;)V

    goto :goto_0

    .line 282
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$700(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 283
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    check-cast p1, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v2

    # setter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I
    invoke-static {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$802(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;I)I

    .line 284
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mColor:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$800(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setColor(I)V

    goto :goto_0
.end method
