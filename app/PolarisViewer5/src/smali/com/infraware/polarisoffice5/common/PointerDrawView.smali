.class public Lcom/infraware/polarisoffice5/common/PointerDrawView;
.super Landroid/widget/ImageView;
.source "PointerDrawView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/PointerDrawView$PointerStatus;,
        Lcom/infraware/polarisoffice5/common/PointerDrawView$EventType;
    }
.end annotation


# static fields
.field private static final DELAY_TIME:I


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mPaint:Landroid/graphics/Paint;

.field private mPointerDrag:[Landroid/graphics/Bitmap;

.field private mPointerDragHalfHeight:I

.field private mPointerDragHalfWidth:I

.field private mPointerIndex:I

.field private mPointerPosition:Landroid/graphics/Point;

.field private mPointerStatus:I

.field private mPointerTap:[Landroid/graphics/Bitmap;

.field private mPointerTapHalfHeight:I

.field private mPointerTapHalfWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    .line 22
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    .line 23
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    .line 24
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    .line 25
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfWidth:I

    .line 26
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfHeight:I

    .line 27
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfWidth:I

    .line 28
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfHeight:I

    .line 29
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    .line 43
    new-instance v0, Lcom/infraware/polarisoffice5/common/PointerDrawView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView$1;-><init>(Lcom/infraware/polarisoffice5/common/PointerDrawView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    .line 65
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setInit()V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    .line 22
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    .line 23
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    .line 24
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    .line 25
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfWidth:I

    .line 26
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfHeight:I

    .line 27
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfWidth:I

    .line 28
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfHeight:I

    .line 29
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    .line 43
    new-instance v0, Lcom/infraware/polarisoffice5/common/PointerDrawView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView$1;-><init>(Lcom/infraware/polarisoffice5/common/PointerDrawView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    .line 70
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setInit()V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    .line 21
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    .line 22
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    .line 23
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    .line 24
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    .line 25
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfWidth:I

    .line 26
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfHeight:I

    .line 27
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfWidth:I

    .line 28
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfHeight:I

    .line 29
    iput v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    .line 43
    new-instance v0, Lcom/infraware/polarisoffice5/common/PointerDrawView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView$1;-><init>(Lcom/infraware/polarisoffice5/common/PointerDrawView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    .line 75
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->setInit()V

    .line 76
    return-void
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/common/PointerDrawView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/PointerDrawView;
    .param p1, "x1"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    return p1
.end method

.method private setInit()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    .line 80
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 83
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 85
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    .line 87
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a4

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v3

    .line 88
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a5

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v4

    .line 89
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a2

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v5

    .line 90
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a1

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v6

    .line 91
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a3

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v7

    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfWidth:I

    .line 93
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfHeight:I

    .line 95
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a4

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v3

    .line 96
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a5

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v4

    .line 97
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a2

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v5

    .line 98
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a1

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v6

    .line 99
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a3

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v7

    .line 100
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfWidth:I

    .line 101
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfHeight:I

    .line 102
    return-void
.end method


# virtual methods
.method public onClear()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 120
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 162
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 166
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTap:[Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 168
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 170
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDrag:[Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/MotionEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 126
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 127
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 129
    .local v1, "y":F
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 130
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 132
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 157
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 158
    return-void

    .line 134
    :pswitch_0
    const/4 v2, 0x0

    iput v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    goto :goto_0

    .line 139
    :pswitch_1
    iput v5, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    .line 141
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfWidth:I

    int-to-float v3, v3

    sub-float v3, v0, v3

    float-to-int v3, v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfHeight:I

    int-to-float v4, v4

    sub-float v4, v1, v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 144
    :pswitch_2
    iget v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    if-nez v2, :cond_1

    .line 145
    iput v6, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    .line 146
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfWidth:I

    int-to-float v3, v3

    sub-float v3, v0, v3

    float-to-int v3, v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerTapHalfHeight:I

    int-to-float v4, v4

    sub-float v4, v1, v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 147
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 149
    :cond_1
    iget v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerStatus:I

    if-ne v2, v5, :cond_0

    .line 151
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerPosition:Landroid/graphics/Point;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfWidth:I

    int-to-float v3, v3

    sub-float v3, v0, v3

    float-to-int v3, v3

    iget v4, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerDragHalfHeight:I

    int-to-float v4, v4

    sub-float v4, v1, v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 152
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 106
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 109
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    goto :goto_0

    .line 110
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 111
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    goto :goto_0

    .line 112
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_4

    .line 113
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    goto :goto_0

    .line 114
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 115
    const/4 v0, 0x4

    iput v0, p0, Lcom/infraware/polarisoffice5/common/PointerDrawView;->mPointerIndex:I

    goto :goto_0
.end method
