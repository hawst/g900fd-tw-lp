.class Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;
.super Ljava/lang/Object;
.source "PenDrawOptionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 447
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 448
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/4 v1, 0x5

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$500(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 451
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/16 v1, 0xe

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    goto :goto_0

    .line 453
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize3:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$600(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 454
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/16 v1, 0x17

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    goto :goto_0

    .line 456
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize4:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$700(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_4

    .line 457
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/16 v1, 0x1f

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    goto :goto_0

    .line 459
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize5:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$800(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_5

    .line 460
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/16 v1, 0x28

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    goto :goto_0

    .line 462
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize6:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$900(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/16 v1, 0x32

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    goto :goto_0
.end method
