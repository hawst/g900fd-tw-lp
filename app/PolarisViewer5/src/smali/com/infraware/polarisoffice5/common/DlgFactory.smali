.class public Lcom/infraware/polarisoffice5/common/DlgFactory;
.super Ljava/lang/Object;
.source "DlgFactory.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/DlgFactory$ProtectReadPasswordDlg;,
        Lcom/infraware/polarisoffice5/common/DlgFactory$ProtectWritePasswordDlg;,
        Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;,
        Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;,
        Lcom/infraware/polarisoffice5/common/DlgFactory$PrintProgressDlg;,
        Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;,
        Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;,
        Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;
    }
.end annotation


# static fields
.field protected static mDlgFactory:Lcom/infraware/polarisoffice5/common/DlgFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/polarisoffice5/common/DlgFactory;->mDlgFactory:Lcom/infraware/polarisoffice5/common/DlgFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/infraware/polarisoffice5/common/DlgFactory;->mDlgFactory:Lcom/infraware/polarisoffice5/common/DlgFactory;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/DlgFactory;-><init>()V

    sput-object v0, Lcom/infraware/polarisoffice5/common/DlgFactory;->mDlgFactory:Lcom/infraware/polarisoffice5/common/DlgFactory;

    .line 34
    :cond_0
    sget-object v0, Lcom/infraware/polarisoffice5/common/DlgFactory;->mDlgFactory:Lcom/infraware/polarisoffice5/common/DlgFactory;

    return-object v0
.end method


# virtual methods
.method public AlertConfirm(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 143
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;

    invoke-direct {v0, p0, p1, p2}, Lcom/infraware/polarisoffice5/common/DlgFactory$1MyRunnable;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 144
    return-void
.end method

.method public createExportToPdfDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 52
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    .line 53
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;
    return-object v0
.end method

.method public createLayoutDlg(Landroid/app/Activity;ILjava/util/ArrayList;)Lcom/infraware/polarisoffice5/common/LayoutDlg;
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "aEventType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/infraware/polarisoffice5/common/LayoutDlg;"
        }
    .end annotation

    .prologue
    .line 102
    .local p3, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 103
    new-instance v0, Lcom/infraware/polarisoffice5/common/LayoutDlg;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v0, p1, v1, p2, p3}, Lcom/infraware/polarisoffice5/common/LayoutDlg;-><init>(Landroid/content/Context;IILjava/util/ArrayList;)V

    .line 107
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/LayoutDlg;
    :goto_0
    return-object v0

    .line 105
    .end local v0    # "dlg":Lcom/infraware/polarisoffice5/common/LayoutDlg;
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/LayoutDlg;

    invoke-direct {v0, p1, p2, p3}, Lcom/infraware/polarisoffice5/common/LayoutDlg;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .restart local v0    # "dlg":Lcom/infraware/polarisoffice5/common/LayoutDlg;
    goto :goto_0
.end method

.method public createOpenProgressDlg(Landroid/app/Activity;Ljava/lang/String;)Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;

    invoke-direct {v0, p0, p1, p2}, Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;Ljava/lang/String;)V

    .line 39
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/DlgFactory$OpenProgressDlg;
    return-object v0
.end method

.method public createPageLayoutProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 83
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    .line 84
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;
    return-object v0
.end method

.method public createPrintProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$PrintProgressDlg;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 57
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$PrintProgressDlg;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$PrintProgressDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    .line 58
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/DlgFactory$PrintProgressDlg;
    return-object v0
.end method

.method public createProtectReadPasswordDlg(Landroid/app/Activity;)Landroid/app/AlertDialog;
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 74
    instance-of v1, p1, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    if-eqz v1, :cond_0

    .line 75
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    .line 77
    :cond_0
    new-instance v1, Lcom/infraware/polarisoffice5/common/DlgFactory$ProtectReadPasswordDlg;

    invoke-direct {v1, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$ProtectReadPasswordDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/DlgFactory$ProtectReadPasswordDlg;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 78
    .local v0, "dlg":Landroid/app/AlertDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0
.end method

.method public createProtectWritePasswordDlg(Landroid/app/Activity;)Landroid/app/AlertDialog;
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 67
    new-instance v1, Lcom/infraware/polarisoffice5/common/DlgFactory$ProtectWritePasswordDlg;

    invoke-direct {v1, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$ProtectWritePasswordDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/DlgFactory$ProtectWritePasswordDlg;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 68
    .local v0, "dlg":Landroid/app/AlertDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 69
    return-object v0
.end method

.method public createSaveProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 48
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    .line 49
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;
    return-object v0
.end method

.method public createSearchProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 43
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    .line 44
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;
    return-object v0
.end method

.method public createSheetProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 62
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V

    .line 63
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;
    return-object v0
.end method

.method public createViewSettDlg(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
    .locals 3
    .param p1, "activity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .param p2, "aEventType"    # I

    .prologue
    .line 90
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 91
    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v0, p1, v1, p2}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;II)V

    .line 95
    .local v0, "dlg":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
    :goto_0
    return-object v0

    .line 93
    .end local v0    # "dlg":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-direct {v0, p1, p2}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)V

    .restart local v0    # "dlg":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
    goto :goto_0
.end method

.method public dismisProgress(Landroid/app/Activity;I)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "id"    # I

    .prologue
    .line 114
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/app/Activity;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v0

    goto :goto_0
.end method
