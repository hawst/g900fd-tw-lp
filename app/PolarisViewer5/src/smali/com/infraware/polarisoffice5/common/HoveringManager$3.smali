.class final Lcom/infraware/polarisoffice5/common/HoveringManager$3;
.super Ljava/lang/Object;
.source "HoveringManager.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/HoveringManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motion"    # Landroid/view/MotionEvent;

    .prologue
    const v8, 0x7f050097

    const/16 v7, 0xa

    const/16 v5, 0x9

    const/4 v6, 0x0

    .line 473
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 518
    :cond_0
    :goto_0
    return v6

    .line 476
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HOVERING(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 479
    instance-of v4, p1, Landroid/widget/ImageView;

    if-eqz v4, :cond_3

    move-object v2, p1

    .line 481
    check-cast v2, Landroid/widget/ImageView;

    .line 483
    .local v2, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 485
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v1, :cond_2

    .line 486
    invoke-virtual {v2}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 488
    :cond_2
    if-eqz v1, :cond_0

    .line 491
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v5, :cond_4

    .line 493
    invoke-virtual {v2}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 494
    .local v0, "color":I
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 502
    .end local v0    # "color":I
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "imageView":Landroid/widget/ImageView;
    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 503
    .local v3, "pairView":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 505
    instance-of v4, v3, Landroid/widget/TextView;

    if-eqz v4, :cond_0

    .line 507
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v5, :cond_5

    .line 509
    check-cast v3, Landroid/widget/TextView;

    .end local v3    # "pairView":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 496
    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v7, :cond_3

    .line 498
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_1

    .line 511
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "imageView":Landroid/widget/ImageView;
    .restart local v3    # "pairView":Ljava/lang/Object;
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v7, :cond_0

    .line 513
    check-cast v3, Landroid/widget/TextView;

    .end local v3    # "pairView":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0501ce

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method
