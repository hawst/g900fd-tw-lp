.class Lcom/infraware/polarisoffice5/common/MemoView$4;
.super Ljava/lang/Object;
.source "MemoView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/MemoView;->init(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/MemoView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/MemoView;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/MemoView$4;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 216
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 214
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 210
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView$4;->this$0:Lcom/infraware/polarisoffice5/common/MemoView;

    const/4 v1, 0x1

    # setter for: Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/MemoView;->access$302(Lcom/infraware/polarisoffice5/common/MemoView;Z)Z

    .line 211
    return-void
.end method
