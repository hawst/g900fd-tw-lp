.class public Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;
.super Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;
.source "OneButtonDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$PrintNoContentDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$WritePasswordOnlyDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReadPasswordOnlyDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$EditProtextDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReadOnlyDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$AlarmNetworkDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$abstractDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;
    }
.end annotation


# instance fields
.field private mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;Lcom/infraware/polarisoffice5/common/dialog/DialogManager;)V
    .locals 3
    .param p1, "a_context"    # Landroid/content/Context;
    .param p2, "a_type"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
    .param p3, "a_Instance"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;-><init>(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;Lcom/infraware/polarisoffice5/common/dialog/DialogManager;)V

    .line 27
    const v0, 0x7f070063

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mPositiveId:I

    .line 28
    sget-object v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;->$SwitchMap$com$infraware$polarisoffice5$common$dialog$DialogManager$DialogType:[I

    invoke-virtual {p2}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 40
    :goto_0
    return-void

    .line 30
    :pswitch_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$SearchResultDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    goto :goto_0

    .line 31
    :pswitch_1
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReplaceResultDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    goto :goto_0

    .line 32
    :pswitch_2
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReadOnlyDialog;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReadOnlyDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    goto :goto_0

    .line 33
    :pswitch_3
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$WritePasswordOnlyDialog;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$WritePasswordOnlyDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    goto :goto_0

    .line 34
    :pswitch_4
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReadPasswordOnlyDialog;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$ReadPasswordOnlyDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    goto :goto_0

    .line 35
    :pswitch_5
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$EditProtextDialog;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$EditProtextDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    goto :goto_0

    .line 36
    :pswitch_6
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$PrintNoContentDialog;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$PrintNoContentDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    goto :goto_0

    .line 38
    :pswitch_7
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$AlarmNetworkDialog;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$AlarmNetworkDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method protected varargs inflateDialog([Ljava/lang/Object;)V
    .locals 1
    .param p1, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    invoke-interface {v0, p1}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;->inflateDialog([Ljava/lang/Object;)V

    .line 55
    invoke-super {p0, p1}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->inflateDialog([Ljava/lang/Object;)V

    .line 56
    return-void
.end method

.method public onLocaleChanged()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog;->mOneDialog:Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/common/dialog/OneButtonDialog$OneDialogable;->onLocaleChanged()V

    .line 63
    invoke-super {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->onLocaleChanged()V

    .line 64
    return-void
.end method
