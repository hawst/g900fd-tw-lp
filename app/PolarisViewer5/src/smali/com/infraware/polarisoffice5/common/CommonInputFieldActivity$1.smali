.class Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity$1;
.super Ljava/lang/Object;
.source "CommonInputFieldActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 87
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 86
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 80
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mInputField:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->access$000(Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "fileName":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;)Lcom/infraware/office/actionbar/ActionTitleBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->getButton()Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 85
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;->access$100(Lcom/infraware/polarisoffice5/common/CommonInputFieldActivity;)Lcom/infraware/office/actionbar/ActionTitleBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->getButton()Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
