.class public Lcom/infraware/polarisoffice5/common/VideoFrameLayout;
.super Landroid/widget/FrameLayout;
.source "VideoFrameLayout.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_VIDEO_STATUS;


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private hasAudioFocus:Z

.field private mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field public mFullScreenDialog:Landroid/app/AlertDialog;

.field public mRcVideo:Landroid/graphics/Rect;

.field private mStrVideoPath:Ljava/lang/String;

.field public mVideoProgress:Landroid/app/ProgressDialog;

.field private mVideoView:Landroid/widget/VideoView;

.field private mVideoViewHolder:Landroid/widget/FrameLayout;

.field public m_bBackupDocModifiedFlag:Z

.field private m_lInsertVideoTotalSize:J

.field private m_nVideoH:I

.field private m_nVideoStatus:I

.field private m_nVideoW:I

.field private mbVideoCtrlAttached:Z

.field prevPoint:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, -0x64

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 91
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 44
    const-string/jumbo v0, "VideoFrameLayout"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->LOG_CAT:Ljava/lang/String;

    .line 46
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 48
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    .line 49
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    .line 51
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    .line 52
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    .line 54
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    .line 57
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_bBackupDocModifiedFlag:Z

    .line 61
    iput v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoStatus:I

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_lInsertVideoTotalSize:J

    .line 64
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    .line 67
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioManager:Landroid/media/AudioManager;

    .line 68
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 69
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->hasAudioFocus:Z

    .line 568
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v4, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    .line 93
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    .line 94
    new-instance v0, Landroid/widget/VideoView;

    invoke-direct {v0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    .line 95
    check-cast p1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 97
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->init()V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v4, -0x64

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 81
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const-string/jumbo v0, "VideoFrameLayout"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->LOG_CAT:Ljava/lang/String;

    .line 46
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 48
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    .line 49
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    .line 51
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    .line 52
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    .line 54
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    .line 57
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_bBackupDocModifiedFlag:Z

    .line 61
    iput v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoStatus:I

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_lInsertVideoTotalSize:J

    .line 64
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    .line 67
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioManager:Landroid/media/AudioManager;

    .line 68
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 69
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->hasAudioFocus:Z

    .line 568
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v4, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    .line 83
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    .line 84
    new-instance v0, Landroid/widget/VideoView;

    invoke-direct {v0, p1, p2}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    .line 85
    check-cast p1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 87
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->init()V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v4, -0x64

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const-string/jumbo v0, "VideoFrameLayout"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->LOG_CAT:Ljava/lang/String;

    .line 46
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 48
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    .line 49
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    .line 51
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    .line 52
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    .line 54
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    .line 57
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_bBackupDocModifiedFlag:Z

    .line 61
    iput v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoStatus:I

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_lInsertVideoTotalSize:J

    .line 64
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    .line 67
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioManager:Landroid/media/AudioManager;

    .line 68
    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 69
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->hasAudioFocus:Z

    .line 568
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v4, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    .line 74
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    .line 75
    new-instance v0, Landroid/widget/VideoView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    .line 76
    check-cast p1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 77
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->init()V

    .line 78
    return-void
.end method

.method private BGMPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 140
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->hasAudioFocus:Z

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 142
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->hasAudioFocus:Z

    .line 144
    :cond_0
    return-void
.end method

.method private BGMResume()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->hasAudioFocus:Z

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->hasAudioFocus:Z

    .line 152
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->BGMPause()V

    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    return v0
.end method

.method static synthetic access$102(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    return p1
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Landroid/widget/VideoView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .prologue
    .line 43
    iget v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoW:I

    return v0
.end method

.method static synthetic access$402(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoW:I

    return p1
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .prologue
    .line 43
    iget v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoH:I

    return v0
.end method

.method static synthetic access$502(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoH:I

    return p1
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    return-object v0
.end method


# virtual methods
.method public checkDefaultVideoSize()Z
    .locals 5

    .prologue
    const/16 v4, 0xaa

    .line 686
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 687
    .local v1, "nWidth":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 689
    .local v0, "nHeight":I
    if-le v1, v4, :cond_0

    if-le v0, v4, :cond_0

    .line 690
    const/4 v2, 0x1

    .line 691
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public checkExistVideoFile()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 235
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 237
    .local v0, "oVideoFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    const/4 v1, 0x1

    .line 247
    :goto_0
    return v1

    .line 242
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 243
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 244
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v4, 0x7f0702e3

    invoke-virtual {v3, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v2

    .line 247
    goto :goto_0
.end method

.method public getSize(J)I
    .locals 8
    .param p1, "size"    # J

    .prologue
    .line 788
    const/high16 v0, 0x4e800000

    .local v0, "nGiga":F
    const/high16 v2, 0x49800000    # 1048576.0f

    .local v2, "nMega":F
    const/high16 v1, 0x44800000    # 1024.0f

    .line 789
    .local v1, "nKilo":F
    const/4 v3, 0x0

    .line 791
    .local v3, "nUnitSize":F
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gez v4, :cond_0

    .line 792
    const/4 v4, 0x0

    .line 803
    :goto_0
    return v4

    .line 794
    :cond_0
    long-to-float v4, p1

    cmpl-float v4, v4, v0

    if-ltz v4, :cond_1

    .line 795
    long-to-float v4, p1

    div-float v3, v4, v0

    .line 803
    :goto_1
    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v4, v4

    goto :goto_0

    .line 796
    :cond_1
    long-to-float v4, p1

    cmpl-float v4, v4, v2

    if-ltz v4, :cond_2

    .line 797
    long-to-float v4, p1

    div-float v3, v4, v2

    goto :goto_1

    .line 798
    :cond_2
    long-to-float v4, p1

    cmpl-float v4, v4, v1

    if-ltz v4, :cond_3

    .line 799
    long-to-float v4, p1

    div-float v3, v4, v1

    goto :goto_1

    .line 801
    :cond_3
    long-to-float v3, p1

    goto :goto_1
.end method

.method public getVideoFileName(I)Ljava/lang/String;
    .locals 2
    .param p1, "nIdx"    # I

    .prologue
    .line 636
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 638
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getVideoRealPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 632
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoStatus()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoStatus:I

    return v0
.end method

.method public init()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 103
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioManager:Landroid/media/AudioManager;

    .line 104
    new-instance v0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$1;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 120
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 125
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0, v6}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->addView(Landroid/view/View;I)V

    .line 127
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    const/16 v4, 0x11

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v5}, Landroid/widget/VideoView;->setZOrderMediaOverlay(Z)V

    .line 133
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v1, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 136
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 155
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    .line 156
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    .line 158
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    .line 160
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    .line 161
    return-void
.end method

.method public onPauseVideo()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 513
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 501
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->canPause()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 503
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    goto :goto_0

    .line 506
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->canPause()Z

    move-result v0

    if-nez v0, :cond_3

    .line 507
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->removeVideoCtrl()V

    goto :goto_0

    .line 510
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 511
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    goto :goto_0
.end method

.method public onPlayVideo(II)Z
    .locals 8
    .param p1, "nTouchX"    # I
    .param p2, "nTocuhY"    # I

    .prologue
    const/4 v7, 0x0

    .line 165
    const-string/jumbo v3, "VideoFrameLayout"

    const-string/jumbo v4, "onPlayVideo"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$2;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$2;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    const-wide/16 v5, 0x3e8

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 175
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setSelectedVideoInfo()Z

    move-result v1

    .line 177
    .local v1, "bRet":Z
    if-eqz v1, :cond_2

    .line 179
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->checkExistVideoFile()Z

    move-result v1

    .line 181
    if-eqz v1, :cond_0

    .line 183
    const/4 v0, 0x0

    .line 184
    .local v0, "bFullScreenPlay":Z
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IDocumentModified_Editor()Z

    move-result v3

    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_bBackupDocModifiedFlag:Z

    .line 186
    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->startFullScreen()Z

    .line 230
    .end local v0    # "bFullScreenPlay":Z
    :cond_0
    :goto_0
    return v1

    .line 190
    .restart local v0    # "bFullScreenPlay":Z
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 192
    .local v2, "oVideoHolderParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 193
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 195
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 196
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 198
    const-string/jumbo v3, "VideoFrameLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Holder W "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " Holder H "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string/jumbo v3, "VideoFrameLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Holder X "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getX()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " Holder Y "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getY()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v3, v7}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 204
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 205
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVisibility(I)V

    .line 207
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    .line 208
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoViewListener()Z

    .line 210
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v4, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$3;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$3;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/VideoView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 223
    .end local v0    # "bFullScreenPlay":Z
    .end local v2    # "oVideoHolderParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_3

    .line 224
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 225
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 227
    :cond_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v3, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v5, 0x7f0702e3

    invoke-virtual {v4, v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method public onStopVideo()V
    .locals 2

    .prologue
    .line 519
    const-string/jumbo v0, "VideoFrameLayout"

    const-string/jumbo v1, "onStopVideo"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    if-nez v0, :cond_1

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 524
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoStatus:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 528
    :pswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->removeVideoCtrl()V

    goto :goto_0

    .line 524
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/16 v8, -0x64

    .line 572
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    if-nez v4, :cond_1

    .line 573
    :cond_0
    const/4 v3, 0x0

    .line 625
    :goto_0
    :pswitch_0
    return v3

    .line 575
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v1, v4

    .line 576
    .local v1, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v2, v4

    .line 580
    .local v2, "y":I
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 582
    .local v0, "tempRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 620
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onStopVideo()V

    .line 621
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    invoke-virtual {v4, v8, v8}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 585
    :pswitch_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    invoke-virtual {v4, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 592
    :pswitch_2
    add-int/lit8 v4, v1, -0xa

    add-int/lit8 v5, v2, -0xa

    add-int/lit8 v6, v1, 0xa

    add-int/lit8 v7, v2, 0xa

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 594
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-nez v4, :cond_2

    .line 598
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onStopVideo()V

    .line 599
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    invoke-virtual {v4, v8, v8}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 604
    :cond_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v4, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 607
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onPauseVideo()V

    goto :goto_0

    .line 613
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onStopVideo()V

    .line 614
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->prevPoint:Landroid/graphics/Point;

    invoke-virtual {v4, v8, v8}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 582
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public playFullScreen()V
    .locals 8

    .prologue
    const v7, 0x7f0702b1

    const/4 v6, 0x0

    .line 729
    const-string/jumbo v3, "VideoFrameLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "playFullScreen Path "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/infraware/common/util/Utils;->getUnknownIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 732
    .local v1, "oVideoIntent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 736
    :try_start_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v4, 0x7f0702ae

    invoke-virtual {v3, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 737
    .local v2, "wrapperVideoIntent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const/16 v4, 0x28

    invoke-virtual {v3, v2, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    .end local v2    # "wrapperVideoIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 740
    :catch_0
    move-exception v0

    .line 742
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v4, v7}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 745
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v4, v7}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public removeVideoCtrl()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 436
    const-string/jumbo v0, "VideoFrameLayout"

    const-string/jumbo v1, "removeVideoCtrl"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->BGMResume()V

    .line 439
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 444
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoViewHolder:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    if-nez v0, :cond_2

    .line 453
    :cond_1
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    .line 454
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    .line 492
    :goto_0
    return-void

    .line 458
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 459
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 461
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v1, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$12;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->post(Ljava/lang/Runnable;)Z

    .line 488
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v2}, Landroid/widget/VideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v2}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 491
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v2}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    goto :goto_0
.end method

.method public resizeVideo()V
    .locals 8

    .prologue
    .line 537
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 538
    .local v2, "nObjWidth":I
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 540
    .local v1, "nObjHeight":I
    iget v6, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoH:I

    mul-int/2addr v6, v2

    iget v7, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoW:I

    mul-int/2addr v7, v1

    if-le v6, v7, :cond_0

    const/4 v0, 0x1

    .line 542
    .local v0, "bCompare":Z
    :goto_0
    const/4 v4, 0x0

    .local v4, "nVideoWidth":I
    const/4 v3, 0x0

    .line 543
    .local v3, "nVideoHeight":I
    if-eqz v0, :cond_1

    .line 545
    iget v6, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoW:I

    mul-int/2addr v6, v1

    iget v7, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoH:I

    div-int v4, v6, v7

    .line 546
    move v3, v1

    .line 554
    :goto_1
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v6}, Landroid/widget/VideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 555
    .local v5, "oLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    iput v4, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 556
    iput v3, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 558
    iget-object v6, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v7, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$13;

    invoke-direct {v7, p0, v5}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$13;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6, v7}, Landroid/widget/VideoView;->post(Ljava/lang/Runnable;)Z

    .line 566
    return-void

    .line 540
    .end local v0    # "bCompare":Z
    .end local v3    # "nVideoHeight":I
    .end local v4    # "nVideoWidth":I
    .end local v5    # "oLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 550
    .restart local v0    # "bCompare":Z
    .restart local v3    # "nVideoHeight":I
    .restart local v4    # "nVideoWidth":I
    :cond_1
    move v4, v2

    .line 551
    iget v6, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoH:I

    mul-int/2addr v6, v2

    iget v7, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoW:I

    div-int v3, v6, v7

    goto :goto_1
.end method

.method public setSelectedVideoInfo()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 400
    const-string/jumbo v2, "VideoFrameLayout"

    const-string/jumbo v3, "setSelectedVideoInfo"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    if-eqz v2, :cond_0

    .line 404
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 405
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->removeVideoCtrl()V

    .line 410
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    .line 411
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->showVideoProgress(Z)V

    .line 412
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetVideoPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    .line 414
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 431
    :cond_1
    :goto_1
    return v0

    .line 406
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->canPause()Z

    move-result v2

    if-nez v2, :cond_0

    .line 407
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->removeVideoCtrl()V

    goto :goto_0

    .line 416
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    .line 419
    const-string/jumbo v2, "VideoFrameLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "EngineGetVideoPath"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mStrVideoPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    .line 422
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IGetVideoRect(Landroid/graphics/Rect;)V

    .line 424
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 426
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-eqz v2, :cond_1

    .line 429
    const-string/jumbo v0, "VideoFrameLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mRcVideo L "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "T "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "R "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "B "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mRcVideo:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 431
    goto/16 :goto_1
.end method

.method public setVideoStatus(I)V
    .locals 0
    .param p1, "nVideoStatus"    # I

    .prologue
    .line 390
    iput p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_nVideoStatus:I

    .line 391
    return-void
.end method

.method public setVideoViewListener()Z
    .locals 3

    .prologue
    .line 253
    const/4 v0, 0x1

    .line 255
    .local v0, "bRet":Z
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mbVideoCtrlAttached:Z

    if-nez v1, :cond_1

    .line 256
    :cond_0
    const/4 v1, 0x0

    .line 385
    :goto_0
    return v1

    .line 259
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$4;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 295
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$5;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$5;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 303
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$6;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$6;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 321
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$7;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$7;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 332
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$8;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$8;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$9;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$9;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 351
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$10;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 377
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$11;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$11;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->setOnZoomChangeListener(Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;)V

    .line 385
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public showFullScreenVideoDialog()V
    .locals 5

    .prologue
    .line 696
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    .line 698
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v2, v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    .line 699
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v3, 0x7f0700c9

    invoke-virtual {v2, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 700
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v3, 0x7f0700c8

    invoke-virtual {v2, v3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 702
    new-instance v0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$15;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$15;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    .line 718
    .local v0, "oClickListener":Landroid/content/DialogInterface$OnClickListener;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v4, 0x7f070063

    invoke-virtual {v3, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 719
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    const/4 v2, -0x2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v4, 0x7f07005f

    invoke-virtual {v3, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 720
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 723
    .end local v0    # "oClickListener":Landroid/content/DialogInterface$OnClickListener;
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 724
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mFullScreenDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 725
    :cond_1
    return-void
.end method

.method public showVideoProgress(Z)V
    .locals 3
    .param p1, "a_bShow"    # Z

    .prologue
    .line 642
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 645
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    .line 647
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 648
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v2, 0x7f0701bf

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 649
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$14;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$14;-><init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 659
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 660
    if-eqz p1, :cond_2

    .line 661
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 662
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 666
    :cond_1
    :goto_0
    return-void

    .line 664
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mVideoProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method public startFullScreen()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 672
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->checkDefaultVideoSize()Z

    move-result v0

    .line 674
    .local v0, "bAvailableMinSize":Z
    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v5, v5, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v5}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    .line 675
    .local v1, "ci":Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    iget v3, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nZoomRatio:I

    .line 676
    .local v3, "nZoomRatio":I
    const/4 v2, 0x0

    .line 677
    .local v2, "nOrientation":I
    iget v5, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nMinZoom:I

    if-ne v3, v5, :cond_0

    if-ne v2, v4, :cond_0

    if-nez v0, :cond_1

    .line 679
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->showFullScreenVideoDialog()V

    .line 682
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public supportLinkedVideo(JLjava/lang/String;)Z
    .locals 6
    .param p1, "lDocSize"    # J
    .param p3, "strVideoPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 769
    const-wide/16 v2, 0x32

    .line 770
    .local v2, "nDocMaxSize":J
    invoke-virtual {p0, p1, p2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getSize(J)I

    move-result v1

    .line 772
    .local v1, "nChkDocSize":I
    const/16 v5, 0x32

    if-ge v1, v5, :cond_0

    .line 774
    const/4 v0, 0x1

    .line 775
    .local v0, "bSupportEmbedded":Z
    invoke-virtual {p0, p3}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->updateVideoInsertSize(Ljava/lang/String;)Z

    move-result v0

    .line 777
    if-eqz v0, :cond_0

    .line 778
    const/4 v4, 0x0

    .line 783
    .end local v0    # "bSupportEmbedded":Z
    :cond_0
    return v4
.end method

.method public updateVideoInsertSize(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strVideoPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 751
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 752
    .local v3, "videofile":Ljava/io/File;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 764
    :cond_0
    :goto_0
    return v4

    .line 755
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 756
    .local v0, "lInsertVideoSize":J
    iget-wide v5, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_lInsertVideoTotalSize:J

    add-long/2addr v5, v0

    invoke-virtual {p0, v5, v6}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getSize(J)I

    move-result v2

    .line 758
    .local v2, "nTmpChkSz":I
    const/16 v5, 0x32

    if-ge v2, v5, :cond_0

    .line 760
    iget-wide v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_lInsertVideoTotalSize:J

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->m_lInsertVideoTotalSize:J

    .line 761
    const/4 v4, 0x1

    goto :goto_0
.end method
