.class Lcom/infraware/polarisoffice5/common/EditInputFilter$1;
.super Ljava/lang/Object;
.source "EditInputFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/EditInputFilter;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/EditInputFilter;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/EditInputFilter;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter$1;->this$0:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter$1;->this$0:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    # getter for: Lcom/infraware/polarisoffice5/common/EditInputFilter;->m_nMaxLength:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->access$000(Lcom/infraware/polarisoffice5/common/EditInputFilter;)I

    move-result v2

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    sub-int v4, p6, p5

    sub-int/2addr v3, v4

    sub-int v0, v2, v3

    .line 35
    .local v0, "nRest":I
    if-gez v0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return-object v1

    .line 38
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le v2, v0, :cond_0

    .line 40
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/EditInputFilter$1;->this$0:Lcom/infraware/polarisoffice5/common/EditInputFilter;

    # invokes: Lcom/infraware/polarisoffice5/common/EditInputFilter;->onToastMessage()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/EditInputFilter;->access$100(Lcom/infraware/polarisoffice5/common/EditInputFilter;)V

    .line 41
    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method
