.class Lcom/infraware/polarisoffice5/common/PopupMoreWindow$1;
.super Ljava/lang/Object;
.source "PopupMoreWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/PopupMoreWindow;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/PopupMoreWindow;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$1;->this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 190
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 192
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 193
    .local v0, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 195
    .local v1, "y":I
    if-ltz v0, :cond_0

    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$1;->this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getWidth()I

    move-result v2

    if-gt v0, v2, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$1;->this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 197
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/PopupMoreWindow$1;->this$0:Lcom/infraware/polarisoffice5/common/PopupMoreWindow;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/PopupMoreWindow;->dismiss()V

    .line 200
    .end local v0    # "x":I
    .end local v1    # "y":I
    :cond_1
    const/4 v2, 0x0

    return v2
.end method
