.class public Lcom/infraware/polarisoffice5/common/MultiListItem;
.super Ljava/lang/Object;
.source "MultiListItem.java"


# instance fields
.field public mActionBarCheckIcon:Z

.field mColor:I

.field mDrawableIconNomal:Landroid/graphics/drawable/Drawable;

.field mEnable:Z

.field mIcon2NormalRes:I

.field mIcon2PressRes:I

.field mIcon2SelectedRes:I

.field mIconNormalRes:I

.field mIconPressRes:I

.field mIconSelectedRes:I

.field mItemId:I

.field mRightBtnState:I

.field mText1:Ljava/lang/String;

.field mText2:Ljava/lang/String;

.field mToolBarID:I

.field mType:I


# direct methods
.method public constructor <init>(IILjava/lang/String;I)V
    .locals 1
    .param p1, "aType"    # I
    .param p2, "aIconNRes"    # I
    .param p3, "aText1"    # Ljava/lang/String;
    .param p4, "aItemId"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mActionBarCheckIcon:Z

    .line 46
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    .line 47
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    .line 48
    iput p4, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mItemId:I

    .line 49
    iput p2, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    .line 51
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;II)V
    .locals 1
    .param p1, "aType"    # I
    .param p2, "iconID"    # I
    .param p3, "aText1"    # Ljava/lang/String;
    .param p4, "aColor"    # I
    .param p5, "nToolBarID"    # I

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mActionBarCheckIcon:Z

    .line 65
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    .line 66
    iput p2, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    .line 67
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    .line 68
    iput p4, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mColor:I

    .line 69
    iput p5, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mToolBarID:I

    .line 70
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;IZ)V
    .locals 1
    .param p1, "aType"    # I
    .param p2, "aIconRes"    # I
    .param p3, "aText1"    # Ljava/lang/String;
    .param p4, "aItemId"    # I
    .param p5, "aEnable"    # Z

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mActionBarCheckIcon:Z

    .line 36
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    .line 37
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    .line 38
    iput p2, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    .line 39
    iput p4, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mItemId:I

    .line 40
    iput-boolean p5, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 41
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .locals 2
    .param p1, "aType"    # I
    .param p2, "aText1"    # Ljava/lang/String;
    .param p3, "aItemId"    # I

    .prologue
    const/4 v1, 0x1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mActionBarCheckIcon:Z

    .line 20
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    .line 21
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    .line 22
    iput p3, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mItemId:I

    .line 23
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 24
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IIZ)V
    .locals 1
    .param p1, "aType"    # I
    .param p2, "aText1"    # Ljava/lang/String;
    .param p3, "aIconNRes"    # I
    .param p4, "aColor"    # I
    .param p5, "bEnable"    # Z

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mActionBarCheckIcon:Z

    .line 56
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    .line 57
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    .line 58
    iput p3, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    .line 59
    iput p4, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mColor:I

    .line 60
    iput-boolean p5, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 61
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IZ)V
    .locals 1
    .param p1, "aType"    # I
    .param p2, "aText1"    # Ljava/lang/String;
    .param p3, "aItemId"    # I
    .param p4, "aEnable"    # Z

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mActionBarCheckIcon:Z

    .line 28
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    .line 29
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    .line 30
    iput p3, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mItemId:I

    .line 31
    iput-boolean p4, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 32
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;IIII)V
    .locals 2
    .param p1, "aType"    # I
    .param p2, "aText1"    # Ljava/lang/String;
    .param p3, "aText2"    # Ljava/lang/String;
    .param p4, "aIconNRes"    # I
    .param p5, "aIconPRes"    # I
    .param p6, "aIconSRes"    # I
    .param p7, "aColor"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 99
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mActionBarCheckIcon:Z

    .line 7
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mType:I

    .line 8
    iput-object p2, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    .line 9
    iput-object p3, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText2:Ljava/lang/String;

    .line 10
    iput p4, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    .line 11
    iput p5, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconPressRes:I

    .line 12
    iput p6, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconSelectedRes:I

    .line 13
    iput p7, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mColor:I

    .line 14
    iput v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mRightBtnState:I

    .line 15
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    .line 16
    return-void
.end method


# virtual methods
.method SetButton2(III)V
    .locals 0
    .param p1, "aIconNRes"    # I
    .param p2, "aIconPRes"    # I
    .param p3, "aIconSRes"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIcon2NormalRes:I

    .line 74
    iput p2, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIcon2PressRes:I

    .line 75
    iput p3, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIcon2SelectedRes:I

    .line 76
    return-void
.end method

.method public getIconNormalRes()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mIconNormalRes:I

    return v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mItemId:I

    return v0
.end method

.method public getRightBtnState()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mRightBtnState:I

    return v0
.end method

.method public getText1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    return-object v0
.end method

.method public getText2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText2:Ljava/lang/String;

    return-object v0
.end method

.method public isEnable()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mEnable:Z

    return v0
.end method

.method public setRightBtnState(I)V
    .locals 0
    .param p1, "nState"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mRightBtnState:I

    return-void
.end method

.method public setText1(Ljava/lang/String;)V
    .locals 0
    .param p1, "replaceText"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/MultiListItem;->mText1:Ljava/lang/String;

    return-void
.end method
