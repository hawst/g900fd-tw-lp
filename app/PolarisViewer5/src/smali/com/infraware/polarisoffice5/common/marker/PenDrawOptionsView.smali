.class public Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
.super Landroid/widget/LinearLayout;
.source "PenDrawOptionsView.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_PEN_MODE;
.implements Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;
.implements Lcom/infraware/polarisoffice5/common/dialog/DialogManager$Dialogable;


# static fields
.field private static final PEN_TRANSPARENCY:I = 0xff

.field public static final PEN_TYPE_HIGHLIGHTER:I = 0x1

.field public static final PEN_TYPE_INK:I = 0x2

.field public static final PEN_TYPE_PENCLE:I = 0x0

.field public static final PEN_TYPE_RULER:I = 0x3


# instance fields
.field private final ADUJUST_MARKER_THICKNESS:I

.field private mColor:I

.field private mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

.field private mColorClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mHighlightType:Landroid/widget/ImageView;

.field private mHighlighterThickness:I

.field private mInkThickness:I

.field mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mPenSeekBar:Landroid/widget/SeekBar;

.field private mPenSize1:Landroid/widget/ImageView;

.field private mPenSize2:Landroid/widget/ImageView;

.field private mPenSize3:Landroid/widget/ImageView;

.field private mPenSize4:Landroid/widget/ImageView;

.field private mPenSize5:Landroid/widget/ImageView;

.field private mPenSize6:Landroid/widget/ImageView;

.field private mPenSizeClickListener:Landroid/view/View$OnClickListener;

.field private mPencleThickness:I

.field private mPencleType:Landroid/widget/ImageView;

.field private mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

.field private mRulerThickness:I

.field private mRulerType:Landroid/widget/ImageView;

.field private mSeekBarChanged:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSizeClickListener:Landroid/view/View$OnClickListener;

.field private mThickness:I

.field private mTracking:Z

.field private mType:I

.field private mTypeClickListener:Landroid/view/View$OnClickListener;

.field private mbrushType:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, 0x1f

    const/4 v3, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;

    .line 39
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;

    .line 40
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;

    .line 41
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;

    .line 42
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;

    .line 44
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize1:Landroid/widget/ImageView;

    .line 45
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize2:Landroid/widget/ImageView;

    .line 46
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize3:Landroid/widget/ImageView;

    .line 47
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize4:Landroid/widget/ImageView;

    .line 48
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize5:Landroid/widget/ImageView;

    .line 49
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize6:Landroid/widget/ImageView;

    .line 52
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 53
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 55
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 56
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 57
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 59
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 60
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 61
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 62
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 63
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 72
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->ADUJUST_MARKER_THICKNESS:I

    .line 75
    iput v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I

    .line 76
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    .line 77
    iput v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mInkThickness:I

    .line 78
    iput v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlighterThickness:I

    .line 79
    iput v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerThickness:I

    .line 80
    iput v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleThickness:I

    .line 81
    const v0, -0x19f0f1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    .line 82
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z

    .line 86
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    .line 100
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$1;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHandler:Landroid/os/Handler;

    .line 358
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mSizeClickListener:Landroid/view/View$OnClickListener;

    .line 381
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 444
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSizeClickListener:Landroid/view/View$OnClickListener;

    .line 469
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    .line 523
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mSeekBarChanged:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 552
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    .line 90
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initResource()V

    .line 91
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v4, 0x1f

    const/4 v3, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 95
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;

    .line 39
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;

    .line 40
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;

    .line 41
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;

    .line 42
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;

    .line 44
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize1:Landroid/widget/ImageView;

    .line 45
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize2:Landroid/widget/ImageView;

    .line 46
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize3:Landroid/widget/ImageView;

    .line 47
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize4:Landroid/widget/ImageView;

    .line 48
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize5:Landroid/widget/ImageView;

    .line 49
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize6:Landroid/widget/ImageView;

    .line 52
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 53
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 55
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 56
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 57
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 59
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 60
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 61
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 62
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 63
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 72
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->ADUJUST_MARKER_THICKNESS:I

    .line 75
    iput v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I

    .line 76
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    .line 77
    iput v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mInkThickness:I

    .line 78
    iput v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlighterThickness:I

    .line 79
    iput v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerThickness:I

    .line 80
    iput v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleThickness:I

    .line 81
    const v0, -0x19f0f1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    .line 82
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z

    .line 86
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    .line 100
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$1;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHandler:Landroid/os/Handler;

    .line 358
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$2;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mSizeClickListener:Landroid/view/View$OnClickListener;

    .line 381
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$3;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 444
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$4;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSizeClickListener:Landroid/view/View$OnClickListener;

    .line 469
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    .line 523
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mSeekBarChanged:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 552
    new-instance v0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$7;-><init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    .line 96
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initResource()V

    .line 97
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I

    return p1
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleThickness:I

    return v0
.end method

.method static synthetic access$1600(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlighterThickness:I

    return v0
.end method

.method static synthetic access$1700(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mInkThickness:I

    return v0
.end method

.method static synthetic access$1800(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerThickness:I

    return v0
.end method

.method static synthetic access$1900(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z

    return p1
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    return v0
.end method

.method static synthetic access$2002(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    return p1
.end method

.method static synthetic access$2100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initPenSize()V

    return-void
.end method

.method static synthetic access$2200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    return v0
.end method

.method static synthetic access$2302(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    return p1
.end method

.method static synthetic access$2400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize3:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize4:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize5:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize6:Landroid/widget/ImageView;

    return-object v0
.end method

.method private changeHeight()V
    .locals 7

    .prologue
    const v6, 0x7f060008

    .line 642
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    if-nez v4, :cond_0

    .line 660
    :goto_0
    return-void

    .line 645
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 647
    .local v0, "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 649
    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 650
    .local v1, "height":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 651
    .local v2, "landscape":Landroid/widget/LinearLayout$LayoutParams;
    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 652
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    invoke-virtual {v4, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 655
    .end local v1    # "height":I
    .end local v2    # "landscape":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 656
    .restart local v1    # "height":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 657
    .local v3, "portrait":Landroid/widget/LinearLayout$LayoutParams;
    iput v1, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 658
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    invoke-virtual {v4, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;
    .locals 2

    .prologue
    .line 322
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 323
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 346
    :goto_0
    return-object v0

    .line 324
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 325
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 326
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 327
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 328
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 329
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 330
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 331
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 332
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 333
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 334
    :cond_5
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 335
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 336
    :cond_6
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_7

    .line 337
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 338
    :cond_7
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_8

    .line 339
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 340
    :cond_8
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_9

    .line 341
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto :goto_0

    .line 342
    :cond_9
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_a

    .line 343
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto/16 :goto_0

    .line 344
    :cond_a
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_b

    .line 345
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    goto/16 :goto_0

    .line 346
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private initPenSize()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 223
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 224
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 225
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize3:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 226
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize4:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 227
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize5:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 228
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize6:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 230
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    sparse-switch v0, :sswitch_data_0

    .line 252
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 266
    :goto_1
    return-void

    .line 233
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize1:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 236
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize2:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 239
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize3:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 242
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize4:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 245
    :sswitch_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize5:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 248
    :sswitch_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize6:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 254
    :pswitch_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleThickness:I

    goto :goto_1

    .line 257
    :pswitch_1
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlighterThickness:I

    goto :goto_1

    .line 260
    :pswitch_2
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mInkThickness:I

    goto :goto_1

    .line 263
    :pswitch_3
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerThickness:I

    goto :goto_1

    .line 230
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xe -> :sswitch_1
        0x17 -> :sswitch_2
        0x1f -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch

    .line 252
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private initResource()V
    .locals 5

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 113
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030035

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 114
    .local v1, "linearLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->addView(Landroid/view/View;)V

    .line 116
    const v2, 0x7f0b018a

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    .line 117
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    const v2, 0x7f0b0197

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;

    .line 119
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mSeekBarChanged:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 120
    const-string/jumbo v2, "HYOHYUN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mPenSeekBar.getMax() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getMax()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const v2, 0x7f0b018b

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;

    .line 130
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 133
    const v2, 0x7f0b018d

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;

    .line 134
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 137
    const v2, 0x7f0b018c

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;

    .line 138
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 141
    const v2, 0x7f0b018e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;

    .line 142
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTypeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 146
    const v2, 0x7f0b0190

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize1:Landroid/widget/ImageView;

    .line 147
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSizeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    const v2, 0x7f0b0191

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize2:Landroid/widget/ImageView;

    .line 150
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize2:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSizeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    const v2, 0x7f0b0192

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize3:Landroid/widget/ImageView;

    .line 153
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize3:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSizeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    const v2, 0x7f0b0193

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize4:Landroid/widget/ImageView;

    .line 156
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize4:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSizeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    const v2, 0x7f0b0194

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize5:Landroid/widget/ImageView;

    .line 159
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize5:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSizeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    const v2, 0x7f0b0195

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize6:Landroid/widget/ImageView;

    .line 162
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSize6:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSizeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    const v2, 0x7f0b0198

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 166
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 168
    const v2, 0x7f0b0199

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 169
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 171
    const v2, 0x7f0b019a

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 172
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 174
    const v2, 0x7f0b019b

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 175
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 177
    const v2, 0x7f0b019c

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 178
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 180
    const v2, 0x7f0b019d

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 181
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 183
    const v2, 0x7f0b019e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 184
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 186
    const v2, 0x7f0b019f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 187
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 189
    const v2, 0x7f0b01a0

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 190
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 192
    const v2, 0x7f0b01a1

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 193
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 195
    const v2, 0x7f0b01a2

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 196
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 198
    const v2, 0x7f0b01a3

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    .line 199
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColorClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenDrawOptionsLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 202
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 203
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 204
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 205
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 206
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 207
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05005f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 208
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 209
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050061

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 210
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050062

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 211
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050063

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 212
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050064

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(I)V

    .line 214
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor12:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f020118

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->initResource(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 219
    return-void
.end method

.method private setThickness(I)V
    .locals 1
    .param p1, "nSize"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setThickness(I)V

    .line 352
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    .line 353
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 355
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initPenSize()V

    .line 356
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 688
    return-void
.end method

.method public dismiss()V
    .locals 0

    .prologue
    .line 682
    return-void
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    return v0
.end method

.method public getThickness()I
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    return v0
.end method

.method public getTrackingMode()Z
    .locals 1

    .prologue
    .line 665
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z

    return v0
.end method

.method public getTransparency()I
    .locals 1

    .prologue
    .line 318
    const/16 v0, 0xff

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 306
    iget v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I

    return v0
.end method

.method public initOptions(III)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "thickness"    # I
    .param p3, "color"    # I

    .prologue
    const/4 v3, 0x1

    .line 269
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I

    .line 270
    iput p2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    .line 271
    invoke-virtual {p0, p3}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setOtherColor(I)V

    .line 274
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setThickness(I)V

    .line 275
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setColor(I)V

    .line 276
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    .line 277
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 280
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getColorView()Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    move-result-object v0

    .line 281
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 284
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I

    packed-switch v1, :pswitch_data_0

    .line 301
    :goto_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initPenSize()V

    .line 303
    return-void

    .line 287
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 290
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 291
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    const/16 v2, 0x7f

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    goto :goto_0

    .line 294
    :pswitch_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 297
    :pswitch_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 284
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 693
    const/4 v0, 0x0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 631
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 632
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v1, :cond_0

    .line 633
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 635
    .local v0, "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onChangeFreeDrawMode(I)V

    .line 638
    .end local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_0
    return-void
.end method

.method public onLocaleChanged()V
    .locals 0

    .prologue
    .line 712
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 700
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 706
    return-void
.end method

.method public setOtherColor(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    const/4 v2, 0x0

    .line 578
    iput p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    .line 579
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setColor(I)V

    .line 581
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor01:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 582
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor02:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 583
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor03:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 584
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor04:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 585
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor05:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 586
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor06:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 587
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor07:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 588
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor08:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 589
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor09:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 590
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor10:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 591
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mColor11:Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerColorImageView;->setSelected(Z)V

    .line 599
    return-void
.end method

.method public setTrackingMode(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 669
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z

    .line 670
    return-void
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    .prologue
    .line 604
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 606
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->changeHeight()V

    .line 609
    if-nez p1, :cond_1

    .line 610
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v1, :cond_0

    .line 611
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 613
    .local v0, "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    if-eqz v1, :cond_0

    .line 614
    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    invoke-virtual {v1, p0}, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->registerObserver(Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;)V

    .line 627
    .end local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    const/4 v1, 0x4

    if-eq p1, v1, :cond_2

    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    .line 619
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    if-eqz v1, :cond_0

    .line 620
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 622
    .restart local v0    # "evBaseViewerActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    if-eqz v1, :cond_0

    .line 623
    iget-object v1, v0, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->mConfigurationChangeNotifier:Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;

    invoke-virtual {v1, p0}, Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeNotifier;->removeObserver(Lcom/infraware/polarisoffice5/common/dialog/ConfigurationChangeObserver;)V

    goto :goto_0
.end method

.method public varargs show([Ljava/lang/Object;)V
    .locals 0
    .param p1, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 676
    return-void
.end method
