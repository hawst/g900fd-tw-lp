.class Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;
.super Ljava/lang/Object;
.source "MarkerOptionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->initResource()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 144
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)I

    move-result v1

    add-int/lit8 v1, v1, -0x5

    # setter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$002(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;I)I

    .line 145
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThicknessBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 146
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$200(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$1;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setThickness(I)V

    .line 148
    :cond_0
    return-void
.end method
