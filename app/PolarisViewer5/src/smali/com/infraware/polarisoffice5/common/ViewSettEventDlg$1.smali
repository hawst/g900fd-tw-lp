.class Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;
.super Ljava/lang/Object;
.source "ViewSettEventDlg.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 351
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mListItems:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;

    .line 352
    .local v0, "item":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;
    iget-boolean v1, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->enable:Z

    if-eqz v1, :cond_0

    .line 354
    iget-boolean v1, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;->checked:Z

    .line 355
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->uncheckOtherItems(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$DialogItem;)V

    .line 356
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->mCustomAdapter:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$ViewSettEventDlgAdapter;->notifyDataSetChanged()V

    .line 360
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    # invokes: Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->isItemDefaultChecked()Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->access$000(Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 361
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 367
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v3

    .line 354
    goto :goto_0

    .line 364
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg$1;->this$0:Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method
