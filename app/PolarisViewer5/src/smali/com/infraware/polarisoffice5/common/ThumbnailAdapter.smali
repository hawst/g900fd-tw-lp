.class public Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;
.super Landroid/widget/BaseAdapter;
.source "ThumbnailAdapter.java"


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mThumbsDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 17
    const-string/jumbo v0, "ThumnailAdapter"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->LOG_CAT:Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->mContext:Landroid/content/Context;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->mThumbsDataList:Ljava/util/ArrayList;

    .line 26
    return-void
.end method


# virtual methods
.method public GetLastBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->mThumbsDataList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->mThumbsDataList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public SetBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->mThumbsDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->mThumbsDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 35
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 40
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x7

    .line 57
    move-object v0, p2

    check-cast v0, Landroid/widget/ImageView;

    .line 59
    .local v0, "imageView":Landroid/widget/ImageView;
    if-nez p2, :cond_0

    .line 61
    new-instance v0, Landroid/widget/ImageView;

    .end local v0    # "imageView":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 62
    .restart local v0    # "imageView":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/16 v2, 0x93

    const/16 v3, 0xcf

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 64
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 65
    invoke-virtual {v0, v4, v5, v4, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 75
    :goto_0
    const-string/jumbo v1, "ThumnailAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "imageView Position = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ThumbnailAdapter;->mThumbsDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 78
    return-object v0

    :cond_0
    move-object v0, p2

    .line 68
    check-cast v0, Landroid/widget/ImageView;

    goto :goto_0
.end method
