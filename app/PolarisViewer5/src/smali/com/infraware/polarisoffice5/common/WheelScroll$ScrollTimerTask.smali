.class Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;
.super Ljava/util/TimerTask;
.source "WheelScroll.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/WheelScroll;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScrollTimerTask"
.end annotation


# instance fields
.field mAccel:F

.field mCnt:I

.field mFrom:I

.field protected mHandler:Landroid/os/Handler;

.field mPos:I

.field mPosArray:[D

.field mScrollInterval:J

.field mShowRunnable:Ljava/lang/Runnable;

.field mStartTime:J

.field mTo:I

.field mUnitWidth:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/WheelScroll;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/WheelScroll;ZIII)V
    .locals 10
    .param p2, "bShort"    # Z
    .param p3, "from"    # I
    .param p4, "to"    # I
    .param p5, "unit_width"    # I

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x258

    .line 377
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->this$0:Lcom/infraware/polarisoffice5/common/WheelScroll;

    .line 378
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 349
    iput-wide v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mScrollInterval:J

    .line 356
    const/16 v0, 0x16

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    .line 357
    iput v8, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mCnt:I

    .line 360
    new-instance v0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask$1;-><init>(Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mShowRunnable:Ljava/lang/Runnable;

    .line 369
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mHandler:Landroid/os/Handler;

    .line 380
    sub-int v0, p3, p4

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p5

    div-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mCnt:I

    .line 383
    iget v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mCnt:I

    if-lt v0, v9, :cond_0

    .line 386
    iput-wide v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mScrollInterval:J

    .line 388
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const-wide/high16 v1, 0x4014000000000000L    # 5.0

    aput-wide v1, v0, v8

    .line 389
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/4 v1, 0x1

    const-wide v2, 0x4013c28f5c28f5c3L    # 4.94

    aput-wide v2, v0, v1

    .line 390
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/4 v1, 0x2

    const-wide v2, 0x401347ae147ae148L    # 4.82

    aput-wide v2, v0, v1

    .line 391
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/4 v1, 0x3

    const-wide v2, 0x40128f5c28f5c28fL    # 4.64

    aput-wide v2, v0, v1

    .line 392
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/4 v1, 0x4

    const-wide v2, 0x401199999999999aL    # 4.4

    aput-wide v2, v0, v1

    .line 393
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const-wide v1, 0x4010666666666666L    # 4.1

    aput-wide v1, v0, v9

    .line 394
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/4 v1, 0x6

    const-wide v2, 0x400e666666666666L    # 3.8

    aput-wide v2, v0, v1

    .line 395
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/4 v1, 0x7

    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    aput-wide v2, v0, v1

    .line 396
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0x8

    const-wide v2, 0x400999999999999aL    # 3.2

    aput-wide v2, v0, v1

    .line 397
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0x9

    const-wide v2, 0x4006cccccccccccdL    # 2.85

    aput-wide v2, v0, v1

    .line 398
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0xa

    const-wide/high16 v2, 0x4004000000000000L    # 2.5

    aput-wide v2, v0, v1

    .line 399
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0xb

    const-wide v2, 0x4001333333333333L    # 2.15

    aput-wide v2, v0, v1

    .line 400
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0xc

    const-wide v2, 0x3ffccccccccccccdL    # 1.8

    aput-wide v2, v0, v1

    .line 401
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0xd

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    aput-wide v2, v0, v1

    .line 402
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0xe

    const-wide v2, 0x3ff3333333333333L    # 1.2

    aput-wide v2, v0, v1

    .line 403
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0xf

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    aput-wide v2, v0, v1

    .line 404
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0x10

    const-wide v2, 0x3fe3333333333333L    # 0.6

    aput-wide v2, v0, v1

    .line 405
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0x11

    const-wide v2, 0x3fd70a3d70a3d70aL    # 0.36

    aput-wide v2, v0, v1

    .line 406
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0x12

    const-wide v2, 0x3fc70a3d70a3d70aL    # 0.18

    aput-wide v2, v0, v1

    .line 407
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0x13

    const-wide v2, 0x3faeb851eb851eb8L    # 0.06

    aput-wide v2, v0, v1

    .line 408
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0x14

    aput-wide v6, v0, v1

    .line 409
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    const/16 v1, 0x15

    aput-wide v6, v0, v1

    .line 423
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mStartTime:J

    .line 424
    iput p3, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mFrom:I

    .line 425
    iput p4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mTo:I

    .line 426
    iput p5, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mUnitWidth:I

    .line 430
    return-void

    .line 413
    :cond_0
    if-eqz p2, :cond_1

    .line 414
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mScrollInterval:J

    .line 415
    sub-int v0, p4, p3

    int-to-float v0, v0

    const v1, 0x46afc800    # 22500.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mAccel:F

    goto :goto_0

    .line 418
    :cond_1
    iput-wide v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mScrollInterval:J

    .line 419
    sub-int v0, p4, p3

    int-to-float v0, v0

    const v1, 0x47afc800    # 90000.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mAccel:F

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x1e

    const/4 v8, 0x5

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 433
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mStartTime:J

    sub-long v0, v4, v6

    .line 435
    .local v0, "diffTime":J
    div-long v4, v0, v12

    long-to-int v2, v4

    .line 437
    .local v2, "n":I
    int-to-long v4, v2

    iget-wide v6, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mScrollInterval:J

    div-long/2addr v6, v12

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 439
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->this$0:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget-object v5, v4, Lcom/infraware/polarisoffice5/common/WheelScroll;->mTimerSyncObj:Ljava/lang/Object;

    monitor-enter v5

    .line 440
    :try_start_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->this$0:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    if-eqz v4, :cond_0

    .line 441
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->this$0:Lcom/infraware/polarisoffice5/common/WheelScroll;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v4}, Ljava/util/Timer;->cancel()V

    .line 442
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->this$0:Lcom/infraware/polarisoffice5/common/WheelScroll;

    const/4 v6, 0x0

    iput-object v6, v4, Lcom/infraware/polarisoffice5/common/WheelScroll;->mScrollTimer:Ljava/util/Timer;

    .line 444
    :cond_0
    monitor-exit v5

    .line 493
    :goto_0
    return-void

    .line 444
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 449
    :cond_1
    const/4 v3, 0x0

    .line 450
    .local v3, "pos":I
    iget-wide v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mScrollInterval:J

    const-wide/16 v6, 0x12c

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 453
    if-le v2, v8, :cond_2

    .line 455
    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mTo:I

    int-to-double v4, v4

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mAccel:F

    float-to-double v6, v6

    rsub-int/lit8 v8, v2, 0xa

    mul-int/lit8 v8, v8, 0x1e

    int-to-double v8, v8

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    div-double/2addr v6, v10

    sub-double/2addr v4, v6

    double-to-int v3, v4

    .line 489
    :goto_1
    iput v3, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPos:I

    .line 490
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mShowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 459
    :cond_2
    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mFrom:I

    int-to-double v4, v4

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mAccel:F

    float-to-double v6, v6

    mul-int/lit8 v8, v2, 0x1e

    int-to-double v8, v8

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    div-double/2addr v6, v10

    add-double/2addr v4, v6

    double-to-int v3, v4

    goto :goto_1

    .line 465
    :cond_3
    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mCnt:I

    if-lt v4, v8, :cond_5

    .line 467
    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mTo:I

    iget v5, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mFrom:I

    if-le v4, v5, :cond_4

    .line 469
    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mTo:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    aget-wide v5, v5, v2

    iget v7, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mUnitWidth:I

    int-to-double v7, v7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    sub-int v3, v4, v5

    goto :goto_1

    .line 472
    :cond_4
    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mTo:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mPosArray:[D

    aget-wide v5, v5, v2

    iget v7, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mUnitWidth:I

    int-to-double v7, v7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    add-int v3, v4, v5

    goto :goto_1

    .line 478
    :cond_5
    const/16 v4, 0xa

    if-le v2, v4, :cond_6

    .line 480
    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mTo:I

    int-to-double v4, v4

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mAccel:F

    float-to-double v6, v6

    rsub-int/lit8 v8, v2, 0x14

    mul-int/lit8 v8, v8, 0x1e

    int-to-double v8, v8

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    div-double/2addr v6, v10

    sub-double/2addr v4, v6

    double-to-int v3, v4

    goto :goto_1

    .line 484
    :cond_6
    iget v4, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mFrom:I

    int-to-double v4, v4

    iget v6, p0, Lcom/infraware/polarisoffice5/common/WheelScroll$ScrollTimerTask;->mAccel:F

    float-to-double v6, v6

    mul-int/lit8 v8, v2, 0x1e

    int-to-double v8, v8

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    div-double/2addr v6, v10

    add-double/2addr v4, v6

    double-to-int v3, v4

    goto :goto_1
.end method
