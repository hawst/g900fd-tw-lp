.class public Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;
.super Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;
.source "TwoButtonDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$1;,
        Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;,
        Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetWlanDialg;,
        Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$SheetDeleteDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$SheetMergeCellDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$abstractDialog;,
        Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;
    }
.end annotation


# instance fields
.field private mTwoDialog:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;Lcom/infraware/polarisoffice5/common/dialog/DialogManager;)V
    .locals 3
    .param p1, "a_context"    # Landroid/content/Context;
    .param p2, "a_type"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;
    .param p3, "a_Instance"    # Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;-><init>(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;Lcom/infraware/polarisoffice5/common/dialog/DialogManager;)V

    .line 39
    const v0, 0x7f07006b

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mPositiveId:I

    .line 40
    const v0, 0x7f070062

    iput v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mNegativeId:I

    .line 42
    sget-object v0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$1;->$SwitchMap$com$infraware$polarisoffice5$common$dialog$DialogManager$DialogType:[I

    invoke-virtual {p2}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 49
    :goto_0
    return-void

    .line 44
    :pswitch_0
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$SheetMergeCellDialog;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$SheetMergeCellDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mTwoDialog:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;

    goto :goto_0

    .line 45
    :pswitch_1
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$SheetDeleteDialog;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$SheetDeleteDialog;-><init>(Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mTwoDialog:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;

    goto :goto_0

    .line 46
    :pswitch_2
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetWlanDialg;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetWlanDialg;-><init>(Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mTwoDialog:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;

    goto :goto_0

    .line 47
    :pswitch_3
    new-instance v0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$AlarmNetMobileDialg;-><init>(Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mTwoDialog:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected varargs inflateDialog([Ljava/lang/Object;)V
    .locals 1
    .param p1, "a_args"    # [Ljava/lang/Object;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mTwoDialog:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;

    invoke-interface {v0, p1}, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;->inflateDialog([Ljava/lang/Object;)V

    .line 55
    invoke-super {p0, p1}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->inflateDialog([Ljava/lang/Object;)V

    .line 56
    return-void
.end method

.method public onLocaleChanged()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog;->mTwoDialog:Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/common/dialog/TwoButtonDialog$TwoDialogable;->onLocaleChanged()V

    .line 61
    invoke-super {p0}, Lcom/infraware/polarisoffice5/common/dialog/BaseDialog;->onLocaleChanged()V

    .line 62
    return-void
.end method
