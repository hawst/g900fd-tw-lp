.class Lcom/infraware/polarisoffice5/common/QuickScrollView$3;
.super Ljava/lang/Object;
.source "QuickScrollView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/QuickScrollView;->showScroll()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/QuickScrollView;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$3;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$3;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mUserMode:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$200(Lcom/infraware/polarisoffice5/common/QuickScrollView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$3;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$600(Lcom/infraware/polarisoffice5/common/QuickScrollView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 286
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$3;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    # getter for: Lcom/infraware/polarisoffice5/common/QuickScrollView;->mHideHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->access$600(Lcom/infraware/polarisoffice5/common/QuickScrollView;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x6a4

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 290
    :goto_0
    return-void

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/QuickScrollView$3;->this$0:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->hide()V

    goto :goto_0
.end method
