.class Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;
.super Ljava/lang/Object;
.source "MarkerOptionsView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 304
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 351
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 307
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070125

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 310
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07016d

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 315
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07006c

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 318
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070074

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 321
    :pswitch_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070072

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 324
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070071

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 327
    :pswitch_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07006d

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 330
    :pswitch_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070075

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 333
    :pswitch_9
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070076

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 336
    :pswitch_a
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070070

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 339
    :pswitch_b
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070077

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 342
    :pswitch_c
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07006f

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 345
    :pswitch_d
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f07006e

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 348
    :pswitch_e
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    iget-object v1, v0, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v2, 0x7f070073

    invoke-static {v1, v0, p1, v2}, Lcom/infraware/common/util/Utils;->showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto/16 :goto_0

    .line 304
    :pswitch_data_0
    .packed-switch 0x7f0b014b
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
