.class Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;
.super Landroid/widget/BaseAdapter;
.source "LayoutDlg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/LayoutDlg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LayoutDlgAdapter"
.end annotation


# instance fields
.field private mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mListItem:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_oInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/common/LayoutDlg;Landroid/content/Context;ILjava/util/List;Ljava/util/ArrayList;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "listType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mListItem:Ljava/util/List;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mData:Ljava/util/ArrayList;

    .line 98
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    .line 100
    iput-object p4, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mListItem:Ljava/util/List;

    .line 101
    iput-object p5, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mData:Ljava/util/ArrayList;

    .line 102
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mListItem:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mListItem:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 112
    const/4 v0, 0x0

    .line 114
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mListItem:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 124
    const/4 v0, 0x0

    .line 126
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 119
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 134
    if-nez p2, :cond_1

    .line 135
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030022

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 137
    new-instance v1, Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->this$0:Lcom/infraware/polarisoffice5/common/LayoutDlg;

    invoke-direct {v1, v2, v4}, Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;-><init>(Lcom/infraware/polarisoffice5/common/LayoutDlg;Lcom/infraware/polarisoffice5/common/LayoutDlg$1;)V

    .line 138
    .local v1, "holder":Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;
    const v2, 0x7f0b0103

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;->txt:Landroid/widget/TextView;

    .line 139
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 145
    :goto_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->getItemEnabled(I)Z

    move-result v0

    .line 146
    .local v0, "enabled":Z
    iget-object v2, v1, Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;->txt:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->getItemEnabled(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 147
    iget-object v3, v1, Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;->txt:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/common/LayoutDlg$LayoutDlgAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Landroid/view/View;->setSelected(Z)V

    .line 149
    if-nez v0, :cond_0

    .line 152
    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Landroid/view/View;->setClickable(Z)V

    .line 154
    :cond_0
    return-object p2

    .line 143
    .end local v0    # "enabled":Z
    .end local v1    # "holder":Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;

    .restart local v1    # "holder":Lcom/infraware/polarisoffice5/common/LayoutDlg$Holder;
    goto :goto_0
.end method
