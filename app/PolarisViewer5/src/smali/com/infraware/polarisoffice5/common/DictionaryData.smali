.class public Lcom/infraware/polarisoffice5/common/DictionaryData;
.super Ljava/lang/Object;
.source "DictionaryData.java"


# static fields
.field public static final ACTIVITY_CLASS:Ljava/lang/String; = "com.sec.android.EDictionary.DioDictActivity"

.field public static final DICTYPE_ENGKOR:I = 0x0

.field public static final DICTYPE_KORENG:I = 0x1

.field public static final DICTYPE_MANTOU_KORTOSIMP:I = 0xd00

.field public static final DICTYPE_MANTOU_SIMPTOKOR:I = 0xd01

.field public static final DICTYPE_OXFORD_FLTRP_CHN_ENG:I = 0x517

.field public static final DICTYPE_OXFORD_FLTRP_ENG_CHN:I = 0x516

.field public static final INTENT_EXTRA_DICTIONARY_NAME:Ljava/lang/String; = "dic_type"

.field public static final INTENT_EXTRA_WORD_NAME:Ljava/lang/String; = "search_word"

.field public static final INTENT_EXTRA_WORD_SUID:Ljava/lang/String; = "search_suid"

.field public static final INTENT_MODE_NAME:Ljava/lang/String; = "display_mode"

.field public static final INTENT_MODE_VALUE:Ljava/lang/String; = "display_mode_view"

.field public static final PACKAGE:Ljava/lang/String; = "com.sec.android.EDictionary"

.field public static final SERVICE_CLASS:Ljava/lang/String; = "com.sec.android.EDictionary.service.DioDictService"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
