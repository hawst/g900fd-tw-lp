.class Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;
.super Ljava/lang/Object;
.source "PenDrawOptionsView.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 527
    const-string/jumbo v0, "HYOHYUN"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onProgressChanged ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1900(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 530
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$2000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 537
    :goto_0
    return-void

    .line 533
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setThickness(I)V

    .line 534
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPenSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    # setter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mThickness:I
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$2002(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I

    .line 535
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->initPenSize()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$2100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/4 v1, 0x1

    # setter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1902(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;Z)Z

    .line 542
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v1, "onStartTrackingTouch"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 547
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$6;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mTracking:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1902(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;Z)Z

    .line 548
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v1, "onStopTrackingTouch"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    return-void
.end method
