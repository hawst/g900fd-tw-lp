.class public Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;
.super Landroid/app/ProgressDialog;
.source "DlgFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/DlgFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PageLayoutProgressDlg"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/DlgFactory;Landroid/app/Activity;)V
    .locals 2
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;->this$0:Lcom/infraware/polarisoffice5/common/DlgFactory;

    .line 278
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v0

    invoke-direct {p0, p2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 280
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;->setTitle(Ljava/lang/CharSequence;)V

    .line 281
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;->setMessage(Ljava/lang/CharSequence;)V

    .line 284
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;->setCancelable(Z)V

    .line 285
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;->setCanceledOnTouchOutside(Z)V

    .line 286
    new-instance v0, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg$1;

    invoke-direct {v0, p0, p1}, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg$1;-><init>(Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;Lcom/infraware/polarisoffice5/common/DlgFactory;)V

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/DlgFactory$PageLayoutProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 293
    return-void
.end method
