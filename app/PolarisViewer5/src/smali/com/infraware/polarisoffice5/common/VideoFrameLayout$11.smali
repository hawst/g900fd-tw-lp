.class Lcom/infraware/polarisoffice5/common/VideoFrameLayout$11;
.super Ljava/lang/Object;
.source "VideoFrameLayout.java"

# interfaces
.implements Lcom/infraware/office/evengine/EvInterface$OnZoomChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoViewListener()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/VideoFrameLayout;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$11;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onZoomChanged(I)V
    .locals 2
    .param p1, "a_nZoom"    # I

    .prologue
    .line 380
    const-string/jumbo v0, "VideoFrameLayout"

    const-string/jumbo v1, "onZoomChanged"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/VideoFrameLayout$11;->this$0:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onStopVideo()V

    .line 383
    :cond_0
    return-void
.end method
