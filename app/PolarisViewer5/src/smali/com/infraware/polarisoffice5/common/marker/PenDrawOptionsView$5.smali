.class Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;
.super Ljava/lang/Object;
.source "PenDrawOptionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/16 v3, 0xff

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 472
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 473
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 475
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 476
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 477
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 479
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # setter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1402(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I

    .line 480
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setPenStyle(I)V

    .line 481
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    .line 482
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1500(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I

    move-result v1

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 485
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 487
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 488
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 489
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 491
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # setter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I
    invoke-static {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1402(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I

    .line 492
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setPenStyle(I)V

    .line 493
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    const/16 v1, 0x7f

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    .line 494
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlighterThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1600(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I

    move-result v1

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    goto :goto_0

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 497
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 499
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 500
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 501
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 503
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # setter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I
    invoke-static {v0, v4}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1402(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I

    .line 504
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    .line 505
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setPenStyle(I)V

    .line 506
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mInkThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1700(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I

    move-result v1

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    goto/16 :goto_0

    .line 508
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1300(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 511
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPencleType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1000(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 512
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mHighlightType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 513
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mbrushType:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1200(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 515
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # setter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mType:I
    invoke-static {v0, v5}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1402(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)I

    .line 516
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setTransparency(I)V

    .line 517
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mPreview:Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$100(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/infraware/polarisoffice5/common/marker/MarkerPreviewImageView;->setPenStyle(I)V

    .line 518
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView$5;->this$0:Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;

    # getter for: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->mRulerThickness:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$1800(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;)I

    move-result v1

    # invokes: Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->setThickness(I)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;->access$400(Lcom/infraware/polarisoffice5/common/marker/PenDrawOptionsView;I)V

    goto/16 :goto_0
.end method
