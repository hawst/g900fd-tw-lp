.class Lcom/infraware/polarisoffice5/common/ExportToPdf$1;
.super Ljava/lang/Object;
.source "ExportToPdf.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/common/ExportToPdf;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/ExportToPdf;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$1;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 246
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$1;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    const-class v2, Lcom/infraware/polarisoffice5/dialog/FileSelectActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 247
    .local v0, "folderSelect":Landroid/content/Intent;
    const-string/jumbo v1, "key_current_file"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$1;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    # getter for: Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_strOriginFile:Ljava/lang/String;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->access$200(Lcom/infraware/polarisoffice5/common/ExportToPdf;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    const-string/jumbo v1, "key_current_folder"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$1;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    # getter for: Lcom/infraware/polarisoffice5/common/ExportToPdf;->m_oItem:Lcom/infraware/filemanager/file/FileListItem;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->access$300(Lcom/infraware/polarisoffice5/common/ExportToPdf;)Lcom/infraware/filemanager/file/FileListItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/filemanager/file/FileListItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    const-string/jumbo v1, "key_interanl_mode"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 250
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/ExportToPdf$1;->this$0:Lcom/infraware/polarisoffice5/common/ExportToPdf;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/infraware/polarisoffice5/common/ExportToPdf;->startActivityForResult(Landroid/content/Intent;I)V

    .line 251
    return-void
.end method
