.class public Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "CommonNumberInputActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;


# static fields
.field private static final FLOAT:I = 0x1

.field private static final INTEGER:I


# instance fields
.field private button0:Landroid/widget/Button;

.field private button1:Landroid/widget/Button;

.field private button2:Landroid/widget/Button;

.field private button3:Landroid/widget/Button;

.field private button4:Landroid/widget/Button;

.field private button5:Landroid/widget/Button;

.field private button6:Landroid/widget/Button;

.field private button7:Landroid/widget/Button;

.field private button8:Landroid/widget/Button;

.field private button9:Landroid/widget/Button;

.field private button_del:Landroid/widget/Button;

.field private button_dot:Landroid/widget/Button;

.field canMinus:Z

.field isDel:Z

.field isDot:Z

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mDescription:I

.field private mEditNumber:Landroid/widget/EditText;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mFieldType:I

.field private mMaxLen:I

.field private mMaxNumber:I

.field private mMinLen:I

.field private mMinNumber:I

.field private mStartValue:I

.field private mStrValue:Ljava/lang/String;

.field private mTitle:I

.field private mTvDescription:Landroid/widget/TextView;

.field private mValue:I

.field private mWheelType:I

.field private m_ToastMsg:Landroid/widget/Toast;

.field savePos:I

.field private textwatcher:Landroid/text/TextWatcher;

.field wasSecondnumberZero:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 56
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->isDot:Z

    .line 57
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->isDel:Z

    .line 58
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->wasSecondnumberZero:Z

    .line 59
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->canMinus:Z

    .line 60
    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->savePos:I

    .line 67
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 68
    iput-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mStrValue:Ljava/lang/String;

    .line 261
    new-instance v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$1;-><init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    .line 402
    new-instance v0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity$2;-><init>(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->textwatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxLen:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinLen:I

    return v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->warning()V

    return-void
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    return v0
.end method

.method private initLayer()V
    .locals 10

    .prologue
    const v9, 0x7f02004c

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 166
    const v0, 0x7f0b005a

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    .line 170
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 173
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->setMinMaxLength()V

    .line 175
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mDescription:I

    if-eqz v0, :cond_0

    .line 176
    const v0, 0x7f0b00f3

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTvDescription:Landroid/widget/TextView;

    .line 177
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTvDescription:Landroid/widget/TextView;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mDescription:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 178
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTvDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    :cond_0
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    const-string/jumbo v1, "%d"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mValue:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->textwatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 193
    const v0, 0x7f0b0101

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button0:Landroid/widget/Button;

    .line 194
    const v0, 0x7f0b00f4

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button1:Landroid/widget/Button;

    .line 195
    const v0, 0x7f0b00f5

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button2:Landroid/widget/Button;

    .line 196
    const v0, 0x7f0b00f6

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button3:Landroid/widget/Button;

    .line 197
    const v0, 0x7f0b00f8

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button4:Landroid/widget/Button;

    .line 198
    const v0, 0x7f0b00f9

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button5:Landroid/widget/Button;

    .line 199
    const v0, 0x7f0b00fa

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button6:Landroid/widget/Button;

    .line 200
    const v0, 0x7f0b00fc

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button7:Landroid/widget/Button;

    .line 201
    const v0, 0x7f0b00fd

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button8:Landroid/widget/Button;

    .line 202
    const v0, 0x7f0b00fe

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button9:Landroid/widget/Button;

    .line 203
    const v0, 0x7f0b0102

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_del:Landroid/widget/Button;

    .line 204
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-ne v0, v5, :cond_3

    .line 206
    const v0, 0x7f0b0100

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    .line 207
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050122

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v8, v7, v1, v2}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 211
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    if-gez v0, :cond_4

    .line 213
    const v0, 0x7f0b0100

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    .line 214
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    const v1, 0x41d2a3d7    # 26.33f

    invoke-virtual {v0, v5, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 216
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050122

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v8, v7, v1, v2}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 217
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 220
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button0:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button1:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button2:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button3:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button4:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button5:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button6:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button7:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button8:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button9:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button0:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_del:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-eq v0, v5, :cond_5

    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    if-gez v0, :cond_6

    .line 234
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->button_dot:Landroid/widget/Button;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    .line 238
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0, v6}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 239
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->canMinus:Z

    .line 246
    :goto_1
    return-void

    .line 185
    :cond_7
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-ne v0, v5, :cond_8

    .line 186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    const-string/jumbo v1, "%.1f"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mValue:I

    int-to-float v3, v3

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 187
    :cond_8
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/16 v1, 0x23

    if-ne v0, v1, :cond_2

    .line 188
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mStrValue:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 189
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mStrValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 243
    :cond_9
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 244
    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->canMinus:Z

    goto :goto_1
.end method

.method private onToastMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "strMsg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 536
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->m_ToastMsg:Landroid/widget/Toast;

    .line 540
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->m_ToastMsg:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 541
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 542
    return-void

    .line 539
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setMinMaxLength()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 249
    new-array v0, v4, [Landroid/text/InputFilter;

    .line 250
    .local v0, "filters":[Landroid/text/InputFilter;
    const-string/jumbo v1, "%d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxLen:I

    .line 251
    const-string/jumbo v1, "%d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinLen:I

    .line 253
    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-ne v1, v4, :cond_1

    .line 254
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxLen:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxLen:I

    .line 257
    :cond_1
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxLen:I

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v5

    .line 258
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 259
    return-void
.end method

.method private warning()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x41200000    # 10.0f

    .line 546
    const/4 v0, 0x0

    .line 547
    .local v0, "formattedString":Ljava/lang/String;
    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/16 v2, 0x23

    if-ne v1, v2, :cond_2

    .line 548
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07024a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 549
    if-eqz v0, :cond_1

    .line 550
    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->onToastMessage(Ljava/lang/String;)V

    .line 557
    :cond_1
    :goto_0
    return-void

    .line 553
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07024b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 554
    if-eqz v0, :cond_1

    .line 555
    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->onToastMessage(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public actionTitleBarButtonClick()V
    .locals 9

    .prologue
    const/16 v8, 0x23

    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x0

    .line 130
    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-eq v3, v7, :cond_0

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-eq v3, v6, :cond_0

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-ne v3, v8, :cond_2

    .line 131
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mValue:I

    .line 135
    :goto_0
    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mValue:I

    iget v4, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mStartValue:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-eq v3, v8, :cond_3

    .line 137
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->finish()V

    .line 162
    :cond_1
    :goto_1
    return-void

    .line 133
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mValue:I

    goto :goto_0

    .line 141
    :cond_3
    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-eq v3, v7, :cond_4

    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-ne v3, v6, :cond_7

    .line 142
    :cond_4
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEditNumber:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 143
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 144
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 146
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 148
    .local v0, "nValue":I
    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-ne v3, v6, :cond_6

    .line 149
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v5, v0, v5}, Lcom/infraware/office/evengine/EvInterface;->ISheetSetRowColSize(III)V

    .line 161
    .end local v0    # "nValue":I
    .end local v2    # "value":Ljava/lang/String;
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->finish()V

    goto :goto_1

    .line 151
    .restart local v0    # "nValue":I
    .restart local v2    # "value":Ljava/lang/String;
    :cond_6
    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-ne v3, v7, :cond_5

    .line 152
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0, v5}, Lcom/infraware/office/evengine/EvInterface;->ISheetSetRowColSize(III)V

    goto :goto_2

    .line 154
    .end local v0    # "nValue":I
    .end local v2    # "value":Ljava/lang/String;
    :cond_7
    iget v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-ne v3, v8, :cond_5

    .line 155
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 156
    .local v1, "result":Landroid/content/Intent;
    const-string/jumbo v3, "key_wheel_type"

    iget v4, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mWheelType:I

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 157
    const-string/jumbo v3, "key_data_type"

    iget v4, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 158
    const-string/jumbo v3, "key_value"

    iget v4, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mValue:I

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 159
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v1}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f0b001d

    const/16 v5, 0x5a0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const v0, 0x7f03001c

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->setContentView(I)V

    .line 75
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 77
    new-instance v0, Lcom/infraware/office/actionbar/ActionTitleBar;

    const/16 v1, 0x47

    invoke-direct {v0, p0, v6, v4, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 79
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_wheel_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mWheelType:I

    .line 80
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_data_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    .line 81
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_max"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    .line 82
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_min"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    .line 83
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_title_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTitle:I

    .line 84
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_value"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mStartValue:I

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mValue:I

    .line 87
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    if-nez v0, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_title_text"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTitle:I

    .line 89
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_filed_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    .line 90
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_description_text"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mDescription:I

    .line 91
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 92
    const v0, 0x7f070106

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTitle:I

    .line 93
    iput v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    .line 94
    iput v5, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    .line 106
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "key_filed_get_value"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mStrValue:Ljava/lang/String;

    .line 108
    new-instance v0, Lcom/infraware/office/actionbar/ActionTitleBar;

    const/16 v1, 0x42

    invoke-direct {v0, p0, v6, v4, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 111
    :cond_1
    const-string/jumbo v0, "#####"

    const-string/jumbo v1, "CommonNumerInputActivity "

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string/jumbo v0, "#####"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "WheelType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mWheelType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string/jumbo v0, "#####"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Max: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Min: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string/jumbo v0, "#####"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTitle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Description : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mDescription:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTitle:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTitle:I

    if-nez v0, :cond_5

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(Ljava/lang/String;)V

    .line 120
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonText(I)V

    .line 122
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 124
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->initLayer()V

    .line 125
    return-void

    .line 96
    :cond_3
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 97
    const v0, 0x7f0701de

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTitle:I

    .line 98
    iput v3, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    .line 99
    iput v5, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    goto/16 :goto_0

    .line 101
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mFieldType:I

    const/16 v1, 0x23

    if-ne v0, v1, :cond_0

    .line 102
    const v0, -0x1001d

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMinNumber:I

    .line 103
    const v0, 0x1001d

    iput v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mMaxNumber:I

    goto/16 :goto_0

    .line 119
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/CommonNumberInputActivity;->mTitle:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    goto :goto_1
.end method

.method public onLocaleChange(I)V
    .locals 0
    .param p1, "nLocale"    # I

    .prologue
    .line 562
    return-void
.end method
