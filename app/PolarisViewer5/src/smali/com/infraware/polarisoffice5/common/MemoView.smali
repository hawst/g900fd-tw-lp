.class public Lcom/infraware/polarisoffice5/common/MemoView;
.super Landroid/widget/LinearLayout;
.source "MemoView.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/common/LocaleChangeListener;


# instance fields
.field private bMemoModified:Z

.field private mActiveMemoId:I

.field private mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

.field private mDeleteBtn:Landroid/widget/ImageButton;

.field private mDocType:I

.field private mFirstMemoId:I

.field private mInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mLastMemoId:I

.field private mMemoText:Landroid/widget/EditText;

.field private mNextBtn:Landroid/widget/ImageButton;

.field private mPrevBtn:Landroid/widget/ImageButton;

.field private mTitleText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mPrevBtn:Landroid/widget/ImageButton;

    .line 30
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mNextBtn:Landroid/widget/ImageButton;

    .line 31
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDeleteBtn:Landroid/widget/ImageButton;

    .line 32
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    .line 33
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mTitleText:Landroid/widget/TextView;

    .line 35
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 36
    iput v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    .line 37
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    .line 38
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .line 39
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    .line 41
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mFirstMemoId:I

    .line 42
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mLastMemoId:I

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mPrevBtn:Landroid/widget/ImageButton;

    .line 30
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mNextBtn:Landroid/widget/ImageButton;

    .line 31
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDeleteBtn:Landroid/widget/ImageButton;

    .line 32
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    .line 33
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mTitleText:Landroid/widget/TextView;

    .line 35
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 36
    iput v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    .line 37
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    .line 38
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .line 39
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    .line 41
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mFirstMemoId:I

    .line 42
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mLastMemoId:I

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mPrevBtn:Landroid/widget/ImageButton;

    .line 30
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mNextBtn:Landroid/widget/ImageButton;

    .line 31
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDeleteBtn:Landroid/widget/ImageButton;

    .line 32
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    .line 33
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mTitleText:Landroid/widget/TextView;

    .line 35
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 36
    iput v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    .line 37
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    .line 38
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .line 39
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    .line 41
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mFirstMemoId:I

    .line 42
    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mLastMemoId:I

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/common/MemoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/MemoView;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/MemoView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/common/MemoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/MemoView;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    return v0
.end method

.method static synthetic access$202(Lcom/infraware/polarisoffice5/common/MemoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/MemoView;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    return p1
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/common/MemoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/MemoView;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    return v0
.end method

.method static synthetic access$302(Lcom/infraware/polarisoffice5/common/MemoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/MemoView;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    return p1
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/common/MemoView;)Lcom/infraware/office/baseframe/EvBaseEditorActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/common/MemoView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    return-object v0
.end method


# virtual methods
.method public finallizeMemoView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 269
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mPrevBtn:Landroid/widget/ImageButton;

    .line 270
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mNextBtn:Landroid/widget/ImageButton;

    .line 271
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    .line 272
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 273
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/MemoView;->removeAllViews()V

    .line 274
    iput-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .line 275
    return-void
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hide()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 279
    iget v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    iget v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    const/16 v3, 0x26

    if-eq v2, v3, :cond_1

    .line 281
    iget v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    if-eq v2, v4, :cond_0

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 287
    .local v0, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/MemoView;->getText()Ljava/lang/String;

    move-result-object v1

    .line 288
    .local v1, "newset":Ljava/lang/String;
    iput-object v1, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strMemo:Ljava/lang/String;

    .line 289
    iget v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    iput v2, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 291
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 292
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->clear()V

    .line 295
    .end local v0    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    .end local v1    # "newset":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    .line 296
    iput v4, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    .line 298
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 299
    .restart local v0    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iput v4, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 300
    iget-object v2, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v3, 0x9

    invoke-virtual {v2, v3, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 301
    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->clear()V

    .line 305
    .end local v0    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :cond_1
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/common/MemoView;->setVisibility(I)V

    .line 307
    return-void
.end method

.method public init(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V
    .locals 7
    .param p1, "act"    # Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 62
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    .line 63
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    iget-object v3, v3, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 64
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getDocType()I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    .line 65
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03002d

    invoke-virtual {v3, v4, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 67
    .local v1, "mTestLayout":Landroid/widget/LinearLayout;
    const v3, 0x7f0b015e

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mPrevBtn:Landroid/widget/ImageButton;

    .line 68
    const v3, 0x7f0b015f

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mNextBtn:Landroid/widget/ImageButton;

    .line 69
    const v3, 0x7f0b015d

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDeleteBtn:Landroid/widget/ImageButton;

    .line 70
    const v3, 0x7f0b0161

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    .line 72
    const v3, 0x7f0b015c

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 73
    .local v2, "titleParent":Landroid/widget/RelativeLayout;
    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mTitleText:Landroid/widget/TextView;

    .line 75
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mPrevBtn:Landroid/widget/ImageButton;

    new-instance v4, Lcom/infraware/polarisoffice5/common/MemoView$1;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/common/MemoView$1;-><init>(Lcom/infraware/polarisoffice5/common/MemoView;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mNextBtn:Landroid/widget/ImageButton;

    new-instance v4, Lcom/infraware/polarisoffice5/common/MemoView$2;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/common/MemoView$2;-><init>(Lcom/infraware/polarisoffice5/common/MemoView;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDeleteBtn:Landroid/widget/ImageButton;

    new-instance v4, Lcom/infraware/polarisoffice5/common/MemoView$3;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/common/MemoView$3;-><init>(Lcom/infraware/polarisoffice5/common/MemoView;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v0, "dividerViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const v3, 0x7f0b0160

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/common/MemoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/MemoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/infraware/common/util/Utils;->setMemoLine(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 193
    iget v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 196
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mNextBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 197
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mPrevBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 200
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->IsViewerMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 202
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 203
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 204
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mTitleText:Landroid/widget/TextView;

    const v4, 0x7f07018f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 207
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    new-instance v4, Lcom/infraware/polarisoffice5/common/MemoView$4;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/common/MemoView$4;-><init>(Lcom/infraware/polarisoffice5/common/MemoView;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 221
    return-void
.end method

.method public isMemoModified()Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    return v0
.end method

.method public isShown()Z
    .locals 1

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/common/MemoView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 332
    const/4 v0, 0x1

    .line 334
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLocaleChanged()V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->IsViewerMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mTitleText:Landroid/widget/TextView;

    const v1, 0x7f07018f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 344
    :goto_0
    return-void

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mTitleText:Landroid/widget/TextView;

    const v1, 0x7f07018c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public setActiveMemoId(I)V
    .locals 6
    .param p1, "nMemoId"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 251
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 252
    .local v0, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iput p1, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    .line 253
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v4, 0x9

    invoke-virtual {v1, v4, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 255
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v4, 0x6

    invoke-virtual {v1, v4, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 256
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    iget-object v4, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->strMemo:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    .line 258
    iput p1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    .line 260
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mPrevBtn:Landroid/widget/ImageButton;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    iget v5, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mFirstMemoId:I

    if-eq v1, v5, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 261
    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mNextBtn:Landroid/widget/ImageButton;

    iget v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActiveMemoId:I

    iget v5, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mLastMemoId:I

    if-eq v1, v5, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 263
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v4}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->IsViewerMode()Z

    move-result v4

    if-nez v4, :cond_2

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 264
    return-void

    :cond_0
    move v1, v3

    .line 260
    goto :goto_0

    :cond_1
    move v1, v3

    .line 261
    goto :goto_1

    :cond_2
    move v2, v3

    .line 263
    goto :goto_2
.end method

.method public setMemoModified(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 316
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->bMemoModified:Z

    .line 317
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "memo_content"    # Ljava/lang/String;

    .prologue
    .line 311
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 312
    return-void
.end method

.method public setTextEnabled(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 224
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 225
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDeleteBtn:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 226
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mTitleText:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const v0, 0x7f07018c

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 227
    return-void

    .line 225
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 226
    :cond_1
    const v0, 0x7f07018f

    goto :goto_1
.end method

.method public show()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 231
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/common/MemoView;->setVisibility(I)V

    .line 233
    iget v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    if-eq v1, v3, :cond_0

    iget v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mDocType:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->getMemoCmdData()Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;

    move-result-object v0

    .line 236
    .local v0, "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v2, 0xd

    invoke-virtual {v1, v2, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 237
    iget v1, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mFirstMemoId:I

    .line 238
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, Lcom/infraware/office/evengine/EvInterface;->IMemoCommand(ILcom/infraware/office/evengine/EV$MEMO_CMD_DATA;)Z

    .line 239
    iget v1, v0, Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;->nMemoId:I

    iput v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mLastMemoId:I

    .line 242
    .end local v0    # "memo":Lcom/infraware/office/evengine/EV$MEMO_CMD_DATA;
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mActivity:Lcom/infraware/office/baseframe/EvBaseEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->IsViewerMode()Z

    move-result v1

    if-nez v1, :cond_2

    .line 244
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 245
    iget-object v1, p0, Lcom/infraware/polarisoffice5/common/MemoView;->mMemoText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 247
    :cond_2
    return-void
.end method
