.class Lcom/infraware/polarisoffice5/common/PasswordActivity$1;
.super Ljava/lang/Object;
.source "PasswordActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/common/PasswordActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/common/PasswordActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/common/PasswordActivity;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/PasswordActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 76
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/PasswordActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/PasswordActivity;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->access$000(Lcom/infraware/polarisoffice5/common/PasswordActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/PasswordActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 91
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/PasswordActivity;

    # getter for: Lcom/infraware/polarisoffice5/common/PasswordActivity;->mEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/PasswordActivity;->access$100(Lcom/infraware/polarisoffice5/common/PasswordActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/common/PasswordActivity$1;->this$0:Lcom/infraware/polarisoffice5/common/PasswordActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/PasswordActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 95
    :cond_0
    return-void
.end method
