.class public Lcom/infraware/polarisoffice5/OfficeWebView;
.super Lcom/infraware/common/baseactivity/BaseWebView;
.source "OfficeWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;
    }
.end annotation


# static fields
.field private static final WEB_PROGRESS_END:I = 0x1

.field private static final WEB_PROGRESS_ERROR:I = 0x63

.field private static final WEB_PROGRESS_START:I


# instance fields
.field private m_layoutTitle:Landroid/widget/LinearLayout;

.field private m_nLocaleCode:I

.field private m_nOrientation:I

.field private m_nTitleId:I

.field private m_oHandler:Landroid/os/Handler;

.field private m_oToastMsg:Landroid/widget/Toast;

.field private m_pvLoading:Landroid/widget/ProgressBar;

.field private m_tvTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseWebView;-><init>()V

    .line 64
    new-instance v0, Lcom/infraware/polarisoffice5/OfficeWebView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/OfficeWebView$1;-><init>(Lcom/infraware/polarisoffice5/OfficeWebView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_oHandler:Landroid/os/Handler;

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_oToastMsg:Landroid/widget/Toast;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeWebView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_oHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeWebView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_pvLoading:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeWebView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_oToastMsg:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$202(Lcom/infraware/polarisoffice5/OfficeWebView;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeWebView;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_oToastMsg:Landroid/widget/Toast;

    return-object p1
.end method

.method private onOrientationChanged()V
    .locals 4

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nOrientation:I

    .line 172
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v2

    if-nez v2, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget v3, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nOrientation:I

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 177
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 178
    .local v1, "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v2, 0x41500000    # 13.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 179
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 181
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 182
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 184
    const/high16 v2, 0x42400000    # 48.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 185
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    :goto_0
    return-void

    .line 189
    :cond_1
    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 190
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 155
    iget v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nOrientation:I

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v3, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->onOrientationChanged()V

    .line 158
    :cond_0
    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v2}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 159
    .local v0, "nlocale":I
    iget v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nLocaleCode:I

    if-eq v2, v0, :cond_1

    .line 161
    const v2, 0x7f0b0034

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/OfficeWebView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 162
    .local v1, "tvTitle":Landroid/widget/TextView;
    iget v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nTitleId:I

    if-lez v2, :cond_1

    if-eqz v1, :cond_1

    .line 163
    iget v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nTitleId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 166
    .end local v1    # "tvTitle":Landroid/widget/TextView;
    :cond_1
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseWebView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 167
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v5, 0x7f0b0034

    .line 111
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseWebView;->onCreate(Landroid/os/Bundle;)V

    .line 113
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 118
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_oWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;-><init>(Lcom/infraware/polarisoffice5/OfficeWebView;Lcom/infraware/polarisoffice5/OfficeWebView$1;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 119
    const v2, 0x7f0b0035

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/OfficeWebView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_pvLoading:Landroid/widget/ProgressBar;

    .line 121
    const v2, 0x7f0b0031

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/OfficeWebView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_layoutTitle:Landroid/widget/LinearLayout;

    .line 122
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeWebView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_tvTitle:Landroid/widget/TextView;

    .line 124
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "key_web_title"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nTitleId:I

    .line 125
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeWebView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 126
    .local v1, "tvTitle":Landroid/widget/TextView;
    iget v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nTitleId:I

    if-lez v2, :cond_1

    if-eqz v1, :cond_1

    .line 127
    iget v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nTitleId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 129
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "key_web_url"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "strUrl":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 131
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_oWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 135
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView;->m_nLocaleCode:I

    .line 136
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->onOrientationChanged()V

    .line 137
    return-void

    .line 133
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->finish()V

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 142
    packed-switch p1, :pswitch_data_0

    .line 149
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseWebView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 145
    :pswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeWebView;->finish()V

    .line 146
    const/4 v0, 0x1

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
