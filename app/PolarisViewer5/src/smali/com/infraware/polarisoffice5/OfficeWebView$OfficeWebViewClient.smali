.class Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "OfficeWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/OfficeWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OfficeWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/OfficeWebView;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/OfficeWebView;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/OfficeWebView;Lcom/infraware/polarisoffice5/OfficeWebView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/OfficeWebView;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/OfficeWebView$1;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;-><init>(Lcom/infraware/polarisoffice5/OfficeWebView;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$000(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 48
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$000(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 41
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 42
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 54
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$000(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$000(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x63

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3, p3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 57
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeWebView$OfficeWebViewClient;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$000(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 60
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 34
    const/4 v0, 0x1

    return v0
.end method
