.class Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;
.super Ljava/lang/Object;
.source "DictionaryPanelMain.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V
    .locals 0

    .prologue
    .line 787
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v6, 0x7f07029f

    const v5, 0x7f07029e

    const v4, 0x7f07029d

    const/4 v0, 0x1

    .line 792
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 817
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 795
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-boolean v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_1

    .line 797
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$900(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 798
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-static {v1, v2, v3, v5}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 800
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-static {v1, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 804
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$900(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 805
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-static {v1, v2, v3, v5}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 807
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-static {v1, v2, v3, v4}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 811
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-boolean v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_3

    .line 812
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    invoke-static {v1, v2, v3, v6}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 814
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    invoke-static {v1, v2, v3, v6}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 792
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0177
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
