.class Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;
.super Ljava/lang/Object;
.source "DictionaryPanelMain.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V
    .locals 0

    .prologue
    .line 1108
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 1112
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # invokes: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->getRootViewRectOnScreen()Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$1300(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1113
    .local v0, "appRect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_OrgAppRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$1400(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/graphics/Rect;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_OrgAppRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$1400(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_OrgAppRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$1400(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 1114
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # setter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_OrgAppRect:Landroid/graphics/Rect;
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$1402(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 1115
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$000(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1116
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # invokes: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setPanelSize()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$100(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    .line 1118
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1120
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsSearchFail:Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$200(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1121
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # invokes: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showFailResult()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$300(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    .line 1129
    :cond_2
    :goto_0
    return-void

    .line 1124
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # invokes: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showDicResult()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$400(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    .line 1125
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$600(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$500(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setConfigChangeSearch(Lcom/infraware/polarisoffice5/common/DictionaryInfo;)Z

    goto :goto_0
.end method
