.class Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;
.super Ljava/lang/Object;
.source "DictionaryPanelMain.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->onConfigurationChanged(Landroid/content/res/Configuration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

.field final synthetic val$isShown:Z


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;Z)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->val$isShown:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$000(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # invokes: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setPanelSize()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$100(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    .line 463
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->val$isShown:Z

    if-eqz v0, :cond_1

    .line 465
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsSearchFail:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$200(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 466
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # invokes: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showFailResult()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$300(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    .line 473
    :cond_1
    :goto_0
    return-void

    .line 469
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # invokes: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showDicResult()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$400(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    .line 470
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$600(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$500(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setConfigChangeSearch(Lcom/infraware/polarisoffice5/common/DictionaryInfo;)Z

    goto :goto_0
.end method
