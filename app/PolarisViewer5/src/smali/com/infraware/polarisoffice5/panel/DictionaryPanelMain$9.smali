.class Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;
.super Ljava/lang/Object;
.source "DictionaryPanelMain.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showDictionaryDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V
    .locals 0

    .prologue
    .line 880
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 894
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # invokes: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->selectDictionary(I)V
    invoke-static {v0, p2}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$1000(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;I)V

    .line 896
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$500(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 897
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$600(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$500(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    move-result-object v1

    iget v1, v1, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->saveSeletedDictionary(I)V

    .line 900
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v0, :cond_1

    .line 901
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setSearchKeyword(Ljava/lang/String;)Z

    .line 905
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$1100(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 906
    return-void

    .line 903
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getMarkedString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setSearchKeyword(Ljava/lang/String;)Z

    goto :goto_0
.end method
