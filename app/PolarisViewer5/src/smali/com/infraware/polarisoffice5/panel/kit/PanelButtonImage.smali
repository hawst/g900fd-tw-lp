.class public Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;
.super Landroid/widget/LinearLayout;
.source "PanelButtonImage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/infraware/polarisoffice5/common/LocaleChangeListener;


# instance fields
.field private mBtnList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageButton;",
            ">;"
        }
    .end annotation
.end field

.field private mButtonCount:I

.field private mContext:Landroid/content/Context;

.field private mEnd:Z

.field private mHandler:Landroid/os/Handler;

.field public mLayerId:I

.field private mTitleResId:I

.field private mTobble:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    iput v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTitleResId:I

    .line 29
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mEnd:Z

    .line 38
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private initButton()V
    .locals 9

    .prologue
    const v8, 0x7f0b016c

    const v7, 0x7f0b016b

    const v6, 0x7f0b016a

    const v5, 0x7f0b0169

    const/4 v4, 0x0

    .line 192
    new-instance v2, Ljava/util/ArrayList;

    iget v3, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    .line 193
    iget v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    packed-switch v2, :pswitch_data_0

    .line 343
    :goto_0
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    iget v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    if-ge v1, v2, :cond_0

    .line 344
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 195
    .end local v1    # "index":I
    :pswitch_0
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 196
    .local v0, "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 197
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 200
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 201
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    .end local v0    # "btn":Landroid/widget/ImageButton;
    :pswitch_1
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 205
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 206
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 209
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 210
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 213
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 214
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    .end local v0    # "btn":Landroid/widget/ImageButton;
    :pswitch_2
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 218
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 219
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 222
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 223
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 226
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 227
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 230
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 231
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 234
    .end local v0    # "btn":Landroid/widget/ImageButton;
    :pswitch_3
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 235
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 236
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 239
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 240
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 243
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 244
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 247
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 248
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    const v2, 0x7f0b016d

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 251
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 252
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 255
    .end local v0    # "btn":Landroid/widget/ImageButton;
    :pswitch_4
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 256
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 257
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 260
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 261
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 264
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 265
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 268
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 269
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    const v2, 0x7f0b016d

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 272
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 273
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    const v2, 0x7f0b016e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 276
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 277
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 280
    .end local v0    # "btn":Landroid/widget/ImageButton;
    :pswitch_5
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 281
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 282
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 285
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 286
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 289
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 290
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 293
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 294
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    const v2, 0x7f0b016d

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 297
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 298
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300
    const v2, 0x7f0b016e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 301
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 302
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    const v2, 0x7f0b016f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 305
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 306
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 309
    .end local v0    # "btn":Landroid/widget/ImageButton;
    :pswitch_6
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 310
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 311
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 314
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 315
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 318
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 319
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 322
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 323
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    const v2, 0x7f0b016d

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 326
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 327
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    const v2, 0x7f0b016e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 330
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 331
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    const v2, 0x7f0b016f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 334
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 335
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    const v2, 0x7f0b0170

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageButton;
    check-cast v0, Landroid/widget/ImageButton;

    .line 338
    .restart local v0    # "btn":Landroid/widget/ImageButton;
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 339
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 346
    .end local v0    # "btn":Landroid/widget/ImageButton;
    .restart local v1    # "index":I
    :cond_0
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTobble:Z

    if-eqz v2, :cond_1

    .line 347
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 349
    :cond_1
    return-void

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private initImageBGResource(I)V
    .locals 4
    .param p1, "dataArrayNorID"    # I

    .prologue
    .line 354
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 356
    .local v2, "imgArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 357
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 358
    .local v1, "id":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 356
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 361
    .end local v1    # "id":I
    :cond_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 362
    return-void
.end method

.method private initImageResource(I)V
    .locals 4
    .param p1, "dataArrayNorID"    # I

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 367
    .local v2, "imgArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 368
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 369
    .local v1, "id":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 367
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 372
    .end local v1    # "id":I
    :cond_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 373
    return-void
.end method

.method private setButtonTitle()V
    .locals 3

    .prologue
    .line 377
    const v1, 0x7f0b0167

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 378
    .local v0, "tv":Landroid/widget/TextView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 379
    iget v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTitleResId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 380
    const v1, 0x7f0b0168

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f02017a

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 381
    return-void
.end method

.method private setEnd()V
    .locals 5

    .prologue
    const v4, 0x7f0b0172

    const/4 v3, 0x0

    .line 153
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mEnd:Z

    if-eqz v2, :cond_0

    .line 155
    const v2, 0x7f0b0173

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 156
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 166
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 160
    :cond_0
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v0, "dividerViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/infraware/common/util/Utils;->setSeparateLine(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0
.end method


# virtual methods
.method public addFun(Landroid/os/Handler;I)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "layerId"    # I

    .prologue
    .line 478
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mHandler:Landroid/os/Handler;

    .line 479
    iput p2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mLayerId:I

    .line 480
    return-void
.end method

.method public addHandler(Landroid/os/Handler;I)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "layerId"    # I

    .prologue
    .line 473
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mHandler:Landroid/os/Handler;

    .line 474
    iput p2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mLayerId:I

    .line 475
    return-void
.end method

.method public clearSelection()V
    .locals 3

    .prologue
    .line 454
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    if-ge v0, v1, :cond_0

    .line 456
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 454
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 458
    :cond_0
    return-void
.end method

.method public clearSelection(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 461
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 462
    return-void
.end method

.method public getSelection()I
    .locals 2

    .prologue
    .line 443
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    if-ge v0, v1, :cond_1

    .line 445
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 450
    .end local v0    # "index":I
    :goto_1
    return v0

    .line 443
    .restart local v0    # "index":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 450
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public initImage(IZI)V
    .locals 3
    .param p1, "buttonCount"    # I
    .param p2, "isToggle"    # Z
    .param p3, "dataArrayId"    # I

    .prologue
    .line 43
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mEnd:Z

    .line 44
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTobble:Z

    .line 45
    iput p1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    .line 47
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 48
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03002f

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 50
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initButton()V

    .line 51
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initImageResource(I)V

    .line 52
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setEnd()V

    .line 53
    return-void
.end method

.method public initImage(IZIZ)V
    .locals 3
    .param p1, "buttonCount"    # I
    .param p2, "isToggle"    # Z
    .param p3, "dataArrayId"    # I
    .param p4, "end"    # Z

    .prologue
    .line 74
    iput-boolean p4, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mEnd:Z

    .line 75
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTobble:Z

    .line 76
    iput p1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    .line 78
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 79
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03002f

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 81
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initButton()V

    .line 82
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initImageResource(I)V

    .line 83
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setEnd()V

    .line 85
    return-void
.end method

.method public initImage(IZIZZ)V
    .locals 3
    .param p1, "buttonCount"    # I
    .param p2, "isToggle"    # Z
    .param p3, "dataArrayId"    # I
    .param p4, "end"    # Z
    .param p5, "isbg"    # Z

    .prologue
    .line 57
    iput-boolean p4, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mEnd:Z

    .line 58
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTobble:Z

    .line 59
    iput p1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    .line 61
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 62
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03002f

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 64
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initButton()V

    .line 65
    if-eqz p5, :cond_0

    .line 66
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initImageBGResource(I)V

    .line 69
    :goto_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setEnd()V

    .line 70
    return-void

    .line 68
    :cond_0
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initImageResource(I)V

    goto :goto_0
.end method

.method public initImageTitle(IZII)V
    .locals 3
    .param p1, "buttonCount"    # I
    .param p2, "isToggle"    # Z
    .param p3, "dataArrayId"    # I
    .param p4, "titleResId"    # I

    .prologue
    .line 88
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mEnd:Z

    .line 89
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTobble:Z

    .line 90
    iput p1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    .line 91
    iput p4, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTitleResId:I

    .line 93
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 94
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03002f

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 96
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setButtonTitle()V

    .line 97
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initButton()V

    .line 98
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initImageResource(I)V

    .line 99
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setEnd()V

    .line 100
    return-void
.end method

.method public initImageTitle(IZIIZ)V
    .locals 3
    .param p1, "buttonCount"    # I
    .param p2, "isToggle"    # Z
    .param p3, "dataArrayId"    # I
    .param p4, "titleResId"    # I
    .param p5, "end"    # Z

    .prologue
    .line 103
    iput-boolean p5, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mEnd:Z

    .line 104
    iput-boolean p2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTobble:Z

    .line 105
    iput p1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    .line 106
    iput p4, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTitleResId:I

    .line 108
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 109
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03002f

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 111
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setButtonTitle()V

    .line 112
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initButton()V

    .line 113
    invoke-direct {p0, p3}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initImageResource(I)V

    .line 114
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setEnd()V

    .line 115
    return-void
.end method

.method public isSelected(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 436
    iget v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    if-ge p1, v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    .line 439
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 388
    const/4 v0, 0x0

    .line 389
    .local v0, "command":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 400
    :goto_0
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTobble:Z

    if-eqz v1, :cond_1

    .line 401
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    :goto_1
    return-void

    .line 390
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 391
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 392
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 393
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 394
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 395
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 396
    :pswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 397
    :pswitch_7
    const/16 v0, 0x8

    goto :goto_0

    .line 406
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->clearSelection()V

    .line 407
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 408
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->onSendMessage(I)V

    goto :goto_1

    .line 410
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 411
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 415
    :goto_2
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->onSendMessage(I)V

    goto :goto_1

    .line 413
    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0169
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onLocaleChanged()V
    .locals 2

    .prologue
    .line 530
    const v1, 0x7f0b0167

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 531
    .local v0, "tv":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 532
    iget v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTitleResId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 533
    :cond_0
    return-void
.end method

.method public onSendMessage(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x1

    .line 488
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 489
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 490
    .local v0, "msg":Landroid/os/Message;
    iget v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mLayerId:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 492
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 494
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->isSelected(I)Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 496
    iput v2, v0, Landroid/os/Message;->arg2:I

    .line 502
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 504
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void

    .line 500
    .restart local v0    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->arg2:I

    goto :goto_0
.end method

.method public setAllEnable(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 465
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    if-ge v0, v1, :cond_0

    .line 467
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 465
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 469
    :cond_0
    return-void
.end method

.method public setButtonBg()V
    .locals 2

    .prologue
    .line 509
    const v0, 0x7f0b0168

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f02017a

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 510
    return-void
.end method

.method public setButtonBg(I)V
    .locals 1
    .param p1, "redId"    # I

    .prologue
    .line 514
    const v0, 0x7f0b0168

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 515
    return-void
.end method

.method public setButtonBgNull()V
    .locals 2

    .prologue
    .line 519
    const v0, 0x7f0b0168

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 520
    return-void
.end method

.method public setButtonLeft(I)V
    .locals 7
    .param p1, "nSpace"    # I

    .prologue
    const v6, 0x7f0b016c

    const v5, 0x7f0b016d

    const v4, 0x7f0b016e

    const/4 v3, 0x4

    .line 120
    iget v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    packed-switch v2, :pswitch_data_0

    .line 142
    :goto_0
    iget v2, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    sub-int v2, p1, v2

    int-to-float v0, v2

    .line 143
    .local v0, "fWeight":F
    const v2, 0x7f0b0171

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 144
    .local v1, "view":Landroid/view/View;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 145
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    return-void

    .line 122
    .end local v0    # "fWeight":F
    .end local v1    # "view":Landroid/view/View;
    :pswitch_0
    const v2, 0x7f0b016b

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 123
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 124
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 125
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 128
    :pswitch_1
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 129
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 130
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 133
    :pswitch_2
    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 134
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 137
    :pswitch_3
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setButtongRes(II)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "resId"    # I

    .prologue
    .line 524
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 525
    return-void
.end method

.method public setSelection(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 420
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 429
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mTobble:Z

    if-eqz v0, :cond_2

    .line 424
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->clearSelection()V

    .line 427
    :cond_2
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mButtonCount:I

    if-ge p1, v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0
.end method

.method public setVisibility(II)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "visibility"    # I

    .prologue
    .line 432
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->mBtnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 433
    return-void
.end method

.method public showSepateLine(ZZ)V
    .locals 5
    .param p1, "show"    # Z
    .param p2, "endView"    # Z

    .prologue
    const v4, 0x7f0b0173

    const v1, 0x7f0b0172

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 169
    if-eqz p1, :cond_0

    .line 170
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 174
    :goto_0
    if-eqz p2, :cond_1

    .line 176
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 177
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 187
    :goto_1
    return-void

    .line 172
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 182
    :cond_1
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 183
    .restart local v0    # "view":Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
