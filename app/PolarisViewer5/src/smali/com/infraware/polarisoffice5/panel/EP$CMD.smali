.class public interface abstract Lcom/infraware/polarisoffice5/panel/EP$CMD;
.super Ljava/lang/Object;
.source "EP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/panel/EP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CMD"
.end annotation


# static fields
.field public static final CELL_FILL:I = 0x33

.field public static final CELL_PROPERTY:I = 0x32

.field public static final CHART_CHART:I = 0x50

.field public static final CHART_DATA_LABEL:I = 0x54

.field public static final CHART_FONT:I = 0x52

.field public static final CHART_FORMAT:I = 0x51

.field public static final CHART_LAYOUT:I = 0x55

.field public static final CHART_STYLE:I = 0x56

.field public static final CHART_TITLE:I = 0x53

.field public static final CMD_INIT:I = -0x1

.field public static final FONT_BG:I = 0x3

.field public static final FONT_COLOR:I = 0x1

.field public static final FONT_EFFECT:I = 0x5

.field public static final FONT_EFFECT_STYLE:I = 0x7

.field public static final FONT_FACE:I = 0x2

.field public static final FONT_POSITION:I = 0x6

.field public static final FONT_SIZE:I = 0x0

.field public static final FONT_STYLE:I = 0x4

.field public static final FONT_UNDER_LINE:I = 0x8

.field public static final GROUP_ARRANGE:I = 0x3c

.field public static final IMAGE_BRIGHT:I = 0x1e

.field public static final IMAGE_CONTRAST:I = 0x1f

.field public static final IMAGE_OPACITY:I = 0x21

.field public static final IMAGE_RESET:I = 0x20

.field public static final LINE_ARROW_END:I = 0x2c

.field public static final LINE_ARROW_START:I = 0x2b

.field public static final LINE_COLOR:I = 0x2a

.field public static final LINE_DASH:I = 0x29

.field public static final LINE_OPACITY:I = 0x2d

.field public static final LINE_STYLE:I = 0x3d

.field public static final LINE_WIDTH:I = 0x28

.field public static final MOBJECT_DALIGN:I = 0x5e

.field public static final MOBJECT_HALIGN:I = 0x5d

.field public static final MOBJECT_VALIGN:I = 0x5c

.field public static final MULTI_ARRANGE:I = 0x3e

.field public static final OBJECT_POS:I = 0x5b

.field public static final PARA_AF:I = 0xf

.field public static final PARA_ALIGN:I = 0xa

.field public static final PARA_BF:I = 0xe

.field public static final PARA_BIDI:I = 0x18

.field public static final PARA_EFFECT_1:I = 0xb

.field public static final PARA_EFFECT_2:I = 0xc

.field public static final PARA_INDENT:I = 0x17

.field public static final PARA_INDENT_FL:I = 0x14

.field public static final PARA_INDENT_LEFT:I = 0x16

.field public static final PARA_INDENT_RIGHT:I = 0x15

.field public static final PARA_SP:I = 0xd

.field public static final PARA_TEXT_FLOW:I = 0x19

.field public static final SHADOW_STYLE:I = 0x22

.field public static final SHAPE_3DFORMAT:I = 0x6e

.field public static final SHAPE_3DFORMAT_DEPTH:I = 0x70

.field public static final SHAPE_3DFORMAT_ONOFF:I = 0x6d

.field public static final SHAPE_3DROTATION:I = 0x6f

.field public static final SHAPE_CROPPING:I = 0x71

.field public static final SHAPE_FILL:I = 0x2e

.field public static final SHAPE_NEON_COLOR:I = 0x6a

.field public static final SHAPE_NEON_DEFAULT:I = 0x6c

.field public static final SHAPE_NEON_NONE:I = 0x69

.field public static final SHAPE_NEON_SIZE:I = 0x6b

.field public static final SHAPE_OPACITY:I = 0x2f

.field public static final SHAPE_REFLECT_DEFAULT:I = 0x68

.field public static final SHAPE_REFLECT_NONE:I = 0x65

.field public static final SHAPE_REFLECT_SIZE:I = 0x67

.field public static final SHAPE_REFLECT_TRANS:I = 0x66

.field public static final SHAPE_SHADOW:I = 0x64

.field public static final SHAPE_STYLE:I = 0x30

.field public static final TABLE_STYLE:I = 0x34

.field public static final TABLE_STYLE_OPTION:I = 0x35

.field public static final TEXT_WRAP:I = 0x5a
