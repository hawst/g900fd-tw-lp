.class public Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
.super Ljava/lang/Object;
.source "DictionaryPanelMain.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final DICTIONARY_PANEL_ANIMATION_TIME:I

.field isTxt:Z

.field mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mActionbarHeight:I

.field public mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field private mFullScreenHeight:I

.field private mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

.field private mHideAnimation:Landroid/view/animation/TranslateAnimation;

.field private mIsShowingDialog:Z

.field private mMode:I

.field protected mPaneNoResult:Landroid/widget/FrameLayout;

.field private mPanelLayout:Landroid/widget/FrameLayout;

.field private mPanelLayoutHeight:I

.field private mPanelSearchLayout:Landroid/widget/FrameLayout;

.field mRootViewGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mShowAnimation:Landroid/view/animation/TranslateAnimation;

.field public mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

.field mWebViewSelectionListener:Landroid/view/View$OnLongClickListener;

.field protected m_ActionBar:Landroid/widget/LinearLayout;

.field private m_Cs:[Ljava/lang/CharSequence;

.field private m_DicHandler:Landroid/os/Handler;

.field protected m_DicNoResult:Landroid/widget/TextView;

.field private m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

.field private m_DictionaryDialog:Landroid/app/AlertDialog;

.field private m_DictionaryPanelHandler:Landroid/os/Handler;

.field private m_IsFullScreenMode:Z

.field private m_IsPDF:Z

.field private m_IsSearchFail:Z

.field private m_Keyword:Ljava/lang/String;

.field private m_OrgAppRect:Landroid/graphics/Rect;

.field protected m_Size:Landroid/widget/ImageButton;

.field private m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

.field protected m_setting:Landroid/widget/ImageButton;

.field protected m_webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 3
    .param p1, "ebva"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 55
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    .line 56
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    .line 57
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPaneNoResult:Landroid/widget/FrameLayout;

    .line 61
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicNoResult:Landroid/widget/TextView;

    .line 62
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    .line 63
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    .line 64
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    .line 65
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    .line 66
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Keyword:Ljava/lang/String;

    .line 67
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_OrgAppRect:Landroid/graphics/Rect;

    .line 69
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    .line 70
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 71
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 73
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicHandler:Landroid/os/Handler;

    .line 74
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 75
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryPanelHandler:Landroid/os/Handler;

    .line 77
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    .line 78
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsPDF:Z

    .line 79
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsSearchFail:Z

    .line 80
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    .line 81
    iput v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayoutHeight:I

    .line 84
    iput v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionbarHeight:I

    .line 86
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    .line 88
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    .line 90
    const/16 v0, 0x12c

    iput v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->DICTIONARY_PANEL_ANIMATION_TIME:I

    .line 787
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 823
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$8;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$8;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mWebViewSelectionListener:Landroid/view/View$OnLongClickListener;

    .line 930
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mIsShowingDialog:Z

    .line 932
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    .line 1108
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mRootViewGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 94
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    .line 95
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 100
    const v0, 0x7f0b013a

    invoke-virtual {p1, v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    .line 101
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 103
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isPortraitMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->attachPortait()V

    .line 112
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0174

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    .line 113
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPaneNoResult:Landroid/widget/FrameLayout;

    .line 116
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017c

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicNoResult:Landroid/widget/TextView;

    .line 117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    .line 118
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mWebViewSelectionListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setLongClickable(Z)V

    .line 121
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0177

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    .line 122
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 124
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0178

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    .line 125
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0175

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    .line 129
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHandler()V

    .line 130
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHideHandler()V

    .line 132
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setContext(Landroid/content/Context;)V

    .line 133
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setHandler(Landroid/os/Handler;)V

    .line 135
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDicListNSortFromDiotek()V

    .line 137
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->readDictionaryDB()V

    .line 138
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setTxt(Z)V

    .line 140
    return-void

    .line 109
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->attachLand()V

    goto/16 :goto_0
.end method

.method public constructor <init>(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 5
    .param p1, "ebva"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 54
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 55
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    .line 56
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    .line 57
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    .line 58
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPaneNoResult:Landroid/widget/FrameLayout;

    .line 61
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicNoResult:Landroid/widget/TextView;

    .line 62
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    .line 63
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    .line 64
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    .line 65
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    .line 66
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Keyword:Ljava/lang/String;

    .line 67
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_OrgAppRect:Landroid/graphics/Rect;

    .line 69
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    .line 70
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 71
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 73
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicHandler:Landroid/os/Handler;

    .line 74
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 75
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryPanelHandler:Landroid/os/Handler;

    .line 77
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    .line 78
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsPDF:Z

    .line 79
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsSearchFail:Z

    .line 80
    iput v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    .line 81
    iput v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayoutHeight:I

    .line 84
    iput v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionbarHeight:I

    .line 86
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    .line 88
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    .line 90
    const/16 v0, 0x12c

    iput v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->DICTIONARY_PANEL_ANIMATION_TIME:I

    .line 787
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$7;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 823
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$8;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$8;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mWebViewSelectionListener:Landroid/view/View$OnLongClickListener;

    .line 930
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mIsShowingDialog:Z

    .line 932
    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    .line 1108
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$11;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mRootViewGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 229
    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    .line 230
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 232
    const v0, 0x7f0b013a

    invoke-virtual {p1, v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    .line 233
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isPortraitMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030030

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 242
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0174

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    .line 243
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPaneNoResult:Landroid/widget/FrameLayout;

    .line 244
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017c

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicNoResult:Landroid/widget/TextView;

    .line 247
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    .line 248
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mWebViewSelectionListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 249
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setLongClickable(Z)V

    .line 251
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0177

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    .line 252
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 254
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0178

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    .line 255
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 257
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0175

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    .line 259
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHandler()V

    .line 260
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHideHandler()V

    .line 261
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 262
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setWebView(Landroid/webkit/WebView;)V

    .line 263
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setContext(Landroid/content/Context;)V

    .line 264
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setHandler(Landroid/os/Handler;)V

    .line 266
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDicListNSortFromDiotek()V

    .line 267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->readDictionaryDB()V

    .line 268
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setTxt(Z)V

    .line 272
    return-void

    .line 240
    :cond_0
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030031

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0
.end method

.method private MatrixTime(I)V
    .locals 8
    .param p1, "delayTime"    # I

    .prologue
    .line 371
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 372
    .local v2, "saveTime":J
    const-wide/16 v0, 0x0

    .line 374
    .local v0, "currTime":J
    :goto_0
    sub-long v4, v0, v2

    int-to-long v6, p1

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 375
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0

    .line 377
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setPanelSize()V

    return-void
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->selectDictionary(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mIsShowingDialog:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->getRootViewRectOnScreen()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_OrgAppRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;
    .param p1, "x1"    # Landroid/graphics/Rect;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_OrgAppRect:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsSearchFail:Z

    return v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showFailResult()V

    return-void
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showDicResult()V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionaryInfo;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    return v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    return v0
.end method

.method private attachLand()V
    .locals 3

    .prologue
    const v2, 0x7f030031

    .line 192
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 194
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    .line 195
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 197
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 207
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0174

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    .line 208
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPaneNoResult:Landroid/widget/FrameLayout;

    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017c

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicNoResult:Landroid/widget/TextView;

    .line 212
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    .line 213
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mWebViewSelectionListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 214
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setLongClickable(Z)V

    .line 216
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0177

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    .line 217
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 219
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0178

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    .line 220
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 222
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0175

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    .line 223
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setWebView(Landroid/webkit/WebView;)V

    .line 225
    :cond_0
    return-void

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0
.end method

.method private attachPortait()V
    .locals 3

    .prologue
    const v2, 0x7f030030

    .line 156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 160
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 170
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0174

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    .line 171
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPaneNoResult:Landroid/widget/FrameLayout;

    .line 172
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017c

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicNoResult:Landroid/widget/TextView;

    .line 175
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    .line 176
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mWebViewSelectionListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 177
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setLongClickable(Z)V

    .line 179
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0177

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    .line 180
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_setting:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0178

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    .line 183
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 185
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f0b0175

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_ActionBar:Landroid/widget/LinearLayout;

    .line 186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setWebView(Landroid/webkit/WebView;)V

    .line 188
    :cond_0
    return-void

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0
.end method

.method private getIndicatorHeight()I
    .locals 3

    .prologue
    .line 1034
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1036
    .local v0, "rect":Landroid/graphics/Rect;
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v2, :cond_0

    .line 1037
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1041
    .local v1, "window":Landroid/view/Window;
    :goto_0
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1042
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2

    .line 1039
    .end local v1    # "window":Landroid/view/Window;
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .restart local v1    # "window":Landroid/view/Window;
    goto :goto_0
.end method

.method private getRootViewRectOnScreen()Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1090
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1092
    .local v1, "ret":Landroid/graphics/Rect;
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v3, :cond_0

    .line 1093
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const v4, 0x7f0b0247

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1097
    .local v2, "rootView":Landroid/view/View;
    :goto_0
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 1098
    .local v0, "location":[I
    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1100
    aget v3, v0, v5

    iput v3, v1, Landroid/graphics/Rect;->left:I

    .line 1101
    aget v3, v0, v5

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Rect;->right:I

    .line 1102
    aget v3, v0, v6

    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 1103
    aget v3, v0, v6

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 1104
    return-object v1

    .line 1095
    .end local v0    # "location":[I
    .end local v2    # "rootView":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v4, 0x7f0b010e

    invoke-virtual {v3, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .restart local v2    # "rootView":Landroid/view/View;
    goto :goto_0
.end method

.method private getScreenHeight()I
    .locals 3

    .prologue
    .line 1048
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v1, :cond_0

    .line 1049
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1052
    .local v0, "display":Landroid/view/Display;
    :goto_0
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    return v1

    .line 1051
    .end local v0    # "display":Landroid/view/Display;
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .restart local v0    # "display":Landroid/view/Display;
    goto :goto_0
.end method

.method private isMultiWindowActivated()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1078
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_1

    .line 1079
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v2}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isMultiWindowSupported(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1, v2}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1086
    :cond_0
    :goto_0
    return v0

    .line 1082
    :cond_1
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v1, :cond_2

    .line 1083
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1, v2}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isMultiWindowSupported(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1, v2}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1086
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPortraitMode(Landroid/app/Activity;)Z
    .locals 3
    .param p1, "act"    # Landroid/app/Activity;

    .prologue
    .line 656
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 657
    .local v1, "Width":I
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 659
    .local v0, "Height":I
    if-le v1, v0, :cond_0

    .line 660
    const/4 v2, 0x0

    .line 662
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private selectDictionary(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 936
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    if-nez v0, :cond_0

    .line 937
    new-instance v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/common/DictionaryInfo;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    .line 939
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getUserSelectDictionary(I)Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    .line 940
    return-void
.end method

.method private setDictionaryHandler()V
    .locals 1

    .prologue
    .line 535
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$4;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicHandler:Landroid/os/Handler;

    .line 563
    return-void
.end method

.method private setDictionaryHideHandler()V
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryPanelHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 588
    :goto_0
    return-void

    .line 573
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$5;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryPanelHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method private setLayoutGone()V
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 390
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearFocus()V

    .line 391
    return-void
.end method

.method private setLayoutVisible()V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 383
    return-void
.end method

.method private setPanelSize()V
    .locals 8

    .prologue
    const v7, 0x7f0202a4

    const/16 v3, 0xc

    const v6, 0x4362ab85    # 226.67f

    const/high16 v5, 0x43180000    # 152.0f

    const/4 v4, -0x1

    .line 975
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->getRootViewRectOnScreen()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionbarHeight:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    .line 976
    iget v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 978
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    if-eqz v1, :cond_1

    .line 980
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_0

    .line 981
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 984
    :goto_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->attachPortait()V

    .line 985
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    const v2, 0x7f0202a7

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 986
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1030
    :goto_1
    return-void

    .line 983
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v2, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 990
    :cond_1
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_2

    .line 991
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 998
    :goto_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->attachPortait()V

    .line 999
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1000
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 994
    :cond_2
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v1

    invoke-direct {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 995
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 996
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 1005
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    if-eqz v1, :cond_5

    .line 1007
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_4

    .line 1008
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1011
    :goto_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->attachLand()V

    .line 1012
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    const v2, 0x7f0202a7

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1013
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 1010
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v2, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 1017
    :cond_5
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_6

    .line 1018
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1025
    :goto_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->attachLand()V

    .line 1026
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1027
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 1021
    :cond_6
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v1

    invoke-direct {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1022
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1023
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_4
.end method

.method private showDicResult()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 620
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsSearchFail:Z

    .line 621
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPaneNoResult:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 622
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 623
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 624
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showPanel()V

    .line 628
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getKeyword()Landroid/text/Spanned;

    move-result-object v0

    .line 652
    .local v0, "keyword":Landroid/text/Spanned;
    return-void
.end method

.method private showDictionaryDialog()V
    .locals 9

    .prologue
    const v8, 0x7f0702e2

    const/4 v7, 0x0

    .line 835
    const/4 v3, 0x0

    .line 837
    .local v3, "index":I
    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v5, :cond_2

    .line 838
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v6

    invoke-direct {v0, v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 842
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    :goto_0
    const v5, 0x7f07029f

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 846
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    if-nez v5, :cond_0

    .line 847
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getSortedAllList()[Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    .line 849
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->readDictionaryDB()V

    .line 850
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDicInfo()Ljava/util/ArrayList;

    move-result-object v1

    .line 852
    .local v1, "dicInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/DictionaryInfo;>;"
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v5, v5, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-nez v5, :cond_3

    .line 853
    const/4 v3, 0x0

    .line 878
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    array-length v5, v5

    if-lez v5, :cond_6

    .line 880
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    new-instance v6, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$9;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    invoke-virtual {v0, v5, v3, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 908
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    .line 909
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 910
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$10;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$10;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 918
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mIsShowingDialog:Z

    .line 919
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 928
    :goto_2
    return-void

    .line 840
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "dicInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/DictionaryInfo;>;"
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v6

    invoke-direct {v0, v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    goto :goto_0

    .line 855
    .restart local v1    # "dicInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/DictionaryInfo;>;"
    :cond_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    if-eqz v5, :cond_4

    .line 856
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v3, v5, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->index:I

    goto :goto_1

    .line 860
    :cond_4
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDicList()Ljava/util/ArrayList;

    move-result-object v4

    .line 862
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/DictionaryInfo;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 864
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v6, v5, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v5, v5, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-ne v6, v5, :cond_5

    .line 866
    move v3, v2

    .line 867
    goto :goto_1

    .line 862
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 923
    .end local v2    # "i":I
    .end local v4    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/DictionaryInfo;>;"
    :cond_6
    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v5, :cond_7

    .line 924
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v5}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 926
    :cond_7
    iget-object v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method private showFailResult()V
    .locals 2

    .prologue
    .line 598
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsSearchFail:Z

    .line 599
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPaneNoResult:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 600
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_webView:Landroid/webkit/WebView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 602
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 603
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showPanel()V

    .line 612
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicNoResult:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getMeaning()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 616
    return-void
.end method


# virtual methods
.method public dipToPx(F)I
    .locals 2
    .param p1, "value"    # F

    .prologue
    const/4 v1, 0x1

    .line 666
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 669
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getDictionaryHideHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 592
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHideHandler()V

    .line 593
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryPanelHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public goTxtSearch()V
    .locals 2

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHandler()V

    .line 277
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHideHandler()V

    .line 278
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 279
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setContext(Landroid/content/Context;)V

    .line 280
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setHandler(Landroid/os/Handler;)V

    .line 282
    return-void
.end method

.method public hidePanel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 395
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mRootViewGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 396
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 436
    :goto_0
    return-void

    .line 399
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setPanelDefaultSize()V

    .line 401
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    if-nez v0, :cond_1

    .line 403
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayoutHeight:I

    .line 404
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayoutHeight:I

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    .line 405
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 407
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    new-instance v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$2;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 425
    :cond_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setLayoutGone()V

    .line 427
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 429
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v0, :cond_2

    .line 431
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 432
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getSheetBar()Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 433
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getSheetBar()Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->show(Z)V

    .line 435
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearFocus()V

    goto :goto_0
.end method

.method public isFullMode()Z
    .locals 1

    .prologue
    .line 1073
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    return v0
.end method

.method public isShown()Z
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 440
    const/4 v0, 0x1

    .line 441
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x1

    const v9, 0x4362ab85    # 226.67f

    const/high16 v8, 0x43180000    # 152.0f

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 675
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 677
    :pswitch_0
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    if-eqz v3, :cond_4

    .line 679
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    .line 680
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    const v4, 0x7f0202a4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 681
    iget v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 683
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v3, :cond_1

    .line 684
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v5

    invoke-direct {v4, v6, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 692
    :goto_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v5

    invoke-direct {v4, v6, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 687
    :cond_1
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v0, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 688
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 689
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 696
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v3, :cond_3

    .line 697
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v9}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v5

    invoke-direct {v4, v6, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 705
    :goto_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v9}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v5

    invoke-direct {v4, v6, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 700
    :cond_3
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v9}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v0, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 701
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 702
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 716
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iput-boolean v10, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    .line 718
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    const v4, 0x7f0202a7

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 719
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->getRootViewRectOnScreen()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionbarHeight:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    .line 720
    iget v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 722
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v3, :cond_5

    .line 723
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v4, v6, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 726
    :goto_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    iget v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v4, v6, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 725
    :cond_5
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v4, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 731
    :cond_6
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v3, :cond_8

    .line 732
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v2

    .line 733
    .local v2, "top":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    const v4, 0x7f0b010e

    invoke-virtual {v3, v4}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 734
    .local v1, "root":Landroid/widget/FrameLayout;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 735
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    iget v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v4, v6, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 736
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 737
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    iget v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionbarHeight:I

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 738
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    if-nez v3, :cond_7

    .line 740
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    iget v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionbarHeight:I

    sub-int v4, v2, v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    invoke-direct {v3, v7, v7, v4, v7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 741
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 742
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v10}, Landroid/view/animation/TranslateAnimation;->setFillBefore(Z)V

    .line 743
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    new-instance v4, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 763
    :cond_7
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 765
    .end local v1    # "root":Landroid/widget/FrameLayout;
    .end local v2    # "top":I
    :cond_8
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v4, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 766
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    iget v5, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    invoke-direct {v4, v6, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 768
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    if-nez v3, :cond_9

    .line 770
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    iget v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I

    int-to-float v4, v4

    invoke-direct {v3, v7, v7, v4, v7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 771
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 772
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v10}, Landroid/view/animation/TranslateAnimation;->setFillBefore(Z)V

    .line 774
    :cond_9
    iget-object v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullShowAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 781
    :pswitch_1
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mIsShowingDialog:Z

    if-nez v3, :cond_0

    .line 782
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showDictionaryDialog()V

    goto/16 :goto_0

    .line 675
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0177
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 446
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isShown()Z

    move-result v0

    .line 448
    .local v0, "isShown":Z
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    .line 449
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViewsInLayout()V

    .line 451
    :cond_0
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    .line 453
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isMultiWindowActivated()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 455
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;

    invoke-direct {v2, p0, v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$3;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 492
    :cond_1
    :goto_0
    return-void

    .line 478
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_3

    .line 479
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setPanelSize()V

    .line 481
    :cond_3
    if-eqz v0, :cond_1

    .line 483
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsSearchFail:Z

    if-eqz v1, :cond_4

    .line 484
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showFailResult()V

    goto :goto_0

    .line 487
    :cond_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->showDicResult()V

    .line 488
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setConfigChangeSearch(Lcom/infraware/polarisoffice5/common/DictionaryInfo;)Z

    goto :goto_0
.end method

.method public onLocaleChange()V
    .locals 2

    .prologue
    .line 498
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 499
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Cs:[Ljava/lang/CharSequence;

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 508
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->stopDictionaryService()V

    .line 509
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v0, :cond_2

    .line 511
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 512
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setContext(Landroid/content/Context;)V

    .line 513
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setTxt(Z)V

    .line 521
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setHandler(Landroid/os/Handler;)V

    .line 523
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDicListNSortFromDiotek()V

    .line 524
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->readDictionaryDB()V

    .line 525
    return-void

    .line 517
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 518
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setContext(Landroid/content/Context;)V

    .line 519
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setTxt(Z)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 532
    :cond_0
    return-void
.end method

.method public setActionbarHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 1057
    iput p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActionbarHeight:I

    .line 1058
    return-void
.end method

.method public setPDF(Z)V
    .locals 0
    .param p1, "isPdf"    # Z

    .prologue
    .line 1062
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsPDF:Z

    .line 1063
    return-void
.end method

.method public setPanelDefaultSize()V
    .locals 7

    .prologue
    const/16 v3, 0xc

    const v6, 0x4362ab85    # 226.67f

    const/high16 v5, 0x43180000    # 152.0f

    const/4 v4, -0x1

    .line 944
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsFullScreenMode:Z

    .line 945
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_Size:Landroid/widget/ImageButton;

    const v2, 0x7f0202a4

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 947
    iget v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 949
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_0

    .line 950
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 957
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 971
    :goto_1
    return-void

    .line 953
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v1

    invoke-direct {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 954
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 955
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 961
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_2

    .line 962
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 969
    :goto_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v3

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 965
    :cond_2
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->dipToPx(F)I

    move-result v1

    invoke-direct {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 966
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 967
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public setSearchKeyword(Ljava/lang/String;)Z
    .locals 3
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 286
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 287
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHandler()V

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DictionaryPanelHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 289
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setDictionaryHideHandler()V

    .line 290
    :cond_1
    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v0, :cond_2

    .line 293
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_IsPDF:Z

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setSearch(Lcom/infraware/polarisoffice5/common/DictionaryInfo;Z)V

    .line 294
    const/4 v0, 0x1

    .line 302
    :goto_0
    return v0

    .line 297
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v0, :cond_3

    .line 298
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    invoke-virtual {v0, p1, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setKeywordSearch(Ljava/lang/String;Lcom/infraware/polarisoffice5/common/DictionaryInfo;)Z

    move-result v0

    goto :goto_0

    .line 302
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_UserSelectDictionary:Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    invoke-virtual {v0, p1, v1}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setKeywordSearch(Ljava/lang/String;Lcom/infraware/polarisoffice5/common/DictionaryInfo;)Z

    move-result v0

    goto :goto_0
.end method

.method public showPanel()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x4362ab85    # 226.67f

    const/high16 v4, 0x43180000    # 152.0f

    const/4 v3, 0x0

    .line 307
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mRootViewGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 308
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    if-nez v1, :cond_0

    .line 310
    const/4 v0, 0x0

    .line 311
    .local v0, "height":I
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-eqz v1, :cond_3

    .line 313
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isPortraitMode(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 314
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v1, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    .line 326
    :goto_0
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v0

    invoke-direct {v1, v3, v3, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 327
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 328
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setFillBefore(Z)V

    .line 330
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    new-instance v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$1;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$1;-><init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 350
    .end local v0    # "height":I
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setLayoutVisible()V

    .line 351
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 353
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isTxt:Z

    if-nez v1, :cond_5

    .line 355
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/infraware/office/baseframe/EvBaseView;->onShowIme(Z)V

    .line 357
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getDocType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 358
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getSheetBar()Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 359
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getSheetBar()Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->show(Z)V

    .line 363
    :cond_1
    :goto_1
    const/16 v1, 0xc8

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->MatrixTime(I)V

    .line 367
    return-void

    .line 316
    .restart local v0    # "height":I
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v1, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    goto :goto_0

    .line 320
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->isPortraitMode(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 321
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v1, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    goto :goto_0

    .line 323
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v1, v4}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    goto :goto_0

    .line 362
    .end local v0    # "height":I
    :cond_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getEditCtrl()Lcom/infraware/polarisoffice5/text/control/EditCtrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/text/control/EditCtrl;->hideSoftKeyboard()Z

    goto :goto_1
.end method

.method public startService(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 1
    .param p1, "ebva"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 145
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 146
    return-void
.end method

.method public startService(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 1
    .param p1, "ebva"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 151
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mTxtActivity:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-static {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 152
    return-void
.end method

.method public stopDictionaryService()V
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    if-eqz v0, :cond_0

    .line 1068
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->m_DicSearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->stopDictionaryService()V

    .line 1069
    :cond_0
    return-void
.end method
