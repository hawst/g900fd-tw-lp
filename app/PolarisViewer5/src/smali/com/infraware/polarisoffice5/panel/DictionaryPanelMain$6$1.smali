.class Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;
.super Ljava/lang/Object;
.source "DictionaryPanelMain.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;->onAnimationEnd(Landroid/view/animation/Animation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;)V
    .locals 0

    .prologue
    .line 751
    iput-object p1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;->this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 754
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;->this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$700(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 755
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;->this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$000(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;->this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$800(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)I

    move-result v2

    invoke-direct {v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 756
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;->this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$700(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;->this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mFullScreenHeight:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$800(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)I

    move-result v2

    invoke-direct {v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 757
    iget-object v0, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;->this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$000(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6$1;->this$1:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain$6;->this$0:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    # getter for: Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->mPanelSearchLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->access$700(Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 758
    return-void
.end method
