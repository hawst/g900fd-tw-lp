.class public Lcom/infraware/polarisoffice5/OfficeDownload;
.super Landroid/app/Activity;
.source "OfficeDownload.java"


# instance fields
.field private m_btnClose:Landroid/widget/Button;

.field private m_cbInfoVisibility:Landroid/widget/CheckBox;

.field private m_nDownKey:I

.field private m_nLocaleCode:I

.field private m_nOrientation:I

.field private m_tvProductDownload:Landroid/widget/TextView;

.field private m_tvProductInfo1:Landroid/widget/TextView;

.field private m_tvProductInfo2:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 23
    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nOrientation:I

    .line 24
    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nLocaleCode:I

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nDownKey:I

    return-void
.end method


# virtual methods
.method public onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
    .locals 2
    .param p1, "theme"    # Landroid/content/res/Resources$Theme;
    .param p2, "resid"    # I
    .param p3, "first"    # Z

    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    .line 87
    const v0, 0x1030059

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 88
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 128
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->onOrientationChanged()V

    .line 131
    :cond_0
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 132
    .local v0, "nLocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nLocaleCode:I

    if-eq v1, v0, :cond_1

    .line 134
    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nLocaleCode:I

    .line 135
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->onLocaleChanged()V

    .line 138
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 139
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeDownload;->requestWindowFeature(I)Z

    .line 41
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nLocaleCode:I

    .line 49
    const v0, 0x7f030038

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeDownload;->setContentView(I)V

    .line 51
    const v0, 0x7f0b01af

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeDownload;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_cbInfoVisibility:Landroid/widget/CheckBox;

    .line 53
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_cbInfoVisibility:Landroid/widget/CheckBox;

    new-instance v1, Lcom/infraware/polarisoffice5/OfficeDownload$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/OfficeDownload$1;-><init>(Lcom/infraware/polarisoffice5/OfficeDownload;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v0, 0x7f0b01ac

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeDownload;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_tvProductInfo1:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f0b01ad

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeDownload;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_tvProductInfo2:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0b01ae

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeDownload;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_tvProductDownload:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0b01b0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeDownload;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_btnClose:Landroid/widget/Button;

    .line 66
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_btnClose:Landroid/widget/Button;

    new-instance v1, Lcom/infraware/polarisoffice5/OfficeDownload$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/OfficeDownload$2;-><init>(Lcom/infraware/polarisoffice5/OfficeDownload;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->onOrientationChanged()V

    .line 80
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_tvProductDownload:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->changeDownloadMarketName(Landroid/content/Context;Landroid/view/View;)V

    .line 81
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 92
    iput p1, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nDownKey:I

    .line 94
    const/16 v0, 0x6f

    if-ne p1, v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->finish()V

    .line 100
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v2, -0x1

    .line 106
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nDownKey:I

    if-eq v1, p1, :cond_0

    .line 107
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nDownKey:I

    .line 122
    :goto_0
    return v0

    .line 111
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 121
    :sswitch_0
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nDownKey:I

    .line 122
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 117
    :sswitch_1
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nDownKey:I

    goto :goto_0

    .line 111
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0x54 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLocaleChanged()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_cbInfoVisibility:Landroid/widget/CheckBox;

    const v1, 0x7f0702bc

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 144
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_tvProductInfo1:Landroid/widget/TextView;

    const v1, 0x7f0702b7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 145
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_tvProductInfo2:Landroid/widget/TextView;

    const v1, 0x7f0702b8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 146
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_tvProductDownload:Landroid/widget/TextView;

    const v1, 0x7f0702b2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 147
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_btnClose:Landroid/widget/Button;

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 149
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_tvProductDownload:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->changeDownloadMarketName(Landroid/content/Context;Landroid/view/View;)V

    .line 150
    return-void
.end method

.method public onOrientationChanged()V
    .locals 2

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nOrientation:I

    .line 155
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeDownload;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_nOrientation:I

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 159
    :cond_0
    return-void
.end method

.method public saveCheckBoxStatus()V
    .locals 3

    .prologue
    const/16 v2, 0x61

    const/4 v1, 0x1

    .line 165
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeDownload;->m_cbInfoVisibility:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 166
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v2, v1}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    .line 169
    :goto_0
    return-void

    .line 168
    :cond_0
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    invoke-virtual {v0, p0, v2, v1}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    goto :goto_0
.end method
