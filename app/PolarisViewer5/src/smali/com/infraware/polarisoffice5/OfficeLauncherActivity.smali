.class public Lcom/infraware/polarisoffice5/OfficeLauncherActivity;
.super Landroid/app/Activity;
.source "OfficeLauncherActivity.java"


# static fields
.field static final DM_EMAIL_CONTENT_URI:Ljava/lang/String; = "content://com.android.email.provider/attachment"

.field static final DM_EMAIL_FILENAME:Ljava/lang/String; = "fileName"

.field static final DM_EMAIL_URI:Ljava/lang/String; = "com.android.email.attachmentprovider"

.field static final DM_EXT_ASC:Ljava/lang/String; = ".asc"

.field static final DM_EXT_CSV:Ljava/lang/String; = ".csv"

.field static final DM_EXT_DOT:Ljava/lang/String; = ".dot"

.field static final DM_EXT_DOTX:Ljava/lang/String; = ".dotx"

.field static final DM_EXT_HWP:Ljava/lang/String; = ".hwp"

.field static final DM_EXT_PDF:Ljava/lang/String; = ".pdf"

.field static final DM_EXT_POT:Ljava/lang/String; = ".pot"

.field static final DM_EXT_POTX:Ljava/lang/String; = ".potx"

.field static final DM_EXT_PPS:Ljava/lang/String; = ".pps"

.field static final DM_EXT_PPSX:Ljava/lang/String; = ".ppsx"

.field static final DM_EXT_RTF:Ljava/lang/String; = ".rtf"

.field static final DM_EXT_SHEET:Ljava/lang/String; = ".xls"

.field static final DM_EXT_SHEET_X:Ljava/lang/String; = ".xlsx"

.field static final DM_EXT_SLIDE:Ljava/lang/String; = ".ppt"

.field static final DM_EXT_SLIDE_X:Ljava/lang/String; = ".pptx"

.field static final DM_EXT_TXT:Ljava/lang/String; = ".txt"

.field static final DM_EXT_WORD:Ljava/lang/String; = ".doc"

.field static final DM_EXT_WORD_X:Ljava/lang/String; = ".docx"

.field static final DM_EXT_XLT:Ljava/lang/String; = ".xlt"

.field static final DM_EXT_XLTX:Ljava/lang/String; = ".xltx"

.field static final DM_EXT_XML:Ljava/lang/String; = ".xml"

.field static final DM_GMAIL_ATTINFO:Ljava/lang/String; = "joinedAttachmentInfos"

.field static final DM_GMAIL_CONTENT_URI:Ljava/lang/String; = "content://gmail-ls/messages/"

.field static final DM_GMAIL_URI:Ljava/lang/String; = "gmail-ls"

.field public static final LOG_CAT:Ljava/lang/String; = "OfficeLauncherActivity"


# instance fields
.field private mAutoSaveTest:I

.field private mNewFilePath:Ljava/lang/String;

.field private mSlideshowDlg:Landroid/app/AlertDialog;

.field private mTexteditorDlg:Landroid/app/AlertDialog;

.field private mViewerDlg:Landroid/app/AlertDialog;

.field private m_PopupMsg:Ljava/lang/String;

.field private m_bBroadcastMaster:Z

.field private m_bBroadcastMode:Z

.field private m_nLocaleCode:I

.field private m_newOpenHandler:Landroid/os/Handler;

.field private m_oLaunchIntent:Landroid/content/Intent;

.field private m_strTxtOpenFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 79
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mNewFilePath:Ljava/lang/String;

    .line 81
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMode:Z

    .line 82
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMaster:Z

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mAutoSaveTest:I

    .line 87
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    .line 88
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_strTxtOpenFilePath:Ljava/lang/String;

    .line 89
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_nLocaleCode:I

    .line 90
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    .line 92
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    .line 93
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    .line 94
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    .line 791
    new-instance v0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$10;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$10;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_newOpenHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_strTxtOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_newOpenHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getExtDocType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 457
    invoke-static {p1}, Lcom/infraware/common/util/FileUtils;->getFileExt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 459
    .local v0, "extention":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "pps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 460
    const-string/jumbo v1, ".pps"

    .line 502
    :goto_0
    return-object v1

    .line 461
    :cond_0
    if-eqz v0, :cond_1

    const-string/jumbo v1, "ppsx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 462
    const-string/jumbo v1, ".ppsx"

    goto :goto_0

    .line 463
    :cond_1
    if-eqz v0, :cond_2

    const-string/jumbo v1, "pot"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 464
    const-string/jumbo v1, ".pot"

    goto :goto_0

    .line 465
    :cond_2
    if-eqz v0, :cond_3

    const-string/jumbo v1, "potx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 466
    const-string/jumbo v1, ".potx"

    goto :goto_0

    .line 467
    :cond_3
    if-eqz v0, :cond_4

    const-string/jumbo v1, "pptx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 468
    const-string/jumbo v1, ".pptx"

    goto :goto_0

    .line 469
    :cond_4
    if-eqz v0, :cond_5

    const-string/jumbo v1, "ppt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 470
    const-string/jumbo v1, ".ppt"

    goto :goto_0

    .line 471
    :cond_5
    if-eqz v0, :cond_6

    const-string/jumbo v1, "dot"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 472
    const-string/jumbo v1, ".dot"

    goto :goto_0

    .line 473
    :cond_6
    if-eqz v0, :cond_7

    const-string/jumbo v1, "dotx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 474
    const-string/jumbo v1, ".dotx"

    goto :goto_0

    .line 475
    :cond_7
    if-eqz v0, :cond_8

    const-string/jumbo v1, "docx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 476
    const-string/jumbo v1, ".docx"

    goto :goto_0

    .line 477
    :cond_8
    if-eqz v0, :cond_9

    const-string/jumbo v1, "doc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 478
    const-string/jumbo v1, ".doc"

    goto/16 :goto_0

    .line 479
    :cond_9
    if-eqz v0, :cond_a

    const-string/jumbo v1, "xlt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 480
    const-string/jumbo v1, ".xlt"

    goto/16 :goto_0

    .line 481
    :cond_a
    if-eqz v0, :cond_b

    const-string/jumbo v1, "xltx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 482
    const-string/jumbo v1, ".xltx"

    goto/16 :goto_0

    .line 483
    :cond_b
    if-eqz v0, :cond_c

    const-string/jumbo v1, "xlsx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 484
    const-string/jumbo v1, ".xlsx"

    goto/16 :goto_0

    .line 485
    :cond_c
    if-eqz v0, :cond_d

    const-string/jumbo v1, "xls"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 486
    const-string/jumbo v1, ".xls"

    goto/16 :goto_0

    .line 487
    :cond_d
    if-eqz v0, :cond_e

    const-string/jumbo v1, "pdf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 488
    const-string/jumbo v1, ".pdf"

    goto/16 :goto_0

    .line 489
    :cond_e
    if-eqz v0, :cond_f

    const-string/jumbo v1, "hwp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 490
    const-string/jumbo v1, ".hwp"

    goto/16 :goto_0

    .line 491
    :cond_f
    if-eqz v0, :cond_10

    const-string/jumbo v1, "txt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 492
    const-string/jumbo v1, ".txt"

    goto/16 :goto_0

    .line 493
    :cond_10
    if-eqz v0, :cond_11

    const-string/jumbo v1, "asc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 494
    const-string/jumbo v1, ".asc"

    goto/16 :goto_0

    .line 495
    :cond_11
    if-eqz v0, :cond_12

    const-string/jumbo v1, "csv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 496
    const-string/jumbo v1, ".csv"

    goto/16 :goto_0

    .line 497
    :cond_12
    if-eqz v0, :cond_13

    const-string/jumbo v1, "rtf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 498
    const-string/jumbo v1, ".rtf"

    goto/16 :goto_0

    .line 499
    :cond_13
    if-eqz v0, :cond_14

    const-string/jumbo v1, "xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 500
    const-string/jumbo v1, ".xml"

    goto/16 :goto_0

    .line 502
    :cond_14
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private getNativeMailFileName(Landroid/content/Intent;I)Ljava/lang/String;
    .locals 22
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "size"    # I

    .prologue
    .line 203
    const/4 v4, 0x0

    .line 204
    .local v4, "LABELS_URI":Landroid/net/Uri;
    const/4 v13, 0x0

    .line 205
    .local v13, "fileName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v21

    .line 206
    .local v21, "uri":Landroid/net/Uri;
    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 209
    .local v11, "contentPath":Ljava/lang/String;
    const/16 v3, 0xa

    invoke-virtual {v11, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v20

    .line 210
    .local v20, "subPath":Ljava/lang/String;
    const-string/jumbo v3, "/"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 212
    .local v10, "content":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 214
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const/4 v3, 0x0

    aget-object v3, v10, v3

    const-string/jumbo v5, "gmail-ls"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 216
    const-string/jumbo v3, "content://gmail-ls/messages/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 217
    .end local v4    # "LABELS_URI":Landroid/net/Uri;
    .local v9, "LABELS_URI":Landroid/net/Uri;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x2

    aget-object v5, v10, v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v5, 0x3

    aget-object v5, v10, v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 218
    .local v2, "ACCOUNT_URI":Landroid/net/Uri;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 221
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_0

    .line 223
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 224
    const-string/jumbo v3, "joinedAttachmentInfos"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 225
    .local v16, "infoColumn":I
    move/from16 v0, v16

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 226
    .local v15, "info":Ljava/lang/String;
    const-string/jumbo v3, "\n"

    const-string/jumbo v5, "|"

    invoke-virtual {v15, v3, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v17

    .line 227
    .local v17, "infoReplace":Ljava/lang/String;
    const-string/jumbo v3, "\\|"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 229
    .local v18, "infoSplit":[Ljava/lang/String;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v14, v3, :cond_0

    .line 232
    aget-object v3, v18, v14

    const/4 v5, 0x5

    aget-object v5, v10, v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 234
    add-int/lit8 v3, v14, 0x1

    aget-object v13, v18, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    .end local v14    # "i":I
    .end local v15    # "info":Ljava/lang/String;
    .end local v16    # "infoColumn":I
    .end local v17    # "infoReplace":Ljava/lang/String;
    .end local v18    # "infoSplit":[Ljava/lang/String;
    :cond_0
    if-eqz v12, :cond_1

    .line 247
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    move-object v4, v9

    .line 275
    .end local v2    # "ACCOUNT_URI":Landroid/net/Uri;
    .end local v9    # "LABELS_URI":Landroid/net/Uri;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .restart local v4    # "LABELS_URI":Landroid/net/Uri;
    :cond_2
    :goto_2
    return-object v13

    .line 229
    .end local v4    # "LABELS_URI":Landroid/net/Uri;
    .restart local v2    # "ACCOUNT_URI":Landroid/net/Uri;
    .restart local v9    # "LABELS_URI":Landroid/net/Uri;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "i":I
    .restart local v15    # "info":Ljava/lang/String;
    .restart local v16    # "infoColumn":I
    .restart local v17    # "infoReplace":Ljava/lang/String;
    .restart local v18    # "infoSplit":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 241
    .end local v14    # "i":I
    .end local v15    # "info":Ljava/lang/String;
    .end local v16    # "infoColumn":I
    .end local v17    # "infoReplace":Ljava/lang/String;
    .end local v18    # "infoSplit":[Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 246
    if-eqz v12, :cond_1

    .line 247
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 246
    :catchall_0
    move-exception v3

    if-eqz v12, :cond_4

    .line 247
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v3

    .line 250
    .end local v2    # "ACCOUNT_URI":Landroid/net/Uri;
    .end local v9    # "LABELS_URI":Landroid/net/Uri;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .restart local v4    # "LABELS_URI":Landroid/net/Uri;
    :cond_5
    const/4 v3, 0x0

    aget-object v3, v10, v3

    const-string/jumbo v5, "com.android.email.attachmentprovider"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 252
    const-string/jumbo v3, "content://com.android.email.provider/attachment"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 253
    const/4 v5, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "contentUri = \""

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, "\""

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 256
    .restart local v12    # "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_6

    .line 258
    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 260
    const-string/jumbo v3, "fileName"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 261
    .local v19, "nameColumn":I
    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v13

    .line 270
    .end local v19    # "nameColumn":I
    :cond_6
    if-eqz v12, :cond_2

    .line 271
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 265
    :catch_1
    move-exception v3

    .line 270
    if-eqz v12, :cond_2

    .line 271
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 270
    :catchall_1
    move-exception v3

    if-eqz v12, :cond_7

    .line 271
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3
.end method

.method private getOpenFilePath(Landroid/content/Intent;Landroid/os/Bundle;)Ljava/lang/String;
    .locals 24
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 281
    const/16 v20, 0x0

    .line 282
    .local v20, "path":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 285
    const-string/jumbo v2, "key_filename"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 286
    if-eqz v20, :cond_1

    move-object/from16 v13, v20

    .line 451
    :cond_0
    :goto_0
    return-object v13

    .line 290
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 293
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 294
    :cond_2
    const/4 v13, 0x0

    goto :goto_0

    .line 296
    :cond_3
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, "file"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 299
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v13

    .line 300
    .local v13, "filePath":Ljava/lang/String;
    const/16 v2, 0x2f

    invoke-virtual {v13, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v13, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 301
    .local v12, "fileName":Ljava/lang/String;
    const/16 v2, 0x2e

    invoke-virtual {v12, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v17

    .line 303
    .local v17, "nIndex":I
    add-int/lit8 v2, v17, 0x1

    invoke-virtual {v12, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/FileUtils;->isSupported(Ljava/lang/String;)Z

    move-result v15

    .line 305
    .local v15, "isSupportedFile":Z
    if-ltz v17, :cond_4

    if-nez v15, :cond_0

    .line 307
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v16

    .line 309
    .local v16, "mimeType":Ljava/lang/String;
    if-eqz v16, :cond_5

    const-string/jumbo v2, "text/plain"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 312
    :cond_5
    const/4 v13, 0x0

    goto :goto_0

    .line 318
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v13    # "filePath":Ljava/lang/String;
    .end local v15    # "isSupportedFile":Z
    .end local v16    # "mimeType":Ljava/lang/String;
    .end local v17    # "nIndex":I
    :cond_6
    const/4 v14, 0x0

    .line 319
    .local v14, "iStream":Ljava/io/InputStream;
    const/16 v19, 0x0

    .line 323
    .local v19, "oStream":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v14

    .line 328
    if-nez v14, :cond_9

    .line 329
    const/4 v13, 0x0

    .line 441
    if-eqz v14, :cond_7

    .line 442
    :try_start_1
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 444
    :cond_7
    if-eqz v19, :cond_0

    .line 445
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 446
    :catch_0
    move-exception v11

    .line 447
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 324
    .end local v11    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 325
    .local v11, "e":Ljava/lang/SecurityException;
    const/4 v13, 0x0

    .line 441
    if-eqz v14, :cond_8

    .line 442
    :try_start_2
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 444
    :cond_8
    if-eqz v19, :cond_0

    .line 445
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 446
    :catch_2
    move-exception v11

    .line 447
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 332
    .end local v11    # "e":Ljava/io/IOException;
    :cond_9
    const/16 v21, 0x0

    .line 333
    .local v21, "strExt":Ljava/lang/String;
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v22

    .line 334
    .local v22, "strMimeType":Ljava/lang/String;
    if-nez v22, :cond_a

    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/infraware/common/util/FileUtils;->getRealPathFromURI(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getExtDocType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 336
    const-string/jumbo v22, ""

    .line 339
    :cond_a
    const-string/jumbo v2, "application/msword"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_b

    const-string/jumbo v2, "application/vnd.ms-word"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_13

    .line 340
    :cond_b
    const-string/jumbo v21, ".doc"

    .line 376
    :cond_c
    :goto_1
    const/4 v12, 0x0

    .line 377
    .restart local v12    # "fileName":Ljava/lang/String;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_EMAIL_DATABASE()Z

    move-result v2

    if-eqz v2, :cond_27

    .line 379
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getNativeMailFileName(Landroid/content/Intent;I)Ljava/lang/String;

    move-result-object v12

    .line 380
    if-nez v12, :cond_d

    .line 381
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "download-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 387
    :cond_d
    :goto_2
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, "content"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 389
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v5, "_data"

    aput-object v5, v4, v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 390
    .local v4, "proj":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 393
    .local v10, "cur":Landroid/database/Cursor;
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 394
    if-eqz v10, :cond_28

    .line 395
    const-string/jumbo v2, "_data"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 396
    .local v9, "colum_index":I
    const/4 v2, -0x1

    if-eq v9, v2, :cond_e

    .line 397
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 398
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 400
    :cond_e
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 402
    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v23

    .line 403
    .local v23, "tempName":Ljava/lang/String;
    const-string/jumbo v2, "content://"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 404
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "download-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 405
    :cond_f
    invoke-static {v12}, Lcom/infraware/common/util/FileUtils;->isDirectory(Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v2

    if-eqz v2, :cond_28

    .line 413
    if-eqz v10, :cond_10

    :try_start_5
    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_10

    .line 415
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 441
    :cond_10
    if-eqz v14, :cond_11

    .line 442
    :try_start_6
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 444
    :cond_11
    if-eqz v19, :cond_12

    .line 445
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :cond_12
    :goto_3
    move-object v13, v12

    .line 448
    goto/16 :goto_0

    .line 341
    .end local v4    # "proj":[Ljava/lang/String;
    .end local v9    # "colum_index":I
    .end local v10    # "cur":Landroid/database/Cursor;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v23    # "tempName":Ljava/lang/String;
    :cond_13
    :try_start_7
    const-string/jumbo v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_14

    .line 342
    const-string/jumbo v21, ".docx"

    goto/16 :goto_1

    .line 343
    :cond_14
    const-string/jumbo v2, "application/vnd.ms-excel"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_15

    .line 344
    const-string/jumbo v21, ".xls"

    goto/16 :goto_1

    .line 345
    :cond_15
    const-string/jumbo v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_16

    .line 346
    const-string/jumbo v21, ".xlsx"

    goto/16 :goto_1

    .line 347
    :cond_16
    const-string/jumbo v2, "application/vnd.ms-powerpoint"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_17

    .line 348
    const-string/jumbo v21, ".ppt"

    goto/16 :goto_1

    .line 349
    :cond_17
    const-string/jumbo v2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_18

    .line 350
    const-string/jumbo v21, ".pptx"

    goto/16 :goto_1

    .line 351
    :cond_18
    const-string/jumbo v2, "application/pdf"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_19

    .line 352
    const-string/jumbo v21, ".pdf"

    goto/16 :goto_1

    .line 353
    :cond_19
    const-string/jumbo v2, "application/hwp"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1a

    const-string/jumbo v2, "application/x-hwp"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1a

    const-string/jumbo v2, "application/haansofthwp"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1b

    .line 354
    :cond_1a
    const-string/jumbo v21, ".hwp"

    goto/16 :goto_1

    .line 355
    :cond_1b
    const-string/jumbo v2, "text/plain"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1c

    .line 356
    const-string/jumbo v21, ".txt"

    goto/16 :goto_1

    .line 357
    :cond_1c
    const-string/jumbo v2, "text/plain"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1d

    .line 358
    const-string/jumbo v21, ".asc"

    goto/16 :goto_1

    .line 359
    :cond_1d
    const-string/jumbo v2, "text/comma-separated-values"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1e

    const-string/jumbo v2, "text/csv"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1f

    .line 360
    :cond_1e
    const-string/jumbo v21, ".csv"

    goto/16 :goto_1

    .line 361
    :cond_1f
    const-string/jumbo v2, "text/rtf"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_20

    const-string/jumbo v2, "application/rtf"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_21

    .line 362
    :cond_20
    const-string/jumbo v21, ".rtf"

    goto/16 :goto_1

    .line 363
    :cond_21
    const-string/jumbo v2, "application/vnd.ms-powerpoint"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_22

    .line 364
    const-string/jumbo v21, ".pps"

    goto/16 :goto_1

    .line 365
    :cond_22
    const-string/jumbo v2, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_23

    .line 366
    const-string/jumbo v21, ".ppsx"

    goto/16 :goto_1

    .line 367
    :cond_23
    const-string/jumbo v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_24

    .line 368
    const-string/jumbo v21, ".dotx"

    goto/16 :goto_1

    .line 369
    :cond_24
    const-string/jumbo v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_25

    .line 370
    const-string/jumbo v21, ".xltx"

    goto/16 :goto_1

    .line 371
    :cond_25
    const-string/jumbo v2, "application/vnd.openxmlformats-officedocument.presentationml.template"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_26

    .line 372
    const-string/jumbo v21, ".potx"

    goto/16 :goto_1

    .line 373
    :cond_26
    const-string/jumbo v2, "xml"

    invoke-static {v2}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string/jumbo v2, "text/xml"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_c

    .line 374
    const-string/jumbo v21, ".xml"

    goto/16 :goto_1

    .line 384
    .restart local v12    # "fileName":Ljava/lang/String;
    :cond_27
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "download-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v12

    goto/16 :goto_2

    .line 446
    .restart local v4    # "proj":[Ljava/lang/String;
    .restart local v9    # "colum_index":I
    .restart local v10    # "cur":Landroid/database/Cursor;
    .restart local v23    # "tempName":Ljava/lang/String;
    :catch_3
    move-exception v11

    .line 447
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 413
    .end local v9    # "colum_index":I
    .end local v11    # "e":Ljava/io/IOException;
    .end local v23    # "tempName":Ljava/lang/String;
    :cond_28
    if-eqz v10, :cond_29

    :try_start_8
    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_29

    .line 415
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 421
    .end local v4    # "proj":[Ljava/lang/String;
    .end local v10    # "cur":Landroid/database/Cursor;
    :cond_29
    :goto_4
    invoke-static {v12}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 423
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v2}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v19

    .line 424
    const/16 v2, 0x400

    new-array v8, v2, [B

    .line 427
    .local v8, "buf":[B
    :goto_5
    invoke-virtual {v14, v8}, Ljava/io/InputStream;->read([B)I

    move-result v18

    .line 428
    .local v18, "numread":I
    if-gtz v18, :cond_31

    .line 435
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v13

    .line 441
    if-eqz v14, :cond_2a

    .line 442
    :try_start_9
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 444
    :cond_2a
    if-eqz v19, :cond_0

    .line 445
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_0

    .line 446
    :catch_4
    move-exception v11

    .line 447
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 408
    .end local v8    # "buf":[B
    .end local v11    # "e":Ljava/io/IOException;
    .end local v18    # "numread":I
    .restart local v4    # "proj":[Ljava/lang/String;
    .restart local v10    # "cur":Landroid/database/Cursor;
    :catch_5
    move-exception v11

    .line 410
    .local v11, "e":Ljava/lang/Exception;
    if-nez v12, :cond_2b

    .line 411
    :try_start_a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "download-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v12

    .line 413
    :cond_2b
    if-eqz v10, :cond_29

    :try_start_b
    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_29

    .line 415
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_4

    .line 437
    .end local v4    # "proj":[Ljava/lang/String;
    .end local v10    # "cur":Landroid/database/Cursor;
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v21    # "strExt":Ljava/lang/String;
    .end local v22    # "strMimeType":Ljava/lang/String;
    :catch_6
    move-exception v11

    .line 438
    .local v11, "e":Ljava/io/IOException;
    :try_start_c
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 441
    if-eqz v14, :cond_2c

    .line 442
    :try_start_d
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 444
    :cond_2c
    if-eqz v19, :cond_2d

    .line 445
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    .line 451
    :cond_2d
    :goto_6
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 413
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v4    # "proj":[Ljava/lang/String;
    .restart local v10    # "cur":Landroid/database/Cursor;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v21    # "strExt":Ljava/lang/String;
    .restart local v22    # "strMimeType":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_2e

    :try_start_e
    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_2e

    .line 415
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_2e
    throw v2
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 440
    .end local v4    # "proj":[Ljava/lang/String;
    .end local v10    # "cur":Landroid/database/Cursor;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v21    # "strExt":Ljava/lang/String;
    .end local v22    # "strMimeType":Ljava/lang/String;
    :catchall_1
    move-exception v2

    .line 441
    if-eqz v14, :cond_2f

    .line 442
    :try_start_f
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 444
    :cond_2f
    if-eqz v19, :cond_30

    .line 445
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    .line 448
    :cond_30
    :goto_7
    throw v2

    .line 431
    .restart local v8    # "buf":[B
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v18    # "numread":I
    .restart local v21    # "strExt":Ljava/lang/String;
    .restart local v22    # "strMimeType":Ljava/lang/String;
    :cond_31
    const/4 v2, 0x0

    :try_start_10
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v8, v2, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_5

    .line 446
    .end local v8    # "buf":[B
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v18    # "numread":I
    .end local v21    # "strExt":Ljava/lang/String;
    .end local v22    # "strMimeType":Ljava/lang/String;
    .restart local v11    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v11

    .line 447
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 446
    .end local v11    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v11

    .line 447
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7
.end method

.method private onLocaleChanged(I)V
    .locals 9
    .param p1, "nLocale"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, -0x2

    .line 809
    iput p1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_nLocaleCode:I

    .line 811
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 814
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    const v1, 0x7f0700a2

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 815
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 816
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 821
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 822
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    const v1, 0x7f0700a2

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 823
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 824
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 827
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 829
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 830
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    const v1, 0x7f0700a2

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 831
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 832
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 834
    :cond_2
    return-void
.end method

.method private onOpenSplash()Z
    .locals 4

    .prologue
    .line 150
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v1

    const/16 v2, 0x61

    const/4 v3, 0x1

    invoke-virtual {v1, p0, v2, v3}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v0

    .line 151
    .local v0, "isViewSplash":Z
    return v0
.end method

.method private onOpenViewer()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 155
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 156
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 157
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 159
    const-string/jumbo v6, "key_new_file"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mNewFilePath:Ljava/lang/String;

    .line 161
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$HOME;->USE_BROADCAST()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 162
    const-string/jumbo v6, "key_broadcast_mode"

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMode:Z

    .line 164
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMode:Z

    if-eqz v6, :cond_0

    .line 165
    const-string/jumbo v6, "key_broadcast_master"

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMaster:Z

    .line 170
    :cond_0
    const-string/jumbo v6, "key_auto_save_test"

    invoke-virtual {v2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mAutoSaveTest:I

    .line 171
    const-string/jumbo v6, "INTCMD"

    invoke-virtual {v2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 173
    .local v0, "OpenType":I
    const/4 v6, 0x1

    if-ne v0, v6, :cond_1

    .line 175
    const-string/jumbo v6, "key_new_file"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 176
    .local v3, "newFilePath":Ljava/lang/String;
    invoke-virtual {p0, v3, v0, v1}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->processOpen(Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 198
    .end local v3    # "newFilePath":Ljava/lang/String;
    :goto_0
    const-string/jumbo v6, "OfficeLauncherActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "POLARIS Office Viewer 5 version "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f070322

    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :goto_1
    return-void

    .line 178
    :cond_1
    const/4 v6, 0x2

    if-eq v0, v6, :cond_2

    const/4 v6, 0x3

    if-ne v0, v6, :cond_3

    .line 181
    :cond_2
    const-string/jumbo v6, "key_template_file"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 182
    .local v5, "tempFilePath":Ljava/lang/String;
    invoke-virtual {p0, v5, v0, v1}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->processOpen(Ljava/lang/String;ILandroid/os/Bundle;)V

    goto :goto_0

    .line 187
    .end local v5    # "tempFilePath":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v2, v1}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getOpenFilePath(Landroid/content/Intent;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .line 188
    .local v4, "openFilePath":Ljava/lang/String;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {v4}, Lcom/infraware/common/util/FileUtils;->isDirectory(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 189
    invoke-virtual {p0, v4, v0, v1}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->processOpen(Ljava/lang/String;ILandroid/os/Bundle;)V

    goto :goto_0

    .line 192
    :cond_4
    const v6, 0x7f070132

    invoke-static {p0, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 193
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->finish()V

    goto :goto_1
.end method


# virtual methods
.method public checkMultipleLaunch(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 7
    .param p1, "a_oViewer"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    const/4 v6, 0x0

    .line 657
    move-object v0, p1

    .line 658
    .local v0, "oViewer":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    const/4 v1, 0x0

    .line 659
    .local v1, "szContents":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getOpenFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    .line 660
    invoke-virtual {p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v4, v3, v6

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 661
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v2, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f07005f

    new-instance v4, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$3;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$3;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070063

    new-instance v4, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$2;

    invoke-direct {v4, p0, v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$2;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$1;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    .line 690
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 691
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mViewerDlg:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 692
    return-void
.end method

.method public checkMultipleLaunch(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 7
    .param p1, "a_oslideshow"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    const/4 v6, 0x0

    .line 743
    move-object v0, p1

    .line 744
    .local v0, "oSlideshow":Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    const/4 v1, 0x0

    .line 745
    .local v1, "szContents":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getOpenFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    .line 746
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v4, v3, v6

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 747
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v2, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f07005f

    new-instance v4, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$9;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$9;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070063

    new-instance v4, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$8;

    invoke-direct {v4, p0, v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$8;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$7;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$7;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    .line 774
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 775
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mSlideshowDlg:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 776
    return-void
.end method

.method public checkMultipleLaunch(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 7
    .param p1, "a_oEditor"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    const/4 v6, 0x0

    .line 699
    move-object v0, p1

    .line 700
    .local v0, "oEditor":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    const/4 v1, 0x0

    .line 701
    .local v1, "szContents":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getOpenFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    .line 702
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v4, v3, v6

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_PopupMsg:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 703
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v2, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f07005f

    new-instance v4, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$6;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$6;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070063

    new-instance v4, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$5;

    invoke-direct {v4, p0, v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$5;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$4;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$4;-><init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    .line 734
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 735
    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mTexteditorDlg:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 736
    return-void
.end method

.method public closeLauncher()V
    .locals 0

    .prologue
    .line 784
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->finish()V

    .line 785
    return-void
.end method

.method public closeLauncherWithIntent()V
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 780
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->finish()V

    .line 781
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 129
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 131
    packed-switch p1, :pswitch_data_0

    .line 137
    :goto_0
    return-void

    .line 134
    :pswitch_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->onOpenViewer()V

    goto :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1003
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 142
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 143
    .local v0, "nLocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_nLocaleCode:I

    if-eq v1, v0, :cond_0

    .line 144
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->onLocaleChanged(I)V

    .line 146
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 147
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_LOW_STORAGE_CHECK()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 104
    invoke-static {}, Lcom/infraware/common/util/FileUtils;->getAvailableInternalStoragePath()Ljava/lang/String;

    move-result-object v1

    .line 105
    .local v1, "internalPath":Ljava/lang/String;
    invoke-static {}, Lcom/infraware/common/util/FileUtils;->getAvailableExternalStoragePath()Ljava/lang/String;

    move-result-object v0

    .line 107
    .local v0, "externalPath":Ljava/lang/String;
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 108
    invoke-static {p0}, Lcom/infraware/common/util/FileUtils;->showUnavailableStorageDialog(Landroid/content/Context;)V

    .line 125
    .end local v0    # "externalPath":Ljava/lang/String;
    .end local v1    # "internalPath":Ljava/lang/String;
    :goto_0
    return-void

    .line 113
    :cond_0
    const v3, 0x7f030008

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->setContentView(I)V

    .line 115
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_nLocaleCode:I

    .line 117
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->onOpenSplash()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 119
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/infraware/polarisoffice5/OfficeDownload;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 120
    .local v2, "product":Landroid/content/Intent;
    const/16 v3, 0x1003

    invoke-virtual {p0, v2, v3}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 124
    .end local v2    # "product":Landroid/content/Intent;
    :cond_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->onOpenViewer()V

    goto :goto_0
.end method

.method protected processEnd()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 647
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->finish()V

    .line 648
    invoke-virtual {p0, v0, v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->overridePendingTransition(II)V

    .line 649
    return-void
.end method

.method protected processOpen(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 15
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "OpenType"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 506
    if-eqz p1, :cond_1a

    .line 507
    const/4 v11, 0x0

    .line 510
    .local v11, "strExt":Ljava/lang/String;
    const/16 v12, 0x2f

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 511
    .local v1, "fileName":Ljava/lang/String;
    const/16 v12, 0x2e

    invoke-virtual {v1, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 513
    .local v5, "nIndex":I
    add-int/lit8 v12, v5, 0x1

    invoke-virtual {v1, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/infraware/common/util/FileUtils;->isSupported(Ljava/lang/String;)Z

    move-result v4

    .line 515
    .local v4, "isSupportedFile":Z
    if-ltz v5, :cond_0

    if-nez v4, :cond_3

    .line 517
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 518
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_2

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "text/plain"

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_2

    .line 520
    const-string/jumbo v11, ".txt"

    .line 533
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    const-string/jumbo v12, ".txt"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    const-string/jumbo v12, ".asc"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 534
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->addOpenPath(Ljava/lang/String;)Z

    move-result v3

    .line 535
    .local v3, "isOpened":Z
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_strTxtOpenFilePath:Ljava/lang/String;

    .line 536
    if-eqz v3, :cond_5

    .line 537
    const v12, 0x7f07029c

    const/4 v13, 0x0

    invoke-static {p0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 538
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->processEnd()V

    .line 644
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v3    # "isOpened":Z
    .end local v4    # "isSupportedFile":Z
    .end local v5    # "nIndex":I
    .end local v11    # "strExt":Ljava/lang/String;
    :goto_1
    return-void

    .line 523
    .restart local v1    # "fileName":Ljava/lang/String;
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v4    # "isSupportedFile":Z
    .restart local v5    # "nIndex":I
    .restart local v11    # "strExt":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->processEnd()V

    goto :goto_1

    .line 528
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    .line 542
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v12

    iget-object v12, v12, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-static {v12}, Lcom/infraware/office/evengine/EvInterface;->getInterface(Ljava/lang/String;)Lcom/infraware/office/evengine/EvInterface;

    .line 543
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/infraware/office/evengine/EvInterface;->CheckOpenedFileList(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    .line 544
    const v12, 0x7f07029c

    const/4 v13, 0x0

    invoke-static {p0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 545
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->processEnd()V

    goto :goto_1

    .line 551
    :cond_5
    const-string/jumbo v12, ".doc"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_6

    const-string/jumbo v12, ".docx"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_6

    const-string/jumbo v12, ".dot"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_6

    const-string/jumbo v12, ".dotx"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_6

    const-string/jumbo v12, ".rtf"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_9

    .line 552
    :cond_6
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    .line 579
    :goto_2
    if-eqz p3, :cond_7

    .line 580
    const-string/jumbo v12, "search-key"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 581
    .local v10, "searchKey":Ljava/lang/String;
    if-eqz v10, :cond_7

    .line 582
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "search-key"

    invoke-virtual {v12, v13, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 588
    .end local v10    # "searchKey":Ljava/lang/String;
    :cond_7
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const/high16 v13, 0x10000

    invoke-virtual {v12, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 589
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const/high16 v13, 0x4000000

    invoke-virtual {v12, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 590
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "INTCMD"

    move/from16 v0, p2

    invoke-virtual {v12, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 591
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_filename"

    move-object/from16 v0, p1

    invoke-virtual {v12, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 592
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_new_file"

    iget-object v14, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mNewFilePath:Ljava/lang/String;

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 594
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$HOME;->USE_BROADCAST()Z

    move-result v12

    if-eqz v12, :cond_8

    iget-boolean v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMode:Z

    if-eqz v12, :cond_8

    .line 595
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_broadcast_mode"

    iget-boolean v14, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMode:Z

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 596
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_broadcast_master"

    iget-boolean v14, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMaster:Z

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 599
    :cond_8
    iget v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->mAutoSaveTest:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_15

    .line 600
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_auto_save_test"

    const/4 v14, 0x4

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 604
    :goto_3
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_MULTI_INSTANCE()Z

    move-result v12

    if-eqz v12, :cond_1b

    .line 607
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/MyApplication;->getCurrentViewer()Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    move-result-object v8

    .line 608
    .local v8, "oViewer":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/MyApplication;->getTextEditor()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    move-result-object v7

    .line 609
    .local v7, "oTextEditor":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    invoke-static {}, Lcom/infraware/polarisoffice5/MyApplication;->getInstance()Lcom/infraware/polarisoffice5/MyApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/MyApplication;->getSlideShow()Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    move-result-object v6

    .line 611
    .local v6, "oSlideshow":Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    if-eqz v8, :cond_16

    .line 613
    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->checkMultipleLaunch(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    goto/16 :goto_1

    .line 553
    .end local v6    # "oSlideshow":Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .end local v7    # "oTextEditor":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .end local v8    # "oViewer":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_9
    const-string/jumbo v12, ".ppt"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_a

    const-string/jumbo v12, ".pptx"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_a

    const-string/jumbo v12, ".pot"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_a

    const-string/jumbo v12, ".potx"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_c

    .line 554
    :cond_a
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$HOME;->USE_BROADCAST()Z

    move-result v12

    if-eqz v12, :cond_b

    iget-boolean v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMode:Z

    if-eqz v12, :cond_b

    .line 555
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    .line 556
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_pps_file"

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 557
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_pps_file_path"

    move-object/from16 v0, p1

    invoke-virtual {v12, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 560
    :cond_b
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    goto/16 :goto_2

    .line 562
    :cond_c
    const-string/jumbo v12, ".pps"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_d

    const-string/jumbo v12, ".ppsx"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_e

    .line 563
    :cond_d
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    .line 564
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_pps_file"

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 565
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_pps_file_path"

    move-object/from16 v0, p1

    invoke-virtual {v12, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 567
    :cond_e
    const-string/jumbo v12, ".xls"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_f

    const-string/jumbo v12, ".xlsx"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_f

    const-string/jumbo v12, ".xlt"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_f

    const-string/jumbo v12, ".xltx"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_f

    const-string/jumbo v12, ".csv"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_10

    .line 568
    :cond_f
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    goto/16 :goto_2

    .line 569
    :cond_10
    const-string/jumbo v12, ".pdf"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_11

    .line 570
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    goto/16 :goto_2

    .line 571
    :cond_11
    const-string/jumbo v12, ".txt"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_12

    const-string/jumbo v12, ".asc"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_13

    .line 572
    :cond_12
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    goto/16 :goto_2

    .line 573
    :cond_13
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HWP_EDIT_SUPPORT()Z

    move-result v12

    if-eqz v12, :cond_14

    const-string/jumbo v12, ".hwp"

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_14

    .line 574
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/word/WordEditorActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    goto/16 :goto_2

    .line 576
    :cond_14
    new-instance v12, Landroid/content/Intent;

    const-class v13, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;

    invoke-direct {v12, p0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    goto/16 :goto_2

    .line 602
    :cond_15
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    const-string/jumbo v13, "key_auto_save_test"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_3

    .line 616
    .restart local v6    # "oSlideshow":Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .restart local v7    # "oTextEditor":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .restart local v8    # "oViewer":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_16
    if-eqz v7, :cond_17

    .line 618
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->checkMultipleLaunch(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V

    goto/16 :goto_1

    .line 621
    :cond_17
    if-eqz v6, :cond_18

    .line 623
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->checkMultipleLaunch(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    goto/16 :goto_1

    .line 626
    :cond_18
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    if-eqz v12, :cond_19

    .line 627
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    invoke-virtual {p0, v12}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 628
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->processEnd()V

    .line 634
    .end local v6    # "oSlideshow":Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .end local v7    # "oTextEditor":Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .end local v8    # "oViewer":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    :cond_19
    :goto_4
    const/4 v12, 0x2

    move/from16 v0, p2

    if-eq v0, v12, :cond_1a

    const/4 v12, 0x3

    move/from16 v0, p2

    if-eq v0, v12, :cond_1a

    .line 636
    sget-object v12, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1a

    iget-boolean v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_bBroadcastMode:Z

    if-nez v12, :cond_1a

    .line 637
    invoke-static {}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;

    move-result-object v9

    .line 638
    .local v9, "recent":Lcom/infraware/filemanager/database/recent/RecentFileManager;
    move-object/from16 v0, p1

    invoke-virtual {v9, p0, v0}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->InsertFileInfoToDB(Landroid/content/Context;Ljava/lang/String;)V

    .line 643
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v4    # "isSupportedFile":Z
    .end local v5    # "nIndex":I
    .end local v9    # "recent":Lcom/infraware/filemanager/database/recent/RecentFileManager;
    .end local v11    # "strExt":Ljava/lang/String;
    :cond_1a
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->processEnd()V

    goto/16 :goto_1

    .line 632
    .restart local v1    # "fileName":Ljava/lang/String;
    .restart local v4    # "isSupportedFile":Z
    .restart local v5    # "nIndex":I
    .restart local v11    # "strExt":Ljava/lang/String;
    :cond_1b
    iget-object v12, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_oLaunchIntent:Landroid/content/Intent;

    invoke-virtual {p0, v12}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_4
.end method
