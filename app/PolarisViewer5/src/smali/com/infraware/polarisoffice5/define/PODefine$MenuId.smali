.class public Lcom/infraware/polarisoffice5/define/PODefine$MenuId;
.super Ljava/lang/Object;
.source "PODefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/define/PODefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MenuId"
.end annotation


# static fields
.field public static final CHANGE_CHARSET:I = 0x32

.field public static final HELP:I = 0x7

.field public static final MANAGE_FILE:I = 0x3

.field public static final MENU_MORE:I = 0x63

.field public static final MENU_SAVE_TEST:I = 0x64

.field public static final MOVE_ALLTYPES:I = 0x3f

.field public static final MOVE_CLOUD:I = 0x3e

.field public static final MOVE_FAVORITE:I = 0x41

.field public static final MOVE_LOCAL:I = 0x3d

.field public static final MOVE_STORAGE:I = 0x3c

.field public static final MOVE_TYPES:I = 0x40

.field public static final NEW_ACCOUNT:I = 0x1c

.field public static final NEW_DOC:I = 0x14

.field public static final NEW_DOCX:I = 0x15

.field public static final NEW_FOLDER:I = 0x1

.field public static final NEW_FORM:I = 0x0

.field public static final NEW_PPT:I = 0x18

.field public static final NEW_PPTX:I = 0x19

.field public static final NEW_TEMPLATE:I = 0x1b

.field public static final NEW_TEXT:I = 0x1a

.field public static final NEW_XLS:I = 0x16

.field public static final NEW_XLSX:I = 0x17

.field public static final NONE:I = -0x1

.field public static final REFRESH:I = 0x5

.field public static final SEARCH_DOC:I = 0x28

.field public static final SEARCH_FILE:I = 0x2

.field public static final SEARCH_PDF:I = 0x2b

.field public static final SEARCH_PPT:I = 0x2a

.field public static final SEARCH_TXT:I = 0x2c

.field public static final SEARCH_XLS:I = 0x29

.field public static final SELECT_ALL:I = 0x5b

.field public static final SETTINGS:I = 0x8

.field public static final SORTBY:I = 0x6

.field public static final SORT_DATE:I = 0x1f

.field public static final SORT_NAME:I = 0x1e

.field public static final SORT_PATH:I = 0x21

.field public static final SORT_RECENT:I = 0x23

.field public static final SORT_SIZE:I = 0x20

.field public static final SORT_TYPE:I = 0x22

.field public static final SYNC_CANCEL:I = 0xa

.field public static final SYNC_FOLDER:I = 0x9

.field public static final UNSELECT_ALL:I = 0x5c

.field public static final UPLOAD:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
