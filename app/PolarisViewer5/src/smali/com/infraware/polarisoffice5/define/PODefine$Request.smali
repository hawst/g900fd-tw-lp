.class public Lcom/infraware/polarisoffice5/define/PODefine$Request;
.super Ljava/lang/Object;
.source "PODefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/define/PODefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Request"
.end annotation


# static fields
.field public static final AUTO_SAVE_TEST:I = 0x1082

.field public static final DIALOG_ADD_ACCOUNT:I = 0x1002

.field public static final DIALOG_COPY_FOLDER:I = 0x101f

.field public static final DIALOG_CREATE_FOLDER:I = 0x1028

.field public static final DIALOG_CREATE_ROOM:I = 0x1067

.field public static final DIALOG_DOWNLOAD_FOLDER:I = 0x1021

.field public static final DIALOG_ENTER_BROADCAST:I = 0x1068

.field public static final DIALOG_INPUT_PASSWORD:I = 0x102a

.field public static final DIALOG_INSTALL_SERVICE:I = 0x1046

.field public static final DIALOG_MAIN_ALLTYPES:I = 0x100d

.field public static final DIALOG_MAIN_BROADCAST:I = 0x100f

.field public static final DIALOG_MAIN_BROWSER:I = 0x100a

.field public static final DIALOG_MAIN_CLOUDS:I = 0x100b

.field public static final DIALOG_MAIN_FAVORITE:I = 0x100e

.field public static final DIALOG_MAIN_TYPES:I = 0x100c

.field public static final DIALOG_MANAGE_FILES:I = 0x101e

.field public static final DIALOG_MENU_CHARSET:I = 0x1034

.field public static final DIALOG_MENU_NEW_FORM:I = 0x1032

.field public static final DIALOG_MENU_SORTBY:I = 0x1033

.field public static final DIALOG_MOVE_FOLDER:I = 0x1020

.field public static final DIALOG_OPEN_DOCUMENT:I = 0x1014

.field public static final DIALOG_OPEN_EXTRACT:I = 0x1016

.field public static final DIALOG_OPEN_SEARCH:I = 0x1017

.field public static final DIALOG_OPEN_SETTINGS:I = 0x1015

.field public static final DIALOG_RENAME_FILE:I = 0x1029

.field public static final DIALOG_SELECT_FILE:I = 0x1066

.field public static final DIALOG_SETTING_BLUTHOOTH:I = 0x1065

.field public static final DIALOG_SETTING_WIFI:I = 0x1064

.field public static final DIALOG_SHOW_LOGO:I = 0x1000

.field public static final DIALOG_START_FOLDER:I = 0x1050

.field public static final DIALOG_TEMPLATE_LIST:I = 0x103c

.field public static final DIALOG_UPLOAD_FILE:I = 0x1022

.field public static final DIALOG_USER_REGIST:I = 0x1001

.field public static final DIALOG_VIEW_SPLASH:I = 0x1003

.field public static final POPUP_CONFIRM_PASSWORD:I = 0x106e

.field private static final PORequest:I = 0x1000

.field public static final REQUEST_BLUETOOTH_ENABLE:I = 0x1078

.field public static final REQUEST_BROADCAST_SLIDESHOW:I = 0x107a

.field public static final REQUEST_BROADCAST_START:I = 0x1079


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
