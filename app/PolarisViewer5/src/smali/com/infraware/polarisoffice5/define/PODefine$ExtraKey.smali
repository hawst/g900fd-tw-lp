.class public Lcom/infraware/polarisoffice5/define/PODefine$ExtraKey;
.super Ljava/lang/Object;
.source "PODefine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/define/PODefine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExtraKey"
.end annotation


# static fields
.field public static final ACCOUNT_ID:Ljava/lang/String; = "key_account_id"

.field public static final ACCOUNT_NICKNAME:Ljava/lang/String; = "key_account_nickname"

.field public static final ACCOUNT_TOKEN1:Ljava/lang/String; = "key_account_token1"

.field public static final ACCOUNT_TOKEN2:Ljava/lang/String; = "key_account_token2"

.field public static final CURRENT_TAB:Ljava/lang/String; = "key_current_tab"

.field public static final DIALOG_TYPE:Ljava/lang/String; = "key_action_type"

.field public static final FILELIST_ITEM:Ljava/lang/String; = "key_filelist_item"

.field public static final FILE_EXT:Ljava/lang/String; = "key_file_ext"

.field public static final FILE_ID:Ljava/lang/String; = "key_file_id"

.field public static final FILE_NAME:Ljava/lang/String; = "key_file_name"

.field public static final FILE_PATH:Ljava/lang/String; = "key_file_path"

.field public static final FILE_WEBEXT:Ljava/lang/String; = "key_file_web_ext"

.field public static final IS_ACCOUNT:Ljava/lang/String; = "key_is_account"

.field public static final IS_ROOT:Ljava/lang/String; = "key_is_root"

.field public static final LIST_TYPE:Ljava/lang/String; = "key_list_type"

.field public static final MENU_ID:Ljava/lang/String; = "key_menu_id"

.field public static final SEARCH_KEY:Ljava/lang/String; = "key_search_key"

.field public static final SEARCH_PATH:Ljava/lang/String; = "key_search_path"

.field public static final SEARCH_TYPE:Ljava/lang/String; = "key_search_type"

.field public static final SERVICE_TYPE:Ljava/lang/String; = "key_service_type"

.field public static final SHOW_TIME:Ljava/lang/String; = "key_show_time"

.field public static final STORAGE_FILE:Ljava/lang/String; = "key_storage_file"

.field public static final STORAGE_PATH:Ljava/lang/String; = "key_storage_path"

.field public static final STORAGE_TYPE:Ljava/lang/String; = "key_storage_type"

.field public static final TAB_INDEX:Ljava/lang/String; = "key_tab_index"

.field public static final WEB_TITLE:Ljava/lang/String; = "key_web_title"

.field public static final WEB_URL:Ljava/lang/String; = "key_web_url"

.field public static final ZIP_CHARSET:Ljava/lang/String; = "key_zip_charset"

.field public static final ZIP_FILE:Ljava/lang/String; = "key_zip_file"

.field public static final ZIP_MOVE:Ljava/lang/String; = "key_zip_move"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
