.class Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController$1;
.super Ljava/lang/Object;
.source "SheetEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;)V
    .locals 0

    .prologue
    .line 1534
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController$1;->this$1:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onKeyDown(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "a_view"    # Landroid/view/View;
    .param p2, "a_keyCode"    # I
    .param p3, "a_event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1554
    const/4 v0, 0x0

    .line 1555
    .local v0, "isSuccess":Z
    packed-switch p2, :pswitch_data_0

    .line 1571
    :pswitch_0
    return v0

    .line 1555
    nop

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "a_keyCode"    # I
    .param p2, "a_event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1576
    const/16 v0, 0x15

    if-eq p1, v0, :cond_0

    const/16 v0, 0x16

    if-ne p1, v0, :cond_1

    .line 1577
    :cond_0
    const/4 v0, 0x1

    .line 1579
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "a_view"    # Landroid/view/View;
    .param p2, "a_keyCode"    # I
    .param p3, "a_event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1538
    const/4 v0, 0x0

    .line 1539
    .local v0, "result":Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 1541
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController$1;->this$1:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$800(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 1549
    :cond_0
    :goto_0
    return v0

    .line 1544
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1546
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController$1;->this$1:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$900(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v1, p2, p3}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto :goto_0
.end method
