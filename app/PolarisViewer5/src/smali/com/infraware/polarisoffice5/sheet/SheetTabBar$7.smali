.class Lcom/infraware/polarisoffice5/sheet/SheetTabBar$7;
.super Ljava/lang/Object;
.source "SheetTabBar.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$7;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 412
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 411
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 405
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$7;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->currentSheetTabEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 406
    .local v1, "fileName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x1f

    if-lt v2, v3, :cond_0

    .line 407
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$7;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0702cc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 408
    .local v0, "errMsg":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$7;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # invokes: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->onToastMessage(Ljava/lang/String;)V
    invoke-static {v2, v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$800(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;Ljava/lang/String;)V

    .line 410
    .end local v0    # "errMsg":Ljava/lang/String;
    :cond_0
    return-void
.end method
