.class public interface abstract Lcom/infraware/polarisoffice5/sheet/SheetEnum$SHEET_FUNCTION_LIST;
.super Ljava/lang/Object;
.source "SheetEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SHEET_FUNCTION_LIST"
.end annotation


# static fields
.field public static final SHEET_FUNCTION_ALL:I = 0x0

.field public static final SHEET_FUNCTION_DATABASE:I = 0x7

.field public static final SHEET_FUNCTION_DATE_TIME:I = 0x4

.field public static final SHEET_FUNCTION_FINANTIAL:I = 0x2

.field public static final SHEET_FUNCTION_INFO:I = 0xa

.field public static final SHEET_FUNCTION_LOGICAL:I = 0x9

.field public static final SHEET_FUNCTION_LOOKUP:I = 0x6

.field public static final SHEET_FUNCTION_MATH:I = 0x3

.field public static final SHEET_FUNCTION_RECENT:I = 0x1

.field public static final SHEET_FUNCTION_STATISTICAL:I = 0x5

.field public static final SHEET_FUNCTION_TEXT:I = 0x8
