.class Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;
.super Landroid/os/Handler;
.source "SheetAutoFilterPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 135
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 136
    .local v2, "nButtonIndex":I
    if-ne v2, v6, :cond_2

    .line 137
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aput-boolean v6, v3, v6

    .line 138
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aput-boolean v8, v3, v7

    .line 145
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aget-boolean v3, v3, v6

    if-eqz v3, :cond_3

    .line 146
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v3

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget v4, v4, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFocusedIndex:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterListItems:[Ljava/lang/String;

    aget-object v5, v5, v6

    invoke-virtual {v3, v4, v6, v5}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    .line 159
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->dismiss()V

    .line 160
    return-void

    .line 140
    :cond_2
    if-ne v2, v7, :cond_0

    .line 141
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aput-boolean v8, v3, v6

    .line 142
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aput-boolean v6, v3, v7

    goto :goto_0

    .line 147
    :cond_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aget-boolean v3, v3, v7

    if-eqz v3, :cond_4

    .line 148
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v3

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget v4, v4, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFocusedIndex:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterListItems:[Ljava/lang/String;

    aget-object v5, v5, v7

    invoke-virtual {v3, v4, v6, v5}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    goto :goto_1

    .line 151
    :cond_4
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mlvFilterlist:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$100(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;

    .line 152
    .local v0, "adapter":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->getSelectedItemIndex()I

    move-result v1

    .line 153
    .local v1, "checkedIndex":I
    if-lez v1, :cond_5

    .line 154
    add-int/lit8 v1, v1, 0x2

    .line 156
    :cond_5
    if-ltz v1, :cond_1

    .line 157
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v3

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget v4, v4, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFocusedIndex:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterListItems:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v3, v4, v8, v5}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    goto :goto_1
.end method
