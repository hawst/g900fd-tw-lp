.class Lcom/infraware/polarisoffice5/sheet/SheetTabBar$1;
.super Landroid/os/Handler;
.source "SheetTabBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->createView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 114
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 126
    :goto_0
    return-void

    .line 117
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    goto :goto_0

    .line 120
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    neg-int v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    goto :goto_0

    .line 123
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    const/16 v1, 0x42

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->fullScroll(I)Z

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch -0x212
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
