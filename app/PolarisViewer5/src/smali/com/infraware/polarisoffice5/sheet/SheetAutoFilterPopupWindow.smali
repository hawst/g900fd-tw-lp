.class public Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;
.super Landroid/widget/PopupWindow;
.source "SheetAutoFilterPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;,
        Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;,
        Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;
    }
.end annotation


# static fields
.field static final AUTOFILTER_BTN_HEIGHT:I = 0x3c

.field static final AUTOFILTER_ITEM_HEIGHT:I = 0x37

.field static final FILTER_ASC:I = 0x1

.field static final FILTER_DES:I = 0x2


# instance fields
.field private mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

.field mAutoFilterHandler:Landroid/os/Handler;

.field mBtnFilters:[Landroid/widget/Button;

.field private mBtnHeight:I

.field private mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

.field mClick_filter:Landroid/view/View$OnClickListener;

.field mFilterListItems:[Ljava/lang/String;

.field private mFilterLists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mFocusedIndex:I

.field private mListHeight:I

.field private mPopupwidth:I

.field private mSelectedFilterBtnRect:Landroid/graphics/Rect;

.field mTotalCount:I

.field mbCheckedItems:[Z

.field mbFixedItems:[Z

.field private mlvFilterlist:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Landroid/view/View;II[Ljava/lang/String;[Z[Z)V
    .locals 2
    .param p1, "aActivity"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .param p2, "aAnchor"    # Landroid/view/View;
    .param p3, "nTotalCount"    # I
    .param p4, "nFocusedIndex"    # I
    .param p5, "filterListitem"    # [Ljava/lang/String;
    .param p6, "bFixedItem"    # [Z
    .param p7, "bCheckedItem"    # [Z

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Landroid/widget/PopupWindow;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 42
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    .line 44
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterLists:Ljava/util/ArrayList;

    .line 46
    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mTotalCount:I

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFocusedIndex:I

    .line 47
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterListItems:[Ljava/lang/String;

    .line 49
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnFilters:[Landroid/widget/Button;

    .line 50
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mSelectedFilterBtnRect:Landroid/graphics/Rect;

    .line 53
    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mListHeight:I

    .line 54
    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnHeight:I

    .line 55
    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mPopupwidth:I

    .line 132
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$1;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mAutoFilterHandler:Landroid/os/Handler;

    .line 163
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mClick_filter:Landroid/view/View$OnClickListener;

    .line 59
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 60
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Sheet - FunctionList popup create failed, activity is NULL"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    iput p3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mTotalCount:I

    .line 64
    iput p4, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFocusedIndex:I

    .line 65
    iput-object p5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterListItems:[Ljava/lang/String;

    .line 66
    iput-object p6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbFixedItems:[Z

    .line 67
    iput-object p7, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    .line 69
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->onCreate()V

    .line 70
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->onCreateView()V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mlvFilterlist:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterLists:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mSelectedFilterBtnRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$302(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;
    .param p1, "x1"    # Landroid/graphics/Rect;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mSelectedFilterBtnRect:Landroid/graphics/Rect;

    return-object p1
.end method

.method private getSelectIndex([Z)I
    .locals 2
    .param p1, "bCheckedItem"    # [Z

    .prologue
    .line 196
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 197
    aget-boolean v1, p1, v0

    if-eqz v1, :cond_0

    .line 199
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 196
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private makeDynamicFilterList(Ljava/util/ArrayList;[Z)V
    .locals 7
    .param p2, "bCheckedItem"    # [Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;[Z)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "aAutofilerMenus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 185
    .local v4, "mListItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v6, v1, :cond_0

    .line 186
    new-instance v2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aget-boolean v3, p2, v6

    invoke-direct {v2, p0, v1, v3}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;Ljava/lang/String;Z)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 188
    :cond_0
    invoke-direct {p0, p2}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->getSelectIndex([Z)I

    move-result v5

    .line 191
    .local v5, "nSelectedIndex":I
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const v3, 0x7f030045

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;Landroid/content/Context;ILjava/util/List;I)V

    .line 192
    .local v0, "adapter":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mlvFilterlist:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 193
    return-void
.end method

.method private onCreate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 74
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 75
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setFocusable(Z)V

    .line 76
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setTouchable(Z)V

    .line 77
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setOutsideTouchable(Z)V

    .line 78
    return-void
.end method

.method private onCreateView()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 81
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 82
    .local v1, "inflator":Landroid/view/LayoutInflater;
    const v5, 0x7f030044

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 84
    .local v4, "popupListView":Landroid/view/View;
    const v5, 0x7f0b01dc

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mlvFilterlist:Landroid/widget/ListView;

    .line 87
    const v5, 0x7f0b01da

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    .line 88
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    const v6, 0x7f040006

    invoke-virtual {v5, v9, v7, v6}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initImage(IZI)V

    .line 89
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    invoke-virtual {v5, v8, v8}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->showSepateLine(ZZ)V

    .line 90
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mAutoFilterHandler:Landroid/os/Handler;

    invoke-virtual {v5, v6, v8}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->addHandler(Landroid/os/Handler;I)V

    .line 93
    const/4 v3, 0x0

    .line 94
    .local v3, "nListCount":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterLists:Ljava/util/ArrayList;

    .line 95
    iget v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mTotalCount:I

    add-int/lit8 v5, v5, -0x2

    new-array v2, v5, [Z

    .line 96
    .local v2, "m_baCheckedItem_Inlist":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mTotalCount:I

    if-ge v0, v5, :cond_3

    .line 97
    if-ne v0, v7, :cond_0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterListItems:[Ljava/lang/String;

    aget-object v5, v5, v0

    const-string/jumbo v6, "Ascending"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    if-ne v0, v9, :cond_2

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterListItems:[Ljava/lang/String;

    aget-object v5, v5, v0

    const-string/jumbo v6, "Descending"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_2

    .line 96
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterLists:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterListItems:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v5, v3, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 100
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aget-boolean v5, v5, v0

    aput-boolean v5, v2, v3

    .line 101
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 104
    :cond_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterLists:Ljava/util/ArrayList;

    invoke-direct {p0, v5, v2}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->makeDynamicFilterList(Ljava/util/ArrayList;[Z)V

    .line 107
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getOrientation()I

    move-result v5

    if-ne v5, v7, :cond_4

    const/4 v5, 0x7

    if-le v3, v5, :cond_4

    .line 108
    const/4 v3, 0x7

    .line 109
    :cond_4
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getOrientation()I

    move-result v5

    if-ne v5, v9, :cond_5

    const/4 v5, 0x3

    if-le v3, v5, :cond_5

    .line 110
    const/4 v3, 0x4

    .line 112
    :cond_5
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aget-boolean v5, v5, v7

    if-eqz v5, :cond_6

    .line 113
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    invoke-virtual {v5, v8}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setSelection(I)V

    .line 119
    :goto_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/high16 v6, 0x42320000    # 44.5f

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mListHeight:I

    .line 120
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/high16 v6, 0x42200000    # 40.0f

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnHeight:I

    .line 121
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/high16 v6, 0x43640000    # 228.0f

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mPopupwidth:I

    .line 123
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setContentView(Landroid/view/View;)V

    .line 124
    iget v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mPopupwidth:I

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setWidth(I)V

    .line 126
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mlvFilterlist:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getCount()I

    move-result v5

    if-le v5, v3, :cond_8

    .line 127
    iget v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mListHeight:I

    mul-int/2addr v5, v3

    iget v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mListHeight:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iget v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnHeight:I

    add-int/2addr v5, v6

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setHeight(I)V

    .line 130
    :goto_3
    return-void

    .line 114
    :cond_6
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mbCheckedItems:[Z

    aget-boolean v5, v5, v9

    if-eqz v5, :cond_7

    .line 115
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    invoke-virtual {v5, v7}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setSelection(I)V

    goto :goto_2

    .line 117
    :cond_7
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnSort:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->clearSelection()V

    goto :goto_2

    .line 129
    :cond_8
    iget v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mListHeight:I

    mul-int/2addr v5, v3

    iget v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mBtnHeight:I

    add-int/2addr v5, v6

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setHeight(I)V

    goto :goto_3
.end method

.method private onPreShow()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method


# virtual methods
.method public show()V
    .locals 4

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->onPreShow()V

    .line 208
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->dismiss()V

    .line 209
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 259
    return-void
.end method
