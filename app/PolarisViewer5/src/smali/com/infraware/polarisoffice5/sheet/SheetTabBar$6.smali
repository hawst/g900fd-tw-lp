.class Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;
.super Ljava/lang/Object;
.source "SheetTabBar.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 360
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$200(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$500(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-boolean v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bSelected:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$200(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$500(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-boolean v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bProtect:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsViewerMode()Z

    move-result v5

    if-nez v5, :cond_1

    .line 363
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$200(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$500(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    .line 365
    .local v3, "oSheetItem":Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v5

    const v8, 0x7f030048

    const/4 v9, 0x0

    invoke-static {v5, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 366
    .local v4, "selectedCompoundBtn":Landroid/widget/LinearLayout;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v8, -0x1

    invoke-direct {v2, v5, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 367
    .local v2, "oSelectorParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v2, v7, v7, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 368
    const v5, 0x7f0b01e5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 369
    .local v0, "edt_sheet_tap_rename":Landroid/widget/EditText;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iput-object v0, v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->currentSheetTabEdit:Landroid/widget/EditText;

    .line 370
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 372
    iget-object v5, v3, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_aszSheetName:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 375
    new-instance v1, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;-><init>(Landroid/content/Context;)V

    .line 376
    .local v1, "inputFilter":Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->getFilters()[Landroid/text/InputFilter;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 378
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 379
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->editNameWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 382
    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 383
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_onclick_okbtn:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 384
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$700(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/LinearLayout;

    move-result-object v5

    iget-object v7, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$500(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 385
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$700(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/LinearLayout;

    move-result-object v5

    iget-object v7, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I
    invoke-static {v7}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$500(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)I

    move-result v7

    invoke-virtual {v5, v4, v7, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 388
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->currentSheetTabEdit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->requestFocus()Z

    .line 391
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->currentSheetTabEdit:Landroid/widget/EditText;

    invoke-static {v5}, Lcom/infraware/office/util/EvUtil;->showIme(Landroid/view/View;)V

    .line 394
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/PopupWindow;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/PopupWindow;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 395
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/PopupWindow;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    move v5, v6

    .line 399
    .end local v0    # "edt_sheet_tap_rename":Landroid/widget/EditText;
    .end local v1    # "inputFilter":Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;
    .end local v2    # "oSelectorParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "oSheetItem":Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;
    .end local v4    # "selectedCompoundBtn":Landroid/widget/LinearLayout;
    :goto_0
    return v5

    :cond_1
    move v5, v7

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 355
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 350
    const/4 v0, 0x0

    return v0
.end method
