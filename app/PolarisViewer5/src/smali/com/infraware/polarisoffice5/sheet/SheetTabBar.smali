.class public Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
.super Ljava/lang/Object;
.source "SheetTabBar.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_MOVE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_EDIT;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;
    }
.end annotation


# static fields
.field public static final MSG_SHEET_SHEETBAR_SCROLFULLLEFT:I = -0x213

.field public static final MSG_SHEET_SHEETBAR_SCROLFULLRIGHT:I = -0x212

.field public static final MSG_SHEET_SHEETBAR_SCROLLLEFT:I = -0x211

.field public static final MSG_SHEET_SHEETBAR_SCROLLRIGHT:I = -0x210

.field static final POPUPMENU_SHOWING_TIME:I = 0x9c4


# instance fields
.field private bEndOfInsertSheet:Z

.field btnSize:I

.field private curIndex:I

.field private curIndex_doubletab:I

.field currentSheetTabEdit:Landroid/widget/EditText;

.field currentSheetTabOkBtn:Landroid/widget/ImageButton;

.field dropPositionX:I

.field editNameWatcher:Landroid/text/TextWatcher;

.field flag_click:Z

.field private mDetector:Landroid/view/GestureDetector;

.field mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private mDragMode:Z

.field mDragView:Landroid/widget/ImageView;

.field mFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field mHandler:Landroid/os/Handler;

.field mImm:Landroid/view/inputmethod/InputMethodManager;

.field private mLocation:[I

.field private mPopupMenu:Landroid/widget/PopupWindow;

.field private mSheetTab:Landroid/widget/LinearLayout;

.field private mSheeteDeleteDialog:Landroid/app/AlertDialog;

.field mShowRunnable:Ljava/lang/Runnable;

.field private mTabScroll:Landroid/widget/HorizontalScrollView;

.field private mToast:Landroid/widget/Toast;

.field public m_nCurrentSheetPosition:I

.field private m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

.field private m_oScrolbarHandler:Landroid/os/Handler;

.field private m_oSheetItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;",
            ">;"
        }
    .end annotation
.end field

.field m_onclick_okbtn:Landroid/widget/TextView$OnEditorActionListener;

.field m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

.field private popupDelete:Landroid/widget/Button;

.field private popupDuplicate:Landroid/widget/Button;

.field private popupInsert:Landroid/widget/Button;

.field private startDragViewIndex:I

.field totalSheetCount:I


# direct methods
.method public constructor <init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 3
    .param p1, "a_oActivity"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_nCurrentSheetPosition:I

    .line 50
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;

    .line 51
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    .line 52
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 60
    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I

    .line 61
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 62
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;

    .line 63
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->flag_click:Z

    .line 64
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheeteDeleteDialog:Landroid/app/AlertDialog;

    .line 66
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mDragMode:Z

    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mLocation:[I

    .line 71
    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->startDragViewIndex:I

    .line 72
    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex:I

    .line 74
    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->dropPositionX:I

    .line 75
    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->btnSize:I

    .line 76
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->currentSheetTabEdit:Landroid/widget/EditText;

    .line 77
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->currentSheetTabOkBtn:Landroid/widget/ImageButton;

    .line 78
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->bEndOfInsertSheet:Z

    .line 79
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mToast:Landroid/widget/Toast;

    .line 84
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mHandler:Landroid/os/Handler;

    .line 85
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mShowRunnable:Ljava/lang/Runnable;

    .line 309
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$5;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 345
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$6;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 403
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$7;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$7;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->editNameWatcher:Landroid/text/TextWatcher;

    .line 422
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_onclick_okbtn:Landroid/widget/TextView$OnEditorActionListener;

    .line 448
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 89
    check-cast p1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .end local p1    # "a_oActivity":Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    .line 91
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mDetector:Landroid/view/GestureDetector;

    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mDetector:Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 93
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mHandler:Landroid/os/Handler;

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/HorizontalScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mDragMode:Z

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$302(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
    .param p1, "x1"    # Landroid/widget/PopupWindow;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;

    return-object p1
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .prologue
    .line 42
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I

    return v0
.end method

.method static synthetic access$502(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I

    return p1
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->onToastMessage(Ljava/lang/String;)V

    return-void
.end method

.method private gestureAction(Landroid/widget/LinearLayout;)V
    .locals 1
    .param p1, "a_compoundBtn"    # Landroid/widget/LinearLayout;

    .prologue
    .line 259
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 299
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$4;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 306
    return-void
.end method

.method private linkSheetTabScrollTouchEvent()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    new-instance v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$2;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 158
    return-void
.end method

.method private onToastMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "a_errMsg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mToast:Landroid/widget/Toast;

    .line 101
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mToast:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 102
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 103
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public IsSheetTabEditMode()Z
    .locals 3

    .prologue
    .line 519
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 521
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0b01e4

    if-ne v1, v2, :cond_0

    .line 522
    const/4 v1, 0x1

    .line 524
    :goto_1
    return v1

    .line 519
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 524
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public addSheet(Ljava/lang/String;IZ)V
    .locals 10
    .param p1, "a_szSheetName"    # Ljava/lang/String;
    .param p2, "a_nIndex"    # I
    .param p3, "a_bprotect"    # Z

    .prologue
    const/4 v9, 0x0

    .line 234
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const v4, 0x7f030047

    const/4 v5, 0x0

    invoke-static {v1, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 235
    .local v2, "compoundBtn":Landroid/widget/LinearLayout;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v4, -0x1

    invoke-direct {v7, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 236
    .local v7, "oSelectorParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v7, v9, v9, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 238
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 239
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->gestureAction(Landroid/widget/LinearLayout;)V

    .line 241
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 243
    .local v6, "oResources":Landroid/content/res/Resources;
    const v1, 0x7f0b01e3

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 244
    .local v8, "txt_sheet_name":Landroid/widget/TextView;
    invoke-virtual {v8, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    const v1, 0x7f0b01e2

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 247
    .local v3, "img_sheet_hold":Landroid/widget/ImageView;
    if-eqz p3, :cond_0

    .line 248
    const v1, 0x7f0201df

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 250
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 253
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;-><init>(Ljava/lang/String;Landroid/widget/LinearLayout;Landroid/widget/ImageView;IZ)V

    .line 254
    .local v0, "oItem":Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    return-void
.end method

.method public constructSheetbar()V
    .locals 5

    .prologue
    .line 189
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 190
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 192
    const/4 v2, 0x0

    .line 193
    .local v2, "pPageList":[Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v3, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetNameList()[Ljava/lang/String;

    move-result-object v2

    .line 196
    if-eqz v2, :cond_2

    .line 197
    const/4 v3, 0x0

    aget-object v3, v2, v3

    if-eqz v3, :cond_0

    .line 200
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_nCurrentSheetPosition:I

    .line 201
    iget v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_nCurrentSheetPosition:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 204
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EV;->getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 207
    const/4 v1, 0x0

    .local v1, "nIndex":I
    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_4

    .line 208
    const/4 v0, 0x0

    .line 209
    .local v0, "bprotect":Z
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    invoke-virtual {v3, v4, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    .line 210
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    iget v3, v3, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bProtectSheet:I

    if-lez v3, :cond_3

    .line 211
    const/4 v0, 0x1

    .line 212
    :cond_3
    aget-object v3, v2, v1

    invoke-virtual {p0, v3, v1, v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->addSheet(Ljava/lang/String;IZ)V

    .line 207
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 215
    .end local v0    # "bprotect":Z
    :cond_4
    iget v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_nCurrentSheetPosition:I

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->setSelectedSheet(I)V

    goto :goto_0
.end method

.method public createView()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const v1, 0x7f0b0127

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    .line 108
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const v1, 0x7f0b0128

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;

    .line 110
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->linkSheetTabScrollTouchEvent()V

    .line 111
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$1;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oScrolbarHandler:Landroid/os/Handler;

    .line 128
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->constructSheetbar()V

    .line 129
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mSheetTab:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x428c0000    # 70.0f

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->btnSize:I

    .line 130
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 543
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mShowRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mShowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 545
    :cond_0
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mShowRunnable:Ljava/lang/Runnable;

    .line 547
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 548
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mHandler:Landroid/os/Handler;

    .line 549
    :cond_1
    return-void
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oScrolbarHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getTabPopupMenu()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method public getbEndOfInsertSheet()Z
    .locals 1

    .prologue
    .line 530
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->bEndOfInsertSheet:Z

    return v0
.end method

.method public isExistFile(Ljava/lang/String;)Z
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 415
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 416
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_aszSheetName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 417
    const/4 v1, 0x1

    .line 419
    :goto_1
    return v1

    .line 415
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 419
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isSheetShown()Z
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 514
    const/4 v0, 0x1

    .line 515
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onDrop(II)V
    .locals 11
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    const/4 v6, 0x0

    .line 163
    const/4 v5, 0x0

    .line 164
    .local v5, "dropIndex":I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 165
    .local v9, "len":I
    const/4 v0, 0x2

    new-array v10, v0, [I

    .line 166
    .local v10, "mSearchLocation":[I
    const/4 v8, 0x1

    .line 167
    .local v8, "lastflag":Z
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v9, :cond_0

    .line 169
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_oSheetButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    .line 170
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->dropPositionX:I

    aget v1, v10, v6

    if-ge v0, v1, :cond_3

    .line 171
    move v5, v7

    .line 172
    const/4 v8, 0x0

    .line 177
    :cond_0
    if-ge p1, v5, :cond_1

    .line 178
    add-int/lit8 v5, v5, -0x1

    .line 180
    :cond_1
    if-eqz v8, :cond_2

    .line 181
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->totalSheetCount:I

    add-int/lit8 v5, v0, -0x1

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v4, 0x1

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISheetEdit(ILjava/lang/String;IIII)V

    .line 186
    return-void

    .line 167
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public setEnable(Z)V
    .locals 2
    .param p1, "a_bEnable"    # Z

    .prologue
    .line 504
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 506
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_oSheetButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 504
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 509
    :cond_0
    return-void
.end method

.method public setSelectedSheet(I)V
    .locals 4
    .param p1, "a_nIndex"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 219
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 221
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_oSheetButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 222
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iput-boolean v2, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bSelected:Z

    .line 223
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-boolean v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bProtect:Z

    if-eqz v1, :cond_0

    .line 224
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_oProtectButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 219
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_oSheetButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 227
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iput-boolean v3, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bSelected:Z

    .line 228
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-boolean v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bProtect:Z

    if-eqz v1, :cond_2

    .line 229
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_oProtectButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 230
    :cond_2
    return-void
.end method

.method public setbEndOfInsertSheet(Z)V
    .locals 0
    .param p1, "a_bEndOfInsertSheet"    # Z

    .prologue
    .line 527
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->bEndOfInsertSheet:Z

    .line 528
    return-void
.end method

.method public show(Z)V
    .locals 2
    .param p1, "a_bShow"    # Z

    .prologue
    const/16 v1, 0x8

    .line 468
    if-eqz p1, :cond_1

    .line 469
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 475
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 477
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mTabScroll:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_0
.end method
