.class Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;
.super Ljava/lang/Object;
.source "SheetTabBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->gestureAction(Landroid/widget/LinearLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 263
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 264
    .local v0, "curIndex":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oSheetItems:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$200(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;

    iget-boolean v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bSelected:Z

    if-eqz v1, :cond_1

    .line 266
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/PopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 269
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    const/4 v2, 0x0

    # setter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v1, v2}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$302(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->flag_click:Z

    .line 274
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 276
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x6

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    .line 282
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-boolean v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->flag_click:Z

    if-eqz v1, :cond_3

    .line 285
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->flag_click:Z

    .line 287
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->DisableCurrentSheetStatus()V

    .line 289
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isSheetPageLoaded(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 290
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->requestSheetPageIndex(I)V

    .line 291
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    const/16 v2, 0x24

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->showDialog(I)V

    .line 293
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isShowMemo()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 294
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->HideMemo()V

    .line 297
    :cond_5
    return-void
.end method
