.class public Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
.super Lcom/infraware/office/baseframe/EvBaseEditorActivity;
.source "SheetEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/infraware/office/evengine/E$EV_KEY_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SCROLL_COMMAND_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SCROLL_FACTOR_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SHEERT_CHART_DIMENSIONS;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CELL_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_LEGEND;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_SERIES;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CHART_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CLEAR;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_COMMENT_EDIT;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_CONDITIONALFORMAT_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_EDIT;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_EDITOR_STATUS;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_FORMAT;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_FUNCTION;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_INPUT_CONFIRM;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_INSERT_CELL;
.implements Lcom/infraware/office/evengine/E$EV_SHEET_SHPW_ROWCOL;
.implements Lcom/infraware/office/evengine/E$EV_STATUS;
.implements Lcom/infraware/office/evengine/EvListener$SheetEditorListener;
.implements Lcom/infraware/polarisoffice5/sheet/SheetEnum$SHEET_FUNCTION_LIST;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;,
        Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;
    }
.end annotation


# static fields
.field private static final CHECK_MENU:I = 0x100


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field espan:Landroid/text/Spannable;

.field private isFormatCopied:Z

.field private isValueCopied:Z

.field mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

.field private mAutofilterRange:[I

.field private mBtKeyContoller:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;

.field private mCellText:Ljava/lang/String;

.field mDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field mEditTouchListener:Landroid/view/View$OnTouchListener;

.field private mEndSelPosition:I

.field mEventHandler:Landroid/os/Handler;

.field private mFuncCellFormat:Landroid/widget/TextView;

.field mImm:Landroid/view/inputmethod/InputMethodManager;

.field private mListPopup:Landroid/app/AlertDialog;

.field private mLoadedSheetPages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

.field private mMsgPopup:Landroid/app/AlertDialog;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mNfcCallback:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;

.field private mNumbersPopup:Landroid/app/AlertDialog;

.field private mRequestSheetPage:I

.field private mSBeamEnabled:Z

.field private mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

.field private mStartSelPosition:I

.field private mToast:Landroid/widget/Toast;

.field private mTvCheckBox:Landroid/widget/TextView;

.field private mTvSymbol:Landroid/widget/TextView;

.field private mTxtDecimal:Landroid/widget/TextView;

.field private mTxtNagativeNum:Landroid/widget/TextView;

.field private m_bChangeKeypad:Z

.field private m_bFilterPopup:Z

.field protected m_bSheetFormatMerge:Z

.field m_baCheckedItem:[Z

.field m_baFixedItem:[Z

.field m_nCellPosArr:[I

.field m_nDialogType:I

.field m_nEditMode:I

.field m_nEditStatus:J

.field private m_nErrMsgId:I

.field m_nFocusedIndex:I

.field private m_nFormulaColorTable:[I

.field m_nLocaleCode:I

.field private m_nPopupListType:I

.field private m_nPopupType:I

.field m_nTotalCount:I

.field private m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

.field m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

.field m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

.field m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

.field m_sTextboxInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

.field m_sfilter_list_item:[Ljava/lang/String;

.field protected m_strSheetFormatMerge:Ljava/lang/String;

.field private mbEditBlock:Z

.field mbFormulaInputByInternalProcess:Z

.field public mbSheetLoadComplete:Z

.field private strSystemDate:Ljava/lang/String;

.field private strSystemTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;-><init>()V

    .line 73
    const-string/jumbo v0, "SheetEditorActivity"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->LOG_CAT:Ljava/lang/String;

    .line 75
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mTvCheckBox:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mTvSymbol:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mTxtDecimal:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mTxtNagativeNum:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mFuncCellFormat:Landroid/widget/TextView;

    .line 76
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNumbersPopup:Landroid/app/AlertDialog;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    .line 77
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mToast:Landroid/widget/Toast;

    .line 78
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 79
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 80
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .line 81
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 82
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sTextboxInfo:Lcom/infraware/office/baseframe/gestureproc/EvObjectProc$OBJECT_RECT_INFO;

    .line 84
    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nPopupType:I

    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nPopupListType:I

    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mStartSelPosition:I

    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEndSelPosition:I

    .line 85
    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditMode:I

    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nLocaleCode:I

    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nDialogType:I

    .line 86
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    .line 87
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mCellText:Ljava/lang/String;

    .line 88
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->strSystemTime:Ljava/lang/String;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->strSystemDate:Ljava/lang/String;

    .line 89
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    .line 91
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    .line 93
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    .line 94
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->espan:Landroid/text/Spannable;

    .line 95
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nFormulaColorTable:[I

    .line 97
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bFilterPopup:Z

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bSheetFormatMerge:Z

    .line 101
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_strSheetFormatMerge:Ljava/lang/String;

    .line 102
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbFormulaInputByInternalProcess:Z

    .line 103
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbEditBlock:Z

    .line 104
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .line 108
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    .line 109
    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nTotalCount:I

    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nFocusedIndex:I

    .line 113
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 114
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;

    .line 115
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSBeamEnabled:Z

    .line 119
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mRequestSheetPage:I

    .line 122
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isFormatCopied:Z

    .line 123
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isValueCopied:Z

    .line 124
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bChangeKeypad:Z

    .line 128
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 129
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 131
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbSheetLoadComplete:Z

    .line 133
    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 207
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$2;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEditTouchListener:Landroid/view/View$OnTouchListener;

    .line 218
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    .line 1509
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$7;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$7;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    .line 1595
    return-void

    .line 95
    nop

    :array_0
    .array-data 4
        -0xa07313
        -0x14a1a0
        -0x729e3e
        -0xd269c7
        -0x40b36f
        -0x1c7dde
        -0xc88062
    .end array-data
.end method

.method private OnSheetHyperlinkClick(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V
    .locals 8
    .param p1, "linkInfo"    # Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .prologue
    const/4 v7, 0x0

    .line 1361
    iget-object v5, p1, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;->szHyperLink:Ljava/lang/String;

    const-string/jumbo v6, "!"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1362
    .local v3, "sheetInfo":[Ljava/lang/String;
    aget-object v4, v3, v7

    .line 1363
    .local v4, "sheetName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1364
    .local v2, "pPageList":[Ljava/lang/String;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v5}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetNameList()[Ljava/lang/String;

    move-result-object v2

    .line 1365
    const/4 v1, -0x1

    .line 1366
    .local v1, "moveIndex":I
    if-eqz v2, :cond_0

    if-eqz v2, :cond_1

    aget-object v5, v2, v7

    if-nez v5, :cond_1

    .line 1379
    :cond_0
    :goto_0
    return-void

    .line 1368
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v5, v2

    if-ge v0, v5, :cond_3

    .line 1369
    aget-object v5, v2, v0

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1370
    add-int/lit8 v1, v0, 0x1

    .line 1368
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1372
    :cond_3
    const/4 v5, -0x1

    if-ne v1, v5, :cond_4

    .line 1373
    const-string/jumbo v5, "SheetEditorActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "sheet move fail, sheet name ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "] is not exist"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1376
    :cond_4
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v5}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v5

    if-eq v1, v5, :cond_0

    .line 1377
    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v6, 0x6

    invoke-virtual {v5, v6, v1}, Lcom/infraware/office/evengine/EvInterface;->IMovePage(II)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/infraware/common/util/SbeamHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSBeamEnabled:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSBeamEnabled:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onMultiWindowStatusChanged(Z)V

    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->searchResultCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .param p1, "x1"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onToastMessage(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nPopupType:I

    return v0
.end method

.method static synthetic access$702(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bFilterPopup:Z

    return p1
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/infraware/office/baseframe/EvBaseView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/infraware/office/baseframe/EvBaseView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    return-object v0
.end method

.method private allMenuEnableforNormalMode()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1258
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getActionBarMenu()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1259
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getActionBarMenu()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1260
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getActionBarDraw()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1261
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->getActionBarDraw()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1262
    :cond_1
    return-void
.end method

.method private getDelayTime()I
    .locals 1

    .prologue
    .line 1660
    const/16 v0, 0x12c

    return v0
.end method

.method private getStdCellPadding()I
    .locals 1

    .prologue
    .line 1661
    const/4 v0, 0x4

    return v0
.end method

.method private getSubCellPadding()I
    .locals 1

    .prologue
    .line 1662
    const/4 v0, 0x2

    return v0
.end method

.method private initializeSheetEngine()V
    .locals 5

    .prologue
    .line 471
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ISheetDrawFormulaRange(ZZ)V

    .line 473
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nFormulaColorTable:[I

    invoke-virtual {v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISheetSetFormulaRangeColor([I)V

    .line 474
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetFrameDetectionArea()Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;

    move-result-object v0

    .line 475
    .local v0, "frameDetectionArea":Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;
    iget v2, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nCtrlBoxMargin:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nCtrlBoxMargin:I

    .line 476
    iget v2, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nFrameDetectionMargin:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nFrameDetectionMargin:I

    .line 477
    iget v2, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nRotCtrlBoxHeight:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nRotCtrlBoxHeight:I

    .line 478
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dipToPx(F)F

    move-result v1

    .line 479
    .local v1, "nPx":F
    float-to-int v2, v1

    iput v2, v0, Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;->m_nSheetDetectionMargin:I

    .line 480
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v0}, Lcom/infraware/office/evengine/EvInterface;->ISetFrameDetectionArea(Lcom/infraware/office/evengine/EV$FRAME_DETECTION_AREA;)V

    .line 484
    return-void
.end method

.method private onCreateMsgPopup(I)Landroid/app/AlertDialog;
    .locals 4
    .param p1, "nType"    # I

    .prologue
    .line 767
    iput p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nPopupType:I

    .line 768
    const/4 v0, 0x0

    .line 769
    .local v0, "nMsgId":I
    packed-switch p1, :pswitch_data_0

    .line 774
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070063

    new-instance v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$6;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$6;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07005f

    new-instance v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$5;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$5;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    .line 787
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 788
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMsgPopup:Landroid/app/AlertDialog;

    return-object v1

    .line 771
    :pswitch_0
    const v0, 0x7f070191

    goto :goto_0

    .line 769
    :pswitch_data_0
    .packed-switch 0x23
        :pswitch_0
    .end packed-switch
.end method

.method private onCreateOnKKOver()V
    .locals 2

    .prologue
    .line 1756
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 1758
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    new-instance v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$8;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$8;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 1772
    return-void
.end method

.method private onMultiWindowStatusChanged(Z)V
    .locals 1
    .param p1, "mIsMaximized"    # Z

    .prologue
    .line 1779
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1780
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->dismiss()V

    .line 1781
    :cond_0
    return-void
.end method

.method private onToastMessage(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1405
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mToast:Landroid/widget/Toast;

    .line 1409
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1410
    return-void

    .line 1407
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method private setNfcCallback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1585
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1593
    :cond_0
    :goto_0
    return-void

    .line 1587
    :cond_1
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 1588
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 1589
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;

    .line 1590
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 1591
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public ActivityMsgProc(IIIIILjava/lang/Object;)I
    .locals 6
    .param p1, "msg"    # I
    .param p2, "p1"    # I
    .param p3, "p2"    # I
    .param p4, "p3"    # I
    .param p5, "p4"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/16 v5, 0x30d

    const/16 v4, 0x30c

    const/16 v3, 0xf

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1272
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isShowSearchBar()Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 1340
    .end local p6    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 1275
    .restart local p6    # "obj":Ljava/lang/Object;
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 1336
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move-result v0

    goto :goto_0

    .line 1278
    :sswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-eq v1, v3, :cond_0

    .line 1283
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_0

    .line 1286
    :sswitch_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->ReplaceBarClose()Z

    move-result v2

    if-ne v2, v0, :cond_2

    move v0, v1

    .line 1287
    goto :goto_0

    .line 1288
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v1

    iget-boolean v1, v1, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsViewerMode()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1290
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1291
    const v1, 0x7f0700ea

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    .line 1304
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1292
    :cond_5
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->bEncryptDoc:Z

    if-eqz v1, :cond_6

    .line 1293
    const v1, 0x7f0700e8

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    goto :goto_1

    .line 1294
    :cond_6
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsViewerMode()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1302
    const v1, 0x7f0700e9

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    goto :goto_1

    .line 1306
    :cond_7
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getActionMode()I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    if-ne v1, v3, :cond_0

    goto :goto_0

    .line 1313
    :sswitch_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->ShowMemo()V

    goto :goto_0

    .line 1316
    :sswitch_3
    invoke-virtual {p0, v5, v0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto :goto_0

    .line 1319
    :sswitch_4
    invoke-virtual {p0, v4, v1, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto :goto_0

    .line 1322
    :sswitch_5
    invoke-virtual {p0, v5, v0, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto/16 :goto_0

    .line 1325
    :sswitch_6
    invoke-virtual {p0, v4, v1, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnEVSheetFuction(III)V

    goto/16 :goto_0

    .line 1328
    :sswitch_7
    check-cast p6, Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;

    .end local p6    # "obj":Ljava/lang/Object;
    invoke-direct {p0, p6}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnSheetHyperlinkClick(Lcom/infraware/office/evengine/EV$SHEET_HYPERLINK_INFO;)V

    goto/16 :goto_0

    .line 1332
    .restart local p6    # "obj":Ljava/lang/Object;
    :sswitch_8
    check-cast p6, Landroid/view/KeyEvent;

    .end local p6    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p2, p6}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->sheetKeyProc(ILandroid/view/KeyEvent;)I

    move-result v0

    goto/16 :goto_0

    .line 1275
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x3 -> :sswitch_0
        0xf -> :sswitch_2
        0x17 -> :sswitch_3
        0x18 -> :sswitch_4
        0x23 -> :sswitch_5
        0x24 -> :sswitch_6
        0x2e -> :sswitch_7
        0x2f -> :sswitch_8
    .end sparse-switch
.end method

.method public CheckMenu()V
    .locals 3

    .prologue
    .line 281
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_0

    .line 300
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 284
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    .line 285
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    iget-boolean v0, v0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsCellSelected()Z

    move-result v0

    if-nez v0, :cond_3

    .line 286
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->sheetProtect()V

    goto :goto_0

    .line 289
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->menuDisable()V

    goto :goto_0

    .line 292
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    iget-boolean v0, v0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v0, :cond_4

    .line 293
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->sheetProtect()V

    goto :goto_0

    .line 296
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->sheetUnProtect()V

    .line 297
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->menuEnable()V

    goto :goto_0
.end method

.method public DisableCurrentSheetStatus()V
    .locals 0

    .prologue
    .line 1218
    return-void
.end method

.method public HideMemo()V
    .locals 3

    .prologue
    .line 325
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MemoView;->isMemoModified()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MemoView;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 327
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v1

    iget-boolean v1, v1, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsViewerMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 328
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 329
    const v1, 0x7f0700ea

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    .line 337
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 344
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->show(Z)V

    .line 345
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/MemoView;->setText(Ljava/lang/String;)V

    .line 346
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/MemoView;->setMemoModified(Z)V

    .line 347
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MemoView;->hide()V

    .line 348
    return-void

    .line 330
    :cond_2
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->bEncryptDoc:Z

    if-eqz v1, :cond_3

    .line 331
    const v1, 0x7f0700e8

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 332
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsViewerMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 333
    const v1, 0x7f07024d

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 335
    :cond_4
    const v1, 0x7f0700e9

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 339
    :cond_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCommentText()Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, "memo_content":Ljava/lang/String;
    if-nez v0, :cond_6

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MemoView;->isMemoModified()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 341
    :cond_6
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/MemoView;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISheetInsertCommentText(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public IsA1Selected()Z
    .locals 2

    .prologue
    .line 1237
    const/4 v0, 0x0

    .line 1238
    .local v0, "m_sCellInfo":Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v0

    .line 1239
    iget-object v1, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v1, v1, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v1, v1, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    if-nez v1, :cond_0

    .line 1240
    const/4 v1, 0x1

    .line 1242
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public IsCanMergeCell()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1221
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v1

    .line 1222
    .local v1, "isSingleCell":Z
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsCellSelected()Z

    move-result v0

    .line 1223
    .local v0, "isCellSelected":Z
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsMergeCells()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1224
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bSheetFormatMerge:Z

    .line 1225
    const v2, 0x7f07022f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_strSheetFormatMerge:Ljava/lang/String;

    .line 1233
    :goto_0
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bSheetFormatMerge:Z

    return v2

    .line 1227
    :cond_0
    const v2, 0x7f070190

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_strSheetFormatMerge:Ljava/lang/String;

    .line 1228
    if-nez v1, :cond_1

    if-nez v0, :cond_2

    .line 1229
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bSheetFormatMerge:Z

    goto :goto_0

    .line 1231
    :cond_2
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bSheetFormatMerge:Z

    goto :goto_0
.end method

.method public IsCanSort()Z
    .locals 1

    .prologue
    .line 1246
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsEmptyCell()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsCellSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsSingleCell()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1247
    :cond_0
    const/4 v0, 0x0

    .line 1249
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public IsCellSelected()Z
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v0, v0, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v0, v0, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 559
    :cond_0
    const/4 v0, 0x0

    .line 560
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public IsEditInTextboxMode()Z
    .locals 1

    .prologue
    .line 626
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditMode:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsEditTextMode()Z
    .locals 1

    .prologue
    .line 622
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditMode:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsEditViewMode()Z
    .locals 1

    .prologue
    .line 634
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditMode:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method IsEmptyCell()Z
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->dwCellType:I

    if-nez v0, :cond_0

    .line 565
    const/4 v0, 0x1

    .line 566
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsFreezeSheet()Z
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bFreeze:I

    if-nez v0, :cond_0

    .line 613
    const/4 v0, 0x0

    .line 614
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public IsHiddenCol()Z
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->dwCellType:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->dwCellType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 577
    const/4 v0, 0x1

    .line 578
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsHiddenRow()Z
    .locals 2

    .prologue
    .line 570
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->dwCellType:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->dwCellType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 571
    const/4 v0, 0x1

    .line 572
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsMemo()Z
    .locals 2

    .prologue
    .line 302
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCommentText()Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "memo_content":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 304
    const/4 v1, 0x1

    .line 306
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public IsMergeCells()Z
    .locals 2

    .prologue
    .line 524
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->dwCellType:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->dwCellType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 525
    const/4 v0, 0x1

    .line 526
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsProtectSheet()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 600
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV;->getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 601
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    .line 602
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    iget v2, v2, Lcom/infraware/office/evengine/EV$SHEET_INFO;->bProtectSheet:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_0

    .line 607
    :goto_0
    return v1

    .line 604
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 606
    :catch_0
    move-exception v0

    .line 607
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public IsSelectionFunctionMode()Z
    .locals 1

    .prologue
    .line 630
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditMode:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsSheetEditMode()Z
    .locals 1

    .prologue
    .line 618
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditMode:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsSheetTextBox()Z
    .locals 1

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetIsTextBox()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1437
    const/4 v0, 0x1

    .line 1439
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsSingleCell()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 543
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 554
    :cond_0
    :goto_0
    return v0

    .line 545
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nRow2:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v3, v3, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    sub-int/2addr v2, v3

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nCol2:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v3, v3, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    sub-int/2addr v2, v3

    if-nez v2, :cond_2

    move v0, v1

    .line 547
    goto :goto_0

    .line 549
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v3, v3, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nRow2:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v3, v3, Lcom/infraware/office/evengine/EV$RANGE;->nRow2:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v3, v3, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nCol2:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v3, v3, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v3, v3, Lcom/infraware/office/evengine/EV$RANGE;->nCol2:I

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 553
    goto :goto_0
.end method

.method public IsSingleMergeCell()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 530
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsMergeCells()Z

    move-result v1

    if-nez v1, :cond_1

    .line 539
    :cond_0
    :goto_0
    return v0

    .line 534
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v1, v1, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nRow1:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v1, v1, Lcom/infraware/office/evengine/EV$RANGE;->nRow2:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nRow2:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v1, v1, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nCol1:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v1, v1, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tActiveRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v1, v1, Lcom/infraware/office/evengine/EV$RANGE;->nCol2:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    iget-object v2, v2, Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;->tSelectedRange:Lcom/infraware/office/evengine/EV$RANGE;

    iget v2, v2, Lcom/infraware/office/evengine/EV$RANGE;->nCol2:I

    if-ne v1, v2, :cond_0

    .line 538
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public IsWholeCols()Z
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetIsWholeCols()Z

    move-result v0

    return v0
.end method

.method public IsWholeRows()Z
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetIsWholeRows()Z

    move-result v0

    return v0
.end method

.method protected NewDocumentMode()V
    .locals 1

    .prologue
    .line 800
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbLoadComplete:Z

    .line 801
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    .line 802
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->NewDocumentMode()V

    .line 803
    return-void
.end method

.method public OnCoreNotify(I)V
    .locals 3
    .param p1, "nNotifyCode"    # I

    .prologue
    .line 459
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnCoreNotify(I)V

    .line 460
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    .line 461
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->constructSheetbar()V

    .line 462
    const/16 v1, 0x24

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 467
    :goto_0
    return-void

    .line 463
    :catch_0
    move-exception v0

    .line 465
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "SheetEditorActivity"

    const-string/jumbo v2, "Can\'t dismissdialog : index : 36"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public OnDrawBitmap(II)V
    .locals 5
    .param p1, "nCallId"    # I
    .param p2, "bShowAutomap"    # I

    .prologue
    const/4 v3, 0x1

    const/16 v4, 0x100

    .line 851
    sparse-switch p1, :sswitch_data_0

    .line 993
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnDrawBitmap(II)V

    .line 994
    return-void

    .line 854
    :sswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    .line 855
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 856
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .line 857
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->setEditMode(I)V

    goto :goto_0

    .line 860
    :sswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    .line 861
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 862
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    goto :goto_0

    .line 867
    :sswitch_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    .line 868
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 869
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .line 870
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 871
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->IsSheetTabEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 872
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->constructSheetbar()V

    goto :goto_0

    .line 883
    :sswitch_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    goto :goto_0

    .line 889
    :sswitch_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    .line 890
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 891
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    .line 892
    const/16 v1, 0x308

    if-ne p1, v1, :cond_0

    .line 893
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->constructSheetbar()V

    goto/16 :goto_0

    .line 920
    :sswitch_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    .line 921
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 922
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    goto/16 :goto_0

    .line 932
    :sswitch_6
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    .line 933
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 934
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .line 935
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 940
    :sswitch_7
    const/16 v1, 0x9

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 958
    :goto_1
    :sswitch_8
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->setEditMode(I)V

    .line 959
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetEditStauts_Editor()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditStatus:J

    .line 960
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 961
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .line 962
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 963
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    .line 964
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v1

    iget-boolean v1, v1, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-nez v1, :cond_1

    .line 966
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/MainActionBar;->sheetUnProtect()V

    .line 970
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 971
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->constructSheetbar()V

    .line 972
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->getbEndOfInsertSheet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 973
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->setbEndOfInsertSheet(Z)V

    .line 974
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->getHandler()Landroid/os/Handler;

    move-result-object v1

    const/16 v2, -0x212

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 941
    :catch_0
    move-exception v0

    .line 943
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "SheetEditorActivity"

    const-string/jumbo v2, "Can\'t dismissdialog : index : 9"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 987
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_9
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    .line 988
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetFormatInfo()Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    .line 989
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    .line 990
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sSheetInfo:Lcom/infraware/office/evengine/EV$SHEET_INFO;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    goto/16 :goto_0

    .line 851
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_8
        0x5 -> :sswitch_8
        0x6 -> :sswitch_8
        0x7 -> :sswitch_8
        0x8 -> :sswitch_8
        0x9 -> :sswitch_8
        0xa -> :sswitch_8
        0xb -> :sswitch_7
        0xc -> :sswitch_9
        0xd -> :sswitch_9
        0xe -> :sswitch_9
        0xf -> :sswitch_9
        0x10 -> :sswitch_9
        0x11 -> :sswitch_9
        0x12 -> :sswitch_9
        0x13 -> :sswitch_9
        0x14 -> :sswitch_9
        0x21 -> :sswitch_0
        0x2c -> :sswitch_9
        0x2d -> :sswitch_8
        0x100 -> :sswitch_8
        0x105 -> :sswitch_8
        0x107 -> :sswitch_8
        0x108 -> :sswitch_8
        0x109 -> :sswitch_3
        0x10a -> :sswitch_3
        0x10b -> :sswitch_3
        0x10c -> :sswitch_3
        0x10d -> :sswitch_3
        0x10e -> :sswitch_3
        0x10f -> :sswitch_3
        0x110 -> :sswitch_5
        0x111 -> :sswitch_5
        0x112 -> :sswitch_5
        0x113 -> :sswitch_6
        0x114 -> :sswitch_5
        0x115 -> :sswitch_5
        0x116 -> :sswitch_5
        0x117 -> :sswitch_5
        0x118 -> :sswitch_5
        0x119 -> :sswitch_5
        0x11a -> :sswitch_5
        0x11b -> :sswitch_5
        0x11c -> :sswitch_2
        0x11d -> :sswitch_3
        0x11e -> :sswitch_5
        0x11f -> :sswitch_5
        0x120 -> :sswitch_5
        0x121 -> :sswitch_5
        0x12b -> :sswitch_6
        0x130 -> :sswitch_5
        0x131 -> :sswitch_5
        0x200 -> :sswitch_6
        0x300 -> :sswitch_5
        0x301 -> :sswitch_5
        0x302 -> :sswitch_6
        0x304 -> :sswitch_5
        0x305 -> :sswitch_8
        0x306 -> :sswitch_4
        0x307 -> :sswitch_4
        0x308 -> :sswitch_4
        0x30c -> :sswitch_5
        0x30d -> :sswitch_5
        0x310 -> :sswitch_6
        0x311 -> :sswitch_5
        0x312 -> :sswitch_6
        0x313 -> :sswitch_5
        0x315 -> :sswitch_1
        0x316 -> :sswitch_6
        0x317 -> :sswitch_6
    .end sparse-switch
.end method

.method public OnEVSheetFuction(III)V
    .locals 11
    .param p1, "eSheetFormatType"    # I
    .param p2, "nParam1"    # I
    .param p3, "nParam2"    # I

    .prologue
    const/4 v3, 0x0

    .line 689
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 691
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 731
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 694
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p2, p3}, Lcom/infraware/office/evengine/EvInterface;->ISheetSetAlignment(II)V

    goto :goto_0

    .line 697
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wFormat:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wFormat:I

    const/16 v1, 0x9

    if-gt v0, v1, :cond_1

    .line 699
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v1, v1, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wFormat:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v2, v2, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wDecimalPlaces:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v3, v3, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->bSeparate:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v4, v4, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wCurrency:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v5, v5, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wNegative:I

    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v6, v6, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wAccounting:I

    iget-object v7, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v7, v7, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wDate:I

    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v8, v8, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wTime:I

    iget-object v9, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v9, v9, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wFraction:I

    const/4 v10, 0x1

    invoke-virtual/range {v0 .. v10}, Lcom/infraware/office/evengine/EvInterface;->ISheetFormat(IIIIIIIIIZ)V

    goto :goto_0

    .line 703
    :pswitch_3
    if-ltz p2, :cond_1

    const/4 v0, 0x4

    if-gt p2, v0, :cond_1

    .line 705
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p2}, Lcom/infraware/office/evengine/EvInterface;->ISheetFunction(I)V

    goto :goto_0

    .line 708
    :pswitch_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetRecalculate()V

    .line 709
    const v0, 0x7f0701c8

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 712
    :pswitch_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x0

    move v1, p2

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISheetEdit(ILjava/lang/String;IIII)V

    goto :goto_0

    .line 715
    :pswitch_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetFixFrame()V

    goto :goto_0

    .line 719
    :pswitch_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p2, p3, v3}, Lcom/infraware/office/evengine/EvInterface;->ISheetShowHideRowCol(III)V

    goto :goto_0

    .line 722
    :pswitch_8
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetProtection()V

    goto :goto_0

    .line 725
    :pswitch_9
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsMergeCells()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsEmptyCell()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetCellType()I

    move-result v0

    const/16 v1, 0x100

    if-ne v0, v1, :cond_3

    .line 726
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetMerge()V

    goto/16 :goto_0

    .line 728
    :cond_3
    const/16 v0, 0x23

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->OnShowDialog(I)V

    goto/16 :goto_0

    .line 691
    nop

    :pswitch_data_0
    .packed-switch 0x300
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public OnGetFormulaFieldSelection()[I
    .locals 1

    .prologue
    .line 1687
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnGetFormulaFieldText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1674
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnGetSheetScrollIInfo()[I
    .locals 1

    .prologue
    .line 1042
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnGetSystemDate(III)Ljava/lang/String;
    .locals 2
    .param p1, "nYear"    # I
    .param p2, "nMonth"    # I
    .param p3, "nDay"    # I

    .prologue
    .line 1031
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 1032
    .local v0, "date":Ljava/util/Date;
    add-int/lit16 v1, p1, -0x76c

    invoke-virtual {v0, v1}, Ljava/util/Date;->setYear(I)V

    .line 1033
    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, v1}, Ljava/util/Date;->setMonth(I)V

    .line 1034
    invoke-virtual {v0, p3}, Ljava/util/Date;->setDate(I)V

    .line 1035
    invoke-static {p0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->strSystemDate:Ljava/lang/String;

    .line 1036
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->strSystemDate:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1037
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->strSystemDate:Ljava/lang/String;

    .line 1038
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public OnGetSystemTime(IID)Ljava/lang/String;
    .locals 2
    .param p1, "nHour"    # I
    .param p2, "nMinute"    # I
    .param p3, "nSecond"    # D

    .prologue
    .line 1018
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 1019
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v0, p1}, Ljava/util/Date;->setHours(I)V

    .line 1020
    invoke-virtual {v0, p2}, Ljava/util/Date;->setMinutes(I)V

    .line 1021
    double-to-int v1, p3

    invoke-virtual {v0, v1}, Ljava/util/Date;->setSeconds(I)V

    .line 1022
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->strSystemTime:Ljava/lang/String;

    .line 1023
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->strSystemTime:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1024
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->strSystemTime:Ljava/lang/String;

    .line 1025
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public OnLoadComplete(I)V
    .locals 3
    .param p1, "bBookmarkExist"    # I

    .prologue
    .line 1726
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_TAB_VIEW_LIST()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1727
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->initSheetBar()V

    .line 1729
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->initializeSheetEngine()V

    .line 1732
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 1733
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    .line 1735
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 1736
    .local v0, "loadedSheetNumber":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1745
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbSheetLoadComplete:Z

    .line 1746
    return-void
.end method

.method public OnOLEFormatInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "nWideString"    # Ljava/lang/String;

    .prologue
    .line 1752
    return-void
.end method

.method public OnSearchMode(IIII)V
    .locals 7
    .param p1, "nResult"    # I
    .param p2, "nCurrentPage"    # I
    .param p3, "nTotalPage"    # I
    .param p4, "nReplaceAllCount"    # I

    .prologue
    const/4 v1, 0x1

    .line 1391
    const-string/jumbo v0, "SheetEditorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "OnSearchMode nReuslt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    packed-switch p1, :pswitch_data_0

    .line 1400
    invoke-super {p0, p1, p2, p3, p4}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnSearchMode(IIII)V

    .line 1401
    :goto_0
    return-void

    .line 1394
    :pswitch_0
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    sget-object v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->searchResultCode:Ljava/lang/String;

    const/16 v3, 0x33

    sget-object v4, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->searchContent:Ljava/lang/String;

    move-object v0, p0

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1392
    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch
.end method

.method public OnSetDataRange(IIII)V
    .locals 0
    .param p1, "row1"    # I
    .param p2, "col1"    # I
    .param p3, "row2"    # I
    .param p4, "col2"    # I

    .prologue
    .line 1711
    return-void
.end method

.method public OnSetFormulaFieldSelection(II)V
    .locals 0
    .param p1, "nStartPos"    # I
    .param p2, "nEndPos"    # I

    .prologue
    .line 1694
    return-void
.end method

.method public OnSetFormulaFieldText(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1680
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mCellText:Ljava/lang/String;

    .line 1682
    return-void
.end method

.method public OnSetFormulaSelectionEnabled(I)V
    .locals 0
    .param p1, "nEnabled"    # I

    .prologue
    .line 1698
    return-void
.end method

.method public OnSetNameBoxText(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1013
    return-void
.end method

.method public OnSetSheetScrollIInfo(IIIII)V
    .locals 0
    .param p1, "nValue"    # I
    .param p2, "nMin"    # I
    .param p3, "nMax"    # I
    .param p4, "nSize"    # I
    .param p5, "bHorizontal"    # I

    .prologue
    .line 1046
    return-void
.end method

.method public OnSheetAutoFilterContext([I[I[I[I)V
    .locals 0
    .param p1, "a_aFilterRange"    # [I
    .param p2, "a_aIndexRange"    # [I
    .param p3, "a_aDataRange"    # [I
    .param p4, "a_aStartRange"    # [I

    .prologue
    .line 1051
    return-void
.end method

.method public OnSheetAutoFilterIndexCellRect([I)V
    .locals 0
    .param p1, "a_aRect"    # [I

    .prologue
    .line 1139
    return-void
.end method

.method public OnSheetAutoFilterMenu(II[Ljava/lang/String;[Z[ZI[I)V
    .locals 4
    .param p1, "nHandleId"    # I
    .param p2, "nFocusedIndex"    # I
    .param p3, "filterObjArr"    # [Ljava/lang/String;
    .param p4, "bFixedItemArr"    # [Z
    .param p5, "bCheckedItemArr"    # [Z
    .param p6, "nCount"    # I
    .param p7, "nCellPos"    # [I

    .prologue
    .line 1059
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v1

    .line 1060
    .local v1, "mActionMode":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/16 v2, 0xc

    if-eq v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/16 v2, 0xd

    if-eq v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/16 v2, 0xe

    if-eq v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/16 v2, 0x9

    if-eq v1, v2, :cond_3

    .line 1064
    iput p6, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nTotalCount:I

    .line 1065
    iput-object p3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    .line 1066
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1068
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1071
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    aget-object v2, v2, v0

    const-string/jumbo v3, "All"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1073
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    const v3, 0x7f0701b1

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1068
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1075
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    aget-object v2, v2, v0

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1077
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    const v3, 0x7f07012b

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 1082
    .end local v0    # "i":I
    :cond_2
    iput p2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nFocusedIndex:I

    .line 1083
    iput-object p4, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_baFixedItem:[Z

    .line 1084
    iput-object p5, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_baCheckedItem:[Z

    .line 1085
    iput-object p7, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nCellPosArr:[I

    .line 1088
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bFilterPopup:Z

    .line 1089
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->makeAutoFilterPopup()V

    .line 1091
    :cond_3
    return-void
.end method

.method public OnSheetAutoFilterMenuEx(Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;)V
    .locals 4
    .param p1, "autofilterMenuInfo"    # Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;

    .prologue
    .line 1095
    const-string/jumbo v2, "LeeEuiChang"

    const-string/jumbo v3, "OnSheetAutoFilterMenuEx"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getActionMode()I

    move-result v1

    .line 1099
    .local v1, "mActionMode":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/16 v2, 0xc

    if-eq v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/16 v2, 0xd

    if-eq v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/16 v2, 0xe

    if-eq v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v2

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    const/16 v2, 0x9

    if-eq v1, v2, :cond_3

    .line 1103
    iget v2, p1, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nItemCount:I

    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nTotalCount:I

    .line 1104
    iget-object v2, p1, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aItemTitle:[Ljava/lang/String;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    .line 1105
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1107
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1110
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    aget-object v2, v2, v0

    const-string/jumbo v3, "All"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1112
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    const v3, 0x7f0701b1

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1107
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1114
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    aget-object v2, v2, v0

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1116
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    const v3, 0x7f07012b

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 1121
    .end local v0    # "i":I
    :cond_2
    iget v2, p1, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->nFocusIndex:I

    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nFocusedIndex:I

    .line 1122
    iget-object v2, p1, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsFixedInfo:[Z

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_baFixedItem:[Z

    .line 1123
    iget-object v2, p1, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aIsCheckedInfo:[Z

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_baCheckedItem:[Z

    .line 1124
    iget-object v2, p1, Lcom/infraware/office/evengine/EV$SHEET_AUTOFILTER_MENU_INFO;->aCellPositionInfo:[I

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nCellPosArr:[I

    .line 1127
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bFilterPopup:Z

    .line 1128
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->makeAutoFilterPopup()V

    .line 1130
    :cond_3
    return-void
.end method

.method public OnSheetAutoFilterStartStateCallBack(I)V
    .locals 0
    .param p1, "nStart"    # I

    .prologue
    .line 1145
    return-void
.end method

.method public OnSheetAutoFilterStatusChanged(I)V
    .locals 0
    .param p1, "nAutoFilterResult"    # I

    .prologue
    .line 1054
    return-void
.end method

.method public OnSheetChart(I)V
    .locals 3
    .param p1, "EV_SHEET_EDITOR_STATUS"    # I

    .prologue
    const/4 v2, 0x0

    .line 1005
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 1006
    const-string/jumbo v1, "a surface chart must contain at least two series."

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1007
    .local v0, "toastMsg":Landroid/widget/Toast;
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 1008
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1010
    .end local v0    # "toastMsg":Landroid/widget/Toast;
    :cond_0
    return-void
.end method

.method public OnSheetCircularReferenceWarning()V
    .locals 2

    .prologue
    .line 1166
    const v0, 0x7f070202

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1167
    return-void
.end method

.method public OnSheetDynamicLoading(I)V
    .locals 3
    .param p1, "a_aProgress"    # I

    .prologue
    const/16 v1, 0x24

    .line 1153
    if-nez p1, :cond_1

    .line 1154
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->showDialog(I)V

    .line 1163
    :cond_0
    :goto_0
    return-void

    .line 1155
    :cond_1
    const/16 v1, 0x64

    if-ne p1, v1, :cond_0

    .line 1157
    const/16 v1, 0x24

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1158
    :catch_0
    move-exception v0

    .line 1160
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "SheetEditorActivity"

    const-string/jumbo v2, "Can\'t dismissdialog : index : 36"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public OnSheetEdit(II)V
    .locals 0
    .param p1, "EEV_SHEET_EDIT"    # I
    .param p2, "EEV_SHEET_EDITOR_RESULT"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 825
    return-void
.end method

.method public OnSheetEditBlock()V
    .locals 1

    .prologue
    .line 1149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbEditBlock:Z

    .line 1150
    return-void
.end method

.method public OnSheetFocus()V
    .locals 0

    .prologue
    .line 998
    return-void
.end method

.method public OnSheetFormulaRangeRect(I[I[I)V
    .locals 0
    .param p1, "a_nCount"    # I
    .param p2, "a_aRangeRect"    # [I
    .param p3, "a_aCurRect"    # [I

    .prologue
    .line 1705
    return-void
.end method

.method public OnSheetFunction(III)V
    .locals 0
    .param p1, "SHEET_EDITOR_STATUS"    # I
    .param p2, "EEV_SHEET_FUNCTION_ERROR"    # I
    .param p3, "EEV_SHEET_FUNCTION_ERROR_CODE"    # I

    .prologue
    .line 1669
    return-void
.end method

.method public OnSheetInputField(II)V
    .locals 0
    .param p1, "EEV_SHEET_EDITOR_RESULT"    # I
    .param p2, "EV_SHEET_INPUTFIELD_RESULT"    # I

    .prologue
    .line 1002
    return-void
.end method

.method public OnSheetMemoNavigate(I)V
    .locals 1
    .param p1, "EV_SHEET_EDITOR_STATUS"    # I

    .prologue
    .line 844
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 845
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->ShowMemo()V

    .line 846
    :cond_0
    return-void
.end method

.method public OnSheetPartialLoad(I)V
    .locals 3
    .param p1, "EEV_SHEET_LOAD_PAGE"    # I

    .prologue
    .line 806
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 807
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    .line 809
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 811
    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mRequestSheetPage:I

    if-ne v1, p1, :cond_1

    .line 813
    const/16 v1, 0x24

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 819
    :cond_1
    :goto_0
    return-void

    .line 814
    :catch_0
    move-exception v0

    .line 816
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "SheetEditorActivity"

    const-string/jumbo v2, "Can\'t dismissdialog : index : 36"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public OnSheetPivotTableInDocument(ZZI)V
    .locals 0
    .param p1, "bExistPivotTableInDocument"    # Z
    .param p2, "bExistPivotTableInCurrentSheet"    # Z
    .param p3, "nSheetIndex"    # I

    .prologue
    .line 1142
    return-void
.end method

.method public OnSheetProtection(I)V
    .locals 2
    .param p1, "EEV_SHEET_EDITOR_RESULT"    # I

    .prologue
    .line 828
    sparse-switch p1, :sswitch_data_0

    .line 837
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->ShowToast()V

    .line 840
    :goto_0
    :sswitch_0
    return-void

    .line 833
    :sswitch_1
    const v0, 0x7f070207

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    .line 834
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 828
    :sswitch_data_0
    .sparse-switch
        -0x5 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method public OnSheetSort(I)V
    .locals 0
    .param p1, "EEV_SHEET_EDITOR_RESULT"    # I

    .prologue
    .line 848
    return-void
.end method

.method protected OnShowDialog(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 734
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nDialogType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nDialogType:I

    if-ne v0, p1, :cond_0

    .line 735
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nDialogType:I

    .line 739
    :goto_0
    return-void

    .line 738
    :cond_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->showDialog(I)V

    goto :goto_0
.end method

.method public OnSpellCheck(Ljava/lang/String;IIIIIII)V
    .locals 0
    .param p1, "pWordStr"    # Ljava/lang/String;
    .param p2, "nLen"    # I
    .param p3, "bClass"    # I
    .param p4, "nPageNum"    # I
    .param p5, "nObjectID"    # I
    .param p6, "nNoteNum"    # I
    .param p7, "nParaIndex"    # I
    .param p8, "nColIndex"    # I

    .prologue
    .line 1719
    return-void
.end method

.method public OnTotalLoadComplete()V
    .locals 5

    .prologue
    .line 424
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    .line 428
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mIsContentSearch:Z

    if-nez v1, :cond_0

    .line 431
    const/16 v1, 0x9

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 440
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnTotalLoadComplete()V

    .line 441
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$4;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$4;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 455
    return-void

    .line 432
    :catch_0
    move-exception v0

    .line 434
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "SheetEditorActivity"

    const-string/jumbo v2, "Can\'t dismissdialog : index : 9"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public ShowMemo()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 310
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetCommentText()Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "memo_content":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->show(Z)V

    .line 313
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v1

    iget-boolean v1, v1, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsViewerMode()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/infraware/polarisoffice5/common/MemoView;->setTextEnabled(Z)V

    .line 314
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/MemoView;->show()V

    .line 315
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/MemoView;->setText(Ljava/lang/String;)V

    .line 321
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/common/MemoView;->setMemoModified(Z)V

    .line 322
    return-void

    :cond_0
    move v1, v2

    .line 313
    goto :goto_0
.end method

.method public ShowToast()V
    .locals 6

    .prologue
    .line 1486
    const-string/jumbo v2, ""

    .line 1487
    .local v2, "strToast":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v3

    iget v0, v3, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    .line 1488
    .local v0, "CurrentSheetNum":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetNameList()[Ljava/lang/String;

    move-result-object v1

    .line 1489
    .local v1, "pPageList":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, -0x1

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1490
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1491
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1492
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070208

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1495
    :goto_0
    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1496
    return-void

    .line 1494
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070209

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public SupportUndoAutofilter()V
    .locals 2

    .prologue
    .line 1519
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterIsRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1520
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->OnSheetAutoFilterCellPos([I)V

    .line 1521
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    .line 1523
    :cond_0
    return-void
.end method

.method protected TemplateDocumetMode()V
    .locals 1

    .prologue
    .line 793
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbLoadComplete:Z

    .line 794
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    .line 795
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->NewDocumentMode()V

    .line 796
    return-void
.end method

.method public afterMoveSheet()V
    .locals 1

    .prologue
    .line 1175
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    iget-boolean v0, v0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-eqz v0, :cond_1

    .line 1176
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->sheetProtect()V

    .line 1180
    :goto_0
    return-void

    .line 1178
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->sheetUnProtect()V

    goto :goto_0
.end method

.method public dismissFilterPopup()V
    .locals 1

    .prologue
    .line 1450
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1451
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->dismiss()V

    .line 1452
    :cond_0
    return-void
.end method

.method public getBitmapPath()[Ljava/lang/String;
    .locals 10

    .prologue
    .line 660
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201da

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 662
    .local v0, "NormalIcon":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201db

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 664
    .local v2, "PressedIcon":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 665
    .local v1, "Normal_path":Ljava/lang/String;
    const/4 v3, 0x0

    .line 667
    .local v3, "Pressed_path":Ljava/lang/String;
    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "SheetAutofilterNormal.png"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 668
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 669
    .local v5, "out":Ljava/io/FileOutputStream;
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x5a

    invoke-virtual {v0, v8, v9, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 670
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "SheetAutofilterPressed.png"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 671
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 672
    .local v6, "out2":Ljava/io/FileOutputStream;
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x5a

    invoke-virtual {v2, v8, v9, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 675
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 676
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 682
    .end local v5    # "out":Ljava/io/FileOutputStream;
    .end local v6    # "out2":Ljava/io/FileOutputStream;
    :goto_0
    const/4 v8, 0x2

    new-array v7, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v8, 0x1

    aput-object v3, v7, v8

    .line 684
    .local v7, "path":[Ljava/lang/String;
    return-object v7

    .line 678
    .end local v7    # "path":[Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 679
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getCellInfo()Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;
    .locals 1

    .prologue
    .line 1721
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sCellInfo:Lcom/infraware/office/evengine/EV$SHEET_CELL_INFO;

    return-object v0
.end method

.method public getCellText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1476
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mCellText:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;
    .locals 3

    .prologue
    .line 1467
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v2, :cond_0

    .line 1468
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 1469
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetCurrentSheetIndex()I

    move-result v0

    .line 1470
    .local v0, "currentSheetIndex":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v2

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EV;->getSheetInfo()Lcom/infraware/office/evengine/EV$SHEET_INFO;

    move-result-object v1

    .line 1471
    .local v1, "sheetInfo":Lcom/infraware/office/evengine/EV$SHEET_INFO;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v1, v0}, Lcom/infraware/office/evengine/EvInterface;->IGetSheetInfo(Lcom/infraware/office/evengine/EV$SHEET_INFO;I)V

    .line 1472
    return-object v1
.end method

.method public getFilterProcessing()Z
    .locals 1

    .prologue
    .line 1447
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bFilterPopup:Z

    return v0
.end method

.method public getSheetBar()Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
    .locals 1

    .prologue
    .line 1443
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    return-object v0
.end method

.method public getSheetHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1658
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method protected initSheetBar()V
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    if-nez v0, :cond_0

    .line 490
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .line 491
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->createView()V

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->show(Z)V

    .line 494
    return-void
.end method

.method public isChangeKeypad()Z
    .locals 1

    .prologue
    .line 1656
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bChangeKeypad:Z

    return v0
.end method

.method public isFormatCopied()Z
    .locals 1

    .prologue
    .line 1652
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isFormatCopied:Z

    return v0
.end method

.method public isSheetEditBlock()Z
    .locals 1

    .prologue
    .line 1480
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mbEditBlock:Z

    return v0
.end method

.method public isSheetPageLoaded(I)Z
    .locals 2
    .param p1, "aReqPage"    # I

    .prologue
    .line 1456
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1457
    const/4 v0, 0x1

    .line 1458
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mLoadedSheetPages:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isShowMemo()Z
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    if-nez v0, :cond_0

    .line 353
    const/4 v0, 0x0

    .line 354
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/MemoView;->isShown()Z

    move-result v0

    goto :goto_0
.end method

.method public isValueCopied()Z
    .locals 1

    .prologue
    .line 1653
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isValueCopied:Z

    return v0
.end method

.method public makeAutoFilterPopup()V
    .locals 8

    .prologue
    .line 1500
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nCellPosArr:[I

    invoke-virtual {v0, v1}, Lcom/infraware/office/baseframe/EvBaseView;->OnSheetAutoFilterCellPos([I)V

    .line 1501
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    if-nez v0, :cond_0

    .line 1502
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    const/4 v2, 0x0

    iget v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nTotalCount:I

    iget v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nFocusedIndex:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sfilter_list_item:[Ljava/lang/String;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_baFixedItem:[Z

    iget-object v7, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_baCheckedItem:[Z

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Landroid/view/View;II[Ljava/lang/String;[Z[Z)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    .line 1504
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1506
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->show()V

    .line 1507
    return-void
.end method

.method public menuDisable()V
    .locals 0

    .prologue
    .line 1265
    return-void
.end method

.method public menuEnable()V
    .locals 0

    .prologue
    .line 1269
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1170
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1171
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1191
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->dismissInlinePopupMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1213
    :goto_0
    return-void

    .line 1193
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isShowMemo()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1194
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->HideMemo()V

    goto :goto_0

    .line 1197
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->ReplaceBarClose()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1198
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_0

    .line 1202
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->IsProtectSheet()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    iget-boolean v0, v0, Lcom/infraware/office/actionbar/MainActionBar;->mPassWordActivity:Z

    if-nez v0, :cond_3

    .line 1203
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->allMenuEnableforNormalMode()V

    .line 1207
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->IsSheetTabEditMode()Z

    move-result v0

    if-ne v0, v1, :cond_4

    .line 1208
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->constructSheetbar()V

    goto :goto_0

    .line 1212
    :cond_4
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 821
    return-void
.end method

.method public onClickActionBar(I)V
    .locals 0
    .param p1, "nViewId"    # I

    .prologue
    .line 1414
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onClickActionBar(I)V

    .line 1415
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->HideMemo()V

    .line 1416
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 358
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 361
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->dismiss()V

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mAutoFilterPopup:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->dismiss()V

    .line 368
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->makeAutoFilterPopup()V

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->getTabPopupMenu()Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->getTabPopupMenu()Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->getTabPopupMenu()Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 375
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mReplaceBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    .line 376
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    .line 384
    :goto_0
    return-void

    .line 377
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mFreeDrawBar:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mFreeDrawBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    .line 378
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    goto :goto_0

    .line 379
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->isShow()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 380
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    goto :goto_0

    .line 382
    :cond_7
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onLayoutChange()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 154
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 155
    const v2, 0x7f03002b

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->setContentView(I)V

    .line 156
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 158
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v2

    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nLocaleCode:I

    .line 161
    const v2, 0x7f0b0132

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/MemoView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    .line 162
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMemoView:Lcom/infraware/polarisoffice5/common/MemoView;

    invoke-virtual {v2, p0}, Lcom/infraware/polarisoffice5/common/MemoView;->init(Lcom/infraware/office/baseframe/EvBaseEditorActivity;)V

    .line 163
    new-instance v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mBtKeyContoller:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$SheetBtKeyController;

    .line 165
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2, p0}, Lcom/infraware/office/baseframe/EvBaseView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 167
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getBitmapPath()[Ljava/lang/String;

    move-result-object v1

    .line 169
    .local v1, "path":[Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v2, :cond_0

    .line 170
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    aget-object v4, v1, v4

    invoke-virtual {v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ISetAutofilterButtonConfiguration(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onLayoutChange()V

    .line 177
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 178
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 179
    new-instance v2, Lcom/infraware/common/util/SbeamHelper;

    const-string/jumbo v3, "text/DirectSharePolarisEditor"

    invoke-direct {v2, p0, v3}, Lcom/infraware/common/util/SbeamHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 180
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 181
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 182
    new-instance v2, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$1;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$1;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 194
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 201
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_1
    :goto_0
    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 202
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onCreateOnKKOver()V

    .line 204
    :cond_2
    return-void

    .line 197
    :cond_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->setNfcCallback()V

    goto :goto_0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 742
    packed-switch p1, :pswitch_data_0

    .line 750
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 744
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onCreateMsgPopup(I)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 747
    :pswitch_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createSheetProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SheetProgressDlg;

    move-result-object v0

    goto :goto_0

    .line 742
    :pswitch_data_0
    .packed-switch 0x23
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1637
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    if-eqz v0, :cond_0

    .line 1639
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->destroy()V

    .line 1640
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_oSheetbar:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    .line 1643
    :cond_0
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onDestroy()V

    .line 1646
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 1647
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1648
    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1650
    :cond_1
    return-void
.end method

.method public onLayoutChange()V
    .locals 0

    .prologue
    .line 389
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 3
    .param p1, "nLocale"    # I

    .prologue
    .line 392
    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nLocaleCode:I

    if-eq v1, p1, :cond_1

    .line 393
    iput p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nLocaleCode:I

    .line 395
    const/4 v0, 0x0

    .line 396
    .local v0, "nTitleId":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNumbersPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNumbersPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 397
    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nPopupType:I

    packed-switch v1, :pswitch_data_0

    .line 407
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNumbersPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 408
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mTxtDecimal:Landroid/widget/TextView;

    const v2, 0x7f07010d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 409
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mTxtNagativeNum:Landroid/widget/TextView;

    const v2, 0x7f07019c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 410
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNumbersPopup:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f070063

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 411
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mNumbersPopup:Landroid/app/AlertDialog;

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f07005f

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 413
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 414
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 415
    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nPopupListType:I

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->showDialog(I)V

    .line 418
    .end local v0    # "nTitleId":I
    :cond_1
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onLocaleChange(I)V

    .line 419
    return-void

    .line 399
    .restart local v0    # "nTitleId":I
    :pswitch_0
    const v0, 0x7f070150

    .line 400
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mTvCheckBox:Landroid/widget/TextView;

    const v2, 0x7f07019b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 403
    :pswitch_1
    const v0, 0x7f070143

    .line 404
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mTvSymbol:Landroid/widget/TextView;

    const v2, 0x7f07019d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 397
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onPause()V

    .line 137
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 755
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 756
    packed-switch p1, :pswitch_data_0

    .line 764
    :goto_0
    return-void

    .line 758
    :pswitch_0
    iput p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nDialogType:I

    .line 759
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 760
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mListPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 761
    :cond_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->removeDialog(I)V

    goto :goto_0

    .line 756
    :pswitch_data_0
    .packed-switch 0x23
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 141
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onResume()V

    .line 143
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/net/Uri;

    .line 146
    .local v0, "filePath":[Landroid/net/Uri;
    const/4 v1, 0x0

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    .line 147
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, p0, v2}, Lcom/infraware/common/util/SbeamHelper;->setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V

    .line 151
    .end local v0    # "filePath":[Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public onSetEditSelection(Lcom/infraware/polarisoffice5/common/ExEditText;)V
    .locals 3
    .param p1, "edit"    # Lcom/infraware/polarisoffice5/common/ExEditText;

    .prologue
    .line 1419
    if-eqz p1, :cond_1

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEndSelPosition:I

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mStartSelPosition:I

    if-ltz v1, :cond_1

    .line 1420
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/common/ExEditText;->length()I

    move-result v1

    iget v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEndSelPosition:I

    if-ge v1, v2, :cond_0

    .line 1421
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/common/ExEditText;->length()I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEndSelPosition:I

    .line 1423
    :cond_0
    :try_start_0
    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mStartSelPosition:I

    iget v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEndSelPosition:I

    invoke-virtual {p1, v1, v2}, Lcom/infraware/polarisoffice5/common/ExEditText;->setSelection(II)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1431
    :cond_1
    :goto_0
    return-void

    .line 1424
    :catch_0
    move-exception v0

    .line 1425
    .local v0, "e":Ljava/lang/IllegalAccessError;
    const/4 v1, 0x0

    :try_start_1
    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEndSelPosition:I

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mStartSelPosition:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1428
    .end local v0    # "e":Ljava/lang/IllegalAccessError;
    :catchall_0
    move-exception v1

    throw v1

    .line 1426
    :catch_1
    move-exception v0

    .line 1427
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const/4 v1, 0x0

    :try_start_2
    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEndSelPosition:I

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mStartSelPosition:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onWindowStatusChanged(ZZZ)V
    .locals 1
    .param p1, "isMaximized"    # Z
    .param p2, "isMinimized"    # Z
    .param p3, "isPinup"    # Z

    .prologue
    .line 1775
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onMultiWindowStatusChanged(Z)V

    .line 1776
    return-void

    .line 1775
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestSheetPageIndex(I)V
    .locals 0
    .param p1, "aReqPage"    # I

    .prologue
    .line 1462
    iput p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mRequestSheetPage:I

    .line 1463
    return-void
.end method

.method public setChangeKeypad(Z)V
    .locals 0
    .param p1, "bChange"    # Z

    .prologue
    .line 1657
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_bChangeKeypad:Z

    return-void
.end method

.method public setEditMode(I)V
    .locals 0
    .param p1, "editMode"    # I

    .prologue
    .line 497
    iput p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nEditMode:I

    .line 516
    return-void
.end method

.method public setEvListener()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 519
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v2, p0

    move-object v4, v3

    move-object v5, p0

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 521
    :cond_0
    return-void
.end method

.method public setFormatCopied(Z)V
    .locals 0
    .param p1, "bcopy"    # Z

    .prologue
    .line 1654
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isFormatCopied:Z

    return-void
.end method

.method public setFuncFormatText()V
    .locals 2

    .prologue
    .line 1383
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wAccounting:I

    if-lez v0, :cond_1

    .line 1384
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mFuncCellFormat:Landroid/widget/TextView;

    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1387
    :cond_0
    :goto_0
    return-void

    .line 1385
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_sFormatInfo:Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;

    iget v0, v0, Lcom/infraware/office/evengine/EV$SHEET_FORMAT_INFO;->wFormat:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 1386
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mFuncCellFormat:Landroid/widget/TextView;

    const-string/jumbo v1, "%"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setValueCopied(Z)V
    .locals 0
    .param p1, "bcopy"    # Z

    .prologue
    .line 1655
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->isValueCopied:Z

    return-void
.end method

.method public sheetFilter()V
    .locals 3

    .prologue
    .line 643
    const/4 v0, 0x0

    .line 645
    .local v0, "bRunningAutofilter":Z
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterIsRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 646
    const/4 v0, 0x1

    .line 648
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilter()V

    .line 650
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterIsRunning()Z

    move-result v1

    if-nez v1, :cond_2

    .line 651
    if-nez v0, :cond_1

    .line 652
    const v1, 0x7f070097

    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onToastMessage(I)V

    .line 654
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/infraware/office/baseframe/EvBaseView;->OnSheetAutoFilterCellPos([I)V

    .line 655
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->drawAllContents()V

    .line 657
    :cond_2
    return-void
.end method

.method protected sheetKeyProc(ILandroid/view/KeyEvent;)I
    .locals 4
    .param p1, "a_keyCode"    # I
    .param p2, "a_keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 1345
    const/4 v1, 0x0

    .line 1346
    .local v1, "result":I
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 1347
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 1349
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v2, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 1357
    :cond_0
    :goto_0
    return v1

    .line 1352
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1354
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v2, v2, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v2, p1, p2}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->onKeyUp(ILandroid/view/KeyEvent;)Z

    goto :goto_0
.end method
