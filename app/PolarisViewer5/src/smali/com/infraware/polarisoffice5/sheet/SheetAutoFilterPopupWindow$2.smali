.class Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;
.super Ljava/lang/Object;
.source "SheetAutoFilterPopupWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 167
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 169
    .local v0, "index":I
    if-nez v0, :cond_0

    .line 170
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget v2, v2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFocusedIndex:I

    const/4 v3, 0x1

    const-string/jumbo v4, "All"

    invoke-virtual {v1, v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    .line 178
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->dismiss()V

    .line 179
    return-void

    .line 173
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterLists:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$200(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v2

    const v3, 0x7f07012b

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget v2, v2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFocusedIndex:I

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v4, v3}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v1

    iget-object v2, v1, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget v3, v1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFocusedIndex:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mFilterLists:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$200(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v1}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    goto :goto_0
.end method
