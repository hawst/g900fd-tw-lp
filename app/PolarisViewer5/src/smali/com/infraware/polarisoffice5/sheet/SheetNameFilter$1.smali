.class Lcom/infraware/polarisoffice5/sheet/SheetNameFilter$1;
.super Ljava/lang/Object;
.source "SheetNameFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 8
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 34
    sget v4, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->MAX_SHEET_NAME_LENGTH:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v5

    sub-int v6, p6, p5

    sub-int/2addr v5, v6

    sub-int v2, v4, v5

    .line 35
    .local v2, "nRest":I
    if-gez v2, :cond_1

    move-object v0, v3

    .line 64
    :cond_0
    :goto_0
    return-object v0

    .line 38
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 39
    .local v0, "checked":Ljava/lang/String;
    sub-int v4, p3, p2

    if-lez v4, :cond_4

    .line 41
    const/4 v1, 0x0

    .line 42
    .local v1, "nIdx":I
    :goto_1
    sub-int v4, p3, p2

    if-ge v1, v4, :cond_3

    .line 44
    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    # invokes: Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->isValid(C)Z
    invoke-static {v4, v5}, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;C)Z

    move-result v4

    if-ne v4, v7, :cond_2

    .line 46
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v2, :cond_0

    .line 50
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    :cond_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    # invokes: Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->isCarriageReturn(C)Z
    invoke-static {v4, v5}, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->access$100(Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;C)Z

    move-result v4

    if-eq v4, v7, :cond_0

    .line 56
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 58
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 60
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter$1;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->onToastMessage(I)V

    goto :goto_0

    .end local v1    # "nIdx":I
    :cond_4
    move-object v0, v3

    .line 64
    goto :goto_0
.end method
