.class Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;
.super Ljava/lang/Object;
.source "SheetTabBar.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "curSheetTab"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x0

    .line 425
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    move-object v7, p1

    .line 426
    check-cast v7, Landroid/widget/EditText;

    .line 427
    .local v7, "edt_sheet_tap_rename":Landroid/widget/EditText;
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 429
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0702ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 430
    .local v8, "errMsg":Ljava/lang/String;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # invokes: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->onToastMessage(Ljava/lang/String;)V
    invoke-static {v0, v8}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$800(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;Ljava/lang/String;)V

    .line 444
    .end local v7    # "edt_sheet_tap_rename":Landroid/widget/EditText;
    .end local v8    # "errMsg":Ljava/lang/String;
    :cond_0
    :goto_0
    return v4

    .line 434
    .restart local v7    # "edt_sheet_tap_rename":Landroid/widget/EditText;
    :cond_1
    invoke-virtual {v7}, Landroid/widget/EditText;->clearFocus()V

    .line 435
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 436
    .local v2, "strRename":Ljava/lang/String;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->curIndex_doubletab:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$500(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)I

    move-result v3

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISheetEdit(ILjava/lang/String;IIII)V

    .line 438
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v7}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 439
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v4}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    goto :goto_0
.end method
