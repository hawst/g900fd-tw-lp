.class Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;
.super Landroid/widget/BaseAdapter;
.source "SheetAutoFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "sheetFilterAdapter"
.end annotation


# instance fields
.field private m_oInflater:Landroid/view/LayoutInflater;

.field private m_oListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p3, "alist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->m_oListItem:Ljava/util/ArrayList;

    .line 145
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    .line 146
    iput-object p3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->m_oListItem:Ljava/util/ArrayList;

    .line 147
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->m_oListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->m_oListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 157
    const/4 v0, 0x0

    .line 159
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->m_oListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 164
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f02012f

    const/4 v3, 0x1

    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "hd":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;
    if-nez p2, :cond_0

    .line 171
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;

    .end local v0    # "hd":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    invoke-direct {v0, v1, v5}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$1;)V

    .line 172
    .restart local v0    # "hd":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030046

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 173
    const v1, 0x7f0b01e1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;->txt_filter_item:Landroid/widget/TextView;

    .line 174
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 186
    :goto_0
    iget-object v2, v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;->txt_filter_item:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    return-object p2

    .line 177
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hd":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;
    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;

    .line 178
    .restart local v0    # "hd":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;
    if-ne p1, v3, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_baCheckedItem:[Z

    add-int/lit8 v2, p1, -0x1

    aget-boolean v1, v1, v2

    if-ne v3, v1, :cond_1

    .line 179
    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_baCheckedItem:[Z

    add-int/lit8 v2, p1, 0x1

    aget-boolean v1, v1, v2

    if-ne v3, v1, :cond_2

    .line 181
    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 183
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method
