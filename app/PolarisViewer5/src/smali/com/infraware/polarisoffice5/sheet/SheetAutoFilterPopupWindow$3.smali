.class Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;
.super Ljava/lang/Object;
.source "SheetAutoFilterPopupWindow.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->show()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 213
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v8

    iget-object v9, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v9}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v8

    if-nez v8, :cond_7

    move v0, v6

    .line 214
    .local v0, "isMultiWindow":Z
    :goto_0
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v8

    iget-object v9, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v9}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getAppRect(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v2

    .line 216
    .local v2, "nDisplayRect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v9, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v9}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v9

    iget-object v9, v9, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getObjectProc()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-result-object v9

    invoke-virtual {v9}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->checkFilterRect()Landroid/graphics/Rect;

    move-result-object v9

    # setter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mSelectedFilterBtnRect:Landroid/graphics/Rect;
    invoke-static {v8, v9}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$302(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 218
    const/4 v1, 0x0

    .local v1, "nActionbarHeight":I
    const/4 v4, 0x0

    .line 219
    .local v4, "nStatusBarHeight":I
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getOrientation()I

    move-result v8

    if-ne v8, v6, :cond_8

    .line 221
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/actionbar/MainActionBar;->isVisible()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 222
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/actionbar/MainActionBar;->getHeight()I

    move-result v1

    .line 223
    :cond_0
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v8

    iget-object v8, v8, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    invoke-virtual {v8}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getObjectProc()Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/baseframe/gestureproc/EvObjectProc;->getStatusBarHeight()I

    move-result v4

    .line 231
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mSelectedFilterBtnRect:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Landroid/graphics/Rect;

    move-result-object v8

    iget v3, v8, Landroid/graphics/Rect;->left:I

    .line 232
    .local v3, "nLeft":I
    add-int v8, v1, v4

    iget-object v9, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mSelectedFilterBtnRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    add-int v5, v8, v9

    .line 235
    .local v5, "nTop":I
    if-eqz v0, :cond_6

    .line 236
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getOrientation()I

    move-result v8

    if-ne v8, v6, :cond_2

    .line 237
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mSelectedFilterBtnRect:Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int v5, v1, v6

    .line 239
    :cond_2
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->getHeight()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v8

    if-le v6, v8, :cond_3

    .line 240
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-virtual {v6, v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setHeight(I)V

    .line 242
    :cond_3
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->getWidth()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v8

    if-le v6, v8, :cond_4

    .line 243
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v6, v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->setWidth(I)V

    .line 245
    :cond_4
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->getHeight()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v8

    sub-int/2addr v8, v5

    if-le v6, v8, :cond_5

    .line 246
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->getHeight()I

    move-result v8

    sub-int v5, v6, v8

    .line 248
    :cond_5
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->getWidth()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v8, v3

    if-le v6, v8, :cond_6

    .line 249
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v6

    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->getWidth()I

    move-result v8

    sub-int v3, v6, v8

    .line 252
    :cond_6
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v8

    invoke-virtual {v6, v8, v7, v3, v5}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 256
    .end local v1    # "nActionbarHeight":I
    .end local v3    # "nLeft":I
    .end local v4    # "nStatusBarHeight":I
    .end local v5    # "nTop":I
    :goto_2
    return-void

    .end local v0    # "isMultiWindow":Z
    .end local v2    # "nDisplayRect":Landroid/graphics/Rect;
    :cond_7
    move v0, v7

    .line 213
    goto/16 :goto_0

    .line 227
    .restart local v0    # "isMultiWindow":Z
    .restart local v1    # "nActionbarHeight":I
    .restart local v2    # "nDisplayRect":Landroid/graphics/Rect;
    .restart local v4    # "nStatusBarHeight":I
    :cond_8
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/actionbar/MainActionBar;->isVisible()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 228
    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getMainActionBar()Lcom/infraware/office/actionbar/MainActionBar;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/office/actionbar/MainActionBar;->getHeight()I

    move-result v1

    goto/16 :goto_1

    .line 255
    .end local v1    # "nActionbarHeight":I
    .end local v4    # "nStatusBarHeight":I
    :cond_9
    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v8, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v8

    const/16 v9, 0x11

    invoke-virtual {v6, v8, v9, v7, v7}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_2
.end method
