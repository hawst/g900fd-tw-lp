.class Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;
.super Ljava/lang/Object;
.source "SheetEditorActivity.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NfcCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V
    .locals 0

    .prologue
    .line 1595
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$1;

    .prologue
    .line 1595
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V

    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 7
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v3, 0x0

    .line 1613
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1630
    :cond_0
    :goto_0
    return-object v3

    .line 1615
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-static {v4}, Lcom/infraware/common/util/SBeamUtils;->isSBeamSupportedDevice(Landroid/content/Context;)Z

    move-result v2

    .line 1616
    .local v2, "sbeamSupport":Z
    if-eqz v2, :cond_0

    .line 1617
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-static {v4}, Lcom/infraware/common/util/SBeamUtils;->isSBeamEnabled(Landroid/content/Context;)Z

    move-result v4

    # setter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSBeamEnabled:Z
    invoke-static {v3, v4}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$1102(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Z)Z

    .line 1619
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mstrOpenFilePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$1200(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefRecord(Landroid/content/Context;[Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 1620
    .local v1, "record":Landroid/nfc/NdefRecord;
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    .line 1622
    .local v0, "payload":Ljava/lang/String;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSBeamEnabled:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1623
    sget-object v3, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_SBEAM_OFF:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v3}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 1628
    :cond_2
    :goto_1
    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefMessage(Landroid/nfc/NdefRecord;)Landroid/nfc/NdefMessage;

    move-result-object v3

    goto :goto_0

    .line 1625
    :cond_3
    const-string/jumbo v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1626
    sget-object v3, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_FILE_NOT_SELECTED:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v3}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;

    move-result-object v1

    goto :goto_1
.end method

.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 1598
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1609
    :cond_0
    :goto_0
    return-void

    .line 1600
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->isSBeamSupportedDevice(Landroid/content/Context;)Z

    move-result v0

    .line 1601
    .local v0, "sbeamSupport":Z
    if-eqz v0, :cond_0

    .line 1602
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSBeamEnabled:Z
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$1100(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1603
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->startSBeamDirectShareService(Landroid/content/Context;)V

    goto :goto_0

    .line 1606
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    sget-object v2, Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;->POPUP_SBEAM_DISABLED:Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;

    invoke-static {v1, v2}, Lcom/infraware/common/util/SBeamUtils;->startSBeamPopupActivity(Landroid/content/Context;Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;)V

    goto :goto_0
.end method
