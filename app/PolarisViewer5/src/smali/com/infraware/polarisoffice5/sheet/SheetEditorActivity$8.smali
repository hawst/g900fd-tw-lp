.class Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$8;
.super Ljava/lang/Object;
.source "SheetEditorActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onCreateOnKKOver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V
    .locals 0

    .prologue
    .line 1758
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 2
    .param p1, "toMulti"    # Z

    .prologue
    .line 1768
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$1300(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1769
    .local v0, "bSplit":Z
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onMultiWindowStatusChanged(Z)V
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Z)V

    .line 1770
    return-void

    .line 1768
    .end local v0    # "bSplit":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "arg0"    # Landroid/graphics/Rect;

    .prologue
    .line 1763
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$1300(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1764
    .local v0, "bSplit":Z
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$8;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # invokes: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onMultiWindowStatusChanged(Z)V
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$1400(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;Z)V

    .line 1765
    return-void

    .line 1763
    .end local v0    # "bSplit":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onZoneChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 1760
    return-void
.end method
