.class Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;
.super Ljava/lang/Object;
.source "SheetAutoFilter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->makeListView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->doButton()V

    .line 116
    if-nez p3, :cond_0

    .line 117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    iget v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_nFocusedIndex:I

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    aget-object v3, v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->finish()V

    .line 121
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    iget v2, v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_nFocusedIndex:I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->arraylist_filter:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->access$100(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    goto :goto_0
.end method
