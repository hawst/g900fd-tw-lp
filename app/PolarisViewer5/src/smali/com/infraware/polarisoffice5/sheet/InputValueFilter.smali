.class public Lcom/infraware/polarisoffice5/sheet/InputValueFilter;
.super Ljava/lang/Object;
.source "InputValueFilter.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;


# instance fields
.field private context:Landroid/content/Context;

.field private filters:[Landroid/text/InputFilter;

.field private m_ToastMsg:Landroid/widget/Toast;

.field private nErrMsgId:I

.field private nMaxLength:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-array v0, v1, [Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->filters:[Landroid/text/InputFilter;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 18
    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nErrMsgId:I

    .line 19
    iput v2, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nMaxLength:I

    .line 27
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->context:Landroid/content/Context;

    .line 29
    packed-switch p2, :pswitch_data_0

    .line 60
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->filters:[Landroid/text/InputFilter;

    new-instance v1, Lcom/infraware/polarisoffice5/sheet/InputValueFilter$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/sheet/InputValueFilter$1;-><init>(Lcom/infraware/polarisoffice5/sheet/InputValueFilter;)V

    aput-object v1, v0, v2

    .line 86
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->filters:[Landroid/text/InputFilter;

    const/4 v1, 0x1

    new-instance v2, Lcom/infraware/polarisoffice5/sheet/InputValueFilter$2;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/sheet/InputValueFilter$2;-><init>(Lcom/infraware/polarisoffice5/sheet/InputValueFilter;)V

    aput-object v2, v0, v1

    .line 101
    return-void

    .line 36
    :pswitch_1
    const v0, 0x7f07010e

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nErrMsgId:I

    .line 37
    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nMaxLength:I

    goto :goto_0

    .line 42
    :pswitch_2
    const v0, 0x7f0701df

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nErrMsgId:I

    .line 43
    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nMaxLength:I

    goto :goto_0

    .line 48
    :pswitch_3
    const v0, 0x7f070107

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nErrMsgId:I

    .line 49
    iput v3, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nMaxLength:I

    goto :goto_0

    .line 54
    :pswitch_4
    const v0, 0x7f07013f

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nErrMsgId:I

    .line 55
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nMaxLength:I

    goto :goto_0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/sheet/InputValueFilter;C)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/InputValueFilter;
    .param p1, "x1"    # C

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->isValidChar(C)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/sheet/InputValueFilter;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/InputValueFilter;

    .prologue
    .line 12
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nErrMsgId:I

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/sheet/InputValueFilter;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/InputValueFilter;
    .param p1, "x1"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->onToastMessage(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/sheet/InputValueFilter;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/InputValueFilter;

    .prologue
    .line 12
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nMaxLength:I

    return v0
.end method

.method private isValidChar(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 118
    const/16 v0, 0x30

    if-lt p1, v0, :cond_0

    const/16 v0, 0x39

    if-gt p1, v0, :cond_0

    .line 120
    const/4 v0, 0x1

    .line 122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onToastMessage(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 105
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->nErrMsgId:I

    if-nez v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->context:Landroid/content/Context;

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    .line 112
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 113
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_1
.end method


# virtual methods
.method public getFilters()[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/InputValueFilter;->filters:[Landroid/text/InputFilter;

    return-object v0
.end method
