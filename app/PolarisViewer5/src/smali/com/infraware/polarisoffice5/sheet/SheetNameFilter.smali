.class public Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;
.super Ljava/lang/Object;
.source "SheetNameFilter.java"


# static fields
.field private static final ERROR_INVALID_CHAR:I = 0x0

.field private static final ERROR_LENGTH:I = 0x1

.field private static final INPUT_FILTER_LENGTH:I = 0x2

.field public static MAX_SHEET_NAME_LENGTH:I


# instance fields
.field private filters:[Landroid/text/InputFilter;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/16 v0, 0x1f

    sput v0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->MAX_SHEET_NAME_LENGTH:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->mContext:Landroid/content/Context;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->filters:[Landroid/text/InputFilter;

    .line 30
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->filters:[Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter$1;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter$1;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;)V

    aput-object v2, v0, v1

    .line 69
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->filters:[Landroid/text/InputFilter;

    const/4 v1, 0x1

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    sget v3, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->MAX_SHEET_NAME_LENGTH:I

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;C)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;
    .param p1, "x1"    # C

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->isValid(C)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;C)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;
    .param p1, "x1"    # C

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->isCarriageReturn(C)Z

    move-result v0

    return v0
.end method

.method private isCarriageReturn(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 121
    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0xd

    if-ne p1, v0, :cond_1

    .line 123
    :cond_0
    const/4 v0, 0x1

    .line 125
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValid(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 101
    sparse-switch p1, :sswitch_data_0

    .line 115
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 113
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 101
    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x2a -> :sswitch_0
        0x2f -> :sswitch_0
        0x3a -> :sswitch_0
        0x3c -> :sswitch_0
        0x3e -> :sswitch_0
        0x3f -> :sswitch_0
        0x5b -> :sswitch_0
        0x5c -> :sswitch_0
        0x5d -> :sswitch_0
        0x7c -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public getFilters()[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->filters:[Landroid/text/InputFilter;

    return-object v0
.end method

.method protected onToastMessage(I)V
    .locals 7
    .param p1, "err"    # I

    .prologue
    const/4 v6, 0x0

    .line 73
    const/4 v0, 0x0

    .line 74
    .local v0, "errMsg":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 89
    const-string/jumbo v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Invalid Error Type : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07025e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 94
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->mContext:Landroid/content/Context;

    invoke-static {v3, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 95
    .local v2, "toast":Landroid/widget/Toast;
    const/16 v3, 0x11

    invoke-virtual {v2, v3, v6, v6}, Landroid/widget/Toast;->setGravity(III)V

    .line 96
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 97
    return-void

    .line 78
    .end local v2    # "toast":Landroid/widget/Toast;
    :pswitch_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0702cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 80
    goto :goto_0

    .line 83
    :pswitch_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07025d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "formattedString":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    sget v4, Lcom/infraware/polarisoffice5/sheet/SheetNameFilter;->MAX_SHEET_NAME_LENGTH:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 86
    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
