.class Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;
.super Landroid/os/Handler;
.source "SheetEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    .line 220
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 221
    .local v0, "bundle":Landroid/os/Bundle;
    iget v3, p1, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    .line 271
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->m_nErrMsgId:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$300(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;)I

    move-result v3

    if-lez v3, :cond_0

    .line 272
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget v4, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->onToastMessage(I)V
    invoke-static {v3, v4}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;I)V

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 223
    :sswitch_0
    const-string/jumbo v3, "SheetEditorActivity"

    const-string/jumbo v4, "messageHandler CloseDialog"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->searchResultCode:Ljava/lang/String;
    invoke-static {}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->access$200()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 225
    .local v2, "resultCode":I
    const/16 v3, 0x33

    if-ne v2, v3, :cond_0

    .line 226
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v3, v5}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->ReplaceText(I)V

    goto :goto_0

    .line 231
    .end local v2    # "resultCode":I
    :sswitch_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->CheckMenu()V

    goto :goto_0

    .line 237
    :sswitch_2
    const/4 v1, -0x1

    .line 238
    .local v1, "nEvent":I
    iget v3, p1, Landroid/os/Message;->arg1:I

    packed-switch v3, :pswitch_data_0

    .line 244
    :goto_1
    if-eq v1, v4, :cond_0

    .line 245
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v6, v1}, Lcom/infraware/office/evengine/EvInterface;->ISheetInsertCell(II)V

    goto :goto_0

    .line 239
    :pswitch_0
    const/4 v1, 0x0

    goto :goto_1

    .line 240
    :pswitch_1
    const/4 v1, 0x1

    goto :goto_1

    .line 241
    :pswitch_2
    const/4 v1, 0x2

    goto :goto_1

    .line 242
    :pswitch_3
    const/4 v1, 0x3

    goto :goto_1

    .line 250
    .end local v1    # "nEvent":I
    :sswitch_3
    const/4 v1, -0x1

    .line 251
    .restart local v1    # "nEvent":I
    iget v3, p1, Landroid/os/Message;->arg1:I

    packed-switch v3, :pswitch_data_1

    .line 257
    :goto_2
    if-eq v1, v4, :cond_0

    .line 258
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v5, v1}, Lcom/infraware/office/evengine/EvInterface;->ISheetInsertCell(II)V

    goto :goto_0

    .line 252
    :pswitch_4
    const/4 v1, 0x0

    goto :goto_2

    .line 253
    :pswitch_5
    const/4 v1, 0x1

    goto :goto_2

    .line 254
    :pswitch_6
    const/4 v1, 0x2

    goto :goto_2

    .line 255
    :pswitch_7
    const/4 v1, 0x3

    goto :goto_2

    .line 263
    .end local v1    # "nEvent":I
    :sswitch_4
    iget v3, p1, Landroid/os/Message;->arg1:I

    packed-switch v3, :pswitch_data_2

    goto :goto_0

    .line 264
    :pswitch_8
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ISheetClear(I)V

    goto :goto_0

    .line 265
    :pswitch_9
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v5}, Lcom/infraware/office/evengine/EvInterface;->ISheetClear(I)V

    goto :goto_0

    .line 266
    :pswitch_a
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity$3;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3, v6}, Lcom/infraware/office/evengine/EvInterface;->ISheetClear(I)V

    goto :goto_0

    .line 221
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xe -> :sswitch_2
        0x12 -> :sswitch_4
        0x13 -> :sswitch_3
        0x100 -> :sswitch_1
    .end sparse-switch

    .line 238
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 251
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 263
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
