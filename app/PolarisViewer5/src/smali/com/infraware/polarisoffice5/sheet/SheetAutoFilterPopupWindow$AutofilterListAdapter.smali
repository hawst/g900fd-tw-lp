.class Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SheetAutoFilterPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutofilterListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mHolder:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

.field private mLayoutId:I

.field private mSelectedIndex:I

.field private mibTouchListener:Landroid/view/View$OnTouchListener;

.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;Landroid/content/Context;ILjava/util/List;I)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "aResLayoutId"    # I
    .param p5, "nSelectedIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p4, "aList":Ljava/util/List;, "Ljava/util/List<Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    .line 267
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 319
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter$1;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mibTouchListener:Landroid/view/View$OnTouchListener;

    .line 268
    iput p3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mLayoutId:I

    .line 269
    iput p5, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mSelectedIndex:I

    .line 270
    return-void
.end method

.method private bindView(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;I)V
    .locals 2
    .param p1, "aHolder"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;
    .param p2, "aItem"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;
    .param p3, "aPosition"    # I

    .prologue
    .line 302
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mtvText:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mSelectedIndex:I

    if-ne v0, p3, :cond_0

    .line 304
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mibIcon:Landroid/widget/ImageButton;

    const v1, 0x7f020034

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 308
    :goto_0
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mrlContainer:Landroid/widget/RelativeLayout;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 309
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mibIcon:Landroid/widget/ImageButton;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 310
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mibIcon:Landroid/widget/ImageButton;

    const v1, 0x7f0b01df

    invoke-virtual {v0, v1, p1}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    .line 312
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mrlContainer:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mClick_filter:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 313
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mibIcon:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    iget-object v1, v1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;->mClick_filter:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mibIcon:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mibTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 317
    return-void

    .line 306
    :cond_0
    iget-object v0, p1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mibIcon:Landroid/widget/ImageButton;

    const v1, 0x7f020031

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method private buildHolder(ILcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;)Landroid/view/View;
    .locals 4
    .param p1, "aLayoutId"    # I
    .param p2, "aHolder"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 292
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 294
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f0b01dd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mrlContainer:Landroid/widget/RelativeLayout;

    .line 295
    const v2, 0x7f0b01df

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mibIcon:Landroid/widget/ImageButton;

    .line 296
    const v2, 0x7f0b01e0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;->mtvText:Landroid/widget/TextView;

    .line 298
    return-object v1
.end method


# virtual methods
.method public getSelectedItemIndex()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mSelectedIndex:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 274
    const/4 v1, 0x0

    .line 276
    .local v1, "localView":Landroid/view/View;
    if-nez p2, :cond_0

    .line 277
    new-instance v2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;

    invoke-direct {v2, v3}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow;)V

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mHolder:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

    .line 278
    iget v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mLayoutId:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mHolder:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

    invoke-direct {p0, v2, v3}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->buildHolder(ILcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;)Landroid/view/View;

    move-result-object v1

    .line 284
    :goto_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;

    .line 285
    .local v0, "autofilterItem":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mHolder:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

    invoke-direct {p0, v2, v0, p1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->bindView(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;I)V

    .line 286
    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mHolder:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 287
    return-object v1

    .line 281
    .end local v0    # "autofilterItem":Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterItem;
    :cond_0
    move-object v1, p2

    .line 282
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterListAdapter;->mHolder:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilterPopupWindow$AutofilterHolder;

    goto :goto_0
.end method
