.class public Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;
.super Ljava/lang/Object;
.source "SheetTabBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SheetItem"
.end annotation


# instance fields
.field public m_aszSheetName:Ljava/lang/String;

.field public m_bProtect:Z

.field public m_bSelected:Z

.field public m_nIndex:I

.field public m_oProtectButton:Landroid/widget/ImageView;

.field public m_oSheetButton:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/widget/LinearLayout;Landroid/widget/ImageView;IZ)V
    .locals 1
    .param p1, "a_aszSheetName"    # Ljava/lang/String;
    .param p2, "a_oSheetButton"    # Landroid/widget/LinearLayout;
    .param p3, "a_oProtectButton"    # Landroid/widget/ImageView;
    .param p4, "a_nIndex"    # I
    .param p5, "a_bProtect"    # Z

    .prologue
    .line 492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_aszSheetName:Ljava/lang/String;

    .line 494
    iput-object p2, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_oSheetButton:Landroid/widget/LinearLayout;

    .line 495
    iput-object p3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_oProtectButton:Landroid/widget/ImageView;

    .line 496
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bSelected:Z

    .line 497
    iput p4, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_nIndex:I

    .line 498
    iput-boolean p5, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$SheetItem;->m_bProtect:Z

    .line 499
    return-void
.end method
