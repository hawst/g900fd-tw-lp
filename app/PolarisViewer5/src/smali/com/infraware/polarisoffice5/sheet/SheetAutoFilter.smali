.class public Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "SheetAutoFilter.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$Holder;,
        Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;
    }
.end annotation


# instance fields
.field FILTER_ASC:I

.field FILTER_DES:I

.field private adapter_filter:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;

.field private arraylist_filter:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private list_filter:Landroid/widget/ListView;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field mHandler:Landroid/os/Handler;

.field private mSortByBtn:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

.field m_baCheckedItem:[Z

.field m_baFixedItem:[Z

.field m_nFocusedIndex:I

.field m_nTotalCount:I

.field m_sfilter_list_item:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 27
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mSortByBtn:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    .line 28
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->list_filter:Landroid/widget/ListView;

    .line 29
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->arraylist_filter:Ljava/util/ArrayList;

    .line 30
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->adapter_filter:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;

    .line 32
    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    .line 33
    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_nTotalCount:I

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_nFocusedIndex:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_ASC:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_DES:I

    .line 36
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$1;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mHandler:Landroid/os/Handler;

    .line 191
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->arraylist_filter:Ljava/util/ArrayList;

    return-object v0
.end method

.method private addActionBar()V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f0b001d

    const/4 v2, 0x1

    const/16 v3, 0x26

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f070134

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 73
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->isShow()Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 75
    :cond_1
    return-void
.end method


# virtual methods
.method public actionTitleBarButtonClick()V
    .locals 0

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->doButton()V

    .line 81
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->finish()V

    .line 82
    return-void
.end method

.method public doButton()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 86
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_baCheckedItem:[Z

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_ASC:I

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_nFocusedIndex:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    iget v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_ASC:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v4, v2}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_baCheckedItem:[Z

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_DES:I

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_nFocusedIndex:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    iget v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_DES:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v4, v2}, Lcom/infraware/office/evengine/EvInterface;->ISheetFilterCommand(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public makeListView()V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->makelistitem()V

    .line 109
    new-instance v0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->arraylist_filter:Ljava/util/ArrayList;

    invoke-direct {v0, p0, p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->adapter_filter:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;

    .line 110
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->list_filter:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->adapter_filter:Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$sheetFilterAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 112
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->list_filter:Landroid/widget/ListView;

    new-instance v1, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter$2;-><init>(Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 123
    return-void
.end method

.method public makelistitem()V
    .locals 5

    .prologue
    .line 126
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    array-length v0, v3

    .line 127
    .local v0, "count":I
    const/4 v2, 0x0

    .line 128
    .local v2, "nListCount":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->arraylist_filter:Ljava/util/ArrayList;

    .line 129
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 130
    iget v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_ASC:I

    if-ne v1, v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    aget-object v3, v3, v1

    const-string/jumbo v4, "Ascending"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_DES:I

    if-ne v1, v3, :cond_2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    aget-object v3, v3, v1

    const-string/jumbo v4, "Descending"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    .line 129
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 134
    :cond_2
    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->arraylist_filter:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v2, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 135
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 138
    :cond_3
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 196
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onBackPressed()V

    .line 197
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v1, 0x7f030043

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->setContentView(I)V

    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->addActionBar()V

    .line 55
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 57
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 59
    const-string/jumbo v1, "filter_count"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_nTotalCount:I

    .line 60
    const-string/jumbo v1, "filter_objArr"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_sfilter_list_item:[Ljava/lang/String;

    .line 61
    const-string/jumbo v1, "filter_focusedIndex"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_nFocusedIndex:I

    .line 62
    const-string/jumbo v1, "filter_fixedItem"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_baFixedItem:[Z

    .line 63
    const-string/jumbo v1, "filter_checkedItem"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_baCheckedItem:[Z

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->startInit()V

    .line 66
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->makeListView()V

    .line 67
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 2
    .param p1, "nLocale"    # I

    .prologue
    .line 201
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f070134

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 202
    return-void
.end method

.method public startInit()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 93
    const v0, 0x7f0b01d8

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->list_filter:Landroid/widget/ListView;

    .line 95
    const v0, 0x7f0b01da

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mSortByBtn:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    .line 96
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mSortByBtn:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    const/4 v1, 0x2

    const v2, 0x7f040005

    invoke-virtual {v0, v1, v4, v2}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->initImage(IZI)V

    .line 97
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mSortByBtn:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->addHandler(Landroid/os/Handler;I)V

    .line 99
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_baCheckedItem:[Z

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_ASC:I

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mSortByBtn:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setSelection(I)V

    .line 105
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->m_baCheckedItem:[Z

    iget v1, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->FILTER_DES:I

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mSortByBtn:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->setSelection(I)V

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetAutoFilter;->mSortByBtn:Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/kit/PanelButtonImage;->clearSelection()V

    goto :goto_0
.end method
