.class Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;
.super Ljava/lang/Object;
.source "SheetTabBar.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/sheet/SheetTabBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 8
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Z

    .prologue
    const/4 v4, 0x0

    .line 452
    if-nez p2, :cond_0

    .line 454
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v7, v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->currentSheetTabEdit:Landroid/widget/EditText;

    .line 455
    .local v7, "edt_sheet_tap_rename":Landroid/widget/EditText;
    invoke-virtual {v7}, Landroid/widget/EditText;->clearFocus()V

    .line 456
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 457
    .local v2, "strRename":Ljava/lang/String;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x3

    iget-object v3, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget v3, v3, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_nCurrentSheetPosition:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISheetEdit(ILjava/lang/String;IIII)V

    .line 459
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v7}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 460
    iget-object v0, p0, Lcom/infraware/polarisoffice5/sheet/SheetTabBar$9;->this$0:Lcom/infraware/polarisoffice5/sheet/SheetTabBar;

    # getter for: Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->m_oActivity:Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/sheet/SheetTabBar;->access$400(Lcom/infraware/polarisoffice5/sheet/SheetTabBar;)Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/sheet/SheetEditorActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v4}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    .line 464
    .end local v2    # "strRename":Ljava/lang/String;
    .end local v7    # "edt_sheet_tap_rename":Landroid/widget/EditText;
    :cond_0
    return-void
.end method
