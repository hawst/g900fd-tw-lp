.class public Lcom/infraware/polarisoffice5/MyApplication;
.super Landroid/app/Application;
.source "MyApplication.java"


# static fields
.field public static m_oApplication:Lcom/infraware/polarisoffice5/MyApplication;

.field public static m_oCurrentActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

.field public static m_oCurrentTextEditor:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

.field public static m_oSlideShow:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    sput-object v0, Lcom/infraware/polarisoffice5/MyApplication;->m_oApplication:Lcom/infraware/polarisoffice5/MyApplication;

    .line 19
    sput-object v0, Lcom/infraware/polarisoffice5/MyApplication;->m_oCurrentActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 20
    sput-object v0, Lcom/infraware/polarisoffice5/MyApplication;->m_oCurrentTextEditor:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 21
    sput-object v0, Lcom/infraware/polarisoffice5/MyApplication;->m_oSlideShow:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 60
    sput-object p0, Lcom/infraware/polarisoffice5/MyApplication;->m_oApplication:Lcom/infraware/polarisoffice5/MyApplication;

    .line 61
    return-void
.end method

.method public static clearApplicationCache(Landroid/content/Context;Ljava/io/File;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dir"    # Ljava/io/File;

    .prologue
    .line 39
    if-nez p1, :cond_0

    .line 40
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p1

    .line 42
    :cond_0
    if-nez p1, :cond_2

    .line 55
    :cond_1
    :goto_0
    return-void

    .line 45
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 47
    .local v0, "children":[Ljava/io/File;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 49
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 50
    aget-object v2, v0, v1

    invoke-static {p0, v2}, Lcom/infraware/polarisoffice5/MyApplication;->clearApplicationCache(Landroid/content/Context;Ljava/io/File;)V

    .line 47
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 52
    :cond_3
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 54
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static getInstance()Lcom/infraware/polarisoffice5/MyApplication;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/infraware/polarisoffice5/MyApplication;->m_oApplication:Lcom/infraware/polarisoffice5/MyApplication;

    return-object v0
.end method


# virtual methods
.method public getCurrentViewer()Lcom/infraware/office/baseframe/EvBaseViewerActivity;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/infraware/polarisoffice5/MyApplication;->m_oCurrentActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    return-object v0
.end method

.method public getSlideShow()Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/infraware/polarisoffice5/MyApplication;->m_oSlideShow:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    return-object v0
.end method

.method public getTextEditor()Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/infraware/polarisoffice5/MyApplication;->m_oCurrentTextEditor:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0}, Lcom/infraware/common/notice/NoticeNotifyManager;->setNoticeUrl(Landroid/content/Context;)V

    .line 28
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 29
    return-void
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 35
    return-void
.end method

.method public setCurrentSlideShow(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0
    .param p1, "a_oSlideShow"    # Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .prologue
    .line 76
    sput-object p1, Lcom/infraware/polarisoffice5/MyApplication;->m_oSlideShow:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    .line 77
    return-void
.end method

.method public setCurrentTextEditor(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0
    .param p1, "a_oEditor"    # Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .prologue
    .line 72
    sput-object p1, Lcom/infraware/polarisoffice5/MyApplication;->m_oCurrentTextEditor:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    .line 73
    return-void
.end method

.method public setCurrentViewer(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V
    .locals 0
    .param p1, "a_oViewer"    # Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .prologue
    .line 68
    sput-object p1, Lcom/infraware/polarisoffice5/MyApplication;->m_oCurrentActivity:Lcom/infraware/office/baseframe/EvBaseViewerActivity;

    .line 69
    return-void
.end method
