.class Lcom/infraware/polarisoffice5/OfficeLauncherActivity$5;
.super Ljava/lang/Object;
.source "OfficeLauncherActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->checkMultipleLaunch(Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

.field final synthetic val$oEditor:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;)V
    .locals 0

    .prologue
    .line 713
    iput-object p1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$5;->this$0:Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$5;->val$oEditor:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 716
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$5;->val$oEditor:Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$5;->this$0:Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

    # getter for: Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->m_newOpenHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->access$100(Lcom/infraware/polarisoffice5/OfficeLauncherActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/text/main/TextEditorActivity;->fisnishbyMultipleLauncher(Landroid/os/Handler;)V

    .line 718
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeLauncherActivity$5;->this$0:Lcom/infraware/polarisoffice5/OfficeLauncherActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/OfficeLauncherActivity;->closeLauncherWithIntent()V

    .line 720
    return-void
.end method
