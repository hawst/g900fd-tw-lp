.class public Lcom/infraware/polarisoffice5/OfficeProductInfo;
.super Landroid/app/Activity;
.source "OfficeProductInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private m_isPause:Z

.field private m_ivLogo:Landroid/widget/ImageView;

.field private m_layoutLogo:Landroid/widget/LinearLayout;

.field private m_layoutTitle:Landroid/widget/LinearLayout;

.field private m_nDownKey:I

.field private m_nLocaleCode:I

.field private m_nOrientation:I

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;

.field private m_oToastMsg:Landroid/widget/Toast;

.field private m_scrollInfo:Landroid/widget/ScrollView;

.field private m_strVersion:Ljava/lang/String;

.field private m_tvTitle:Landroid/widget/TextView;

.field private m_tvVersion:Landroid/widget/TextView;

.field private m_tv_about_info:Landroid/widget/TextView;

.field private m_tv_about_info2:Landroid/widget/TextView;

.field onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nOrientation:I

    .line 33
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nLocaleCode:I

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nDownKey:I

    .line 47
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oToastMsg:Landroid/widget/Toast;

    .line 48
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_isPause:Z

    .line 51
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;

    .line 52
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 55
    iput-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;

    .line 295
    new-instance v0, Lcom/infraware/polarisoffice5/OfficeProductInfo$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo$2;-><init>(Lcom/infraware/polarisoffice5/OfficeProductInfo;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 301
    new-instance v0, Lcom/infraware/polarisoffice5/OfficeProductInfo$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo$3;-><init>(Lcom/infraware/polarisoffice5/OfficeProductInfo;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->onNetWorkUseAlarmClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeProductInfo;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_isPause:Z

    return v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeProductInfo;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/OfficeProductInfo;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeProductInfo;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$202(Lcom/infraware/polarisoffice5/OfficeProductInfo;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/OfficeProductInfo;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->mCheckBox:Landroid/widget/CheckBox;

    return-object p1
.end method


# virtual methods
.method public connectInfraHome()V
    .locals 4

    .prologue
    .line 279
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 280
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->startActivity(Landroid/content/Intent;)V

    .line 281
    return-void
.end method

.method public onCheckboxClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 285
    move-object v1, p1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 288
    .local v0, "checked":Z
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 293
    :goto_0
    return-void

    .line 290
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b003c
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 141
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->onOrientationChanged()V

    .line 144
    :cond_0
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 145
    .local v0, "nlocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nLocaleCode:I

    if-eq v1, v0, :cond_1

    .line 147
    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nLocaleCode:I

    .line 148
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tvTitle:Landroid/widget/TextView;

    const v2, 0x7f0700c0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 149
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tv_about_info:Landroid/widget/TextView;

    const v2, 0x7f0702b7

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 150
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tv_about_info2:Landroid/widget/TextView;

    const v2, 0x7f0702b8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 153
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 154
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 66
    :cond_0
    const v0, 0x7f03003a

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->setContentView(I)V

    .line 69
    new-instance v0, Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/OfficeProductInfo;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;

    .line 70
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "version "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f070322

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_strVersion:Ljava/lang/String;

    .line 76
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nLocaleCode:I

    .line 77
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->onOrientationChanged()V

    .line 78
    iput-object p0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->mActivity:Landroid/app/Activity;

    .line 79
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 103
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 107
    iput p1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nDownKey:I

    .line 108
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v2, -0x1

    .line 115
    const/16 v1, 0x6f

    if-ne p1, v1, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->finish()V

    .line 119
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nDownKey:I

    if-eq v1, p1, :cond_1

    .line 120
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nDownKey:I

    .line 135
    :goto_0
    return v0

    .line 124
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 134
    :sswitch_0
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nDownKey:I

    .line 135
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 130
    :sswitch_1
    iput v2, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nDownKey:I

    goto :goto_0

    .line 124
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0x54 -> :sswitch_1
    .end sparse-switch
.end method

.method public onOrientationChanged()V
    .locals 7

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    iput v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nOrientation:I

    .line 159
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v5

    if-nez v5, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->getWindow()Landroid/view/Window;

    move-result-object v5

    iget v6, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nOrientation:I

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 164
    :cond_0
    const v5, 0x7f0b0031

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_layoutTitle:Landroid/widget/LinearLayout;

    .line 165
    const v5, 0x7f0b0034

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tvTitle:Landroid/widget/TextView;

    .line 166
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tvTitle:Landroid/widget/TextView;

    const v6, 0x7f0700ac

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 168
    const v5, 0x7f0b01b7

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ScrollView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_scrollInfo:Landroid/widget/ScrollView;

    .line 170
    const v5, 0x7f0b01b4

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_layoutLogo:Landroid/widget/LinearLayout;

    .line 172
    const v5, 0x7f0b01b6

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tvVersion:Landroid/widget/TextView;

    .line 173
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tvVersion:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_strVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    const v5, 0x7f0b01b8

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tv_about_info:Landroid/widget/TextView;

    .line 176
    const v5, 0x7f0b01b9

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tv_about_info2:Landroid/widget/TextView;

    .line 178
    const v5, 0x7f0b01ba

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_ivLogo:Landroid/widget/ImageView;

    .line 179
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_ivLogo:Landroid/widget/ImageView;

    new-instance v6, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;

    invoke-direct {v6, p0}, Lcom/infraware/polarisoffice5/OfficeProductInfo$1;-><init>(Lcom/infraware/polarisoffice5/OfficeProductInfo;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 216
    .local v1, "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v5, 0x41500000    # 13.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v5

    float-to-int v5, v5

    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 217
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_tvTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 219
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 220
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_layoutLogo:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 221
    .local v2, "mainLogoParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_ivLogo:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 222
    .local v4, "subLogoParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_scrollInfo:Landroid/widget/ScrollView;

    invoke-virtual {v5}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 224
    .local v3, "scrollParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_nOrientation:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 226
    const/high16 v5, 0x42400000    # 48.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 227
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 229
    const/high16 v5, 0x41f40000    # 30.5f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 230
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_layoutLogo:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 232
    const/high16 v5, 0x42100000    # 36.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 233
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_ivLogo:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 235
    const/high16 v5, 0x42280000    # 42.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 236
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_scrollInfo:Landroid/widget/ScrollView;

    invoke-virtual {v5, v3}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 253
    :goto_0
    return-void

    .line 240
    :cond_1
    const/high16 v5, 0x42200000    # 40.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 241
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_layoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    const/high16 v5, 0x41cc0000    # 25.5f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 244
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_layoutLogo:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 246
    const/high16 v5, 0x41a80000    # 21.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 247
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_ivLogo:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 249
    const/high16 v5, 0x41940000    # 18.5f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 250
    iget-object v5, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_scrollInfo:Landroid/widget/ScrollView;

    invoke-virtual {v5, v3}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_isPause:Z

    .line 85
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 86
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_isPause:Z

    .line 92
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 93
    return-void
.end method

.method public onToastMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 258
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oToastMsg:Landroid/widget/Toast;

    .line 262
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 263
    return-void

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo;->m_oToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
