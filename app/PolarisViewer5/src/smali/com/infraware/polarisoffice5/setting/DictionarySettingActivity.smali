.class public Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;
.super Landroid/app/Activity;
.source "DictionarySettingActivity.java"


# static fields
.field public static final DICTIONARY_SETTING_CHINESE:I = 0x3

.field public static final DICTIONARY_SETTING_ENGLISH:I = 0x2

.field public static final DICTIONARY_SETTING_JAPANESE:I = 0x4

.field public static final DICTIONARY_SETTING_KOREAN:I = 0x1


# instance fields
.field private m_ChnList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private m_Context:Landroid/content/Context;

.field private m_CurrentPopupIndex:I

.field private m_DictionaryDialog:Landroid/app/AlertDialog;

.field private m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

.field private m_EngList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private m_JpnList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private m_KorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/common/DictionaryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private m_LayoutTitle:Landroid/widget/LinearLayout;

.field private m_TvTitle:Landroid/widget/TextView;

.field private m_lvSettingList:Landroid/widget/ListView;

.field private m_nLocaleType:I

.field private m_nOrientation:I

.field private m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;

.field private m_oSettingClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private m_oSettingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/DictSettingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_KorList:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_EngList:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_ChnList:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_JpnList:Ljava/util/ArrayList;

    .line 39
    iput v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    .line 45
    iput-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 48
    iput v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nLocaleType:I

    .line 49
    iput v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nOrientation:I

    .line 51
    iput-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionaryDialog:Landroid/app/AlertDialog;

    .line 237
    new-instance v0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$1;-><init>(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;)Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->showDictionaryDialog(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->setSummary(I)V

    return-void
.end method

.method private getDBList()V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method private onOrientationChanged()V
    .locals 4

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nOrientation:I

    .line 104
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v2

    if-nez v2, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget v3, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nOrientation:I

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_TvTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 110
    .local v1, "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v2, 0x41500000    # 13.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 111
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_TvTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_LayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 114
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 116
    const/high16 v2, 0x42400000    # 48.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 117
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_LayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    :goto_0
    return-void

    .line 121
    :cond_1
    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 122
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_LayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setSettingList()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 128
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->readDictionaryDB()V

    .line 129
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDicInfo()Ljava/util/ArrayList;

    move-result-object v0

    .line 130
    .local v0, "dicInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/common/DictionaryInfo;>;"
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 132
    const/4 v1, 0x0

    .line 134
    .local v1, "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    new-instance v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    .end local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    invoke-direct {v1}, Lcom/infraware/polarisoffice5/setting/DictSettingItem;-><init>()V

    .line 135
    .restart local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_IsTitle:Z

    .line 136
    const v2, 0x7f070089

    iput v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nTitleId:I

    .line 137
    iput-boolean v5, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 138
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    .end local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    invoke-direct {v1}, Lcom/infraware/polarisoffice5/setting/DictSettingItem;-><init>()V

    .line 142
    .restart local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    iput v4, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nId:I

    .line 143
    const v2, 0x7f070087

    iput v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nTitleId:I

    .line 144
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-nez v2, :cond_8

    .line 146
    :cond_0
    iput-boolean v5, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 147
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070088

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    .line 163
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    new-instance v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    .end local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    invoke-direct {v1}, Lcom/infraware/polarisoffice5/setting/DictSettingItem;-><init>()V

    .line 167
    .restart local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    iput v6, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nId:I

    .line 168
    const v2, 0x7f07007d

    iput v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nTitleId:I

    .line 169
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_c

    .line 171
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-nez v2, :cond_2

    .line 172
    iput-boolean v5, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 173
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070088

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    .line 187
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    new-instance v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    .end local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    invoke-direct {v1}, Lcom/infraware/polarisoffice5/setting/DictSettingItem;-><init>()V

    .line 191
    .restart local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    iput v7, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nId:I

    .line 192
    const v2, 0x7f070078

    iput v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nTitleId:I

    .line 193
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-nez v2, :cond_10

    .line 195
    :cond_4
    iput-boolean v5, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 196
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070088

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    .line 210
    :cond_5
    :goto_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    new-instance v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    .end local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    invoke-direct {v1}, Lcom/infraware/polarisoffice5/setting/DictSettingItem;-><init>()V

    .line 214
    .restart local v1    # "settingItem":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    iput v8, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nId:I

    .line 215
    const v2, 0x7f070082

    iput v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nTitleId:I

    .line 216
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicType:I

    if-nez v2, :cond_14

    .line 218
    :cond_6
    iput-boolean v5, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 219
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070088

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    .line 233
    :cond_7
    :goto_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->setList(Ljava/util/ArrayList;)V

    .line 235
    return-void

    .line 151
    :cond_8
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 154
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 155
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070085

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 156
    :cond_9
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 157
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070083

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 158
    :cond_a
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 159
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 160
    :cond_b
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 161
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070084

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 177
    :cond_c
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 178
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 179
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_1

    .line 180
    :cond_d
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 181
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_1

    .line 182
    :cond_e
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 183
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07007c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_1

    .line 184
    :cond_f
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 185
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07007a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_1

    .line 200
    :cond_10
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 201
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 202
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07008d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_2

    .line 203
    :cond_11
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 204
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07008b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_2

    .line 205
    :cond_12
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 206
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07008e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_2

    .line 207
    :cond_13
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 208
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07008c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_2

    .line 223
    :cond_14
    iput-boolean v4, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    .line 224
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 225
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070080

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_3

    .line 226
    :cond_15
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 227
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07007e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_3

    .line 228
    :cond_16
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 229
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070081

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_3

    .line 230
    :cond_17
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 231
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07007f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_3
.end method

.method private setSummary(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 310
    iget v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    if-nez v0, :cond_4

    .line 312
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_KorList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070085

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    .line 362
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->notifyDataSetChanged()V

    .line 363
    return-void

    .line 314
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_KorList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070083

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto :goto_0

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_KorList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 317
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070086

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto :goto_0

    .line 318
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_KorList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 323
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    if-ne v0, v1, :cond_8

    .line 325
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_EngList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 326
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 327
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_EngList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 328
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 329
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_EngList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 330
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 331
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_EngList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 336
    :cond_8
    iget v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    if-ne v0, v2, :cond_c

    .line 338
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_ChnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 339
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 340
    :cond_9
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_ChnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 341
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 342
    :cond_a
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_ChnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 343
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 344
    :cond_b
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_ChnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 349
    :cond_c
    iget v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    if-ne v0, v3, :cond_0

    .line 351
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_JpnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 352
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070080

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 353
    :cond_d
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_JpnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 354
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07007e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 355
    :cond_e
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_JpnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 356
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070081

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0

    .line 357
    :cond_f
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_JpnList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicWhat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private showDictionaryDialog(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    const/4 v7, 0x0

    .line 255
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 256
    .local v2, "dicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_Context:Landroid/content/Context;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v5, 0x7f07029f

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 259
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    .line 291
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    .line 293
    .local v1, "cs":[Ljava/lang/CharSequence;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 294
    new-instance v4, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$2;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$2;-><init>(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 302
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionaryDialog:Landroid/app/AlertDialog;

    .line 303
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 304
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 305
    return-void

    .line 262
    .end local v1    # "cs":[Ljava/lang/CharSequence;
    :pswitch_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_KorList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 264
    iput v7, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    .line 265
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_KorList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 269
    .end local v3    # "i":I
    :pswitch_1
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_EngList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 271
    const/4 v4, 0x1

    iput v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    .line 272
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_EngList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 276
    .end local v3    # "i":I
    :pswitch_2
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_ChnList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 278
    const/4 v4, 0x2

    iput v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    .line 279
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_ChnList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 283
    .end local v3    # "i":I
    :pswitch_3
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_JpnList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 285
    const/4 v4, 0x3

    iput v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    .line 286
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_JpnList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/common/DictionaryInfo;->dicName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 259
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 376
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 377
    .local v0, "nlocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nLocaleType:I

    if-eq v1, v0, :cond_0

    .line 378
    iput v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nLocaleType:I

    .line 380
    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nLocaleType:I

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->onLocaleChange(I)V

    .line 383
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_1

    .line 384
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->onOrientationChanged()V

    .line 386
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 387
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nLocaleType:I

    .line 59
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_nOrientation:I

    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 67
    :cond_0
    const v0, 0x7f03000d

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->setContentView(I)V

    .line 69
    const v0, 0x7f0b0031

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_LayoutTitle:Landroid/widget/LinearLayout;

    .line 70
    const v0, 0x7f0b0034

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_TvTitle:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_TvTitle:Landroid/widget/TextView;

    const v1, 0x7f070089

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 73
    iput-object p0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_Context:Landroid/content/Context;

    .line 74
    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 75
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setContext(Landroid/content/Context;)V

    .line 77
    const v0, 0x7f0b0043

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    .line 78
    new-instance v0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;

    .line 80
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 82
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 83
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 85
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDicListNSortFromDiotek()V

    .line 86
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->initDB()V

    .line 88
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->onOrientationChanged()V

    .line 89
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 2
    .param p1, "nLocale"    # I

    .prologue
    .line 390
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getDicListNSortFromDiotek()V

    .line 391
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getDBList()V

    .line 395
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_TvTitle:Landroid/widget/TextView;

    const v1, 0x7f070089

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 397
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionaryDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_DictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 400
    iget v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_CurrentPopupIndex:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->showDictionaryDialog(I)V

    .line 402
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->getDBList()V

    .line 98
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 99
    return-void
.end method
