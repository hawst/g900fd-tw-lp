.class Lcom/infraware/polarisoffice5/setting/SettingActivity$15;
.super Ljava/lang/Object;
.source "SettingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0

    .prologue
    .line 922
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$15;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 924
    invoke-static {}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$15;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-virtual {v1, v2}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->deleteAll(Landroid/content/Context;)V

    .line 925
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$15;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$500(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->getItemById(I)Lcom/infraware/polarisoffice5/setting/SettingItem;

    move-result-object v0

    .line 926
    .local v0, "item":Lcom/infraware/polarisoffice5/setting/SettingItem;
    if-eqz v0, :cond_0

    .line 927
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 929
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$15;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$500(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->notifyDataSetChanged()V

    .line 930
    return-void
.end method
