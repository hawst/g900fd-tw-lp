.class Lcom/infraware/polarisoffice5/setting/SettingActivity$7;
.super Ljava/lang/Object;
.source "SettingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0

    .prologue
    .line 475
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$7;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 478
    packed-switch p2, :pswitch_data_0

    .line 488
    :goto_0
    return-void

    .line 481
    :pswitch_0
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$7;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x12f

    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$7;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1500(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    .line 482
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$7;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->onOpenSource()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1900(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto :goto_0

    .line 478
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
