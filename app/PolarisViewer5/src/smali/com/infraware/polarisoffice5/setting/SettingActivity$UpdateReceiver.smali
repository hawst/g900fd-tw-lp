.class public Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UpdateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0

    .prologue
    .line 1272
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x20

    .line 1277
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1279
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_a

    .line 1281
    const-string/jumbo v2, "SETTING_ACTIVITY"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "BroadCast Intent Action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1282
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_DOWNLOAD_COMPLETE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1283
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1284
    .local v1, "bundle":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const-string/jumbo v3, "APKFileNameForInstall"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strFileName:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$3002(Lcom/infraware/polarisoffice5/setting/SettingActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1285
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    .line 1318
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 1286
    :cond_1
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_LATEST_VERSION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1287
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-virtual {v2, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    goto :goto_0

    .line 1288
    :cond_2
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_DISABLE_CONFIRM_VERSION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1289
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    goto :goto_0

    .line 1290
    :cond_3
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_CONNECTION_ERROR"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1291
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    goto :goto_0

    .line 1292
    :cond_4
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1293
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v3, 0x22

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    goto :goto_0

    .line 1294
    :cond_5
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_FILE_WRITE_ERROR"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1295
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v3, 0x23

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    goto :goto_0

    .line 1296
    :cond_6
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_UPDATE_AVAILABE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1298
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1299
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1301
    :cond_7
    const-string/jumbo v2, "NEW_VERSION"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1303
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloadAccept:Z
    invoke-static {v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2900(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1304
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v3, 0x384

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1200(Lcom/infraware/polarisoffice5/setting/SettingActivity;I)V

    goto/16 :goto_0

    .line 1307
    :cond_8
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const-string/jumbo v3, "NEW_VERSION"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strNewVersion:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$3102(Lcom/infraware/polarisoffice5/setting/SettingActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1308
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v3, 0x3e8

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1200(Lcom/infraware/polarisoffice5/setting/SettingActivity;I)V

    goto/16 :goto_0

    .line 1312
    :cond_9
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-virtual {v2, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    goto/16 :goto_0

    .line 1317
    :cond_a
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-virtual {v2, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    goto/16 :goto_0
.end method
