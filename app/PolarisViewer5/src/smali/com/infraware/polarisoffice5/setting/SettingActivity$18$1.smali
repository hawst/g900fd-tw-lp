.class Lcom/infraware/polarisoffice5/setting/SettingActivity$18$1;
.super Ljava/lang/Object;
.source "SettingActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity$18;)V
    .locals 0

    .prologue
    .line 1007
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$1;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1011
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$1;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/update/UpdateService;

    move-result-object v5

    if-nez v5, :cond_0

    .line 1019
    :goto_0
    return-void

    .line 1014
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$1;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/update/UpdateService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/common/update/UpdateService;->getAPKCurMByte()D

    move-result-wide v0

    .line 1015
    .local v0, "nCurMB":D
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$1;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/update/UpdateService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/common/update/UpdateService;->getAPKTotalMByte()D

    move-result-wide v2

    .line 1017
    .local v2, "nTotalMB":D
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$1;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0b01a8

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1018
    .local v4, "txtProgressCount":Landroid/widget/TextView;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "MB / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "MB"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
