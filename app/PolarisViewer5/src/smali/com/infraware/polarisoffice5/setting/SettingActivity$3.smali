.class Lcom/infraware/polarisoffice5/setting/SettingActivity$3;
.super Ljava/lang/Object;
.source "SettingActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 11
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 280
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$700(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .line 283
    .local v2, "item":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bPause:Z
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget v5, v2, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    const/16 v6, 0x15

    if-eq v5, v6, :cond_0

    iget v5, v2, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    const/16 v6, 0x16

    if-eq v5, v6, :cond_0

    iget v5, v2, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    const/16 v6, 0x17

    if-ne v5, v6, :cond_1

    .line 419
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    iget-boolean v5, v2, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    if-eqz v5, :cond_0

    .line 289
    iget v5, v2, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    packed-switch v5, :pswitch_data_0

    .line 418
    :goto_1
    :pswitch_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$500(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 292
    :pswitch_1
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    .line 293
    .local v0, "formatString":[Ljava/lang/String;
    const/4 v5, 0x0

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const v7, 0x7f070003

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    .line 294
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const v7, 0x7f070004

    invoke-virtual {v6, v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    .line 296
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v8

    invoke-direct {v6, v7, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v7, 0x7f0702a1

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/infraware/polarisoffice5/setting/SettingActivity$3$1;

    invoke-direct {v7, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$3$1;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity$3;)V

    invoke-virtual {v6, v0, v7}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mFormatDialog:Landroid/app/AlertDialog;
    invoke-static {v5, v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$902(Lcom/infraware/polarisoffice5/setting/SettingActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 306
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mFormatDialog:Landroid/app/AlertDialog;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$900(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 307
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mFormatDialog:Landroid/app/AlertDialog;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$900(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 310
    .end local v0    # "formatString":[Ljava/lang/String;
    :pswitch_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->setCreateBackup()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$100(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto :goto_1

    .line 313
    :pswitch_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->setDisplayExt()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto :goto_1

    .line 316
    :pswitch_4
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->setAutoFit()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto :goto_1

    .line 319
    :pswitch_5
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v6, 0x2bc

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V
    invoke-static {v5, v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1200(Lcom/infraware/polarisoffice5/setting/SettingActivity;I)V

    goto :goto_1

    .line 322
    :pswitch_6
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->onSetDictionary()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto :goto_1

    .line 325
    :pswitch_7
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    const/16 v6, 0x12d

    invoke-static {v5, v6}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 328
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 329
    .local v1, "inflater1":Landroid/view/LayoutInflater;
    const v5, 0x7f030006

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 331
    .local v3, "networkView":Landroid/view/View;
    const v5, 0x7f0b003b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 332
    .local v4, "tv":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const v5, 0x7f0b003c

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v6, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1502(Lcom/infraware/polarisoffice5/setting/SettingActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 333
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 334
    const v5, 0x7f07005d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 335
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v10}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmUpdateClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 340
    :cond_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 341
    const v5, 0x7f07005b

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 342
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v10}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmUpdateClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 346
    :cond_3
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 350
    .end local v1    # "inflater1":Landroid/view/LayoutInflater;
    .end local v3    # "networkView":Landroid/view/View;
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_4
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->settingUpdate()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1600(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto/16 :goto_1

    .line 354
    :pswitch_8
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAbout()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1700(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto/16 :goto_1

    .line 357
    :pswitch_9
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    const/16 v6, 0x12e

    invoke-static {v5, v6}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_7

    .line 360
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 361
    .restart local v1    # "inflater1":Landroid/view/LayoutInflater;
    const v5, 0x7f030006

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 363
    .restart local v3    # "networkView":Landroid/view/View;
    const v5, 0x7f0b003b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 364
    .restart local v4    # "tv":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const v5, 0x7f0b003c

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v6, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1502(Lcom/infraware/polarisoffice5/setting/SettingActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 365
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 366
    const v5, 0x7f07005d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 367
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v10}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmEulaClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 372
    :cond_5
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 373
    const v5, 0x7f07005b

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 374
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v10}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmEulaClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 378
    :cond_6
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 382
    .end local v1    # "inflater1":Landroid/view/LayoutInflater;
    .end local v3    # "networkView":Landroid/view/View;
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_7
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->onEULA()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto/16 :goto_1

    .line 386
    :pswitch_a
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    const/16 v6, 0x12f

    invoke-static {v5, v6}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_a

    .line 389
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 390
    .restart local v1    # "inflater1":Landroid/view/LayoutInflater;
    const v5, 0x7f030006

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 392
    .restart local v3    # "networkView":Landroid/view/View;
    const v5, 0x7f0b003b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 393
    .restart local v4    # "tv":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const v5, 0x7f0b003c

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v6, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1502(Lcom/infraware/polarisoffice5/setting/SettingActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 394
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_8

    .line 395
    const v5, 0x7f07005d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 396
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v10}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmCopyRightsClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 401
    :cond_8
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 402
    const v5, 0x7f07005b

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 403
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v10}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmCopyRightsClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v10, v10, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 407
    :cond_9
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;

    move-result-object v6

    sget-object v7, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7, v8}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 411
    .end local v1    # "inflater1":Landroid/view/LayoutInflater;
    .end local v3    # "networkView":Landroid/view/View;
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_a
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->onOpenSource()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$1900(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto/16 :goto_1

    .line 415
    :pswitch_b
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->onDictionary()V
    invoke-static {v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto/16 :goto_1

    .line 289
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_b
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
