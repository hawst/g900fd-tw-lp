.class public Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;
.super Landroid/widget/BaseAdapter;
.source "SetDictionaryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$1;,
        Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private colorSummary:Landroid/content/res/ColorStateList;

.field private colorTitle:Landroid/content/res/ColorStateList;

.field private m_oContext:Landroid/content/Context;

.field private m_oInflater:Landroid/view/LayoutInflater;

.field private m_oItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/SettingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    .line 39
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 46
    if-eqz p1, :cond_0

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    .line 52
    :cond_0
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oContext:Landroid/content/Context;

    .line 53
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    :cond_0
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 86
    :cond_0
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemById(I)Lcom/infraware/polarisoffice5/setting/SettingItem;
    .locals 4
    .param p1, "nId"    # I

    .prologue
    const/4 v2, 0x0

    .line 56
    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object v1, v2

    .line 66
    :cond_1
    :goto_0
    return-object v1

    .line 59
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 61
    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .line 62
    .local v1, "item":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iget v3, v1, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    if-eq v3, p1, :cond_1

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1    # "item":Lcom/infraware/polarisoffice5/setting/SettingItem;
    :cond_3
    move-object v1, v2

    .line 66
    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 93
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 100
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f03000e

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 102
    new-instance v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;

    invoke-direct {v2, p0, v7}, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;-><init>(Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$1;)V

    .line 103
    .local v2, "holder":Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;
    const v5, 0x7f0b0044

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    .line 104
    const v5, 0x7f0b0047

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    .line 105
    const v5, 0x7f0b0048

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    .line 106
    const v5, 0x7f0b004a

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    .line 107
    const v5, 0x7f0b0045

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    .line 108
    const v5, 0x7f0b004b

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    iput-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    .line 110
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 111
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .line 113
    .local v4, "setting":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iget-boolean v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_IsTitle:Z

    if-eqz v5, :cond_0

    .line 115
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    iget v6, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 118
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 121
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 122
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 124
    invoke-virtual {p2, v9}, Landroid/view/View;->setClickable(Z)V

    .line 126
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oContext:Landroid/content/Context;

    const v6, 0x41caa3d7    # 25.33f

    invoke-static {v5, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    int-to-float v1, v5

    .line 127
    .local v1, "height":F
    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    const/4 v5, -0x1

    float-to-int v6, v1

    invoke-direct {v3, v5, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 128
    .local v3, "layoutParam":Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {p2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    .end local v1    # "height":F
    .end local v3    # "layoutParam":Landroid/widget/AbsListView$LayoutParams;
    :goto_0
    return-object p2

    .line 132
    :cond_0
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 134
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 135
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 137
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    .line 138
    .local v0, "config":Lcom/infraware/common/config/RuntimeConfig;
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget v6, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 140
    iget v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    packed-switch v5, :pswitch_data_0

    .line 156
    :goto_1
    iget v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    sparse-switch v5, :sswitch_data_0

    .line 199
    :goto_2
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    iget v6, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nCheckShow:I

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 201
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget-boolean v6, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 202
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    iget-boolean v6, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 203
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    iget-boolean v6, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 205
    iget-boolean v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    if-eqz v5, :cond_1

    .line 207
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 215
    :goto_3
    iget-boolean v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    invoke-virtual {p2, v5}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 143
    :pswitch_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oContext:Landroid/content/Context;

    const/16 v6, 0x5a

    invoke-virtual {v0, v5, v6, v9}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v5

    iput v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    goto :goto_1

    .line 146
    :pswitch_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oContext:Landroid/content/Context;

    const/16 v6, 0x5b

    const/16 v7, 0xa

    invoke-virtual {v0, v5, v6, v7}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v5

    iput v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    goto :goto_1

    .line 149
    :pswitch_2
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oContext:Landroid/content/Context;

    const/16 v6, 0x5c

    const/16 v7, 0x14

    invoke-virtual {v0, v5, v6, v7}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v5

    iput v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    goto :goto_1

    .line 152
    :pswitch_3
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oContext:Landroid/content/Context;

    const/16 v6, 0x5d

    const/16 v7, 0x1e

    invoke-virtual {v0, v5, v6, v7}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v5

    iput v5, v4, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    goto :goto_1

    .line 159
    :sswitch_0
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f070085

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 162
    :sswitch_1
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f070083

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 165
    :sswitch_2
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f070086

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 168
    :sswitch_3
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f070084

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 171
    :sswitch_4
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f07007b

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 174
    :sswitch_5
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f070079

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 177
    :sswitch_6
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f07007c

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 180
    :sswitch_7
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f07007a

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 183
    :sswitch_8
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f07008d

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 186
    :sswitch_9
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f07008b

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 189
    :sswitch_a
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f070080

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 192
    :sswitch_b
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f07007e

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 195
    :sswitch_c
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v6, 0x7f07007f

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 211
    :cond_1
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->bringToFront()V

    .line 212
    iget-object v5, v2, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 140
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 156
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0xa -> :sswitch_4
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xd -> :sswitch_7
        0x14 -> :sswitch_8
        0x15 -> :sswitch_9
        0x1e -> :sswitch_a
        0x1f -> :sswitch_b
        0x20 -> :sswitch_c
    .end sparse-switch
.end method

.method public setList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/SettingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/setting/SettingItem;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 73
    :cond_0
    return-void
.end method
