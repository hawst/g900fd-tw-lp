.class Lcom/infraware/polarisoffice5/setting/SettingActivity$1;
.super Ljava/lang/Object;
.source "SettingActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    check-cast p2, Lcom/infraware/common/update/UpdateService$UpdateBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/infraware/common/update/UpdateService$UpdateBinder;->getService()Lcom/infraware/common/update/UpdateService;

    move-result-object v1

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$002(Lcom/infraware/polarisoffice5/setting/SettingActivity;Lcom/infraware/common/update/UpdateService;)Lcom/infraware/common/update/UpdateService;

    .line 160
    const-string/jumbo v0, "SETTING_ACTIVITY"

    const-string/jumbo v1, "onServiceConnected()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 165
    const-string/jumbo v0, "SETTING_ACTIVITY"

    const-string/jumbo v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$002(Lcom/infraware/polarisoffice5/setting/SettingActivity;Lcom/infraware/common/update/UpdateService;)Lcom/infraware/common/update/UpdateService;

    .line 167
    return-void
.end method
