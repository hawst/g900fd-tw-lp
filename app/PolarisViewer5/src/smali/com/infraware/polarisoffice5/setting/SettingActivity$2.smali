.class Lcom/infraware/polarisoffice5/setting/SettingActivity$2;
.super Landroid/os/Handler;
.source "SettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 232
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 234
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 258
    :goto_0
    return-void

    .line 237
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->setCreateBackup()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$100(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto :goto_0

    .line 240
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->setDisplayExt()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto :goto_0

    .line 243
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SettingActivity;->setAutoFit()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    goto :goto_0

    .line 246
    :sswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strVersionCode:Ljava/lang/String;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onUpdateApk(Ljava/lang/String;)Z

    goto :goto_0

    .line 249
    :sswitch_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$500(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 252
    :sswitch_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$600(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$600(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$600(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$500(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 234
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x28 -> :sswitch_5
        0x64 -> :sswitch_3
    .end sparse-switch
.end method
