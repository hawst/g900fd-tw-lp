.class Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$1;
.super Ljava/lang/Object;
.source "SetDictionaryActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->access$000(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .line 87
    .local v0, "item":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iget-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    if-nez v1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;

    iget v2, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    # setter for: Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nDictionary:I
    invoke-static {v1, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->access$102(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;I)I

    .line 91
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;

    # invokes: Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->showDictionaryDialog()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->access$200(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;)V

    goto :goto_0
.end method
