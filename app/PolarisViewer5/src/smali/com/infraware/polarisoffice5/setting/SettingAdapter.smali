.class public Lcom/infraware/polarisoffice5/setting/SettingAdapter;
.super Landroid/widget/BaseAdapter;
.source "SettingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private colorSummary:Landroid/content/res/ColorStateList;

.field private colorTitle:Landroid/content/res/ColorStateList;

.field private m_oContext:Landroid/content/Context;

.field private m_oHandler:Landroid/os/Handler;

.field private m_oInflater:Landroid/view/LayoutInflater;

.field private m_oItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/SettingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 41
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    .line 42
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 49
    if-eqz p1, :cond_0

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    .line 55
    :cond_0
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oHandler:Landroid/os/Handler;

    .line 57
    return-void
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/setting/SettingAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    const/4 v0, 0x0

    .line 84
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 90
    :cond_0
    const/4 v0, 0x0

    .line 92
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemById(I)Lcom/infraware/polarisoffice5/setting/SettingItem;
    .locals 4
    .param p1, "nId"    # I

    .prologue
    const/4 v2, 0x0

    .line 60
    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object v1, v2

    .line 70
    :cond_1
    :goto_0
    return-object v1

    .line 63
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 65
    iget-object v3, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .line 66
    .local v1, "item":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iget v3, v1, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    if-eq v3, p1, :cond_1

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1    # "item":Lcom/infraware/polarisoffice5/setting/SettingItem;
    :cond_3
    move-object v1, v2

    .line 70
    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 97
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 104
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f03000e

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 106
    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;

    const/4 v6, 0x0

    invoke-direct {v2, p0, v6}, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;-><init>(Lcom/infraware/polarisoffice5/setting/SettingAdapter;Lcom/infraware/polarisoffice5/setting/SettingAdapter$1;)V

    .line 107
    .local v2, "holder":Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;
    const v6, 0x7f0b0044

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    .line 108
    const v6, 0x7f0b0047

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    .line 109
    const v6, 0x7f0b0048

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    .line 110
    const v6, 0x7f0b004a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    iput-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    .line 111
    const v6, 0x7f0b0045

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    .line 112
    const v6, 0x7f0b004b

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    iput-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    .line 114
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 115
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .line 117
    .local v5, "setting":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iget-boolean v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_IsTitle:Z

    if-eqz v6, :cond_1

    .line 119
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    iget v7, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 122
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 125
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 126
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 128
    const/4 v6, 0x0

    invoke-virtual {p2, v6}, Landroid/view/View;->setClickable(Z)V

    .line 211
    :cond_0
    :goto_0
    return-object p2

    .line 132
    :cond_1
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 134
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 135
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 137
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    .line 138
    .local v0, "config":Lcom/infraware/common/config/RuntimeConfig;
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget v7, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 140
    iget v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    if-lez v6, :cond_2

    .line 141
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    iget v7, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 144
    :cond_2
    iget v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    sparse-switch v6, :sswitch_data_0

    .line 166
    :cond_3
    :goto_1
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget-boolean v7, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 167
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    iget-boolean v7, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 168
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    iget-boolean v7, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 170
    iget-boolean v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    if-eqz v6, :cond_7

    .line 172
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 180
    :goto_2
    iget-boolean v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    invoke-virtual {p2, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 182
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    iget v7, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nCheckShow:I

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 183
    iget v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nCheckShow:I

    if-nez v6, :cond_0

    .line 185
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020016

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 186
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    new-instance v7, Lcom/infraware/polarisoffice5/setting/SettingAdapter$1;

    invoke-direct {v7, p0, v5}, Lcom/infraware/polarisoffice5/setting/SettingAdapter$1;-><init>(Lcom/infraware/polarisoffice5/setting/SettingAdapter;Lcom/infraware/polarisoffice5/setting/SettingItem;)V

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    const/4 v3, 0x0

    .line 196
    .local v3, "isChecked":Z
    iget v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    packed-switch v6, :pswitch_data_0

    .line 208
    :goto_3
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 147
    .end local v3    # "isChecked":Z
    :sswitch_0
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    const/16 v7, 0xb

    const/4 v8, 0x1

    invoke-virtual {v0, v6, v7, v8}, Lcom/infraware/common/config/RuntimeConfig;->getIntPreference(Landroid/content/Context;II)I

    move-result v1

    .line 148
    .local v1, "defaultFormat":I
    if-nez v1, :cond_4

    .line 149
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v7, 0x7f070003

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 151
    :cond_4
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    const v7, 0x7f070004

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 154
    .end local v1    # "defaultFormat":I
    :sswitch_1
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Version "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    const v9, 0x7f070322

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_6

    const/4 v4, 0x1

    .line 157
    .local v4, "isDebuggable":Z
    :goto_4
    if-nez v4, :cond_5

    invoke-static {}, Lcom/infraware/common/util/CMLog;->getDebugEnable()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 158
    :cond_5
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Version "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    const v9, 0x7f070322

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "_Debug"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 156
    .end local v4    # "isDebuggable":Z
    :cond_6
    const/4 v4, 0x0

    goto :goto_4

    .line 161
    :sswitch_2
    iget-boolean v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    if-eqz v6, :cond_3

    invoke-static {}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;

    move-result-object v6

    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->isEmpty(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 162
    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    goto/16 :goto_1

    .line 176
    :cond_7
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->bringToFront()V

    .line 177
    iget-object v6, v2, Lcom/infraware/polarisoffice5/setting/SettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 199
    .restart local v3    # "isChecked":Z
    :pswitch_0
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    const/16 v7, 0xc

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v7, v8}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v3

    .line 200
    goto/16 :goto_3

    .line 202
    :pswitch_1
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    const/16 v7, 0xd

    const/4 v8, 0x1

    invoke-virtual {v0, v6, v7, v8}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v3

    .line 203
    goto/16 :goto_3

    .line 205
    :pswitch_2
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oContext:Landroid/content/Context;

    const/16 v7, 0xe

    const/4 v8, 0x1

    invoke-virtual {v0, v6, v7, v8}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v3

    goto/16 :goto_3

    .line 144
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_2
        0x15 -> :sswitch_1
    .end sparse-switch

    .line 196
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/SettingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/setting/SettingItem;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 75
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 77
    :cond_0
    return-void
.end method
