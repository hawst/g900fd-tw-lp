.class Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$1;
.super Ljava/lang/Object;
.source "DictionarySettingActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 242
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->access$000(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    .line 243
    .local v0, "item":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    iget-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    if-nez v1, :cond_0

    .line 250
    :goto_0
    return-void

    .line 247
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 248
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->access$100(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;)Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->notifyDataSetChanged()V

    .line 249
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity$1;->this$0:Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;

    iget v2, v0, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nId:I

    # invokes: Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->showDictionaryDialog(I)V
    invoke-static {v1, v2}, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;->access$200(Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;I)V

    goto :goto_0
.end method
