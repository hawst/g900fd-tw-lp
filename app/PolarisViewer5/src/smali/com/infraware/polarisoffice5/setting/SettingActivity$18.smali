.class Lcom/infraware/polarisoffice5/setting/SettingActivity$18;
.super Ljava/lang/Object;
.source "SettingActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0

    .prologue
    .line 984
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 987
    const-string/jumbo v7, "DOWNLOAD_TRHEAD"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Thread Start!, UpdateBinder : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v9}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/update/UpdateService;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0b01a9

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 990
    .local v0, "progressBar":Landroid/widget/ProgressBar;
    const/16 v7, 0x3e8

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 991
    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 993
    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0b01a7

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 994
    .local v5, "txtProgressMessage":Landroid/widget/TextView;
    const v7, 0x7f070271

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    .line 996
    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0b01a6

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 998
    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0b01a8

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 999
    .local v4, "txtProgressCount":Landroid/widget/TextView;
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1001
    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0b01ab

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1002
    .local v6, "txtProgressPercent":Landroid/widget/TextView;
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1004
    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/4 v8, 0x1

    # setter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloading:Z
    invoke-static {v7, v8}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2702(Lcom/infraware/polarisoffice5/setting/SettingActivity;Z)Z

    .line 1006
    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$1;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$1;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity$18;)V

    .line 1022
    .local v2, "thread_txtProgressCount":Ljava/lang/Runnable;
    new-instance v3, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$2;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$2;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity$18;)V

    .line 1043
    .local v3, "thread_txtProgressPercent":Ljava/lang/Runnable;
    new-instance v1, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$3;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$3;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity$18;)V

    .line 1064
    .local v1, "thread_txtProgressBar":Ljava/lang/Runnable;
    :goto_0
    iget-object v7, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloading:Z
    invoke-static {v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2700(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1067
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 1068
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 1069
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->post(Ljava/lang/Runnable;)Z

    .line 1072
    const-wide/16 v7, 0xc8

    :try_start_0
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1074
    :catch_0
    move-exception v7

    goto :goto_0

    .line 1076
    :cond_0
    return-void
.end method
