.class Lcom/infraware/polarisoffice5/setting/SettingActivity$18$3;
.super Ljava/lang/Object;
.source "SettingActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity$18;)V
    .locals 0

    .prologue
    .line 1044
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$3;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 1048
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$3;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    iget-object v6, v6, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/update/UpdateService;

    move-result-object v6

    if-nez v6, :cond_1

    .line 1061
    :cond_0
    :goto_0
    return-void

    .line 1051
    :cond_1
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$3;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    iget-object v6, v6, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/update/UpdateService;

    move-result-object v6

    invoke-virtual {v6}, Lcom/infraware/common/update/UpdateService;->getAPKCurMByte()D

    move-result-wide v0

    .line 1052
    .local v0, "nCurMB":D
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$3;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    iget-object v6, v6, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/update/UpdateService;

    move-result-object v6

    invoke-virtual {v6}, Lcom/infraware/common/update/UpdateService;->getAPKTotalMByte()D

    move-result-wide v3

    .line 1054
    .local v3, "nTotalMB":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v3, v6

    if-eqz v6, :cond_0

    .line 1057
    div-double v6, v0, v3

    const-wide v8, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v8

    double-to-int v2, v6

    .line 1059
    .local v2, "nPercent":I
    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$18$3;->this$1:Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    iget-object v6, v6, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0b01a9

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    .line 1060
    .local v5, "progressBar":Landroid/widget/ProgressBar;
    invoke-virtual {v5, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method
