.class public Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;
.super Landroid/widget/BaseAdapter;
.source "DictionarySettingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$1;,
        Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private colorSummary:Landroid/content/res/ColorStateList;

.field private colorTitle:Landroid/content/res/ColorStateList;

.field private m_oContext:Landroid/content/Context;

.field private m_oInflater:Landroid/view/LayoutInflater;

.field private m_oItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/DictSettingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 37
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    .line 42
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 45
    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0501d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    .line 51
    :cond_0
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oContext:Landroid/content/Context;

    .line 52
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    :cond_0
    const/4 v0, 0x0

    .line 65
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 71
    :cond_0
    const/4 v0, 0x0

    .line 73
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 78
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 84
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f03000e

    invoke-virtual {v4, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 86
    new-instance v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;

    invoke-direct {v1, p0, v8}, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;-><init>(Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$1;)V

    .line 87
    .local v1, "holder":Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;
    const v4, 0x7f0b0044

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    .line 88
    const v4, 0x7f0b0047

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    .line 89
    const v4, 0x7f0b0048

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    .line 90
    const v4, 0x7f0b004a

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    .line 91
    const v4, 0x7f0b0045

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    .line 92
    const v4, 0x7f0b004b

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    iput-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    .line 94
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 95
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;

    .line 97
    .local v3, "setting":Lcom/infraware/polarisoffice5/setting/DictSettingItem;
    iget-boolean v4, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_IsTitle:Z

    if-eqz v4, :cond_0

    .line 99
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_title:Landroid/widget/TextView;

    iget v5, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nTitleId:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 102
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 105
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 106
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 108
    invoke-virtual {p2, v7}, Landroid/view/View;->setClickable(Z)V

    .line 110
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oContext:Landroid/content/Context;

    const v5, 0x41caa3d7    # 25.33f

    invoke-static {v4, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    int-to-float v0, v4

    .line 111
    .local v0, "height":F
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    const/4 v4, -0x1

    float-to-int v5, v0

    invoke-direct {v2, v4, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 112
    .local v2, "layoutParam":Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {p2, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    .end local v0    # "height":F
    .end local v2    # "layoutParam":Landroid/widget/AbsListView$LayoutParams;
    :goto_0
    return-object p2

    .line 115
    :cond_0
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_flDivider:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 117
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->colorTitle:Landroid/content/res/ColorStateList;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 118
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->colorSummary:Landroid/content/res/ColorStateList;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 120
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget v5, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nTitleId:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 122
    iget-object v4, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 123
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    iget-object v5, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nSummary:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    :cond_1
    iget v4, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nId:I

    .line 129
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemTitle:Landroid/widget/TextView;

    iget-boolean v5, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 130
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_itemSummary:Landroid/widget/TextView;

    iget-boolean v5, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 131
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    iget-boolean v5, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 133
    iget-boolean v4, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    if-eqz v4, :cond_2

    .line 135
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 143
    :goto_1
    iget-boolean v4, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_bIsEnable:Z

    invoke-virtual {p2, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 145
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_checkBox:Landroid/widget/CheckBox;

    iget v5, v3, Lcom/infraware/polarisoffice5/setting/DictSettingItem;->m_nCheckShow:I

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 139
    :cond_2
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->bringToFront()V

    .line 140
    iget-object v4, v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter$ViewHolder;->m_buttonDisable:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public setList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/DictSettingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/setting/DictSettingItem;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 56
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/DictionarySettingAdapter;->m_oItemList:Ljava/util/ArrayList;

    .line 58
    :cond_0
    return-void
.end method
