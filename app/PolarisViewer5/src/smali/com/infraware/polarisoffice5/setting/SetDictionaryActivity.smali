.class public Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;
.super Landroid/app/Activity;
.source "SetDictionaryActivity.java"


# static fields
.field public static final SET_DICTIONARY_EN:I = 0x1

.field public static final SET_DICTIONARY_JA:I = 0x3

.field public static final SET_DICTIONARY_KO:I = 0x0

.field public static final SET_DICTIONARY_ZH:I = 0x2


# instance fields
.field private m_bInternalMode:Z

.field private m_lvDictionaryList:Landroid/widget/ListView;

.field private m_nDictionary:I

.field public m_nLocaleCode:I

.field public m_nOrientation:I

.field private m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

.field private m_oDictionaryAdapter:Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;

.field private m_oDictionaryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private m_oDictionaryDialog:Landroid/app/AlertDialog;

.field private m_oDictionaryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/SettingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 30
    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nLocaleCode:I

    .line 33
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_bInternalMode:Z

    .line 35
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryDialog:Landroid/app/AlertDialog;

    .line 81
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$1;-><init>(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;
    .param p1, "x1"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nDictionary:I

    return p1
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->showDictionaryDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;
    .param p1, "x1"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->setDictionary(I)V

    return-void
.end method

.method private onLocaleChanged(I)V
    .locals 2
    .param p1, "nLocale"    # I

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nLocaleCode:I

    .line 169
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->showDictionaryDialog()V

    .line 171
    const v1, 0x7f0b0044

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 172
    .local v0, "tvTitle":Landroid/widget/TextView;
    const v1, 0x7f0700b6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 173
    return-void
.end method

.method private onOrientationChanged()V
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nOrientation:I

    .line 178
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nOrientation:I

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 182
    :cond_0
    return-void
.end method

.method private setDictionary(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 246
    iget v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nDictionary:I

    packed-switch v0, :pswitch_data_0

    .line 262
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryAdapter:Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->notifyDataSetChanged()V

    .line 263
    return-void

    .line 249
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    const/16 v1, 0x5a

    add-int/lit8 v2, p1, 0x0

    invoke-virtual {v0, p0, v1, v2}, Lcom/infraware/common/config/RuntimeConfig;->setIntPreference(Landroid/content/Context;II)V

    goto :goto_0

    .line 252
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    const/16 v1, 0x5b

    add-int/lit8 v2, p1, 0xa

    invoke-virtual {v0, p0, v1, v2}, Lcom/infraware/common/config/RuntimeConfig;->setIntPreference(Landroid/content/Context;II)V

    goto :goto_0

    .line 255
    :pswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    const/16 v1, 0x5c

    add-int/lit8 v2, p1, 0x14

    invoke-virtual {v0, p0, v1, v2}, Lcom/infraware/common/config/RuntimeConfig;->setIntPreference(Landroid/content/Context;II)V

    goto :goto_0

    .line 258
    :pswitch_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    const/16 v1, 0x5d

    add-int/lit8 v2, p1, 0x1e

    invoke-virtual {v0, p0, v1, v2}, Lcom/infraware/common/config/RuntimeConfig;->setIntPreference(Landroid/content/Context;II)V

    goto :goto_0

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setSettingList()V
    .locals 3

    .prologue
    .line 118
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 120
    const/4 v0, 0x0

    .line 122
    .local v0, "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 123
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x0

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 124
    const v1, 0x7f070087

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 125
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 127
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 130
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x1

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 131
    const v1, 0x7f07007d

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 132
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 134
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 137
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x2

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 138
    const v1, 0x7f070078

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 139
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 141
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 144
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x3

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 145
    const v1, 0x7f070082

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 146
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 148
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryAdapter:Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->setList(Ljava/util/ArrayList;)V

    .line 151
    return-void
.end method

.method private showDictionaryDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 186
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 189
    :cond_0
    const/4 v0, 0x0

    .line 190
    .local v0, "dictionaryString":[Ljava/lang/String;
    const v1, 0x7f070087

    .line 192
    .local v1, "nTitleId":I
    iget v2, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nDictionary:I

    packed-switch v2, :pswitch_data_0

    .line 229
    :goto_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v2, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$2;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity$2;-><init>(Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryDialog:Landroid/app/AlertDialog;

    .line 240
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 241
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 242
    return-void

    .line 195
    :pswitch_0
    const v1, 0x7f070087

    .line 197
    new-array v0, v7, [Ljava/lang/String;

    .line 198
    const v2, 0x7f070085

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 199
    const v2, 0x7f070083

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 200
    const v2, 0x7f070086

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    .line 201
    const v2, 0x7f070084

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    goto :goto_0

    .line 204
    :pswitch_1
    const v1, 0x7f07007d

    .line 206
    new-array v0, v7, [Ljava/lang/String;

    .line 207
    const v2, 0x7f07007b

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 208
    const v2, 0x7f070079

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 209
    const v2, 0x7f07007c

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    .line 210
    const v2, 0x7f07007a

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    goto :goto_0

    .line 213
    :pswitch_2
    const v1, 0x7f070078

    .line 215
    new-array v0, v5, [Ljava/lang/String;

    .line 216
    const v2, 0x7f07008d

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 217
    const v2, 0x7f07008b

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    goto/16 :goto_0

    .line 220
    :pswitch_3
    const v1, 0x7f070082

    .line 222
    new-array v0, v6, [Ljava/lang/String;

    .line 223
    const v2, 0x7f070080

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 224
    const v2, 0x7f07007e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 225
    const v2, 0x7f07007f

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    goto/16 :goto_0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 156
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 157
    .local v0, "nLocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nLocaleCode:I

    if-eq v1, v0, :cond_0

    .line 158
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->onLocaleChanged(I)V

    .line 160
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_1

    .line 161
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->onOrientationChanged()V

    .line 163
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 164
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 50
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 54
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 56
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 58
    const-string/jumbo v4, "key_interanl_mode"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_bInternalMode:Z

    .line 61
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 62
    .local v0, "closeFilter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 64
    const v4, 0x7f03000c

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->setContentView(I)V

    .line 66
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v4

    iput v4, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_nLocaleCode:I

    .line 67
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->onOrientationChanged()V

    .line 69
    const v4, 0x7f0b0044

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 70
    .local v3, "tvTitle":Landroid/widget/TextView;
    const v4, 0x7f0700b6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 72
    const v4, 0x7f0b0042

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_lvDictionaryList:Landroid/widget/ListView;

    .line 73
    new-instance v4, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryAdapter:Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;

    .line 75
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_lvDictionaryList:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryAdapter:Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 76
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_lvDictionaryList:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 77
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_lvDictionaryList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02003d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 78
    iget-object v4, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_lvDictionaryList:Landroid/widget/ListView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 79
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 113
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 114
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 98
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 99
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->setSettingList()V

    .line 105
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;->m_oDictionaryAdapter:Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/setting/SetDictionaryAdapter;->notifyDataSetChanged()V

    .line 107
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 108
    return-void
.end method
