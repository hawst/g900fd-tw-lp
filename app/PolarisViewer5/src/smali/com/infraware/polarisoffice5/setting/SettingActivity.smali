.class public Lcom/infraware/polarisoffice5/setting/SettingActivity;
.super Landroid/app/Activity;
.source "SettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;,
        Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;
    }
.end annotation


# static fields
.field public static final APK_FILE_NAME_FOR_INSTALL:Ljava/lang/String; = "APKFileNameForInstall"

.field private static final DOWNLOAD_CANCEL:I = 0x1f

.field private static final DOWNLOAD_CONFIRM_ERROR:I = 0x24

.field private static final DOWNLOAD_CONNECTION_ERROR:I = 0x21

.field private static final DOWNLOAD_FILE_WRITE_ERROR:I = 0x23

.field private static final DOWNLOAD_LATEST_VERSION:I = 0x20

.field private static final DOWNLOAD_NETWORK_ERROR:I = 0x22

.field private static final DOWNLOAD_SUCCESS:I = 0x1e

.field private static final POPUP_CANCELED:I = 0x258

.field private static final POPUP_CLEAR_RECENT:I = 0x2bc

.field private static final POPUP_MSG:I = 0xc8

.field private static final POPUP_NEED_UPDATE:I = 0x3e8

.field private static final POPUP_START_DOWNLOAD:I = 0x384

.field private static final POPUP_UPDATE:I = 0x190

.field private static final POPUP_UPDATE_OK_CANCEL:I = 0x1f4

.field public static final SETTING_ABOUT:I = 0x15

.field public static final SETTING_AUTO_FIT:I = 0x3

.field public static final SETTING_CLEAR_RECENT:I = 0x4

.field public static final SETTING_CREATE_BACKUP:I = 0x1

.field public static final SETTING_DEFAULT_DICTIONARY:I = 0x7

.field public static final SETTING_DEFAULT_FORMAT:I = 0x0

.field public static final SETTING_DISPLAY_FILE:I = 0x2

.field public static final SETTING_EULA:I = 0x16

.field public static final SETTING_OPEN_SOURCE:I = 0x17

.field public static final SETTING_SET_DICTIONARY:I = 0x6

.field public static final SETTING_START_FOLDER:I = 0x5

.field public static final SETTING_UPDATE:I = 0x14

.field private static final SYNC_SETTING_COMPLETE:I = 0x28

.field private static final UPDATE_START:I = 0x64


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCancelDialog:Landroid/app/AlertDialog;

.field private mCancelMsg:I

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mClearDialog:Landroid/app/AlertDialog;

.field private mCloseReceiver:Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mCycleDialog:Landroid/app/AlertDialog;

.field private mDownloadDialog:Landroid/app/AlertDialog;

.field private mFormatDialog:Landroid/app/AlertDialog;

.field private mIsSubUpdate:Z

.field private mLayoutTitle:Landroid/widget/LinearLayout;

.field private mPopupDialog:Landroid/app/AlertDialog;

.field private mPopupMsg:I

.field private mPopupNeedUpdateDialog:Landroid/app/AlertDialog;

.field private mPopupUpdateDialog:Landroid/app/AlertDialog;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSizeDialog:Landroid/app/AlertDialog;

.field private mSyncDialog:Landroid/app/AlertDialog;

.field private mSyncProgress:Landroid/app/ProgressDialog;

.field private mTempPath:Ljava/lang/String;

.field private mTvTitle:Landroid/widget/TextView;

.field private mUpdateReceiver:Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;

.field private m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

.field private m_IsUpdatedCanceled:Z

.field private m_Service_Status:I

.field private m_bInternalMode:Z

.field private m_bPause:Z

.field private m_isDownloadAccept:Z

.field private m_isDownloading:Z

.field private m_lvSettingList:Landroid/widget/ListView;

.field private m_nFreeSize:J

.field public m_nLocaleCode:I

.field public m_nOrientation:I

.field private m_nPathSize:J

.field private m_nSelectedId:I

.field private m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

.field private m_oEventHandler:Landroid/os/Handler;

.field private m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;

.field private m_oSettingClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private m_oSettingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/setting/SettingItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_oView:Landroid/view/View;

.field private m_progressUpdate:Ljava/lang/Thread;

.field private m_strFileName:Ljava/lang/String;

.field private m_strNewVersion:Ljava/lang/String;

.field private m_strSubUpdateURL:Ljava/lang/String;

.field private m_strUpdateURL:Ljava/lang/String;

.field private m_strVersionCode:Ljava/lang/String;

.field onAlarmCopyRightsClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onAlarmEulaClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onAlarmUpdateClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private updateBinder:Lcom/infraware/common/update/UpdateService;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 91
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strVersionCode:Ljava/lang/String;

    .line 92
    iput v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    iput v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelMsg:I

    .line 94
    iput v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nLocaleCode:I

    .line 97
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strFileName:Ljava/lang/String;

    .line 98
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mTempPath:Ljava/lang/String;

    .line 99
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strNewVersion:Ljava/lang/String;

    .line 101
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloading:Z

    .line 102
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mIsSubUpdate:Z

    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    .line 103
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloadAccept:Z

    .line 108
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    .line 109
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 110
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    .line 111
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    .line 112
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mFormatDialog:Landroid/app/AlertDialog;

    .line 113
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    .line 114
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncDialog:Landroid/app/AlertDialog;

    .line 116
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCycleDialog:Landroid/app/AlertDialog;

    .line 117
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSizeDialog:Landroid/app/AlertDialog;

    .line 118
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupNeedUpdateDialog:Landroid/app/AlertDialog;

    .line 119
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    .line 121
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    .line 125
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strSubUpdateURL:Ljava/lang/String;

    .line 127
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oEventHandler:Landroid/os/Handler;

    .line 130
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_progressUpdate:Ljava/lang/Thread;

    .line 131
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bPause:Z

    .line 132
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_Service_Status:I

    .line 137
    iput v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nSelectedId:I

    .line 139
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;

    .line 141
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_IsUpdatedCanceled:Z

    .line 143
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    .line 150
    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 155
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingActivity$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$1;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mConnection:Landroid/content/ServiceConnection;

    .line 275
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$3;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 437
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$4;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 443
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingActivity$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$5;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmUpdateClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 459
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingActivity$6;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$6;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmEulaClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 475
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingActivity$7;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$7;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAlarmCopyRightsClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 1272
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/update/UpdateService;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/setting/SettingActivity;Lcom/infraware/common/update/UpdateService;)Lcom/infraware/common/update/UpdateService;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Lcom/infraware/common/update/UpdateService;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->setCreateBackup()V

    return-void
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/common/config/RuntimeConfig;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/setting/SettingActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onSetDictionary()V

    return-void
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/infraware/polarisoffice5/setting/SettingActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCheckBox:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->settingUpdate()V

    return-void
.end method

.method static synthetic access$1700(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onAbout()V

    return-void
.end method

.method static synthetic access$1800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onEULA()V

    return-void
.end method

.method static synthetic access$1900(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onOpenSource()V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->setDisplayExt()V

    return-void
.end method

.method static synthetic access$2000(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onDictionary()V

    return-void
.end method

.method static synthetic access$2100(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/infraware/polarisoffice5/setting/SettingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_IsUpdatedCanceled:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/infraware/polarisoffice5/setting/SettingActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelMsg:I

    return p1
.end method

.method static synthetic access$2602(Lcom/infraware/polarisoffice5/setting/SettingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mIsSubUpdate:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloading:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/infraware/polarisoffice5/setting/SettingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloading:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloadAccept:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/infraware/polarisoffice5/setting/SettingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloadAccept:Z

    return p1
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->setAutoFit()V

    return-void
.end method

.method static synthetic access$3002(Lcom/infraware/polarisoffice5/setting/SettingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strFileName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3102(Lcom/infraware/polarisoffice5/setting/SettingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strNewVersion:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strVersionCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Lcom/infraware/polarisoffice5/setting/SettingAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bPause:Z

    return v0
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mFormatDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$902(Lcom/infraware/polarisoffice5/setting/SettingActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/setting/SettingActivity;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mFormatDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private createDialog(I)V
    .locals 8
    .param p1, "id"    # I

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f070063

    const v4, 0x7f07005f

    const/high16 v3, 0x7f070000

    const/4 v5, 0x0

    .line 827
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1109
    :cond_3
    :goto_0
    return-void

    .line 838
    :cond_4
    const/4 v0, 0x0

    .line 840
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 842
    :sswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$8;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$8;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    .line 850
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 851
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 854
    :sswitch_1
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 855
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 856
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 857
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, -0x2

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, Lcom/infraware/polarisoffice5/setting/SettingActivity$9;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$9;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 867
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$10;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$10;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 884
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 885
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 888
    :sswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$12;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$12;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$11;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$11;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    .line 902
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 903
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 906
    :sswitch_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelMsg:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$13;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$13;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    .line 914
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 915
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloadAccept:Z

    .line 916
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 919
    :sswitch_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f0702a0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0702af

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$15;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$15;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$14;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$14;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    .line 937
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 938
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 941
    :sswitch_5
    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    check-cast v0, Landroid/view/LayoutInflater;

    .line 942
    .restart local v0    # "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030037

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;

    .line 943
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;

    const v2, 0x7f0b01aa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 945
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_5

    .line 947
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 948
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 951
    :cond_5
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$17;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$17;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$16;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$16;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    .line 980
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 982
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v1}, Lcom/infraware/common/update/UpdateService;->downloadProcess()V

    .line 984
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$18;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    const-string/jumbo v3, "progressDownload"

    invoke-direct {v1, v7, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_progressUpdate:Ljava/lang/Thread;

    .line 1079
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_progressUpdate:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1080
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 1083
    :sswitch_6
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0700c2

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$20;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$20;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/polarisoffice5/setting/SettingActivity$19;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$19;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupNeedUpdateDialog:Landroid/app/AlertDialog;

    .line 1105
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupNeedUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1106
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupNeedUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 840
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x190 -> :sswitch_1
        0x1f4 -> :sswitch_2
        0x258 -> :sswitch_3
        0x2bc -> :sswitch_4
        0x384 -> :sswitch_5
        0x3e8 -> :sswitch_6
    .end sparse-switch
.end method

.method private onAbout()V
    .locals 2

    .prologue
    .line 1335
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/OfficeProductInfo;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1336
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 1337
    return-void
.end method

.method private onDictionary()V
    .locals 2

    .prologue
    .line 1385
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/setting/DictionarySettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1386
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 1387
    return-void
.end method

.method private onEULA()V
    .locals 5

    .prologue
    .line 1345
    const v3, 0x7f07002f

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1347
    .local v1, "strEULA":Ljava/lang/String;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_WEB_VIEW()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1348
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/infraware/polarisoffice5/OfficeWebView;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1349
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "key_web_title"

    const v4, 0x7f070032

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1350
    const-string/jumbo v3, "key_web_url"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1351
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 1365
    :goto_0
    return-void

    .line 1354
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1355
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1356
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1357
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private onLocaleChanged(I)V
    .locals 8
    .param p1, "nLocale"    # I

    .prologue
    const v7, 0x7f070063

    const v6, 0x7f07005f

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/high16 v3, 0x7f070000

    .line 748
    iput p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nLocaleCode:I

    .line 750
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 751
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    const v2, 0x7f0702a0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 752
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    const v2, 0x7f0702af

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 753
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setText(I)V

    .line 754
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mClearDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setText(I)V

    .line 757
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 759
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 760
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    iget v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 761
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setText(I)V

    .line 764
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 766
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 767
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f0700a6

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 768
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setText(I)V

    .line 771
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 773
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 774
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    iget v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 775
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setText(I)V

    .line 776
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setText(I)V

    .line 779
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 781
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 782
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    iget v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelMsg:I

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 783
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setText(I)V

    .line 786
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 788
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 789
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setText(I)V

    .line 792
    :cond_5
    const v1, 0x7f0b0044

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 793
    .local v0, "tvTitle":Landroid/widget/TextView;
    const v1, 0x7f0700bf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 795
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mTvTitle:Landroid/widget/TextView;

    const v2, 0x7f0700bf

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 797
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->notifyDataSetChanged()V

    .line 798
    return-void
.end method

.method private onOpenSource()V
    .locals 5

    .prologue
    const v4, 0x7f070030

    .line 1369
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_WEB_VIEW()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1370
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/infraware/polarisoffice5/OfficeWebView;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1371
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "key_web_title"

    const v3, 0x7f0702bf

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1372
    const-string/jumbo v2, "key_web_url"

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1373
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 1381
    :goto_0
    return-void

    .line 1376
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1377
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1378
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1379
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private onOrientationChanged()V
    .locals 4

    .prologue
    .line 802
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nOrientation:I

    .line 803
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v2

    if-nez v2, :cond_0

    .line 805
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget v3, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nOrientation:I

    invoke-static {v2, v3}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 808
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 809
    .local v1, "layoutParamTitleText":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v2, 0x41500000    # 13.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 810
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 812
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 813
    .local v0, "layoutParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 815
    const/high16 v2, 0x42400000    # 48.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 816
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 823
    :goto_0
    return-void

    .line 820
    :cond_1
    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {p0, v2}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 821
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private onSetDictionary()V
    .locals 3

    .prologue
    .line 1131
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/setting/SetDictionaryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1132
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "key_interanl_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1133
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 1134
    return-void
.end method

.method private setAutoFit()V
    .locals 4

    .prologue
    const/16 v3, 0xe

    const/4 v1, 0x1

    .line 1125
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    invoke-virtual {v2, p0, v3, v1}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v0

    .line 1126
    .local v0, "isAutoFit":Z
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v2, p0, v3, v1}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    .line 1127
    return-void
.end method

.method private setCreateBackup()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/4 v1, 0x0

    .line 1113
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    invoke-virtual {v2, p0, v3, v1}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v0

    .line 1114
    .local v0, "isBackup":Z
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v2, p0, v3, v1}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    .line 1115
    return-void

    .line 1114
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private setDisplayExt()V
    .locals 4

    .prologue
    const/16 v3, 0xd

    const/4 v1, 0x1

    .line 1119
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    invoke-virtual {v2, p0, v3, v1}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v0

    .line 1120
    .local v0, "isShowExt":Z
    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v2, p0, v3, v1}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    .line 1121
    return-void
.end method

.method private setSettingList()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 569
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 572
    const/4 v0, 0x0

    .line 574
    .local v0, "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 576
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HAS_EDITOR()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 578
    iput-boolean v3, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_IsTitle:Z

    .line 579
    const v1, 0x7f0700b0

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 581
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_DEFAULT_FORMAT()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 585
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 586
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iput v2, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 587
    const v1, 0x7f0700b5

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 588
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 590
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    :cond_0
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 594
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iput v3, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 595
    const v1, 0x7f0700b3

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 596
    const v1, 0x7f0700b4

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 597
    iput v2, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nCheckShow:I

    .line 598
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 600
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 602
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_FILE_MANAGER()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 604
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 605
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x2

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 606
    const v1, 0x7f0700bb

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 607
    const v1, 0x7f0700bc

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 608
    iput v2, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nCheckShow:I

    .line 609
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 611
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 614
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_SETTING_AUTOFIT()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 616
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 617
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x3

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 618
    const v1, 0x7f0700ad

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 619
    const v1, 0x7f0700ae

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 620
    iput v2, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nCheckShow:I

    .line 621
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 623
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 626
    :cond_2
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 627
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x4

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 628
    const v1, 0x7f0700b1

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 629
    const v1, 0x7f0700b2

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 630
    invoke-static {}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->isEmpty(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 631
    iput-boolean v2, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 635
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_FILE_MANAGER()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 639
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 640
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x5

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 641
    const v1, 0x7f0700bd

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 642
    const v1, 0x7f0700be

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 643
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 644
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    :cond_3
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 649
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 650
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/4 v1, 0x6

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 651
    const v1, 0x7f0700b6

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 652
    const v1, 0x7f0700b7

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 653
    iput-boolean v3, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 654
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 658
    :cond_4
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 659
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    iput-boolean v3, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_IsTitle:Z

    .line 660
    const v1, 0x7f0700af

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 662
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 664
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_ONLINE_UPDATE()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 666
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 667
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/16 v1, 0x14

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 668
    const v1, 0x7f0700c1

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 669
    const v1, 0x7f0700c3

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 670
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 672
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 675
    :cond_5
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 676
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/16 v1, 0x15

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 677
    const v1, 0x7f0700ac

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 678
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 680
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 682
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 683
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/16 v1, 0x16

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 684
    const v1, 0x7f070005

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 685
    const v1, 0x7f0700b8

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 686
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 688
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    new-instance v0, Lcom/infraware/polarisoffice5/setting/SettingItem;

    .end local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    invoke-direct {v0}, Lcom/infraware/polarisoffice5/setting/SettingItem;-><init>()V

    .line 691
    .restart local v0    # "settingItem":Lcom/infraware/polarisoffice5/setting/SettingItem;
    const/16 v1, 0x17

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nId:I

    .line 692
    const v1, 0x7f0700b9

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nTitleId:I

    .line 693
    const v1, 0x7f0700ba

    iput v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_nSummary:I

    .line 694
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    .line 696
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 698
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->setList(Ljava/util/ArrayList;)V

    .line 699
    return-void

    .line 633
    :cond_6
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/setting/SettingItem;->m_bIsEnable:Z

    goto/16 :goto_0
.end method

.method private settingUpdate()V
    .locals 4

    .prologue
    .line 423
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_IsUpdatedCanceled:Z

    .line 426
    const/16 v0, 0x190

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V

    .line 427
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oEventHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 435
    :goto_0
    return-void

    .line 431
    :cond_0
    const v0, 0x7f070092

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    .line 432
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private startSyncProgress()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1138
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    .line 1139
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    const v1, 0x7f070270

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1140
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 1141
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 1142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/infraware/polarisoffice5/setting/SettingActivity$21;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$21;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1153
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mSyncProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1154
    return-void
.end method


# virtual methods
.method protected makeOfficeDir()V
    .locals 3

    .prologue
    .line 1396
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mTempPath:Ljava/lang/String;

    .line 1397
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->makeRootDir()V

    .line 1398
    sget-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    const-string/jumbo v1, "polarisTemp"

    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/infraware/common/util/FileUtils;->createCustomPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mTempPath:Ljava/lang/String;

    .line 1399
    return-void
.end method

.method protected makeRootDir()V
    .locals 2

    .prologue
    .line 1391
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1392
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    .line 1393
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 730
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 731
    packed-switch p1, :pswitch_data_0

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 734
    :pswitch_0
    if-eqz p3, :cond_0

    .line 737
    const-string/jumbo v1, "key_new_folder"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 739
    .local v0, "selectFolder":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 740
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oConfig:Lcom/infraware/common/config/RuntimeConfig;

    const/16 v2, 0x5e

    invoke-virtual {v1, p0, v2, v0}, Lcom/infraware/common/config/RuntimeConfig;->setStringPreference(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 731
    :pswitch_data_0
    .packed-switch 0x1050
        :pswitch_0
    .end packed-switch
.end method

.method public onCheckboxClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 493
    move-object v1, p1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 496
    .local v0, "checked":Z
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 501
    :goto_0
    return-void

    .line 498
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 496
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b003c
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "_newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 717
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 718
    .local v0, "nLocale":I
    iget v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nLocaleCode:I

    if-eq v1, v0, :cond_0

    .line 719
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onLocaleChanged(I)V

    .line 721
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_1

    .line 722
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onOrientationChanged()V

    .line 724
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 725
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f0b0034

    .line 172
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 174
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 176
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 180
    :cond_0
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 181
    const v5, 0x103012c

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->setTheme(I)V

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 184
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 186
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    .line 188
    const-string/jumbo v5, "key_version"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strVersionCode:Ljava/lang/String;

    .line 189
    const-string/jumbo v5, "key_interanl_mode"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bInternalMode:Z

    .line 192
    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 193
    .local v0, "closeFilter":Landroid/content/IntentFilter;
    const-string/jumbo v5, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 195
    new-instance v5, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCloseReceiver:Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;

    .line 196
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCloseReceiver:Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;

    invoke-virtual {p0, v5, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 198
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070320

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strUpdateURL:Ljava/lang/String;

    .line 199
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070321

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strSubUpdateURL:Ljava/lang/String;

    .line 201
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 202
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v5, "com.infraware.polarisviewer5.ACTION_DOWNLOAD_COMPLETE"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 203
    const-string/jumbo v5, "com.infraware.polarisviewer5.ACTION_DISABLE_CONFIRM_VERSION"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 204
    const-string/jumbo v5, "com.infraware.polarisviewer5.ACTION_LATEST_VERSION"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 205
    const-string/jumbo v5, "com.infraware.polarisviewer5.ACTION_CONNECTION_ERROR"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 206
    const-string/jumbo v5, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 207
    const-string/jumbo v5, "com.infraware.polarisviewer5.ACTION_FILE_WRITE_ERROR"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 208
    const-string/jumbo v5, "com.infraware.polarisviewer5.ACTION_USER_CANCEL"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 209
    const-string/jumbo v5, "com.infraware.polarisviewer5.ACTION_UPDATE_AVAILABE"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 211
    new-instance v5, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mUpdateReceiver:Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;

    .line 212
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mUpdateReceiver:Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;

    invoke-virtual {p0, v5, v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 214
    const v5, 0x7f03000c

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->setContentView(I)V

    .line 216
    const v5, 0x7f0b0031

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 217
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mTvTitle:Landroid/widget/TextView;

    .line 219
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v5

    iput v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_nLocaleCode:I

    .line 220
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onOrientationChanged()V

    .line 222
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 224
    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    move-result-object v5

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    .line 225
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v5, p0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->setContext(Landroid/content/Context;)V

    .line 228
    :cond_3
    new-instance v5, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;

    invoke-direct {v5, p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity$2;-><init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oEventHandler:Landroid/os/Handler;

    .line 261
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 262
    .local v4, "tvTitle":Landroid/widget/TextView;
    const v5, 0x7f0700bf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 264
    const v5, 0x7f0b0042

    invoke-virtual {p0, v5}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    .line 265
    new-instance v5, Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oEventHandler:Landroid/os/Handler;

    invoke-direct {v5, p0, v6}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    .line 267
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 268
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 269
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02003d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 270
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_lvSettingList:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 272
    iput-object p0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mActivity:Landroid/app/Activity;

    .line 273
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mCloseReceiver:Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 560
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mUpdateReceiver:Lcom/infraware/polarisoffice5/setting/SettingActivity$UpdateReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 562
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_DictionarySearchControl:Lcom/infraware/polarisoffice5/common/DictionarySearchControl;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/DictionarySearchControl;->stopDictionaryService()V

    .line 564
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 565
    return-void
.end method

.method public onInstall(Ljava/lang/String;)V
    .locals 4
    .param p1, "installPath"    # Ljava/lang/String;

    .prologue
    .line 1323
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1326
    .local v0, "apkFile":Ljava/io/File;
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1327
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const-string/jumbo v3, "application/vnd.android.package-archive"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1328
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1331
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1329
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 704
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 706
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 707
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 708
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->finish()V

    .line 711
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 536
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bPause:Z

    .line 537
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 538
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 543
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bPause:Z

    .line 544
    iget v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_Service_Status:I

    if-eq v0, v1, :cond_0

    .line 546
    iget v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_Service_Status:I

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    .line 547
    iput v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_Service_Status:I

    .line 550
    :cond_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->setSettingList()V

    .line 551
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_oSettingAdapter:Lcom/infraware/polarisoffice5/setting/SettingAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/setting/SettingAdapter;->notifyDataSetChanged()V

    .line 553
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 554
    return-void
.end method

.method public onStopService(I)Z
    .locals 4
    .param p1, "status"    # I

    .prologue
    const/4 v3, 0x0

    const/16 v1, 0xc8

    const/4 v2, 0x1

    .line 1186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;

    if-eqz v0, :cond_0

    .line 1188
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;

    iput-boolean v3, v0, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 1189
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->updateBinder:Lcom/infraware/common/update/UpdateService;

    .line 1192
    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_bPause:Z

    if-eqz v0, :cond_2

    .line 1194
    iput p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_Service_Status:I

    .line 1269
    :cond_1
    :goto_0
    :pswitch_0
    return v2

    .line 1198
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 1201
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1203
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1206
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_4

    .line 1208
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1210
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mDownloadDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1214
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupNeedUpdateDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5

    .line 1216
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupNeedUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1218
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupNeedUpdateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1222
    :cond_5
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloading:Z

    if-eqz v0, :cond_6

    .line 1223
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_isDownloading:Z

    .line 1225
    :cond_6
    packed-switch p1, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 1228
    :pswitch_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->makeOfficeDir()V

    .line 1229
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strFileName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1230
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mTempPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onInstall(Ljava/lang/String;)V

    goto :goto_0

    .line 1233
    :pswitch_3
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_IsUpdatedCanceled:Z

    if-nez v0, :cond_1

    .line 1234
    const v0, 0x7f0700a1

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    .line 1235
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1244
    :pswitch_4
    const v0, 0x7f070092

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    .line 1245
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V

    goto :goto_0

    .line 1248
    :pswitch_5
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mIsSubUpdate:Z

    if-nez v0, :cond_7

    .line 1250
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_IsUpdatedCanceled:Z

    if-nez v0, :cond_1

    .line 1251
    const v0, 0x7f070096

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    .line 1253
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mIsSubUpdate:Z

    .line 1254
    const/16 v0, 0x190

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V

    .line 1255
    iget-object v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strVersionCode:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onUpdateApk(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 1260
    :cond_7
    const v0, 0x7f070095

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    .line 1261
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V

    goto/16 :goto_0

    .line 1265
    :pswitch_6
    const v0, 0x7f070090

    iput v0, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupMsg:I

    .line 1266
    invoke-direct {p0, v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->createDialog(I)V

    goto/16 :goto_0

    .line 1225
    nop

    :pswitch_data_0
    .packed-switch 0x1e
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method

.method public onUpdateApk(Ljava/lang/String;)Z
    .locals 7
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1158
    const-string/jumbo v6, "connectivity"

    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 1159
    .local v2, "netManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v2, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1160
    .local v1, "mobile":Landroid/net/NetworkInfo;
    invoke-virtual {v2, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1162
    .local v3, "wifi":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    if-nez v3, :cond_2

    :cond_0
    move v4, v5

    .line 1181
    :cond_1
    :goto_0
    return v4

    .line 1165
    :cond_2
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1167
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/infraware/common/update/UpdateService;

    invoke-direct {v0, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1168
    .local v0, "bindIntent":Landroid/content/Intent;
    iget-boolean v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mIsSubUpdate:Z

    if-ne v5, v4, :cond_4

    .line 1169
    const-string/jumbo v5, "DOWNLOAD_URL"

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strSubUpdateURL:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1173
    :goto_1
    const-string/jumbo v5, "CURRENT_VER"

    invoke-virtual {v0, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1174
    iget-object v5, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v5, v4}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0

    .line 1171
    :cond_4
    const-string/jumbo v5, "DOWNLOAD_URL"

    iget-object v6, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity;->m_strUpdateURL:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method
