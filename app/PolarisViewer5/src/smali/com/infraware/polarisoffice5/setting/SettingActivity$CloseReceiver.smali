.class public Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/setting/SettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CloseReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/setting/SettingActivity;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 509
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 510
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2100(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2100(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 516
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2100(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->cancel()V

    .line 518
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 521
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    const/16 v2, 0x1f

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->onStopService(I)Z

    .line 522
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2200(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->cancel()V

    .line 525
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 526
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    # getter for: Lcom/infraware/polarisoffice5/setting/SettingActivity;->mPopupUpdateDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->access$2300(Lcom/infraware/polarisoffice5/setting/SettingActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->cancel()V

    .line 528
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/setting/SettingActivity$CloseReceiver;->this$0:Lcom/infraware/polarisoffice5/setting/SettingActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/setting/SettingActivity;->finish()V

    goto :goto_0
.end method
