.class public Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "OfficeProductInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/OfficeProductInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CloseActionReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/OfficeProductInfo;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 269
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 271
    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeProductInfo$CloseActionReceiver;->this$0:Lcom/infraware/polarisoffice5/OfficeProductInfo;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/OfficeProductInfo;->finish()V

    .line 275
    :cond_0
    return-void
.end method
