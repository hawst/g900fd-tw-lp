.class Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;
.super Ljava/lang/Object;
.source "PDFBookmarkActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

.field final synthetic val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;Lcom/infraware/polarisoffice5/viewer/TreeElement;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;->this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;->this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    # invokes: Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->collapse(Lcom/infraware/polarisoffice5/viewer/TreeElement;)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->access$100(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;Lcom/infraware/polarisoffice5/viewer/TreeElement;)V

    .line 264
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;->this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTreeViewAdapter:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->access$300(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;)Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->notifyDataSetChanged()V

    .line 265
    return-void

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;->this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    # invokes: Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->expand(Lcom/infraware/polarisoffice5/viewer/TreeElement;)V
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->access$200(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;Lcom/infraware/polarisoffice5/viewer/TreeElement;)V

    goto :goto_0
.end method
