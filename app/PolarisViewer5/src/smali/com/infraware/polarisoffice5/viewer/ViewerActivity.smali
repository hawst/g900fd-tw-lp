.class public Lcom/infraware/polarisoffice5/viewer/ViewerActivity;
.super Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.source "ViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;
    }
.end annotation


# instance fields
.field protected mMenu:Landroid/view/Menu;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mNfcCallback:Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;

.field private mSBeamEnabled:Z

.field private mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;-><init>()V

    .line 40
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mMenu:Landroid/view/Menu;

    .line 42
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 43
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSBeamEnabled:Z

    .line 46
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 47
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 167
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/viewer/ViewerActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/ViewerActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/viewer/ViewerActivity;)Lcom/infraware/common/util/SbeamHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/ViewerActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/viewer/ViewerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/ViewerActivity;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSBeamEnabled:Z

    return v0
.end method

.method static synthetic access$302(Lcom/infraware/polarisoffice5/viewer/ViewerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/ViewerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSBeamEnabled:Z

    return p1
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/viewer/ViewerActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/ViewerActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method private setNfcCallback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 151
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 153
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 154
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 155
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/viewer/ViewerActivity;Lcom/infraware/polarisoffice5/viewer/ViewerActivity$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;

    .line 156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 157
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iput-object v2, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 162
    iput-object v2, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/ViewerActivity$NfcCallback;

    goto :goto_0
.end method


# virtual methods
.method public OnLoadComplete(I)V
    .locals 0
    .param p1, "bBookmarkExist"    # I

    .prologue
    .line 216
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 52
    const v1, 0x7f03002b

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->setContentView(I)V

    .line 53
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1, p0}, Lcom/infraware/office/baseframe/EvBaseView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 58
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60
    new-instance v1, Lcom/infraware/common/util/SbeamHelper;

    const-string/jumbo v2, "text/DirectSharePolarisEditor"

    invoke-direct {v1, p0, v2}, Lcom/infraware/common/util/SbeamHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 61
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 62
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 63
    new-instance v1, Lcom/infraware/polarisoffice5/viewer/ViewerActivity$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/viewer/ViewerActivity$1;-><init>(Lcom/infraware/polarisoffice5/viewer/ViewerActivity;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 75
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 80
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->setNfcCallback()V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 115
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 116
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onDestroy()V

    .line 101
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 105
    :cond_0
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 0
    .param p1, "nLocale"    # I

    .prologue
    .line 147
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onLocaleChange(I)V

    .line 148
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 84
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onResume()V

    .line 86
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/net/Uri;

    .line 89
    .local v0, "filePath":[Landroid/net/Uri;
    const/4 v1, 0x0

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    .line 90
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, p0, v2}, Lcom/infraware/common/util/SbeamHelper;->setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V

    .line 94
    .end local v0    # "filePath":[Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public setEvListener()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 109
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/ViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 111
    :cond_0
    return-void
.end method
