.class public Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PDFBookmarkActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CloseActionReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 117
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 119
    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->finish()V

    .line 123
    :cond_0
    return-void
.end method
