.class Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PDFBookmarkActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TreeViewAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/infraware/polarisoffice5/viewer/TreeElement;",
        ">;"
    }
.end annotation


# instance fields
.field private mDataSet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/infraware/polarisoffice5/viewer/TreeElement;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mItemLayout:I

.field final synthetic this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;


# direct methods
.method public constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/viewer/TreeElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p4, "dataset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/viewer/TreeElement;>;"
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    .line 195
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 197
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 198
    iput-object p4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->mDataSet:Ljava/util/List;

    .line 199
    iput p3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->mItemLayout:I

    .line 200
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v6, 0x7f020271

    const/4 v7, 0x0

    .line 205
    if-nez p2, :cond_1

    .line 206
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->mItemLayout:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 207
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;)V

    .line 208
    .local v0, "holder":Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;
    const v3, 0x7f0b0180

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->layout:Landroid/widget/LinearLayout;

    .line 209
    const v3, 0x7f0b0182

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 210
    const v3, 0x7f0b0181

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    .line 212
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 217
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->mDataSet:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/TreeElement;

    .line 219
    .local v2, "treeElement":Lcom/infraware/polarisoffice5/viewer/TreeElement;
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 221
    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->hasChild()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->isExpanded()Z

    move-result v3

    if-nez v3, :cond_2

    .line 222
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 230
    :cond_0
    :goto_1
    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getLevel()I

    move-result v1

    .line 231
    .local v1, "level":I
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->layout:Landroid/widget/LinearLayout;

    add-int/lit8 v4, v1, 0x1

    mul-int/lit8 v4, v4, 0x1e

    iget-object v5, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v5

    iget-object v6, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v3, v4, v5, v7, v6}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 233
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->layout:Landroid/widget/LinearLayout;

    new-instance v4, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;

    invoke-direct {v4, p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;Lcom/infraware/polarisoffice5/viewer/TreeElement;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    new-instance v4, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;

    invoke-direct {v4, p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$2;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;Lcom/infraware/polarisoffice5/viewer/TreeElement;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    return-object p2

    .line 214
    .end local v0    # "holder":Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;
    .end local v1    # "level":I
    .end local v2    # "treeElement":Lcom/infraware/polarisoffice5/viewer/TreeElement;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;
    goto :goto_0

    .line 223
    .restart local v2    # "treeElement":Lcom/infraware/polarisoffice5/viewer/TreeElement;
    :cond_2
    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->hasChild()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->isExpanded()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 224
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v4, 0x7f020272

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 225
    :cond_3
    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->hasChild()Z

    move-result v3

    if-nez v3, :cond_0

    .line 226
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 227
    iget-object v3, v0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
