.class public Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;
.super Landroid/widget/ScrollView;
.source "PDFAnnotListView.java"


# instance fields
.field mActivity:Landroid/app/Activity;

.field rootv:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->mActivity:Landroid/app/Activity;

    .line 20
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->mActivity:Landroid/app/Activity;

    .line 20
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    .line 28
    return-void
.end method


# virtual methods
.method public Attach(I)I
    .locals 7
    .param p1, "nAnnotCnt"    # I

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->removeAllViews()V

    .line 66
    :cond_0
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    .line 67
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 68
    const/4 v0, 0x0

    .line 73
    .local v0, "i":I
    move v3, p1

    .line 75
    .local v3, "nCount":I
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EV;->getPdfAnnotationListItem()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    move-result-object v1

    .line 78
    .local v1, "item":Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    .line 79
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFAnnotationListItem(ILcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V

    .line 81
    if-eqz v1, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f030002

    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->mActivity:Landroid/app/Activity;

    invoke-static {v4, v5, v1, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->createItemView(Landroid/content/Context;ILcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;Landroid/app/Activity;)Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    move-result-object v2

    .line 85
    .local v2, "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setVisibility(I)V

    .line 86
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 78
    .end local v2    # "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_2
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->addView(Landroid/view/View;)V

    .line 91
    return v3
.end method

.method public deleteSelectedAnnotation()V
    .locals 5

    .prologue
    .line 106
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    if-nez v3, :cond_1

    .line 115
    :cond_0
    return-void

    .line 108
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 109
    .local v2, "nChild":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 110
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 111
    .local v1, "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 112
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getData()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    move-result-object v4

    iget v4, v4, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->AnnotItem:I

    invoke-virtual {v3, v4}, Lcom/infraware/office/evengine/EvInterface;->IRemovePDFAnnotation(I)V

    .line 109
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public dipToPx(F)F
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 59
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public getSelectCount()I
    .locals 4

    .prologue
    .line 95
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    if-nez v3, :cond_1

    .line 96
    const/4 v2, 0x0

    .line 102
    :cond_0
    return v2

    .line 97
    :cond_1
    const/4 v2, 0x0

    .line 98
    .local v2, "ret":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 99
    .local v1, "nChild":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 100
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 101
    add-int/lit8 v2, v2, 0x1

    .line 99
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 33
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 34
    packed-switch p1, :pswitch_data_0

    .line 41
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 38
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setDeleteMode(Z)V
    .locals 5
    .param p1, "bOn"    # Z

    .prologue
    .line 136
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    if-nez v3, :cond_1

    .line 146
    :cond_0
    return-void

    .line 138
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 139
    .local v2, "nChild":I
    const/16 v0, 0x8

    .line 140
    .local v0, "Mode":I
    if-eqz p1, :cond_2

    .line 141
    const/4 v0, 0x0

    .line 142
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 143
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 144
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 142
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setSelectAll()V
    .locals 5

    .prologue
    .line 118
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    if-nez v4, :cond_1

    .line 133
    :cond_0
    return-void

    .line 120
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    .line 121
    .local v3, "nChild":I
    const/4 v0, 0x0

    .line 123
    .local v0, "bChecked":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_2

    .line 124
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 125
    .local v2, "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-nez v4, :cond_3

    .line 126
    const/4 v0, 0x1

    .line 131
    .end local v2    # "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    .line 132
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 123
    .restart local v2    # "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setView(Landroid/app/Activity;)Z
    .locals 3
    .param p1, "a_activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x1

    .line 46
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->mActivity:Landroid/app/Activity;

    .line 47
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->removeAllViews()V

    .line 50
    :cond_0
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    .line 51
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 53
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->rootv:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->addView(Landroid/view/View;)V

    .line 55
    return v2
.end method
