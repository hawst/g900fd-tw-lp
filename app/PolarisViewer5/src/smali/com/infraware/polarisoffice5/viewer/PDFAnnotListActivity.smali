.class public Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;
.super Lcom/infraware/common/baseactivity/BaseSwitchableActivity;
.source "PDFAnnotListActivity.java"

# interfaces
.implements Lcom/infraware/office/baseframe/EvBaseE$EV_ACTIONBAR_EVENT;
.implements Lcom/infraware/office/baseframe/EvBaseE$PopupDialogEventType;
.implements Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;
    }
.end annotation


# static fields
.field protected static final HANDLE_MODE_CLOSE_DIALOG:I = 0x3

.field protected static final dialogId:Ljava/lang/String;


# instance fields
.field currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

.field llBtnView:Landroid/widget/LinearLayout;

.field llDelBtn:Landroid/widget/LinearLayout;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field mHandler:Landroid/os/Handler;

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;

.field nCount:I

.field nCountCheckItem:I

.field noTocImg:Landroid/widget/ImageView;

.field noTocText:Landroid/widget/TextView;

.field selectChild:I

.field treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/String;

    const-string/jumbo v1, "DIALOG_ID"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->dialogId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 31
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocImg:Landroid/widget/ImageView;

    .line 32
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    .line 33
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llDelBtn:Landroid/widget/LinearLayout;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    .line 36
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCountCheckItem:I

    .line 45
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;

    .line 46
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 153
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$2;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mHandler:Landroid/os/Handler;

    .line 518
    return-void
.end method


# virtual methods
.method public OnPDFAnnotationCount(I)I
    .locals 7
    .param p1, "nAnnotCnt"    # I

    .prologue
    .line 503
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->attach(I)V

    .line 504
    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    sget-object v2, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->dialogId:Ljava/lang/String;

    const/16 v3, 0xa

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 505
    return p1
.end method

.method public actionTitleBarButtonClick()V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->setSelectAll()V

    .line 305
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->onSelectAnnotItem()V

    .line 315
    :goto_0
    return-void

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f020065

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonImage(I)V

    .line 309
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->onSelectAnnotItem()V

    .line 311
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->setDeleteMode(Z)V

    goto :goto_0
.end method

.method public attach(I)V
    .locals 5
    .param p1, "nAnnotCnt"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    .line 124
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    iget v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->Attach(I)I

    move-result v1

    .line 125
    .local v1, "testnum":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v3, 0x7f020066

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonImage(I)V

    .line 126
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0700ca

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(Ljava/lang/String;)V

    .line 128
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v2}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 132
    :try_start_0
    iget v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    if-eqz v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocImg:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 151
    :goto_0
    return-void

    .line 138
    :cond_0
    const v2, 0x7f0b0023

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocImg:Landroid/widget/ImageView;

    .line 139
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocImg:Landroid/widget/ImageView;

    const v3, 0x7f02011e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 140
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocImg:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 141
    const v2, 0x7f0b0024

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    .line 142
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700d6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;
    .locals 3
    .param p1, "what"    # I
    .param p2, "key1"    # Ljava/lang/String;
    .param p3, "value1"    # I
    .param p4, "key2"    # Ljava/lang/String;
    .param p5, "value2"    # I

    .prologue
    .line 485
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 486
    .local v1, "message":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->what:I

    .line 487
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 489
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p2, :cond_0

    .line 490
    invoke-virtual {v0, p2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 491
    :cond_0
    if-eqz p4, :cond_1

    .line 492
    invoke-virtual {v0, p4, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 494
    :cond_1
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 496
    return-object v1
.end method

.method public nextItem()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 318
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 320
    .local v2, "selItem":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 322
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 324
    :cond_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-nez v5, :cond_5

    .line 326
    iput v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    .line 327
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v5, v7}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iget v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 345
    :goto_0
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-nez v5, :cond_1

    .line 347
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 350
    :cond_1
    if-eqz v2, :cond_7

    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-eq v2, v5, :cond_7

    .line 351
    invoke-virtual {v2, v7}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setSelected(Z)V

    .line 357
    :goto_1
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-eqz v5, :cond_2

    .line 358
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v5, v8}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setSelected(Z)V

    .line 360
    :cond_2
    const/4 v5, 0x2

    new-array v1, v5, [I

    .line 361
    .local v1, "location":[I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v5, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getLocationOnScreen([I)V

    .line 362
    aget v4, v1, v8

    .line 363
    .local v4, "treeViewTop":I
    aget v5, v1, v8

    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getHeight()I

    move-result v6

    add-int v3, v5, v6

    .line 364
    .local v3, "treeViewBottom":I
    aget v0, v1, v8

    .line 366
    .local v0, "currItemBottom":I
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-eqz v5, :cond_3

    .line 368
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v5, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getLocationOnScreen([I)V

    .line 369
    aget v5, v1, v8

    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getHeight()I

    move-result v6

    add-int v0, v5, v6

    .line 372
    :cond_3
    if-ge v3, v0, :cond_8

    .line 373
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    sub-int v6, v0, v3

    invoke-virtual {v5, v7, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->scrollBy(II)V

    .line 376
    :cond_4
    :goto_2
    return-void

    .line 331
    .end local v0    # "currItemBottom":I
    .end local v1    # "location":[I
    .end local v3    # "treeViewBottom":I
    .end local v4    # "treeViewTop":I
    :cond_5
    iget v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    .line 332
    iget v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    iget v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    if-lt v5, v6, :cond_6

    .line 336
    iput v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    .line 337
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v5, v7}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iget v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    goto :goto_0

    .line 341
    :cond_6
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v5, v7}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iget v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    iput-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    goto/16 :goto_0

    .line 354
    :cond_7
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1

    .line 374
    .restart local v0    # "currItemBottom":I
    .restart local v1    # "location":[I
    .restart local v3    # "treeViewBottom":I
    .restart local v4    # "treeViewTop":I
    :cond_8
    aget v5, v1, v8

    if-ge v5, v4, :cond_4

    .line 375
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    aget v6, v1, v8

    sub-int/2addr v6, v4

    invoke-virtual {v5, v7, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->scrollBy(II)V

    goto :goto_2
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 290
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFAnnotationCount()V

    .line 291
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->setDeleteMode(Z)V

    .line 293
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 299
    :goto_0
    return-void

    .line 298
    :cond_0
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    iput v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    .line 63
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 65
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->setContentView(I)V

    .line 68
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;

    .line 69
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 73
    const v0, 0x7f0b001f

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    .line 74
    const v0, 0x7f0b0020

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llDelBtn:Landroid/widget/LinearLayout;

    .line 75
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llDelBtn:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$1;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    const v0, 0x7f0b001e

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    .line 94
    new-instance v0, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f0b001d

    const/16 v2, 0x56

    invoke-direct {v0, p0, v1, v3, v2}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 99
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->setView(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f020066

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonImage(I)V

    .line 102
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0700ca

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 105
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 108
    :cond_0
    const v0, 0x7f0b0023

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocImg:Landroid/widget/ImageView;

    .line 109
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocImg:Landroid/widget/ImageView;

    const v1, 0x7f02011e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 110
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 111
    const v0, 0x7f0b0024

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0, v3}, Lcom/infraware/office/actionbar/ActionTitleBar;->setButtonEnabled(Z)V

    .line 119
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFAnnotationCount()V

    .line 120
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 50
    packed-switch p1, :pswitch_data_0

    .line 54
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 52
    :pswitch_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createSearchProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SearchProgressDlg;

    move-result-object v0

    goto :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$CloseActionReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 514
    :cond_0
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onDestroy()V

    .line 515
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 172
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 173
    sparse-switch p1, :sswitch_data_0

    .line 189
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_0
    :sswitch_0
    return v0

    .line 181
    :sswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 183
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_0

    .line 173
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x3d -> :sswitch_0
        0x3e -> :sswitch_1
        0x42 -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 194
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v11, :cond_1

    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    if-lez v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llDelBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x42

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3e

    if-eq p1, v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llDelBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 201
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 254
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v11

    :cond_2
    :goto_0
    return v11

    .line 205
    :sswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nextItem()V

    goto :goto_0

    .line 210
    :sswitch_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->prevItem()V

    goto :goto_0

    .line 217
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-nez v0, :cond_4

    .line 219
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llDelBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 221
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->deleteSelectedAnnotation()V

    .line 224
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFAnnotationCount()V

    .line 225
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->setDeleteMode(Z)V

    .line 227
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 230
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->actionTitleBarButtonClick()V

    goto :goto_0

    .line 233
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    .line 235
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 236
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getSelectBtn()Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_5

    move v1, v11

    :cond_5
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 237
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->onSelectAnnotItem()V

    goto :goto_0

    .line 240
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getData()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    move-result-object v10

    .line 241
    .local v10, "item":Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iget v2, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nType:I

    iget v3, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nPageNum:I

    iget v4, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nIndex:I

    iget-object v5, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    aget v5, v5, v1

    iget-object v6, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    aget v6, v6, v11

    iget-object v7, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    iget-object v8, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    move v9, v1

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IGotoAnnotation(IIIIFFFFZ)V

    .line 245
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->invalidate()V

    .line 246
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->finish()V

    goto/16 :goto_0

    .line 201
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x14 -> :sswitch_0
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
    .end sparse-switch
.end method

.method public onLocaleChange(I)V
    .locals 3
    .param p1, "nLocale"    # I

    .prologue
    .line 280
    const v0, 0x7f0700ca

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->setTitle(I)V

    .line 281
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->noTocText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/16 v3, 0xa

    .line 458
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->showDialog(I)V

    .line 461
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 463
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p0}, Lcom/infraware/office/evengine/EvInterface;->ISetPdfAnnotListener(Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 467
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 469
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->onSelectAnnotItem()V

    .line 470
    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    sget-object v2, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->dialogId:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 481
    :goto_1
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onResume()V

    .line 482
    return-void

    .line 476
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFAnnotationCount()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 477
    :catch_0
    move-exception v6

    .line 478
    .local v6, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v6}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 464
    .end local v6    # "npe":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public onSelectAnnotItem()V
    .locals 8

    .prologue
    const v7, 0x7f0b0022

    const v6, 0x7f0b0021

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 439
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getSelectCount()I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCountCheckItem:I

    .line 441
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v1, 0x7f07026a

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCountCheckItem:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(Ljava/lang/String;)V

    .line 444
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 445
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCountCheckItem:I

    if-nez v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llDelBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 447
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 448
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 454
    :goto_0
    return-void

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llDelBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 451
    invoke-virtual {p0, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 452
    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method public prevItem()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 381
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 383
    .local v2, "selItem":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 385
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 388
    :cond_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-nez v4, :cond_6

    .line 390
    iget v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    .line 391
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    iput-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 409
    :goto_0
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-nez v4, :cond_1

    .line 411
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 414
    :cond_1
    if-eqz v2, :cond_8

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-eq v2, v4, :cond_8

    .line 415
    invoke-virtual {v2, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setSelected(Z)V

    .line 420
    :goto_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-eqz v4, :cond_2

    .line 421
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v4, v7}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setSelected(Z)V

    .line 423
    :cond_2
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 424
    .local v1, "location":[I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v4, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getLocationOnScreen([I)V

    .line 425
    aget v3, v1, v7

    .line 426
    .local v3, "treeViewTop":I
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    if-eqz v4, :cond_3

    .line 427
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    invoke-virtual {v4, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getLocationOnScreen([I)V

    .line 429
    :cond_3
    aget v0, v1, v7

    .line 431
    .local v0, "currItemTop":I
    if-gt v3, v0, :cond_4

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getHeight()I

    move-result v4

    if-le v0, v4, :cond_5

    .line 432
    :cond_4
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    sub-int v5, v0, v3

    invoke-virtual {v4, v6, v5}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->scrollBy(II)V

    .line 433
    :cond_5
    return-void

    .line 395
    .end local v0    # "currItemTop":I
    .end local v1    # "location":[I
    .end local v3    # "treeViewTop":I
    :cond_6
    iget v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    .line 396
    iget v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    if-gez v4, :cond_7

    .line 399
    iget v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->nCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    .line 400
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    iput-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    goto :goto_0

    .line 405
    :cond_7
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v4, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->selectChild:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    iput-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    goto :goto_0

    .line 418
    :cond_8
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->getImageButton()Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1
.end method
