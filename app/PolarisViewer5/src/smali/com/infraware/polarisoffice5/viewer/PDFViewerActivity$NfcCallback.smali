.class Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;
.super Ljava/lang/Object;
.source "PDFViewerActivity.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NfcCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;


# direct methods
.method private constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V
    .locals 0

    .prologue
    .line 1357
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;
    .param p2, "x1"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$1;

    .prologue
    .line 1357
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 6
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 1378
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1379
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-static {v2}, Lcom/infraware/common/util/SBeamUtils;->isSBeamSupportedDevice(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    .line 1390
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-static {v3}, Lcom/infraware/common/util/SBeamUtils;->isSBeamEnabled(Landroid/content/Context;)Z

    move-result v3

    # setter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSBeamEnabled:Z
    invoke-static {v2, v3}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$802(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;Z)Z

    .line 1393
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$900(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefRecord(Landroid/content/Context;[Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 1396
    .local v1, "record":Landroid/nfc/NdefRecord;
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    .line 1398
    .local v0, "payload":Ljava/lang/String;
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSBeamEnabled:Z
    invoke-static {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$800(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1399
    sget-object v2, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_SBEAM_OFF:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v2}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 1406
    :cond_0
    :goto_0
    invoke-static {v1}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefMessage(Landroid/nfc/NdefRecord;)Landroid/nfc/NdefMessage;

    move-result-object v2

    .line 1409
    .end local v0    # "payload":Ljava/lang/String;
    .end local v1    # "record":Landroid/nfc/NdefRecord;
    :goto_1
    return-object v2

    .line 1401
    .restart local v0    # "payload":Ljava/lang/String;
    .restart local v1    # "record":Landroid/nfc/NdefRecord;
    :cond_1
    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1402
    sget-object v2, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_FILE_NOT_SELECTED:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v2}, Lcom/infraware/common/util/SBeamUtils;->createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;

    move-result-object v1

    goto :goto_0

    .line 1409
    .end local v0    # "payload":Ljava/lang/String;
    .end local v1    # "record":Landroid/nfc/NdefRecord;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 1361
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1362
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-static {v0}, Lcom/infraware/common/util/SBeamUtils;->isSBeamSupportedDevice(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 1364
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSBeamEnabled:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$800(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1365
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    sget-object v1, Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;->POPUP_SBEAM_DISABLED:Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;

    invoke-static {v0, v1}, Lcom/infraware/common/util/SBeamUtils;->startSBeamPopupActivity(Landroid/content/Context;Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;)V

    .line 1374
    :cond_0
    :goto_0
    return-void

    .line 1369
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-static {v0}, Lcom/infraware/common/util/SBeamUtils;->startSBeamDirectShareService(Landroid/content/Context;)V

    goto :goto_0
.end method
