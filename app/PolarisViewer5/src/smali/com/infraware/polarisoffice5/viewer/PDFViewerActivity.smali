.class public Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;
.super Lcom/infraware/office/baseframe/EvBaseViewerActivity;
.source "PDFViewerActivity.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_PEN_MODE;
.implements Lcom/infraware/office/evengine/EvListener$PdfViewerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;,
        Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    }
.end annotation


# static fields
.field private static final BOOST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.twdvfs.DVFS_BOOSTER_PERMISSION"

.field private static final Dictionary_URL_infix_wiki:Ljava/lang/String; = ".wikipedia.org/wiki/"

.field private static final Dictionary_URL_prefix_wiki:Ljava/lang/String; = "http://"

.field private static final MORE_DIC_SEARCH:I = 0x5

.field private static final MORE_GOOGLE_SEARCH:I = 0x3

.field private static final MORE_SHARE:I = 0x2

.field private static final MORE_TTS:I = 0x1

.field private static final MORE_WIKI_SEARCH:I = 0x4

.field private static final NO_MORE:I


# instance fields
.field private final KEY_MAXIMUM_POWER_SAVING_MODE:Ljava/lang/String;

.field private bHasText:Z

.field private bLoadHasText:Z

.field private btnClickListener:Landroid/view/View$OnClickListener;

.field private currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

.field private mActivity:Landroid/app/Activity;

.field private mAnnotNextBtn:Landroid/widget/ImageButton;

.field private mAnnotPrevBtn:Landroid/widget/ImageButton;

.field private mAnnotShow:Ljava/lang/Boolean;

.field private mAnnotText:Landroid/widget/EditText;

.field private mAnnotation:Landroid/view/View;

.field private mFileSaveAs:Ljava/lang/String;

.field private mIsBackPressSave:Z

.field private mLayoutMode:I

.field protected mMenu:Landroid/view/Menu;

.field private mMoreMenu:I

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mNfcCallback:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;

.field private mPosX:I

.field private mPosY:I

.field private mReflowText:Ljava/lang/Boolean;

.field private mSBeamEnabled:Z

.field private mSaveAsWebStorage:Z

.field private mSaveWebStorage:Z

.field private mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

.field private m_ToastMsg:Landroid/widget/Toast;

.field private m_nErrMsgId:I

.field private mbAddSticyNote:Z

.field private nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

.field onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

.field onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;-><init>()V

    .line 74
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    .line 75
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_ToastMsg:Landroid/widget/Toast;

    .line 77
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMenu:Landroid/view/Menu;

    .line 78
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mReflowText:Ljava/lang/Boolean;

    .line 79
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotNextBtn:Landroid/widget/ImageButton;

    .line 80
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotPrevBtn:Landroid/widget/ImageButton;

    .line 81
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    .line 82
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 83
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 84
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 85
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mPosX:I

    .line 86
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mPosY:I

    .line 88
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    .line 89
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    .line 99
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbAddSticyNote:Z

    .line 101
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mLayoutMode:I

    .line 103
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mIsBackPressSave:Z

    .line 105
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFileSaveAs:Ljava/lang/String;

    .line 107
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 108
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;

    .line 109
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSBeamEnabled:Z

    .line 110
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->bHasText:Z

    .line 111
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->bLoadHasText:Z

    .line 115
    const-string/jumbo v0, "maximum_power_saving"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->KEY_MAXIMUM_POWER_SAVING_MODE:Ljava/lang/String;

    .line 118
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSaveWebStorage:Z

    .line 119
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSaveAsWebStorage:Z

    .line 121
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 122
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 124
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    .line 471
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$2;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$2;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 487
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$3;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 503
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$4;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 975
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->btnClickListener:Landroid/view/View$OnClickListener;

    .line 1357
    return-void
.end method

.method private OnReflowText()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 847
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mReflowText:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 848
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mReflowText:Ljava/lang/Boolean;

    .line 849
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onReflow()V

    .line 854
    :goto_0
    return-void

    .line 851
    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mReflowText:Ljava/lang/Boolean;

    .line 852
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onReflow()V

    goto :goto_0
.end method

.method private TitleRename()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 671
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFileSaveAs:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 672
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFileSaveAs:Ljava/lang/String;

    invoke-static {v1}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/actionbar/MainActionBar;->setTitle(Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFileSaveAs:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    .line 674
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->getDocFileExtentionType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mDocExtensionType:I

    .line 676
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v2}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    .line 677
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFileSaveAs:Ljava/lang/String;

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 678
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_STANDARD_UI()Z

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IPDFUpdated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0, v2}, Lcom/infraware/office/actionbar/MainActionBar;->showTitle(Z)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/common/util/SbeamHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    return-object v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    return-object v0
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSBeamEnabled:Z

    return v0
.end method

.method static synthetic access$802(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSBeamEnabled:Z

    return p1
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method private onToastMessage(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 768
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_ToastMsg:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 769
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_ToastMsg:Landroid/widget/Toast;

    .line 773
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_ToastMsg:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 774
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 775
    return-void

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_ToastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method private releaseDvfsLockIntent()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1452
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "FT03"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1453
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "maximum_power_saving"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 1454
    .local v2, "power_mode":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "com.sec.android.app.twdvfs.DVFS_BOOSTER_PERMISSION"

    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 1455
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1456
    .local v0, "dvfsLockIntent":Landroid/content/Intent;
    const-string/jumbo v3, "com.sec.android.intent.action.DVFS_POLARIS_PDF_VIEW"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1457
    const-string/jumbo v3, "PACKAGE"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1458
    const-string/jumbo v3, "ENABLE"

    const-string/jumbo v4, "0"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1459
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1461
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "com.sec.android.intent.action.SURFSETPROP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1462
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "enable"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1463
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1473
    .end local v0    # "dvfsLockIntent":Landroid/content/Intent;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "power_mode":I
    :cond_1
    :goto_0
    return-void

    .line 1467
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1468
    .restart local v0    # "dvfsLockIntent":Landroid/content/Intent;
    const-string/jumbo v3, "com.sec.android.intent.action.DVFS_POLARIS_PDF_VIEW"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1469
    const-string/jumbo v3, "PACKAGE"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1470
    const-string/jumbo v3, "ENABLE"

    const-string/jumbo v4, "0"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1471
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private resultWebStorage(ILandroid/content/Intent;)V
    .locals 1
    .param p1, "resultCode"    # I
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1478
    packed-switch p1, :pswitch_data_0

    .line 1491
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    if-lez v0, :cond_0

    .line 1492
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onToastMessage(I)V

    .line 1493
    :cond_0
    return-void

    .line 1481
    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1484
    :pswitch_1
    const v0, 0x7f070258

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1487
    :pswitch_2
    const v0, 0x7f070265

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_0

    .line 1478
    nop

    :pswitch_data_0
    .packed-switch 0x1002
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setDvfsLockIntent()V
    .locals 6

    .prologue
    .line 1427
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "FT03"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1428
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "maximum_power_saving"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 1429
    .local v2, "power_mode":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "com.sec.android.app.twdvfs.DVFS_BOOSTER_PERMISSION"

    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 1430
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1431
    .local v0, "dvfsLockIntent":Landroid/content/Intent;
    const-string/jumbo v3, "com.sec.android.intent.action.DVFS_POLARIS_PDF_VIEW"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1432
    const-string/jumbo v3, "PACKAGE"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1433
    const-string/jumbo v3, "ENABLE"

    const-string/jumbo v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1434
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1436
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "com.sec.android.intent.action.SURFSETPROP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1437
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "enable"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1438
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1448
    .end local v0    # "dvfsLockIntent":Landroid/content/Intent;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "power_mode":I
    :cond_1
    :goto_0
    return-void

    .line 1442
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1443
    .restart local v0    # "dvfsLockIntent":Landroid/content/Intent;
    const-string/jumbo v3, "com.sec.android.intent.action.DVFS_POLARIS_PDF_VIEW"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1444
    const-string/jumbo v3, "PACKAGE"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1445
    const-string/jumbo v3, "ENABLE"

    const-string/jumbo v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1446
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private setNfcCallback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1341
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 1343
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 1344
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 1345
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;

    invoke-direct {v0, p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;

    .line 1346
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 1347
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 1355
    :cond_0
    :goto_0
    return-void

    .line 1351
    :cond_1
    iput-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 1352
    iput-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$NfcCallback;

    goto :goto_0
.end method


# virtual methods
.method public ActivityMsgProc(IIIIILjava/lang/Object;)I
    .locals 3
    .param p1, "msg"    # I
    .param p2, "p1"    # I
    .param p3, "p2"    # I
    .param p4, "p3"    # I
    .param p5, "p4"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 1206
    sparse-switch p1, :sswitch_data_0

    .line 1247
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    .line 1249
    .end local p6    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v2

    .line 1208
    .restart local p6    # "obj":Ljava/lang/Object;
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_1

    .line 1212
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1213
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 1215
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_2

    .line 1216
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->hidePanel()V

    .line 1217
    :cond_2
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_0

    .line 1220
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    .line 1221
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 1222
    :cond_3
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine$Diotek;->USE_DICTIONARY_SEARCH(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1224
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-nez v0, :cond_4

    .line 1225
    new-instance v0, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;-><init>(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    .line 1228
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_5

    .line 1229
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/MainActionBar;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->setActionbarHeight(I)V

    .line 1232
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1234
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->hideAnnot(Z)V

    goto :goto_0

    .line 1244
    :sswitch_2
    check-cast p6, Landroid/view/KeyEvent;

    .end local p6    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p6}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->processShortCut(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 1206
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x9 -> :sswitch_0
        0x30 -> :sswitch_2
    .end sparse-switch
.end method

.method public FindBarShow(Z)V
    .locals 11
    .param p1, "bShow"    # Z

    .prologue
    const v10, 0x7f0b0119

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1261
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->FindBarShow(Z)V

    .line 1262
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1263
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 1264
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 1265
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 1266
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x3

    move v3, v2

    move v4, v2

    move v6, v5

    move v7, v5

    move v8, v5

    move v9, v2

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IGotoAnnotation(IIIIFFFFZ)V

    .line 1267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 1269
    :cond_0
    return-void
.end method

.method public OnActionBarEvent(I)V
    .locals 2
    .param p1, "eEventType"    # I

    .prologue
    .line 819
    sparse-switch p1, :sswitch_data_0

    .line 841
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    .line 844
    :goto_0
    return-void

    .line 821
    :sswitch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    .line 822
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IAnnotationShow(Z)V

    .line 823
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnReflowText()V

    .line 825
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->hideAnnot(Z)V

    .line 826
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->updateActionBarIcon()V

    goto :goto_0

    .line 829
    :sswitch_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnEndFreeDraw()V

    goto :goto_0

    .line 832
    :sswitch_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onEmailSendDialog()V

    goto :goto_0

    .line 835
    :sswitch_3
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 836
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnPrintActivity()V

    goto :goto_0

    .line 838
    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->OnActionBarEvent(I)V

    goto :goto_0

    .line 819
    nop

    :sswitch_data_0
    .sparse-switch
        0x38 -> :sswitch_0
        0x51 -> :sswitch_3
        0x54 -> :sswitch_1
        0x58 -> :sswitch_2
    .end sparse-switch
.end method

.method public OnAnnotationEvent()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 874
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    .line 875
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->IAnnotationShow(Z)V

    .line 877
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 878
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->hideAnnot(Z)V

    .line 879
    :cond_0
    return-void

    .line 874
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public OnAnnotationListEvent()V
    .locals 2

    .prologue
    .line 884
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mReflowText:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 885
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    .line 892
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 894
    .local v0, "PdfAnnotListIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 895
    return-void

    .line 888
    .end local v0    # "PdfAnnotListIntent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public OnEditCopyCut(IILjava/lang/String;Ljava/lang/String;I)V
    .locals 20
    .param p1, "caller"    # I
    .param p2, "nType"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "data"    # Ljava/lang/String;
    .param p5, "nMode"    # I

    .prologue
    .line 315
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getMWDnD()Lcom/infraware/common/multiwindow/MWDnDOperator;

    move-result-object v4

    move/from16 v5, p1

    move/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v9, p5

    invoke-virtual/range {v4 .. v9}, Lcom/infraware/common/multiwindow/MWDnDOperator;->onEditCopy(IILjava/lang/String;Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    if-eqz v4, :cond_0

    .line 323
    const/16 v18, 0x0

    .line 325
    .local v18, "popup":Landroid/widget/Toast;
    const/4 v13, 0x0

    .line 326
    .local v13, "intent":Landroid/content/Intent;
    const-string/jumbo v11, ""

    .line 327
    .local v11, "TempText":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 328
    move-object/from16 v11, p3

    .line 329
    :cond_2
    if-eqz p4, :cond_3

    .line 330
    move-object/from16 v11, p4

    .line 332
    :cond_3
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    .line 333
    const-string/jumbo v11, " "

    .line 335
    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    packed-switch v4, :pswitch_data_0

    .line 405
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "FT03"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    .line 408
    .local v14, "isSamsung":Z
    packed-switch p5, :pswitch_data_1

    .line 436
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    move/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->OnEditCopyCut(IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 341
    .end local v14    # "isSamsung":Z
    :pswitch_0
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 343
    new-instance v13, Landroid/content/Intent;

    .end local v13    # "intent":Landroid/content/Intent;
    const-string/jumbo v4, "android.intent.action.SEND"

    invoke-direct {v13, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 344
    .restart local v13    # "intent":Landroid/content/Intent;
    const-string/jumbo v4, "text/plain"

    invoke-virtual {v13, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_6

    .line 347
    const-string/jumbo v4, "android.intent.extra.TEXT"

    invoke-virtual {v13, v4, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0702c9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v13, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 355
    :pswitch_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 356
    new-instance v13, Landroid/content/Intent;

    .end local v13    # "intent":Landroid/content/Intent;
    const-string/jumbo v4, "android.intent.action.WEB_SEARCH"

    invoke-direct {v13, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 357
    .restart local v13    # "intent":Landroid/content/Intent;
    const-string/jumbo v4, "query"

    invoke-virtual {v13, v4, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 360
    :catch_0
    move-exception v12

    .line 361
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 366
    .end local v12    # "e":Ljava/lang/Exception;
    :pswitch_2
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 367
    const/4 v10, 0x0

    .line 370
    .local v10, "LookupUrl":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v16

    .line 371
    .local v16, "locale":Ljava/lang/String;
    if-nez v16, :cond_7

    .line 372
    const-string/jumbo v16, "en"

    .line 374
    :cond_7
    if-eqz v11, :cond_0

    .line 376
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".wikipedia.org/wiki/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "UTF-8"

    invoke-static {v11, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 379
    const-string/jumbo v4, "\\+"

    const-string/jumbo v5, "%20"

    invoke-virtual {v10, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v10

    .line 384
    :goto_2
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 385
    .local v19, "uri":Landroid/net/Uri;
    new-instance v15, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.VIEW"

    move-object/from16 v0, v19

    invoke-direct {v15, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 387
    .local v15, "it":Landroid/content/Intent;
    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 388
    :catch_1
    move-exception v12

    .line 389
    .restart local v12    # "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 380
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v15    # "it":Landroid/content/Intent;
    .end local v19    # "uri":Landroid/net/Uri;
    :catch_2
    move-exception v12

    .line 381
    .local v12, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v12}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_2

    .line 396
    .end local v10    # "LookupUrl":Ljava/lang/String;
    .end local v12    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v16    # "locale":Ljava/lang/String;
    :pswitch_3
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 397
    new-instance v17, Landroid/os/Message;

    invoke-direct/range {v17 .. v17}, Landroid/os/Message;-><init>()V

    .line 398
    .local v17, "msg":Landroid/os/Message;
    const/16 v4, 0x378

    move-object/from16 v0, v17

    iput v4, v0, Landroid/os/Message;->what:I

    .line 399
    move-object/from16 v0, v17

    iput-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 400
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->messageHandler:Landroid/os/Handler;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 411
    .end local v17    # "msg":Landroid/os/Message;
    .restart local v14    # "isSamsung":Z
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0700c7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    .line 415
    if-nez v14, :cond_5

    .line 416
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 419
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0700c6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    .line 423
    if-nez v14, :cond_5

    .line 424
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 427
    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07029b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    .line 431
    if-nez v14, :cond_0

    .line 432
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 335
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 408
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public OnLoadComplete(I)V
    .locals 2
    .param p1, "bBookmarkExist"    # I

    .prologue
    .line 1498
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetPDFAnnotationMoveable(Z)V

    .line 1499
    return-void
.end method

.method public OnPDFHyperLink(Ljava/lang/String;IIII)V
    .locals 4
    .param p1, "strString"    # Ljava/lang/String;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 1182
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_HYPERLINK()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1183
    const-string/jumbo v2, "http://"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1184
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1185
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1187
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1201
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 1188
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 1189
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1191
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    const-string/jumbo v2, "mailto:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1192
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SENDTO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1193
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1195
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1196
    :catch_1
    move-exception v0

    .line 1197
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public OnPDFPageRendered(I)V
    .locals 0
    .param p1, "nPageNum"    # I

    .prologue
    .line 1509
    return-void
.end method

.method public OnPDFScreenOffset(II)V
    .locals 1
    .param p1, "offsetX"    # I
    .param p2, "offsetY"    # I

    .prologue
    .line 1504
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseView;->setScreenOffset(II)V

    .line 1505
    return-void
.end method

.method public OnPrintActivity()V
    .locals 3

    .prologue
    .line 811
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/print/PrintActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 812
    .local v0, "printIntent":Landroid/content/Intent;
    const-string/jumbo v1, "key_filename"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 813
    const/16 v1, 0xf

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 814
    return-void
.end method

.method public OnReceiveAnnot()V
    .locals 11

    .prologue
    .line 997
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mIsBackPressSave:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1029
    :cond_0
    :goto_0
    return-void

    .line 1000
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1003
    :cond_2
    const/4 v8, 0x0

    .line 1010
    .local v8, "bUsePropertiesMenu":Z
    const v0, 0x7f0b0119

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/infraware/office/baseframe/EvBaseView;

    .line 1011
    .local v9, "evBaseView":Lcom/infraware/office/baseframe/EvBaseView;
    iget-object v0, v9, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v10, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    .line 1012
    .local v10, "popupMenu":Lcom/infraware/polarisoffice5/common/PopupMenuWindow;
    const/4 v0, 0x4

    new-array v7, v0, [I

    .line 1014
    .local v7, "tempobj":[I
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget v1, v1, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nPageNum:I

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v3, v3, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v4, v4, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget v6, v6, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nType:I

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvInterface;->IPDFMapRectToView(IIIIII[I)V

    .line 1027
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1028
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->printAnnot()V

    goto :goto_0
.end method

.method public OnReceiveNextAnnot(IIIILjava/lang/String;FFFFIII)V
    .locals 0
    .param p1, "nPageNum"    # I
    .param p2, "nIndex"    # I
    .param p3, "nType"    # I
    .param p4, "nStyle"    # I
    .param p5, "strString"    # Ljava/lang/String;
    .param p6, "left"    # F
    .param p7, "top"    # F
    .param p8, "right"    # F
    .param p9, "bottom"    # F
    .param p10, "AnnotItem"    # I
    .param p11, "color"    # I
    .param p12, "Thickness"    # I

    .prologue
    .line 1178
    return-void
.end method

.method public OnReceivePrevAnnot(IIIILjava/lang/String;FFFFIII)V
    .locals 0
    .param p1, "nPageNum"    # I
    .param p2, "nIndex"    # I
    .param p3, "nType"    # I
    .param p4, "nStyle"    # I
    .param p5, "strString"    # Ljava/lang/String;
    .param p6, "left"    # F
    .param p7, "top"    # F
    .param p8, "right"    # F
    .param p9, "bottom"    # F
    .param p10, "AnnotItem"    # I
    .param p11, "color"    # I
    .param p12, "Thickness"    # I

    .prologue
    .line 1154
    return-void
.end method

.method public OnSaveAsDocument(Ljava/lang/String;)V
    .locals 2
    .param p1, "newPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 648
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->showDialog(I)V

    .line 651
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbSaveing:Z

    .line 652
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbIsSavedAs:Z

    .line 653
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFileSaveAs:Ljava/lang/String;

    .line 655
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvInterface;->ISaveDocument(Ljava/lang/String;)V

    .line 660
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->DeleteOpenedFileList(Ljava/lang/String;)Z

    .line 661
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mTempPath:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/infraware/office/evengine/EvInterface;->AddOpendFileList(Ljava/lang/String;Ljava/lang/String;)Z

    .line 662
    return-void
.end method

.method public OnSaveDoc(I)V
    .locals 10
    .param p1, "bOk"    # I

    .prologue
    const v9, 0x7f070230

    const/16 v3, 0xb

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 686
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->isPossibleMakePdf()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 687
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mOfficePrintManager:Lcom/infraware/polarisoffice5/print/OfficePrintManager;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/print/OfficePrintManager;->makePdf()V

    .line 765
    :cond_0
    :goto_0
    return-void

    .line 690
    :cond_1
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbSaveing:Z

    .line 691
    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->messageHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    sget-object v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->dialogId:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->fillMsg(ILjava/lang/String;ILjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 693
    sparse-switch p1, :sswitch_data_0

    .line 759
    const v0, 0x7f070265

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    .line 763
    :cond_2
    :goto_1
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    if-lez v0, :cond_0

    .line 764
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onToastMessage(I)V

    goto :goto_0

    .line 696
    :sswitch_0
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mCheckOriginalKeep:Z

    .line 697
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    .line 698
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbIsSavedAs:Z

    if-ne v0, v8, :cond_3

    .line 699
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFileSaveAs:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrRequestThumbnailPath:Ljava/lang/String;

    .line 701
    :cond_3
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbSaveAndFinish:Z

    if-nez v0, :cond_4

    .line 702
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mRequestThumbnailOnSave:Z

    .line 706
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_5

    .line 707
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbIsSavedAs:Z

    if-eqz v0, :cond_7

    .line 708
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFileSaveAs:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onMediaDBBroadCast(Ljava/lang/String;)V

    .line 713
    :cond_5
    :goto_2
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbIsSavedAs:Z

    .line 715
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    iput-boolean v8, v0, Lcom/infraware/office/actionbar/MainActionBar;->mFileNameShow:Z

    .line 716
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->TitleRename()V

    .line 717
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 718
    invoke-static {}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->getInstance()Lcom/infraware/filemanager/database/recent/RecentFileManager;

    move-result-object v6

    .line 719
    .local v6, "recent":Lcom/infraware/filemanager/database/recent/RecentFileManager;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v6, p0, v0}, Lcom/infraware/filemanager/database/recent/RecentFileManager;->InsertFileInfoToDB(Landroid/content/Context;Ljava/lang/String;)V

    .line 723
    .end local v6    # "recent":Lcom/infraware/filemanager/database/recent/RecentFileManager;
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, v5}, Lcom/infraware/office/baseframe/EvBaseView;->SetOpenType(I)V

    .line 724
    iput v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    .line 725
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mIsBackPressSave:Z

    if-ne v0, v8, :cond_2

    .line 726
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mIsBackPressSave:Z

    .line 727
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->finish()V

    goto :goto_1

    .line 710
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onMediaDBBroadCast(Ljava/lang/String;)V

    goto :goto_2

    .line 734
    :sswitch_1
    const v0, 0x7f07019e

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_1

    .line 738
    :sswitch_2
    iput v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_1

    .line 742
    :sswitch_3
    const v0, 0x7f0700a7

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_1

    .line 746
    :sswitch_4
    iput v9, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_1

    .line 750
    :sswitch_5
    const v0, 0x7f070090

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_1

    .line 754
    :sswitch_6
    iput v9, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_nErrMsgId:I

    goto :goto_1

    .line 693
    :sswitch_data_0
    .sparse-switch
        -0x13 -> :sswitch_0
        -0x12 -> :sswitch_6
        -0x11 -> :sswitch_5
        -0x10 -> :sswitch_4
        -0x1 -> :sswitch_3
        0x1 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_2
    .end sparse-switch
.end method

.method public OnSelectAnnots(IIII)V
    .locals 4
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1051
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mIsBackPressSave:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1063
    :cond_0
    :goto_0
    return-void

    .line 1054
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mFindBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 1057
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 1059
    const v2, 0x7f0b0119

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/office/baseframe/EvBaseView;

    .line 1060
    .local v0, "evBaseView":Lcom/infraware/office/baseframe/EvBaseView;
    iget-object v2, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v1, v2, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    .line 1062
    .local v1, "popupMenu":Lcom/infraware/polarisoffice5/common/PopupMenuWindow;
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->recievePdfSelectAnnots(IIII)V

    goto :goto_0
.end method

.method public OnSetCurrAnnot(IIIIFFFFLjava/lang/String;IIII)V
    .locals 1
    .param p1, "nPageNum"    # I
    .param p2, "nIndex"    # I
    .param p3, "nType"    # I
    .param p4, "nStyle"    # I
    .param p5, "left"    # F
    .param p6, "top"    # F
    .param p7, "right"    # F
    .param p8, "bottom"    # F
    .param p9, "strString"    # Ljava/lang/String;
    .param p10, "AnnotItem"    # I
    .param p11, "color"    # I
    .param p12, "Thickness"    # I
    .param p13, "fillColor"    # I

    .prologue
    .line 928
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    if-nez v0, :cond_0

    .line 929
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 931
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p1, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nPageNum:I

    .line 932
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p2, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nIndex:I

    .line 933
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p3, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nType:I

    .line 934
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p5, v0, Landroid/graphics/RectF;->left:F

    .line 935
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p7, v0, Landroid/graphics/RectF;->right:F

    .line 936
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p6, v0, Landroid/graphics/RectF;->top:F

    .line 937
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p8, v0, Landroid/graphics/RectF;->bottom:F

    .line 938
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput-object p9, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->pText:Ljava/lang/String;

    .line 939
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p10, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->AnnotItem:I

    .line 940
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nStyle:I

    .line 941
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p11, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nColor:I

    .line 942
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p12, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nThickness:I

    .line 944
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p13, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->fillColor:I

    .line 945
    return-void
.end method

.method public OnSetNextAnnot(IIIIFFFFLjava/lang/String;IIII)V
    .locals 2
    .param p1, "nPageNum"    # I
    .param p2, "nIndex"    # I
    .param p3, "nType"    # I
    .param p4, "nStyle"    # I
    .param p5, "left"    # F
    .param p6, "top"    # F
    .param p7, "right"    # F
    .param p8, "bottom"    # F
    .param p9, "strString"    # Ljava/lang/String;
    .param p10, "AnnotItem"    # I
    .param p11, "color"    # I
    .param p12, "Thickness"    # I
    .param p13, "fillColor"    # I

    .prologue
    .line 950
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    if-nez v0, :cond_0

    .line 951
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 953
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p1, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nPageNum:I

    .line 954
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p2, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nIndex:I

    .line 955
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p3, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nType:I

    .line 956
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p5, v0, Landroid/graphics/RectF;->left:F

    .line 957
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p7, v0, Landroid/graphics/RectF;->right:F

    .line 958
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p6, v0, Landroid/graphics/RectF;->top:F

    .line 959
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p8, v0, Landroid/graphics/RectF;->bottom:F

    .line 960
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput-object p9, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->pText:Ljava/lang/String;

    .line 961
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p10, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->AnnotItem:I

    .line 962
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nStyle:I

    .line 963
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p11, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nColor:I

    .line 964
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p12, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nThickness:I

    .line 966
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotNextBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 967
    const v0, 0x7f0b0136

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotNextBtn:Landroid/widget/ImageButton;

    .line 969
    :cond_1
    if-nez p1, :cond_2

    .line 970
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotNextBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 973
    :goto_0
    return-void

    .line 972
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotNextBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public OnSetPrevAnnot(IIIIFFFFLjava/lang/String;IIII)V
    .locals 2
    .param p1, "nPageNum"    # I
    .param p2, "nIndex"    # I
    .param p3, "nType"    # I
    .param p4, "nStyle"    # I
    .param p5, "left"    # F
    .param p6, "top"    # F
    .param p7, "right"    # F
    .param p8, "bottom"    # F
    .param p9, "strString"    # Ljava/lang/String;
    .param p10, "AnnotItem"    # I
    .param p11, "color"    # I
    .param p12, "Thickness"    # I
    .param p13, "fillColor"    # I

    .prologue
    .line 900
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    if-nez v0, :cond_0

    .line 901
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 903
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p1, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nPageNum:I

    .line 904
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p2, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nIndex:I

    .line 905
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p3, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nType:I

    .line 906
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p5, v0, Landroid/graphics/RectF;->left:F

    .line 907
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p7, v0, Landroid/graphics/RectF;->right:F

    .line 908
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p6, v0, Landroid/graphics/RectF;->top:F

    .line 909
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iput p8, v0, Landroid/graphics/RectF;->bottom:F

    .line 910
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput-object p9, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->pText:Ljava/lang/String;

    .line 911
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p10, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->AnnotItem:I

    .line 912
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p4, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nStyle:I

    .line 913
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p11, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nColor:I

    .line 914
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iput p12, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nThickness:I

    .line 916
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotPrevBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 917
    const v0, 0x7f0b0137

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotPrevBtn:Landroid/widget/ImageButton;

    .line 919
    :cond_1
    if-nez p1, :cond_2

    .line 920
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotPrevBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 923
    :goto_0
    return-void

    .line 922
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotPrevBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public OnSingleTap(II)V
    .locals 0
    .param p1, "posX"    # I
    .param p2, "posY"    # I

    .prologue
    .line 1048
    return-void
.end method

.method public addStickyNote()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1082
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mbAddSticyNote:Z

    .line 1083
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mPosX:I

    iget v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mPosY:I

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ICreatePDFStickyNote(II)V

    .line 1084
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    .line 1085
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IAnnotationShow(Z)V

    .line 1086
    return-void
.end method

.method public connectRunShare()V
    .locals 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    if-nez v0, :cond_0

    .line 516
    :goto_0
    return-void

    .line 513
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 514
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public deleteAnnot()V
    .locals 0

    .prologue
    .line 1079
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 1254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mIsBackPressSave:Z

    .line 1255
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->finish()V

    .line 1256
    return-void
.end method

.method public getAnnotShow()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getPageMode()I
    .locals 1

    .prologue
    .line 600
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mLayoutMode:I

    return v0
.end method

.method public getReflowText()Z
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mReflowText:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public hasAnnots()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IHasPDFAnnots()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public hasText()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 636
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public hideAnnot(Z)V
    .locals 11
    .param p1, "bChangeScreen"    # Z

    .prologue
    const v10, 0x7f0b0119

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1119
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1120
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->modifyAnnot()V

    .line 1121
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1122
    iput-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 1123
    iput-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 1124
    iput-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    .line 1126
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x3

    move v3, v2

    move v4, v2

    move v6, v5

    move v7, v5

    move v8, v5

    move v9, v2

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IGotoAnnotation(IIIIFFFFZ)V

    .line 1127
    if-eqz p1, :cond_0

    .line 1128
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0, v10}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 1130
    :cond_0
    return-void
.end method

.method public modifyAnnot()V
    .locals 0

    .prologue
    .line 1070
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 779
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 780
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 781
    packed-switch p1, :pswitch_data_0

    .line 794
    :goto_0
    return-void

    .line 783
    :pswitch_0
    const-string/jumbo v0, "key_new_file"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnSaveAsDocument(Ljava/lang/String;)V

    goto :goto_0

    .line 788
    :cond_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 790
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->resultWebStorage(ILandroid/content/Intent;)V

    goto :goto_0

    .line 781
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch

    .line 788
    :pswitch_data_1
    .packed-switch 0x32
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1274
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 1275
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/infraware/office/util/EvUtil;->hideIme(Landroid/content/Context;Landroid/os/IBinder;)V

    .line 1277
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1278
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 1291
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1292
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->modifyAnnot()V

    .line 1293
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->hideAnnot(Z)V

    .line 1338
    :goto_0
    return-void

    .line 1297
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->FindBarClose()Z

    move-result v0

    if-ne v0, v1, :cond_3

    .line 1298
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    goto :goto_0

    .line 1337
    :cond_3
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onBookmarkMenu()V
    .locals 2

    .prologue
    .line 594
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 596
    .local v0, "PdfBookmarkIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 597
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1414
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1416
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->dismissPopupWindow()Z

    .line 1421
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    if-eqz v0, :cond_0

    .line 1422
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->m_DictionaryPanel:Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/panel/DictionaryPanelMain;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1423
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 293
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onContinuousLayout()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 612
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mLayoutMode:I

    if-nez v0, :cond_0

    .line 613
    iput v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mLayoutMode:I

    .line 614
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->isScroll(Z)V

    .line 615
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetPageMode(I)V

    .line 617
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 128
    iput-object p0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    .line 130
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 131
    const v1, 0x7f03002b

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->setContentView(I)V

    .line 132
    new-instance v1, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    invoke-direct {v1, p0}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 133
    iput v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 135
    const v1, 0x7f0b0133

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    .line 147
    const v1, 0x7f0b0139

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    .line 148
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 150
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 151
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1, p0}, Lcom/infraware/office/baseframe/EvBaseView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 156
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 158
    new-instance v1, Lcom/infraware/common/util/SbeamHelper;

    const-string/jumbo v2, "text/DirectSharePolarisEditor"

    invoke-direct {v1, p0, v2}, Lcom/infraware/common/util/SbeamHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 159
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 160
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    new-instance v1, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$1;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 173
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 178
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->setNfcCallback()V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 288
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 289
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 640
    packed-switch p1, :pswitch_data_0

    .line 644
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 642
    :pswitch_0
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createSaveProgressDlg(Landroid/app/Activity;)Lcom/infraware/polarisoffice5/common/DlgFactory$SaveProgressDlg;

    move-result-object v0

    goto :goto_0

    .line 640
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 182
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/porting/EvClipboardHelper;->thisFinalize()V

    .line 184
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    .line 186
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 188
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onDestroy()V

    .line 191
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 193
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 195
    :cond_1
    return-void
.end method

.method public onEmailSendDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 797
    const/4 v0, 0x0

    .line 798
    .local v0, "bSendFile":Z
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IPDFUpdated()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 799
    const/4 v0, 0x0

    .line 803
    :goto_0
    if-ne v0, v2, :cond_1

    .line 804
    const/4 v1, 0x0

    const/4 v2, 0x7

    invoke-virtual {p0, v1, v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onSendEMail(ZI)Z

    .line 808
    :goto_1
    return-void

    .line 801
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 807
    :cond_1
    const/16 v1, 0x20

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->showDialog(I)V

    goto :goto_1
.end method

.method public onLocaleChange(I)V
    .locals 2
    .param p1, "nLocale"    # I

    .prologue
    const v1, 0x7f0701a9

    .line 305
    const v0, 0x7f0b0135

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 306
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 308
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onLocaleChange(I)V

    .line 309
    return-void
.end method

.method public onMediaDBBroadCast(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 665
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 666
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 668
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 200
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onPause()V

    .line 202
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 207
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->IAnnotationShow(Z)V

    .line 210
    :cond_0
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onResume()V

    .line 213
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/net/Uri;

    .line 216
    .local v0, "filePath":[Landroid/net/Uri;
    const/4 v1, 0x0

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    .line 217
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, p0, v2}, Lcom/infraware/common/util/SbeamHelper;->setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V

    .line 220
    .end local v0    # "filePath":[Landroid/net/Uri;
    :cond_1
    return-void
.end method

.method public onSingleLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 604
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mLayoutMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 605
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mLayoutMode:I

    .line 606
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->isScroll(Z)V

    .line 607
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetPageMode(I)V

    .line 609
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->setDvfsLockIntent()V

    .line 226
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onStart()V

    .line 227
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->releaseDvfsLockIntent()V

    .line 233
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseViewerActivity;->onStop()V

    .line 234
    return-void
.end method

.method public printAnnot()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1093
    const v0, 0x7f0b0119

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/infraware/office/baseframe/EvBaseView;

    .line 1094
    .local v11, "evBaseView":Lcom/infraware/office/baseframe/EvBaseView;
    iget-object v0, v11, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v12, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    .line 1095
    .local v12, "popupMenu":Lcom/infraware/polarisoffice5/common/PopupMenuWindow;
    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 1096
    invoke-virtual {v12}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 1098
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->pText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1099
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1102
    const/4 v10, 0x0

    .line 1103
    .local v10, "bLandScape":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 1104
    const/4 v10, 0x1

    .line 1106
    :cond_1
    invoke-virtual {v11}, Lcom/infraware/office/baseframe/EvBaseView;->getWidth()I

    move-result v0

    if-ltz v0, :cond_2

    invoke-virtual {v11}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    if-ltz v0, :cond_2

    .line 1107
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v11}, Lcom/infraware/office/baseframe/EvBaseView;->getWidth()I

    move-result v2

    invoke-virtual {v11}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotation:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0, v10, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 1108
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget v2, v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nType:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget v3, v3, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nPageNum:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget v4, v4, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nIndex:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v5, v5, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v6, v6, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v7, v7, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->currItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    iget-object v8, v8, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    move v9, v1

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IGotoAnnotation(IIIIFFFFZ)V

    .line 1113
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mQuickScroll:Lcom/infraware/polarisoffice5/common/QuickScrollView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/QuickScrollView;->hide()V

    .line 1115
    return-void
.end method

.method protected processShortCut(Landroid/view/KeyEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 238
    const/16 v0, 0x1000

    .line 240
    .local v0, "METAKEY_CTRL_ON":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v5

    and-int/lit16 v5, v5, 0x1000

    if-eqz v5, :cond_1

    move v2, v3

    .line 241
    .local v2, "isCtrl":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_2

    move v1, v3

    .line 243
    .local v1, "bShift":Z
    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 277
    :cond_0
    :goto_2
    return-void

    .end local v1    # "bShift":Z
    .end local v2    # "isCtrl":Z
    :cond_1
    move v2, v4

    .line 240
    goto :goto_0

    .restart local v2    # "isCtrl":Z
    :cond_2
    move v1, v4

    .line 241
    goto :goto_1

    .line 246
    .restart local v1    # "bShift":Z
    :sswitch_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onHelp()V

    goto :goto_2

    .line 249
    :sswitch_1
    if-eqz v2, :cond_0

    .line 250
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "FT03"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnPrintActivity()V

    goto :goto_2

    .line 255
    :sswitch_2
    if-eqz v2, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->InitFindBar()V

    .line 257
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->FindBarShow(Z)V

    .line 262
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v3}, Lcom/infraware/office/actionbar/MainActionBar;->hide()V

    goto :goto_2

    .line 266
    :sswitch_3
    if-eqz v2, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->selectAll()V

    goto :goto_2

    .line 271
    :sswitch_4
    if-eqz v2, :cond_0

    .line 272
    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v4, v6}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_2

    .line 243
    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_3
        0x1f -> :sswitch_4
        0x22 -> :sswitch_2
        0x2c -> :sswitch_1
        0x83 -> :sswitch_0
    .end sparse-switch
.end method

.method public runCreateHighLight()V
    .locals 2

    .prologue
    .line 567
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ICreatePDFAnnotation(I)V

    .line 568
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnAnnotationEvent()V

    .line 569
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 570
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    .line 571
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IAnnotationShow(Z)V

    .line 573
    :cond_0
    return-void
.end method

.method public runCreateStrikeout()V
    .locals 2

    .prologue
    .line 585
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ICreatePDFAnnotation(I)V

    .line 586
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnAnnotationEvent()V

    .line 587
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 588
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    .line 589
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IAnnotationShow(Z)V

    .line 591
    :cond_0
    return-void
.end method

.method public runCreateUnderline()V
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ICreatePDFAnnotation(I)V

    .line 577
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->OnAnnotationEvent()V

    .line 578
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 579
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    .line 580
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mAnnotShow:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IAnnotationShow(Z)V

    .line 582
    :cond_0
    return-void
.end method

.method public runDictionarySearch()V
    .locals 4

    .prologue
    .line 558
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    if-nez v0, :cond_0

    .line 564
    :goto_0
    return-void

    .line 560
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 561
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public runGoogleSearch()V
    .locals 11

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 519
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x135

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 522
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 523
    .local v0, "inflater1":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 525
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 526
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 527
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 528
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 529
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 546
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 534
    .restart local v0    # "inflater1":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 535
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 536
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onGoolgeSearchClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 540
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 544
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->connectGooleSearch()V

    goto :goto_0
.end method

.method public runShare()V
    .locals 11

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 442
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x134

    invoke-static {v3, v4}, Lcom/infraware/common/define/CMModelDefine;->isShowConnetNetWorkPopup(Landroid/content/Context;I)Z

    move-result v3

    if-ne v3, v8, :cond_2

    .line 445
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 446
    .local v0, "inflater1":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 448
    .local v1, "networkView":Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 449
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0b003c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 450
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectWlan(Landroid/app/Activity;)Z

    move-result v3

    if-ne v3, v8, :cond_0

    .line 451
    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 452
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_WLAN_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    .line 469
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 457
    .restart local v0    # "inflater1":Landroid/view/LayoutInflater;
    .restart local v1    # "networkView":Landroid/view/View;
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getIsConnectMobileNetwork(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 458
    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 459
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_MOBILE_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onRunShareClickListener:Landroid/content/DialogInterface$OnClickListener;

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->onNetWorkUseAlarmCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    aput-object v7, v6, v10

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 463
    :cond_1
    invoke-static {}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->instance()Lcom/infraware/polarisoffice5/common/dialog/DialogManager;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;

    sget-object v5, Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;->ARARM_NETWORK_NOT_USE:Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lcom/infraware/polarisoffice5/common/dialog/DialogManager;->show(Landroid/content/Context;Lcom/infraware/polarisoffice5/common/dialog/DialogManager$DialogType;[Ljava/lang/Object;)V

    goto :goto_0

    .line 467
    .end local v0    # "inflater1":Landroid/view/LayoutInflater;
    .end local v1    # "networkView":Landroid/view/View;
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->connectRunShare()V

    goto :goto_0
.end method

.method public runWikipediaSearch()V
    .locals 4

    .prologue
    .line 549
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mClipBoard:Lcom/infraware/office/baseframe/porting/EvClipboardHelper;

    if-nez v0, :cond_0

    .line 555
    :goto_0
    return-void

    .line 552
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mMoreMenu:I

    .line 553
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IEditDocument(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public selectAll()V
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISelectAll()V

    .line 1090
    return-void
.end method

.method public setEvListener()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 281
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 283
    :cond_0
    return-void
.end method
