.class Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;
.super Ljava/lang/Object;
.source "PDFBookmarkActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

.field final synthetic val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;Lcom/infraware/polarisoffice5/viewer/TreeElement;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    iput-object p2, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 239
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getBookmarkType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 240
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "url":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 242
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 244
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    invoke-virtual {v2, v0}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->startActivity(Landroid/content/Intent;)V

    .line 249
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "url":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->this$1:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    iget-object v2, v2, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->finish()V

    .line 250
    return-void

    .line 245
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getBookmarkType()I

    move-result v2

    if-nez v2, :cond_0

    .line 246
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter$1;->val$treeElement:Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getItem()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->IGotoPDFBookmark(J)V

    goto :goto_0
.end method
