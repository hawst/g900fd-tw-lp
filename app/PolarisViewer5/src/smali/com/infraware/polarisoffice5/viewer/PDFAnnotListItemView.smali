.class public Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
.super Landroid/widget/LinearLayout;
.source "PDFAnnotListItemView.java"


# static fields
.field public static focuseView:Landroid/view/View;

.field public static touchView:Landroid/view/View;


# instance fields
.field private m_Activity:Landroid/app/Activity;

.field private m_SelectBtn:Landroid/widget/CheckBox;

.field private m_oData:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;

.field private m_oitemView:Landroid/view/View;

.field private m_olistener:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    sput-object v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->focuseView:Landroid/view/View;

    .line 20
    sput-object v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->touchView:Landroid/view/View;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oitemView:Landroid/view/View;

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oData:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;

    .line 25
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_Activity:Landroid/app/Activity;

    .line 26
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_SelectBtn:Landroid/widget/CheckBox;

    .line 28
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_olistener:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oitemView:Landroid/view/View;

    .line 24
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oData:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;

    .line 25
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_Activity:Landroid/app/Activity;

    .line 26
    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_SelectBtn:Landroid/widget/CheckBox;

    .line 28
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_olistener:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;

    .line 50
    return-void
.end method

.method public static createItemView(Landroid/content/Context;ILcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;Landroid/app/Activity;)Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resid"    # I
    .param p2, "item"    # Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    .param p3, "a_Activity"    # Landroid/app/Activity;

    .prologue
    const/4 v6, 0x0

    const v11, 0x7f0b001c

    const v10, 0x7f0b0017

    .line 96
    const/4 v5, 0x0

    .line 97
    .local v5, "v":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    const/4 v4, 0x0

    .line 98
    .local v4, "txt":Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 100
    .local v0, "btn":Landroid/widget/ImageView;
    if-eqz p1, :cond_0

    .line 101
    invoke-static {p0, p1, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    check-cast v5, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 102
    .restart local v5    # "v":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    const v6, 0x7f0b0019

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "txt":Landroid/widget/TextView;
    check-cast v4, Landroid/widget/TextView;

    .line 103
    .restart local v4    # "txt":Landroid/widget/TextView;
    const v6, 0x7f0b0018

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "btn":Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    .line 105
    .restart local v0    # "btn":Landroid/widget/ImageView;
    iget v6, p2, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nStyle:I

    sparse-switch v6, :sswitch_data_0

    .line 148
    :goto_0
    iget-wide v1, p2, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nTime:J

    .line 149
    .local v1, "ntime":J
    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v1

    invoke-static {p0, v6, v7}, Lcom/infraware/common/util/FileUtils;->getDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, "time":Ljava/lang/String;
    const v6, 0x7f0b001a

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    const v6, 0x7f0b001b

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p2, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nPageNum:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0700d5

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    invoke-virtual {v5, v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    invoke-virtual {v5, v6}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setSelectBtn(Landroid/widget/CheckBox;)V

    .line 156
    invoke-virtual {v5, v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 160
    invoke-virtual {v5, v4}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setItemView(Landroid/view/View;)V

    .line 163
    invoke-virtual {v5, p2}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setData(Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V

    .line 165
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 166
    invoke-virtual {v5, v10}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 167
    iget-object v6, p2, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->pText:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    invoke-virtual {v5, v10}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getListener()Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    invoke-virtual {v5, v10}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getListener()Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 179
    invoke-virtual {v5, v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getListener()Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    invoke-virtual {v5, p3}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->setActivity(Landroid/app/Activity;)V

    move-object v6, v5

    .line 182
    .end local v1    # "ntime":J
    .end local v3    # "time":Ljava/lang/String;
    :cond_0
    return-object v6

    .line 107
    :sswitch_0
    const v6, 0x7f020101

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 110
    :sswitch_1
    const v6, 0x7f020111

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 113
    :sswitch_2
    iget v6, p2, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nLineStyle:I

    if-eqz v6, :cond_1

    .line 114
    const v6, 0x7f0200fa

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 116
    :cond_1
    const v6, 0x7f020108

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 119
    :sswitch_3
    const v6, 0x7f0200fd

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 122
    :sswitch_4
    const v6, 0x7f020110

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 125
    :sswitch_5
    const v6, 0x7f02010e

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 128
    :sswitch_6
    const v6, 0x7f020100

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 131
    :sswitch_7
    const v6, 0x7f02010f

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 134
    :sswitch_8
    iget v6, p2, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nSubType:I

    if-eqz v6, :cond_2

    .line 135
    const v6, 0x7f0200f7

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 137
    :cond_2
    const v6, 0x7f020102

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 140
    :sswitch_9
    const v6, 0x7f02010b

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 143
    :sswitch_a
    const v6, 0x7f020105

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 105
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_3
        0xc -> :sswitch_4
        0xd -> :sswitch_5
        0xf -> :sswitch_6
        0x1a -> :sswitch_7
        0x1b -> :sswitch_8
        0x1c -> :sswitch_9
        0x1d -> :sswitch_a
    .end sparse-switch
.end method


# virtual methods
.method public dipToPx(F)F
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 186
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_Activity:Landroid/app/Activity;

    return-object v0
.end method

.method public getData()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oData:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->getItem()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    move-result-object v0

    return-object v0
.end method

.method public getItemView()Landroid/view/View;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oitemView:Landroid/view/View;

    return-object v0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 190
    const v0, 0x7f030002

    return v0
.end method

.method public getListener()Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_olistener:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;

    return-object v0
.end method

.method public getSelectBtn()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_SelectBtn:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "a_Activity"    # Landroid/app/Activity;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_Activity:Landroid/app/Activity;

    .line 63
    return-void
.end method

.method public setData(Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V
    .locals 1
    .param p1, "data"    # Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oData:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oData:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oData:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->setItem(Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V

    .line 78
    return-void
.end method

.method public setItemView(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_oitemView:Landroid/view/View;

    .line 54
    return-void
.end method

.method public setSelectBtn(Landroid/widget/CheckBox;)V
    .locals 0
    .param p1, "btn"    # Landroid/widget/CheckBox;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->m_SelectBtn:Landroid/widget/CheckBox;

    .line 82
    return-void
.end method
