.class public Lcom/infraware/polarisoffice5/viewer/TreeElement;
.super Ljava/lang/Object;
.source "TreeElement.java"


# instance fields
.field private BookmarkType:I

.field private color:[F

.field private expanded:Z

.field private font_style:I

.field private level:I

.field private mItem:J

.field private mTitle:Ljava/lang/String;

.field private mURL:Ljava/lang/String;

.field private mhasChild:Z

.field private nTitleLen:I

.field private nURLLen:I


# direct methods
.method public constructor <init>(Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V
    .locals 5
    .param p1, "item"    # Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mItem:J

    .line 9
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->nTitleLen:I

    .line 10
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->BookmarkType:I

    .line 11
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->nURLLen:I

    .line 12
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->font_style:I

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->color:[F

    .line 16
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mhasChild:Z

    .line 24
    iput v2, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->level:I

    .line 25
    iget-boolean v0, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->HasKids:Z

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mhasChild:Z

    .line 26
    iget-object v0, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->szTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mTitle:Ljava/lang/String;

    .line 27
    iget-wide v0, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->item:J

    iput-wide v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mItem:J

    .line 28
    iget v0, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->nTitleLen:I

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->nTitleLen:I

    .line 29
    iget v0, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->BookmarkType:I

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->BookmarkType:I

    .line 30
    iget v0, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->nURLLen:I

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->nURLLen:I

    .line 31
    iget-object v0, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->szURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mURL:Ljava/lang/String;

    .line 32
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->color:[F

    iget-object v1, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->color:[F

    aget v1, v1, v2

    aput v1, v0, v2

    .line 33
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->color:[F

    iget-object v1, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->color:[F

    aget v1, v1, v3

    aput v1, v0, v3

    .line 34
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->color:[F

    iget-object v1, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->color:[F

    aget v1, v1, v4

    aput v1, v0, v4

    .line 35
    iget v0, p1, Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;->font_style:I

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->font_style:I

    .line 36
    return-void
.end method


# virtual methods
.method public getBookmarkType()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->BookmarkType:I

    return v0
.end method

.method public getItem()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mItem:J

    return-wide v0
.end method

.method public getLevel()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->level:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mURL:Ljava/lang/String;

    return-object v0
.end method

.method public hasChild()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->mhasChild:Z

    return v0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->expanded:Z

    return v0
.end method

.method public setExpanded(Z)V
    .locals 0
    .param p1, "expanded"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->expanded:Z

    .line 72
    return-void
.end method

.method public setLevel(I)V
    .locals 0
    .param p1, "level"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/infraware/polarisoffice5/viewer/TreeElement;->level:I

    .line 64
    return-void
.end method
