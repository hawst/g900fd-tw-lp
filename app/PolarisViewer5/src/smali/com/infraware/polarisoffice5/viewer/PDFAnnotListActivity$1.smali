.class Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$1;
.super Ljava/lang/Object;
.source "PDFAnnotListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$1;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$1;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->deleteSelectedAnnotation()V

    .line 83
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFAnnotationCount()V

    .line 84
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$1;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->treeView:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListView;->setDeleteMode(Z)V

    .line 86
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity$1;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 88
    return-void
.end method
