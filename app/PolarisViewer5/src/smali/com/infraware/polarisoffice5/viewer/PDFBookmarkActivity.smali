.class public Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;
.super Lcom/infraware/common/baseactivity/BaseSwitchableActivity;
.source "PDFBookmarkActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;,
        Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;
    }
.end annotation


# instance fields
.field private mTocDataSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/polarisoffice5/viewer/TreeElement;",
            ">;"
        }
    .end annotation
.end field

.field private mTreeViewAdapter:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

.field private m_oCloseFilter:Landroid/content/IntentFilter;

.field private m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;

    .line 35
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    .line 38
    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTreeViewAdapter:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    .line 189
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;Lcom/infraware/polarisoffice5/viewer/TreeElement;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;
    .param p1, "x1"    # Lcom/infraware/polarisoffice5/viewer/TreeElement;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->collapse(Lcom/infraware/polarisoffice5/viewer/TreeElement;)V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;Lcom/infraware/polarisoffice5/viewer/TreeElement;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;
    .param p1, "x1"    # Lcom/infraware/polarisoffice5/viewer/TreeElement;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->expand(Lcom/infraware/polarisoffice5/viewer/TreeElement;)V

    return-void
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;)Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTreeViewAdapter:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    return-object v0
.end method

.method private collapse(Lcom/infraware/polarisoffice5/viewer/TreeElement;)V
    .locals 5
    .param p1, "treeElement"    # Lcom/infraware/polarisoffice5/viewer/TreeElement;

    .prologue
    .line 173
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->setExpanded(Z)V

    .line 175
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 177
    .local v1, "position":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v2, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/polarisoffice5/viewer/TreeElement;>;"
    add-int/lit8 v0, v1, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 180
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getLevel()I

    move-result v4

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/infraware/polarisoffice5/viewer/TreeElement;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getLevel()I

    move-result v3

    if-lt v4, v3, :cond_1

    .line 186
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 187
    return-void

    .line 183
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private expand(Lcom/infraware/polarisoffice5/viewer/TreeElement;)V
    .locals 11
    .param p1, "treeElement"    # Lcom/infraware/polarisoffice5/viewer/TreeElement;

    .prologue
    const/4 v10, 0x0

    .line 147
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->setExpanded(Z)V

    .line 148
    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getLevel()I

    move-result v3

    .line 149
    .local v3, "level":I
    add-int/lit8 v4, v3, 0x1

    .line 151
    .local v4, "nextLevel":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 153
    .local v5, "position":I
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v7

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getItem()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFBookmarkCount(J)I

    move-result v0

    .line 154
    .local v0, "childNodeCount":I
    new-array v6, v0, [Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    .line 156
    .local v6, "subItem":[Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v7, v6

    if-ge v2, v7, :cond_0

    .line 157
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v7

    invoke-virtual {v7}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v7

    invoke-virtual {v7}, Lcom/infraware/office/evengine/EV;->getPdfBookmarkListItem()Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    move-result-object v7

    aput-object v7, v6, v2

    .line 156
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 160
    :cond_0
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v7

    invoke-virtual {p1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getItem()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9, v10, v6}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFBookmarkList(JI[Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V

    .line 162
    add-int/lit8 v2, v0, -0x1

    :goto_1
    if-ltz v2, :cond_2

    .line 163
    aget-object v7, v6, v2

    if-eqz v7, :cond_1

    .line 164
    new-instance v1, Lcom/infraware/polarisoffice5/viewer/TreeElement;

    aget-object v7, v6, v2

    invoke-direct {v1, v7}, Lcom/infraware/polarisoffice5/viewer/TreeElement;-><init>(Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V

    .line 165
    .local v1, "element":Lcom/infraware/polarisoffice5/viewer/TreeElement;
    invoke-virtual {v1, v4}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->setLevel(I)V

    .line 166
    invoke-virtual {v1, v10}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->setExpanded(Z)V

    .line 167
    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    add-int/lit8 v8, v5, 0x1

    invoke-virtual {v7, v8, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 162
    .end local v1    # "element":Lcom/infraware/polarisoffice5/viewer/TreeElement;
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 170
    :cond_2
    return-void
.end method

.method private initialRoot(I)V
    .locals 7
    .param p1, "rootNodeCount"    # I

    .prologue
    .line 127
    new-array v2, p1, [Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    .line 129
    .local v2, "rootNode":[Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 130
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v3

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EV;->getPdfBookmarkListItem()Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;

    move-result-object v3

    aput-object v3, v2, v1

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 133
    :cond_0
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    const-wide/16 v4, 0x0

    array-length v6, v2

    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFBookmarkList(JI[Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V

    .line 135
    const/4 v1, 0x0

    :goto_1
    if-ge v1, p1, :cond_2

    .line 136
    aget-object v3, v2, v1

    if-eqz v3, :cond_1

    .line 137
    new-instance v0, Lcom/infraware/polarisoffice5/viewer/TreeElement;

    aget-object v3, v2, v1

    invoke-direct {v0, v3}, Lcom/infraware/polarisoffice5/viewer/TreeElement;-><init>(Lcom/infraware/office/evengine/EV$PDF_BOOKMARK_LIST_ITEM;)V

    .line 138
    .local v0, "element":Lcom/infraware/polarisoffice5/viewer/TreeElement;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->expand(Lcom/infraware/polarisoffice5/viewer/TreeElement;)V

    .line 135
    .end local v0    # "element":Lcom/infraware/polarisoffice5/viewer/TreeElement;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 144
    :cond_2
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 42
    invoke-super {p0, p1}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v4, 0x7f030034

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->setContentView(I)V

    .line 45
    new-instance v4, Lcom/infraware/office/actionbar/ActionTitleBar;

    const v5, 0x7f0b001d

    invoke-direct {v4, p0, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;-><init>(Landroid/app/Activity;I)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 46
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    const v5, 0x7f0700cb

    invoke-virtual {v4, v5}, Lcom/infraware/office/actionbar/ActionTitleBar;->setTitle(I)V

    .line 47
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v4}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 49
    const v4, 0x7f0b0184

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    .line 50
    .local v3, "tocTreeLayout":Landroid/widget/FrameLayout;
    const v4, 0x7f0b0186

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 54
    .local v0, "emptyLayout":Landroid/widget/FrameLayout;
    const/4 v2, 0x0

    .line 55
    .local v2, "rootNodeCount":I
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 56
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v4

    const-wide/16 v5, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/infraware/office/evengine/EvInterface;->IGetPDFBookmarkCount(J)I

    move-result v2

    .line 58
    :cond_0
    if-lez v2, :cond_1

    .line 59
    invoke-virtual {v3, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 60
    invoke-virtual {v0, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 62
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->initialRoot(I)V

    .line 64
    const v4, 0x7f0b0185

    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 65
    .local v1, "mTocListView":Landroid/widget/ListView;
    new-instance v4, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    const v5, 0x7f030033

    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;

    invoke-direct {v4, p0, p0, v5, v6}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTreeViewAdapter:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    .line 66
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTreeViewAdapter:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$TreeViewAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 67
    invoke-virtual {v1, v7}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 70
    new-instance v4, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$1;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$1;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;)V

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 98
    .end local v1    # "mTocListView":Landroid/widget/ListView;
    :goto_0
    new-instance v4, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;

    invoke-direct {v4, p0}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;-><init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;)V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;

    .line 99
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    .line 100
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    const-string/jumbo v5, "com.infraware.polarisviewer5.CLOSE"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 101
    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->m_oCloseReceiver:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$CloseActionReceiver;

    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->m_oCloseFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v4, v5}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 102
    return-void

    .line 93
    :cond_1
    invoke-virtual {v3, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 94
    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->onDestroy()V

    .line 107
    return-void
.end method

.method public onLocaleChange(I)V
    .locals 0
    .param p1, "nLocale"    # I

    .prologue
    .line 111
    return-void
.end method
