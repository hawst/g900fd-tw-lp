.class public Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;
.super Ljava/lang/Object;
.source "PDFAnnotListItem.java"


# instance fields
.field public AnnotItem:I

.field public nIndex:I

.field public nPageNum:I

.field public nType:I

.field public pText:Ljava/lang/String;

.field public rect:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->AnnotItem:I

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nType:I

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nIndex:I

    iput v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nPageNum:I

    .line 23
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->pText:Ljava/lang/String;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->rect:[F

    .line 25
    return-void
.end method


# virtual methods
.method public getItem()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EV;->getPdfAnnotationListItem()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    move-result-object v0

    .line 42
    .local v0, "result":Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    iget v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nPageNum:I

    iput v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nPageNum:I

    .line 43
    iget v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nIndex:I

    iput v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nIndex:I

    .line 44
    iget v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nType:I

    iput v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nType:I

    .line 45
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->pText:Ljava/lang/String;

    iput-object v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->pText:Ljava/lang/String;

    .line 46
    iget v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->AnnotItem:I

    iput v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->AnnotItem:I

    .line 47
    iget-object v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->rect:[F

    aget v2, v2, v3

    aput v2, v1, v3

    .line 48
    iget-object v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->rect:[F

    aget v2, v2, v4

    aput v2, v1, v4

    .line 49
    iget-object v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->rect:[F

    aget v2, v2, v5

    aput v2, v1, v5

    .line 50
    iget-object v1, v0, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->rect:[F

    aget v2, v2, v6

    aput v2, v1, v6

    .line 52
    return-object v0
.end method

.method public setItem(Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;)V
    .locals 4
    .param p1, "a_item"    # Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    .prologue
    const/4 v3, 0x4

    .line 28
    iget v1, p1, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nPageNum:I

    iput v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nPageNum:I

    .line 29
    iget v1, p1, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nIndex:I

    iput v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nIndex:I

    .line 30
    iget v1, p1, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nType:I

    iput v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->nType:I

    .line 31
    iget-object v1, p1, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->pText:Ljava/lang/String;

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->pText:Ljava/lang/String;

    .line 32
    iget v1, p1, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->AnnotItem:I

    iput v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->AnnotItem:I

    .line 33
    new-array v1, v3, [F

    iput-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->rect:[F

    .line 34
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 35
    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItem;->rect:[F

    iget-object v2, p1, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    aget v2, v2, v0

    aput v2, v1, v0

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_0
    return-void
.end method
