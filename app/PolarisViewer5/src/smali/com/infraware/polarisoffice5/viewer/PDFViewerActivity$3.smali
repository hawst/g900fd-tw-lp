.class Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$3;
.super Ljava/lang/Object;
.source "PDFViewerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$3;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 490
    packed-switch p2, :pswitch_data_0

    .line 500
    :goto_0
    return-void

    .line 493
    :pswitch_0
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$3;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$200(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x135

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$3;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$400(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/infraware/common/config/RuntimeConfig;->setBooleanPreference(Landroid/content/Context;IZ)V

    .line 494
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$3;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->connectGooleSearch()V

    goto :goto_0

    .line 490
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
