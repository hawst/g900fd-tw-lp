.class Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$1;
.super Ljava/lang/Object;
.source "PDFBookmarkActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->mTocDataSet:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->access$000(Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/viewer/TreeElement;

    .line 78
    .local v1, "treeElement":Lcom/infraware/polarisoffice5/viewer/TreeElement;
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getBookmarkType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getUrl()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 79
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "url":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 83
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    invoke-virtual {v3, v0}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->startActivity(Landroid/content/Intent;)V

    .line 88
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "url":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity$1;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/viewer/PDFBookmarkActivity;->finish()V

    .line 89
    return-void

    .line 84
    :cond_1
    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getBookmarkType()I

    move-result v3

    if-nez v3, :cond_0

    .line 85
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v3

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/viewer/TreeElement;->getItem()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/infraware/office/evengine/EvInterface;->IGotoPDFBookmark(J)V

    goto :goto_0
.end method
