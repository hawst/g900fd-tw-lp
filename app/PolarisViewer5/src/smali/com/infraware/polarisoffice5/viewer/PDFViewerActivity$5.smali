.class Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;
.super Ljava/lang/Object;
.source "PDFViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)V
    .locals 0

    .prologue
    .line 975
    iput-object p1, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    .line 979
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 993
    :goto_0
    return-void

    .line 981
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$500(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v2

    iget v2, v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nType:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$500(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v3

    iget v3, v3, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nPageNum:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$500(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v4

    iget v4, v4, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nIndex:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$500(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v5

    iget-object v5, v5, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$500(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v6

    iget-object v6, v6, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$500(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v7

    iget-object v7, v7, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->prevItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$500(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v8

    iget-object v8, v8, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IGotoAnnotation(IIIIFFFFZ)V

    goto :goto_0

    .line 987
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$600(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v2

    iget v2, v2, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nType:I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$600(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v3

    iget v3, v3, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nPageNum:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$600(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v4

    iget v4, v4, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->nIndex:I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$600(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v5

    iget-object v5, v5, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v6}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$600(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v6

    iget-object v6, v6, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v7}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$600(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v7

    iget-object v7, v7, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$5;->this$0:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;

    # getter for: Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->nextItem:Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;
    invoke-static {v8}, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;->access$600(Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity;)Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;

    move-result-object v8

    iget-object v8, v8, Lcom/infraware/polarisoffice5/viewer/PDFViewerActivity$AnnotItem;->rect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IGotoAnnotation(IIIIFFFFZ)V

    goto/16 :goto_0

    .line 979
    :pswitch_data_0
    .packed-switch 0x7f0b0136
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
