.class public Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemViewEventListener;
.super Ljava/lang/Object;
.source "PDFAnnotListItemViewEventListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 24
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 26
    .local v11, "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    :cond_1
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getData()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    move-result-object v10

    .line 34
    .local v10, "item":Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iget v2, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nType:I

    iget v3, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nPageNum:I

    iget v4, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nIndex:I

    iget-object v5, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    aget v5, v5, v1

    iget-object v6, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    iget-object v7, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    iget-object v8, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    move v9, v1

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IGotoAnnotation(IIIIFFFFZ)V

    .line 38
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 39
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 44
    .end local v10    # "item":Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    .end local v11    # "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 45
    .restart local v11    # "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->onSelectAnnotItem()V

    goto :goto_0

    .line 22
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0017 -> :sswitch_0
        0x7f0b001c -> :sswitch_1
    .end sparse-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v2, 0x7f0b001c

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 89
    :goto_0
    return v1

    .line 56
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 58
    :pswitch_2
    invoke-virtual {p1, v6}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 61
    :pswitch_3
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 64
    :pswitch_4
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;

    .line 67
    .local v11, "itemv":Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    iget-object v0, v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->llBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 70
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v6

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 74
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListActivity;->onSelectAnnotItem()V

    goto :goto_0

    :cond_0
    move v2, v1

    .line 70
    goto :goto_1

    .line 77
    :cond_1
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getData()Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;

    move-result-object v10

    .line 78
    .local v10, "item":Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iget v2, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nType:I

    iget v3, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nPageNum:I

    iget v4, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->nIndex:I

    iget-object v5, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    aget v5, v5, v1

    iget-object v7, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    aget v6, v7, v6

    iget-object v7, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    iget-object v8, v10, Lcom/infraware/office/evengine/EV$PDF_ANNOT_ITEM;->rect:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    move v9, v1

    invoke-virtual/range {v0 .. v9}, Lcom/infraware/office/evengine/EvInterface;->IGotoAnnotation(IIIIFFFFZ)V

    .line 82
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 83
    invoke-virtual {v11}, Lcom/infraware/polarisoffice5/viewer/PDFAnnotListItemView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 54
    :pswitch_data_0
    .packed-switch 0x7f0b0017
        :pswitch_0
    .end packed-switch

    .line 56
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
