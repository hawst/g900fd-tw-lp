.class Lcom/infraware/polarisoffice5/OfficeWebView$1;
.super Landroid/os/Handler;
.source "OfficeWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/OfficeWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/OfficeWebView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/OfficeWebView;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 69
    if-nez p1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 75
    :sswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_pvLoading:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$100(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_pvLoading:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$100(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 80
    :sswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_pvLoading:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$100(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_pvLoading:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$100(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 85
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oToastMsg:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$200(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/Toast;

    move-result-object v0

    if-nez v0, :cond_2

    .line 88
    iget-object v1, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    # setter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oToastMsg:Landroid/widget/Toast;
    invoke-static {v1, v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$202(Lcom/infraware/polarisoffice5/OfficeWebView;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 92
    :goto_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oToastMsg:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$200(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/OfficeWebView$1;->this$0:Lcom/infraware/polarisoffice5/OfficeWebView;

    # getter for: Lcom/infraware/polarisoffice5/OfficeWebView;->m_oToastMsg:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/OfficeWebView;->access$200(Lcom/infraware/polarisoffice5/OfficeWebView;)Landroid/widget/Toast;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 72
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x63 -> :sswitch_2
    .end sparse-switch
.end method
