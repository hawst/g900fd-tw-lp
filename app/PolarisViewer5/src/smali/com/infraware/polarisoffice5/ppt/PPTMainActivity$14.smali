.class Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$14;
.super Ljava/lang/Object;
.source "PPTMainActivity.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onClickSlideShow(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0

    .prologue
    .line 2195
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$14;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2197
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$14;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 2198
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$14;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$000(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 2199
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$14;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$902(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;

    .line 2200
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$14;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    const/4 v1, 0x1

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$2402(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z

    .line 2201
    return-void
.end method
