.class public interface abstract Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;
.super Ljava/lang/Object;
.source "SlideShowGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SlideShowGestureDetectorListener"
.end annotation


# virtual methods
.method public abstract onDoubleTap(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end method

.method public abstract onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onTouchDown(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onTouchMove(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
.end method

.method public abstract onTouchUp(Landroid/view/MotionEvent;)Z
.end method
