.class Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$1;
.super Ljava/lang/Object;
.source "SlideDragNDropListView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 100
    const-string/jumbo v0, "SlideDragNDropListView"

    const-string/jumbo v1, "AnimationListener.onAnimationEnd"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$000(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$000(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 103
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mSelectView:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$002(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;Landroid/view/View;)Landroid/view/View;

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Landroid/os/Handler;

    move-result-object v0

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->END_LONG_PRESS:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$100()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 106
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 96
    const-string/jumbo v0, "SlideDragNDropListView"

    const-string/jumbo v1, "AnimationListener.onAnimationRepeat"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 92
    const-string/jumbo v0, "SlideDragNDropListView"

    const-string/jumbo v1, "AnimationListener.onAnimationStart"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    return-void
.end method
