.class public Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;
.super Ljava/lang/Object;
.source "SlideShowGestureDetector.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;
    }
.end annotation


# static fields
.field private static final DOUBLE_TAP_TIMEOUT:I

.field private static final TAP:I = 0x1


# instance fields
.field private mAlwaysInBiggerTapRegion:Z

.field private mAlwaysInMoveRegion:Z

.field private mAlwaysInTapRegion:Z

.field private mBiggerTouchSlopSquare:I

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mDoubleTapSlopSquare:I

.field private mHandler:Landroid/os/Handler;

.field private mIsDoubleTapping:Z

.field private mIsDoubleTappingEnabled:Z

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

.field private mMaximumFlingVelocity:I

.field private mPreviousUpEvent:Landroid/view/MotionEvent;

.field private mStillDown:Z

.field private mTouchSlopSquare:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;Z)V
    .locals 5
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "listener"    # Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;
    .param p3, "isDoubleTappingEnabled"    # Z

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v3, 0x190

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mBiggerTouchSlopSquare:I

    .line 28
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInMoveRegion:Z

    .line 29
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mIsDoubleTappingEnabled:Z

    .line 44
    new-instance v3, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$1;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$1;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)V

    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mHandler:Landroid/os/Handler;

    .line 59
    if-nez p2, :cond_0

    .line 60
    new-instance v3, Ljava/lang/NullPointerException;

    const-string/jumbo v4, "EvAdvanceGestureDetector must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 62
    :cond_0
    iput-object p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    .line 63
    iput-boolean p3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mIsDoubleTappingEnabled:Z

    .line 65
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 66
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    .line 67
    .local v2, "touchSlop":I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    .line 68
    .local v1, "doubleTapSlop":I
    mul-int v3, v2, v2

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mTouchSlopSquare:I

    .line 69
    mul-int v3, v1, v1

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mDoubleTapSlopSquare:I

    .line 70
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mMaximumFlingVelocity:I

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    .prologue
    .line 12
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mStillDown:Z

    return v0
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method private isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "firstDown"    # Landroid/view/MotionEvent;
    .param p2, "firstUp"    # Landroid/view/MotionEvent;
    .param p3, "secondDown"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 194
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInBiggerTapRegion:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mIsDoubleTappingEnabled:Z

    if-nez v3, :cond_1

    .line 201
    :cond_0
    :goto_0
    return v2

    .line 196
    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    sget v5, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    .line 199
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    sub-int v0, v3, v4

    .line 200
    .local v0, "deltaX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    sub-int v1, v3, v4

    .line 201
    .local v1, "deltaY":I
    mul-int v3, v0, v0

    mul-int v4, v1, v1

    add-int/2addr v3, v4

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mDoubleTapSlopSquare:I

    if-ge v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public GestureDetectorFinalize()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    .line 75
    return-void
.end method

.method public SetAlwaysInMoveRegion(Z)V
    .locals 0
    .param p1, "alwaysInMoveRegion"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInMoveRegion:Z

    .line 79
    return-void
.end method

.method public cancel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 184
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 185
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 187
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mIsDoubleTapping:Z

    .line 188
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mStillDown:Z

    .line 189
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInTapRegion:Z

    .line 190
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 191
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 22
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 83
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 84
    .local v4, "action":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v17

    .line 85
    .local v17, "y":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v16

    .line 86
    .local v16, "x":F
    const/4 v10, 0x0

    .line 88
    .local v10, "handled":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    if-nez v18, :cond_0

    .line 89
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 91
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 93
    and-int/lit16 v0, v4, 0xff

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_0

    .line 180
    :cond_1
    :goto_0
    return v10

    .line 95
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v9

    .line 96
    .local v9, "hadTapMessage":Z
    if-eqz v9, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeMessages(I)V

    .line 97
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    if-eqz v9, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 99
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mIsDoubleTapping:Z

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v18

    or-int v10, v10, v18

    .line 106
    :goto_1
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mLastMotionX:F

    .line 107
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mLastMotionY:F

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->recycle()V

    .line 110
    :cond_3
    invoke-static/range {p2 .. p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    .line 111
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInMoveRegion:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5

    .line 112
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInTapRegion:Z

    .line 113
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 119
    :goto_2
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mStillDown:Z

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onTouchDown(Landroid/view/MotionEvent;)Z

    move-result v18

    or-int v10, v10, v18

    .line 122
    goto/16 :goto_0

    .line 103
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    sget v20, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->DOUBLE_TAP_TIMEOUT:I

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 116
    :cond_5
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInTapRegion:Z

    .line 117
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInBiggerTapRegion:Z

    goto :goto_2

    .line 124
    .end local v9    # "hadTapMessage":Z
    :pswitch_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mLastMotionX:F

    move/from16 v18, v0

    sub-float v11, v18, v16

    .line 125
    .local v11, "scrollX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mLastMotionY:F

    move/from16 v18, v0

    sub-float v12, v18, v17

    .line 126
    .local v12, "scrollY":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mIsDoubleTapping:Z

    move/from16 v18, v0

    if-eqz v18, :cond_6

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v11, v12}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onTouchMove(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v18

    or-int v10, v10, v18

    goto/16 :goto_0

    .line 129
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v18, v0

    if-eqz v18, :cond_8

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    sub-float v18, v16, v18

    move/from16 v0, v18

    float-to-int v6, v0

    .line 131
    .local v6, "deltaX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    sub-float v18, v17, v18

    move/from16 v0, v18

    float-to-int v7, v0

    .line 132
    .local v7, "deltaY":I
    mul-int v18, v6, v6

    mul-int v19, v7, v7

    add-int v8, v18, v19

    .line 133
    .local v8, "distance":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mTouchSlopSquare:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v8, v0, :cond_7

    .line 134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v11, v12}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onTouchMove(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v10

    .line 135
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mLastMotionX:F

    .line 136
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mLastMotionY:F

    .line 137
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInTapRegion:Z

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeMessages(I)V

    .line 140
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mBiggerTouchSlopSquare:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v8, v0, :cond_1

    .line 141
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInBiggerTapRegion:Z

    goto/16 :goto_0

    .line 145
    .end local v6    # "deltaX":I
    .end local v7    # "deltaY":I
    .end local v8    # "distance":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v11, v12}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onTouchMove(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v10

    .line 146
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mLastMotionX:F

    .line 147
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mLastMotionY:F

    goto/16 :goto_0

    .line 151
    .end local v11    # "scrollX":F
    .end local v12    # "scrollY":F
    :pswitch_2
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mStillDown:Z

    .line 152
    invoke-static/range {p2 .. p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v5

    .line 153
    .local v5, "currentUpEvent":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mIsDoubleTapping:Z

    move/from16 v18, v0

    if-eqz v18, :cond_a

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onTouchUp(Landroid/view/MotionEvent;)Z

    move-result v10

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v18

    or-int v10, v10, v18

    .line 168
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    if-eqz v18, :cond_9

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->recycle()V

    .line 171
    :cond_9
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/VelocityTracker;->recycle()V

    .line 173
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 174
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mIsDoubleTapping:Z

    goto/16 :goto_0

    .line 157
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v18, v0

    if-eqz v18, :cond_b

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onTouchUp(Landroid/view/MotionEvent;)Z

    move-result v10

    goto :goto_3

    .line 160
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 161
    .local v13, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v18, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mMaximumFlingVelocity:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 162
    invoke-virtual {v13}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v15

    .line 163
    .local v15, "velocityY":F
    invoke-virtual {v13}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v14

    .line 164
    .local v14, "velocityX":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v14, v15}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v10

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-interface {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;->onTouchUp(Landroid/view/MotionEvent;)Z

    move-result v10

    goto/16 :goto_3

    .line 177
    .end local v5    # "currentUpEvent":Landroid/view/MotionEvent;
    .end local v13    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v14    # "velocityX":F
    .end local v15    # "velocityY":F
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->cancel()V

    goto/16 :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
