.class public Lcom/infraware/polarisoffice5/ppt/HorizontalListView;
.super Landroid/widget/AdapterView;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field protected mAdapter:Landroid/widget/ListAdapter;

.field private mAdapterViewWidth:I

.field public mAlwaysOverrideTouch:Z

.field protected mCurrentX:I

.field private mDataChanged:Z

.field private mDataObserver:Landroid/database/DataSetObserver;

.field private mDisplayOffset:I

.field private mGesture:Landroid/view/GestureDetector;

.field private mInitialX:F

.field private mInitialY:F

.field private mLastScrollState:I

.field private mLeftViewIndex:I

.field private mMaxX:I

.field protected mNextX:I

.field private mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

.field private mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private mRemovedViewQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mRightViewIndex:I

.field protected mScroller:Landroid/widget/Scroller;

.field mSetSelectionFromLeft:Z

.field private mTouchFrame:Landroid/graphics/Rect;

.field mbMouseActionUp:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAlwaysOverrideTouch:Z

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    .line 52
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    .line 55
    const v0, 0x7fffffff

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    .line 56
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    .line 59
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    .line 63
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDataChanged:Z

    .line 69
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mSetSelectionFromLeft:Z

    .line 70
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mbMouseActionUp:Z

    .line 85
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLastScrollState:I

    .line 128
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;-><init>(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    .line 513
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$4;-><init>(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    .line 89
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x42fa0000    # 125.0f

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapterViewWidth:I

    .line 90
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    .line 91
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mGesture:Landroid/view/GestureDetector;

    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->initView()V

    .line 93
    return-void
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/HorizontalListView;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->reset()V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method private addAndMeasureChild(Landroid/view/View;I)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "viewPos"    # I

    .prologue
    const/4 v1, -0x1

    const/high16 v3, -0x80000000

    .line 283
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 284
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_0

    .line 285
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 288
    .restart local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 289
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getWidth()I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getHeight()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->measure(II)V

    .line 291
    return-void
.end method

.method private fillList(I)V
    .locals 3
    .param p1, "dx"    # I

    .prologue
    .line 352
    const/4 v1, 0x0

    .line 353
    .local v1, "edge":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 354
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 355
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    .line 357
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->fillListRight(II)V

    .line 359
    const/4 v1, 0x0

    .line 360
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 361
    if-eqz v0, :cond_1

    .line 362
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 364
    :cond_1
    invoke-direct {p0, v1, p1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->fillListLeft(II)V

    .line 365
    return-void
.end method

.method private fillListLeft(II)V
    .locals 4
    .param p1, "leftEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 388
    :goto_0
    add-int v1, p1, p2

    if-lez v1, :cond_0

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    if-ltz v1, :cond_0

    .line 389
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 390
    .local v0, "child":Landroid/view/View;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 391
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    .line 392
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    .line 393
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    goto :goto_0

    .line 395
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private fillListRight(II)V
    .locals 4
    .param p1, "rightEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 369
    :goto_0
    add-int v1, p1, p2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 371
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 372
    .local v0, "child":Landroid/view/View;
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 373
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr p1, v1

    .line 375
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    .line 376
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mCurrentX:I

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    .line 379
    :cond_0
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    if-gez v1, :cond_1

    .line 380
    const/4 v1, 0x0

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    .line 382
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    goto :goto_0

    .line 385
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private declared-synchronized initView()V
    .locals 1

    .prologue
    .line 96
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mCurrentX:I

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    .line 101
    const v0, 0x7fffffff

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private positionItems(I)V
    .locals 7
    .param p1, "dx"    # I

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 418
    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    add-int/2addr v4, p1

    iput v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    .line 419
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    .line 421
    .local v3, "left":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 422
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 423
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 424
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 425
    .local v1, "childWidth":I
    const/4 v4, 0x0

    add-int v5, v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 426
    add-int/2addr v3, v1

    .line 421
    .end local v1    # "childWidth":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 430
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "i":I
    .end local v3    # "left":I
    :cond_1
    return-void
.end method

.method private removeNonVisibleItems(I)V
    .locals 4
    .param p1, "dx"    # I

    .prologue
    const/4 v3, 0x0

    .line 398
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 399
    .local v0, "child":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p1

    if-gtz v1, :cond_0

    .line 400
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDisplayOffset:I

    .line 401
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 402
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 403
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    .line 404
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 407
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 408
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getWidth()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 409
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 410
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 411
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    .line 412
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 414
    :cond_1
    return-void
.end method

.method private declared-synchronized reset()V
    .locals 1

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->initView()V

    .line 172
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->removeAllViewsInLayout()V

    .line 173
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->requestLayout()V

    .line 174
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    monitor-exit p0

    return-void

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v0, :cond_0

    .line 246
    const/4 v0, 0x0

    .line 248
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getFirstVisiblePosition()I
    .locals 3

    .prologue
    .line 271
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapterViewWidth:I

    .line 272
    .local v1, "width":I
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mCurrentX:I

    div-int v0, v2, v1

    .line 274
    .local v0, "leftViewIndex":I
    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 279
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 479
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 480
    return v1
.end method

.method protected onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 470
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->reportScrollStateChange(I)V

    .line 471
    monitor-enter p0

    .line 472
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    const/4 v2, 0x0

    neg-float v3, p3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 473
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->requestLayout()V

    .line 475
    const/4 v0, 0x1

    return v0

    .line 473
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/high16 v5, 0x40a00000    # 5.0f

    .line 454
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 464
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :cond_0
    :goto_0
    return v2

    .line 456
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mInitialX:F

    .line 457
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mInitialY:F

    goto :goto_0

    .line 460
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mInitialX:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 461
    .local v0, "deltaX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mInitialY:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 462
    .local v1, "deltaY":F
    cmpl-float v3, v0, v5

    if-gtz v3, :cond_1

    cmpl-float v3, v1, v5

    if-lez v3, :cond_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected declared-synchronized onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 297
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 349
    :goto_0
    monitor-exit p0

    return-void

    .line 301
    :cond_0
    :try_start_1
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDataChanged:Z

    if-eqz v3, :cond_1

    .line 302
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mCurrentX:I

    .line 303
    .local v1, "oldCurrentX":I
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->initView()V

    .line 304
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->removeAllViewsInLayout()V

    .line 305
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    .line 306
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDataChanged:Z

    .line 309
    .end local v1    # "oldCurrentX":I
    :cond_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 310
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 311
    .local v2, "scrollx":I
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    .line 314
    .end local v2    # "scrollx":I
    :cond_2
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    if-gtz v3, :cond_3

    .line 315
    const/4 v3, 0x0

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    .line 316
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 318
    :cond_3
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    if-lt v3, v4, :cond_4

    .line 319
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    .line 320
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 323
    :cond_4
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mCurrentX:I

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    sub-int v0, v3, v4

    .line 325
    .local v0, "dx":I
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->removeNonVisibleItems(I)V

    .line 326
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->fillList(I)V

    .line 327
    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->positionItems(I)V

    .line 329
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mCurrentX:I

    .line 331
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_5

    .line 332
    new-instance v3, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$2;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$2;-><init>(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)V

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 295
    .end local v0    # "dx":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 340
    .restart local v0    # "dx":I
    :cond_5
    :try_start_2
    new-instance v3, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$3;

    invoke-direct {v3, p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$3;-><init>(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)V

    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected onScroll(I)V
    .locals 1
    .param p1, "distanceX"    # I

    .prologue
    .line 484
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->reportScrollStateChange(I)V

    .line 486
    monitor-enter p0

    .line 487
    :try_start_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    .line 488
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 489
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->requestLayout()V

    .line 490
    return-void

    .line 488
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 441
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mbMouseActionUp:Z

    .line 442
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 443
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mbMouseActionUp:Z

    .line 448
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 449
    .local v0, "handled":Z
    return v0

    .line 446
    .end local v0    # "handled":Z
    :cond_0
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mbMouseActionUp:Z

    goto :goto_0
.end method

.method public pointToPosition(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 493
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 494
    .local v2, "frame":Landroid/graphics/Rect;
    if-nez v2, :cond_0

    .line 495
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 496
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 499
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildCount()I

    move-result v1

    .line 500
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 501
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 502
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 503
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 504
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 505
    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLeftViewIndex:I

    add-int/2addr v4, v3

    add-int/lit8 v4, v4, 0x1

    .line 510
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v4

    .line 500
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 510
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method

.method reportScrollStateChange(I)V
    .locals 2
    .param p1, "newState"    # I

    .prologue
    .line 261
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLastScrollState:I

    if-eq p1, v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 263
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mLastScrollState:I

    .line 264
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 267
    :cond_0
    return-void
.end method

.method public declared-synchronized scrollTo(I)V
    .locals 5
    .param p1, "x"    # I

    .prologue
    .line 433
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->reportScrollStateChange(I)V

    .line 435
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mNextX:I

    sub-int v3, p1, v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 436
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    monitor-exit p0

    return-void

    .line 433
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 47
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 163
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 165
    :cond_0
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 166
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 167
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->reset()V

    .line 168
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    .line 112
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemLongClickListener;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 117
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 107
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0
    .param p1, "l"    # Landroid/widget/AbsListView$OnScrollListener;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 126
    return-void
.end method

.method public setSelection(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 183
    return-void
.end method

.method public declared-synchronized setSelectionFromLeft(II)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "x"    # I

    .prologue
    .line 187
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-le p1, v4, :cond_1

    .line 240
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 192
    :cond_1
    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {p0, v4}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 193
    .local v0, "child":Landroid/view/View;
    const/4 v3, 0x0

    .line 195
    .local v3, "width":I
    if-nez v0, :cond_2

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v4, :cond_2

    .line 196
    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v6, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRightViewIndex:I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-interface {v5, v6, v4, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 198
    if-eqz v0, :cond_0

    .line 201
    const/4 v4, -0x1

    invoke-direct {p0, v0, v4}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 202
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 203
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 205
    if-nez v3, :cond_2

    .line 206
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mAdapterViewWidth:I

    .line 211
    :cond_2
    if-nez v3, :cond_3

    if-eqz v0, :cond_3

    .line 212
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 215
    :cond_3
    const/4 v1, 0x0

    .line 224
    .local v1, "leftEdge":I
    mul-int v1, v3, p1

    .line 226
    sub-int v2, v1, p2

    .line 228
    .local v2, "nextX":I
    if-gez v2, :cond_4

    .line 229
    const/4 v2, 0x0

    .line 232
    :cond_4
    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    if-le v2, v4, :cond_5

    .line 233
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mMaxX:I

    .line 236
    :cond_5
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->scrollTo(I)V

    .line 238
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->setSelection(I)V

    .line 239
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->requestLayout()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 187
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "leftEdge":I
    .end local v2    # "nextX":I
    .end local v3    # "width":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method
