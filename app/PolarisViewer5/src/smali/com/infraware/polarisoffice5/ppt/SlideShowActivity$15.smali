.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$15;
.super Landroid/content/BroadcastReceiver;
.source "SlideShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 1021
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$15;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 1025
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1027
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/net/Uri;

    .line 1028
    .local v0, "filePath":[Landroid/net/Uri;
    const/4 v1, 0x0

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$15;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mFilePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$3400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1029
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$15;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$3500(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/common/util/SbeamHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$15;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v1, v0, v2, p1}, Lcom/infraware/common/util/SbeamHelper;->setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V

    .line 1031
    .end local v0    # "filePath":[Landroid/net/Uri;
    :cond_0
    return-void
.end method
