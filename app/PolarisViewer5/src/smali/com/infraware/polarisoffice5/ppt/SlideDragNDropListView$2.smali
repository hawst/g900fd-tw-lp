.class Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;
.super Landroid/os/Handler;
.source "SlideDragNDropListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$300(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getIsEncryptDoc()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->m_oActivity:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$300(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->IsViewerMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    iget-boolean v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mbSlideDragNDropEnable:Z

    if-nez v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_LONG_PRESS:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$400()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 153
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onStartLongPress()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$500(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V

    goto :goto_0

    .line 155
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->END_LONG_PRESS:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$100()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 156
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onEndLongPress()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V

    goto :goto_0

    .line 158
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->START_DRAG:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$700()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onStartDrag()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$800(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)V

    goto :goto_0

    .line 161
    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->RUN_DRAG:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$900()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevX:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$1000(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mPrevY:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$1100(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)I

    move-result v2

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onRunDrag(II)V
    invoke-static {v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$1200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;II)V

    .line 163
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$200(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;)Landroid/os/Handler;

    move-result-object v0

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->RUN_DRAG:I
    invoke-static {}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->access$900()I

    move-result v1

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
