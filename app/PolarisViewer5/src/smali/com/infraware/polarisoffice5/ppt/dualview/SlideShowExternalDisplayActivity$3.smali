.class Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;
.super Ljava/lang/Object;
.source "SlideShowExternalDisplayActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/ppt/SlideShowSurfaceView$SlideShowSurfaceViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceChanged(II)V
    .locals 0
    .param p1, "oldHeight"    # I
    .param p2, "newHeight"    # I

    .prologue
    .line 166
    return-void
.end method

.method public onSurfaceCreated(II)V
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v1, 0x1

    .line 159
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbPPSFile:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbSlideShow:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$002(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;Z)Z

    .line 161
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$300(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0, v1, p1, p2}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 163
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$300(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowStartPage:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$100(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)I

    move-result v3

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$200(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getWidth()I

    move-result v4

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$3;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$200(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getHeight()I

    move-result v5

    const/4 v6, 0x0

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISlideShow(IIIIIZ)V

    .line 165
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged()V
    .locals 0

    .prologue
    .line 167
    return-void
.end method
