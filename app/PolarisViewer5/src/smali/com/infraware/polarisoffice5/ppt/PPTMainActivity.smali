.class public Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
.super Lcom/infraware/office/baseframe/EvBaseEditorActivity;
.source "PPTMainActivity.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/infraware/office/evengine/E$EV_MOVE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SCREENMODE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_SLIDE_TEMPLATE_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_VIDEO_STATUS;
.implements Lcom/infraware/office/evengine/EvListener$PptEditorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;,
        Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;,
        Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$UnRedoActionMode;
    }
.end annotation


# static fields
.field private static final ACTION_HDMI_PLUGGED:Ljava/lang/String; = "android.intent.action.HDMI_PLUGGED"

.field private static final ACTION_WIFI_DISPLAY:Ljava/lang/String; = "android.intent.action.WIFI_DISPLAY"

.field public static final ExternalDisplayResolution:[[I

.field public static final MSG_UPDATE_SLIDE_NOTE:I = 0x1

.field public static final POPUPMENU_DISMISS:I = 0x2

.field public static final POPUPMENU_SHOWING_TIME:I = 0x9c4

.field public static final THUMBNAIL_QUALITY:F = 0.4f


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mAddSlideBtn_landscape:Landroid/widget/ImageView;

.field private mAddSlideBtn_portrait:Landroid/widget/ImageView;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallPPTBackID:I

.field private mDlgHandler:Landroid/os/Handler;

.field private mHDMIResolution:I

.field private mHandler:Landroid/os/Handler;

.field private mHideAnimation:Landroid/view/animation/TranslateAnimation;

.field private mLSlideList:Landroid/widget/ListView;

.field private mLSlideManage:Landroid/view/View;

.field private mLatestModifyCheckPageIndex:I

.field private mListScrollState:I

.field private mMarkerColor:I

.field private mMarkerThickness:I

.field private mMarkerType:I

.field private mMasterSlideImage:Landroid/graphics/Bitmap;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mNfcCallback:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;

.field private mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

.field private mPSlideManage:Landroid/view/View;

.field private mPSlideManageLayout:Landroid/widget/LinearLayout;

.field private mPageNum:I

.field private mPointerColor:I

.field private mPopupLayout:Landroid/widget/PopupWindow;

.field private mPopupMenu:Landroid/widget/PopupWindow;

.field private mSBeamEnabled:Z

.field private mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

.field private mShowAnimation:Landroid/view/animation/TranslateAnimation;

.field private mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

.field private mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

.field private mSlideImageHeight:I

.field private mSlideImageWidth:I

.field private mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

.field private mSlideNote:Landroid/view/View;

.field private mSlideNoteEditText:Landroid/widget/EditText;

.field private mSlideNoteLayout:Landroid/widget/FrameLayout;

.field private mSlideNoteLayoutLandScapeHeight:I

.field private mSlideNoteLayoutPortraitHeight:I

.field private mSlideNoteString:Ljava/lang/String;

.field private mSlideNoteTextView:Landroid/widget/TextView;

.field private mSlideShowBtn_landscape:Landroid/widget/ImageView;

.field private mSlideShowBtn_portrait:Landroid/widget/ImageView;

.field private mSlideShowDialog:Landroid/app/AlertDialog;

.field public mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

.field private mWIFIDisplayResolution:I

.field private m_SlideShowSettingsDialog:Landroid/app/AlertDialog;

.field private mbContinueMode:Z

.field private mbDismissPopup:Z

.field private mbFlicking:Z

.field private mbMasterSlideImage:Z

.field private mbMultiWindow:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

.field private mbPSlideMangeOpen:Z

.field private mbScreenModeUpdate:Z

.field private mbScrolling:Z

.field private mbSmartGuides:Z

.field private mbStartAnimation:Z

.field private mbWaitingPSlideManageShow:Z

.field private mbeforeSlideShowForScreenMode:I

.field private mbeforeSlideShowForZoom:I

.field private surfaceBackUp:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 139
    const/16 v0, 0x19

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v3

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [I

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [I

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [I

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [I

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [I

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [I

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x280
        0x1e0
    .end array-data

    :array_2
    .array-data 4
        0x2
        0x2d0
        0x1e0
    .end array-data

    :array_3
    .array-data 4
        0x4
        0x2d0
        0x240
    .end array-data

    :array_4
    .array-data 4
        0x8
        0x500
        0x2d0
    .end array-data

    :array_5
    .array-data 4
        0x10
        0x780
        0x438
    .end array-data

    :array_6
    .array-data 4
        0x20
        0x320
        0x258
    .end array-data

    :array_7
    .array-data 4
        0x40
        0x400
        0x300
    .end array-data

    :array_8
    .array-data 4
        0x80
        0x480
        0x360
    .end array-data

    :array_9
    .array-data 4
        0x100
        0x500
        0x300
    .end array-data

    :array_a
    .array-data 4
        0x200
        0x500
        0x320
    .end array-data

    :array_b
    .array-data 4
        0x400
        0x550
        0x300
    .end array-data

    :array_c
    .array-data 4
        0x800
        0x556
        0x300
    .end array-data

    :array_d
    .array-data 4
        0x1000
        0x500
        0x400
    .end array-data

    :array_e
    .array-data 4
        0x2000
        0x578
        0x41a
    .end array-data

    :array_f
    .array-data 4
        0x4000
        0x578
        0x384
    .end array-data

    :array_10
    .array-data 4
        0x8000
        0x640
        0x384
    .end array-data

    :array_11
    .array-data 4
        0x10000
        0x640
        0x4b0
    .end array-data

    :array_12
    .array-data 4
        0x20000
        0x690
        0x400
    .end array-data

    :array_13
    .array-data 4
        0x40000
        0x690
        0x41a
    .end array-data

    :array_14
    .array-data 4
        0x80000
        0x780
        0x4b0
    .end array-data

    :array_15
    .array-data 4
        0x100000
        0x320
        0x1e0
    .end array-data

    :array_16
    .array-data 4
        0x200000
        0x356
        0x1e0
    .end array-data

    :array_17
    .array-data 4
        0x400000
        0x360
        0x1e0
    .end array-data

    :array_18
    .array-data 4
        0x800000
        0x280
        0x168
    .end array-data
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;-><init>()V

    .line 95
    const-string/jumbo v0, "PPTMainActivity"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->LOG_CAT:Ljava/lang/String;

    .line 97
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    .line 99
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    .line 100
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbWaitingPSlideManageShow:Z

    .line 102
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManage:Landroid/view/View;

    .line 103
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    .line 104
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    .line 105
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .line 106
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    .line 107
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    .line 108
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    .line 109
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupLayout:Landroid/widget/PopupWindow;

    .line 110
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    .line 111
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayout:Landroid/widget/FrameLayout;

    .line 112
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    .line 113
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowDialog:Landroid/app/AlertDialog;

    .line 114
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteTextView:Landroid/widget/TextView;

    .line 115
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 116
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    .line 118
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMasterSlideImage:Landroid/graphics/Bitmap;

    .line 119
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    .line 121
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbeforeSlideShowForScreenMode:I

    .line 122
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbeforeSlideShowForZoom:I

    .line 125
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageWidth:I

    .line 126
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageHeight:I

    .line 127
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mListScrollState:I

    .line 128
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScrolling:Z

    .line 129
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbFlicking:Z

    .line 130
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLatestModifyCheckPageIndex:I

    .line 132
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPageNum:I

    .line 133
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPointerColor:I

    .line 134
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerType:I

    .line 135
    const/16 v0, 0xa

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerThickness:I

    .line 136
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerColor:I

    .line 137
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I

    .line 138
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I

    .line 146
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMasterSlideImage:Z

    .line 147
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbStartAnimation:Z

    .line 148
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    .line 149
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z

    .line 151
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScreenModeUpdate:Z

    .line 153
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbSmartGuides:Z

    .line 155
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 156
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;

    .line 157
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSBeamEnabled:Z

    .line 159
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    .line 160
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    .line 161
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    .line 162
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    .line 164
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->m_SlideShowSettingsDialog:Landroid/app/AlertDialog;

    .line 166
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayoutLandScapeHeight:I

    .line 167
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayoutPortraitHeight:I

    .line 169
    const v0, 0xffff

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mCallPPTBackID:I

    .line 178
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 179
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 181
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMultiWindow:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    .line 438
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    .line 540
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$4;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHandler:Landroid/os/Handler;

    .line 563
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 662
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->surfaceBackUp:Z

    .line 1566
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$9;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$9;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mDlgHandler:Landroid/os/Handler;

    .line 1800
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$10;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    .line 2270
    return-void
.end method

.method static synthetic access$000(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I

    return v0
.end method

.method static synthetic access$1002(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I

    return p1
.end method

.method static synthetic access$1100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I

    return v0
.end method

.method static synthetic access$1102(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I

    return p1
.end method

.method static synthetic access$1200(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbWaitingPSlideManageShow:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->surfaceBackUp:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->surfaceBackUp:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onSlideShowActivity(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onChangeScreenMode()V

    return-void
.end method

.method static synthetic access$1800(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbSmartGuides:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onSmartGuides()V

    return-void
.end method

.method static synthetic access$200(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbStartAnimation:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onSlideShowExternalDisplayActivity(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->m_SlideShowSettingsDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->showSlideShowSettingsDialog(I)V

    return-void
.end method

.method static synthetic access$2700(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSBeamEnabled:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSBeamEnabled:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mstrOpenFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Lcom/infraware/common/util/SbeamHelper;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageWidth:I

    return v0
.end method

.method static synthetic access$500(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setSlideNoteString()V

    return-void
.end method

.method static synthetic access$700(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getSlideNoteString()V

    return-void
.end method

.method static synthetic access$800(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->decideSlideNoteEditable()V

    return-void
.end method

.method static synthetic access$900(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$902(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
    .param p1, "x1"    # Landroid/widget/PopupWindow;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    return-object p1
.end method

.method private decideSlideNoteEditable()V
    .locals 5

    .prologue
    .line 1447
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->IsViewerMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1448
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    const v1, 0x20001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 1456
    :goto_0
    return-void

    .line 1452
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/16 v4, 0x403

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1454
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    goto :goto_0
.end method

.method private getHaveToDrawingPageCount()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1976
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVisibleSlideList()Landroid/widget/AdapterView;

    move-result-object v2

    .line 1977
    .local v2, "slideListView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    if-eqz v2, :cond_2

    .line 1978
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLatestModifyCheckPageIndex:I

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/widget/AdapterView;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1979
    invoke-virtual {v2, v0}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    .line 1980
    .local v1, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->getExistImage()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v4, v5}, Lcom/infraware/office/evengine/EvInterface;->IPageModified_Editor(I)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1981
    :cond_0
    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLatestModifyCheckPageIndex:I

    .line 1982
    add-int/lit8 v3, v0, 0x1

    .line 1988
    .end local v0    # "i":I
    .end local v1    # "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :goto_1
    return v3

    .line 1978
    .restart local v0    # "i":I
    .restart local v1    # "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1987
    .end local v0    # "i":I
    .end local v1    # "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :cond_2
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLatestModifyCheckPageIndex:I

    goto :goto_1
.end method

.method private getHaveToDrawingPageCountInViewRect()I
    .locals 5

    .prologue
    .line 1992
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVisibleSlideList()Landroid/widget/AdapterView;

    move-result-object v2

    .line 1993
    .local v2, "slideListView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    if-eqz v2, :cond_2

    .line 1994
    invoke-virtual {v2}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/widget/AdapterView;->getLastVisiblePosition()I

    move-result v3

    if-gt v0, v3, :cond_2

    .line 1995
    invoke-virtual {v2, v0}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    .line 1996
    .local v1, "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->getExistImage()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Lcom/infraware/office/evengine/EvInterface;->IPageModified_Editor(I)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 1997
    :cond_0
    add-int/lit8 v3, v0, 0x1

    .line 2000
    .end local v0    # "i":I
    .end local v1    # "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :goto_1
    return v3

    .line 1994
    .restart local v0    # "i":I
    .restart local v1    # "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2000
    .end local v0    # "i":I
    .end local v1    # "listItem":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private getSlideNoteString()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 1418
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    iget v0, v0, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPageNum:I

    .line 1419
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPageNum:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->IGetSlideNoteString_Editor(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    .line 1422
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    const v1, 0x20001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 1423
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1424
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 1426
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    .line 1427
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    .line 1429
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1430
    return-void
.end method

.method private getVisibleSlideList()Landroid/widget/AdapterView;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/AdapterView",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1967
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1968
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .line 1972
    :goto_0
    return-object v0

    .line 1969
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    if-eqz v0, :cond_1

    .line 1970
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    goto :goto_0

    .line 1972
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onChangeScreenMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1535
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1536
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z

    if-ne v0, v1, :cond_1

    .line 1537
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 1538
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z

    .line 1539
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isScroll(Z)V

    .line 1547
    :cond_0
    :goto_0
    return-void

    .line 1542
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 1543
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z

    .line 1544
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isScroll(Z)V

    goto :goto_0
.end method

.method private onCreateSlideList()V
    .locals 6

    .prologue
    .line 1340
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    instance-of v2, v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v2, :cond_2

    .line 1341
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->onListClear()V

    .line 1343
    const/4 v1, 0x0

    .line 1344
    .local v1, "interfaceHandleValue":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v2, :cond_0

    .line 1345
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->getJNIInterfaceHandleValue()I

    move-result v1

    .line 1349
    :cond_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    const v3, 0x7f030049

    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-virtual {v2, v1, v3, v4}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->initResources(II[I)V

    .line 1354
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v2, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nTotalPages:I

    if-ge v0, v2, :cond_1

    .line 1355
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    new-instance v3, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;-><init>(ZLandroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->onAdd(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;)V

    .line 1354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1358
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 1359
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onUpdateSlideListForShow(Landroid/widget/AdapterView;)V

    .line 1364
    .end local v0    # "i":I
    .end local v1    # "interfaceHandleValue":I
    :cond_2
    :goto_1
    return-void

    .line 1360
    .restart local v0    # "i":I
    .restart local v1    # "interfaceHandleValue":I
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 1361
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onUpdateSlideListForShow(Landroid/widget/AdapterView;)V

    goto :goto_1

    .line 1349
    :array_0
    .array-data 4
        0x7f0b01e6
        0x7f0b01e7
        0x7f0b01e8
        0x7f0b01e9
    .end array-data
.end method

.method private onHideLSlideManage()V
    .locals 2

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeAllMessage()V

    .line 1333
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1334
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1335
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    .line 1337
    :cond_0
    return-void
.end method

.method private onHideSlideNote()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1490
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1491
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScreenModeUpdate:Z

    .line 1493
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1495
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setSlideNoteString()V

    .line 1496
    new-instance v1, Lcom/infraware/polarisoffice5/common/ImmManager;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/common/ImmManager;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/common/ImmManager;->hideForcedIme()V

    .line 1500
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onOpenViewDlg()V
    .locals 3

    .prologue
    .line 1556
    invoke-static {}, Lcom/infraware/polarisoffice5/common/DlgFactory;->getInstance()Lcom/infraware/polarisoffice5/common/DlgFactory;

    move-result-object v1

    const/16 v2, 0x2f

    invoke-virtual {v1, p0, v2}, Lcom/infraware/polarisoffice5/common/DlgFactory;->createViewSettDlg(Lcom/infraware/office/baseframe/EvBaseViewerActivity;I)Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;

    move-result-object v0

    .line 1558
    .local v0, "dialog":Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;
    const-string/jumbo v2, "SINGLE_SLIDE_VIEW"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getContinuMode()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setDefaultChecked(Ljava/lang/String;Z)V

    .line 1559
    const-string/jumbo v1, "SMART_GUIDE"

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbSmartGuides:Z

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setDefaultChecked(Ljava/lang/String;Z)V

    .line 1560
    const-string/jumbo v1, "SPELL_CHECK"

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbSpellCheck:Z

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setDefaultChecked(Ljava/lang/String;Z)V

    .line 1561
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mDlgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->setEventHandler(Landroid/os/Handler;)Landroid/app/AlertDialog;

    .line 1563
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/ViewSettEventDlg;->show()V

    .line 1564
    return-void

    .line 1558
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onShowLSlideManage()V
    .locals 2

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onUpdateSlideListForShow(Landroid/widget/AdapterView;)V

    .line 1268
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1269
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->resetSlideManageButtonEnable()V

    .line 1270
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    .line 1272
    :cond_0
    return-void
.end method

.method private onShowSlideNote()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1460
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 1462
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 1466
    :cond_0
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHidePSlideManage(Z)Z

    .line 1467
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHideSlideManageOpenButton()Z

    .line 1469
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isScroll(Z)V

    .line 1470
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_2

    .line 1472
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_1

    .line 1473
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 1479
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setSlideNoteBodyHeight(I)V

    .line 1480
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1482
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getSlideNoteString()V

    .line 1483
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->decideSlideNoteEditable()V

    .line 1485
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1486
    return-void

    .line 1476
    :cond_2
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScreenModeUpdate:Z

    goto :goto_0
.end method

.method private onSlideShowActivity(I)V
    .locals 3
    .param p1, "startPageNum"    # I

    .prologue
    const/4 v2, 0x1

    .line 1217
    invoke-direct {p0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setDisableInputMode(Z)V

    .line 1220
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IStopFlicking()V

    .line 1222
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->hideDictionaryPanel()V

    .line 1224
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1225
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeAllMessage()V

    .line 1227
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1228
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "POINTER_COLOR"

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPointerColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1229
    const-string/jumbo v1, "MARKER_TYPE"

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1230
    const-string/jumbo v1, "MARKER_THICKNESS"

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerThickness:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1231
    const-string/jumbo v1, "MARKER_COLOR"

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1232
    const-string/jumbo v1, "PPT_FILEPATH"

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1233
    const-string/jumbo v1, "InterfaceHandleAddress"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->GetInterfaceHandle()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1234
    const-string/jumbo v1, "START_PAGE_NUM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1235
    const-string/jumbo v1, "SLIDE_THUMBNAIL_IMAGE_WIDTH"

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1236
    const-string/jumbo v1, "SLIDE_THUMBNAIL_IMAGE_HEIGHT"

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1237
    const-string/jumbo v1, "ORIENTATION"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1238
    const-string/jumbo v1, "INTERFACE_HANDLE"

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->GetInterfaceHandle()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1240
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IIsContinuePageView_Editor()I

    move-result v1

    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbeforeSlideShowForScreenMode:I

    .line 1242
    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1243
    return-void
.end method

.method private onSlideShowExternalDisplayActivity(I)V
    .locals 8
    .param p1, "startPageNum"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1190
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1191
    .local v0, "intent":Landroid/content/Intent;
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I

    if-eqz v2, :cond_1

    .line 1192
    const-string/jumbo v2, "HDMI_RESOLUTION"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1193
    const-string/jumbo v2, "WIFIDISPLAY_RESOLUTION"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1194
    const-string/jumbo v2, "WIDTH"

    sget-object v3, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I

    aget-object v3, v3, v4

    aget v3, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1195
    const-string/jumbo v2, "HEIGHT"

    sget-object v3, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I

    aget-object v3, v3, v4

    aget v3, v3, v7

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1205
    :cond_0
    :goto_0
    const-string/jumbo v2, "START_PAGE_NUM"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1206
    const-string/jumbo v2, "POINTER_COLOR"

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPointerColor:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1207
    const-string/jumbo v2, "MARKER_TYPE"

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerType:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1208
    const-string/jumbo v2, "MARKER_THICKNESS"

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerThickness:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1209
    const-string/jumbo v2, "MARKER_COLOR"

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerColor:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1211
    const/16 v2, 0xb

    invoke-virtual {p0, v0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1212
    return-void

    .line 1197
    :cond_1
    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I

    if-eqz v2, :cond_0

    .line 1198
    const-string/jumbo v2, "HDMI_RESOLUTION"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1199
    const-string/jumbo v2, "WIFIDISPLAY_RESOLUTION"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1200
    const-string/jumbo v2, "WIDTH"

    sget-object v3, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I

    aget-object v3, v3, v4

    aget v3, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1201
    const-string/jumbo v2, "HEIGHT"

    sget-object v3, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I

    aget-object v3, v3, v4

    aget v3, v3, v7

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1202
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.WIFI_DISPLAY_REQ"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1203
    .local v1, "resSetActionIntent":Landroid/content/Intent;
    const-string/jumbo v2, "res"

    sget-object v3, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I

    aget-object v3, v3, v4

    aget v3, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private onSmartGuides()V
    .locals 3

    .prologue
    .line 1551
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbSmartGuides:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbSmartGuides:Z

    .line 1552
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbSmartGuides:Z

    invoke-virtual {v0, v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetGuides(IZ)V

    .line 1553
    return-void

    .line 1551
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onUpdateSlideListForMovePage()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1391
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    instance-of v2, v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v2, :cond_0

    .line 1392
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v2

    add-int/lit8 v1, v2, 0x1

    .line 1393
    .local v1, "nPrevPage":I
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v2

    iget v0, v2, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    .line 1394
    .local v0, "nCurPage":I
    if-ne v1, v0, :cond_1

    .line 1395
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v0}, Lcom/infraware/office/evengine/EvInterface;->IPageModified_Editor(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1414
    .end local v0    # "nCurPage":I
    .end local v1    # "nPrevPage":I
    :cond_0
    :goto_0
    return-void

    .line 1399
    .restart local v0    # "nCurPage":I
    .restart local v1    # "nPrevPage":I
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->setPageInfoText()V

    .line 1400
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3, v5}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->setSelectedPosition(IZ)V

    .line 1402
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 1403
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSelectedItem(I)V

    .line 1405
    :cond_2
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 1406
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSelectedItem(I)V

    .line 1409
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v2, v0}, Lcom/infraware/office/evengine/EvInterface;->IPageModified_Editor(I)Z

    move-result v2

    if-ne v2, v5, :cond_0

    .line 1410
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v2, v4}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->request(I)V

    .line 1411
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v2, v4}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->setDrawAllThumbnail(Z)V

    goto :goto_0
.end method

.method private onUpdateSlideListForShow(Landroid/widget/AdapterView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "slideList":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    const/4 v2, 0x0

    .line 1367
    if-nez p1, :cond_1

    .line 1388
    .end local p1    # "slideList":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    :cond_0
    :goto_0
    return-void

    .line 1368
    .restart local p1    # "slideList":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v0, :cond_0

    .line 1369
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 1370
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1372
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v0, :cond_0

    .line 1373
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->setSelectedPosition(IZ)V

    .line 1375
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    const/16 v1, 0xfa

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->request(I)V

    .line 1378
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->setDrawAllThumbnail(Z)V

    .line 1380
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 1381
    instance-of v0, p1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    if-eqz v0, :cond_2

    .line 1382
    check-cast p1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .end local p1    # "slideList":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSelectedItem(I)V

    goto :goto_0

    .line 1384
    .restart local p1    # "slideList":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    :cond_2
    instance-of v0, p1, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    if-eqz v0, :cond_0

    .line 1385
    check-cast p1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .end local p1    # "slideList":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSelectedItem(I)V

    goto :goto_0
.end method

.method private setDisableInputMode(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 2379
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, p1}, Lcom/infraware/office/baseframe/EvBaseView;->setDisableTextInput(Z)V

    .line 2380
    return-void
.end method

.method private setNfcCallback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2260
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2268
    :cond_0
    :goto_0
    return-void

    .line 2262
    :cond_1
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 2263
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 2264
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$1;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;

    .line 2265
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 2266
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mNfcCallback:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$NfcCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private setSlideNoteBodyHeight(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 2353
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 2355
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2356
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayoutPortraitHeight:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2365
    :goto_0
    return-void

    .line 2361
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2362
    .restart local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayoutLandScapeHeight:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method private setSlideNoteString()V
    .locals 5

    .prologue
    .line 1434
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1435
    .local v0, "slideNoteString":Ljava/lang/String;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1436
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    .line 1437
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteString:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 1438
    if-nez v0, :cond_2

    .line 1439
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPageNum:I

    const-string/jumbo v3, ""

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/infraware/office/evengine/EvInterface;->ISlideNoteInput(ILjava/lang/String;I)V

    .line 1443
    :cond_1
    :goto_0
    return-void

    .line 1441
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPageNum:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/infraware/office/evengine/EvInterface;->ISlideNoteInput(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private showSlideShowSettingsDialog(I)V
    .locals 5
    .param p1, "nPage"    # I

    .prologue
    .line 2128
    new-instance v2, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;

    invoke-direct {v2, p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;-><init>(Landroid/content/Context;)V

    .line 2130
    .local v2, "slideShowSettingsAdapter":Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;
    const/4 v1, -0x1

    .line 2132
    .local v1, "index":I
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v0, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 2134
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f070218

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2136
    new-instance v3, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$13;

    invoke-direct {v3, p0, p1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$13;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2147
    const v3, 0x7f07005f

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2148
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->m_SlideShowSettingsDialog:Landroid/app/AlertDialog;

    .line 2149
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->m_SlideShowSettingsDialog:Landroid/app/AlertDialog;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2150
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->m_SlideShowSettingsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 2152
    return-void
.end method


# virtual methods
.method public ActivityMsgProc(IIIIILjava/lang/Object;)I
    .locals 4
    .param p1, "msg"    # I
    .param p2, "p1"    # I
    .param p3, "p2"    # I
    .param p4, "p3"    # I
    .param p5, "p4"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1609
    sparse-switch p1, :sswitch_data_0

    .line 1735
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    move-result v0

    .line 1737
    :goto_0
    return v0

    .line 1611
    :sswitch_0
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHideSlideNote()Z

    .line 1613
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHideSlideManageOpenButton()Z

    .line 1614
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHidePSlideManage(Z)Z

    .line 1616
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 1619
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1620
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getScreenView()Lcom/infraware/office/baseframe/EvBaseView;

    move-result-object v0

    iget-object v0, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    iget-object v0, v0, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->mPopupMenu:Lcom/infraware/polarisoffice5/common/PopupMenuWindow;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PopupMenuWindow;->dismissAll()V

    .line 1622
    :cond_0
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    :cond_1
    :goto_1
    :sswitch_1
    move v0, v2

    .line 1737
    goto :goto_0

    .line 1625
    :sswitch_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->isChangedDataSet()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1626
    goto :goto_0

    .line 1629
    :sswitch_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 1631
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isFreeDrawMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1633
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->setFocus()V

    .line 1634
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->requestLayout()V

    .line 1635
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->clearFocus()V

    goto :goto_1

    .line 1638
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isFindReplaceMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1640
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_1

    .line 1642
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->requestLayout()V

    .line 1643
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->clearFocus()V

    goto :goto_1

    .line 1648
    :sswitch_4
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v3, "PPT MainActivity eOnInfraPenDrawing"

    invoke-static {v0, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1649
    if-ne p2, v2, :cond_3

    .line 1652
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1653
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1654
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1655
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_1

    .line 1660
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1661
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1662
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1663
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_1

    .line 1671
    :sswitch_5
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onBeforeSurfaceChanged(IIII)V

    .line 1672
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto :goto_1

    .line 1675
    :sswitch_6
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onSurfaceChanged(IIII)V

    .line 1678
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_4

    .line 1680
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScreenModeUpdate:Z

    if-ne v0, v2, :cond_4

    .line 1681
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScreenModeUpdate:Z

    .line 1683
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    .line 1684
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 1699
    :cond_4
    :goto_2
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 1687
    :cond_5
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z

    if-ne v0, v2, :cond_6

    .line 1688
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 1689
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isScroll(Z)V

    goto :goto_2

    .line 1692
    :cond_6
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 1693
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isScroll(Z)V

    goto :goto_2

    .line 1702
    :sswitch_7
    if-nez p2, :cond_7

    .line 1703
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setDragNDropEnable(Z)V

    .line 1704
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSlideSelectEnable(Z)V

    .line 1705
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSlideMoveEnable(Z)V

    .line 1707
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setDragNDropEnable(Z)V

    .line 1708
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSlideSelectEnable(Z)V

    .line 1709
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSlideMoveEnable(Z)V

    .line 1711
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1712
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1713
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1714
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1716
    invoke-super/range {p0 .. p6}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->ActivityMsgProc(IIIIILjava/lang/Object;)I

    goto/16 :goto_1

    .line 1719
    :cond_7
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setDragNDropEnable(Z)V

    .line 1720
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSlideSelectEnable(Z)V

    .line 1721
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSlideMoveEnable(Z)V

    .line 1723
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setDragNDropEnable(Z)V

    .line 1724
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSlideSelectEnable(Z)V

    .line 1725
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSlideMoveEnable(Z)V

    .line 1727
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1728
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1729
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1730
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_1

    .line 1609
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x5 -> :sswitch_6
        0x9 -> :sswitch_0
        0xc -> :sswitch_5
        0x29 -> :sswitch_7
        0x2d -> :sswitch_2
        0x31 -> :sswitch_3
        0x34 -> :sswitch_4
    .end sparse-switch
.end method

.method public OnActionBarEvent(I)V
    .locals 2
    .param p1, "eEventType"    # I

    .prologue
    const/4 v1, 0x1

    .line 1774
    sparse-switch p1, :sswitch_data_0

    .line 1795
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnActionBarEvent(I)V

    .line 1798
    :goto_0
    :sswitch_0
    return-void

    .line 1776
    :sswitch_1
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    if-ne v0, v1, :cond_0

    .line 1777
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHidePSlideManage(Z)Z

    goto :goto_0

    .line 1780
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowPSlideManage()V

    goto :goto_0

    .line 1784
    :sswitch_2
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowSlideNote()V

    goto :goto_0

    .line 1787
    :sswitch_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onChangeScreenMode()V

    goto :goto_0

    .line 1792
    :sswitch_4
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onOpenViewDlg()V

    goto :goto_0

    .line 1774
    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_1
        0x29 -> :sswitch_3
        0x30 -> :sswitch_2
        0x65 -> :sswitch_0
        0x77 -> :sswitch_4
    .end sparse-switch
.end method

.method public OnDrawBitmap(II)V
    .locals 4
    .param p1, "nCallId"    # I
    .param p2, "bShowAutomap"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1057
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1058
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPageNum:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v1, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    if-eq v0, v1, :cond_0

    .line 1059
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1062
    :cond_0
    const/16 v0, 0x1a

    if-ne p1, v0, :cond_3

    .line 1063
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScrolling:Z

    .line 1069
    :goto_0
    const/16 v0, 0x3d

    if-ne p1, v0, :cond_4

    .line 1070
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbFlicking:Z

    .line 1078
    :goto_1
    const-string/jumbo v0, "HYOHYUN"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "PPT Main Activity nCallId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " HexString = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    const/16 v0, 0x2e

    if-eq v0, p1, :cond_1

    .line 1081
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mCallPPTBackID:I

    .line 1083
    :cond_1
    const/16 v0, 0x106

    if-eq p1, v0, :cond_2

    const/16 v0, 0x124

    if-eq p1, v0, :cond_2

    const/16 v0, 0x125

    if-ne p1, v0, :cond_5

    .line 1086
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1087
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeAllMessage()V

    .line 1093
    :goto_2
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnDrawBitmap(II)V

    .line 1094
    return-void

    .line 1066
    :cond_3
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScrolling:Z

    goto :goto_0

    .line 1073
    :cond_4
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbFlicking:Z

    goto :goto_1

    .line 1091
    :cond_5
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onUpdateSlideListForMovePage()V

    goto :goto_2
.end method

.method public OnLoadComplete(I)V
    .locals 4
    .param p1, "bBookmarkExsit"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 1098
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onCreateSlideList()V

    .line 1100
    invoke-static {}, Lcom/infraware/common/config/RuntimeConfig;->getInstance()Lcom/infraware/common/config/RuntimeConfig;

    move-result-object v0

    .line 1101
    .local v0, "oConfig":Lcom/infraware/common/config/RuntimeConfig;
    const/16 v1, 0xe

    invoke-virtual {v0, p0, v1, v2}, Lcom/infraware/common/config/RuntimeConfig;->getBooleanPreference(Landroid/content/Context;IZ)Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 1103
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    .line 1112
    :cond_0
    :goto_0
    return-void

    .line 1107
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1108
    if-nez p1, :cond_0

    .line 1109
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v3}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    goto :goto_0
.end method

.method public OnOLEFormatInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "nWideString"    # Ljava/lang/String;

    .prologue
    .line 2386
    return-void
.end method

.method public OnPptDrawSlidesBitmap(I)V
    .locals 3
    .param p1, "pageNum"    # I

    .prologue
    const/4 v1, 0x1

    .line 1026
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbStartAnimation:Z

    if-ne v0, v1, :cond_1

    .line 1027
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->setUpdateThumbnail(Z)V

    .line 1033
    :goto_0
    if-nez p1, :cond_0

    .line 1034
    const-string/jumbo v0, "ThumbnailRequester"

    const-string/jumbo v1, "OnPptDrawSlidesBitmap: ISlideObjStartEx() Event canceled (make thumbnail fail)"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->setMakingThumbnail(Z)V

    .line 1038
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    const/16 v1, 0x96

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->request(I)V

    .line 1040
    const-string/jumbo v0, "ThumbnailRequester"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ISlideObjStartEx() Callback : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    return-void

    .line 1029
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->notifyDataSetChanged()V

    .line 1030
    const-string/jumbo v0, "PPTMainActivity"

    const-string/jumbo v1, "notifyDataSetChanged"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public OnPptGetSlidenoteBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1044
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnPptGetSlidesBitmap(IIIIZLjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "bBitmapImage"    # I
    .param p2, "nPageNum"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "isHideSlide"    # Z
    .param p6, "strSlideTitle"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 983
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 1021
    :cond_0
    :goto_0
    return-object v1

    .line 986
    :cond_1
    iget-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMasterSlideImage:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 987
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMasterSlideImage:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 988
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMasterSlideImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 989
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMasterSlideImage:Landroid/graphics/Bitmap;

    .line 991
    :cond_2
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, p4, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMasterSlideImage:Landroid/graphics/Bitmap;

    .line 993
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMasterSlideImage:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 995
    :cond_3
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    instance-of v2, v2, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v2, :cond_0

    .line 996
    const-string/jumbo v2, "PPTMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "OnPptGetSlidesBitmap pageNum:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    add-int/lit8 v3, p2, -0x1

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItemImage(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1001
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_5

    .line 1002
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, p3, :cond_4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, p4, :cond_5

    .line 1003
    :cond_4
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1004
    const/4 v1, 0x0

    .line 1008
    :cond_5
    if-nez v1, :cond_6

    .line 1009
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, p4, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1012
    :cond_6
    if-nez p1, :cond_7

    .line 1013
    const/4 v0, 0x0

    .line 1017
    .local v0, "bHaveBitmap":Z
    :goto_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    add-int/lit8 v3, p2, -0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1, p5}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->changeListItems(ILjava/lang/Boolean;Landroid/graphics/Bitmap;Z)V

    goto :goto_0

    .line 1015
    .end local v0    # "bHaveBitmap":Z
    :cond_7
    const/4 v0, 0x1

    .restart local v0    # "bHaveBitmap":Z
    goto :goto_1
.end method

.method public OnPptOnDrawSlidenote(I)V
    .locals 0
    .param p1, "pageNum"    # I

    .prologue
    .line 1047
    return-void
.end method

.method public OnPptSlideDelete()V
    .locals 0

    .prologue
    .line 976
    return-void
.end method

.method public OnPptSlideMoveNext()V
    .locals 0

    .prologue
    .line 973
    return-void
.end method

.method public OnPptSlideMovePrev()V
    .locals 0

    .prologue
    .line 970
    return-void
.end method

.method public OnPptSlideShowDrawBitmap()V
    .locals 0

    .prologue
    .line 1053
    return-void
.end method

.method public OnPptSlideShowEffectEnd(I)V
    .locals 0
    .param p1, "nSubType"    # I

    .prologue
    .line 979
    return-void
.end method

.method public OnPptSlideShowGetBitmap(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1050
    const/4 v0, 0x0

    return-object v0
.end method

.method public OnPptSlideexInsert()V
    .locals 0

    .prologue
    .line 967
    return-void
.end method

.method public OnPptSlidenoteStart()V
    .locals 0

    .prologue
    .line 964
    return-void
.end method

.method public OnSpellCheck(Ljava/lang/String;IIIIIII)V
    .locals 0
    .param p1, "pWordStr"    # Ljava/lang/String;
    .param p2, "nLen"    # I
    .param p3, "bClass"    # I
    .param p4, "nPageNum"    # I
    .param p5, "nObjectID"    # I
    .param p6, "nNoteNum"    # I
    .param p7, "nParaIndex"    # I
    .param p8, "nColIndex"    # I

    .prologue
    .line 2315
    return-void
.end method

.method public OnUndoOrRedo(ZIII)V
    .locals 4
    .param p1, "bUndo"    # Z
    .param p2, "nAction"    # I
    .param p3, "nPage1"    # I
    .param p4, "nPage2"    # I

    .prologue
    const/4 v3, 0x0

    .line 936
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    instance-of v1, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v1, :cond_0

    .line 937
    packed-switch p2, :pswitch_data_0

    .line 960
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->OnUndoOrRedo(ZIII)V

    .line 961
    return-void

    .line 939
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    add-int/lit8 v2, p3, -0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->onInsertAtLocation(I)V

    .line 940
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVisibleSlideList()Landroid/widget/AdapterView;

    move-result-object v0

    .line 941
    .local v0, "slideListView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    if-eqz v0, :cond_0

    .line 942
    instance-of v1, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 943
    check-cast v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSelectedItem(I)V

    .line 944
    :cond_1
    instance-of v1, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    if-eqz v1, :cond_2

    .line 945
    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .end local v0    # "slideListView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSelectedItem(I)V

    .line 946
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v1, v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->request(I)V

    .line 947
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v1, v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->setDrawAllThumbnail(Z)V

    goto :goto_0

    .line 951
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    add-int/lit8 v2, p3, -0x1

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->onDeleteAtLocation(I)I

    goto :goto_0

    .line 954
    :pswitch_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    add-int/lit8 v2, p3, -0x1

    add-int/lit8 v3, p4, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->onDrop(II)V

    goto :goto_0

    .line 937
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public canRequestThumbnail()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2005
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-nez v0, :cond_1

    move v5, v7

    .line 2044
    :cond_0
    :goto_0
    return v5

    .line 2008
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v6, v0, Lcom/infraware/office/baseframe/EvBaseView;->mGestureProc:Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;

    .line 2009
    .local v6, "gesture":Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;
    invoke-virtual {v6}, Lcom/infraware/office/baseframe/gestureproc/EvBaseGestureProc;->getTouchStatus()I

    move-result v0

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-eqz v0, :cond_2

    move v5, v7

    .line 2010
    goto :goto_0

    .line 2013
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScrolling:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbFlicking:Z

    if-eqz v0, :cond_4

    :cond_3
    move v5, v7

    .line 2014
    goto :goto_0

    .line 2017
    :cond_4
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mListScrollState:I

    if-eqz v0, :cond_5

    move v5, v7

    .line 2018
    goto :goto_0

    .line 2021
    :cond_5
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetIsValidateScreenSlides()Z

    move-result v0

    if-ne v0, v5, :cond_7

    :cond_6
    move v5, v7

    .line 2022
    goto :goto_0

    .line 2025
    :cond_7
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mCallPPTBackID:I

    const/16 v1, 0x21a

    if-eq v0, v1, :cond_8

    const/16 v0, 0x21e

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mCallPPTBackID:I

    if-ne v0, v1, :cond_9

    .line 2028
    :cond_8
    const-string/jumbo v0, "HYOHYUN"

    const-string/jumbo v1, "mView.mCallBackID == eEV_GUI_BWP_INSERT_STRING_EVENT"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2029
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageWidth:I

    iget v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageHeight:I

    const v3, 0x3ecccccd    # 0.4f

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v4}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v4

    iget v4, v4, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->ISlideObjStartEx(IIFIZ)V

    move v5, v7

    .line 2030
    goto :goto_0

    .line 2032
    :cond_9
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mCallPPTBackID:I

    const/16 v1, 0x114

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mCallPPTBackID:I

    const/16 v1, 0x115

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mCallPPTBackID:I

    const/16 v1, 0x116

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mCallPPTBackID:I

    const/16 v1, 0x117

    if-ne v0, v1, :cond_b

    :cond_a
    move v5, v7

    .line 2037
    goto :goto_0

    .line 2041
    :cond_b
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mIsContentSearch:Z

    if-eqz v0, :cond_0

    move v5, v7

    .line 2042
    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 1944
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1945
    iget-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1947
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    .line 1952
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected finalizePPTMainActivity()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 389
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_CHECK_MEMORYLEAK()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 390
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    .line 392
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 393
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 394
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    .line 397
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 398
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 399
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    .line 402
    :cond_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 403
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 404
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    .line 407
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    if-eqz v1, :cond_3

    .line 408
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 409
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    .line 412
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    if-eqz v1, :cond_4

    .line 414
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->finalizeSlideDragNDropHorizontalListView()V

    .line 415
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 416
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v1, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setParent(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 417
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .line 420
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    if-eqz v1, :cond_5

    .line 421
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    .line 424
    :cond_5
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    if-eqz v1, :cond_6

    .line 425
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    .line 427
    :cond_6
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    if-eqz v1, :cond_7

    .line 428
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 429
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 430
    .local v0, "view":Landroid/view/View;
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    .line 431
    invoke-static {v0}, Lcom/infraware/common/util/Utils;->unbindDrawables(Landroid/view/View;)V

    .line 435
    .end local v0    # "view":Landroid/view/View;
    :cond_7
    return-void
.end method

.method public getContinuMode()Z
    .locals 2

    .prologue
    .line 1525
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1526
    const/4 v0, 0x0

    .line 1529
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z

    goto :goto_0
.end method

.method public getFreeDrawOptionMargin()I
    .locals 2

    .prologue
    .line 1601
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1602
    const/high16 v0, 0x41400000    # 12.0f

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->dipToPx(F)F

    move-result v0

    float-to-int v0, v0

    .line 1604
    :goto_0
    return v0

    :cond_0
    const v0, 0x4313547b    # 147.33f

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->dipToPx(F)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getIntentVideo()V
    .locals 4

    .prologue
    .line 1905
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1907
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->IGetVideoPath()Ljava/lang/String;

    move-result-object v1

    .line 1909
    .local v1, "strVideoPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1911
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1912
    .local v2, "uri":Landroid/net/Uri;
    const-string/jumbo v3, "video/*"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1913
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1915
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public getInvisibleSlideImage()I
    .locals 3

    .prologue
    .line 2058
    const/4 v0, 0x0

    .line 2060
    .local v0, "pageNum":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->isChangedDataSet()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2061
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getHaveToDrawingPageCount()I

    move-result v0

    .line 2064
    :cond_0
    return v0
.end method

.method public getPopupmenu()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method public getVisibleSlideImage()I
    .locals 3

    .prologue
    .line 2048
    const/4 v0, 0x0

    .line 2050
    .local v0, "pageNum":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->isChangedDataSet()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2051
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getHaveToDrawingPageCountInViewRect()I

    move-result v0

    .line 2054
    :cond_0
    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const/4 v11, 0x1

    const/4 v4, -0x1

    const/4 v10, 0x0

    .line 1115
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1117
    const/16 v0, 0xb

    if-ne p1, v0, :cond_4

    .line 1118
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v2, p0

    move-object v4, p0

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 1119
    if-eqz p3, :cond_0

    .line 1120
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 1121
    .local v8, "extras":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 1122
    .local v7, "color":I
    const-string/jumbo v0, "POINTER_COLOR"

    invoke-virtual {v8, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPointerColor:I

    .line 1123
    const-string/jumbo v0, "MARKER_TYPE"

    invoke-virtual {v8, v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerType:I

    .line 1124
    const-string/jumbo v0, "MARKER_THICKNESS"

    const/16 v1, 0xa

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerThickness:I

    .line 1125
    const-string/jumbo v0, "MARKER_COLOR"

    invoke-virtual {v8, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerColor:I

    .line 1128
    .end local v7    # "color":I
    .end local v8    # "extras":Landroid/os/Bundle;
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v2

    move v3, v10

    move v4, v10

    move v5, v10

    move v6, v10

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISlideShow(IIIIIZ)V

    .line 1133
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbeforeSlideShowForScreenMode:I

    if-eqz v0, :cond_3

    .line 1135
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsContinuePageView_Editor()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v0

    if-ne v0, v11, :cond_1

    .line 1136
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v10}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 1143
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetViewMode(I)V

    .line 1144
    iput v10, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbeforeSlideShowForScreenMode:I

    .line 1145
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onUpdateSlideListForMovePage()V

    .line 1147
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/MainActionBar;->hideAllToolbarDelay()Z

    .line 1150
    invoke-direct {p0, v10}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setDisableInputMode(Z)V

    .line 1187
    :cond_2
    :goto_1
    return-void

    .line 1139
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsContinuePageView_Editor()I

    move-result v0

    if-ne v0, v11, :cond_1

    .line 1140
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v11}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    goto :goto_0

    .line 1152
    :cond_4
    const/16 v0, 0x28

    if-ne p1, v0, :cond_8

    .line 1153
    const-string/jumbo v0, "PPTMAinActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "FullScreen resultCode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1154
    if-ne p2, v4, :cond_6

    .line 1155
    const-string/jumbo v0, "PPTMainActivity"

    const-string/jumbo v1, "MdeiaViedoManager Succcess"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v0, v10}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->setVideoStatus(I)V

    goto :goto_1

    .line 1156
    :cond_6
    if-nez p2, :cond_7

    .line 1157
    const-string/jumbo v0, "PPTMainActivity"

    const-string/jumbo v1, "MdeiaViedoManager Canceled & BackPressed"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1158
    :cond_7
    if-ne p2, v11, :cond_5

    .line 1159
    const-string/jumbo v0, "PPTMainActivity"

    const-string/jumbo v1, "MdeiaViedoManager RESULT_FIRST_USER"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1162
    :cond_8
    const/16 v0, 0x29

    if-ne p1, v0, :cond_9

    .line 1163
    if-ne p2, v4, :cond_2

    .line 1164
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$8;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$8;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v10, v1, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$8;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 1174
    :cond_9
    const/16 v0, 0x15

    if-ne p1, v0, :cond_2

    .line 1175
    const/4 v7, -0x1

    .line 1176
    .restart local v7    # "color":I
    const/4 v9, -0x1

    .line 1178
    .local v9, "type":I
    if-eqz p3, :cond_a

    .line 1179
    const-string/jumbo v0, "color_value"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 1180
    const-string/jumbo v0, "color_type"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 1183
    :cond_a
    const/4 v0, 0x2

    if-ne v9, v0, :cond_2

    .line 1184
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getSelectedPosition()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1, v7}, Lcom/infraware/office/evengine/EvInterface;->ISetSlideBackgroundColor(II)V

    goto/16 :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1742
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onVideoBackPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1760
    :cond_0
    :goto_0
    return-void

    .line 1745
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    .line 1746
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1747
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    goto :goto_0

    .line 1750
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupLayout:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_3

    .line 1751
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupLayout:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1752
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    goto :goto_0

    .line 1756
    :cond_3
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHideSlideNote()Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1759
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onBeforeSurfaceChanged(IIII)V
    .locals 10
    .param p1, "oldWidth"    # I
    .param p2, "oldHeight"    # I
    .param p3, "newWidth"    # I
    .param p4, "newHeight"    # I

    .prologue
    const/high16 v9, 0x42c80000    # 100.0f

    const/4 v8, 0x1

    .line 666
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->surfaceBackUp:Z

    .line 676
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v6

    if-ne v6, v8, :cond_1

    .line 677
    if-ne p3, p1, :cond_0

    if-ge p4, p2, :cond_0

    .line 683
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMultiWindow:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    invoke-virtual {v6, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 684
    sub-int v0, p2, p4

    .line 685
    .local v0, "coverHeight":I
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getHeight()I

    move-result v6

    invoke-static {p0, v9}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    add-int v5, v6, v7

    .line 690
    .local v5, "slideManageHeight":I
    if-le v0, v5, :cond_0

    .line 691
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHideSlideManageOpenButton()Z

    .line 692
    const-string/jumbo v6, "PPTMainActivity"

    const-string/jumbo v7, "***onBeforeSurfaceChanged calling onShowPSlideManage(false)"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    if-eqz v6, :cond_0

    .line 695
    iput-boolean v8, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->surfaceBackUp:Z

    .line 696
    invoke-virtual {p0, v8}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHidePSlideManage(Z)Z

    .line 702
    .end local v0    # "coverHeight":I
    .end local v5    # "slideManageHeight":I
    :cond_0
    if-eq p3, p1, :cond_1

    .line 704
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 705
    .local v1, "display":Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 706
    .local v3, "outSize":Landroid/graphics/Point;
    invoke-virtual {v1, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 709
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getHeight()I

    move-result v6

    invoke-static {p0, v9}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    add-int v5, v6, v7

    .line 713
    .restart local v5    # "slideManageHeight":I
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMultiWindow:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    invoke-virtual {v6, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 714
    iget v4, v3, Landroid/graphics/Point;->y:I

    .line 715
    .local v4, "screenheight":I
    sub-int v0, v4, p4

    .line 717
    .restart local v0    # "coverHeight":I
    if-ge v0, v5, :cond_1

    .line 718
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowSlideManageOpenButton()Z

    .line 720
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    if-eqz v6, :cond_1

    .line 721
    const-string/jumbo v6, "PPTMainActivity"

    const-string/jumbo v7, "***onBeforeSurfaceChanged calling onShowPSlideManage()"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowPSlideManage()V

    .line 746
    .end local v0    # "coverHeight":I
    .end local v1    # "display":Landroid/view/Display;
    .end local v3    # "outSize":Landroid/graphics/Point;
    .end local v4    # "screenheight":I
    .end local v5    # "slideManageHeight":I
    :cond_1
    :goto_0
    return-void

    .line 730
    .restart local v1    # "display":Landroid/view/Display;
    .restart local v3    # "outSize":Landroid/graphics/Point;
    .restart local v5    # "slideManageHeight":I
    :cond_2
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMultiWindow:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    invoke-virtual {v6, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getAppRect(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 731
    .local v2, "multiwindowheight":I
    sub-int v0, v2, p4

    .line 733
    .restart local v0    # "coverHeight":I
    if-ge v0, v5, :cond_1

    .line 734
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowSlideManageOpenButton()Z

    .line 736
    iget-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    if-eqz v6, :cond_1

    .line 737
    const-string/jumbo v6, "PPTMainActivity"

    const-string/jumbo v7, "***onBeforeSurfaceChanged calling onShowPSlideManage()"

    invoke-static {v6, v7}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowPSlideManage()V

    goto :goto_0
.end method

.method public onChangeScreen(I)V
    .locals 4
    .param p1, "nType"    # I

    .prologue
    const/4 v3, 0x0

    .line 610
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 611
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getVisibleSlideList()Landroid/widget/AdapterView;

    move-result-object v1

    .line 612
    .local v1, "slideListView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<Landroid/widget/ListAdapter;>;"
    instance-of v2, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 613
    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    .line 615
    .local v0, "listView":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;
    if-eqz v0, :cond_0

    .line 616
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->onStopDrag()V

    .line 625
    .end local v0    # "listView":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_1

    .line 626
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    .line 627
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    .line 629
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupLayout:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_2

    .line 630
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupLayout:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    .line 631
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    .line 638
    :cond_2
    const/4 v2, 0x1

    if-ne p1, v2, :cond_4

    .line 639
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHideLSlideManage()V

    .line 647
    :goto_1
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onChangeScreen(I)V

    .line 648
    return-void

    .line 618
    :cond_3
    instance-of v2, v1, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 619
    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    .line 621
    .local v0, "listView":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;
    if-eqz v0, :cond_0

    .line 622
    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->onStopDrag()V

    goto :goto_0

    .line 642
    .end local v0    # "listView":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHideSlideManageOpenButton()Z

    .line 643
    invoke-virtual {p0, v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHidePSlideManage(Z)Z

    .line 645
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowLSlideManage()V

    goto :goto_1
.end method

.method protected onChangedFromMultiwindowToNormalWindow()V
    .locals 0

    .prologue
    .line 856
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 857
    return-void
.end method

.method protected onChangedFromNormalWindowToMultiWindow()V
    .locals 0

    .prologue
    .line 852
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 853
    return-void
.end method

.method public onClickActionBar(I)V
    .locals 1
    .param p1, "nViewId"    # I

    .prologue
    .line 1764
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onClickActionBar(I)V

    .line 1765
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeAllMessage()V

    .line 1766
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHideSlideNote()Z

    .line 1767
    return-void
.end method

.method public onClickSlideManageOpenCloseBtn(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2068
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 2069
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v7

    if-nez v7, :cond_2

    .line 2070
    const/4 v0, 0x0

    .line 2071
    .local v0, "bLandScape":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    .line 2072
    const/4 v0, 0x1

    .line 2073
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getmView()Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v7}, Lcom/infraware/office/baseframe/EvBaseView;->getBitmapWidth()I

    move-result v3

    .line 2074
    .local v3, "nWidth":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getmView()Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v7}, Lcom/infraware/office/baseframe/EvBaseView;->getBitmapHeight()I

    move-result v2

    .line 2075
    .local v2, "nHeight":I
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v7

    add-int/2addr v2, v7

    .line 2078
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v7, :cond_1

    .line 2079
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v7, v0, v3, v2}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 2081
    :cond_1
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    new-instance v8, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$11;

    invoke-direct {v8, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$11;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    const-wide/16 v9, 0xc8

    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2125
    .end local v0    # "bLandScape":I
    .end local v2    # "nHeight":I
    .end local v3    # "nWidth":I
    :goto_0
    return-void

    .line 2093
    :cond_2
    const v7, 0x7f0b011a

    invoke-virtual {p0, v7}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2094
    .local v1, "dummyView":Landroid/widget/ImageView;
    const/4 v4, 0x0

    .line 2095
    .local v4, "oBitmap":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    iget-object v8, v7, Lcom/infraware/office/baseframe/EvBaseView;->mSyncObject:Ljava/lang/Object;

    monitor-enter v8

    .line 2096
    :try_start_0
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v7}, Lcom/infraware/office/baseframe/EvBaseView;->getPrivateBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 2097
    .local v6, "tmp":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v7

    if-nez v7, :cond_3

    .line 2098
    invoke-static {v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2100
    :cond_3
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2101
    move-object v5, v4

    .line 2102
    .local v5, "postBitmap":Landroid/graphics/Bitmap;
    if-eqz v5, :cond_4

    .line 2103
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2104
    sget-object v7, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2105
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2108
    :cond_4
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHandler:Landroid/os/Handler;

    new-instance v8, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$12;

    invoke-direct {v8, p0, v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$12;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Landroid/graphics/Bitmap;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 2100
    .end local v5    # "postBitmap":Landroid/graphics/Bitmap;
    .end local v6    # "tmp":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7
.end method

.method public onClickSlideShow(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2155
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->OnEndFreeDraw()V

    .line 2156
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 2158
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->FindBarClose()Z

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_0

    .line 2160
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    invoke-virtual {v9}, Lcom/infraware/office/actionbar/MainActionBar;->show()V

    .line 2258
    :goto_0
    return-void

    .line 2164
    :cond_0
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {v9}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2165
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {v9}, Landroid/widget/PopupWindow;->dismiss()V

    .line 2166
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    .line 2167
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    goto :goto_0

    .line 2171
    :cond_1
    const/high16 v9, 0x43200000    # 160.0f

    invoke-static {p0, v9}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    .line 2172
    .local v6, "popupWidth":I
    const/high16 v9, 0x42ab0000    # 85.5f

    invoke-static {p0, v9}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v4

    .line 2174
    .local v4, "popupHeight":I
    const-string/jumbo v9, "layout_inflater"

    invoke-virtual {p0, v9}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2175
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f03004f

    const/4 v10, 0x0

    invoke-virtual {v1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 2176
    .local v5, "popupView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 2179
    .local v0, "config":Landroid/content/res/Configuration;
    new-instance v9, Landroid/widget/PopupWindow;

    invoke-direct {v9, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    .line 2181
    iget v9, v0, Landroid/content/res/Configuration;->orientation:I

    packed-switch v9, :pswitch_data_0

    .line 2193
    :goto_1
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 2194
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 2195
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    new-instance v10, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$14;

    invoke-direct {v10, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$14;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 2204
    const v9, 0x7f0b0220

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 2205
    .local v8, "startFirstPage":Landroid/widget/Button;
    const v9, 0x7f0b0221

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 2206
    .local v7, "startCurrentPage":Landroid/widget/Button;
    invoke-virtual {v8}, Landroid/widget/Button;->setSingleLine()V

    .line 2207
    invoke-virtual {v7}, Landroid/widget/Button;->setSingleLine()V

    .line 2209
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/view/View;->measure(II)V

    .line 2211
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 2212
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 2214
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 2215
    .local v3, "padding":Landroid/graphics/Rect;
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {v9}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 2216
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    iget v10, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v6

    iget v11, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 2217
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    iget v10, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v10, v4

    iget v11, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 2218
    new-instance v9, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$15;

    invoke-direct {v9, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$15;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2228
    new-instance v9, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;

    invoke-direct {v9, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$16;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    invoke-virtual {v7, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2239
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2240
    invoke-static {v8}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setButtonTextHoveringListener(Landroid/widget/Button;)Z

    .line 2241
    invoke-static {v7}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setButtonTextHoveringListener(Landroid/widget/Button;)Z

    .line 2244
    :cond_2
    const/4 v9, 0x2

    new-array v2, v9, [I

    .line 2246
    .local v2, "location":[I
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 2248
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 2249
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    const/16 v10, 0x33

    const/4 v11, 0x0

    aget v11, v2, v11

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v12

    add-int/2addr v11, v12

    sub-int/2addr v11, v6

    const/4 v12, 0x1

    aget v12, v2, v12

    sub-int/2addr v12, v4

    invoke-virtual {v9, v5, v10, v11, v12}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 2250
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_0

    .line 2183
    .end local v2    # "location":[I
    .end local v3    # "padding":Landroid/graphics/Rect;
    .end local v7    # "startCurrentPage":Landroid/widget/Button;
    .end local v8    # "startFirstPage":Landroid/widget/Button;
    :pswitch_0
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0201a7

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 2187
    :pswitch_1
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0201a8

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 2253
    .restart local v2    # "location":[I
    .restart local v3    # "padding":Landroid/graphics/Rect;
    .restart local v7    # "startCurrentPage":Landroid/widget/Button;
    .restart local v8    # "startFirstPage":Landroid/widget/Button;
    :cond_3
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    const/16 v10, 0x33

    const/4 v11, 0x0

    aget v11, v2, v11

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v12

    add-int/2addr v11, v12

    const/4 v12, 0x1

    aget v12, v2, v12

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v13

    add-int/2addr v12, v13

    sub-int/2addr v12, v4

    invoke-virtual {v9, v5, v10, v11, v12}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 2254
    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_0

    .line 2181
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2345
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2347
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2348
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setSlideNoteBodyHeight(I)V

    .line 2350
    :cond_0
    return-void
.end method

.method protected onContentSearchStart()V
    .locals 2

    .prologue
    .line 2370
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2371
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onHidePSlideManage(Z)Z

    .line 2373
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 861
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 869
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 871
    :goto_0
    return v0

    .line 863
    :pswitch_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v1, v0}, Lcom/infraware/office/evengine/EvInterface;->ISetObjPos(I)V

    goto :goto_0

    .line 866
    :pswitch_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetObjPos(I)V

    goto :goto_0

    .line 861
    :pswitch_data_0
    .packed-switch 0x7f0b0282
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 186
    const v0, 0x7f03002b

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setContentView(I)V

    .line 188
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    .line 189
    .local v12, "layoutInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f03004b

    const/4 v1, 0x0

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManage:Landroid/view/View;

    .line 190
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManage:Landroid/view/View;

    const v1, 0x7f0b01ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    .line 192
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 194
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setParent(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 195
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSlideDragNDropListener(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;)V

    .line 196
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManage:Landroid/view/View;

    const v2, 0x7f0b01f0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropHorizontalListView;->setSlideIndicatorView(Landroid/view/View;)V

    .line 198
    :cond_0
    const v0, 0x7f0b012d

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    .line 199
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 201
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout$LayoutParams;

    .line 202
    .local v13, "pLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v0, -0x1

    iput v0, v13, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 203
    const/4 v0, -0x2

    iput v0, v13, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 204
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManage:Landroid/view/View;

    invoke-virtual {v0, v13}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 206
    const v0, 0x7f03004a

    const/4 v1, 0x0

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    .line 207
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    const v1, 0x7f0b01ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    .line 208
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 210
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    invoke-virtual {v0, p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setParent(Lcom/infraware/office/baseframe/EvBaseViewerActivity;)V

    .line 211
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropListener:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSlideDragNDropListener(Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView$SlideDragNDropListener;)V

    .line 212
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideList:Landroid/widget/ListView;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    const v2, 0x7f0b01eb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListView;->setSlideIndicatorView(Landroid/view/View;)V

    .line 214
    :cond_1
    const v0, 0x7f0b0116

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 215
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 218
    .local v11, "lLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v0, 0x43020000    # 130.0f

    invoke-static {p0, v0}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 219
    const/4 v0, -0x1

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 220
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mLSlideManage:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 222
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    .line 223
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mShowAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 224
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    .line 225
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHideAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 227
    const v0, 0x7f0b0125

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    .line 228
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 230
    const v0, 0x7f0b0130

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayout:Landroid/widget/FrameLayout;

    .line 231
    const v0, 0x7f0b012e

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    .line 232
    const v0, 0x7f0b0131

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    .line 233
    const v0, 0x7f0b012f

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteTextView:Landroid/widget/TextView;

    .line 234
    invoke-static {}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->getInstance()Lcom/infraware/common/multiwindow/SFeatureWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Lcom/infraware/common/multiwindow/SFeatureWrapper;->turnOffWritingBuddy(Landroid/widget/EditText;)V

    .line 235
    const/high16 v0, 0x42f00000    # 120.0f

    invoke-static {p0, v0}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageWidth:I

    .line 236
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-static {p0, v0}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideImageHeight:I

    .line 238
    const/high16 v0, 0x42980000    # 76.0f

    invoke-static {p0, v0}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayoutLandScapeHeight:I

    .line 239
    const v0, 0x432f8000    # 175.5f

    invoke-static {p0, v0}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteLayoutPortraitHeight:I

    .line 240
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPointerColor:I

    .line 241
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMarkerColor:I

    .line 243
    const v0, 0x7f0b01f2

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    .line 244
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$1;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$1;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 252
    const v0, 0x7f0b01f1

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    .line 253
    const v0, 0x7f0b01ed

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    .line 256
    const v0, 0x7f0b01ec

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    .line 257
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    new-instance v1, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$2;

    invoke-direct {v1, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$2;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 265
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->IsViewerMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 270
    :cond_2
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 271
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0, p0}, Lcom/infraware/office/baseframe/EvBaseView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 273
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 274
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 275
    .local v9, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v0, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 276
    const-string/jumbo v0, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, v9}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 280
    .end local v9    # "filter":Landroid/content/IntentFilter;
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v0}, Lcom/infraware/office/baseframe/EvBaseView;->requestFocus()Z

    .line 284
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 285
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 286
    new-instance v0, Lcom/infraware/common/util/SbeamHelper;

    const-string/jumbo v1, "text/DirectSharePolarisEditor"

    invoke-direct {v0, p0, v1}, Lcom/infraware/common/util/SbeamHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    .line 287
    new-instance v10, Landroid/content/IntentFilter;

    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    .line 288
    .local v10, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v0, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 289
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$3;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$3;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 301
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, v10}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 307
    .end local v10    # "intentFilter":Landroid/content/IntentFilter;
    :cond_4
    :goto_0
    invoke-static {p0}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getInstance(Landroid/content/Context;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    .line 308
    return-void

    .line 304
    :cond_5
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setNfcCallback()V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 879
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 880
    .local v0, "inflater":Landroid/view/MenuInflater;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->GetObjCtrlType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 894
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 897
    :goto_0
    return-void

    .line 883
    :pswitch_1
    const v1, 0x7f0a0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 888
    :pswitch_2
    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 891
    :pswitch_3
    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 880
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 359
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "FT03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 364
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeAllMessage()V

    .line 365
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHandler:Landroid/os/Handler;

    .line 367
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    if-eqz v0, :cond_1

    .line 368
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    .line 369
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onDestroy()V

    .line 370
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .line 373
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_CHECK_MEMORYLEAK()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 375
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->finalizePPTMainActivity()V

    .line 378
    :cond_2
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onDestroy()V

    .line 381
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    .line 382
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 383
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 385
    :cond_3
    return-void
.end method

.method public onHidePSlideManage(Z)Z
    .locals 4
    .param p1, "changeOpenFlag"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1295
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbWaitingPSlideManageShow:Z

    .line 1297
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->removeAllMessage()V

    .line 1299
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 1300
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1302
    if-ne p1, v0, :cond_0

    .line 1303
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    .line 1305
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020253

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1306
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    const v2, 0x7f07021e

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1310
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onHideSlideManageOpenButton()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1314
    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbWaitingPSlideManageShow:Z

    .line 1316
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1317
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1318
    const/4 v0, 0x1

    .line 1320
    :cond_0
    return v0
.end method

.method public onLocaleChange(I)V
    .locals 4
    .param p1, "nLocale"    # I

    .prologue
    const v2, 0x7f07024f

    .line 652
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 653
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNoteTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 655
    const/4 v0, 0x0

    .line 656
    .local v0, "bLandScape":I
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v1}, Lcom/infraware/office/baseframe/EvBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 657
    const/4 v0, 0x1

    .line 658
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v2}, Lcom/infraware/office/baseframe/EvBaseView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 659
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onLocaleChange(I)V

    .line 660
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPopupMenu:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 354
    :cond_0
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onPause()V

    .line 355
    return-void
.end method

.method public onPlayVideo(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1884
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    if-nez v2, :cond_0

    .line 1885
    const v2, 0x7f0b011c

    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    .line 1887
    :cond_0
    const/4 v0, 0x0

    .line 1888
    .local v0, "bRet":Z
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getVideoStatus()I

    move-result v2

    if-nez v2, :cond_2

    .line 1889
    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1891
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1893
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getIntentVideo()V

    .line 1894
    const/4 v0, 0x1

    move v1, v0

    .line 1900
    .end local v0    # "bRet":Z
    .local v1, "bRet":I
    :goto_0
    return v1

    .line 1898
    .end local v1    # "bRet":I
    .restart local v0    # "bRet":Z
    :cond_1
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v2, p1, p2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onPlayVideo(II)Z

    move-result v0

    :cond_2
    move v1, v0

    .line 1900
    .restart local v1    # "bRet":I
    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 901
    invoke-super {p0, p1, p2}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 902
    const/16 v0, 0x26

    if-ne p1, v0, :cond_0

    .line 903
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->removeDialog(I)V

    .line 904
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 312
    const/4 v0, 0x0

    .line 313
    .local v0, "adapterInit":Z
    const/4 v2, 0x0

    .line 315
    .local v2, "interfaceHandleAddress":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getInterfaceHandleAddress()I

    move-result v3

    if-eqz v3, :cond_5

    .line 316
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v3}, Lcom/infraware/office/baseframe/EvBaseView;->getInterfaceHandleAddress()I

    move-result v2

    .line 322
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->getJNIInterfaceHandleValue()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 324
    const/4 v0, 0x1

    .line 327
    :cond_1
    invoke-super {p0}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->onResume()V

    .line 329
    if-ne v0, v4, :cond_2

    .line 330
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onCreateSlideList()V

    .line 334
    :cond_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$SAMSUNG;->USE_SBEAM()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 335
    invoke-static {}, Lcom/infraware/common/util/Utils;->isJB()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 336
    new-array v1, v4, [Landroid/net/Uri;

    .line 337
    .local v1, "filePath":[Landroid/net/Uri;
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mstrOpenFilePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v5

    .line 338
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSbeamHelper:Lcom/infraware/common/util/SbeamHelper;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v1, p0, v4}, Lcom/infraware/common/util/SbeamHelper;->setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V

    .line 342
    .end local v1    # "filePath":[Landroid/net/Uri;
    :cond_3
    iput-boolean v5, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbDismissPopup:Z

    .line 343
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    if-eqz v3, :cond_4

    .line 345
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v3, v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->setMakingThumbnail(Z)V

    .line 346
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->request()V

    .line 348
    :cond_4
    return-void

    .line 318
    :cond_5
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->getNativeInterfaceHandle()I

    move-result v3

    if-eqz v3, :cond_0

    .line 319
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v3}, Lcom/infraware/office/evengine/EvInterface;->getNativeInterfaceHandle()I

    move-result v2

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 1964
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 1956
    const-string/jumbo v0, "PPTMainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onScrollStateChanged scrollState:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957
    iput p2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mListScrollState:I

    .line 1958
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mListScrollState:I

    if-nez v0, :cond_0

    .line 1959
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mThumbnailRequester:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$ThumbnailHandler;->request()V

    .line 1961
    :cond_0
    return-void
.end method

.method public onShowPSlideManage()V
    .locals 3

    .prologue
    .line 1246
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mMainActionBar:Lcom/infraware/office/actionbar/MainActionBar;

    if-eqz v0, :cond_0

    .line 1251
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1252
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideList:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-direct {p0, v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onUpdateSlideListForShow(Landroid/widget/AdapterView;)V

    .line 1253
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManageLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1254
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->resetSlideManageButtonEnable()V

    .line 1255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z

    .line 1257
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020252

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1258
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    const v1, 0x7f07021d

    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1261
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mPSlideManage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    .line 1263
    :cond_1
    return-void
.end method

.method public onShowSlideManageOpenButton()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1324
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1325
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1326
    const/4 v0, 0x1

    .line 1328
    :cond_0
    return v0
.end method

.method public onStopVideo()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1918
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    if-nez v2, :cond_1

    .line 1928
    :cond_0
    :goto_0
    return v0

    .line 1921
    :cond_1
    const-string/jumbo v2, "PPTMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mVideoFrameLayout.getVideoStatus() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getVideoStatus()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1923
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getVideoStatus()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->getVideoStatus()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 1927
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mVideoFrameLayout:Lcom/infraware/polarisoffice5/common/VideoFrameLayout;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/VideoFrameLayout;->onStopVideo()V

    move v0, v1

    .line 1928
    goto :goto_0
.end method

.method protected onSurfaceChanged(IIII)V
    .locals 10
    .param p1, "oldWidth"    # I
    .param p2, "oldHeight"    # I
    .param p3, "newWidth"    # I
    .param p4, "newHeight"    # I

    .prologue
    .line 750
    sub-int v6, p4, p2

    const/high16 v7, 0x42c80000    # 100.0f

    invoke-static {p0, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    if-ne v6, v7, :cond_1

    .line 848
    :cond_0
    :goto_0
    return-void

    .line 761
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 762
    .local v1, "display":Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 763
    .local v3, "outSize":Landroid/graphics/Point;
    invoke-virtual {v1, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 765
    iget v4, v3, Landroid/graphics/Point;->y:I

    .line 766
    .local v4, "screenheight":I
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mView:Lcom/infraware/office/baseframe/EvBaseView;

    invoke-virtual {v6}, Lcom/infraware/office/baseframe/EvBaseView;->getBottom()I

    move-result v0

    .line 767
    .local v0, "contentViewHeight":I
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getHeight()I

    move-result v6

    const/high16 v7, 0x42c80000    # 100.0f

    invoke-static {p0, v7}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v7

    add-int v5, v6, v7

    .line 771
    .local v5, "slideManageHeight":I
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 774
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMultiWindow:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    invoke-virtual {v6, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 776
    if-ne p3, p1, :cond_0

    sub-int v6, v4, v0

    if-ge v6, v5, :cond_0

    .line 783
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbWaitingPSlideManageShow:Z

    .line 785
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    new-instance v7, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$6;

    invoke-direct {v7, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$6;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v7, v8, v9}, Landroid/widget/ImageButton;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 811
    :cond_2
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMultiWindow:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    invoke-virtual {v6, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getAppRect(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 813
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbMultiWindow:Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    invoke-virtual {v6, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getAppRect(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 815
    .local v2, "multiwindowheight":I
    if-ne p3, p1, :cond_0

    sub-int v6, v2, v0

    if-ge v6, v5, :cond_0

    .line 822
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbWaitingPSlideManageShow:Z

    .line 824
    iget-object v6, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideManageOpenCloseBtn:Landroid/widget/ImageButton;

    new-instance v7, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;

    invoke-direct {v7, p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;-><init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v7, v8, v9}, Landroid/widget/ImageButton;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onVideoBackPressed()Z
    .locals 1

    .prologue
    .line 1933
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onStopVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1937
    const/4 v0, 0x1

    .line 1939
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepareChangeScreen()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2318
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    .line 2320
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScreenModeUpdate:Z

    if-ne v0, v1, :cond_0

    .line 2321
    iput-boolean v2, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbScreenModeUpdate:Z

    .line 2323
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2324
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 2341
    :cond_0
    :goto_0
    return-void

    .line 2327
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbContinueMode:Z

    if-ne v0, v1, :cond_2

    .line 2328
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 2330
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isScroll(Z)V

    goto :goto_0

    .line 2332
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->ISetScreenMode(I)V

    .line 2334
    invoke-virtual {p0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isScroll(Z)V

    goto :goto_0
.end method

.method public resetSlideManageButtonEnable()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1277
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->isFindReplaceMode()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 1278
    .local v0, "buttonEnable":Z
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getOrientation()I

    move-result v2

    if-ne v2, v1, :cond_3

    .line 1280
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1281
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1282
    :cond_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 1283
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1292
    :cond_1
    :goto_1
    return-void

    .line 1277
    .end local v0    # "buttonEnable":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1287
    .restart local v0    # "buttonEnable":Z
    :cond_3
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    .line 1288
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideShowBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1289
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 1290
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_1
.end method

.method public setEvListener()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 604
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    move-object v1, p0

    move-object v2, p0

    move-object v4, p0

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 606
    :cond_0
    return-void
.end method

.method public setViewerMode(Z)V
    .locals 3
    .param p1, "isViewerMode"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1506
    invoke-super {p0, p1}, Lcom/infraware/office/baseframe/EvBaseEditorActivity;->setViewerMode(Z)V

    .line 1508
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mSlideNote:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1509
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->setSlideNoteString()V

    .line 1510
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->getSlideNoteString()V

    .line 1511
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->decideSlideNoteEditable()V

    .line 1514
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1515
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1516
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1522
    :goto_0
    return-void

    .line 1519
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_portrait:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1520
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mAddSlideBtn_landscape:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
