.class public Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;
.super Landroid/view/ExternalSurfaceView;
.source "SlideShowExternalSurfaceView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView$PointerStatus;,
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView$EventType;
    }
.end annotation


# static fields
.field private static final DELAY_TIME:I


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mConnected:Z

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mHandler:Landroid/os/Handler;

.field private mPaint:Landroid/graphics/Paint;

.field private mPointerDrag:[Landroid/graphics/Bitmap;

.field private mPointerDragHalfHeight:I

.field private mPointerDragHalfWidth:I

.field private mPointerIndex:I

.field private mPointerMode:Z

.field private mPointerPosition:Landroid/graphics/Point;

.field private mPointerStatus:I

.field private mPointerTap:[Landroid/graphics/Bitmap;

.field private mPointerTapHalfHeight:I

.field private mPointerTapHalfWidth:I

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setInit()V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ExternalSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 30
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    .line 31
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mConnected:Z

    .line 32
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerMode:Z

    .line 34
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPaint:Landroid/graphics/Paint;

    .line 36
    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    .line 37
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    .line 38
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    .line 39
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    .line 40
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTapHalfWidth:I

    .line 41
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTapHalfHeight:I

    .line 42
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDragHalfWidth:I

    .line 43
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDragHalfHeight:I

    .line 44
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerIndex:I

    .line 58
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView$1;

    invoke-direct {v0, p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView$1;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mHandler:Landroid/os/Handler;

    .line 90
    invoke-direct {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->setInit()V

    .line 91
    return-void
.end method

.method static synthetic access$002(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    return p1
.end method

.method private setInit()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 96
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 98
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPaint:Landroid/graphics/Paint;

    .line 99
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 100
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 102
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 104
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    .line 107
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a4

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v3

    .line 108
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a5

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v4

    .line 109
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a2

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v5

    .line 110
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a1

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v6

    .line 111
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a3

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v7

    .line 112
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTapHalfWidth:I

    .line 113
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTapHalfHeight:I

    .line 116
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a4

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v3

    .line 117
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a5

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v4

    .line 118
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a2

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v5

    .line 119
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a1

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v6

    .line 120
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a3

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v7

    .line 121
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDragHalfWidth:I

    .line 122
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDragHalfHeight:I

    .line 123
    return-void
.end method


# virtual methods
.method public destory()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    .line 223
    :cond_0
    return-void
.end method

.method public drawAllContents()V
    .locals 3

    .prologue
    .line 246
    const/4 v0, 0x0

    .line 248
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 249
    :try_start_1
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->onDraw(Landroid/graphics/Canvas;)V

    .line 252
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    if-eqz v0, :cond_1

    .line 258
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 260
    :cond_1
    :goto_0
    return-void

    .line 252
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 253
    :catch_0
    move-exception v1

    .line 257
    if-eqz v0, :cond_1

    .line 258
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 257
    :catchall_1
    move-exception v1

    if-eqz v0, :cond_2

    .line 258
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_2
    throw v1
.end method

.method public getBitmap(II)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "aScreenWidth"    # I
    .param p2, "aScreenHeight"    # I

    .prologue
    const v5, 0x7f0700a7

    const/4 v4, 0x0

    .line 226
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 228
    :try_start_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 242
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v1

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 235
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 236
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_1
    throw v1
.end method

.method public getConnected()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mConnected:Z

    return v0
.end method

.method public onClear()V
    .locals 2

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mConnected:Z

    if-nez v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 266
    :try_start_0
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mConnected:Z

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 268
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 270
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerMode:Z

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 274
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTap:[Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerIndex:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 286
    :catch_0
    move-exception v0

    goto :goto_0

    .line 276
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 278
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDrag:[Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerIndex:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 283
    :cond_3
    const/16 v0, 0xff

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawARGB(IIII)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 166
    iget-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mConnected:Z

    if-nez v3, :cond_0

    .line 203
    :goto_0
    return-void

    .line 169
    :cond_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/infraware/office/evengine/EvInterface;->IGetDualViewPosForSlideShow(II)Lcom/infraware/office/evengine/EV$DUALVIEW_POS;

    move-result-object v0

    .line 170
    .local v0, "position":Lcom/infraware/office/evengine/EV$DUALVIEW_POS;
    iget v1, v0, Lcom/infraware/office/evengine/EV$DUALVIEW_POS;->nPosX:I

    .line 171
    .local v1, "x":I
    iget v2, v0, Lcom/infraware/office/evengine/EV$DUALVIEW_POS;->nPosY:I

    .line 173
    .local v2, "y":I
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 174
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 176
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 202
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 178
    :pswitch_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    goto :goto_1

    .line 183
    :pswitch_1
    iput v6, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    .line 185
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDragHalfWidth:I

    sub-int v4, v1, v4

    iget v5, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDragHalfHeight:I

    sub-int v5, v2, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    goto :goto_1

    .line 188
    :pswitch_2
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    if-nez v3, :cond_2

    .line 189
    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    .line 190
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTapHalfWidth:I

    sub-int v4, v1, v4

    iget v5, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerTapHalfHeight:I

    sub-int v5, v2, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    .line 191
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 193
    :cond_2
    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerStatus:I

    if-ne v3, v6, :cond_1

    .line 195
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerPosition:Landroid/graphics/Point;

    iget v4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDragHalfWidth:I

    sub-int v4, v1, v4

    iget v5, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerDragHalfHeight:I

    sub-int v5, v2, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    .line 196
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 144
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerIndex:I

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 147
    const/4 v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerIndex:I

    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 149
    const/4 v0, 0x2

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerIndex:I

    goto :goto_0

    .line 150
    :cond_3
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_4

    .line 151
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerIndex:I

    goto :goto_0

    .line 152
    :cond_4
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 153
    const/4 v0, 0x4

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerIndex:I

    goto :goto_0
.end method

.method public setConnected(Z)V
    .locals 0
    .param p1, "connected"    # Z

    .prologue
    .line 130
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mConnected:Z

    .line 131
    return-void
.end method

.method public setEvInterface(Lcom/infraware/office/evengine/EvInterface;)V
    .locals 0
    .param p1, "evInterface"    # Lcom/infraware/office/evengine/EvInterface;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 127
    return-void
.end method

.method public setPointerMode(Z)V
    .locals 0
    .param p1, "pointerMode"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->mPointerMode:Z

    .line 139
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->drawAllContents()V

    .line 294
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 297
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 300
    return-void
.end method
