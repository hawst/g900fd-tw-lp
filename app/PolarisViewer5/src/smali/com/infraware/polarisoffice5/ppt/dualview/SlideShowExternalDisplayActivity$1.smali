.class Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;
.super Landroid/os/Handler;
.source "SlideShowExternalDisplayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 124
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 137
    :goto_0
    return-void

    .line 126
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    const/4 v1, 0x1

    # setter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mbSlideShow:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$002(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;Z)Z

    .line 127
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    .line 129
    .local v7, "display":Landroid/view/Display;
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$300(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideShowStartPage:I
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$100(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)I

    move-result v3

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;
    invoke-static {v4}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$200(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;
    invoke-static {v5}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$200(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->getHeight()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISlideShow(IIIIIZ)V

    goto :goto_0

    .line 132
    .end local v7    # "display":Landroid/view/Display;
    :pswitch_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$1;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->getSlideNoteString()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$400(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
