.class public Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;
.super Landroid/opengl/GLSurfaceView;
.source "SlideShowGLSurfaceView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;
    }
.end annotation


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private bSurfaceCreated:Z

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mHeight:I

.field private mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mWidth:I

.field private m_bSurfaceChanged:Z

.field public myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const-string/jumbo v0, "SlideShowGLSurfaceView"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->LOG_CAT:Ljava/lang/String;

    .line 18
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 21
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->bSurfaceCreated:Z

    .line 22
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->m_bSurfaceChanged:Z

    .line 24
    iput-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    .line 26
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mWidth:I

    .line 27
    iput v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mHeight:I

    .line 41
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 42
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 43
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setEGLContextClientVersion(I)V

    .line 44
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    invoke-direct {v0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    .line 45
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    invoke-virtual {p0, v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 46
    invoke-virtual {p0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->setRenderMode(I)V

    .line 47
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->bSurfaceCreated:Z

    .line 48
    return-void
.end method


# virtual methods
.method public drawAllContents()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 59
    const-string/jumbo v0, "SlideShowGLSurfaceView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "drawAllContents"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    iget v2, v2, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsSlideShow()Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowContinue()V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IIsSlideShow()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 70
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->requestRender()V

    goto :goto_0

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->myRenderer:Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;

    iget v0, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer;->mStatus:I

    if-ne v0, v3, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->requestRender()V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 86
    return-void
.end method

.method public setEvInterface(Lcom/infraware/office/evengine/EvInterface;)V
    .locals 0
    .param p1, "evInterface"    # Lcom/infraware/office/evengine/EvInterface;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 52
    return-void
.end method

.method public setSlideShowSurfaceViewListener(Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    .line 56
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v3, 0x1

    .line 89
    invoke-super {p0, p1, p2, p3, p4}, Landroid/opengl/GLSurfaceView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 90
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->bSurfaceCreated:Z

    if-nez v0, :cond_2

    .line 91
    const-string/jumbo v0, "SlideShowGLSurfaceView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "surfaceChanged_create width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    invoke-interface {v0, p3, p4}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;->onSurfaceCreated(II)V

    .line 95
    :cond_0
    iput p3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mWidth:I

    .line 96
    iput p4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mHeight:I

    .line 97
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->bSurfaceCreated:Z

    .line 114
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->drawAllContents()V

    .line 115
    return-void

    .line 100
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mWidth:I

    if-ne v0, p3, :cond_3

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mHeight:I

    if-eq v0, p4, :cond_1

    .line 101
    :cond_3
    const-string/jumbo v0, "SlideShowGLSurfaceView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "surfaceChanged_resize width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iput p3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mWidth:I

    .line 104
    iput p4, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mHeight:I

    .line 106
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_4

    .line 107
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, v3, p3, p4}, Lcom/infraware/office/evengine/EvInterface;->IChangeScreen(III)V

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    if-eqz v0, :cond_5

    .line 109
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mHeight:I

    invoke-interface {v0, v1, p4}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;->onSurfaceChanged(II)V

    .line 110
    :cond_5
    iput-boolean v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->m_bSurfaceChanged:Z

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 119
    const-string/jumbo v0, "SlideShowGLSurfaceView"

    const-string/jumbo v1, "surfaceCreated"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 124
    const-string/jumbo v1, "SlideShowGLSurfaceView"

    const-string/jumbo v2, "surfaceDestroyed"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->mListener:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;

    invoke-interface {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView$SlideShowGLSurfaceViewListener;->onSurfaceDestroyed()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
