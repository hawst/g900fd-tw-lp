.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;
.super Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;
.source "SlideShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0, p1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$HandlerLoop;-><init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 321
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_THUMBNAILOFSLIDESHOW()Z

    move-result v0

    if-nez v0, :cond_1

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 325
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideGLImage:Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGLSurfaceView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$900(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/common/PointerDrawView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerOption:Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/common/marker/MarkerOptionsView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mMarkerMode:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserMode:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEraserAllMode:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1400(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->getGestureMode()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mGLGestureProc:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1500(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureProc;->getGestureMode()I

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    move v6, v1

    .line 339
    .local v6, "isSlideShowBusy":Z
    :goto_1
    if-nez v6, :cond_9

    .line 340
    const/4 v7, 0x0

    .line 341
    .local v7, "item":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    const/4 v4, 0x1

    .line 343
    .local v4, "popPageNum":I
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 344
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 345
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    add-int/lit8 v2, v4, -0x1

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v7

    .line 347
    if-eqz v7, :cond_3

    .line 348
    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->getExistImage()Z

    move-result v0

    if-eq v0, v1, :cond_3

    .line 357
    :cond_4
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSeekBarMoveHistory:Ljava/util/Stack;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-ne v0, v1, :cond_8

    .line 358
    iget v9, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->mLastProgress:I

    .local v9, "progress":I
    :goto_2
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getMax()I

    move-result v0

    if-gt v9, v0, :cond_7

    .line 359
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0, v9}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getPageFromProgress(I)I

    move-result v4

    .line 361
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideDragNDropAdapter:Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;

    move-result-object v0

    add-int/lit8 v2, v4, -0x1

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;

    move-result-object v7

    .line 363
    if-eqz v7, :cond_5

    .line 364
    invoke-virtual {v7}, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->getExistImage()Z

    move-result v0

    if-ne v0, v1, :cond_7

    .line 365
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mLastProgress percent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->mLastProgress:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "% (100%)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->mLastProgress:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->mLastProgress:I

    .line 358
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .end local v4    # "popPageNum":I
    .end local v6    # "isSlideShowBusy":Z
    .end local v7    # "item":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    .end local v9    # "progress":I
    :cond_6
    move v6, v5

    .line 325
    goto/16 :goto_1

    .line 375
    .restart local v4    # "popPageNum":I
    .restart local v6    # "isSlideShowBusy":Z
    .restart local v7    # "item":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    .restart local v9    # "progress":I
    :cond_7
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->mLastProgress:I

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-le v0, v1, :cond_8

    .line 376
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->killHandlerLoop()V

    goto/16 :goto_0

    .line 381
    .end local v9    # "progress":I
    :cond_8
    const-string/jumbo v0, "SlideShowActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Call ISlideObjStartEx to get bitmap, Page :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImageWidth:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideImageHeight:I
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$1900(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v2

    const v3, 0x3ecccccd    # 0.4f

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvInterface;->ISlideObjStartEx(IIFIZ)V

    .line 387
    .end local v4    # "popPageNum":I
    .end local v7    # "item":Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
    :cond_9
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->isDie:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->isFinish()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    const-string/jumbo v0, "SlideShowActivity"

    const-string/jumbo v1, "Next Handler Loop"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    invoke-virtual {p0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->obtainMessage()Landroid/os/Message;

    move-result-object v8

    .line 390
    .local v8, "nextTryMsg":Landroid/os/Message;
    const/4 v0, 0x6

    iput v0, v8, Landroid/os/Message;->what:I

    .line 392
    const-wide/16 v0, 0x190

    invoke-virtual {p0, v8, v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 323
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
