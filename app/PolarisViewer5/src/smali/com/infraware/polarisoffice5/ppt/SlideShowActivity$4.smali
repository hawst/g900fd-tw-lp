.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;
.super Landroid/os/CountDownTimer;
.source "SlideShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 431
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->ModeSwitchTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 443
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbFinishCalled:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2300(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onChangeMode(I)V

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsAutoModeSwitch:Z

    .line 445
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "arg0"    # J

    .prologue
    .line 436
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$4;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsAutoModeSwitch:Z

    .line 437
    return-void
.end method
