.class public Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;
.super Ljava/lang/Object;
.source "SlideShowExternalDisplayGestureProc.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_HID_ACTION;
.implements Lcom/infraware/office/evengine/E$EV_SLIDESHOW_PLAY_TYPE;
.implements Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;,
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$GestureMode;
    }
.end annotation


# instance fields
.field private final LOG_CAT:Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;

.field private mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

.field private mGestureMode:I

.field private mListener:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;

.field private mPenDragTime:[I

.field private mPenDragX:[I

.field private mPenDragY:[I

.field private mPenPressure:[I

.field private mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

.field private mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

.field private mSlideImage:Landroid/view/View;

.field private mSlideShowMode:I


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;Landroid/view/View;Lcom/infraware/polarisoffice5/common/PointerDrawView;Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "listener"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;
    .param p3, "slideImage"    # Landroid/view/View;
    .param p4, "pointerDraw"    # Lcom/infraware/polarisoffice5/common/PointerDrawView;
    .param p5, "slideExternalImage"    # Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x100

    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string/jumbo v0, "SlideShowExternalDisplayGestureProc"

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->LOG_CAT:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mActivity:Landroid/app/Activity;

    .line 24
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    .line 25
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;

    .line 26
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideImage:Landroid/view/View;

    .line 27
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 28
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    .line 30
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    .line 31
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    .line 33
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragX:[I

    .line 34
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragY:[I

    .line 36
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragTime:[I

    .line 37
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenPressure:[I

    .line 51
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mActivity:Landroid/app/Activity;

    .line 52
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 53
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-direct {v0, p1, p0, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;-><init>(Landroid/app/Activity;Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector$SlideShowGestureDetectorListener;Z)V

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    .line 54
    iput-object p2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;

    .line 55
    iput-object p3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideImage:Landroid/view/View;

    .line 56
    iput-object p4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 57
    iput-object p5, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    .line 58
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideImage:Landroid/view/View;

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 59
    return-void
.end method


# virtual methods
.method public Gesturefinalize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->GestureDetectorFinalize()V

    .line 64
    :cond_0
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 65
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    .line 66
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;

    .line 67
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 68
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideImage:Landroid/view/View;

    .line 69
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    .line 70
    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    .line 71
    return-void
.end method

.method public cancelGesture()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->cancel()V

    .line 76
    :cond_0
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 178
    const-string/jumbo v0, "SlideShowExternalDisplayGestureProc"

    const-string/jumbo v1, "onDoubleTap"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x3

    iput v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 185
    const-string/jumbo v0, "SlideShowExternalDisplayGestureProc"

    const-string/jumbo v1, "onDoubleTapConfirmed"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mListener:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;

    invoke-interface {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc$SlideShowGestureProcListener;->onDoubleTapConfirmed()V

    .line 187
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 192
    const-string/jumbo v0, "SlideShowExternalDisplayGestureProc"

    const-string/jumbo v1, "onFling"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    if-ne v0, v2, :cond_0

    .line 194
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 195
    iput v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    .line 196
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mActivity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    invoke-virtual {v0, v2}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->slideShowPlay(I)V

    .line 206
    :cond_0
    :goto_0
    return v2

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mActivity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->slideShowPlay(I)V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 165
    const-string/jumbo v0, "SlideShowExternalDisplayGestureProc"

    const-string/jumbo v1, "onSingleTapConfirmed"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    if-eq v0, v3, :cond_0

    .line 167
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 168
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mActivity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    invoke-virtual {v0, v3}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->slideShowPlay(I)V

    .line 172
    :cond_0
    iput v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    .line 173
    return v2
.end method

.method public onTouchDown(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 88
    const-string/jumbo v0, "SlideShowExternalDisplayGestureProc"

    const-string/jumbo v2, "onTouchDown"

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    if-ne v0, v3, :cond_1

    .line 90
    :cond_0
    iput v7, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    .line 91
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    if-ne v0, v3, :cond_2

    .line 92
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onTouch(Landroid/view/MotionEvent;)V

    .line 93
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->onTouch(Landroid/view/MotionEvent;)V

    .line 98
    :cond_1
    :goto_0
    return v7

    .line 95
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_1

    .line 96
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    long-to-int v5, v4

    const/4 v4, -0x1

    invoke-static {p1, v4}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    move v4, v1

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto :goto_0
.end method

.method public onTouchMove(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 15
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 119
    const-string/jumbo v1, "SlideShowExternalDisplayGestureProc"

    const-string/jumbo v2, "onTouchMove"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 121
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 122
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onTouch(Landroid/view/MotionEvent;)V

    .line 123
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->onTouch(Landroid/view/MotionEvent;)V

    .line 160
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 125
    :cond_1
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    .line 126
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    .line 127
    .local v6, "N":I
    if-lez v6, :cond_3

    const/16 v1, 0x64

    if-ge v6, v1, :cond_3

    .line 128
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_1
    if-ge v14, v6, :cond_2

    .line 129
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragX:[I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    float-to-int v2, v2

    aput v2, v1, v14

    .line 130
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragY:[I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v2

    float-to-int v2, v2

    aput v2, v1, v14

    .line 132
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragTime:[I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v1, v14

    .line 133
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenPressure:[I

    move-object/from16 v0, p2

    invoke-static {v0, v14}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v2

    aput v2, v1, v14

    .line 128
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragX:[I

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragY:[I

    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragTime:[I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenPressure:[I

    invoke-virtual/range {v1 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetPenPosition([I[I[I[II)V

    goto :goto_0

    .line 137
    .end local v14    # "j":I
    :cond_3
    if-nez v6, :cond_4

    .line 138
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragX:[I

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    aput v3, v1, v2

    .line 139
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragY:[I

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    aput v3, v1, v2

    .line 141
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragTime:[I

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    long-to-int v3, v3

    aput v3, v1, v2

    .line 142
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenPressure:[I

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v3

    aput v3, v1, v2

    .line 144
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragX:[I

    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragY:[I

    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragTime:[I

    iget-object v11, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenPressure:[I

    const/4 v12, 0x1

    invoke-virtual/range {v7 .. v12}, Lcom/infraware/office/evengine/EvInterface;->ISetPenPosition([I[I[I[II)V

    goto/16 :goto_0

    .line 147
    :cond_4
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragX:[I

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    aput v3, v1, v2

    .line 148
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragY:[I

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    aput v3, v1, v2

    .line 150
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragTime:[I

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    long-to-int v3, v3

    aput v3, v1, v2

    .line 151
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenPressure:[I

    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v3

    aput v3, v1, v2

    .line 153
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v8, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragX:[I

    iget-object v9, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragY:[I

    iget-object v10, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenDragTime:[I

    iget-object v11, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPenPressure:[I

    const/4 v12, 0x1

    invoke-virtual/range {v7 .. v12}, Lcom/infraware/office/evengine/EvInterface;->ISetPenPosition([I[I[I[II)V

    goto/16 :goto_0

    .line 156
    .end local v6    # "N":I
    :cond_5
    iget v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    .line 157
    iget-object v7, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    const/4 v8, 0x1

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v9, v1

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v10, v1

    const/4 v11, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    long-to-int v12, v1

    const/4 v1, -0x1

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v13

    invoke-virtual/range {v7 .. v13}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto/16 :goto_0
.end method

.method public onTouchUp(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x2

    .line 103
    const-string/jumbo v0, "SlideShowExternalDisplayGestureProc"

    const-string/jumbo v2, "onTouchUp"

    invoke-static {v0, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    if-ne v0, v7, :cond_0

    .line 105
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    if-ne v0, v1, :cond_2

    .line 106
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mPointerDraw:Lcom/infraware/polarisoffice5/common/PointerDrawView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/common/PointerDrawView;->onTouch(Landroid/view/MotionEvent;)V

    .line 107
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideExternalImage:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;

    invoke-virtual {v0, p1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalSurfaceView;->onTouch(Landroid/view/MotionEvent;)V

    .line 112
    :cond_0
    :goto_0
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    if-eq v0, v1, :cond_1

    .line 113
    iput v4, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureMode:I

    .line 114
    :cond_1
    return v7

    .line 109
    :cond_2
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v5, v5

    const/4 v6, -0x1

    invoke-static {p1, v6}, Lcom/infraware/common/util/Utils;->getPressure(Landroid/view/MotionEvent;I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->IHIDAction(IIIIII)V

    goto :goto_0
.end method

.method public setSlideShowMode(I)V
    .locals 2
    .param p1, "slideShowMode"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    .line 80
    iget v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mSlideShowMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->SetAlwaysInMoveRegion(Z)V

    .line 84
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayGestureProc;->mGestureDetector:Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowGestureDetector;->SetAlwaysInMoveRegion(Z)V

    goto :goto_0
.end method
