.class public Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;
.super Ljava/lang/Object;
.source "SlideDragNDropListItem.java"


# instance fields
.field private mImage:Landroid/graphics/Bitmap;

.field private mbExistImage:Z

.field private mbSlideHide:Z


# direct methods
.method public constructor <init>(ZLandroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bExistImage"    # Z
    .param p2, "image"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mbExistImage:Z

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mImage:Landroid/graphics/Bitmap;

    .line 8
    iput-boolean v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mbSlideHide:Z

    .line 11
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mbExistImage:Z

    .line 12
    iput-object p2, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mImage:Landroid/graphics/Bitmap;

    .line 13
    return-void
.end method


# virtual methods
.method public getExistImage()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mbExistImage:Z

    return v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public isHideSlide()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mbSlideHide:Z

    return v0
.end method

.method public setExistImage(Z)V
    .locals 0
    .param p1, "bExistImage"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mbExistImage:Z

    .line 29
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mImage:Landroid/graphics/Bitmap;

    .line 21
    return-void
.end method

.method public setSlideHideInfo(Z)V
    .locals 0
    .param p1, "slideHide"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideDragNDropListItem;->mbSlideHide:Z

    .line 33
    return-void
.end method
