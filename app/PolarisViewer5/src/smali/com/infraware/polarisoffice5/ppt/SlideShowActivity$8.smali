.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;
.super Ljava/lang/Object;
.source "SlideShowActivity.java"

# interfaces
.implements Lcom/infraware/polarisoffice5/ppt/SlideShowGLRenderer$SlideShowGLRendererListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 684
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContinue()V
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowContinue()V

    .line 720
    return-void
.end method

.method public onPlay(II)V
    .locals 5
    .param p1, "nPlayType"    # I
    .param p2, "nPage"    # I

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 696
    if-ne p1, v2, :cond_1

    .line 698
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    .line 699
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(I)V

    .line 700
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    .line 716
    :cond_0
    :goto_0
    return-void

    .line 702
    :cond_1
    if-ne p1, v3, :cond_2

    .line 704
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    .line 705
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(I)V

    .line 706
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    goto :goto_0

    .line 708
    :cond_2
    if-ne p1, v4, :cond_0

    .line 709
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/infraware/office/evengine/EvInterface;->IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v1

    iget v0, v1, Lcom/infraware/office/evengine/EV$CONFIG_INFO;->nCurPage:I

    .line 710
    .local v0, "currentPageNum":I
    if-eq v0, p2, :cond_0

    .line 713
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->changeScreenForSlideShow()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    .line 714
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v4, p2, v2}, Lcom/infraware/office/evengine/EvInterface;->ISlideShowPlay(III)V

    goto :goto_0
.end method

.method public onStart(II)V
    .locals 8
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 686
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mbSlideShow:Z
    invoke-static {v0, v7}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2502(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z

    .line 688
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->isSupportSlideShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v3

    move v1, p1

    move v2, p2

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISlideShow(IIIIIZ)V

    .line 694
    :goto_0
    return-void

    .line 691
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mSlideShowStartPage:I
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2600(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)I

    move-result v3

    move v1, p1

    move v2, p2

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v7}, Lcom/infraware/office/evengine/EvInterface;->ISlideShow(IIIIIZZ)V

    goto :goto_0
.end method

.method public onUpdateSeekBar()V
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$8;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->updateSeekBarPos()V

    .line 723
    return-void
.end method
