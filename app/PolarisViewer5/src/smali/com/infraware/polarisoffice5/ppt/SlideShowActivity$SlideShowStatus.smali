.class public Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;
.super Ljava/lang/Object;
.source "SlideShowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SlideShowStatus"
.end annotation


# static fields
.field public static final ERASER_STATUS:[I

.field public static final MARKER_OPTION_STATUS:[I

.field public static final MARKER_STATUS:[I

.field public static final POINTER_OPTION_STATUS:[I

.field public static final POINTER_STATUS:[I

.field public static final SELECT_STATUS:[I

.field public static final SHOW_END_STATUS:[I

.field public static final SLIDE_STATUS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 225
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->SLIDE_STATUS:[I

    .line 226
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->POINTER_STATUS:[I

    .line 227
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->POINTER_OPTION_STATUS:[I

    .line 228
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->MARKER_STATUS:[I

    .line 229
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->MARKER_OPTION_STATUS:[I

    .line 230
    new-array v0, v1, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->ERASER_STATUS:[I

    .line 231
    new-array v0, v1, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->SELECT_STATUS:[I

    .line 233
    new-array v0, v1, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$SlideShowStatus;->SHOW_END_STATUS:[I

    return-void

    .line 225
    :array_0
    .array-data 4
        0x0
        0x8
        0x8
        0x8
        0x8
        0x8
        0x8
    .end array-data

    .line 226
    :array_1
    .array-data 4
        0x0
        0x0
        0x8
        0x0
        0x8
        0x8
        0x8
    .end array-data

    .line 227
    :array_2
    .array-data 4
        0x0
        0x0
        0x8
        0x0
        0x8
        0x0
        0x8
    .end array-data

    .line 228
    :array_3
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x8
        0x8
        0x8
    .end array-data

    .line 229
    :array_4
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x8
        0x8
        0x0
    .end array-data

    .line 230
    :array_5
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x8
        0x8
        0x8
    .end array-data

    .line 231
    :array_6
    .array-data 4
        0x0
        0x8
        0x8
        0x0
        0x0
        0x8
        0x8
    .end array-data

    .line 233
    :array_7
    .array-data 4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
