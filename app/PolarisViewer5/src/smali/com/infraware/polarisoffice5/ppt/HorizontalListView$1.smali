.class Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;
.super Landroid/database/DataSetObserver;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/HorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    .line 132
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    monitor-enter v1

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    const/4 v2, 0x1

    # setter for: Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->mDataChanged:Z
    invoke-static {v0, v2}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->access$002(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;Z)Z

    .line 134
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->invalidate()V

    .line 136
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->requestLayout()V

    .line 137
    return-void

    .line 134
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    # invokes: Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->reset()V
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->access$100(Lcom/infraware/polarisoffice5/ppt/HorizontalListView;)V

    .line 142
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->invalidate()V

    .line 143
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/HorizontalListView$1;->this$0:Lcom/infraware/polarisoffice5/ppt/HorizontalListView;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/HorizontalListView;->requestLayout()V

    .line 144
    return-void
.end method
