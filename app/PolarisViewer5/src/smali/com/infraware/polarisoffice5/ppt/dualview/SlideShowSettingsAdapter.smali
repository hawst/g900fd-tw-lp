.class public Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;
.super Landroid/widget/BaseAdapter;
.source "SlideShowSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;,
        Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;
    }
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mSlideShowSettingsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 19
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 20
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mSlideShowSettingsList:Ljava/util/List;

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 48
    .local v0, "resources":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mSlideShowSettingsList:Ljava/util/List;

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;

    const v3, 0x7f070223

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020059

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mSlideShowSettingsList:Ljava/util/List;

    new-instance v2, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;

    const v3, 0x7f070221

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020058

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;-><init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mSlideShowSettingsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 61
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mSlideShowSettingsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 62
    :cond_0
    const/4 v0, 0x0

    .line 63
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mSlideShowSettingsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 68
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 73
    if-nez p2, :cond_0

    .line 74
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03004e

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 75
    new-instance v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;-><init>()V

    .line 76
    .local v0, "holder":Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;
    const v2, 0x7f0b021e

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;->mText:Landroid/widget/TextView;

    .line 77
    const v2, 0x7f0b021d

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;->mImage:Landroid/widget/ImageView;

    .line 78
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 84
    :goto_0
    invoke-virtual {p0, p1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter;->getItem(I)Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;

    move-result-object v1

    .line 85
    .local v1, "listItem":Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;
    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;->mText:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v2, v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;->getImage()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 87
    return-object p2

    .line 81
    .end local v0    # "holder":Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;
    .end local v1    # "listItem":Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$SlideShowSettingsItem;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowSettingsAdapter$ViewHolder;
    goto :goto_0
.end method
