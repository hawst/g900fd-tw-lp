.class Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;
.super Ljava/lang/Object;
.source "PPTMainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onSurfaceChanged(IIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0

    .prologue
    .line 824
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 827
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbWaitingPSlideManageShow:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1200(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 829
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowSlideManageOpenButton()Z

    .line 831
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->surfaceBackUp:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1300(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1400(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z
    invoke-static {v0, v2}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1402(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z

    .line 833
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->surfaceBackUp:Z
    invoke-static {v0, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1302(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;Z)Z

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mbPSlideMangeOpen:Z
    invoke-static {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1400(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 837
    const-string/jumbo v0, "PPTMainActivity"

    const-string/jumbo v1, "***onSurfaceChanged calling onShowPSlideManage()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    iget-object v0, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$7;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-virtual {v0}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->onShowPSlideManage()V

    .line 842
    :cond_1
    return-void
.end method
