.class Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;
.super Landroid/content/BroadcastReceiver;
.source "PPTMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x2

    const/4 v5, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 566
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 567
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v4, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 568
    const-string/jumbo v4, "state"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 569
    .local v3, "state":Z
    if-eqz v3, :cond_1

    .line 570
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    const/4 v5, 0x4

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I
    invoke-static {v4, v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1002(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)I

    .line 599
    .end local v3    # "state":Z
    :cond_0
    :goto_0
    return-void

    .line 574
    .restart local v3    # "state":Z
    :cond_1
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mHDMIResolution:I
    invoke-static {v4, v6}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1002(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)I

    goto :goto_0

    .line 578
    .end local v3    # "state":Z
    :cond_2
    const-string/jumbo v4, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 579
    const-string/jumbo v4, "state"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 580
    .local v3, "state":I
    if-ne v3, v7, :cond_5

    .line 581
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I
    invoke-static {v4, v6}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1102(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)I

    .line 582
    const-string/jumbo v4, "resBitMask"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 583
    .local v2, "resBitMask":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    sget-object v4, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 584
    sget-object v4, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    aget-object v4, v4, v1

    aget v4, v4, v6

    and-int/2addr v4, v2

    if-eqz v4, :cond_3

    .line 585
    sget-object v4, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I
    invoke-static {v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I

    move-result v5

    aget-object v4, v4, v5

    aget v4, v4, v7

    sget-object v5, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    aget-object v5, v5, v1

    aget v5, v5, v7

    if-ge v4, v5, :cond_4

    .line 586
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I
    invoke-static {v4, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1102(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)I

    .line 583
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 588
    :cond_4
    sget-object v4, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I
    invoke-static {v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I

    move-result v5

    aget-object v4, v4, v5

    aget v4, v4, v7

    sget-object v5, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    aget-object v5, v5, v1

    aget v5, v5, v7

    if-ne v4, v5, :cond_3

    .line 589
    sget-object v4, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    iget-object v5, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I
    invoke-static {v5}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1100(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;)I

    move-result v5

    aget-object v4, v4, v5

    aget v4, v4, v8

    sget-object v5, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->ExternalDisplayResolution:[[I

    aget-object v5, v5, v1

    aget v5, v5, v8

    if-ge v4, v5, :cond_3

    .line 590
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I
    invoke-static {v4, v1}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1102(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)I

    goto :goto_2

    .line 595
    .end local v1    # "i":I
    .end local v2    # "resBitMask":I
    :cond_5
    if-nez v3, :cond_0

    .line 596
    iget-object v4, p0, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity$5;->this$0:Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->mWIFIDisplayResolution:I
    invoke-static {v4, v6}, Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;->access$1102(Lcom/infraware/polarisoffice5/ppt/PPTMainActivity;I)I

    goto/16 :goto_0
.end method
