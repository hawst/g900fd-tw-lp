.class Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;
.super Ljava/lang/Object;
.source "SlideShowActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V
    .locals 0

    .prologue
    .line 738
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 745
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->cancelModeSwitchTimer()V
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)V

    .line 746
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    iget-boolean v3, v3, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bStateLastSlide:Z

    if-nez v3, :cond_0

    .line 747
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->ModeSwitchTimer:Landroid/os/CountDownTimer;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2200(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/os/CountDownTimer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 750
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 764
    :cond_1
    :pswitch_0
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPrevPage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2100(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mNextPage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2000(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    .line 798
    :goto_0
    return v1

    .line 753
    :pswitch_1
    const-string/jumbo v3, "SlideShowActivity"

    const-string/jumbo v4, "mPreviewSeekbarListener MotionEvent.ACTION_UP or ACTION_CANCEL Start"

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsSeekbarDragging:Z
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2900(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 756
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsSeekbarDragging:Z
    invoke-static {v2, v1}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2902(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z

    .line 757
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 758
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->saveSeekBarHistroy()V

    .line 759
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->getPageFromCurrentProgress()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->onClickPlayPage(I)V

    goto :goto_0

    .line 769
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    :pswitch_2
    goto :goto_0

    .line 772
    :pswitch_3
    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # setter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->m_bIsSeekbarDragging:Z
    invoke-static {v3, v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$2902(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;Z)Z

    .line 773
    const-string/jumbo v2, "SlideShowActivity"

    const-string/jumbo v3, "mPreviewSeekbarListener MotionEvent.ACTION_DOWN Start"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewSeekBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$700(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_4

    .line 775
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 776
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 777
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 779
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v2, p2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->addViewToPreviewIamgeLayout(Landroid/view/MotionEvent;)V

    .line 780
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    # getter for: Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->mPreviewImageFrameLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->access$800(Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 781
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->saveSeekBarHistroy()V

    goto/16 :goto_0

    .line 784
    :pswitch_4
    const-string/jumbo v2, "SlideShowActivity"

    const-string/jumbo v3, "mPreviewSeekbarListener MotionEvent.ACTION_MOVE Start"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v2, p2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->positionPreviewImage(Landroid/view/MotionEvent;)V

    .line 786
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v2, p2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->addViewToPreviewIamgeLayout(Landroid/view/MotionEvent;)V

    .line 787
    iget-object v2, p0, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity$10;->this$0:Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;

    invoke-virtual {v2}, Lcom/infraware/polarisoffice5/ppt/SlideShowActivity;->saveSeekBarHistroy()V

    goto/16 :goto_0

    .line 750
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 769
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
