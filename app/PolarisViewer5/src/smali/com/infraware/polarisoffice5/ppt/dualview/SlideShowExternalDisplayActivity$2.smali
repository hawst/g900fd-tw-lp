.class Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "SlideShowExternalDisplayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;


# direct methods
.method constructor <init>(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 145
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    const-string/jumbo v1, "state"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onFinish()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$500(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    const-string/jumbo v1, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    const-string/jumbo v1, "state"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity$2;->this$0:Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;

    # invokes: Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->onFinish()V
    invoke-static {v1}, Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;->access$500(Lcom/infraware/polarisoffice5/ppt/dualview/SlideShowExternalDisplayActivity;)V

    goto :goto_0
.end method
